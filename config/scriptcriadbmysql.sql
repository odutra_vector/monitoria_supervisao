CREATE DATABASE IF NOT EXISTS $db DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci/

CREATE TABLE IF NOT EXISTS `$db`.`agcalibragem` (
  `idagcalibragem` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `dataini` date DEFAULT NULL,
  `datafim` date DEFAULT NULL,
  `participantes` varchar(20) NULL DEFAULT NULL,
  `usercont` varchar(20) NULL DEFAULT NULL,
  `idmonitoria` int(7) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `gravacao` varchar(1000) DEFAULT NULL,
  `idmonicalib` varchar(20) NULL DEFAULT NULL,
  `finalizado` enum('S','N') DEFAULT 'N',
  `useragenda` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  KEY `idagcalibragem` (`idagcalibragem`),
  KEY `idmonitoria` (`idmonitoria`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`alertas` (
  `idalertas` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomealerta` varchar(100) DEFAULT NULL,
  `automatico` enum('S','N') NOT NULL DEFAULT 'N',
  `coralerta` varchar(10) DEFAULT NULL,
  `idconfemailenv` varchar(6) DEFAULT 'NENHUM',
  `userfg` enum('S','N') DEFAULT 'N',
  `user_web` varchar(1000) DEFAULT NULL,
  `user_adm` varchar(1000) DEFAULT NULL,
  `ativo` enum('S','N') NOT NULL DEFAULT 'S',
  PRIMARY KEY (`idalertas`),
  KEY `nomealerta` (`nomealerta`),
  KEY `idconfemailenv` (`idconfemailenv`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1/

CREATE TABLE IF NOT EXISTS `$db`.`atualiza_status` (
  `idatualiza_status` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `tab_user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `iduser` int(4) unsigned zerofill NOT NULL,
  `qtdeatu` int(6) NOT NULL,
  PRIMARY KEY (`idatualiza_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`aval_plan` (
  `idaval_plan` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomeaval_plan` varchar(50) DEFAULT NULL,
  `nivel` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  PRIMARY KEY (`idaval_plan`),
  KEY `nomeaval_plan` (`nomeaval_plan`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`camposparam` (
  `idcamposparam` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idparam_moni` int(2) unsigned zerofill DEFAULT NULL,
  `idcoluna_oper` int(2) unsigned zerofill DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  `classifica` enum('S','N') DEFAULT 'N',
  `tipoclass` enum('NENHUM','DECRESCENTE','CRESCENTE') DEFAULT 'NENHUM',
  `visumoni` enum('S','N') DEFAULT 'S',
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idcamposparam`),
  KEY `idparam_moni` (`idparam_moni`),
  KEY `idcoluna_oper` (`idcoluna_oper`),
  KEY `posicao` (`posicao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`cat_ebook` (
  `idcat_ebook` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomecat_ebook` varchar(50) DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idcat_ebook`),
  UNIQUE KEY `nomecat_ebook` (`nomecat_ebook`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`chat` (
  `idchat` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  `iduser_para` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser_para` varchar(10) DEFAULT NULL,
  `assunto` varchar(100) DEFAULT NULL,
  `msg` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idchat`),
  KEY `iduser` (`iduser`),
  KEY `assunto` (`assunto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`coluna_oper` (
  `idcoluna_oper` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `incorpora` enum('SUPER_OPER','OPERADOR','DADOS') DEFAULT NULL,
  `nomecoluna` varchar(50) DEFAULT NULL,
  `unico` enum('S','N') DEFAULT NULL,
  `tipo` enum('VARCHAR','INT','TIME','DATE','TEXT') DEFAULT NULL,
  `tamanho` varchar(6) DEFAULT NULL,
  `caracter` varchar(100) DEFAULT NULL,
  `restricao` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`idcoluna_oper`),
  KEY `nomecoluna` (`nomecoluna`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`confemailenv` (
  `idconfemailenv` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `tipo` VARCHAR (200) DEFAULT NULL,
  `fundoimg` text,
  `titulo` varchar(200) DEFAULT NULL,
  `msg` text,
  `campo` varchar(300) DEFAULT NULL,
  `iduser_adm` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idconfemailenv`),
  KEY `iduser_adm` (`iduser_adm`),
  KEY `titulo` (`titulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`configselecao` (
  `idconfigselecao` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill NOT NULL,
  `qtdedia` int(4) DEFAULT NULL,
  `reposicao` int(4) NOT NULL DEFAULT '0',
  `dmenos` int(3) DEFAULT NULL,
  PRIMARY KEY (`idconfigselecao`),
  UNIQUE KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1/

CREATE TABLE IF NOT EXISTS `$db`.`conf_calib` (
  `idconf_calib` int(1) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `bloq` enum('N','S') DEFAULT 'N',
  `tempobloq` varchar(5) DEFAULT NULL,
  `idconfemailenv` int(2) unsigned zerofill DEFAULT NULL,
  `intercambio` enum('N','S') DEFAULT 'N',
  `camposag` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `idusers_acesso` varchar(1000) DEFAULT NULL,
  `ativo` enum('N','S') DEFAULT 'N',
  PRIMARY KEY (`idconf_calib`),
  KEY `idconfemailenv` (`idconfemailenv`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`conf_chat` (
  `idconf_chat` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `assunto` varchar(100) DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idconf_chat`),
  KEY `assunto` (`assunto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`conf_mail` (
  `idconf_mail` int(1) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  `host` varchar(50) DEFAULT NULL,
  `porta` varchar(6) DEFAULT NULL,
  `autentica` int(1) DEFAULT '1',
  `tls` enum('SIM','NAO') DEFAULT 'NAO',
  `iduser_adm` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idconf_mail`),
  KEY `iduser_adm` (`iduser_adm`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`conf_rel` (
  `idconf_rel` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idparam_moni` int(2) unsigned zerofill DEFAULT NULL,
  `idfluxo` int(4) unsigned zerofill DEFAULT NULL,
  `idconf_calib` int(1) unsigned zerofill DEFAULT NULL,
  `idplanilha_moni` varchar(100) DEFAULT NULL,
  `idplanilha_web` varchar(100) DEFAULT NULL,
  `tipoacesso` enum('L','V','R') DEFAULT NULL,
  `trocanome` enum('S','N') DEFAULT NULL,
  `trocafiltro` enum('S','N') DEFAULT NULL,
  `obriga_aud` ENUM( 'S', 'N' ) DEFAULT 'N',
  `idrel_aud` int(6) unsigned zerofill DEFAULT NULL,
  `vdn` varchar(100) DEFAULT NULL,
  `limita_meta` varchar(10) COLLATE utf8_bin NOT NULL DEFAULT 'N,0',
  `diasctt` int(11) DEFAULT '3',
  `qtdeoper` int(4) DEFAULT '0',
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idconf_rel`),
  UNIQUE KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idparam_moni` (`idparam_moni`),
  KEY `idfluxo` (`idfluxo`),
  KEY `idplanilha_moni` (`idplanilha_moni`),
  KEY `idplanilha_web` (`idplanilha_web`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `contestreg` (
  `idcontestreg` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(8) DEFAULT NULL,
  `idfila` int(8) unsigned zerofill DEFAULT NULL,
  `tipomoni` varchar(1) DEFAULT NULL,
  `obscontest` text,
  PRIMARY KEY (`idcontestreg`),
  KEY `iduser` (`iduser`),
  KEY `idfila` (`idfila`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1/

CREATE TABLE IF NOT EXISTS `$db`.`corsistema` (
  `idcorsistema` int(2) NOT NULL AUTO_INCREMENT,
  `iddados_cli` int(1) unsigned zerofill DEFAULT NULL,
  `objeto` varchar(45) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `cor` varchar(45) DEFAULT NULL,
  `tipo` enum('SISTEMA','PLANILHA','GRAFICOS') DEFAULT NULL,
  PRIMARY KEY (`idcorsistema`),
  KEY `idcorsistema` (`idcorsistema`),
  KEY `iddados_cli` (`iddados_cli`),
  KEY `cor` (`cor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`dados_cli` (
  `iddados_cli` int(1) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomedados_cli` varchar(50) DEFAULT NULL,
  `site` varchar(100) DEFAULT NULL,
  `imagem` varchar(100) DEFAULT NULL,
  `tipotopo` varchar(100) DEFAULT NULL,
  `imgbanner` varchar(100) DEFAULT NULL,
  `assdigital` enum('S','N') DEFAULT 'S',
  `textoass` text,
  PRIMARY KEY (`iddados_cli`),
  KEY `nomedados_cli` (`nomedados_cli`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`dados_monitoria` (
  `iddados_monitoria` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomedb` varchar(50) DEFAULT NULL,
  `hpa` time DEFAULT NULL,
  `hmonitor` time DEFAULT NULL,
  PRIMARY KEY (`iddados_monitoria`),
  UNIQUE KEY `nomedb` (`nomedb`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`definicao` (
  `iddefinicao` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomedefinicao` varchar(50) DEFAULT NULL,
  `tipodefinicao` enum('normal','automatico') DEFAULT 'normal',
  `ativo` enum('S','N') DEFAULT 'N',
  `idfluxo` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`iddefinicao`),
  KEY `nomedefinicao` (`nomedefinicao`),
  KEY `idfluxo` (`idfluxo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`diasdescon` (
  `iddiasdescon` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idperiodo` int(2) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`iddiasdescon`),
  KEY `idperiodo` (`idperiodo`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`dimen_estim` (
  `iddimen_estim` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `qnt_grava` int(6) DEFAULT NULL,
  `qnt_oper` int(6) DEFAULT NULL,
  `multiplicador` int(2) DEFAULT NULL,
  `estimativa` int(6) DEFAULT NULL,
  `qtdeext` int(5) DEFAULT '0',
  `tma` time DEFAULT NULL,
  `tta` varchar(10) DEFAULT NULL,
  `tm` time DEFAULT NULL,
  `ttm` varchar(10) DEFAULT NULL,
  `idperiodo` int(2) unsigned zerofill DEFAULT NULL,
  `divperiodo` int(2) DEFAULT NULL,
  `qnt_pad` int(4) DEFAULT NULL,
  `qnt_monitores` decimal(10,2) DEFAULT NULL,
  `hs_monitor` time DEFAULT NULL,
  `meta_monitor` int(4) DEFAULT NULL,
  `percpartpa` int(3) DEFAULT NULL,
  `percpartmeta` int(3) DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `horacad` time DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`iddimen_estim`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idperiodo` (`idperiodo`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`dimen_mod` (
  `iddimen_mod` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `qnt_grava` int(6) DEFAULT NULL,
  `qnt_oper` int(6) DEFAULT NULL,
  `multiplicador` int(2) DEFAULT NULL,
  `estimativa` int(6) DEFAULT NULL,
  `qtdeext` int(5) DEFAULT '0',
  `tma` time DEFAULT NULL,
  `tta` varchar(10) DEFAULT NULL,
  `tm` time DEFAULT NULL,
  `ttm` varchar(10) DEFAULT NULL,
  `idperiodo` int(2) unsigned zerofill DEFAULT NULL,
  `divperiodo` int(2) DEFAULT NULL,
  `qnt_pad` decimal(10,2) DEFAULT NULL,
  `qnt_monitores` decimal(10,2) DEFAULT NULL,
  `hs_monitor` time DEFAULT NULL,
  `meta_monitor` int(4) DEFAULT NULL,
  `percpartpa` int(3) DEFAULT NULL,
  `percpartmeta` int(3) DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `horacad` time DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`iddimen_mod`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idperiodo` (`idperiodo`),
  KEY `iduser` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`dimen_moni` (
  `iddimen_moni` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `iddimen_mod` int(6) unsigned zerofill DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `idperiodo` int(2) unsigned zerofill DEFAULT NULL,
  `meta_m` int(4) DEFAULT NULL,
  `meta_s` int(4) DEFAULT NULL,
  `meta_d` int(4) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`iddimen_moni`),
  KEY `idmonitor` (`idmonitor`),
  KEY `iddimen_mod` (`iddimen_mod`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idperiodo` (`idperiodo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`ebook` (
  `idebook` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `arquivo` text,
  `titulo` varchar(200) DEFAULT NULL,
  `palavra` varchar(1000) DEFAULT NULL,
  `descri` varchar(1000) DEFAULT NULL,
  `procedimento` varchar(1000) DEFAULT NULL,
  `datareceb` date DEFAULT NULL,
  `iduser_env` varchar(100) DEFAULT NULL,
  `tabuser_env` varchar(10) DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `horacad` time DEFAULT NULL,
  `dataalt` date DEFAULT NULL,
  `horaalt` time DEFAULT NULL,
  `tporienta` enum('TEXTO','ARQUIVO') DEFAULT NULL,
  `tipo` varchar(30) DEFAULT NULL,
  `categoria` varchar(50) DEFAULT NULL,
  `subcategoria` varchar(100) DEFAULT NULL,
  `ativo` enum('S','N') NOT NULL DEFAULT 'S',
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idebook`),
  KEY `iduser_env` (`iduser_env`),
  KEY `titulo` (`titulo`),
  KEY `categoria` (`categoria`),
  KEY `subcategoria` (`subcategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`fila_grava` (
  `idfila_grava` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idupload` int(6) unsigned zerofill DEFAULT NULL,
  `idoperador` int(7) unsigned zerofill DEFAULT NULL,
  `idsuper_oper` int(6) unsigned zerofill DEFAULT NULL,
  `idauditor` int(7) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `datactt` date DEFAULT NULL,
  `horactt` time DEFAULT NULL,
  `camaudio` varchar(300) DEFAULT NULL,
  `narquivo` varchar(300) DEFAULT NULL,
  `arquivo` varchar(300) DEFAULT NULL,
  `monitorando` int(1) DEFAULT NULL,
  `monitorado` int(1) DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idfila_grava`),
  KEY `idoperador` (`idoperador`),
  KEY `idsuper_oper` (`idsuper_oper`),
  KEY `idauditor` (`idauditor`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idupload` (`idupload`),
  KEY `narquivo` (`narquivo`(70)),
  KEY `arquivo` (`arquivo`(70)),
  KEY `idmonitor` (`idmonitor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`fila_oper` (
  `idfila_oper` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idupload` int(6) unsigned zerofill DEFAULT NULL,
  `idoperador` int(7) unsigned zerofill DEFAULT NULL,
  `idsuper_oper` int(6) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `camaudio` varchar(300) DEFAULT NULL,
  `arquivo` varchar(300) DEFAULT NULL,
  `qtdmoni` int(3) unsigned zerofill DEFAULT NULL,
  `monitorando` int(1) DEFAULT NULL,
  `monitorado` int(1) DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idfila_oper`),
  KEY `idoperador` (`idoperador`),
  KEY `idsuper_oper` (`idsuper_oper`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idupload` (`idupload`),
  KEY `camaudio` (`camaudio`(255)),
  KEY `arquivo` (`arquivo`(255)),
  KEY `idmonitor` (`idmonitor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`filtro_dados` (
  `idfiltro_dados` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomefiltro_dados` varchar(100) DEFAULT NULL,
  `idfiltro_nomes` int(3) unsigned zerofill DEFAULT NULL,
  `nivel` int(1) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idfiltro_dados`),
  KEY `idfiltro_nomes` (`idfiltro_nomes`),
  KEY `nivel` (`nivel`),
  KEY `nomefiltro_dados` (`nomefiltro_dados`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`filtro_nomes` (
  `idfiltro_nomes` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomefiltro_nomes` varchar(100) DEFAULT NULL,
  `nivel` int(1) DEFAULT NULL,
  `trocafiltro` enum('S','N') DEFAULT 'N',
  `visualiza` enum('S','N') DEFAULT 'S',
  `obrigatorio` enum('N','S') DEFAULT 'N',
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idfiltro_nomes`),
  KEY `nomefiltro_nomes` (`nomefiltro_nomes`),
  KEY `nivel` (`nivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`fluxo` (
  `idfluxo` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomefluxo` varchar(50) DEFAULT NULL,
  `atvfluxo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idfluxo`),
  UNIQUE KEY `nomefluxo` (`nomefluxo`),
  KEY `idfluxo` (`idfluxo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`formulas` (
  `idformulas` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomeformulas` varchar(50) DEFAULT NULL,
  `tipo` enum('P','RM','RR') DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idrelmoni` int(2) unsigned zerofill DEFAULT NULL,
  `idrelresult` int(2) unsigned zerofill DEFAULT NULL,
  `formula` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idformulas`),
  KEY `idplan` (`idplanilha`),
  KEY `idrelmoni` (`idrelmoni`),
  KEY `idrelresult` (`idrelresult`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`graficocorpo` (
  `idgraficocorpo` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrelresultcorpo` int(2) unsigned zerofill DEFAULT NULL,
  `cor` varchar(7) DEFAULT NULL,
  `campo` varchar(100) DEFAULT NULL,
  `tipograf` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idgraficocorpo`),
  KEY `idgraficocorpo` (`idgraficocorpo`),
  KEY `idrelresultcorpo` (`idrelresultcorpo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`grafrelmoni` (
  `idgrafrelmoni` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrelmoni` int(2) unsigned zerofill NOT NULL,
  `tipograf` varchar(30) DEFAULT NULL,
  `background` varchar(6) DEFAULT NULL,
  `cordados` varchar(6) DEFAULT NULL,
  `width` varchar(4) DEFAULT NULL,
  `heigth` varchar(4) DEFAULT NULL,
  `margin` int(3) DEFAULT NULL,
  `marginbottom` int(3) DEFAULT NULL,
  `valores` enum('S','N') DEFAULT 'S',
  `pointwidth` varchar(3) DEFAULT NULL,
  `rotacaoval` enum('NAO','45','90','180','360') DEFAULT 'NAO',
  `posicaoval_x` int(4) DEFAULT NULL,
  `posicaoval_y` int(4) DEFAULT NULL,
  `legenda` enum('SIM','NAO') DEFAULT 'NAO',
  `direcaoleg` enum('H','V') DEFAULT NULL,
  `posicaoleg_x` int(4) DEFAULT NULL,
  `posicaoleg_y` int(4) DEFAULT NULL,
  `bgcolorleg` varchar(6) DEFAULT NULL,
  `bdcolorleg` varchar(6) DEFAULT NULL,
  `rotacaocat` varchar(3) DEFAULT NULL,
  `posicaocat` enum('D','E') DEFAULT NULL,
  PRIMARY KEY (`idgrafrelmoni`),
  KEY `idrelmoni` (`idrelmoni`),
  KEY `tipograf` (`tipograf`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`grupo` (
  `idgrupo` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `descrigrupo` varchar(100) DEFAULT NULL,
  `complegrupo` varchar(300) DEFAULT NULL,
  `valor_grupo` decimal(10,2) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `filtro_vinc` enum('S','P') DEFAULT NULL,
  `idrel` int(6) unsigned zerofill DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  `tab` enum('S','N') DEFAULT 'S',
  `fg` enum('S','N') DEFAULT 'N',
  KEY `idgrupo` (`idgrupo`),
  KEY `descrigrupo` (`descrigrupo`),
  KEY `idrel` (`idrel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`histresp` (
  `idhistresp` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoria` int(7) unsigned zerofill DEFAULT NULL,
  `idmonitoria_fluxo` int(9) unsigned zerofill DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idaval_plan` int(6) unsigned zerofill DEFAULT NULL,
  `idgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `idsubgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `idpergunta` int(6) unsigned zerofill DEFAULT NULL,
  `idresposta` int(10) unsigned zerofill DEFAULT NULL,
  `obs` text,
  `data` DATE DEFAULT NULL,
  `hora` TIME DEFAULT NULL,
  `iduser_adm` int(4) unsigned zerofill  NULL DEFAULT NULL,
  PRIMARY KEY (`idhistresp`),
  KEY `idmonitoria` (`idmonitoria`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idaval_plan` (`idaval_plan`),
  KEY `idgrupo` (`idgrupo`),
  KEY `idsubgrupo` (`idsubgrupo`),
  KEY `idpergunta` (`idpergunta`),
  KEY `idresposta` (`idresposta`),
  KEY `idmonitoria_fluxo` (`idmonitoria_fluxo`),
  KEY `iduser_adm` (`iduser_adm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`indice` (
  `idindice` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `inicio` decimal(10,2) DEFAULT NULL,
  `fim` decimal(10,2) DEFAULT NULL,
  `nomeindice` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idindice`),
  KEY `idindice` (`idindice`),
  KEY `nomeindice` (`nomeindice`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`interperiodo` (
  `idinterperiodo` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idperiodo` int(2) unsigned zerofill DEFAULT NULL,
  `dataini` date DEFAULT NULL,
  `datafim` date DEFAULT NULL,
  `dtinictt` date DEFAULT NULL,
  `dtfimctt` date DEFAULT NULL,
  `semana` int(1) DEFAULT NULL,
  KEY `idinterperiodo` (`idinterperiodo`),
  KEY `idperiodo` (`idperiodo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`intervalo_notas` (
  `idintervalo_notas` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idindice` int(2) unsigned zerofill DEFAULT NULL,
  `numini` decimal(10,2) DEFAULT NULL,
  `numfim` decimal(10,2) DEFAULT NULL,
  `nomeintervalo_notas` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idintervalo_notas`),
  KEY `idindice` (`idindice`),
  KEY `numini` (`numini`),
  KEY `numfim` (`numfim`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`itenscontest` (
  `iditenscontest` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoria` int(7) unsigned zerofill DEFAULT NULL,
  `idmonitoria_fluxo` int(9) unsigned zerofill DEFAULT NULL,
  `idspergunta` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`iditenscontest`),
  KEY `idmonitoria` (`idmonitoria`),
  KEY `idmonitoria_fluxo` (`idmonitoria_fluxo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`login_adm` (
  `idlogin_adm` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `iduser_adm` int(4) unsigned zerofill DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  PRIMARY KEY (`idlogin_adm`),
  KEY `iduser_adm` (`iduser_adm`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`login_web` (
  `idlogin_web` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `iduser_web` int(4) unsigned zerofill DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  PRIMARY KEY (`idlogin_web`),
  KEY `iduser` (`iduser_web`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`moniavalia` (
  `idmonitoria` int(7) unsigned zerofill DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idaval_plan` int(4) unsigned zerofill DEFAULT NULL,
  `valor_aval` decimal(10,2) DEFAULT NULL,
  `valor_final_aval` decimal(10,2) DEFAULT NULL,
  `valor_fg` decimal(10,2) DEFAULT NULL,
  `idgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `valor_grupo` decimal(10,2) DEFAULT NULL,
  `valor_final_grup` decimal(10,2) DEFAULT NULL,
  `tab_grup` enum('S','N') DEFAULT NULL,
  `idsubgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `valor_sub` decimal(10,2) DEFAULT NULL,
  `valor_final_sub` decimal(10,2) DEFAULT NULL,
  `tab_sub` enum('S','N') DEFAULT NULL,
  `idpergunta` int(6) unsigned zerofill DEFAULT NULL,
  `tipo_resp` enum('U','M') DEFAULT NULL,
  `valor_perg` decimal(10,2) DEFAULT NULL,
  `valor_final_perg` decimal(10,2) DEFAULT NULL,
  `tab_perg` enum('S','N') DEFAULT NULL,
  `idresposta` int(6) unsigned zerofill DEFAULT NULL,
  `avalia` enum('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','AVALIACAO_FG','FG','FGM','PERC','AVALIACAO_OK') DEFAULT NULL,
  `valor_resp` decimal(10,2) DEFAULT NULL,
  `obs` text,
  KEY `idmonitoria` (`idmonitoria`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idaval_plan` (`idaval_plan`),
  KEY `idgrupo` (`idgrupo`),
  KEY `idsubgrupo` (`idsubgrupo`),
  KEY `idpergunta` (`idpergunta`),
  KEY `idresposta` (`idresposta`),
  KEY `avalia` (`avalia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`moniavaliaadm` (
  `idmonitoriaadm` int(7) unsigned zerofill DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idaval_plan` int(4) unsigned zerofill DEFAULT NULL,
  `valor_aval` decimal(10,2) DEFAULT NULL,
  `valor_final_aval` decimal(10,2) DEFAULT NULL,
  `valor_fg` decimal(10,2) DEFAULT NULL,
  `idgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `valor_grup` decimal(10,2) DEFAULT NULL,
  `valor_final_grup` decimal(10,2) DEFAULT NULL,
  `tab_grup` enum('S','N') DEFAULT NULL,
  `idsubgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `valor_sub` decimal(10,2) DEFAULT NULL,
  `valor_final_sub` decimal(10,2) DEFAULT NULL,
  `tab_sub` enum('S','N') DEFAULT NULL,
  `idpergunta` int(6) unsigned zerofill DEFAULT NULL,
  `tipo_resp` enum('U','M') DEFAULT NULL,
  `valor_perg` decimal(10,2) DEFAULT NULL,
  `valor_final_perg` decimal(10,2) DEFAULT NULL,
  `tab_perg` enum('S','N') DEFAULT NULL,
  `idresposta` int(6) unsigned zerofill DEFAULT NULL,
  `avalia` enum('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','AVALIACAO_FG','FG','FGM') DEFAULT NULL,
  `valor_resp` decimal(10,2) DEFAULT NULL,
  `obs` text,
  KEY `idmonitoriaadm` (`idmonitoriaadm`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idaval_plan` (`idaval_plan`),
  KEY `idgrupo` (`idgrupo`),
  KEY `idsubgrupo` (`idsubgrupo`),
  KEY `idpergunta` (`idpergunta`),
  KEY `idresposta` (`idresposta`),
  KEY `avalia` (`avalia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`moniavaliacalib` (
  `idmonitoriacalib` int(7) unsigned zerofill NOT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idaval_plan` int(4) unsigned zerofill DEFAULT NULL,
  `valor_aval` decimal(10,2) DEFAULT NULL,
  `valor_final_aval` decimal(10,2) DEFAULT NULL,
  `valor_fg` decimal(10,2) DEFAULT NULL,
  `idgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `valor_grupo` decimal(10,2) DEFAULT NULL,
  `valor_final_grup` decimal(10,2) DEFAULT NULL,
  `tab_grup` enum('S','N') DEFAULT NULL,
  `idsubgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `valor_sub` decimal(10,2) DEFAULT NULL,
  `valor_final_sub` decimal(10,2) DEFAULT NULL,
  `tab_sub` enum('S','N') DEFAULT NULL,
  `idpergunta` int(6) unsigned zerofill DEFAULT NULL,
  `tipo_resp` enum('U','M') DEFAULT NULL,
  `valor_perg` decimal(10,2) DEFAULT NULL,
  `valor_final_perg` decimal(10,2) DEFAULT NULL,
  `tab_perg` enum('S','N') DEFAULT NULL,
  `idresposta` int(6) unsigned zerofill DEFAULT NULL,
  `avalia` enum('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','AVALIACAO_FG','FG','FGM','AVALIACAO_OK') DEFAULT NULL,
  `valor_resp` decimal(10,2) DEFAULT NULL,
  `obs` text,
  KEY `idmonitoriacalib` (`idmonitoriacalib`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idaval_plan` (`idaval_plan`),
  KEY `idgrupo` (`idgrupo`),
  KEY `idsubgrupo` (`idsubgrupo`),
  KEY `idpergunta` (`idpergunta`),
  KEY `idresposta` (`idresposta`),
  KEY `avalia` (`avalia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monicalibtabulacao` (
  `idmonicalibtabulacao` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoriacalib` int(7) unsigned zerofill DEFAULT NULL,
  `idtabulacao` int(2) unsigned zerofill DEFAULT NULL,
  `idperguntatab` int(4) unsigned zerofill DEFAULT NULL,
  `idrespostatab` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idmonicalibtabulacao`),
  KEY `idmonitoriacalib` (`idmonitoriacalib`),
  KEY `idtabulacao` (`idtabulacao`),
  KEY `idperguntatab` (`idperguntatab`),
  KEY `idrespostatab` (`idrespostatab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monitabulacao` (
  `idmonitabulacao` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoria` int(7) unsigned zerofill DEFAULT NULL,
  `idtabulacao` int(2) unsigned zerofill DEFAULT NULL,
  `idperguntatab` int(4) unsigned zerofill DEFAULT NULL,
  `idrespostatab` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idmonitabulacao`),
  KEY `idmonitoria` (`idmonitoria`),
  KEY `idtabulacao` (`idtabulacao`),
  KEY `idperguntatab` (`idperguntatab`),
  KEY `idrespostatab` (`idrespostatab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monitor` (
  `idmonitor` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomemonitor` varchar(200) DEFAULT NULL,
  `CPF` varchar(11) DEFAULT NULL,
  `iduser_resp` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `tmonitor` enum('I','E') DEFAULT 'I',
  `senha` varchar(41) DEFAULT NULL,
  `entrada` time DEFAULT NULL,
  `saida` time DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `senha_ini` enum('S','N') DEFAULT 'S',
  `ass` enum('S','N') DEFAULT NULL,
  `status` enum('MONITORANDO','PAUSA','GRAVACAO','AGUARDANDO','OFFLINE') DEFAULT NULL,
  `hstatus` time DEFAULT NULL,
  `meta_m` int(4) DEFAULT NULL,
  `meta_s` int(4) DEFAULT NULL,
  `meta_d` int(4) DEFAULT NULL,
  `ebook` varchar(100) DEFAULT NULL,
  `confapres` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idmonitor`),
  UNIQUE KEY `nomemonitor` (`nomemonitor`),
  UNIQUE KEY `CPF` (`CPF`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `iduser_resp` (`iduser_resp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monitoria` (
  `idmonitoria` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoriavinc` int(7) unsigned zerofill DEFAULT NULL,
  `idfila` int(8) unsigned zerofill DEFAULT NULL,
  `tabfila` varchar(11) DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idoperador` int(7) unsigned zerofill DEFAULT NULL,
  `idsuper_oper` int(6) unsigned zerofill DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `datactt` date DEFAULT NULL,
  `horactt` time DEFAULT NULL,
  `horaini` time DEFAULT NULL,
  `horafim` time DEFAULT NULL,
  `inilig` time DEFAULT NULL,
  `tmpaudio` time DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `qtdefg` int(2) DEFAULT NULL,
  `qtdefgm` int(2) DEFAULT NULL,
  `idmonitoria_fluxo` int(9) unsigned zerofill DEFAULT NULL,
  `gravacao` text,
  `situacao` enum('A','E') DEFAULT 'A',
  `obs` text,
  `checkfg` varchar(10) DEFAULT NULL,
  `tmonitoria` enum('I','E') DEFAULT 'I',
  `alertas` VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (`idmonitoria`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idmonitoriavinc` (`idmonitoriavinc`),
  KEY `idoperador` (`idoperador`),
  KEY `idsuper_oper` (`idsuper_oper`),
  KEY `idmonitor` (`idmonitor`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idmonitoria_fluxo` (`idmonitoria_fluxo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monitoriaadm` (
  `idmonitoria` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoriavinc` int(7) unsigned zerofill DEFAULT NULL,
  `idfila` int(8) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idoperador` int(7) unsigned zerofill DEFAULT NULL,
  `idsuper_oper` int(6) unsigned zerofill DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `horaini` time DEFAULT NULL,
  `horafim` time DEFAULT NULL,
  `tmpaudio` time DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `qtdefg` int(2) DEFAULT NULL,
  `qtdefgm` int(2) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `gravacao` text,
  PRIMARY KEY (`idmonitoria`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idmonitoriavinc` (`idmonitoriavinc`),
  KEY `idoperador` (`idoperador`),
  KEY `idsuper_oper` (`idsuper_oper`),
  KEY `idmonitor` (`idmonitor`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monitoriacalib` (
  `idmonitoriacalib` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idagcalibragem` int(3) unsigned zerofill DEFAULT NULL,
  `idmonitoriacalibvinc` int(7) unsigned zerofill DEFAULT NULL,
  `idfila` int(8) unsigned zerofill DEFAULT NULL,
  `tabfila` varchar(11) DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idoperador` int(7) unsigned zerofill DEFAULT NULL,
  `idsuper_oper` int(6) unsigned zerofill DEFAULT NULL,
  `iduser` varchar(15) DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `horaini` time DEFAULT NULL,
  `horafim` time DEFAULT NULL,
  `tmpaudio` time DEFAULT NULL,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `qtdefg` int(2) DEFAULT NULL,
  `qtdefgm` int(2) DEFAULT NULL,
  `type` varchar(30) DEFAULT NULL,
  `gravacao` text,
  `situacao` enum('A','E') DEFAULT 'A',
  `obs` TEXT NOT NULL,
  PRIMARY KEY (`idmonitoriacalib`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idagcalibragem` (`idagcalibragem`),
  KEY `idmonitoriacalibvinc` (`idmonitoriacalibvinc`),
  KEY `idoperador` (`idoperador`),
  KEY `idsuper_oper` (`idsuper_oper`),
  KEY `idmonitor` (`idmonitor`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`monitoria_fluxo` (
  `idmonitoria_fluxo` int(9) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitoria` int(7) unsigned zerofill DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  `idrel_fluxo` int(4) unsigned zerofill DEFAULT NULL,
  `idstatus` int(4) unsigned zerofill DEFAULT NULL,
  `iddefinicao` int(4) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `obs` text,
  PRIMARY KEY (`idmonitoria_fluxo`),
  KEY `idmonitoria` (`idmonitoria`),
  KEY `iduser` (`iduser`),
  KEY `idstatus` (`idstatus`),
  KEY `iddefinicao` (`iddefinicao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`moni_login` (
  `idmoni_login` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `tempo` time DEFAULT NULL,
  `login` time DEFAULT NULL,
  `logout` time DEFAULT NULL,
  PRIMARY KEY (`idmoni_login`),
  KEY `idmonitor` (`idmonitor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`moni_pausa` (
  `idmoni_pausa` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `idmotivo` int(4) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `horaini` time DEFAULT NULL,
  `horafim` time DEFAULT NULL,
  `tempo` time DEFAULT NULL,
  `lib_super` enum('S','N') DEFAULT 'N',
  `iduser_adm` int(4) unsigned zerofill DEFAULT NULL,
  `obs` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idmoni_pausa`),
  KEY `iduser_adm` (`iduser_adm`),
  KEY `idmotivo` (`idmotivo`),
  KEY `idmonitor` (`idmonitor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`motivo` (
  `idmotivo` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomemotivo` varchar(50) DEFAULT NULL,
  `idtipo_motivo` int(2) unsigned zerofill DEFAULT NULL,
  `acesso` enum('E','I') DEFAULT 'E',
  `qtde` varchar(2) NULL DEFAULT NULL,
  `tempo` varchar(7) NULL DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idmotivo`),
  KEY `idtipo_motivo` (`idtipo_motivo`),
  KEY `nomemotivo` (`nomemotivo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`operador` (
  `idoperador` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `operador` varchar(255) DEFAULT NULL,
  `cpfoper` VARCHAR(11) DEFAULT NULL,
  `idsuper_oper` int(6) DEFAULT NULL,
  `dtbase` date DEFAULT NULL,
  `horabase` time DEFAULT NULL,
  `senha` VARCHAR(41),
  `altsenha` INT(1) DEFAULT '0',
  PRIMARY KEY (`idoperador`),
  UNIQUE KEY `operador` (`operador`),
  KEY `idsuper_oper` (`idsuper_oper`),
  KEY `cpfoper` (`cpfoper`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`param_book` (
  `idparam_book` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `camarq` varchar(200) DEFAULT NULL,
  `tipoarq` varchar(30) DEFAULT NULL,
  `tamanho` int(9) DEFAULT NULL,
  `dias` int(2) DEFAULT NULL,
  PRIMARY KEY (`idparam_book`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`param_moni` (
  `idparam_moni` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `filtro_dataimp` varchar(10) DEFAULT NULL,
  `filtro_ultmoni` varchar(10) DEFAULT NULL,
  `filtro_datafimimp` varchar(10) DEFAULT NULL,
  `filtro_dultimp` varchar(10) DEFAULT NULL,
  `filtro_limmulti` varchar(10) DEFAULT NULL,
  `ordemctt` varchar(10) NULL DEFAULT 'N,C',
  `idindice` int(2) unsigned zerofill DEFAULT NULL,
  `tipomoni` enum('G','O') DEFAULT NULL,
  `checkfg` enum('S','N') DEFAULT NULL,
  `tipoimp` enum('LA','L','A','SL','I') DEFAULT NULL,
  `semdados` enum('N','S') NULL DEFAULT 'N',
  `tpaudio` varchar(100) DEFAULT NULL,
  `tpcompress` varchar(100) DEFAULT NULL,
  `tparquivo` varchar(100) DEFAULT NULL,
  `tiposervidor` varchar(10) DEFAULT NULL,
  `endereco` varchar(30) DEFAULT NULL,
  `loginftp` varchar(20) DEFAULT NULL,
  `senhaftp` varchar(200) DEFAULT NULL,
  `camaudio` varchar(300) DEFAULT NULL,
  `ccampos` varchar(1) DEFAULT NULL,
  `cpartes` varchar(2) DEFAULT NULL,
  `conf` enum('S','N') DEFAULT NULL,
  `hpa` time DEFAULT NULL,
  `hmonitor` time DEFAULT NULL,
  PRIMARY KEY (`idparam_moni`),
  KEY `idindice` (`idindice`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`perfil_adm` (
  `idperfil_adm` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomeperfil_adm` varchar(100) DEFAULT NULL,
  `tabelas` varchar(1000) DEFAULT NULL,
  `filtro_arvore` enum('S','N') DEFAULT 'S',
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idperfil_adm`),
  UNIQUE KEY `nomeperfil_adm` (`nomeperfil_adm`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`perfil_web` (
  `idperfil_web` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomeperfil_web` varchar(100) DEFAULT NULL,
  `nivel` enum('1','2','3','4','5','6','7','8','9','10') DEFAULT NULL,
  `areas` varchar(800) DEFAULT NULL,
  `filtro_arvore` enum('S','N') DEFAULT 'S',
  `visuext` ENUM( 'N', 'S' ) NULL DEFAULT 'N',
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idperfil_web`),
  UNIQUE KEY `nomeperfil_web` (`nomeperfil_web`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`pergunta` (
  `idpergunta` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `descripergunta` varchar(1000) DEFAULT NULL,
  `complepergunta` varchar(1000) DEFAULT NULL,
  `valor_perg` decimal(10,2) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `tipo_resp` enum('M','U') DEFAULT 'U',
  `idresposta` int(6) unsigned zerofill DEFAULT NULL,
  `padrao` enum('S','N') DEFAULT 'N',
  `descriresposta` varchar(1000) DEFAULT NULL,
  `compleresposta` varchar(1000) DEFAULT NULL,
  `modcalc` enum ('ABS','PERC') NULL DEFAULT NULL,
  `avalia` enum('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','AVALIACAO_FG','FG','FGM','PERC','AVALIACAO_OK') DEFAULT NULL,
  `avaliaplan` varchar(4) DEFAULT NULL,
  `idrel_origem` varchar(6) DEFAULT NULL,
  `idrel_avaliaplan` varchar(6) DEFAULT NULL,
  `valor_resp` decimal(10,2) DEFAULT NULL,
  `percperda` decimal(10,2) DEFAULT NULL,
  `maxrep` INT (3) DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  `ativo_resp` enum('S','N') DEFAULT 'S',
  KEY `idpergunta` (`idpergunta`),
  KEY `idresposta` (`idresposta`),
  KEY `avaliaplan` (`avaliaplan`),
  KEY `idrel_avaliaplan` (`idrel_avaliaplan`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`perguntatab` (
  `idperguntatab` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `descriperguntatab` varchar(200) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idperguntatab`),
  KEY `descriperguntatab` (`descriperguntatab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`periodo` (
  `idperiodo` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `mes` int(2) DEFAULT NULL,
  `nmes` varchar(20) DEFAULT NULL,
  `ano` varchar(4) DEFAULT NULL,
  `dataini` date DEFAULT NULL,
  `datafim` date DEFAULT NULL,
  `dtinictt` date DEFAULT NULL,
  `dtfimctt` date DEFAULT NULL,
  `dias` int(2) unsigned zerofill DEFAULT NULL,
  `diasprod` int(2) unsigned zerofill DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idperiodo`),
  KEY `iduser` (`iduser`),
  KEY `nmes` (`nmes`),
  KEY `ano` (`ano`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`planilha` (
  `idplanilha` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `descriplanilha` varchar(100) DEFAULT NULL,
  `compleplanilha` varchar(300) DEFAULT NULL,
  `tabplanilha` enum('S','N') DEFAULT 'N',
  `ativoweb` enum('S','N') DEFAULT 'S',
  `ativomoni` enum('S','N') DEFAULT 'S',
  `idtabulacao` varchar(20) DEFAULT NULL,
  `idgrupo` int(6) unsigned zerofill DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `idaval_plan` int(4) unsigned zerofill DEFAULT NULL,
  `tab` enum('S','N') DEFAULT 'N',
  KEY `idplanilha` (`idplanilha`),
  KEY `descriplanilha` (`descriplanilha`),
  KEY `idgrupo` (`idgrupo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`regmonitoria` (
  `idregmonitoria` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idfila` int(8) unsigned zerofill DEFAULT NULL,
  `idmotivo` int(4) unsigned zerofill DEFAULT NULL,
  `idoperador` int(7) unsigned zerofill DEFAULT NULL,
  `idsuper_oper` int(6) unsigned zerofill DEFAULT NULL,
  `idmonitor` int(4) unsigned zerofill DEFAULT NULL,
  `data` date DEFAULT NULL,
  `horaini` time DEFAULT NULL,
  `horafim` time DEFAULT NULL,
  `tmpaudio` time DEFAULT NULL,
  `gravacao` text,
  PRIMARY KEY (`idregmonitoria`),
  KEY `idmonitor` (`idmonitor`),
  KEY `idmotivo` (`idmotivo`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idoperador` (`idoperador`),
  KEY `idsuper_oper` (`idsuper_oper`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relemailfg` (
  `idrelemailfg` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idconfemailenv` int(2) unsigned zerofill DEFAULT NULL,
  `user_adm` varchar(1000) DEFAULT NULL,
  `user_web` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idrelemailfg`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `idconfemailenv` (`idconfemailenv`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relemailfluxo` (
  `idrelemailfluxo` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `idrel_fluxo` int(2) unsigned zerofill DEFAULT NULL,
  `idfluxo` int(2) unsigned zerofill DEFAULT NULL,
  `idstatus` int(2) unsigned zerofill DEFAULT NULL,
  `iddefinicao` int(2) unsigned zerofill DEFAULT NULL,
  `idconfemailenv` int(2) unsigned zerofill DEFAULT NULL,
  `usersadm` varchar(1000) DEFAULT NULL,
  `usersweb` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`idrelemailfluxo`),
  KEY `idrel_fluxo` (`idrel_fluxo`),
  KEY `idfluxo` (`idfluxo`),
  KEY `iddefinicao` (`iddefinicao`),
  KEY `idstatus` (`idstatus`),
  KEY `idconfemailenv` (`idconfemailenv`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relemailimp` (
  `idrelemailimp` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idconfemailenv` int(2) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `usersadm` varchar(200) DEFAULT NULL,
  `usersweb` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idrelemailimp`),
  KEY `idconfemailenv` (`idconfemailenv`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relmoni` (
  `idrelmoni` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomerelmoni` varchar(50) DEFAULT NULL,
  `qtdecol` int(1) unsigned zerofill DEFAULT NULL,
  `filtros` varchar(50) DEFAULT NULL,
  `idsperfiladm` varchar(50) DEFAULT NULL,
  `idsperfilweb` varchar(50) DEFAULT NULL,
  `vincrel` int(2) unsigned zerofill DEFAULT NULL,
  `tabpri` varchar(30) DEFAULT NULL,
  `agrupa` varchar(30) DEFAULT NULL,
  `tabsec` enum('monitoria','regmonitoria','moni_pausa') DEFAULT NULL,
  `tabterc` varchar(200) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idrelmoni`),
  KEY `nomerelmoni` (`nomerelmoni`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relmonicol` (
  `idrelmonicol` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrelmoni` int(2) unsigned zerofill DEFAULT NULL,
  `tabela` varchar(50) DEFAULT NULL,
  `coluna` varchar(50) DEFAULT NULL,
  `nomeapresenta` varchar(100) DEFAULT NULL,
  `tipo` enum('DATA','OUTROS','QUANTIDADE','PERCENTUAL') DEFAULT 'OUTROS',
  `serie` int(1) DEFAULT NULL,
  `grafico` enum('bar','column','pie','line','spline','bar,spline') DEFAULT NULL,
  `cor` varchar(6) DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  `ordena` enum('S','N') DEFAULT 'N',
  `formula` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idrelmonicol`),
  KEY `idrelmoni` (`idrelmoni`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relresult` (
  `idrelresult` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomerelresult` varchar(100) DEFAULT NULL,
  `filtros` varchar(100) DEFAULT NULL,
  `descricao` varchar(500) DEFAULT NULL,
  `idsperfadm` varchar(100) DEFAULT NULL,
  `idsperfweb` varchar(100) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idrelresult`),
  KEY `INDEX` (`nomerelresult`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relresultcab` (
  `idrelresultcab` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrelresult` int(2) unsigned zerofill DEFAULT NULL,
  `qtdelogos` int(1) DEFAULT NULL,
  `caminhologo` varchar(1000) DEFAULT NULL,
  `posilogo` varchar(20) DEFAULT NULL,
  `width` varchar(4) DEFAULT NULL,
  `height` varchar(4) DEFAULT NULL,
  `campos` varchar(150) DEFAULT NULL,
  `posicampos` enum('C','E','D') DEFAULT NULL,
  PRIMARY KEY (`idrelresultcab`),
  KEY `idrelresult` (`idrelresult`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relresultcampos` (
  `idrelresultcampos` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrelresult` int(2) unsigned zerofill NOT NULL,
  `campo` enum('P','F','QTMO','QTOP','QTMOP','QTSUPER','MO','QTFG','QTFGOP','QTFGI','QTFGIOP','QTFGM','QTFGMOP','QTFGMI','QTFGMIOP','O') DEFAULT NULL,
  `idformula` int(2) unsigned zerofill DEFAULT NULL,
  `descricampo` varchar(100) DEFAULT NULL,
  `valor` varchar(100) DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  PRIMARY KEY (`idrelresultcampos`),
  KEY `idrelresult` (`idrelresult`),
  KEY `campo` (`campo`),
  KEY `idformula` (`idformula`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`relresultcorpo` (
  `idrelresultcorpo` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idrelresult` int(2) unsigned zerofill NOT NULL,
  `tipodados` enum('CON','H','C') DEFAULT NULL,
  `frequenciadados` enum('G','D','S','M','A') NOT NULL,
  `dados` enum('F','MO','MS','P','PI','PG','A','IT','TAB') DEFAULT NULL,
  `tabulacao` int(2) unsigned zerofill DEFAULT NULL,
  `idformula` int(2) unsigned zerofill DEFAULT NULL,
  `agrupadados` enum('MO','MS','F') DEFAULT NULL,
  `apresdados` enum('TABELA','GRAFICO') DEFAULT NULL,
  `tipografico` varchar(100) DEFAULT NULL,
  `corfdgraf` varchar(6) DEFAULT NULL,
  `cordados` varchar(6) DEFAULT NULL,
  `valores` enum('S','N') DEFAULT 'S',
  `rotacaoval` int(3) DEFAULT NULL,
  `margem` varchar(5) DEFAULT NULL,
  `marginbottom` int(3) DEFAULT NULL,
  `widthgraf` varchar(5) DEFAULT NULL,
  `heightgraf` varchar(5) DEFAULT NULL,
  `textrotacao` varchar(3) DEFAULT NULL,
  `pointwidth` varchar(3) DEFAULT NULL,
  `legenda` enum('true','false') DEFAULT NULL,
  `posileg` varchar(10) DEFAULT NULL,
  `orientaleg` varchar(15) DEFAULT NULL,
  `posicaoX` varchar(5) DEFAULT NULL,
  `posicaoY` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idrelresultcorpo`),
  KEY `index` (`idrelresult`),
  KEY `idformula` (`idformula`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`rel_aval` (
  `idrel_aval` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idaval_plan` int(4) unsigned zerofill DEFAULT NULL,
  `valor` decimal(10,2) DEFAULT NULL,
  `posicao` int(1) DEFAULT NULL,
  PRIMARY KEY (`idrel_aval`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idaval_plan` (`idaval_plan`),
  KEY `posicao` (`posicao`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`rel_filtros` (
  `idrel_filtros` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`idrel_filtros`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`rel_fluxo` (
  `idrel_fluxo` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idfluxo` int(4) unsigned zerofill DEFAULT NULL,
  `idstatus` int(4) unsigned zerofill DEFAULT NULL,
  `iddefinicao` int(4) unsigned zerofill DEFAULT NULL,
  `idatustatus` int(4) unsigned zerofill DEFAULT NULL,
  `tipo` enum('inicia','contesta','editar','cancela','finaliza','senhaoper','sequencia') DEFAULT NULL,
  `classifica` enum('replica','treplica','erro','feedback') DEFAULT NULL,
  `vis_adm` text,
  `vis_web` text,
  `resp_adm` text,
  `resp_web` text,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idrel_fluxo`),
  KEY `idfluxo` (`idfluxo`),
  KEY `idstatus` (`idstatus`),
  KEY `iddefinicao` (`iddefinicao`),
  KEY `idatustatus` (`idatustatus`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`respostatab` (
  `idrespostatab` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `descrirespostatab` varchar(200) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idrespostatab`),
  KEY `descrirespostatab` (`descrirespostatab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`selecao` (
  `idselecao` int(8) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idupselecao` int(6) unsigned zerofill NOT NULL,
  `idrel_filtros` int(6) unsigned zerofill NOT NULL,
  `operador` varchar(500) NULL DEFAULT NULL,
  `datactt` date DEFAULT NULL,
  `horactt` time DEFAULT NULL,
  `nomearquivo` varchar(200) DEFAULT NULL,
  `selecionado` int(1) NOT NULL DEFAULT '0',
  `reposicao` int(1) NOT NULL DEFAULT '0',
  `enviado` int(1) NOT NULL DEFAULT '0',
  `tpselecao` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`idselecao`),
  KEY `idupselecao` (`idupselecao`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `datactt` (`datactt`),
  KEY `nomearquivo` (`nomearquivo`(70))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1/

CREATE TABLE IF NOT EXISTS `$db`.`status` (
  `idstatus` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomestatus` varchar(50) DEFAULT NULL,
  `fase` int(2) DEFAULT NULL,
  `dias` int(2) DEFAULT NULL,
  `iddefiniauto` varchar(9) DEFAULT NULL,
  `idatualizapara` int(4) DEFAULT NULL,
  `idfluxo` int(4) unsigned zerofill DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idstatus`),
  KEY `nomestatus` (`nomestatus`),
  KEY `idfluxo` (`idfluxo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`subebook` (
  `idsubebook` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomesubebook` varchar(100) DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idsubebook`),
  UNIQUE KEY `nomesubebook` (`nomesubebook`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`subgrupo` (
  `idsubgrupo` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `descrisubgrupo` varchar(100) DEFAULT NULL,
  `complesubgrupo` varchar(300) DEFAULT NULL,
  `valor_sub` decimal(10,2) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `idpergunta` int(6) unsigned zerofill DEFAULT NULL,
  `posicao` int(2) DEFAULT NULL,
  `tab` enum('S','N') DEFAULT 'N',
  KEY `id` (`idsubgrupo`),
  KEY `descrisubgrupo` (`descrisubgrupo`),
  KEY `idpergunta` (`idpergunta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`super_oper` (
  `idsuper_oper` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `super_oper` varchar(255) NOT NULL,
  `cpfsuper` varchar(11) NULL DEFAULT NULL,
  `dtbase` date NOT NULL,
  `horabase` time NOT NULL,
  PRIMARY KEY (`idsuper_oper`),
  UNIQUE KEY `super_oper` (`super_oper`),
  KEY `cpfsuper` (`cpfsuper`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`tabulacao` (
  `idreltabulacao` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idtabulacao` int(2) unsigned zerofill NOT NULL,
  `nometabulacao` varchar(100) NOT NULL,
  `idperguntatab` int(4) unsigned zerofill DEFAULT NULL,
  `idrespostatab` int(4) unsigned zerofill DEFAULT NULL,
  `idproxpergunta` int(4) unsigned zerofill DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `ativorel` enum('S','N') DEFAULT 'S',
  `obriga` enum('S','N') DEFAULT 'S',
  `posicao` int(2) DEFAULT NULL,
  PRIMARY KEY (`idreltabulacao`),
  KEY `idperguntatab` (`idperguntatab`),
  KEY `idrespostatab` (`idrespostatab`),
  KEY `idtabulacao` (`idtabulacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`telefone` (
  `idtelefone` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `ddd` int(2) DEFAULT NULL,
  `telefone` int(8) DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `tentativa` int(2) DEFAULT NULL,
  `obs` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`idtelefone`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`tipo_motivo` (
  `idtipo_motivo` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nometipo_motivo` varchar(50) DEFAULT NULL,
  `vincula` enum('regmonitoria','moni_pausa','monitoria','automatico') DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT NULL,
  PRIMARY KEY (`idtipo_motivo`),
  KEY `nometipo_motivo` (`nometipo_motivo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`upload` (
  `idupload` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `tabfila` varchar(20) DEFAULT NULL,
  `opcaoimp` varchar(1) DEFAULT 'R',
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  `tabuser` varchar(10) DEFAULT NULL,
  `camupload` varchar(300) DEFAULT NULL,
  `idperiodo` int(2) DEFAULT NULL,
  `semana` int(1) DEFAULT NULL,
  `importacao` int(1) DEFAULT NULL,
  `dataini` date DEFAULT NULL,
  `datafim` date DEFAULT NULL,
  `dataup` date DEFAULT NULL,
  `horaup` time DEFAULT NULL,
  `imp` enum('S','N') DEFAULT 'N',
  `dataimp` date DEFAULT NULL,
  `horaimp` time DEFAULT NULL,
  `qtdeimp` int(5) DEFAULT NULL,
  `qtdeerro` int(5) DEFAULT NULL,
  PRIMARY KEY (`idupload`),
  KEY `idrel_filtros` (`idrel_filtros`),
  KEY `iduser` (`iduser`),
  KEY `camupload` (`camupload`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`upselecao` (
  `idupselecao` int(6) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `qtdeenv` INT( 7 ) NOT NULL DEFAULT '0',
  `descarte` INT( 7 ) NOT NULL DEFAULT '0',
  `qtdesel` INT( 7 ) NOT NULL DEFAULT '0',
  `qtdeefetiva` INT( 7 ) NOT NULL DEFAULT '0',
  `qtdeexced` INT(7) NOT NULL DEFAULT '0',
  `qtdeselexced` INT(7) NOT NULL DEFAULT '0',
  `reposicao` int(7) NOT NULL DEFAULT '0',
  `caminhoarq` varchar(500) DEFAULT NULL,
  `nomearquivo` varchar(500) DEFAULT NULL,
  `nomesistema` varchar(500) DEFAULT NULL,
  `tab_user` varchar(10) DEFAULT NULL,
  `iduser` int(4) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`idupselecao`),
  KEY `iduser` (`iduser`),
  KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1/

CREATE TABLE IF NOT EXISTS `$db`.`useradmfiltro` (
  `iduseradmfiltro` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `iduser_adm` int(4) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`iduseradmfiltro`),
  KEY `iduser_adm` (`iduser_adm`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`userwebfiltro` (
  `iduserwebfiltro` int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `iduser_web` int(4) unsigned zerofill DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`iduserwebfiltro`),
  KEY `iduser_web` (`iduser_web`),
  KEY `idrel_filtros` (`idrel_filtros`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`user_adm` (
  `iduser_adm` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomeuser_adm` varchar(200) DEFAULT NULL,
  `apelido` varchar(200) DEFAULT NULL,
  `CPF` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `idperfil_adm` int(2) unsigned zerofill DEFAULT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `senha` varchar(41) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `senha_ini` enum('S','N') DEFAULT 'S',
  `ass` enum('S','N') DEFAULT 'N',
  `import` enum('S','N') DEFAULT 'N',
  `selecaoaudios` enum('N','S') DEFAULT 'N',
  `calibragem` enum('S','N') DEFAULT 'N',
  `confapres` varchar(100) DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `dataalt` date DEFAULT NULL,
  `horaalt` time DEFAULT NULL,
  PRIMARY KEY (`iduser_adm`),
  UNIQUE KEY `nomeuser_adm` (`nomeuser_adm`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `CPF` (`CPF`),
  KEY `idperfil_adm` (`idperfil_adm`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`user_web` (
  `iduser_web` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nomeuser_web` varchar(200) DEFAULT NULL,
  `CPF` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `idperfil_web` int(2) unsigned zerofill DEFAULT NULL,
  `usuario` varchar(20) DEFAULT NULL,
  `senha` varchar(41) DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  `senha_ini` enum('S','N') DEFAULT 'S',
  `ass` enum('S','N') DEFAULT 'N',
  `import` enum('S','N') DEFAULT 'N',
  `selecaoaudios` enum('N','S') DEFAULT 'N',
  `calibragem` enum('S','N') DEFAULT 'N',
  `confapres` varchar(100) DEFAULT NULL,
  `datacad` date DEFAULT NULL,
  `dataalt` date DEFAULT NULL,
  `horaalt` time DEFAULT NULL,
  PRIMARY KEY (`iduser_web`),
  UNIQUE KEY `nomeuser_web` (`nomeuser_web`),
  UNIQUE KEY `CPF` (`CPF`),
  UNIQUE KEY `usuario` (`usuario`),
  KEY `idperfil_web` (`idperfil_web`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`vinc_plan` (
  `idvinc_plan` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `planvinc` varchar(100) DEFAULT NULL,
  `idrel_filtros` int(6) unsigned zerofill DEFAULT NULL,
  `ativo` enum('S','N') DEFAULT 'S',
  PRIMARY KEY (`idvinc_plan`),
  KEY `planvinc` (`planvinc`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idvinc_plan` (`idvinc_plan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`rel_resp_especifica` (
  `idrel_resp_especifica` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) DEFAULT NULL,
  `qtde` int(2) NULL DEFAULT '1',
  `idrel_filtros` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idrel_resp_especifica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE TABLE IF NOT EXISTS `$db`.`resp_especificas` (
  `id` int(3) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `idplanilha` int(4) unsigned zerofill DEFAULT NULL,
  `idresposta` int(4) unsigned zerofill DEFAULT NULL,
  `idrel_resp_especifica` int(2) unsigned zerofill DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idplanilha` (`idplanilha`),
  KEY `idresposta` (`idresposta`),
  KEY `idrel_resp_especifica` (`idrel_resp_especifica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8/

CREATE INDEX data ON atualiza_status (data)/
CREATE INDEX dataini ON agcalibragem (dataini)/
CREATE INDEX datafim ON agcalibragem (datafim)/
CREATE INDEX datactt ON fila_grava (datactt)/
CREATE INDEX horactt ON fila_grava (horactt)/
CREATE INDEX data ON histresp (data)/
CREATE INDEX dataini ON interperiodo (dataini)/
CREATE INDEX datafim ON interperiodo (datafim)/
CREATE INDEX data ON login_adm (data)/
CREATE INDEX data ON login_web (data)/
CREATE INDEX data ON monitoria (data)/
CREATE INDEX horaini ON monitoria (horaini)/
CREATE INDEX datactt ON monitoria (datactt)/
CREATE INDEX horactt ON monitoria (horactt)/
CREATE INDEX data ON monitoria_fluxo (data)/
CREATE INDEX hora ON monitoria_fluxo (hora)/
CREATE INDEX data ON moni_login (data)/
CREATE INDEX data ON moni_pausa (data)/
CREATE INDEX horaini ON moni_pausa (horaini)/
CREATE INDEX horafim ON moni_pausa (horafim)/
CREATE INDEX data ON regmonitoria (data)/
CREATE INDEX horaini ON regmonitoria (horaini)/
CREATE INDEX horactt ON selecao (horactt)/
CREATE INDEX dataimp ON upload (dataimp)/
CREATE INDEX horaimp ON upload (horaimp)/

ALTER TABLE `agcalibragem` ADD CONSTRAINT `agcalibragem_ibfk_1` FOREIGN KEY (`idmonitoria`) REFERENCES `monitoria` (`idmonitoria`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `agcalibragem` ADD CONSTRAINT `agcalibragem_ibfk_2` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `chat`
--
ALTER TABLE `chat` ADD CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`assunto`) REFERENCES `conf_chat` (`assunto`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `conf_calib`
--
ALTER TABLE `conf_calib` ADD CONSTRAINT `conf_calib_ibfk_1` FOREIGN KEY (`idconfemailenv`) REFERENCES `confemailenv` (`idconfemailenv`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `conf_mail`
--
ALTER TABLE `conf_mail` ADD CONSTRAINT `conf_mail_ibfk_1` FOREIGN KEY (`iduser_adm`) REFERENCES `user_adm` (`iduser_adm`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `conf_rel`
--
ALTER TABLE `conf_rel` ADD CONSTRAINT `conf_rel_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `corsistema`
--
ALTER TABLE `corsistema` ADD CONSTRAINT `corsistema_ibfk_1` FOREIGN KEY (`iddados_cli`) REFERENCES `dados_cli` (`iddados_cli`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `definicao`
--
ALTER TABLE `definicao` ADD CONSTRAINT `definicao_ibfk_1` FOREIGN KEY (`idfluxo`) REFERENCES `fluxo` (`idfluxo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `dimen_estim`
--
ALTER TABLE `dimen_estim` ADD CONSTRAINT `dimen_estim_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `dimen_estim` ADD CONSTRAINT `dimen_estim_ibfk_2` FOREIGN KEY (`idperiodo`) REFERENCES `periodo` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `dimen_mod`
--
ALTER TABLE  `dimen_mod` ADD FOREIGN KEY (  `idperiodo` ) REFERENCES  `periodo` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE  `dimen_mod` ADD FOREIGN KEY (  `idrel_filtros` ) REFERENCES  `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
--
-- Restrições para a tabela `dimen_moni`
--
ALTER TABLE  `dimen_moni` ADD FOREIGN KEY (  `idmonitor` ) REFERENCES  `monitor` (`idmonitor`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE  `dimen_moni` ADD FOREIGN KEY (  `iddimen_mod` ) REFERENCES  `dimen_mod` (`iddimen_mod`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE  `dimen_moni` ADD FOREIGN KEY (  `idperiodo` ) REFERENCES  `periodo` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE/
--
-- Restrições para a tabela `ebook`
--
ALTER TABLE `ebook` ADD CONSTRAINT `ebook_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `cat_ebook` (`nomecat_ebook`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `ebook` ADD CONSTRAINT `ebook_ibfk_2` FOREIGN KEY (`subcategoria`) REFERENCES `subebook` (`nomesubebook`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `fila_grava`
--
ALTER TABLE `fila_grava` ADD CONSTRAINT `fila_grava_ibfk_1` FOREIGN KEY (`idoperador`) REFERENCES `operador` (`idoperador`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `fila_grava` ADD CONSTRAINT `fila_grava_ibfk_3` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `fila_grava` ADD CONSTRAINT `fila_grava_ibfk_4` FOREIGN KEY (`idupload`) REFERENCES `upload` (`idupload`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `fila_oper`
--
ALTER TABLE `fila_oper` ADD CONSTRAINT `fila_oper_ibfk_1` FOREIGN KEY (`idoperador`) REFERENCES `operador` (`idoperador`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `fila_oper` ADD CONSTRAINT `fila_oper_ibfk_2` FOREIGN KEY (`idsuper_oper`) REFERENCES `super_oper` (`idsuper_oper`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `fila_oper` ADD CONSTRAINT `fila_oper_ibfk_3` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `fila_oper` ADD CONSTRAINT `fila_oper_ibfk_4` FOREIGN KEY (`idupload`) REFERENCES `upload` (`idupload`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `filtro_dados`
--
ALTER TABLE `filtro_dados` ADD CONSTRAINT `filtro_dados_ibfk_1` FOREIGN KEY (`idfiltro_nomes`) REFERENCES `filtro_nomes` (`idfiltro_nomes`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restricoes para a tabela  `formula`
--
ALTER TABLE `formulas` ADD FOREIGN KEY (`idrelmoni`) REFERENCES `relmoni`(`idrelmoni`) ON DELETE RESTRICT ON UPDATE RESTRICT/

--
-- Restrições para a tabela `graficocorpo`
--
ALTER TABLE `graficocorpo` ADD CONSTRAINT `graficocorpo_ibfk_1` FOREIGN KEY (`idrelresultcorpo`) REFERENCES `relresultcorpo` (`idrelresultcorpo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `grafrelmoni`
--
ALTER TABLE `grafrelmoni` ADD CONSTRAINT `grafrelmoni_ibfk_1` FOREIGN KEY (`idrelmoni`) REFERENCES `relmoni` (`idrelmoni`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `histresp`
--
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_1` FOREIGN KEY (`idmonitoria`) REFERENCES `monitoria` (`idmonitoria`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_2` FOREIGN KEY (`idplanilha`) REFERENCES `planilha` (`idplanilha`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_3` FOREIGN KEY (`idaval_plan`) REFERENCES `aval_plan` (`idaval_plan`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_4` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_5` FOREIGN KEY (`idsubgrupo`) REFERENCES `subgrupo` (`idsubgrupo`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_6` FOREIGN KEY (`idpergunta`) REFERENCES `pergunta` (`idpergunta`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_7` FOREIGN KEY (`idresposta`) REFERENCES `pergunta` (`idresposta`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `histresp` ADD CONSTRAINT `histresp_ibfk_8` FOREIGN KEY (`idmonitoria_fluxo`) REFERENCES `monitoria_fluxo` (`idmonitoria_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `intervalo_notas`
--
ALTER TABLE `intervalo_notas` ADD CONSTRAINT `intervalo_notas_ibfk_1` FOREIGN KEY (`idindice`) REFERENCES `indice` (`idindice`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `itenscontest`
--
ALTER TABLE `itenscontest` ADD CONSTRAINT `itenscontest_ibfk_1` FOREIGN KEY (`idmonitoria`) REFERENCES `monitoria` (`idmonitoria`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `itenscontest` ADD CONSTRAINT `itenscontest_ibfk_2` FOREIGN KEY (`idmonitoria_fluxo`) REFERENCES `monitoria_fluxo` (`idmonitoria_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `login_adm`
--
ALTER TABLE `login_adm` ADD CONSTRAINT `login_adm_ibfk_1` FOREIGN KEY (`iduser_adm`) REFERENCES `user_adm` (`iduser_adm`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `login_web`
--
ALTER TABLE `login_web` ADD CONSTRAINT `login_web_ibfk_1` FOREIGN KEY (`iduser_web`) REFERENCES `user_web` (`iduser_web`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `moni_login`
--
ALTER TABLE `moni_login` ADD CONSTRAINT `moni_login_ibfk_1` FOREIGN KEY (`idmonitor`) REFERENCES `monitor` (`idmonitor`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `moni_pausa`
--
ALTER TABLE `moni_pausa` ADD CONSTRAINT `moni_pausa_ibfk_1` FOREIGN KEY (`idmonitor`) REFERENCES `monitor` (`idmonitor`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `moniavalia`
--
ALTER TABLE `moniavalia` ADD CONSTRAINT `moniavalia_ibfk_1` FOREIGN KEY (`idmonitoria`) REFERENCES `monitoria` (`idmonitoria`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `moniavalia` ADD CONSTRAINT `moniavalia_ibfk_3` FOREIGN KEY (`idaval_plan`) REFERENCES `aval_plan` (`idaval_plan`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `moniavalia` ADD CONSTRAINT `moniavalia_ibfk_6` FOREIGN KEY (`idpergunta`) REFERENCES `pergunta` (`idpergunta`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `moniavalia` ADD CONSTRAINT `moniavalia_ibfk_7` FOREIGN KEY (`idresposta`) REFERENCES `pergunta` (`idresposta`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `moniavaliacalib`
--
ALTER TABLE `moniavaliacalib` ADD CONSTRAINT `moniavaliacalib_ibfk_1` FOREIGN KEY (`idmonitoriacalib`) REFERENCES `monitoriacalib` (`idmonitoriacalib`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `monitabulacao`
--
ALTER TABLE `monitabulacao` ADD CONSTRAINT `monitabulacao_ibfk_1` FOREIGN KEY (`idmonitoria`) REFERENCES `monitoria` (`idmonitoria`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `monitabulacao` ADD CONSTRAINT `monitabulacao_ibfk_2` FOREIGN KEY (`idtabulacao`) REFERENCES `tabulacao` (`idtabulacao`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `monitabulacao` ADD CONSTRAINT `monitabulacao_ibfk_3` FOREIGN KEY (`idperguntatab`) REFERENCES `perguntatab` (`idperguntatab`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `monitabulacao` ADD CONSTRAINT `monitabulacao_ibfk_4` FOREIGN KEY (`idrespostatab`) REFERENCES `respostatab` (`idrespostatab`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `monitoria`
--
ALTER TABLE `monitoria` ADD CONSTRAINT `monitoria_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `monitoria` ADD CONSTRAINT `monitoria_ibfk_2` FOREIGN KEY (`idmonitor`) REFERENCES `monitor` (`idmonitor`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `monitoria_fluxo`
--
ALTER TABLE `monitoria_fluxo` ADD CONSTRAINT `monitoria_fluxo_ibfk_1` FOREIGN KEY (`idmonitoria`) REFERENCES `monitoria` (`idmonitoria`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `monitoria_fluxo` ADD CONSTRAINT `monitoria_fluxo_ibfk_2` FOREIGN KEY (`idstatus`) REFERENCES `status` (`idstatus`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `monitoria_fluxo` ADD CONSTRAINT `monitoria_fluxo_ibfk_3` FOREIGN KEY (`iddefinicao`) REFERENCES `definicao` (`iddefinicao`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `monitoriacalib`
--
ALTER TABLE `monitoriacalib` ADD CONSTRAINT `monitoriacalib_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `motivo`
--
ALTER TABLE `motivo` ADD CONSTRAINT `motivo_ibfk_1` FOREIGN KEY (`idtipo_motivo`) REFERENCES `tipo_motivo` (`idtipo_motivo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `param_moni`
--
ALTER TABLE `param_moni` ADD CONSTRAINT `param_moni_ibfk_1` FOREIGN KEY (`idindice`) REFERENCES `indice` (`idindice`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `regmonitoria`
--
ALTER TABLE `regmonitoria` ADD CONSTRAINT `regmonitoria_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `regmonitoria` ADD CONSTRAINT `regmonitoria_ibfk_2` FOREIGN KEY (`idoperador`) REFERENCES `operador` (`idoperador`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `regmonitoria` ADD CONSTRAINT `regmonitoria_ibfk_3` FOREIGN KEY (`idsuper_oper`) REFERENCES `super_oper` (`idsuper_oper`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `regmonitoria` ADD CONSTRAINT `regmonitoria_ibfk_4` FOREIGN KEY (`idmonitor`) REFERENCES `monitor` (`idmonitor`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `rel_aval`
--
ALTER TABLE `rel_aval` ADD CONSTRAINT `rel_aval_ibfk_1` FOREIGN KEY (`idaval_plan`) REFERENCES `aval_plan` (`idaval_plan`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `rel_fluxo`
--
ALTER TABLE `rel_fluxo` ADD CONSTRAINT `rel_fluxo_ibfk_1` FOREIGN KEY (`idfluxo`) REFERENCES `fluxo` (`idfluxo`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `rel_fluxo` ADD CONSTRAINT `rel_fluxo_ibfk_2` FOREIGN KEY (`idstatus`) REFERENCES `status` (`idstatus`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `rel_fluxo` ADD CONSTRAINT `rel_fluxo_ibfk_3` FOREIGN KEY (`iddefinicao`) REFERENCES `definicao` (`iddefinicao`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `rel_fluxo` ADD CONSTRAINT `rel_fluxo_ibfk_4` FOREIGN KEY (`idatustatus`) REFERENCES `status` (`idstatus`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relemailfg`
--
ALTER TABLE `relemailfg` ADD CONSTRAINT `relemailfg_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailfg` ADD CONSTRAINT `relemailfg_ibfk_2` FOREIGN KEY (`idconfemailenv`) REFERENCES `confemailenv` (`idconfemailenv`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relemailfluxo`
--
ALTER TABLE `relemailfluxo` ADD CONSTRAINT `relemailfluxo_ibfk_1` FOREIGN KEY (`idrel_fluxo`) REFERENCES `rel_fluxo` (`idrel_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailfluxo` ADD CONSTRAINT `relemailfluxo_ibfk_2` FOREIGN KEY (`idfluxo`) REFERENCES `fluxo` (`idfluxo`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailfluxo` ADD CONSTRAINT `relemailfluxo_ibfk_3` FOREIGN KEY (`iddefinicao`) REFERENCES `definicao` (`iddefinicao`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailfluxo` ADD CONSTRAINT `relemailfluxo_ibfk_4` FOREIGN KEY (`idstatus`) REFERENCES `status` (`idstatus`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailfluxo` ADD CONSTRAINT `relemailfluxo_ibfk_5` FOREIGN KEY (`idconfemailenv`) REFERENCES `confemailenv` (`idconfemailenv`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailfluxo` ADD CONSTRAINT `relemailfluxo_ibfk_6` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relemailimp`
--
ALTER TABLE `relemailimp` ADD CONSTRAINT `relemailimp_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `relemailimp` ADD CONSTRAINT `relemailimp_ibfk_2` FOREIGN KEY (`idconfemailenv`) REFERENCES `confemailenv` (`idconfemailenv`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relmonicol`
--
ALTER TABLE `relmonicol` ADD CONSTRAINT `relmonicol_ibfk_1` FOREIGN KEY (`idrelmoni`) REFERENCES `relmoni` (`idrelmoni`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relresultcab`
--
ALTER TABLE `relresultcab` ADD CONSTRAINT `relresultcab_ibfk_1` FOREIGN KEY (`idrelresult`) REFERENCES `relresult` (`idrelresult`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relresultcampos`
--
ALTER TABLE `relresultcampos` ADD CONSTRAINT `relresultcampos_ibfk_1` FOREIGN KEY (`idrelresult`) REFERENCES `relresult` (`idrelresult`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `relresultcorpo`
--
ALTER TABLE `relresultcorpo` ADD CONSTRAINT `relresultcorpo_ibfk_1` FOREIGN KEY (`idrelresult`) REFERENCES `relresult` (`idrelresult`) ON DELETE CASCADE ON UPDATE CASCADE/
--
-- Restrições para a tabela `status`
--
ALTER TABLE `status` ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`idfluxo`) REFERENCES `fluxo` (`idfluxo`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `telefone`
--
ALTER TABLE `telefone` ADD CONSTRAINT `telefone_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `upload`
--
ALTER TABLE `upload` ADD CONSTRAINT `upload_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `user_adm`
--
ALTER TABLE `user_adm` ADD CONSTRAINT `user_adm_ibfk_1` FOREIGN KEY (`idperfil_adm`) REFERENCES `perfil_adm` (`idperfil_adm`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `user_web`
--
ALTER TABLE `user_web` ADD CONSTRAINT `user_web_ibfk_1` FOREIGN KEY (`idperfil_web`) REFERENCES `perfil_web` (`idperfil_web`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `useradmfiltro`
--
ALTER TABLE `useradmfiltro` ADD CONSTRAINT `useradmfiltro_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `useradmfiltro` ADD CONSTRAINT `useradmfiltro_ibfk_2` FOREIGN KEY (`iduser_adm`) REFERENCES `user_adm` (`iduser_adm`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `useradmfiltro` ADD CONSTRAINT `useradmfiltro_ibfk_3` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `userwebfiltro`
--
ALTER TABLE `userwebfiltro` ADD CONSTRAINT `userwebfiltro_ibfk_1` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `userwebfiltro` ADD CONSTRAINT `userwebfiltro_ibfk_2` FOREIGN KEY (`iduser_web`) REFERENCES `user_web` (`iduser_web`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE `userwebfiltro` ADD CONSTRAINT `userwebfiltro_ibfk_3` FOREIGN KEY (`idrel_filtros`) REFERENCES `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

--
-- Restrições para a tabela `vinc_plan`
--
ALTER TABLE `vinc_plan` ADD CONSTRAINT `vinc_plan_ibfk_1` FOREIGN KEY (`idplanilha`) REFERENCES `planilha` (`idplanilha`) ON DELETE CASCADE ON UPDATE CASCADE/

-- Restrições para tabela 'selecao'

ALTER TABLE  `selecao` ADD FOREIGN KEY (`idupselecao`) REFERENCES  `upselecao` (`idupselecao`) ON DELETE CASCADE ON UPDATE CASCADE/
ALTER TABLE  `selecao` ADD FOREIGN KEY (`idrel_filtros`) REFERENCES  `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/

ALTER TABLE  `configselecao` ADD FOREIGN KEY (  `idrel_filtros` ) REFERENCES  `rel_filtros` (`idrel_filtros`) ON DELETE CASCADE ON UPDATE CASCADE/