<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);

include_once('criadbmysql.php');
include_once('criadbmssql.php');

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
$path_banco = monitoria_;
$servidor = $_POST['server'];
$tpbanco = $_POST['tpserver'];
$cordb =
$db = $path_banco.strtolower(str_ireplace("monitoria_", "", str_replace(" ","_",$_POST['db'])));
$caracter = "[]$[><}{)(:;,!?*%&#@]";
$config = $rais."/monitoriateste/config/conexao.php";
if($_POST['server'] == "" || $_POST['user'] == "" || $_POST['senha'] == "" || $_POST['db'] == "" || $_POST['porta'] == "") {
  $msg = "Os campos obrigatórios para configurações do bando de dados devem estar preenchidos!!!";
  header("location:index.php?msg=".$msg);
  break;
}
if((eregi($caracter, $_POST['server'])) || (eregi($caracter, $_POST['user'])) || (eregi($caracter, $_POST['db'])) || (eregi($caracter, $_POST['porta']))) {
  $msg = "Os campos de configuracao possuem caracteres invalidos $caracter";
  header("location:index.php?msg=".$msg);
  break;
}
if(file_exists($config)) {
  $msg = "O arquivo ''conexao.php'' já existe.As configurações do servidor devem ser alteradas pelo modo administrador.";
  header("location:index.php?msg=".$msg);
}
else {
  $criacom = fopen('comandossql.php', 'w');
  if($criacom) {
    if($tpbanco == "mssql") {
      fwrite($criacom, "<?php\r\n");
      fwrite($criacom, "function comandossql() {\r\n");
      fwrite($criacom, '$conecta'." = mssql_connect;\r\n");
      fwrite($criacom, '$query'." = mssql_query;\r\n");
      fwrite($criacom, '$fetch_array'." = mssql_fetch_array;\r\n");
      fwrite($criacom, '$fetch_assoc'." = mssql_fetch_assoc;\r\n");
      fwrite($criacom, '$field'." = mssql_fetch_field;\r\n");
      fwrite($criacom, '$fetch_object'." = mssql_fetch_object;\r\n");
      fwrite($criacom, '$fetch_row'." = mssql_fetch_row;\r\n");
      fwrite($criacom, '$length'." = mssql_field_length;\r\n");
      fwrite($criacom, '$field_name'." = mssql_field_name;\r\n");
      fwrite($criacom, '$num_campos'." = mssql_num_fields;\r\n");
      fwrite($criacom, '$num_rows'." = mssql_num_rows;\r\n");
      fwrite($criacom, '$result'." = mssql_result;\r\n");
      fwrite($criacom, '$enccon'." = mssql_close;\r\n");
      fwrite($criacom, '$rows_affected'." = mssql_rows_affected;\r\n");
      fwrite($criacom, '$seldb'." = mssql_select_db;\r\n");
      fwrite($criacom, '$_SESSION['."'conecta'".'] = $conecta;'."\r\n");
      fwrite($criacom, '$_SESSION['."'query'".'] = $query;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_array'".'] = $fetch_array;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_assoc'".'] = $fetch_assoc;'."\r\n");
      fwrite($criacom, '$_SESSION['."'field'".'] = $field;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_object'".'] = $fetch_object;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_row'".'] = $fetch_row;'."\r\n");
      fwrite($criacom, '$_SESSION['."'length'".'] = $length;'."\r\n");
      fwrite($criacom, '$_SESSION['."'field_name'".'] = $field_name;'."\r\n");
      fwrite($criacom, '$_SESSION['."'num_campos'".'] = $num_campos;'."\r\n");
      fwrite($criacom, '$_SESSION['."'num_rows'".'] = $num_rows;'."\r\n");
      fwrite($criacom, '$_SESSION['."'result'".'] = $result;'."\r\n");
      fwrite($criacom, '$_SESSION['."'rows_affected'".'] = $rows_affected;'."\r\n");
      fwrite($criacom, '$_SESSION['."'seldb'".'] = $seldb;'."\r\n");
      fwrite($criacom, '$_SESSION['."'enccon'".'] = $enccon;'."\r\n");
      fwrite($criacom, "}\r\n");
      fwrite($criacom, "?>");
      fclose($criacom);
    }
    if($tpbanco == "mysql") {
      fwrite($criacom, "<?php\r\n");
      fwrite($criacom, "function comandossql() {\r\n");
      fwrite($criacom, '$conecta'." = mysql_connect;\r\n");
      fwrite($criacom, '$query'." = mysql_query;\r\n");
      fwrite($criacom, '$fetch_array'." = mysql_fetch_array;\r\n");
      fwrite($criacom, '$fetch_assoc'." = mysql_fetch_assoc;\r\n");
      fwrite($criacom, '$field'." = mysql_fetch_field;\r\n");
      fwrite($criacom, '$insert_id'." = mysql_insert_id;\r\n");
      fwrite($criacom, '$fetch_object'." = mysql_fetch_object;\r\n");
      fwrite($criacom, '$fetch_row'." = mysql_fetch_row;\r\n");
      fwrite($criacom, '$length'." = mysql_fetch_lengths;\r\n");
      fwrite($criacom, '$listdb'." = mysql_list_dbs;\r\n");
      fwrite($criacom, '$field_name'." = mysql_field_name;\r\n");
      fwrite($criacom, '$num_campos'." = mysql_num_fields;\r\n");
      fwrite($criacom, '$num_rows'." = mysql_num_rows;\r\n");
      fwrite($criacom, '$result'." = mysql_result;\r\n");
      fwrite($criacom, '$seldb'." = mysql_select_db;\r\n");
      fwrite($criacom, '$enccon'." = mysql_close;\r\n");
      fwrite($criacom, '$_SESSION['."'conecta'".'] = $conecta;'."\r\n");
      fwrite($criacom, '$_SESSION['."'query'".'] = $query;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_array'".'] = $fetch_array;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_assoc'".'] = $fetch_assoc;'."\r\n");
      fwrite($criacom, '$_SESSION['."'field'".'] = $field;'."\r\n");
      fwrite($criacom, '$_SESSION['."'insert_id'".'] = $insert_id;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_object'".'] = $fetch_object;'."\r\n");
      fwrite($criacom, '$_SESSION['."'fetch_row'".'] = $fetch_row;'."\r\n");
      fwrite($criacom, '$_SESSION['."'length'".'] = $length;'."\r\n");
      fwrite($criacom, '$_SESSION['."'field_name'".'] = $field_name;'."\r\n");
      fwrite($criacom, '$_SESSION['."'num_campos'".'] = $num_campos;'."\r\n");
      fwrite($criacom, '$_SESSION['."'num_rows'".'] = $num_rows;'."\r\n");
      fwrite($criacom, '$_SESSION['."'result'".'] = $result;'."\r\n");
      fwrite($criacom, '$_SESSION['."'listdb'".'] = $listdb;'."\r\n");
      fwrite($criacom, '$_SESSION['."'seldb'".'] = $seldb;'."\r\n");
      fwrite($criacom, '$_SESSION['."'enccon'".'] = $enccon;'."\r\n");
      fwrite($criacom, "}\r\n");
      fwrite($criacom, "?>");
      fclose($criacom);
    }
  }
  else {
    $msg = "Houve erro na criação do arquivo de comandos do banco de dados";
    header("location:index.php?msg=".$msg);
    break;
  }
  include_once($_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/config/comandossql.php");
  comandossql();
  $conexao = $_SESSION['conecta']($_POST['server'], $_POST['user'], $_POST['senha']) or die (mysql_error());
  if($conexao) {
    $cria = fopen('conexao.php', 'w');
    if($cria) {
      fwrite($cria, "<?php\r\n");
      if($_POST['porta'] == "") {
	fwrite($cria, '$server = base64_decode("'.base64_encode($_POST['server']).'")'.";\r\n");
      }
      else {
      	fwrite($cria, '$server = base64_decode("'.base64_encode($_POST['server'].":".$_POST['porta']).'")'.";\r\n");
      }
      fwrite($cria, '$user = base64_decode("'.base64_encode($_POST['user']).'")'.";\r\n");
      fwrite($cria, '$password = base64_decode("'.base64_encode($_POST['senha']).'")'.";\r\n");
      fwrite($cria, '$conectar = $_SESSION['."'conecta'".']($server, $user, $password) or die (mysql_error());'."\r\n");
      fwrite($cria, "?>");
      fclose($cria);
    }
    else {
      $msg = "Houve erro na criação do arquivo de configuração da conexao";
      header("location:index.php?msg=".$msg);
    }
     include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
       $data = date('Y-m-d');
       if($tpbanco == "mysql") {
           $arqsql = $rais."/monitoria_supervisao/config/scriptcriadbmysql.sql";
       }
       if($tpbanco == "mssql") {
           $arqsql = $rais."/monitoria_supervisao/config/scriptcriadbmssql.sql";
       }
       if($tpbanco == "oracle") {
           $arqsql = $rais."/monitoria_supervisao/config/scriptcriadboracle.sql";
       }
       $abresql = fopen($arqsql,"r+");
       $readarqsql = fread($abresql, filesize($arqsql));
       $querys = explode("/",$readarqsql);
       $criadb = 1;
       foreach($querys as $q) {
           if($q == "") {

           }
           else {
               $sqlquerys[] = $q;
           }
       }
       foreach($sqlquerys as $query) {
           if($criadb == 0) {
               $msg = "Erro no processo de criação do banco de dados, favor contatar o Administrador";
               header("location:index.php?msg=".$msg);
           }
           else {
               $query = str_replace('$db', $db, $query);
               $query = trim($query);
               if(eregi("CREATE DATABASE",$query)) {
                   $criatabs = $_SESSION['query']($query) or die ("erro na criação da tabela $query");
                   $seldb = $_SESSION['seldb']($db);
               }
               else {
                   $criatabs = $_SESSION['query']($query) or die (mysql_error());
               }
               if($criatabs == true) {
                   $criadb = 1;
               }
               else {
                   $criadb = 0;
               }
           }
       }
       //$criadb = $_SESSION['query'](fopen("/monitoria_supervisao/config/scriptcriadbmysql.sql","r"));
       if($criadb == 1) {
           $new = new TabelasSql();
           $new->ColunasTabelas();
           $tmp = mkdir($rais."/monitoria_supervisao/tmp/".strtoupper($_POST['nome']));
           $tabelas = $new->colunastabelas;
           $use = array('operador','super_oper');
           $caracter = "!@#$%&*()+=:;<>{}[]|\/";
           foreach($use as $tabuse) {
                   $detoper = "SHOW COLUMNS FROM $tabuse";
                   $edetoper = $_SESSION['query']($detoper) or die ("erro na query de consulta dos detalhes das colunas");
                   while($ldetoper = $_SESSION['fetch_array']($edetoper)) {
                       if($ldetoper['Key'] == "PRI" OR $ldetoper['Key'] == "UNI") {
                           $key = "S";
                       }
                       else {
                           if($ldetoper['Field'] == "operador" OR $ldetoper['Field'] == "super_oper") {
                               $key = "S";
                           }
                           else {
                               $key = "N";
                           }
                       }
                       $ptipo = explode("(",$ldetoper['Type']);
                       $tipo = strtoupper($ptipo[0]);
                       if($ptipo[1] == "") {
                           $tam = "'"."'";
                       }
                       else {
                           $ptam = explode(")",$ptipo[1]);
                           $tam = $ptam[0];
                       }
                       $col = $ldetoper['Field'];
                       $intocoloper[$tabuse][$col] = "'".strtoupper($tabuse)."','$col','$key','$tipo',$tam,'$caracter'";
                   }
           }
           $selbd = $_SESSION['seldb']($db);
           foreach($intocoloper as $tab) {
               foreach($tab as $c => $d) {
                   $colunas = "INSERT INTO coluna_oper (incorpora,nomecoluna,unico,tipo,tamanho,caracter) VALUES ($d)";
                   $ecolunas = $_SESSION['query']($colunas) or die ("erro na query de inserção dos dados na tabela coluna_oper");
               }
           }
           $colfila = array('DATE' => 'datactt','TIME' => 'horactt');
           foreach($colfila as $kcf => $cf) {
               $colunas = "INSERT INTO coluna_oper (incorpora,nomecoluna,unico,tipo,tamanho,caracter) VALUES ('DADOS','$cf','N','$kcf','','')";
               $ecolunas = $_SESSION['query']($colunas) or die ("erro na query de inserção dos dados na tabela coluna_oper");
           }
           $data = date('Y-m-d');
           $hora = date('H:i:s');
           $supernulo = "INSERT INTO super_oper (super_oper,dtbase,horabase) VALUES ('NAO INFORMADO','$data','$hora')";
           $esuper = $_SESSION['query']($supernulo) or die ("erro na query de inserção do supervisor nulo");
           $idsuper = $_SESSION['insert_id']();
           $opernulo = "INSERT INTO operador (operador,idsuper_oper,dtbase,horabase) VALUES ('NAO LOCALIZADO','$idsuper','$data','$hora')";
           $eoper = $_SESSION['query']($opernulo) or die ("erro na query de inserção do operador nulo");
           $tipo = "INSERT INTO tipo_motivo (nometipo_motivo,vincula,ativo) VALUES ('MONITORAR','moni_pausa','S')";
           $etipo = $_SESSION['query']($tipo) or die ("erro na query de inserÃƒÂ§ÃƒÂ£o do valor tipo_motivo");
           $create = "INSERT INTO motivo (nomemotivo, idtipo_motivo, ativo) VALUES ('INICIAR', '01', 'S')";
           $ecreate = $_SESSION['query']($create) or die ("erro na query de criação do motivo");
           //$cad_tab = $_SESSION['query']($tab_formulas) or die ("erro na query de criaÃƒÂ§ÃƒÂ£o da tabela ''formulas''");
           $paginas = "CLIENTES,SELECAO,FILTROS,ESTRUTURA,CONFIG. ENVIO E-MAIL,CALENDARIO,PARAMETROS MONITORIA,PERFIL ADM,USUARIO ADM,PERFIL WEB,USUARIO WEB,MONITOR,ESTRUTURA FLUXO,FLUXO,RELACIONAMENTO ESTRUTURA,MOTIVO,DIMENSIONAMENTO,MONITORIAS,TABULACAO,AVALIACOES PLANILHA,PERGUNTA/RESPOSTA TABULA,FORMULAS,PERGUNTAS,SUB-GRUPO,GRUPO,PLANILHA,INDICE,CALIBRAGEM,CADASTRO E-BOOK,ORIENTACOES,RELATORIOS MONITORIA,RELATORIOS RESULTADOS,OPERADOR,LISTA OPERADOR,SUPERVISOR,FILA MONITORIA,FECHAMENTO,EXPORTACAO,LOG,RESPOSTAS ESPECIFICAS,EXPORTACAO CALIB,EXPORTACAO IMP,ALERTAS,IMPRODUTIVAS";
           $perfiladm = "INSERT INTO `$db`.`perfil_adm` (
                         `nomeperfil_adm` , `tabelas`, `ativo`) VALUES ('ADMINISTRADOR','$paginas','S');";
           $cadperfil = $_SESSION['query']($perfiladm) or die (mysql_error());
           $senhaadmin = md5(admin);
           $useradmin = "INSERT INTO `$db`.`user_adm` (`iduser_adm` ,`nomeuser_adm` ,`CPF` ,`email` ,`idperfil_adm` ,`usuario` ,`senha` ,`ativo` ,`senha_ini`, `datacad`)
                           VALUES ('0001', 'ADMIN', '00000000000', 'admin@admin.com.br', '01', 'admin', '$senhaadmin', 'S', 'S', '$data');";
           $caduser = $_SESSION['query']($useradmin) or die (mysql_error());
           $nome = str_replace(" ","_",$_POST['db']);
           $imagem = 'Logo_Flor_vector_75X35.png';
           $cadcli = "INSERT INTO dados_cli (nomedados_cli, site, imagem, assdigital, textoass) VALUES ('$nome', 'www.inicial.com.br', '$imagem', 'N', '')";
           $ecadcli = $_SESSION['query']($cadcli) or die ("erro na query de inserção dos dados do cliente");
           $cores = array('corfd_pag' => 'CCCCCC', 'corfd_ntab' => '6699CC', 'corfd_coltexto' => '999999', 'corfd_colcampos' => 'FFFFFF');
           foreach($cores as $class => $cor) {
               if($class == 'corfd_pag') {
                   $objeto = "div";
               }
               else {
                   $objeto = "td";
               }
               $cadcor = "INSERT INTO corsistema (iddados_cli, objeto, class, cor, tipo) VALUES ('1','$objeto', '$class', '$cor', 'SISTEMA')";
               $ecadcor = $_SESSION['query']($cadcor) or die ("erro na que de inserção das cores do sistema");
           }
           if($caduser) {
               $reindex = rename($rais."/monitoria_supervisao/config/index.php", $rais."/monitoria_supervisao/config/indexmod.php");
               $delindex = unlink($rais."/monitoria_supervisao/config/index.php");
               $index = fopen('index.php','w');
               fwrite($index, "<?php\r\n");
               fwrite($index, "header('location:/monitoria_supervisao/index.php?msg=Configuração já executada, favor entrar com login e senha!!!');\r\n");
               fwrite($index, "break;\r\n");
               fwrite($index, "?>");
               fclose($index);
               $config = true;
               $msg = "Cadastro do novo banco de dados e tabelas iniciais efetuados com sucesso!!!";
               header("location:/monitoria_supervisao/index.php?msg=".$msg);
           }
           else {
               $seldb = $_SESSION['seldb']($_SESSION['selbanco']);
               $msg = "Erro para inserção dos dados do cliente na base, favor contatar o administrador";
               header("location:index.php?msg=".$msg);
           }
       }
       else {
           $seldb = $_SESSION['seldb']($_SESSION['selbanco']);
           $msg = "Erro no processo de acadstramento, favor verificar arquivo de criação da base!!!";
           header("location:index.php?msg=".$msg);
       }
  }
  else {
       $msg = "Erro nos dados de acesso ao servidor, favor verficar!!!";
       header("location:index.php?msg=".$msg);
  }
}
?>