<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CONFIGURAÇÃO INICIAL - SIGMA-Q</title>
<link href="/monitoria_supervisao/styleinicio.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.6.2.js"></script>
<link href="styleinicio.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
       $("#bconfig").click(function() {
           var tpserver = $("#tpserver").val();
           var server = $("#server").val();
           var user = $("#user").val();
           var senha = $("#senha").val();
           var porta = $("#porta").val();
           var db = $("#db").val();
           if(tpserver == "" || server == "" || user == "" || senha == "" || porta == "" || db == "") {
               alert("Todos os campos precisam estar preenchidos");               
               return false;
           }
       });
    });
</script>
</head>
<body>
    <div id="tudo">
            <div style="background-color: #FFF; border: 3px #3399ff solid;" id="config"><br />
                <div style="margin: 10px; text-align: center;"><span class="span" style="color:#000">Para iniciar o sofware de MONITORIA é necessário cadastrar os dados abaixo para configurar o acesso ao banco de dados.</span><p></div>
                <form action="criaconfig.php" method="POST">
                <table align="center" width="290" border="0">
                    <tr>
                        <td width="150" align="left"><span class="textos">Tipo Servidor</span></td>
                        <td width="197"><select name="tpserver" id="tpserver" class="">
                        <option value="mysql">MYSQL</option>
                        <option value="mssql">MSSQL</option>
                        <option value="oracle">POSTGRESQL</option>
                        <option value="oracle">ORACLE</option>
                        </select></td>
                    </tr>
                    <tr>
                        <td width="80" align="left"><span class="textos">Servidor</span></td>
                        <td width="197"><input class="input" name="server" id="server" type="text" /></td>
                        </tr>
                    <tr>
                        <td align="left"><span class="textos">Porta</span></td>
                        <td><input class="input" name="porta" id="porta" type="text" /></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="textos">User</span></td>
                        <td><input class="input" name="user" id="user" type="text" /></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="textos">Senha</span></td>
                        <td><input class="input" name="senha" id="senha" type="password" /></td>
                    </tr>
                    <tr>
                        <td align="left"><span class="textos">Novo Banco</span></td>
                        <td><input class="input" name="db" id="db" type="text" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" height="40px"><input class="button" name="config" id="bconfig" type="submit" value="Continuar Configuração" /></td>
                    </tr>
                </table>
                </form>
            <span class="span" style="margin: 10px;"><?php echo $_GET['msg'];?></span>
        </div>
    </div>
</div>
</body>
</html>