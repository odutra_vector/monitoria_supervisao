<?php
function criadbmysql ($db) {
  $data = date('Y-m-d');
  $banco = "CREATE DATABASE IF NOT EXISTS $db";
  $cad = $_SESSION['query']($banco) or die (mysql_error());
  $base = $_SESSION['seldb']($db);
  $tab_cli = "CREATE TABLE `$db`.`dados_cli` (
              `id` INT( 1 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
              `nome` VARCHAR( 50 ) NOT NULL ,
              `site` VARCHAR( 100 ) NOT NULL ,
              `imagem` VARCHAR( 100 ) NOT NULL ,
              `assdigital` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'S',
              `textoass` TEXT NOT NULL
              ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_cli) or die ("erro na query de criação da tabela ''dados_cli''");
  $tab_corsistema = "CREATE  TABLE `$db`.`corsistema` (
                      `idcorsistema` INT NOT NULL AUTO_INCREMENT ,
                      `objeto` VARCHAR(45) NOT NULL ,
                      `class` VARCHAR(45) NOT NULL ,
                      `cor` VARCHAR(45) NOT NULL ,
                      `tipo` ENUM ('PADRAO','PLANILHA','GRAFICOS') NOT NULL,
                      PRIMARY KEY (`idcorsistema`))
                      ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_corsistema) or die ("erro na query de criação da tabela ''corsistema''");
  $tab_user_adm = "CREATE TABLE $db.user_adm (
		  `id` INT( 4 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ,
		  `nome` VARCHAR( 100 ) NOT NULL ,
		  `CPF` VARCHAR( 11 ) NOT NULL ,
		  `email` VARCHAR( 100 ) NOT NULL ,
		  `perfil` INT( 2 ) UNSIGNED ZEROFILL NOT NULL ,
		  `usuario` VARCHAR( 20 ) NOT NULL ,
		  `senha` VARCHAR( 41 ) NOT NULL ,
		  `ativo` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'S',
		  `senha_ini` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'S',
                  `ass` ENUM ('S','N') NULL,
		  `import` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'N',
                  `calibragem` ENUM ('S','N') NOT NULL DEFAULT 'N',
                  `confapres` VARCHAR (100) NOT NULL,
		  `datacad` DATE NOT NULL ,
		  `dataalt` DATE NOT NULL ,
 		  `horaalt` TIME NOT NULL ,
		  PRIMARY KEY ( `id` ),
		  UNIQUE KEY `nome` (`nome`),
		  UNIQUE KEY `usuario` (`usuario`),
		  UNIQUE KEY `CPF` (`CPF`),
		  INDEX `perfil` (`perfil`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_user_adm) or die (mysql_error());
  $tab_user_web = "CREATE TABLE $db.user_web (
		  `id` INT( 4 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT ,
		  `nome` VARCHAR( 100 ) NOT NULL ,
		  `CPF` VARCHAR( 11 ) NOT NULL ,
		  `email` VARCHAR( 100 ) NOT NULL ,
		  `perfil` INT( 2 ) UNSIGNED ZEROFILL NOT NULL,
		  `usuario` VARCHAR( 20 ) NOT NULL ,
		  `senha` VARCHAR( 41 ) NOT NULL ,
		  `ativo` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'S',
		  `senha_ini` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'S',
                  `ass` ENUM ('S','N') NULL,
		  `import` ENUM( 'S', 'N' ) NOT NULL DEFAULT 'N',
                  `calibragem` ENUM ('S','N') NOT NULL DEFAULT 'N',
                  `confapres` VARCHAR (100) NOT NULL,
		  `datacad` DATE NOT NULL ,
		  `dataalt` DATE NOT NULL ,
 		  `horaalt` TIME NOT NULL ,
		  PRIMARY KEY ( `id` ),
		  UNIQUE KEY `nome` (`nome`),
		  UNIQUE KEY `CPF` (`CPF`),
		  UNIQUE KEY `usuario` (`usuario`),
		  INDEX `perfil` (`perfil`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_user_web) or die (mysql_error());
  $tab_perfil_adm = "CREATE TABLE `$db`.`perfil_adm` (
		  `id` INT(2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `nome` VARCHAR(20) NOT NULL,
		  `estrutura` ENUM ('E', 'C') NOT NULL,
		  `rel_estru` ENUM ('E', 'C') NOT NULL,
		  `user_adm` ENUM('E','C') NOT NULL,
		  `perfil_adm` ENUM('E','C') NOT NULL,
		  `user_web` ENUM('E','C') NOT NULL,
		  `perfil_web` ENUM('E','C') NOT NULL,
		  `filtros` ENUM('E','C') NOT NULL,
		  `monitor` ENUM('E','C') NOT NULL,
		  `definicao` ENUM('E','C') NOT NULL,
		  `status` ENUM('E','C') NOT NULL,
		  `estrufluxo` ENUM('E','C') NOT NULL,
		  `fluxo` ENUM('E','C') NOT NULL,
		  `motivo` ENUM('E','C') NOT NULL,
		  `dimen` ENUM('E','C') NOT NULL,
		  `aval_plan` ENUM('E','C') NOT NULL,
		  `resposta` ENUM('E','C') NOT NULL,
		  `pergunta` ENUM('E','C') NOT NULL,
		  `subgrupo` ENUM('E','C') NOT NULL,
		  `grupo` ENUM('E','C') NOT NULL,
		  `planilha` ENUM('E','C') NOT NULL,
		  `calibragem` ENUM('E','C') NOT NULL,
		  `upload` ENUM('E','C') NOT NULL,
		  `operador` ENUM('E','C') NOT NULL,
		  `super_oper` ENUM('E','C') NOT NULL,
		  `vendas` ENUM('E','C') NOT NULL,
		  `nivel` ENUM('01','02','03','04','05','06','07','08','09','10') NOT NULL,
		  `relatorios` ENUM('E','C') NOT NULL,
		  `monitorias` ENUM('E','C') NOT NULL,
		  `ebook` ENUM('E','C') NOT NULL,
		  `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		  PRIMARY KEY ( `id` ),
		  UNIQUE KEY `nome` (`nome`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_perfil_adm) or die (mysql_error());
  $tab_user_login = "CREATE TABLE `$db`.`user_login` (
		    `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `iduser_adm` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `nome_adm` VARCHAR (100) NOT NULL,
		    `ip` VARCHAR (15) NOT NULL,
		    `data` DATE NOT NULL,
		    `hora` TIME NOT NULL,
		    PRIMARY KEY ( `id` ),
		    INDEX `iduser` (`iduser_adm`),
		    INDEX `nomeuser` (`nome_adm`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_user_login) or die (mysql_error());
  $tab_useradmfiltro = "CREATE TABLE `$db`.`useradmfiltro` (
		       `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		       `iduser` INT (4) UNSIGNED ZEROFILL NOT NULL,
		       `idrelfiltro` INT (6) UNSIGNED ZEROFILL,
		       PRIMARY KEY (`id`),
		       INDEX `iduser` (`iduser`),
		       INDEX `idrelfiltro` (`idrelfiltro`)
		       ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_useradmfiltro) or die (mysql_error());
  $tab_userwebfiltro = "CREATE TABLE `$db`.`userwebfiltro` (
		       `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		       `iduser` INT (4) UNSIGNED ZEROFILL NOT NULL,
		       `idrelfiltro` INT (6) UNSIGNED ZEROFILL,
		       PRIMARY KEY (`id`),
		       INDEX `iduser` (`iduser`),
		       INDEX `idrelfiltro` (`idrelfiltro`)
		       ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_userwebfiltro) or die (mysql_error());
  $tab_perfil_web = "CREATE TABLE `$db`.`perfil_web` (
		    `id`INT (2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `nome` VARCHAR (20) NOT NULL,
		    `nivel` ENUM('1','2','3','4','5','6','7','8','9','10') NOT NULL,
                    `importacao` ENUM('V','NV') NOT NULL,
		    `relatorios` ENUM('V','NV') NOT NULL,
		    `monitorias` ENUM('V','NV') NOT NULL,
		    `calibragem` ENUM('V','NV') NOT NULL,
		    `fluxo` ENUM('V','NV') NOT NULL,
		    `ebook` ENUM('V','NV') NOT NULL,
		    `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		    PRIMARY KEY ( `id` ),
		    UNIQUE KEY `nome` (`nome`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_perfil_web) or die (mysql_error());
  $tab_user_login_web = "CREATE TABLE `$db`.`user_login_web` (
			`id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
			`iduser_web` INT (4) UNSIGNED ZEROFILL NOT NULL,
			`nome_web` VARCHAR (100) NOT NULL,
			`ip` VARCHAR (15) NOT NULL,
			`data` DATE NOT NULL,
			`hora` TIME NOT NULL,
			PRIMARY KEY ( `id` ),
			INDEX `iduser` (`iduser_web`),
			INDEX `nomeuser` (`nome_web`)
			) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cadtab = $_SESSION['query']($tab_user_login_web) or die (mysql_error());
  $tab_monitor = "CREATE TABLE `$db`.`monitor` (
		  `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `nome` VARCHAR (100) NOT NULL,
		  `CPF` VARCHAR (11) NOT NULL,
		  `resp` INT (4) UNSIGNED ZEROFILL,
		  `usuario` VARCHAR (20) NOT NULL,
		  `senha` VARCHAR (41) NOT NULL,
		  `entrada` TIME NOT NULL,
		  `saida` TIME NOT NULL,
		  `datacad` DATE NOT NULL,
		  `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		  `senha_ini` ENUM('S','N') NOT NULL DEFAULT 'S',
                  `ass` ENUM ('S','N') NULL,
		  `status` ENUM('MONITORANDO','PAUSA','GRAVACAO','AGUARDANDO','OFFLINE') NOT NULL,
                  `hstatus` TIME NOT NULL,
		  `meta_m` INT (4) NOT NULL,
		  `meta_s` INT (4) NOT NULL,
		  `meta_d` INT (4) NOT NULL,
		  `ebook` VARCHAR (100) NOT NULL,
                  `confapres` VARCHAR (100) NOT NULL,
		  PRIMARY KEY ( `id` ),
		  UNIQUE KEY `nome` (`nome`),
		  UNIQUE KEY `CPF` (`CPF`),
		  UNIQUE KEY `usuario` (`usuario`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_monitor) or die (mysql_error());
  $tab_moni_dimen = "CREATE TABLE `$db`.`moni_dimen` (
		    `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `id_moni` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `nome_moni` VARCHAR (100) NOT NULL,
		    `dataini` DATE NOT NULL,
		    `datafim` DATE NOT NULL,
		    `meta_m` INT (4) NOT NULL,
		    `real_m` INT (4) NOT NULL,
		    `meta_s` INT (4) NOT NULL,
		    `real_s` INT (4) NOT NULL,
		    `meta_d` INT (4) NOT NULL,
		    `real_d` INT (4) NOT NULL,
		    `t_producao` DATE NOT NULL,
		    `t_ocioso` DATE NOT NULL,
		    PRIMARY KEY ( `id` ),
		    INDEX `id_moni` (`id_moni`),
		    INDEX `idrelfiltro` (`idrelfiltro`),
		    INDEX `nome_moni` (`nome_moni`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_moni_dimen) or die (mysql_error());
  $tab_moni_pausa = "CREATE TABLE `$db`.`moni_pausa` (
		    `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `id_moni` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `nome_moni` VARCHAR (100) NOT NULL,
		    `idmotivo` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `nome_mot` VARCHAR (50)  NOT NULL,
		    `data`DATE NOT NULL,
		    `horaini` TIME NOT NULL,
		    `horafim` TIME NOT NULL,
		    `tempo` TIME NOT NULL,
		    `lib_super` ENUM ('S','N') DEFAULT 'S',
		    `idsuper` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    PRIMARY KEY ( `id` ),
		    INDEX `idsuper` (`idsuper`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_moni_pausa) or die (mysql_error());
  $tab_moni_login = "CREATE TABLE `$db`.`moni_login` (
		    `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `id_moni` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `nome_moni` VARCHAR (100) NOT NULL,
		    `ip` VARCHAR (15) NOT NULL,
		    `data` DATE NOT NULL,
		    `tempo` TIME NOT NULL,
		    `login` TIME NOT NULL,
		    `logout` TIME NOT NULL,
		    PRIMARY KEY ( `id` )
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_moni_login) or dir (mysql_error());
  $tab_tipo_motivo = "CREATE TABLE `$db`.`tipo_motivo` (
		      `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		      `nome` VARCHAR( 50 ) NOT NULL ,
		      `vincula` ENUM( 'MONITOR', 'MONITORIA' ) NOT NULL ,
		      `ativo` ENUM( 'S', 'N' ) NOT NULL
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_tipo_motivo) or die ("erro no cadastro da tabela ''tipo_motivo''");
  $create = "INSERT INTO tipo_motivo (nome, vincula, ativo) VALUES('MONITORAR','MONITOR','S')";
  $ecrerate = $_SESSION['query']($create) or die ("erro na query de inserção dos valores");
  $tab_motivo = "CREATE TABLE `$db`.`motivo` (
		    `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `nome` VARCHAR (50) NOT NULL,
		    `tipo` INT (2) UNSIGNED ZEROFILL NOT NULL,
		    `qtde` VARCHAR ( 2 ) NOT NULL ,
		    `tempo` VARCHAR ( 7 ) NULL ,
		    `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		    PRIMARY KEY ( `id` ),
		    INDEX `tipo` (`tipo`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_motivo) or die (mysql_error());
  $create = "INSERT INTO motivo (nome, tipo, ativo) VALUES ('INICIAR', '01', 'S')";
  $ecreate = $_SESSION['query']($create) or die ("erro na query de criação do motivo");
  $tab_status = "CREATE TABLE `$db`.`status` (
		`id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		`nome` VARCHAR (50) NOT NULL,
		`fase` INT(2) NOT NULL,
		`dias` INT(2) NOT NULL,
		`contadias` VARCHAR (9) NOT NULL,
		`ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		`idfluxo` INT (4) UNSIGNED ZEROFILL NOT NULL,
		PRIMARY KEY ( `id` ),
		INDEX `nome` (`nome`),
		INDEX `idfluxo` (`idfluxo`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_status) or die (mysql_error());
  $tab_definicao = "CREATE TABLE `$db`.`definicao` (
		    `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `nome` VARCHAR (50) NOT NULL,
		    `ativo` ENUM('S','N') NOT NULL DEFAULT 'N',
		    `idfluxo` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    PRIMARY KEY ( `id` ),
		    INDEX `nome` (`nome`),
		    INDEX `idfluxo` (`idfluxo`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_definicao) or die (mysql_error());
  $tab_fluxo = "CREATE TABLE `$db`.`fluxo` (
		`id_fluxo` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		`nome` VARCHAR (50) NOT NULL,
		`atvfluxo` ENUM('S','N') NOT NULL DEFAULT 'S',
		PRIMARY KEY ( `id_fluxo` ),
		UNIQUE KEY `nome` (`nome`),
		INDEX `id_fluxo` (`id_fluxo`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_fluxo) or die (mysql_error());
  $tab_relfluxo = "CREATE TABLE `$db`.`rel_fluxo` (
		`id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		`id_fluxo` INT(4) UNSIGNED ZEROFILL NOT NULL,
		`id_status` INT (4) UNSIGNED ZEROFILL NOT NULL,
		`id_defini` INT (4) UNSIGNED ZEROFILL NOT NULL,
		`id_atustatus` INT (4) UNSIGNED ZEROFILL NOT NULL,
		`tipo` ENUM('inicia','editar','cancela','finaliza','senhaoper','sequencia') NOT NULL,
		`vis_adm` TEXT,
		`vis_web` TEXT,
		`resp_adm` TEXT,
		`resp_web` TEXT,
		`ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		PRIMARY KEY ( `id` ),
		INDEX `id_fluxo` (`id_fluxo`),
		INDEX `id_status` (`id_status`),
		INDEX `id_defini` (`id_defini`),
		INDEX `id_atustatus` (`id_atustatus`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_relfluxo) or die (mysql_error());
  $tab_oper_fluxo = "CREATE TABLE `$db`.`oper_fluxo` (
		    `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `id_fluxo` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `nome_fluxo` VARCHAR (50) NOT NULL,
		    `id_filtro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `filtro` VARCHAR (100) NOT NULL,
		    `data` DATE NOT NULL,
		    `hora` TIME NOT NULL,
		    `resp_adm` VARCHAR (100) NOT NULL,
		    PRIMARY KEY ( `id` ),
		    INDEX `id_fluxo` (`id_fluxo`),
		    INDEX `idrelfiltro` (`idrelfiltro`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_oper_fluxo) or die (mysql_error());
  $tab_filtro_nomes = "CREATE TABLE `$db`.`filtro_nomes` (
		      `id` int (3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      `nome` VARCHAR (100) NOT NULL,
		      `nivel` INT (1) NOT NULL,
		      `trocafiltro` ENUM ('S','N') NOT NULL DEFAULT 'N',
		      `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		      PRIMARY KEY ( `id` ),
		      INDEX `nome` ( `nome` ),
		      INDEX `nivel` ( `nivel`)
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_filtro_nomes) or die (mysql_error());
  $tab_filtro_dados = "CREATE TABLE `$db`.`filtro_dados` (
		      `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      `nome` VARCHAR (100) NOT NULL,
		      `id_filtro` INT (3) UNSIGNED ZEROFILL NOT NULL,
		      `nivel` INT (1) NOT NULL,
		      `ativo` ENUM ('S','N') NOT NULL DEFAULT 'S',
		      PRIMARY KEY ( `id` ),
		      INDEX `id_filtro` (`id_filtro`),
		      INDEX `nivel` (`nivel`),
		      INDEX `nome` (`nome`)
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_filtro_dados) or die (mysql_error());
  $tab_rel_filtros = "CREATE TABLE `$db`.`rel_filtros` (
		      `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      PRIMARY KEY ( `id` )
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_rel_filtros) or die (mysql_error());
  $tab_colunas_oper = "CREATE TABLE `$db`.`coluna_oper` (
		      `id` INT (2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      `incorpora` ENUM ('SUPERVISOR','OPERADOR', 'DADOS') NOT NULL,
		      `ncolum` VARCHAR (50) NOT NULL,
		      `unico` ENUM ('S','N') NOT NULL,
		      `tipo` ENUM ('TEXTO', 'NUMERO', 'TIME', 'DATA'),
		      `tamanho` VARCHAR (6) NOT NULL,
		      `caracter` VARCHAR (100) NOT NULL,
		      PRIMARY KEY (`id`),
		      INDEX `ncolum` (`ncolum`)
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_colunas_oper) or die (mysql_error());
  $tab_parammoni = "CREATE TABLE `$db`.`param_moni` (
		      `id` INT (2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      `indicenota` INT (2) UNSIGNED ZEROFILL NOT NULL,
		      `tipomoni` ENUM ('G','O') NOT NULL,
		      `tipoimp` ENUM ('L','A','SL') NOT NULL,
		      `tpaudio` VARCHAR (100) NOT NULL,
		      `camaudio` VARCHAR(300) NOT NULL,
		      `ccampos` VARCHAR(1) NOT NULL,
		      `cpartes` VARCHAR(2) NOT NULL,
		      `conf`  ENUM ('S','N') NOT NULL,
		      `hpa` TIME NOT NULL,
		      `hmonitor` TIME NOT NULL,
		      PRIMARY KEY (`id`),
		      INDEX `indicenota` (`indicenota`)
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_parammoni) or die (mysql_error());
  $tab_camposparam = "CREATE TABLE `$db`.`camposparam` (
		     `id` INT (5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		     `idparam` INT (2) UNSIGNED ZEROFILL NOT NULL,
		     `idcolunaoper` INT (2) UNSIGNED ZEROFILL NOT NULL,
		     `posicao` INT(2) NOT NULL,
		     `classifica` ENUM ('S','N') DEFAULT 'N',
		     `tipoclass` ENUM ('NENHUM','DECRESCENTE','CRESCENTE') DEFAULT 'NENHUM',
		     `visumoni` ENUM ('S','N') DEFAULT 'S',
		     `ativo` ENUM ('S','N') DEFAULT 'S',
		     PRIMARY KEY (`id`),
		     INDEX `idparam` (`idparam`),
		     INDEX `idcolunaoper` (`idcolunaoper`),
		     INDEX `posicao` (`posicao`)
		     ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_camposparam) or die (mysql_error());
  $tab_config_rel = "CREATE TABLE `$db`.`config_rel` (
		    `idconfig` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `param_moni` INT (2) UNSIGNED ZEROFILL NOT NULL,
		    `idfluxo` INT (4) UNSIGNED ZEROFILL NOT NULL,
                    `confcalib` INT (1) UNSIGNED ZEROFILL NOT NULL,
		    `planmoni` VARCHAR (100) NOT NULL,
		    `planweb` VARCHAR (100) NOT NULL,
		    `tipoacesso` ENUM ('L','V', 'R') NOT NULL,
		    `trocanome` ENUM ('S','N') NOT NULL,
		    `trocafiltro` ENUM ('S','N') NOT NULL,
		    `vdn` VARCHAR (100) NOT NULL,
                    `ativo` ENUM ('S','N') NOT NULL DEFAULT 'S',
		    PRIMARY KEY (`idconfig`),
		    UNIQUE `idrelfiltro` (`idrelfiltro`),
		    INDEX `param_moni` (`param_moni`),
		    INDEX `planmoni` (`planmoni`),
		    INDEX `planweb` (`planweb`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_config_rel) or die (mysql_error());
  $tab_ebook = "CREATE TABLE `$db`.`ebook` (
		`id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		`arquivo` TEXT NOT NULL,
		`titulo` VARCHAR (200) NOT NULL,
		`descri` VARCHAR (1000) NOT NULL,
		`procedimento` VARCHAR (1000) NOT NULL,
		`datareceb` DATE NOT NULL,
		`usuarioenv` VARCHAR (100) NOT NULL,
		`datacad` DATE NOT NULL,
		`horacad` TIME NOT NULL,
		`dataalt` DATE NOT NULL,
		`horaalt` TIME NOT NULL,
		`tporienta` ENUM ('TEXTO', 'ARQUIVO') NOT NULL,
		`tipo` VARCHAR (30) NOT NULL,
		`categoria` VARCHAR (50) NOT NULL,
		`subcategoria` VARCHAR (100) NOT NULL,
		`ativo`ENUM('S','N') NOT NULL DEFAULT 'S',
		`id_caduser` INT (4) UNSIGNED ZEROFILL NOT NULL,
		`nome_usercad` VARCHAR (100) NOT NULL,
		PRIMARY KEY ( `id` ),
		INDEX `categoria` (`categoria`),
		INDEX `subcategoria` (`subcategoria`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_ebook) or die (mysql_error());
  $tab_parambook = "CREATE TABLE `$db`.`param_book` (
		   `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL ,
		   `camarq` VARCHAR( 200 ) NOT NULL ,
		   `tipoarq` VARCHAR( 30 ) NOT NULL ,
		   `tamanho` INT( 9 ) NOT NULL ,
		   `dias` INT( 2 ) NOT NULL ,
		   PRIMARY KEY ( `id` )
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_parambook) or die (mysql_error());
  $tab_cat_ebook = "CREATE TABLE `$db`.`cat_ebook` (
		   `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `nome` VARCHAR (50) NOT NULL,
		   `id_usercad` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   `nome_usercad` VARCHAR (100) NOT NULL,
		   `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		   PRIMARY KEY ( `id` ),
		   UNIQUE KEY `nome` (`nome`)
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_cat_ebook) or die (mysql_error());
  $tab_sub_ebook = "CREATE TABLE `$db`.`sub_ebook`(
		   `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `nome` VARCHAR (100) NOT NULL,
		   `id_usercad` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   `nome_usercad` VARCHAR (100) NOT NULL,
		   `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		   PRIMARY KEY ( `id` ),
		   UNIQUE KEY `nome` (`nome`)
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_sub_ebook) or die (mysql_error());
  $tab_dados_monitoria = "CREATE TABLE `$db`.`dados_monitoria` (
			 `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
			 `nbase` VARCHAR (50) NOT NULL,
			 `hpa` TIME NOT NULL,
			 `hmonitor` TIME NOT NULL,
			 PRIMARY KEY ( `id` ),
			 UNIQUE KEY ( `nbase` )
			 ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_dados_monitoria) or die ("erro no processo de criação da tabela ''dados_monitoria''");
  $tab_dimen_estim = "CREATE TABLE `$db`.`dimen_estim` (
		     `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		     `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		     `qnt_grava` INT (6) NOT NULL,
		     `qnt_oper` INT (6) NOT NULL,
		     `multiplicador` INT (2) NOT NULL,
		     `estimativa` INT (6) NOT NULL,
		     `tma` TIME NOT NULL,
		     `tta` VARCHAR (10) NOT NULL,
		     `tm` TIME NOT NULL,
		     `ttm` VARCHAR (10) NOT NULL,
		     `periodo` INT (2) UNSIGNED ZEROFILL NOT NULL,
		     `divperiodo` INT (1) NOT NULL,
		     `qnt_pad` INT (4) NOT NULL,
		     `qnt_monitores` DECIMAL (10,2) NOT NULL,
		     `hs_monitor` TIME NOT NULL,
		     `meta_monitor` INT (4) NOT NULL,
		     `percpartpa` INT (3) NOT NULL,
		     `percpartmeta` INT (3) NOT NULL,
		     `datacad` DATE NOT NULL,
		     `horacad` TIME NOT NULL,
		     `resp` INT (4) UNSIGNED ZEROFILL NOT NULL,
		     PRIMARY KEY ( `id` ),
		     INDEX `idrelfiltro` (`idrelfiltro`),
		     INDEX `periodo` (`periodo`),
		     INDEX `resp` (`resp`)
		     ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_dimen_estim) or die ("erro no processo de criação da tabela ''dimin_estim''");
  $tab_dimen_moni = "CREATE TABLE `$db`.`dimen_moni` (
		    `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `idmeta` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `idmoni` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `nome_moni` VARCHAR (100) NOT NULL,
		    `periodo` INT (2) NOT NULL,
		    `meta_m` INT (4) NOT NULL,
		    `meta_s` INT (4) NOT NULL,
		    `meta_d` INT (4) NOT NULL,
		    `ativo` ENUM('S','N') NOT NULL DEFAULT 'S',
		    PRIMARY KEY ( `id` ),
		    INDEX `idmoni` (`idmoni`),
		    INDEX `idmeta` (`idmeta`),
		    INDEX `idrelfiltro` (`idrelfiltro`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_dimen_moni) or die ("erro no processo de criação da tabela ''dimen_moni''");
  $tab_pergunta = "CREATE TABLE `$db`.`pergunta` (
		  `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `descri` VARCHAR (200) NOT NULL,
		  `comple` VARCHAR (300) NOT NULL,
		  `valor_perg` DECIMAL (10,2),
		  `ativo` ENUM ('S','N') DEFAULT 'S' NOT NULL,
		  `tipo_resp` ENUM('M','U') DEFAULT 'U' NOT NULL,
		  `id_resp` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `descri_resp` VARCHAR (100) NOT NULL,
		  `comple_resp` VARCHAR (300) NOT NULL,
		  `avalia` ENUM('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','FG', 'FGM') NOT NULL,
		  `valor_resp` DECIMAL (10,2),
		  `posicao` INT (2) NOT NULL,
		  `ativo_resp` ENUM('S','N') DEFAULT 'S' NOT NULL,
		  INDEX `id` ( `id` ),
		  INDEX `id_resp` (`id_resp`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_pergunta) or die ("erro no processo de criação da tabela ''pergunta''");
  $tab_dimen_mod = "CREATE TABLE `$db`.`dimen_mod` (
		  `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `qnt_grava` INT (6) NOT NULL,
		  `qnt_oper` INT (6) NOT NULL,
		  `multiplicador` INT (2) NOT NULL,
		  `estimativa` INT (6) NOT NULL,
		  `tma` TIME NOT NULL,
		  `tta` VARCHAR (10) NOT NULL,
		  `tm` TIME NOT NULL,
		  `ttm` VARCHAR (10) NOT NULL,
		  `periodo` INT (2) UNSIGNED ZEROFILL NOT NULL,
		  `divperiodo` INT (1) NOT NULL,
		  `qnt_pad` DECIMAL (10,2) NOT NULL,
		  `qnt_monitores` DECIMAL (10,2) NOT NULL,
		  `hs_monitor` TIME NOT NULL,
		  `meta_monitor` INT (4) NOT NULL,
		  `percpartpa` INT (3) NOT NULL,
		  `percpartmeta` INT (3) NOT NULL,
		  `datacad` DATE NOT NULL,
		  `horacad` TIME NOT NULL,
		  `resp` INT (4) UNSIGNED ZEROFILL NOT NULL,
		  PRIMARY KEY ( `id` ),
		  INDEX `idrelfiltro` (`idrelfiltro`),
		  INDEX `periodo` (`periodo`),
		  INDEX `resp` (`resp`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_dimen_mod) or die ("erro no processo de criação da tabela ''dimen_mod''");
  $tab_subgrupo = "CREATE TABLE `$db`.`subgrupo` (
		  `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `descri` VARCHAR (100) NOT NULL,
		  `comple` VARCHAR (300) NOT NULL,
		  `valor_sub` DECIMAL (10,2),
		  `ativo` ENUM('S','N') DEFAULT 'S',
		  `id_perg` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `posicao` INT (2) NOT NULL,
		  `tab` ENUM('S','N') DEFAULT 'N',
		  INDEX `id` ( `id` ),
		  INDEX `id_perg` (`id_perg`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_subgrupo) or die ("erro no processo de criação da tabela ''subgrupo''");
  $tab_grupo = 	"CREATE TABLE `$db`.`grupo` (
		`id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		`descri` VARCHAR (100) NOT NULL,
		`comple` VARCHAR (300) NOT NULL,
		`valor_grup` DECIMAL (10,2),
		`ativo` ENUM('S','N') DEFAULT 'S',
		`filtro_vinc` ENUM('S','P') NOT NULL,
		`id_rel` INT (6) UNSIGNED ZEROFILL NOT NULL,
		`posicao` INT (2) NOT NULL,
		`tab` ENUM('S','N') DEFAULT 'S',
		INDEX `id` ( `id` ),
		INDEX `id_rel` (`id_rel`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_grupo) or die ("erro no processo de criação da tabela ''grupo''");
  $tab_planilha = "CREATE TABLE `$db`.`planilha` (
		  `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `descri` VARCHAR (100) NOT NULL,
		  `comple` VARCHAR (300) NOT NULL,
		  `tab_plan` ENUM('S','N') DEFAULT 'N',
		  `ativo_web` ENUM('S','N') DEFAULT 'S',
		  `ativo_moni` ENUM('S','N') DEFAULT 'S',
		  `id_grup` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `posicao` INT (2) NOT NULL,
		  `ativo` ENUM ('S','N') NOT NULL DEFAULT 'S',
		  `idaval` INT(4) UNSIGNED ZEROFILL NOT NULL,
		  `tab` ENUM('S','N') DEFAULT 'N',
		  INDEX `id` ( `id` ),
		  INDEX `id_grup` (`id_grup`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_planilha) or die ("erro no processo de criação da tabela ''planilha''");
  $tab_vincplan = "CREATE TABLE `$db`.`vinc_plan` (
		   `id` INT (3) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `idplan` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   `planvinc` VARCHAR (100) NOT NULL,
                   `idrelfiltro` INT (6) UNSIGNED ZEROFILL NULL,
	           `ativo` ENUM ('S','N') NOT NULL DEFAULT 'S',
                   INDEX `planvinc` (`planvinc`),
		   INDEX `idplan` (`idplan`),
		   INDEX `id` (`id`)
		   ) ENGINE = InnDB DEFAULT CHARSET=utf8;";
  $cad_vincplan = $_SESSION['query']($tab_vincplan) or die ("erro na query de criação da tabela ''vinc_plan''");
  $tab_aval_plan = "CREATE TABLE `$db`.`aval_plan` (
		   `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `nome` VARCHAR(50) NOT NULL,
		   `nivel` ENUM('1','2','3','4','5','6','7','8','9','10'),
		   PRIMARY KEY ( `id` )
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_aval_plan = $_SESSION['query']($tab_aval_plan) or die ("erro no processo de criação da tabela ''aval_plan''");
  $tab_rel_aval = "CREATE TABLE `$db`.`rel_aval` (
		`id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		`id_plan` INT (4) UNSIGNED ZEROFILL NOT NULL,
		`id_aval` INT (4) UNSIGNED ZEROFILL NOT NULL,
		`valor` DECIMAL (10,2),
		PRIMARY KEY (`id`),
		INDEX `id_plan` (`id_plan`),
		INDEX `id_aval` (`id_aval`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_rel_aval = $_SESSION['query']($tab_rel_aval) or die ("erro no processo de criação da tabela ''config_mail''");
  $tab_monitoria = "CREATE TABLE `$db`.`monitoria` (
		   `idmoni` INT (7) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
           `idmonivinc` INT (7) UNSIGNED ZEROFILL NOT NULL,
		   `idfila` INT (8) UNSIGNED ZEROFILL NOT NULL,
		   `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		   `idoper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		   `idsuper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		   `monitor` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   `data` DATE NOT NULL,
		   `horaini` TIME NOT NULL,
		   `horafim` TIME NOT NULL,
		   `tmpaudio` TIME NOT NULL,
		   `id_plan` INT (4) UNSIGNED ZEROFILL NOT NULL,
           `qtdefg` INT (2) NOT NULL,
           `qtdefgm` INT (2) NOT NULL,
		   `type` VARCHAR (30) NOT NULL,
		   `gravacao` TEXT NOT NULL,
		   PRIMARY KEY ( `idmoni` ),
		   INDEX `id_plan` (`id_plan`),
           INDEX `idmonivinc` (`idmonivinc`),
		   INDEX `idoper` (`idoper`),
		   INDEX `idsuper` (`idsuper`),
		   INDEX `monitor` (`monitor`),
		   INDEX `idrelfiltro` (`idrelfiltro`)
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_monitoria) or die ("erro no processo de criação da tabela ''monitoria''");
  $tab_monitoria_fluxo = "CREATE TABLE `$db`.`monitoria_fluxo` (
			  `id` INT( 9 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
			  `idmonitoria` INT( 7 ) UNSIGNED ZEROFILL NOT NULL ,
			  `tipouser` ENUM( 'monitor', 'user_web', 'user_adm' ) NOT NULL ,
			  `iduser` INT( 4 ) UNSIGNED ZEROFILL NOT NULL ,
			  `idstatus` INT( 4 ) UNSIGNED ZEROFILL NOT NULL ,
			  `iddefinicao` INT( 4 ) UNSIGNED ZEROFILL NOT NULL ,
			  `data` DATE NOT NULL ,
			  `hora` TIME NOT NULL ,
			  `obs` TEXT NOT NULL,
                          PRIMARY KEY ( `id` ),
			  INDEX `idmonitoria` (`idmonitoria`),
			  INDEX `iduser` (`iduser`),
			  INDEX `idstatus` (`idstatus`),
			  INDEX `iddefinicao` (`iddefinicao`)
			  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_monitoria_fluxo) or die ("erro na query de criação da tabela ''monitoria_fluxo''");
  $tab_regmonitoria = "CREATE TABLE IF NOT EXISTS `$db`.`regmonitoria` (
		      `idreg` int(7) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      `idrelfiltro` int(6) UNSIGNED ZEROFILL NOT NULL,
		      `idfila` int(8) UNSIGNED ZEROFILL NOT NULL,
                      `idoper` INT (10) UNSIGNED ZEROFILL NOT NULL,
                      `idsuper` INT (4) UNSIGNED ZEROFILL NOT NULL,
		      `monitor` INT (4) UNSIGNED ZEROFILL NOT NULL,
		      `data` date NOT NULL,
		      `horaini` time NOT NULL,
		      `horafim` time NOT NULL,
		      `tmpaudio` time NOT NULL,
		      `gravacao` text NOT NULL,
		      PRIMARY KEY (`idreg`),
		      INDEX `monitor` (`monitor`),
		      INDEX `idrelfiltro` (`idfila`),
                      INDEX `idoper` (`idoper`),
                      INDEX `idsuper` (`idsuper`)
		      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
  $cad_tab = $_SESSION['query']($tab_regmonitoria) or die ("erro na criação da tabela ''regmonitoria''");
  $tab_moniavalia = "CREATE TABLE `$db`.`moniavalia` (
		    `idmoni` INT (7) UNSIGNED ZEROFILL NOT NULL,
		    `id_plan` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `id_aval` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `valor_aval` DECIMAL (10,2) NOT NULL,
		    `valor_final_aval` DECIMAL (10,2) NOT NULL,
                    `valor_fg` DECIMAL (10,2) NOT NULL,
		    `id_grup` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `valor_grup` DECIMAL (10,2),
		    `valor_final_grup` DECIMAL (10,2),
		    `tab_grup` ENUM('S','N') NOT NULL,
		    `id_sub` INT (6) UNSIGNED ZEROFILL,
		    `valor_sub` DECIMAL (10,2),
		    `valor_final_sub` DECIMAL (10,2),
		    `tab_sub` ENUM('S','N'),
		    `id_perg` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `tipo_resp` ENUM('U','M') NOT NULL,
		    `valor_perg` DECIMAL (10,2),
		    `valor_final_perg` DECIMAL (10,2),
		    `tab_perg` ENUM('S','N') NOT NULL,
		    `id_resp` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `avalia` ENUM('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','FG', 'FGM') NOT NULL,
		    `valor_resp` DECIMAL (10,2),
		    `obs` TEXT NOT NULL,
		    INDEX `idmoni` ( `idmoni` ),
		    INDEX `id_plan` (`id_plan`),
		    INDEX `id_aval` (`id_aval`),
		    INDEX `id_grup` (`id_grup`),
		    INDEX `id_sub` (`id_sub`),
		    INDEX `id_perg` (`id_perg`),
		    INDEX `id_resp` (`id_resp`),
		    INDEX `avalia` (`avalia`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_moniavalia) or die ("erro no processo de criação da tabela ''moniavalia''");
  $tab_moniavaliacalib = "CREATE TABLE `$db`.`moniavaliacalib` (
		    `idmoni` INT (7) UNSIGNED ZEROFILL NOT NULL,
		    `id_plan` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `id_aval` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `valor_aval` DECIMAL (10,2) NOT NULL,
		    `valor_final_aval` DECIMAL (10,2) NOT NULL,
                    `valor_fg` DECIMAL (10,2) NOT NULL,
		    `id_grup` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `valor_grup` DECIMAL (10,2),
		    `valor_final_grup` DECIMAL (10,2),
		    `tab_grup` ENUM('S','N') NOT NULL,
		    `id_sub` INT (6) UNSIGNED ZEROFILL,
		    `valor_sub` DECIMAL (10,2),
		    `valor_final_sub` DECIMAL (10,2),
		    `tab_sub` ENUM('S','N'),
		    `id_perg` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `tipo_resp` ENUM('U','M') NOT NULL,
		    `valor_perg` DECIMAL (10,2),
		    `valor_final_perg` DECIMAL (10,2),
		    `tab_perg` ENUM('S','N') NOT NULL,
		    `id_resp` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `avalia` ENUM('TABULACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','AVALIACAO','FG', 'FGM') NOT NULL,
		    `valor_resp` DECIMAL (10,2),
		    `obs` TEXT NOT NULL,
		    INDEX `idmoni` ( `idmoni` ),
		    INDEX `id_plan` (`id_plan`),
		    INDEX `id_aval` (`id_aval`),
		    INDEX `id_grup` (`id_grup`),
		    INDEX `id_sub` (`id_sub`),
		    INDEX `id_perg` (`id_perg`),
		    INDEX `id_resp` (`id_resp`),
		    INDEX `avalia` (`avalia`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8";
  $cad_tab = $_SESSION['query']($tab_moniavaliacalib) or die ("erro no processo de criação da tabela ''moniavalia''");
  $tab_agcalibragem =  "CREATE TABLE `$db`.`agcalibragem` (
                        `id` INT( 3 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                        `dataini` DATE NOT NULL ,
                        `datafim` DATE NOT NULL ,
                        `participantes` VARCHAR ( 20 ) NOT NULL ,
                        `usercont` VARCHAR ( 20 ) NOT NULL ,
                        `idmoni` INT ( 7 ) UNSIGNED ZEROFILL NOT NULL ,
                        `idrelfiltro` INT ( 6 ) UNSIGNED ZEROFILL NOT NULL ,
                        `gravacao` VARCHAR ( 1000 ) NOT NULL ,
                        `idmonicalib` INT (7) UNSIGNED ZEROFILL NULL,
                        `finalizado` ENUM ('S','N') NOT NULL DEFAULT 'S',
                        INDEX ( `idmoni` ),
                        INDEX ( `idrelfiltro` )
                        ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_agcalibragem) or die ("erro no processo de criação da tabela ''agcalibragem''");
  $tab_confcalib = "CREATE TABLE `$db`.`conf_calib` (
                    `id` INT( 1 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    `bloq` ENUM( 'N', 'S' ) NOT NULL DEFAULT 'N',
                    `tempobloq` VARCHAR( 5 ) NOT NULL ,
                    `confemail` INT( 2 ) UNSIGNED ZEROFILL NOT NULL ,
                    `intercambio` ENUM ('N','S') NOT NULL DEFAULT 'N',
                    `usersacesso` VARCHAR( 1000 ) NOT NULL ,
                    `ativo` ENUM ('N','S') NOT NULL DEFAULT 'N',
                    INDEX ( `confemail` )
                    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_confcalib) or die ("erro no processo de criação da tabela ''conf_calib''");
  $tab_monitoriacalib = "CREATE TABLE `$db`.`monitoriacalib` ( `idmoni` int( 7 ) unsigned zerofill NOT NULL AUTO_INCREMENT ,
                        `idcalib` INT ( 3 ) UNSIGNED ZEROFILL NOT NULL ,
                        `idmonivinc` int( 7 ) unsigned zerofill NOT NULL ,
                        `idrelfiltro` int( 6 ) unsigned zerofill NOT NULL ,
                        `idfila` int( 8 ) unsigned zerofill NOT NULL ,
                        `idoper` int( 6 ) unsigned zerofill NOT NULL ,
                        `idsuper` int( 4 ) unsigned zerofill NOT NULL ,
                        `iduser` VARCHAR ( 20 ) NOT NULL ,
                        `data` date NOT NULL ,
                        `horaini` time NOT NULL ,
                        `horafim` time NOT NULL ,
                        `tmpaudio` time NOT NULL ,
                        `id_plan` int( 4 ) unsigned zerofill NOT NULL ,
                        `qtdefg` int( 2 ) NOT NULL ,
                        `qtdefgm` int( 2 ) NOT NULL ,
                        `gravacao` varchar( 1000 ) NOT NULL ,
                        PRIMARY KEY ( `idmoni` ) ,
                        INDEX `idcalib` (`idcalib`),
                        INDEX `id_plan` ( `id_plan` ) ,
                        INDEX `iduser` ( `iduser` ) ,
                        INDEX `idfila` ( `idfila` ) ,
                        INDEX `idrelfiltro` ( `idrelfiltro` ) ,
                        INDEX `idmonivinc` ( `idmonivinc` ) )
                        ENGINE = InnoDB DEFAULT CHARSET = utf8;";
  $cad_tab = $_SESSION['query']($tab_monitoriacalib) or die ("erro no processo de criação da tabela ''monitoriacalib''");
  $tab_monitoriaadm = "CREATE TABLE `$db`.`monitoriaadm` (
		   `idmoni` INT (7) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `idfila` INT (8) UNSIGNED ZEROFILL NOT NULL,
		   `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		   `idoper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		   `idsuper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		   `monitor` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   `horaini` TIME NOT NULL,
		   `horafim` TIME NOT NULL,
		   `tmpaudio` TIME NOT NULL,
		   `id_plan` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   `type` VARCHAR (30) NOT NULL,
		   `gravacao` TEXT NOT NULL,
		   PRIMARY KEY ( `idmoni` ),
		   INDEX `id_plan` (`id_plan`),
		   INDEX `idoper` (`idoper`),
		   INDEX `idsuper` (`idsuper`),
		   INDEX `monitor` (`monitor`),
		   INDEX `idrelfiltro` (`idrelfiltro`)
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_monitoriaadm) or die ("erro no processo de criação da tabela ''monitoriaadm''");
  $tab_moniavaliaadm = "CREATE TABLE `$db`.`moniavaliaadm` (
		    `idmoni` INT (7) UNSIGNED ZEROFILL NOT NULL,
		    `id_plan` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `id_aval` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `valor_aval` DECIMAL (10,2) NOT NULL,
		    `valor_final_aval` DECIMAL (10,2) NOT NULL,
		    `id_grup` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `valor_grup` DECIMAL (10,2),
		    `valor_final_grup` DECIMAL (10,2),
		    `tab_grup` ENUM('S','N') NOT NULL,
		    `id_sub` INT (6) UNSIGNED ZEROFILL,
		    `valor_sub` DECIMAL (10,2),
		    `valor_final_sub` DECIMAL (10,2),
		    `tab_sub` ENUM('S','N'),
		    `id_perg` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `tipo_resp` ENUM('U','M') NOT NULL,
		    `valor_perg` DECIMAL (10,2),
		    `valor_final_perg` DECIMAL (10,2),
		    `tab_perg` ENUM('S','N') NOT NULL,
		    `id_resp` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `avalia` ENUM('AVALIACAO', 'TABULACAO', 'SUPERACAO', 'FG'),
		    `valor_resp` DECIMAL (10,2),
		    `obs` TEXT NOT NULL,
		    INDEX `idmoni` ( `idmoni` ),
		    INDEX `id_plan` (`id_plan`),
		    INDEX `id_aval` (`id_aval`),
		    INDEX `id_grup` (`id_grup`),
		    INDEX `id_sub` (`id_sub`),
		    INDEX `id_perg` (`id_perg`),
		    INDEX `id_resp` (`id_resp`),
		    INDEX `avalia` (`avalia`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_moniavaliaadm) or die ("erro no processo de criação da tabela ''moniavaliaadm''");
  $tab_chat = "CREATE TABLE `$db`.`chat` (
	      `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
	      `id_user` INT (4) UNSIGNED ZEROFILL NOT NULL,
	      `nuser` VARCHAR (100) NOT NULL,
	      `id_para` INT (4) UNSIGNED ZEROFILL NOT NULL,
	      `npara` VARCHAR (100)  NOT NULL,
	      `assunto` VARCHAR (100) NOT NULL,
	      `msg` VARCHAR (500) NOT NULL,
	      PRIMARY KEY ( `id` ),
	      INDEX `id_user` (`id_user`),
	      INDEX `assunto` (`assunto`)
	      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_chat) or die ("erro no processo de criação da tabela ''chat''");
  $tab_conf_chat = "CREATE TABLE `$db`.`conf_chat` (
		   `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `assunto` VARCHAR (100) NOT NULL,
		   `caduser` INT (4) UNSIGNED ZEROFILL NOT NULL,
		   PRIMARY KEY ( `id` ),
		   INDEX `assunto` ( `assunto` )
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_conf_chat) or die ("erro no processo de criação da tabela ''conf_chat''");
  $tab_operador = "CREATE TABLE `$db`.`operador` (
		  `idoper` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `idsuper` INT (4) NOT NULL,
		  `dtbase` DATE NOT NULL,
		  `horabase` TIME NOT NULL,
		  PRIMARY KEY (`idoper`),
		  INDEX `idsuper` (`idsuper`)
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_operador) or die ("erro no processo de criação da tabela ''operador''");
  $tab_filaoper = "CREATE TABLE `$db`.`fila_oper` (
		  `id` INT (8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `idupload` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `idoper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `idsuper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `dataini` DATE NOT NULL,
		  `datafim` DATE NOT NULL,
		  `qtdmoni` INT (3) UNSIGNED ZEROFILL NOT NULL,
		  `monitorando` INT (1) NOT NULL,
		  PRIMARY KEY (`id`),
		  INDEX `idoper` ( `idoper` ),
		  INDEX `idsuper` ( `idsuper` ),
		  INDEX `idrelfiltro` ( `idrelfiltro` ),
		  INDEX `idupload` ( `idupload` )
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_filaoper) or die ("erro no processo de criação da tabela ''fila_oper''");
  $tab_filagrava = "CREATE TABLE `$db`.`fila_grava` (
		  `id` INT (8) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		  `idupload` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `idoper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `idsuper` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		  `dataini` DATE NOT NULL,
		  `datafim` DATE NOT NULL,
		  `monitorado` INT (1) NOT NULL,
		  `camaudio` VARCHAR (300) NOT NULL,
		  `monitorando` INT (1) NOT NULL,
		  PRIMARY KEY (`id`),
		  INDEX `idoper` ( `idoper` ),
		  INDEX `idsuper` ( `idsuper` ),
		  INDEX `idrelfiltro` ( `idrelfiltro` ),
		  INDEX `idupload` ( `idupload` )
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_filagrava) or die ("erro no processo de criação da tabela ''fila_grava''");
  $tab_upload = "CREATE TABLE `$db`.`upload` (
		    `id` INT (6) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `idrelfiltro` INT (6) UNSIGNED ZEROFILL NOT NULL,
		    `iduser` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    `tipouser` ENUM ('user_adm','user_web') NOT NULL,
		    `camupload` VARCHAR (300) NOT NULL,
		    `dataini` DATE NOT NULL,
		    `datafim` DATE NOT NULL,
		    `dataup` DATE NOT NULL,
		    `horaup` TIME NOT NULL,
		    `imp` ENUM ('S','N') NOT NULL DEFAULT 'N',
		    `dataimp` DATE NOT NULL,
		    `horaimp` TIME NOT NULL,
		    PRIMARY KEY ( `id` ),
		    INDEX `idrelfiltro` (`idrelfiltro`),
		    INDEX `iduser` (`iduser`)
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8";
  $cad_tab = $_SESSION['query']($tab_upload) or die ("erro no processo de criação da tabela ''upload''");
  $tab_conta_audios = "CREATE TABLE `$db`.`qtdaudios` (
		      `qtd` INT (9) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		      PRIMARY KEY (`qtd`)
		      ) ENGINE = Innodb DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_conta_audios) or die ("erro no processo de criação da tabela ''qtdaudios''");
  $tab_superoper = "CREATE TABLE `$db`.`super_oper` (
		   `idsuper` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `dtbase` DATE NOT NULL,
		   `horabase` TIME NOT NULL,
		   PRIMARY KEY (`idsuper`)
		   ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_superoper) or die ("erro no processo de criação da tabela ''super_oper''");
  $tab_periodo = "CREATE TABLE `$db`.`periodo` (
		    `id` INT (2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		    `mes` INT (2) NOT NULL,
		    `nmes` VARCHAR (20) NOT NULL,
		    `dataini` DATE NOT NULL,
		    `datafim` DATE NOT NULL,
		    `dias` INT (3) UNSIGNED ZEROFILL NOT NULL,
		    `datacad` DATE NOT NULL,
		    `hora` TIME NOT NULL,
		    `usuario` INT (4) UNSIGNED ZEROFILL NOT NULL,
		    PRIMARY KEY (`id`),
		    INDEX `usuario` (`usuario`)
		    ) ENGINE = Innodb DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_periodo) or die ("erro no processo de criação da tabela ''periodo''");
  $tab_diasdecon = "CREATE TABLE `$db`.`diasdescon` (
		   `id` INT (4) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
		   `idperiodo` INT (2) UNSIGNED ZEROFILL NOT NULL,
		   `data` DATE NOT NULL,
		   `usuario` INT(4) UNSIGNED ZEROFILL NOT NULL,
		   PRIMARY KEY (`id`),
		   INDEX `idperiodo` (`idperiodo`),
		   INDEX `usuario` (`usuario`)
		   ) ENGINE = Innodb DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_diasdecon) or die ("erro no processo de criação da tabela ''diasdescon''");
  $tab_configemail = "CREATE TABLE `$db`.`config_mail` (
		     `id` INT( 1 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		     `email` VARCHAR( 50 ) NOT NULL ,
		     `senha` VARCHAR( 50 ) NOT NULL ,
		     `host` VARCHAR( 50 ) NOT NULL ,
		     `porta` VARCHAR( 6 ) NOT NULL ,
		     `tls` ENUM( 'SIM', 'NAO' ) NOT NULL DEFAULT 'NAO',
		     `usercad` INT( 4 ) UNSIGNED ZEROFILL NOT NULL ,
		      INDEX ( `usercad` )
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_configemail) or die ("erro no processo de criação da tabela ''config_mail''");
  $tab_confemail = "CREATE TABLE `$db`.`confemailenv` (
		      `id` INT (2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY,
		      `tipo` ENUM ('IMPORTACAO','FLUXO','USUARIO','MONITOR','CALIB'),
		      `titulo` VARCHAR(200) NOT NULL,
		      `msg` TEXT NOT NULL,
		      `campo` VARCHAR (300) NOT NULL,
		      `idcaduser` INT (4) UNSIGNED ZEROFILL NOT NULL,
		      INDEX `iduser` (`idcaduser`)
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_confemail) or die ("erro no cadastramento da tabela ''confemailenv''");
  $tab_relemailfluxo = "CREATE TABLE `$db`.`relemailfluxo` (
		       `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY,
		       `idrel` INT ( 6 ) UNSIGNED ZEROFILL NOT NULL,
		       `idrelfluxo` INT( 2 ) UNSIGNED ZEROFILL NOT NULL,
		       `idfluxo` INT ( 2 ) UNSIGNED ZEROFILL NOT NULL,
		       `idstatus` INT ( 2 ) UNSIGNED ZEROFILL NOT NULL,
		       `iddefini` INT( 2 ) UNSIGNED ZEROFILL NOT NULL,
		       `idconf` INT ( 2 ) UNSIGNED ZEROFILL NOT NULL,
		       `usersadm` VARCHAR( 100 ) NOT NULL,
		       `usersweb` VARCHAR( 100 ) NOT NULL,
		       INDEX `idrelfluxo` ( `idrelfluxo` ), 
		       INDEX `idfluxo` ( `idfluxo` ),
		       INDEX `iddefoni` ( `iddefini` ),
		       INDEX `idstatus` (`idstatus`),
		       INDEX `idconf` (`idconf`),
		       INDEX `idrel` (`idrel`)
		       ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_relemailfluxo) or die ("erro no cadastramento da tabela ''relemailfluxo''");
  $tab_relemailimp = "CREATE TABLE `$db`.`relemailimp` (
		      `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		      `idconf` INT  (2) UNSIGNED ZEROFILL NOT NULL,
		      `idrelfiltro` INT( 6 ) UNSIGNED ZEROFILL NOT NULL ,
		      `usersadm` VARCHAR( 200 ) NOT NULL,
		      `usersweb` VARCHAR( 200 ) NOT NULL,
		      INDEX `idconf` (`idconf`),
		      INDEX `idrelfiltro` (`idrelfiltro`)
		      ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_relemailimp) or die ("erro na criação da tabela ''relemailimp''");
  $tab_relmoni = "CREATE TABLE `$db`.`relmoni` (
		  `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		  `nome` VARCHAR( 50 ) NOT NULL ,
		  `qtdecol` INT( 1 ) UNSIGNED ZEROFILL NOT NULL ,
		  `tipo` ENUM( 'ANALITICO', 'SINTETICO' ) NOT NULL ,
		  `filtros` VARCHAR( 50 ) NOT NULL ,
		  `idperfadm` VARCHAR( 50 ) NOT NULL ,
		  `idperfweb` VARCHAR( 50 ) NOT NULL ,
		  `vincrel` INT( 2 ) UNSIGNED ZEROFILL,
		  `tabpri` ENUM ('monitoria','regmonitoria') NOT NULL
		  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_relmoni) or die ("erro na criação da tabela ''relmoni''");
  $tab_relmonicol = "CREATE TABLE `$db`.`relmonicol` (
		    `id` INT( 3 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		    `idrelmoni` INT( 2 ) UNSIGNED ZEROFILL NOT NULL ,
		    `tabela` VARCHAR( 50 ) NOT NULL ,
		    `coluna` VARCHAR( 50 ) NOT NULL ,
		    `nomeapres` VARCHAR( 100) NOT NULL ,
		    `posicao` INT (2) NOT NULL,
		    `ordena` ENUM('S','N') NOT NULL DEFAULT 'N',
		    `formula` VARCHAR( 50 ) NOT NULL ,
		    INDEX `idrelmoni` ( `idrelmoni` )
		    ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_relmonicol) or die ("erro na query de criação da tabela ''relmonicol''");
  $tab_indice = "CREATE TABLE `$db`.`indice` (
		`id` INT(2) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`idindice` INT (2) UNSIGNED ZEROFILL NOT NULL ,
		`numini` DECIMAL (10,2) NOT NULL ,
		`numfim` DECIMAL (10,2) NOT NULL ,
		`nome` VARCHAR (50) NOT NULL,
		INDEX `idindice` (`idindice`)
		) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_indice) or die ("erro na query de criação da tabela ''indice''");
  $tab_intervalo_notas = "CREATE TABLE `$db`.`intervalo_notas` (
			  `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			  `idindice` INT ( 2 ) UNSIGNED ZEROFILL NOT NULL ,
			  `numini` DECIMAL ( 10,2 ) NOT NULL ,
			  `numfim` DECIMAL ( 10,2 ) NOT NULL ,
			  `nome` VARCHAR( 50 ) NOT NULL,
			  INDEX `idindice` (`idindice`)
			  ) ENGINE = InnoDB DEFAULT CHARSET=utf8;";
  $cad_tab = $_SESSION['query']($tab_intervalo_notas) or die ("erro na query de criação da tabela ''intervalo_notas''");
  $tab_formulas = "CREATE TABLE `$db`.`formulas` (
                  `id` INT( 2 ) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                  `titulo` VARCHAR( 50 ) NOT NULL ,
                  `tipo` ENUM( 'PLANILHA', 'RELATORIO' ) NOT NULL ,
                  `idvinc` INT(6) UNSIGNED ZEROFILL NOT NULL,
                  `formula` VARCHAR( 1000 ) NOT NULL,
                  INDEX `idvinc` (`idvinc`)
                  ) ENGINE = InnoDB;";
  $cad_tab = $_SESSION['query']($tab_formulas) or die ("erro na query de criação da tabela ''formulas''");
  $perfiladm = "INSERT INTO `$db`.`perfil_adm` (
	      `nome` , `estrutura`, `rel_estru`, `user_adm` , `perfil_adm` , `user_web` , `perfil_web` , `filtros`, `monitor` , `definicao` , `status` , `estrufluxo`, `fluxo` , `motivo`,
	      `aval_plan`, `dimen` , `resposta` , `pergunta` , `subgrupo` , `grupo` , `planilha` , `calibragem` , `upload` , `operador` , `super_oper` , `vendas` , `nivel` , `relatorios` ,
	      `monitorias` , `ebook`, `ativo`) VALUES ('Administrador', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', 'E', '1', 'E', 'E',
	      'E', 'E', 'E', 'E', 'S');";
  $cadperfil = $_SESSION['query']($perfiladm) or die (mysql_error());
  $senhaadmin = md5(admin);
  $useradmin = "INSERT INTO `$db`.`user_adm` (`id` ,`nome` ,`CPF` ,`email` ,`perfil` ,`usuario` ,`senha` ,`ativo` ,`senha_ini`, `datacad`)
		VALUES ('0001', 'admin', '00000000000', 'admin@admin.com.br', '01', 'admin', '$senhaadmin', 'S', 'S', '$data');";
  $caduser = $_SESSION['query']($useradmin) or die (mysql_error());
  $alter = "ALTER TABLE `user_adm` ADD FOREIGN KEY ( `perfil` ) REFERENCES `$db`.`perfil_adm` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;"; //relaciona a coluna perfil com a coluna nome da tabela perfil_adm
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `user_login` ADD FOREIGN KEY ( `iduser_adm` ) REFERENCES `$db`.`user_adm` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `user_login` ADD FOREIGN KEY ( `nome_adm` ) REFERENCES `$db`.`user_adm` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;"; // relaciona as colunas iduser_adm e nome_adm com as colunas id e nome da tabela user_adm
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `user_web` ADD FOREIGN KEY ( `perfil` ) REFERENCES `$db`.`perfil_web` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";// relaciona a coluna perfil com a coluna nome da tabela perfil_web
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `user_login_web` ADD FOREIGN KEY ( `iduser_web` ) REFERENCES `$db`.`user_web` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `user_login_web` ADD FOREIGN KEY ( `nome_web` ) REFERENCES `$db`.`user_web` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_login` ADD FOREIGN KEY ( `id_moni` ) REFERENCES `$db`.`monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_login` ADD FOREIGN KEY ( `nome_moni` ) REFERENCES `$db`.`monitor` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_pausa` ADD FOREIGN KEY ( `id_moni` ) REFERENCES `$db`.`monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_pausa` ADD FOREIGN KEY ( `nome_moni` ) REFERENCES `$db`.`monitor` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `rel_fluxo` ADD FOREIGN KEY ( `id_fluxo` ) REFERENCES `$db`.`fluxo` (`id_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `rel_fluxo` ADD FOREIGN KEY ( `id_status` ) REFERENCES `$db`.`status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `rel_fluxo` ADD FOREIGN KEY ( `id_defini` ) REFERENCES `$db`.`definicao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `rel_fluxo` ADD FOREIGN KEY ( `id_atustatus` ) REFERENCES `$db`.`status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `ebook` ADD FOREIGN KEY ( `categoria` ) REFERENCES `$db`.`cat_ebook` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `ebook` ADD FOREIGN KEY ( `subcategoria` ) REFERENCES `$db`.`sub_ebook` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `id_plan` ) REFERENCES `$db`.`planilha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `id_aval` ) REFERENCES `$db`.`aval_plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `id_grup` ) REFERENCES `$db`.`grupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `id_sub` ) REFERENCES `$db`.`subgrupo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `id_perg` ) REFERENCES `$db`.`pergunta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `id_resp` ) REFERENCES `$db`.`pergunta` (`id_resp`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_dimen` ADD FOREIGN KEY ( `id_moni` ) REFERENCES `$db`.`monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_dimen` ADD FOREIGN KEY ( `nome_moni` ) REFERENCES `$db`.`monitor` (`nome`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `chat` ADD FOREIGN KEY ( `id_user` ) REFERENCES `$db`.`monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `chat` ADD FOREIGN KEY ( `assunto` ) REFERENCES `$db`.`conf_chat` (`assunto`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `filtro_dados` ADD FOREIGN KEY ( `id_filtro` ) REFERENCES `$db`.`filtro_nomes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `filtro_dados` ADD FOREIGN KEY ( `nivel` ) REFERENCES `$db`.`filtro_nomes` (`nivel`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `status` ADD FOREIGN KEY ( `idfluxo` ) REFERENCES `$db`.`fluxo` (`id_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `definicao` ADD FOREIGN KEY ( `idfluxo` ) REFERENCES `$db`.`fluxo` (`id_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `monitoria` ADD FOREIGN KEY ( `monitor` ) REFERENCES `$db`.`monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoria''");
  $alter = "ALTER TABLE `moniavalia` ADD FOREIGN KEY ( `idmoni` ) REFERENCES `$db`.`monitoria` (`idmoni`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `config_rel` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `monitoria` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `monitoria` ADD FOREIGN KEY ( `id_plan` ) REFERENCES `$db`.`planilha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `moni_dimen` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `oper_fluxo` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `useradmfiltro` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `userwebfiltro` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `upload` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `useradmfiltro` ADD FOREIGN KEY ( `iduser` ) REFERENCES `$db`.`user_adm` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `useradmfiltro` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `userwebfiltro` ADD FOREIGN KEY ( `iduser` ) REFERENCES `$db`.`user_web` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `userwebfiltro` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `vinc_plan` ADD FOREIGN KEY ( `idplan` ) REFERENCES `$db`.`planilha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `fila_grava` ADD FOREIGN KEY ( `idoper` ) REFERENCES `$db`.`operador` (`idoper`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `fila_grava` ADD FOREIGN KEY ( `idsuper` ) REFERENCES `$db`.`super_oper` (`idsuper`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `fila_grava` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `fila_oper` ADD FOREIGN KEY ( `idoper` ) REFERENCES `$db`.`operador` (`idoper`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `fila_oper` ADD FOREIGN KEY ( `idsuper` ) REFERENCES `$db`.`super_oper` (`idsuper`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `fila_oper` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `dimen_estim` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `dimen_estim` ADD FOREIGN KEY ( `periodo` ) REFERENCES `$db`.`periodo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `config_mail` ADD FOREIGN KEY ( `usercad` ) REFERENCES `$db`.`user_adm` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die (mysql_error());
  $alter = "ALTER TABLE `relemailfluxo` ADD FOREIGN KEY ( `idrelfluxo` ) REFERENCES `$db`.`rel_fluxo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailfluxo''");
  $alter = "ALTER TABLE `relemailfluxo` ADD FOREIGN KEY ( `idfluxo` ) REFERENCES `$db`.`fluxo` (`id_fluxo`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailfluxo''");
  $alter = "ALTER TABLE `relemailfluxo` ADD FOREIGN KEY ( `iddefini` ) REFERENCES `$db`.`definicao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailfluxo''");
  $alter = "ALTER TABLE `relemailfluxo` ADD FOREIGN KEY ( `idstatus` ) REFERENCES `$db`.`status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailfluxo''");
  $alter = "ALTER TABLE `relemailfluxo` ADD FOREIGN KEY ( `idconf` ) REFERENCES `$db`.`confemailenv` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailfluxo''");
  $alter = "ALTER TABLE `relemailfluxo` ADD FOREIGN KEY ( `idrel` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailfluxo''");
  $alter = "ALTER TABLE `relemailimp` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailimp''");
  $alter = "ALTER TABLE `fila_grava` ADD FOREIGN KEY ( `idupload` ) REFERENCES `$db`.`upload` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''fila_grava''");
  $alter = "ALTER TABLE `fila_oper` ADD FOREIGN KEY ( `idupload` ) REFERENCES `$db`.`upload` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''fila_oper''");
  $alter = "ALTER TABLE `relemailimp` ADD FOREIGN KEY ( `idconf` ) REFERENCES `$db`.`confemailenv` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relemailimp''");
  $alter = "ALTER TABLE `regmonitoria` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''regmonitoria''");
  $alter = "ALTER TABLE `regmonitoria` ADD FOREIGN KEY ( `idoper` ) REFERENCES `$db`.`operador` (`idoper`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''regmonitoria''");
  $alter = "ALTER TABLE `regmonitoria` ADD FOREIGN KEY ( `idsuper` ) REFERENCES `$db`.`super_oper` (`idsuper`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''regmonitoria''");
  $alter = "ALTER TABLE `regmonitoria` ADD FOREIGN KEY ( `monitor` ) REFERENCES `$db`.`monitor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''regmonitoria''");
  $alter = "ALTER TABLE `rel_aval` ADD FOREIGN KEY ( `id_plan` ) REFERENCES `$db`.`planilha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''rel_aval''");
  $alter = "ALTER TABLE `rel_aval` ADD FOREIGN KEY ( `id_aval` ) REFERENCES `$db`.`aval_plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''rel_aval''");
  $alter = "ALTER TABLE `relmonicol` ADD FOREIGN KEY ( `idrelmoni` ) REFERENCES `$db`.`relmoni` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''relmonicol''");
  $alter = "ALTER TABLE `intervalo_notas` ADD FOREIGN KEY ( `idindice` ) REFERENCES `$db`.`indice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''intervalo_notas''");
  $alter = "ALTER TABLE `monitoria_fluxo` ADD FOREIGN KEY ( `idmonitoria` ) REFERENCES `$db`.`monitoria` (`idmoni`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoria_fluxo''");
  $alter = "ALTER TABLE `monitoria_fluxo` ADD FOREIGN KEY ( `idstatus` ) REFERENCES `$db`.`status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoria_fluxo''");
  $alter = "ALTER TABLE `monitoria_fluxo` ADD FOREIGN KEY ( `iddefinicao` ) REFERENCES `$db`.`definicao` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoria_fluxo''");
  $alter = "ALTER TABLE `param_moni` ADD FOREIGN KEY ( `indicenota` ) REFERENCES `$db`.`indice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''indicenota''");
  $alter = "ALTER TABLE `motivo` ADD FOREIGN KEY ( `tipo` ) REFERENCES `$db`.`tipo_motivo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''motivo''");
  $alter = "ALTER TABLE `conf_calib` ADD FOREIGN KEY ( `confemail` ) REFERENCES `$db`.`confemailenv` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''conf_calib''");
  $alter = "ALTER TABLE `monitoriacalib` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoriacalib''");
  $alter = "ALTER TABLE `monitoriacalib` ADD FOREIGN KEY ( `id_plan` ) REFERENCES `$db`.`planilha` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoriacalib''");
  $alter = "ALTER TABLE `agcalibragem` ADD FOREIGN KEY ( `idmoni` ) REFERENCES `$db`.`monitoria` (`idmoni`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''agcalibragem''");
  $alter = "ALTER TABLE `agcalibragem` ADD FOREIGN KEY ( `idrelfiltro` ) REFERENCES `$db`.`rel_filtros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''agcalibragem''");
  $alter = "ALTER TABLE `monitoriacalib` ADD FOREIGN KEY ( `idcalib` ) REFERENCES `$db`.`agcalibragem` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoriacalib''");
  $alter = "ALTER TABLE `moniavaliacalib` ADD FOREIGN KEY ( `idmoni` ) REFERENCES `$db`.`monitoriacalib` (`idmoni`) ON DELETE CASCADE ON UPDATE CASCADE;";
  $exealt = $_SESSION['query']($alter) or die ("erro na criação da chave estrangeira da tabela ''monitoriacalib''");
  
  if($caduser) {
  return true;
  }
  else {
  return false;
  }
}
?>