<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MANUAL</title>
<link href="style.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.6.2.js" type="text/javascript"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script src="/monitoria_supervisao/js/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
</head>
<script type="text/javascript">
    $(document).ready(function() {
        $('.ulsubmenu').hide();
        var ck = 0;
        $("li[id*='li']").live('click',function() {
            $('.ulsubmenu').hide();
            var id = $(this).attr('id');
            var ncampo = id.substr(2);
            $('#menu'+ncampo).show();
        })
        $('#conteudo').load('/monitoria_supervisao/docs/conteudo.php');
        $("a").live('click',function() {
            var id = $(this).attr('id');
            var quebra = id.split("#");
            $('#conteudo').load('/monitoria_supervisao/docs/conteudo.php',{pag:quebra[0],submenu:quebra[1]});
        });
        
         $("a[rel*='agenda']").prettyPhoto();
         $("a[rel*='agendar']").prettyPhoto();
    });
</script>
<body>
<div id="tudo">
    <div id="barra" style="width:1024px; background-color:#06C; float:left">
        <form id="formpesq" method="post">
        <table width="1024">
          <tr>
            <td width="300"><input type="text" id="texto" name="texto" value="" style="width:300px"/></td>
            <td width="712"><input type="submit" id="busca" name="busca" value="" style="background-image:url(/monitoria_supervisao/images/search.png); border: 0px; width: 25px; height: 25px"  /></td>
          </tr>
        </table>   
        </form>
    </div>
        <div id="menu">
            <br/>
            <form method="post" action="">
                <input type="submit" id="reset" name="reset" value="RESET" />
            </form>
    	<?php
        $rais = $_SERVER['DOCUMENT_ROOT'];
        $paginas = array('SUMARIO' => 'sumario','FAQ' => 'faq','CONFIGURAÇÕES DE ESTRUTURA' => 'configestru',
                         'USUÁRIOS' => 'users','CONFIGURAÇÕES GERAIS' => 'configgeral','PLANILHA' => 'planilha','ADICIONAIS' => 'adicionais');
        $menu['SUMARIO'] = array('Login','Senha','Logout','Monitor','User WEB','User ADM','Filtros','Estrutura','Relacionamento Estrutura','Monitoria','ID Monitoria','Fila Gravação','Fila Operador','Data','DataCtt','Hora',
                                'HoraCtt','Cad. Base','Hora Cad.','Administrador','Monitorias','Produção','Calibragem','Importação','Relatório
                                Semanal','E-Book','Cadastros','Clientes','Intervalo Notas','Parametros Monitoria','Config. de Envio de E-mail','Usuários','Perfil Administradores','Perfil WEB','Usuários WEB','Monitor','Configurações Gerais','Calendário','Estrutura Fluxo','Relacionamento Fluxo','Motivo','Dimensionamento','Operador',
                                'Lista Operador','Supervisor Operador','Monitorias','Fila Monitorias','ABS','PERC','Planilha','Formulas Notas/ Médias',
                                'Perguntas','Sub-Grupos','Grupos','Planilha','Tabulação','Perg. Resp. - Tabulação','Calibragem','Adicionais',
                                'Cadastro E-Book','Orientações','Assuntos Chat','Configurações Relatórios','Monitorias','Resultados','Gestão','Fila de Monitorias','Fechamento','Exportação Base');
        $menu['FAQ'] = array('Instalacao','Cadastros Iniciais para funcionamento mínimo do sistema','Parametros','Area de Monitoria','Relatórios Semanais','Agendamento Calibragem','Histórico Calibragem','Consulta de Dados');
        $menu['CONFIGURAÇÕES DE ESTRUTURA'] = array('Clientes','Filtros','Estrutura','Relacionamento Estrutura','Intervalo Notas',
                                                    'Parametros Monitoria','Config. de Envio de E-mail');
        $menu['USUÁRIOS'] = array('Perfil Administradores','Perfil WEB','Usuários WEB','Monitor');
        $menu['CONFIGURAÇÕES GERAIS'] = array('Calibragem','Calendário','Estrutura Fluxo','Relacionamento Fluxo','Motivo','Dimensionamento','Operador','Lista Operador','Supervisor Operador','Monitorias','Fila Monitorias');
        $menu['PLANILHA'] = array('Formulas Notas/ Médias','Perguntas','Sub-Grupos','Grupos','Planilha','Tabulação','Perg. Resp. - Tabulação');
        $menu['ADICIONAIS'] = array('Cadastro E-Book','Orientações','Assusntos Chat');
        ?>
        <ul class="ulmenu">
        <?php
        $cmenu = count($menu);
        $i = 0;
        foreach($menu as $km => $m) {
            $i++;
        ?>
            <li id="li<?php echo $i;?>"><strong><?php echo $km;?></strong>
            <ul class="ulsubmenu" id="menu<?php echo $i;?>">
            <?php
            $csub = count($m);
            $newmenu = array_unique($m);
            if($km == "SUMARIO") {
                asort($newmenu);
            }
            foreach($newmenu as $sub) {
                ?>
                <li><a href="#" id="<?php echo $paginas[$km]."#".$sub;?>"><?php echo $sub;?></a></li>
                <?php
            }
            ?>
            </ul>
            </li>
        <?php   
        }
        ?>
        </ul>
    </div>
    <div id="conteudo">
    </div>
</div>
</body>
</html>
