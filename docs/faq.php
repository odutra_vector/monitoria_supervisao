<br />
<strong>TÓPICO: </strong><?php echo strtoupper($this->menu)." - ".$this->submenu;?>
<br /><br />
<?php 
if($this->submenu == "Instalacao") {
    ?>
    <strong>Instalação</strong><br/><br/>
    O sistema deve ser instalado em um servidor <span style="font-weight: bold">APACHE</span> com banco de dados <span style="font-weight: bold">MYSQL</span>.<br/><br/>
    O primeiro passo é copiar os arquivos fonte do sistema, para a pasta html do servidor WEB (Apache).<br/><br/>
    Após a copia dos arquvios para o servidor, acesse o caminho: <a target="_blank" href="/monitoria_supervisao/config/index.php">/monitoria_supervisao/config</a>.<br/><br/>
    Nesta área será solicitada as configurações do servidor de banco de dados, sendo os dados:<br/><br/>
    <?php 
        $tamdiv = round((690 / 3),2);
        $imgagenda = array('configini.jpg');
        $caminhos = array('/monitoria_supervisao/docs/images/','/monitoria_supervisao/docs/images/thumbnails/');
        $cimg = count($imgagenda);
        $i = 0;
        foreach($imgagenda as $img) {
            $i++;
            if($i == 1) {
                $title = "title=\"Config\"";
            }
            else {
                $title = "";
            }
        ?>
        <a href="<?php echo $caminhos[0].$img;?>" rel="configini"  <?php echo $title;?>><img src="<?php echo $caminhos[1].$img;?>" width="300" height="198" tite="<?php echo basename($img,'.php');?>"/></a><br/><br/>
      <?php
      }
      ?>
    - Tipo do Servidor;<br/>
    - Servidor (IP do servidor).<br/>
    - Porta de acesso ao servidor.<br/>
    - User (usuário de acesso ao banco de dados).<br/>
    - Senha (do usuário do banco de dados).<br/>
    - Nova Banco (cadastro do primeiro cliente do sistema).<br/><br/>
    Após informar estes dados, clique em "Continuar Configurações".<br/><br/>
    O sistema automáticamente irá criar o primeiro bando de dados e todas configurações necessáris para acesso ao sistema.<br/><br/>
    O primeiro usuário criado no sistema é o <span style="font-weight: bold">"admin"</span> e que em seu primeiro acesso terá a senha "admin".
    <?php
}
if($this->submenu == "Cadastros Iniciais para funcionamento mínimo do sistema") {
    
}
?>
