<br />
<strong>TÓPICO: </strong><?php echo strtoupper($this->menu)." - ".$this->submenu;?>
<br /><br />
<?php 
if($this->submenu == "Login") {
    ?>
    <strong>Login</strong> - sequencia de cadasteres para definir o acesso do usuário ao sistema. A sequencia de cadasteres é definida pelos administradores do sistema, podendo conter qualquer tipo de senquencia e caracter.<br /><br />        
    Com o login, o usuário deve ingressar na tela inicial do sistema para preencher os dados solicitados, <strong>LOGIN e SENHA</strong>, nos caso de acessos externos o <strong>CAPTCHA</strong>.
    <?php
}
if($this->submenu == "Senha") {
    ?>
    <strong>Senha</strong> - a senha inicial do usuário é definida automáricamente pelo sistema, seguindo uma sequencia aleatória de caracteres contendo 10 caracteres sendo numero, letras e caracteres especiais.<br />
    <br />Após receber a senha inicial o usuário deverá acessar a tela inicial do sistema para efetuar o primeiro login, onde o sistema irá solicitar impreterivelmente a trocar da mesma. <br /><br />A nova senha para acesso ao sistema 
    deverá conter no mínimo 8 caracteres, sendo alphanumérica. Todo este processo de acesso ao sistema e troca de senha, visa garantir a segurança e integridade dos dados. Assim como a privacidade dos usuários.
    <?php
}
if($this->submenu == "Logout") {
    ?>
    <strong>Logout</strong> - O logou do sistema deve ser feito sempre utilizando-se o botão que fica no quadro localizado no canto superior esquerdo da tela, contendo os dados de acesso do usuário.<br/><br/>
    Este processo para sair do sistema deve ser seguido para garantir o correto funcionamento do sistema, pois o usuário que simplesmente fecha o navegador pode correr o risco do seu usuário continuar logado no sistema,
    isto porque nem todos nevegadores limpam as SESSION quando fechados.<br/><br/>O que irá definir se um usuário já está ou não logado no sistema é sua SESSION, o servidor automaticamente limpa esta SESSION após 30min.
    <?php
}
if($this->submenu == "Monitor") {
    ?>
    <strong>Monitor</strong> - Monitor é o usuário destinado a realização das monitorias que terão seu input no sistema, desta forma, este é o usuário mais importante para o funcionando do sistema. Sem os usuários
    "Monitores" cadastrados no sistema, nenhum dado para resultados estará e nenhuma monitoria poderá ser realizada.<br/><br/>
    Para cadastro dos monitores é importe existir usuários <a href="#" id="<?php echo $this->pag."#User ADM";?>"><strong>ADMINISTRADORES</strong></a> cadastros, pois o monitor precisar estar relacionado a um lider direto.<br/><br/>
    Os monitores podem ser cadastrados no caminho abaixo:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> USUÁRIOS -> MONITORES</strong><br/><br/>
    No cadastro dos monitores os dados: <strong>Nome, CPF, responsável, hora entrada e hora saida</strong> são obritórios.
    <?php
}
if($this->submenu == "User WEB") {
    ?>
    <strong>Usuário WEB</strong> - Usuários WEB, são os usuários que terão acesso externo ao sistema, este usuário não terá acesso a área administrativa do sistema, que controla o funcionanmento e input de dados.<br/><br/>
    O cadastro dos Usuários WEB pode ser efetuado na área Administrativa seguindo o caminho abaxo:<br/><br/>
    <strong>ADMINISTRADOR -> CADASTROS -> USUÁRIOS -> USUÁRIOS WEB</strong><br/><br/>
    É importante primeramente o cadastramento dos perfis que vão compor os <strong>Usuários WEB</strong>.<br/><br/>
    São obrigatórios os seguintes dados para cadastro: <strong>Nome, CPF, e-mail, perfil, se poderá agendar calibragem e se poderá efetuar importações remotas de audios.</strong><br/><br/>
    O sistema irá criticar duplicidades de <strong>nomes, login e cpf</strong>. Desta forma, impedindo o cadastro.
    
    <?php
}
if($this->submenu == "User ADM") {
    ?>
    <strong>Usuário ADMINISTRATIVOS</strong> - Usuários ADM, são os usuários que terão acesso a área administrativa do sistema, o acesso a área administrativa pode ser configurada para permitir edição ou somente visualização de informações,
    desta forma, é possível criar vários perfis de usuários admnistrativos, cada qual com sua particularidade de acesso restrito ou não.<br/><br/>
    O cadastro dos Usuários ADM pode ser efetuado na área Administrativa seguindo o caminho abaxo:<br/><br/>
    <strong>ADMINISTRADOR -> CADASTROS -> USUÁRIOS -> USUÁRIOS ADMINISTRATIVOS</strong><br/><br/>
    É importante primeramente o cadastramento dos perfis que vão compor os <strong>Usuários ADMINISTRATIVOS</strong>.<br/><br/>
    São obrigatórios os seguintes dados para cadastro: <strong>Nome, CPF, e-mail, perfil, se poderá agendar calibragem e se poderá efetuar importações remotas de audios.</strong><br/><br/>
    O sistema irá criticar duplicidades de <strong>nomes, login e cpf</strong>. Desta forma, impedindo o cadastro.
    <?php
}
if($this->submenu == "Filtros") {
    ?>
    <strong>Filtros</strong> - Os <strong>filtros</strong> são totalmente configuráveis para cada base criada no sistema. Podendo os mesmos serem alterados, ativados ou inativos a qualquer momento, respeitando a premissa de que, um filtro inativo
    deixa de ser visualizado no sistema em utilizações.<br/><br/>
    Os filtro são responsáveis pela consolidação dos dados em consultas ou extração de dados, a planilha é o 1º filtro macro em qualquer consulta no sistema, sendo assim, os filtros criados podem gerar diferentes consolidaçoes conforme as <a href="#" id="<?php echo $this->pag."#Estrutura";?>"><strong>Estruturas</strong></a>
    criadas.<br/><br/>
    Para cadastramento dos filtros é importante respeitar uma regra, todo texto que contém palavras separadas por espaço, deverá ter a substituição do <strong>espaço " " por underline (_)</strong>. Esta condição é primordial para que os caminhos dos audios salvos
    não apresentem erros, pois a localização dos audios para abertura pode não ser reconhecida por alguns tipos de servidores, principalmente com sistema operacional Windows. Desta forma, o caracter <strong>underline</strong> impede que qualquer erro de localização do arquivo ocorra. Também garante maior integridade
    na criação das arvores dos audios.<br/><br/>
    Como os filtros são criados em arvore, sua hierarquia também é seguida na arvore, desta forma, é importante ter idéia do nível hierarquico que será seguido na criação dos filtros.<br/><br/>
    Os filtros podem ser cadastros no caminho abaixo:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> CONFIGURAÇÕES DE ESTRUTURA -> FILTROS</strong><br/><br/>
    Os filtros também estão diretamente ligados aos Relacionamentos que serão criados, no próprio sumário tem a explicação do que significa um Relacionamento entre filtros.
    <?php
}
if($this->submenu == "Estrutura") {
    ?>
    <strong>Estrutura</strong> - A Estrutura é a nomenclatura utilizada para criar diversas opções para cada filtro, desta forma possibilitanto uma grande quantidade de cruzamentos de filtros que serão utilizados no <strong><a href="#" id="<?php echo $this->pag."#Relacionamento Estrutura";?>">Relacionamento</a></strong>.<br/><br/>
    O nível da estrutura respeita o nível hierarquico dos filtros, desta forma, duas estruturas diferentes mas relacionadas ao mesmo filtros, terão o mesmo nível hierarquico.<br/><br/>
    <strong><a href="#" id="<?php echo $this->pag."#Relacionamento Estrutura";?>">Relacionamento</a></strong> é uma nomenclatura própria do sistema para entitular o vinculo entre filtros, sua explicação está no Sumário.<br/><br/>
    Para cadastro das Estrururas acesse o caminho abaixo:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> CONFIGURAÇÕES DE ESTRUTURA -> ESTRUTURA</strong>
    <?php
}
if($this->submenu == "Relacionamento Estrutura") {
    ?>
    <strong>Relacionamento</strong> - Relacionamento é a nomenclatura associada ao sistema quando são criados vinculos entre as estruturas dos filtros no sistema.<br/><br/>
    Cada relacionamento representa a menor consolidação possível dos dados, os Relacionamentos são criados em formato de arvore, possibilitando uma grande quantidade de consolidação
    dos dados somente dependendo da quantidade de filtros criados. Caso o usuário tente criar um relacionamento que já existe o sistema irá criticar, para não permitir duplicidade.<br/><br/>
    Cada Relacionameto criado gerado um códgo individual que será acompanhado por todo sistema, é este relacionamento que estará vinculo a monitoria ou registros que tenham input no sistema.<br/><br/>
    O Relacionamento pode ser cadastrado no caminho:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> CONFIGURAÇÕES DE ESTRUTURA -> RELACIONAMENTO</strong><br/><br/>
    Um Relacionamento somente poderá ser apagado enquanto não exitir nenhum registro efetuado no sistema utilizando este relacionamento.
    <?php
}
if($this->submenu == "Monitoria") {
    ?>
    <strong>Monitoria</strong> - A monitoria é o registro de avaliação de uma análise de um audio ou ligação em tempo real. A monitoria está sempre relacionada a uma ou mais planilhas cadastras.<br/><br/>
    A monitoria sempre conterá os seguintes dados: <strong>Operador, Relacionamento, data, hora, monitor que realizou, planilha</strong> os demais dados podem ser acrescentados dinamicamente no sistema.<br/><br/>
    Os dados que não são fixos podem ser acrescentados juntamente com os audios ou lista de dados pre-formatada, também podem ser acrescentados durante a monitoria, bastando criar os campos desejados.<br/><br/>
    Cada monitoria gera um <strong>código individual</strong> para garantir a consistencia do sistema, todos relatórios e resultados visualizados estão relacionados a um núemro de monitoria ou registro.<br/><br/>
    Quando uma monitoria é apagada do sistema, todos dados co-relacionados também são apgados, para garantir a consistencia das informações e o audio ou registro que foi importado ao sistema volta para fila de monitoria (gravação ou on-line).
    <?php
}
if($this->submenu == "ID Monitoria") {
    ?>
    <strong>ID Monitoria</strong> - <strong>ID Monitoria</strong> é a código gerado automaticamente pelo sistema para registrar a monitoria realizada. Este código é único e para cada monitoria salva com uma planilha diferente, desta forma, uma mesma ligação
    ou audio pode conter mais de uma monitoria, desde que exista mais de 1 planilha vinculada ao <a href="#" id="<?php echo $this->pag."#Relacionamento Estrutura";?>"><strong>Relacionamento</strong></a>.<br/><br/>
    O código das monitorias excluídas não são re-aproveitados pelo sistema, pois a sequencia lógica de numeração é sempre crescente.<br/><br/>
    É possível cofigurar o sistema para validar as monitoradas que contenham respostas que são consideradas (Falhas Graves, Falhas Gravissímas, Não Conformidades Graves), desta forma, estas monitorias só ficam disponíveis para consulta e só vão constar nos relatórios
    após sua validação.<br/><br/>
    As monitorias que precisam ser validadas são visíveis através do caminho abaixo:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> CONFIGURAÇÕES GERAIS -> MONITORIAS</strong>
    <?php
}
if($this->submenu == "Fila Gravação") {
    ?>
    <strong>Fila Gravação</strong> - A fila de gravação é a fila ordenada criada pelo sistema para disponibilisar os audios aos monitores, esta fila segue diversos parametros para ordenação, sendo que alguns destes parametros podem ser configurados através do sistema.<br/><br/>
    Os parametros configuráveis através do sistema são: ordem por data da importação, ordem pela ultima monitoria realizada do operador, dias a contar da data da importação, limita a quantidade de monitorias por operador.<br/><br/>
    Além dos parametros configuravéis a ordenação da fila inicia-se pelo percentual de atigimento da meta do <strong>Relacionamento</strong> e pela data de contato mais atual.<br/><br/>
    A fila de gravação pode ser configurada juntamente com os <a href="#" id="<?php echo $this->pag."#Parametros Monitoria";?>"><strong>Parametros Monitoria</strong></a> no caminho abaixo:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> CONFIGURAÇÕES GERAIS -> PARAMETROS MONITORIA</strong><br/><br/>
    A fila de gravação pode ser visualizada em um relatório, neste relatório é possível escolher se a fila será visualizada por Relacionamento ou por Operador. Na opção por Relacionamento é informado a quantidade de audios Importados (I.),
    Monitorias Realizadas (R.), Improdutovas (I.) e Disponível para Monitoria (EST.).<br/><br/> Na opção por Operador será solicitado qual o tipo de consolidação dos dados, os dados serão apresentados por operador dia a dia, informando a a quantidade de audios importados do operador no dia,
    quantidade de monitorias realizadas do operador e improdutivas.<br/><br/>
    É aconselhável que os dados enviados ao sistema não contenham caracteres especiais (*&%$#@!?/\|;:<>(){}[]°º-=+) e também é aconselhável a substituição de acentuação por letras não acentuadas. Estas condições favorecem a correta visualização das informações em qualquer ambiente, pois a codificação de
    caracteres pode variar bastante entre servidores e desktop dos usuários.
    <?php
}
if($this->submenu == "Fila Operador") {
    ?>
    <strong>Fila Operador</strong> - A fila por operador é a fila ordenada criada pelo sistema para disponibilizar o próximo operador que será monitorado de forma randomica.<br/><br/>
    Os parametros configuráveis através do sistema são: limita a quantidade de monitorias por operador, possibilita a monitoria por operador limitando a 1 por dia e o por data da ultima monitoria.<br/><br/>
    Além dos parametros configuravéis a ordenação da fila inicia-se pelo percentual de atigimento da meta do <strong>Relacionamento</strong>.<br/><br/>
    A fila de gravação pode ser configurada juntamente com os <a href="#" id="<?php echo $this->pag."#Parametros Monitoria";?>"><strong>Parametros Monitoria</strong></a> no caminho abaixo:<br/>
    <strong>ADMINISTRADOR -> CADASTROS -> CONFIGURAÇÕES GERAIS -> PARAMETROS MONITORIA</strong><br/><br/>
    A fila por operador  pode ser visualizada em um relatório, neste relatório é possível escolher se a fila será visualizada por Relacionamento ou por Operador. Na opção por Relacionamento é informado a quantidade de audios Importados (I.),
    Monitorias Realizadas (R.), Improdutovas (I.) e Disponível para Monitoria (EST.).<br/><br/> Na opção por Operador será solicitado qual o tipo de consolidação dos dados, os dados serão apresentados por operador dia a dia, informando a a quantidade de audios importados do operador no dia,
    quantidade de monitorias realizadas do operador e improdutivas.<br/><br/>
    É aconselhável que os dados enviados ao sistema não contenham caracteres especiais (*&%$#@!?/\|;:<>(){}[]°º-=+) e também é aconselhável a substituição de acentuação por letras não acentuadas. Estas condições favorecem a correta visualização das informações em qualquer ambiente, pois a codificação de
    caracteres pode variar bastante entre servidores e desktop dos usuários.
    <?php
}
if($this->submenu == "Data") {
    ?>
    <strong>Data</strong> - A Data no sistema, corresponde a data da monitoria ou a data atual, esta nomenclatura depende do local onde está sendo consultada a data, mas sempre será sinalizado em que situação a <storng>data</strong> está condicionada.
    <?php
}
if($this->submenu == "DataCtt") {
    ?>
    <strong>Data</strong> - A Datactt, somente é aplicada nos casos de monitoria por gravação, pois a DataCtt correponde a data em qua a ligação foi gravada. O campo DataCtt já vem pré-configurado pelo sistema nas tabelas que se fazem necessárias esta informação.<br/><br/>
    Desta forma, caso a monitoria seja on-line, a DataCtt de contato não será disponibilizada para visualização apesar de ser salva no sistema como sendo igual a data da monitoria.<br/><br/>
    Para importação no sistema é importante lembrar que as datas devem ser informadas no seguinte formato "DDMMAAAA", ex: 01012011.
    <?php
}
if($this->submenu == "Hora") {
    ?>
    <strong>Hora</strong> - A Hora corresponde a hora de realização da monitoria, hora de cadastro e hora atual de utilização do sistema, sempre sendo sinalizado em que situação este dado está sendo apresentado.<br/>
    <?php
}
if($this->submenu == "HoraCtt") {
    ?>
    <strong>HoraCtt</strong> - A HoraCtt somente é utilizada nas monitorias por gravação, desta forma, corresponde a hora em que o audio foi gravado.<br/><br/>
    A formato da HoraCtt no sistema deve ser informado nas importações no seguinte layout "HHMMSS", ex: "000000".<br/>
    <?php
}
if($this->submenu == "Cad. Base") {
    ?>
    <strong>Cad. Base</strong> - Esta nomenclatura se referete a data em o que o cadastro foi efetuado no sistema referente a determinada informação. Ou seja, a data em que usuários foram cadastrados, a data em que um fluxo de monitoria foi criado, a data em que um operador 
    foi adicionado a base. Esta data sempre será apresentada no formato "DD/MM/AAAA", ex: 01/01/1900.
    <?php
}
if($this->submenu == "Hora Cad.") {
     ?>
    <strong>Hora Cad.</strong> - Esta nomenclatura se referete a hora em que um cadastro doi efetuado no sistema referente a determinada informação. Como na nomenclatura <a href="#" id="<?php echo $this->pag."#Cad. Base";?>"><strong>Cad. Base</strong></a>, representa
    a hora que um usuário, fluxo ou operador foram inseridos na base de dados.
    <?php
}
if($this->submenu == "Administrador") {
    ?>
    <strong>Administrador</strong> - Nomenclatura para intitular o usuário que tem acesso total e irestrito ao sistema, imdependente de perfil ou área do sistema. Este usuário também não possui CPF cadastrado, exatamente por ser um usuário diferenciado.
    O usuário administrador é cadastrado quando é feita a instalação do sistema ou quando um novo cliente é cadastro. Desta forma, sua criação é automática, seus dados também não podem ser alterados por segurança.<br/><br/>
    Esta nomenclatura também é utilizada para intitular uma das áreas do sistema, esta área é responsável pela Administração, Controlole e Gestão do sistema. Nesta área são efetuados todos cadastros do sistema, desde usuários, passando por planilhas, fluxo, estrutura e etc.<br/>
    Também é possível controlar usários, relatórios e logs do sistema e fazer a Gestão da <a href="#" id="<?php echo $this->pag."#Fila Gravação";?>">Fila Gravação </a>, <a href="#" id="<?php echo $this->pag."#Fila Operador";?>">Fila Operador</a>, dos logs do sistema
    e do fechamento.<br/><br/>
    A área <strong>ADMINISTRADOR</strong> é dividida em três modulos:<br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Cadastros";?>">CADASTROS</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Configurações Relatórios";?>">CONFIGURAÇÕES RELATÓRIOS</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Gestão";?>">GESTÃO</a><br/><br/>
    <?php
}
if($this->submenu == "Monitorias") {
    ?>
    <strong>Monitorias</strong> - A palavra monitoria no sistema refere-se ao registro salvo após o preenchimento de uma (Ficha, Planilha, Tabulação ou qualquer nome que seja atribuido a um conjunto de pergunta e respostas). A <strong>monitoria</strong> sempre gera um <a href="#" id="<?php echo $this->pag."#ID Monitoria";?>">ID Monitoria</a> individual 
    e único a cada registro salvo para cada (Ficha, Planilha, Tabulação e etc), ou seja, em uma <strong>monitoria</strong> que está sendo realizada, se existiram 3 (Fichas, Planilhas e etc), serão salvos 3 <strong>monitorias</strong> com <a href="#" id="<?php echo $this->pag."#ID Monitoria";?>">ID Monitoria</a> diferente. E nesta caso mesmo que o audio seja
    o mesmo, o sistema automáticamente vai vincular este audio para cada <strong>monitoria</strong> salva.<br/>
    O sistema entende de forma diferente, tanto em contexto como em nomenclatura, um registro que é salvo assossiado a uma (Ficha, Planilha e etc) do registro que é salvo como <strong>Improdutivo</strong>. O registro <strong>Improdutivo</strong> é um registro que não está ligado a nenhuma (Ficha, Planilha e etc).<br/><br/>
    As <strong>montiorias</strong> podem ser visualizadas através da Área "MONITORIAS", bastando-se criar e configurar um relatório que forneça o <a href="#" id="<?php echo $this->pag."#ID Monitoria";?>">ID Monitoria</a>, assim o sistema já assossia automáticamente o <a href="#" id="<?php echo $this->pag."#ID Monitoria";?>">ID Monitoria</a> a tela
    para visualizar o registro.<br/><br/>
    As <strong>monitorias</strong> são a principal fonte de dados do sistema, pois é través delas que são consolidados os dados para abastecer os relatórios análiticos, gerencias e de produção. Desta forma, o sistema também possui alguns pontos de segurança para garantir a consistencias das informações.<br/>Um destes pontos de verificação é identificar possíveis monitorias realizadas
    que não possuam perguntas e respostas salvas, o que geraria uma inconsistencia da quantidade de monitorias com as quantidades de respostas, desta forma, o sistema exclui automáticamente toda monitoria realizada e que não possua respostas salvas.
    <?php
}
if($this->submenu == "Produção") {
    ?>
    <strong>Produção</strong> - A <strong>produção</strong> é a quantificação dos registros de monitoria realizados, entre um determinado período, que pode considerar a <a href="#" id="<?php echo $this->pag."#DataCtt";?>">DataCtt</a> ou a <a href="#" id="<?php echo $this->pag."#Data";?>">Data</a> da monitoria. A <strong>produção</strong> pode ser visualizada no sistema
    em uma área exclusiva <strong>"PRODUÇÃO"</strong>. Nesta área é possível visualizar a <strong>produção</strong> de diversas formas, sendo elas:<br/><br/>
    MONITOR<br/>
    RELACIONAMENTO<br/>
    FILTRO<br/><br/>
    
    Além dos filtros acima, é possível escolher o mês e o tipo de consolidação por período <strong>(DIA, SEMANA e MÊS)</strong>.<br/><br/>
   
    Na visualização por <strong>MONITOR</strong> pode-se ver a produção, monitor a montior e dia a dia. Quando esta opção é esclolhida também abre-se mais uma caixa perguntando se o usuário deseja filtrar por <strong>Supervisor</strong>, ou visualizar todos monitores. Onde o resultado também pode ser exportado para excel, word e PDF como em qualquer área do sistema.<br/><br/>
    Na visualização por <strong>RELACIONAMENTO</strong> pode-se visualizar a produção dia a dia por <a href="#" id="<?php echo $this->pag."#Relacionamento"?>">Relacionamento</a>. Esta opção tras todos relacionamentos ativos no sistema e sua respectiva produção.<br/><br/>
    Na visualização por <strong>FILTRO</strong> é possivel escolher qual filtro será utilizado para consolidar a informação, desta forma, independente de quantos filtros existam no sistema, o usuário poderá escolher 1 e o relatório vai consolidar por cada nomenclatura existente no filtro escolhido.
    <?php
}
if($this->submenu == "Calibragem") {
    ?>
    <strong>CALIBRAGEM</strong> - A <strong>Calibragem</strong> é uma ferramenta disponibilizada no sistema que possibilita a realização de calibragens on-line entre 1 ou mais usuários do sistema, independentemente do seu tipo de acesso (<a href="#" id="<?php echo $this->pag."#Administrador";?>">ADMINISTRADOR</a>, <a href="#" id="<?php echo $this->pag."#Usuários WEB";?>">WEB</a> ou <a href="#" id="<?php echo $this->pag."#Monitor";?>">MONITOR</a>).<br/>
    <div class="divconteudo">
    Desta forma, é possível avaliar o nível de equiparação das avaliações entre os diferentes usuários do sistema. Esta ferramenta possui 4 modulos para administração do processo de calibragem, sendo os modulos:<br/><br/>
    <strong>AGENDA</strong> - Este modulo demonstra uma agenda das calibragens que foram marcadas, informando qual monitoria foi utilizada como base, qual o usuário padrão, quais usuários foram convocados, o período da calibragem (inicio e fim) e as monitorias já realizadas.<br/>
    É possível pesquisar uma calibragem específica utilizando diversoso filtros, dentres eles, os usuários, o mês, o ano e os filtros criados no sistema.<br/><br/>
        <?php 
        $tamdiv = round((690 / 2),2);
        $imgagenda = array('agenda1.jpg','agenda2.jpg');
        $caminhos = array('/monitoria_supervisao/docs/images/','/monitoria_supervisao/docs/images/thumbnails/');
        $cimg = count($imgagenda);
        foreach($imgagenda as $img) {
            $i++;
            if($i == 1) {
                $title = "title=\"Agenda\"";
            }
            else {
                $title = "";
            }
        ?>
            <a href="<?php echo $caminhos[0].$img;?>" rel="agenda" style="outline: none" border="0"  <?php echo $title;?>><img src="<?php echo $caminhos[1].$img;?>" width="300" height="198" tite="<?php echo basename($img,'.php');?>"/></a>
        <?php
        }
        ?>
    </div>
    <div class="divconteudo"><br/> 
    <strong>AGENDAR CALIBRAGEM</strong> - Este modulo da calibragem permito o agendamento das calibragens que serão realizadas. Este agendamento por ser executado utilizando diversos filtros. Os participantes
    das calibragens pode ser qualquer usuário do sistema, (<a href="#" id="<?php echo $this->pag."#Administrador";?>">ADMINISTRADOR</a>, <a href="#" id="<?php echo $this->pag."#Usuários WEB";?>">WEB</a> ou <a href="#" id="<?php echo $this->pag."#Monitor";?>">MONITOR</a>). A calibragem só é possível de ser efetuada com alguma monitoria já realizada no sistema, a mesma monitoria pode ser utilizada para calibragem
    diversas vezes, desde que, o intervalo que será realizada a calibragem não se interseccione.
    <br/><br/>
     <?php 
        $tamdiv = round((690 / 3),2);
        $imgagenda = array('agendar1.jpg','agendar2.jpg','agendar3.jpg');
        $caminhos = array('/monitoria_supervisao/docs/images/','/monitoria_supervisao/docs/images/thumbnails/');
        $cimg = count($imgagenda);
        $i = 0;
        foreach($imgagenda as $img) {
            $i++;
            if($i == 1) {
                $title = "title=\"Agendar\"";
            }
            else {
                $title = "";
            }
        ?>
        <a href="<?php echo $caminhos[0].$img;?>" rel="agendar"  <?php echo $title;?>><img src="<?php echo $caminhos[1].$img;?>" width="300" height="198" tite="<?php echo basename($img,'.php');?>"/></a>
      <?php
      }
      ?>
    </div>
    <div class="divconteudo"><br/>
    <strong>REAL. CALIBRAGEM</strong> - Este modulo serve para o usuário realizar a monitoria de calibragem, ou seja, ouvir o audio, tabular a planilha e salvar. Para os usuário (<a href="#" id="<?php echo $this->pag."#Administrador";?>">ADMINISTRADOR</a> e <a href="#" id="<?php echo $this->pag."#Usuários WEB";?>">WEB</a>) fica disponível uma opção 
    na área de ''CALIBRAGEM'' onde é possível visualizar todas calibragens agendadas e pendentes para este usuário. Já para o usuário <a href="#" id="<?php echo $this->pag."#Monitor";?>">MONITOR</a>, as monitorias de calibragem são disponibilizadas logo após o agendamento
    executado. A monitoria escolhida para calibragem, aparece para o monitor logo após a ultima monitoria realizada.<br/><br/>
    </div>
    <div class="divconteudo">
    <strong>ESTATISTICAS</strong> - Nesta opção será gerado o relatório comparativo das monitorias realizadas atráves da calibragem, o relatório pode ser criado conforme específicações de cada cliente. Desta forma,
    a forma como as informações são apresentas e quais informações são apresentadas, pode variar conforme as necessidades do cliente. O relatório poderá conter qualquer informação vinculada e salva na base de dados
    referente as monitorias realizadas.<br/><br/>
    </div>
    <?php
}

if($this->submenu == "Calendário") {
    ?>
    <strong>Calendário</strong> - O sistema permite criar um calendário de trabalho com data inicial e final, além dos dias não produtivos, este calendário será utilizado pelo sistema em diversos momentos, como distribuição de audios e calculo de produtividade.<br/><br/> O calendário de produção é unico para todo o sistema, 
    desta forma é adequado definir previamente o ciclo de produção, para que o sistema disponibilize os audios corretamente e possa executar os calculos de produção corretamente.<br/><br/> Normalmente o ciclo de produção do modo de monitoria <strong>"ON-LINE"</strong> possui as mesma
    datas de inicio e fim de mês do calendário <strong>GREGORIADO</strong>, já monitorias realizadas através de <strong>"GRAVAÇÃO"</strong> o ciclo de produção é diferente das datas dos contatos, desta forma o calendário deve se adequar aos dois modos de monitoria, caso o cliente possua as duas formas de monitoria. 
    <?php
}

if($this->submenu == "Cadastros") {
    ?>
    <strong>Cadastros</strong> - Cadastros no sistema refere-se a Área de <strong>CADASTROS</strong> presente no menu dos Administradores. Esta área contém 90% da configuração de funcionamento do sistema, ficando fora desta área somente a configuração dos Relatórios, que é efetuado em área específica.<br/><br/>
    Dentro de <strong>CADASTROS</strong> existe 5 grupos de opções para executar configurações, sendo elas:<br/><br/>
    CONFIGURAÇÕES DE ESTRUTURA<br/><br/>
    USUÁRIOS<br/><br/>
    CONFIGURAÇÕES GERAIS<br/><br/>
    PLANILHAS<br/><br/>
    ADICIONAIS<br/><br/>
    As caracteristicas e utilização de cada grupo, será explicada melhor nos tópicos detinados exclusivamente a cada grupo. 
    <?php
}

if($this->submenu == "Adicionais") {
    ?>
    <strong>Adicionais</strong> - Esta palavra defini a área do sistemas detinada a funcionalidades adicionais do sistema, que não estão diretamente relacionados com a execusão das monitorias e seu processo. Esta área está no menu <strong>ADMINISTRADOR</strong> e possiu os seguintes tópicos:<br/><br/>
    - <a href="#" id="<?php echo $this->pag."#CADASTRO E-BOOK"?>">CADASTRO E-BOOK</a><br/>
    - <a href="#" id="<?php echo $this->pag."#ORIENTAÇÕES"?>">ORIENTAÇÕES</a><br/>
    - <a href="#" id="<?php echo $this->pag."#ASSUNTO CHAT"?>">ASSUNTO CHAT</a><br/><br/>
    <?php
}

if($this->submenu == "Assuntos Chat") {
    ?>
    <strong>Assuntos Chat</strong> - Neste modulo é possível cadastrar os assuntos que serão utilizados pelos monitores para se comunicar com seus lideres, desta forma, garantindo que os assuntos possam ser menssurados e comtabilizados no banco de dados. Asssim, é possível gerar relatórios para acompanhar
    as dúvidas e identificar melhorias na operação e nos processo.<br/><br/>
    Como todas conversas são registradas no banco de dados é possível fazer diversos cruzamentos de dados para gerar relatórios por:<br/><br/>
    - Assunto<br/>
    - Monitor<br/>
    - Quantidades de menssagens<br/>
    <?php
}

if($this->submenu == "Cadastro E-Book") {
    ?>
    <strong>Cadastro E-Book</strong> - Área destinada ao cadastro dos assuntos, sub-assuntos e tipos de arquivos liberados para serem armazenados no servidor, o sistema faz validação dos arquivo pelo tipo <strong>MIME</strong> dos arquivos, garantindo total segurança no armazenamento das informações e segurança no servidor.<br/><br/> O E-book cria arvores com assuntos e sub-assuntos
    para facilizar o localização dos orientações, assim como faz busca por palavra chave nos campos existentes.<br/><br/>
    <?php
}

if($this->submenu == "Clientes") {
    ?>
    <strong>Clientes</strong> - Definição do nome da empresa que terá  seu atendimento monitorado, independente das empresas terceirizadas que prestam serviços a empresa cadastrada. Também é utilizado para nomear o banco de dados de cada cliente e assim permitir uma separação completa dos dado.<br/><br/>Trazendo segurança e sigilo das informações, mesmo com clientis concorrentes.
    <?php
}

if($this->submenu == "Config. de Envio de E-mail") {
    ?>
    <strong>Config. de Envio de E-mail</strong> - Esta nomenclatura refere-se a área para cadastramento do e-mail (SMTP) que será utilizado pelo sistema, para enviar menssagens aos usuáriios. Diversas áreas do sistema enviam menssagens aos usuários, sendo: <a href="#" id="<?php echo $this->pag."#User ADM"?>">Usuário ADM</a>, <a href="#" id="<?php echo $this->pag."#User WEB"?>">Usuário WEB</a>, 
    <a href="#" id="<?php echo $this->pag."#Monitor"?>">Monitor</a>, <a href="#" id="<?php echo $this->pag."#Estrutura Fluxo"?>">Estrutura Fluxo</a>, Agendamento de Calibragem, Alertas de Falhas.<br/><br/>
    Na mesma área, também são feitos os cadastros dos modelos contendo o texto e assunto do e-mail para cada tipo de utilização, sendo: <strong>Importação, Usuário, Fluxo, Monitor, Calibragem e Falha Grave</strong>.
    <?php
}

if($this->submenu == "Configurações Gerais") {
    ?>
    <strong>Configurações Gerais</strong> - Esta nomenclatura é um grupo configurações do sistema que encontra-se na área <strong>ADMINISTRADOR</strong>, este grupo contém diversas configurações importantes ao sistema mas não definem a estrutura do sistema. Esta área contém os seguintes modulos:<br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Calibragem"?>">CALIBRAGEM</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Calendário"?>">CALENDÁRIO</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#EStrutura Fluxo"?>">ESTRUTURA FLUXO</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Relacionamenot Fluxo"?>">RELACIONAMENTO FLUXO</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Motivo"?>">MOTIVO</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Dimensionamento"?>">DIMENSIONAMENTO</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Operador"?>">OPERADOR</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Lista Operador"?>">LISTA OPERADOR</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Supervisor Operador"?>">SUPERVISOR OPERADOR</a><br/><br/>
    - <a href="#" id="<?php echo $this->pag."#Monitorias"?>">MONITORIAS</a><br/><br/>
    <?php
}

if($this->submenu == "Clientes") {
    ?>
    <strong></strong>
    <?php
}

if($this->submenu == "Clientes") {
    ?>
    <strong></strong>
    <?php
}

if($this->submenu == "Clientes") {
    ?>
    <strong></strong>
    <?php
}

if($this->submenu == "Clientes") {
    ?>
    <strong></strong>
    <?php
}
?>