<?php


include_once("seguranca.php");
header('Content-type: text/html; charset=utf-8');

protegePagina();

if(isset($_GET['cliente'])) {
    $part = explode("-",$_GET['cliente']);
    $tabela = $part[1];
    $list[] = $part[0];
    $_SESSION['selbanco'] = $part[0]; // cria session do cliente que o usuário selecionou acesso
}
else {
    $_SESSION['selbanco'] = $_SESSION['selbanco'];
}
if(count($_SESSION['userbancos']) > 1) {
    $_SESSION['userbancos'] = $list;
    include_once('config/conexao.php');
    $selbanco = $_SESSION['seldb']($_SESSION['selbanco']);
    $_SESSION['user_tabela'] = $tabela;
    if($tabela == "user_adm" OR $tabela == "user_web") {
        $aliasperf = explode("_",$tabela);
        $aliasperf = $aliasperf[1];
    }
    else {
        $aliasperf = $tabela;
    }
    $dadoscli = "SELECT * FROM dados_cli WHERE nomedados_cli='".str_replace("monitoria_","", $_SESSION['selbanco'])."'";
    $edadoscli = $_SESSION['fetch_array']($_SESSION['query']($dadoscli)) or die ("erro na query para obter as configurações do cliente");
    $_SESSION['idcli'] = $edadoscli['iddados_cli'];
    $_SESSION['nomecli'] = $edadoscli['nomedados_cli'];
    $_SESSION['site'] = $_SERVER['HTTP_HOST'];
    $_SESSION['imagem'] = $edadoscli['imagem'];
    $_SESSION['ass'] = $edadoscli['assdigital'];
    $seluser = "SELECT * FROM $tabela WHERE usuario='".$_SESSION['usuarioLogin']."' AND senha='".$_SESSION['usuarioSenha']."'";
    $myquery = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta dos dados do usuário");
    $_SESSION['usuarioID'] =  $myquery['id'.$tabela]; // Pega o valor da coluna 'id do registro encontrado no MySQL
    $_SESSION['usuarioNome'] = $myquery['nome'.$tabela]; // Pega o valor da coluna 'nome' do registro encontrado no MySQL
    $_SESSION['usuarioemail'] = $myquery['email']; //Pega o valor da coluna e-mail do Mysql
    $_SESSION['usuarioperfil'] = $myquery['idperfil_'.$aliasperf]; //Pega o valor da coluna perfil do Mysql
    $_SESSION['tmonitor'] = $myquery['tmonitor'];
    $_SESSION['hlogin'] = date('H:i:s');
    $_SESSION['confapres'] = explode(",",$myquery['confapres']);
    $_SESSION['participante'] = $_SESSION['usuarioID']."-".$_SESSION['user_tabela'];
    $_SESSION['assuser'] = $myquery['ass'];
    $selcoloper = "SELECT MIN(idcoluna_oper), COUNT(*) as result, nomecoluna FROM coluna_oper WHERE incorpora='OPERADOR'";
    $eselcoloper = $_SESSION['fetch_array']($_SESSION['query']($selcoloper)) or die ("erro na consulta da coluna de identificação do operador");
    if($eselcoloper['result'] >= 1) {
        $_SESSION['operador'] = $eselcoloper['nomecoluna'];
    }
    else {
        $_SESSION['operador'] = "";
    }
    $selcolsuper = "SELECT MIN(idcoluna_oper), COUNT(*) as result, nomecoluna FROM coluna_oper WHERE incorpora='SUPER_OPER'";
    $eselcolsuper = $_SESSION['fetch_array']($_SESSION['query']($selcolsuper)) or die ("erro na consulta da coluna de identificação do operador");
    if($eselcolsuper['result'] >= 1) {
         $_SESSION['super'] = $eselcolsuper['nomecoluna'];
    }
    else {
        $_SESSION['super'] = "";
    }
    $id_user = $_SESSION['usuarioID'];
    $nome_user = $_SESSION['usuarioNome'];
    $data = date('Y-m-d');
    $hora = date('H:i:s');
    if($_SESSION['user_tabela'] == "monitor") {
        $countlog = "SELECT COUNT(*) as result FROM moni_login WHERE idmonitor='$id_user'";
        $ecountlog = $_SESSION['fetch_array']($_SESSION['query']($countlog)) or die ("erro na query de contagem dos logs do usuÃ¡rio");
        if($ecountlog['result'] >= 1) {
          $sellog = "SELECT * FROM moni_login WHERE idmonitor='$id_user' ORDER BY data DESC, login DESC, logout DESC LIMIT 1";
          $esellog = $_SESSION['fetch_array']($_SESSION['query']($sellog)) or die ("erro na query de verificaÃ§Ã£o de log");
          if($esellog['logout'] == "00:00:00" OR $esellog['logout'] == "") {
            $horafim = $ehora['saida'];
            $horaini = $esellog['login'];
            $verifdt = "SELECT '".$esellog['data']."' < '$data' as result";
            $everifdt = $_SESSION['fetch_array']($_SESSION['query']($verifdt));
            if($everifdt['result'] == 1) {
                $calctempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$horafim') - TIME_TO_SEC('$horaini')) as result";
            }
            else {
                $calctempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$hora') - TIME_TO_SEC('$horaini')) as result";
            }
            $ecalctempo = $_SESSION['fetch_array']($_SESSION['query']($calctempo)) or die ("erro na query de contagem do tempo de login");
            $atureg = "UPDATE moni_login SET logout='$horafim', tempo='".$ecalctempo['result']."' WHERE idmoni_login='".$esellog['idmoni_login']."'";
            $eatureg = $_SESSION['query']($atureg) or die ("erro na query de atualizaÃ§Ã£o do log");
          }
          else {
          }
        }
        else {
        }
      $registro = "INSERT INTO moni_login (idmonitor, ip, data, login) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')";
      $ereg = $_SESSION['query']($registro) or die ("erro na query de inserção do login");
      $status = "UPDATE monitor SET status='AGUARDANDO' WHERE idmonitor='$id_user'";
      $estatus = $_SESSION['query']($status) or die ("erro na query de inserção do status");
      $filas = array('fila_oper' => 'fo','fila_grava' => 'fg');
      foreach($filas as $kf => $f) {
          $upfila = "UPDATE $kf SET monitorando='0', idmonitor=NULL WHERE idmonitor='".$_SESSION['usuarioID']."' AND monitorando='0'";
          $eup = $_SESSION['query']($upfila) or die (mysql_error());
          $upfila = "UPDATE $kf SET monitorando='0', idmonitor=NULL WHERE idmonitor='".$_SESSION['usuarioID']."' AND monitorando='1' AND monitorado='0'";
          $eup = $_SESSION['query']($upfila) or die (mysql_error());
      }
    }
    if($_SESSION['user_tabela'] == "user_web") {
      $registro = $_SESSION['query']("INSERT INTO login_web (iduser_web, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuário
    }
    if($_SESSION['user_tabela'] == "user_adm") {
      $registro = $_SESSION['query']("INSERT INTO login_adm (iduser_adm, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuário
    }
}

//$url = $_SERVER['HTTP_REFERER'];
$idorientacao = $_GET['orientacao'];
$txtpesq = $_GET['texto'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1" />
<meta http-equiv="Content-Type" content="text/html, charset=utf-8"/>
<title>MENU PRINCIPAL</title>
<script type="text/javascript" language="javascript">
    history.forward();
</script>

<?php
if(isset($_GET['orientacao'])) {
    echo "<script type=\"text/javascript\">";
        echo "function ebook() {\n";
        $idorientacao = base64_encode($idorientacao);
            echo "window.open('/monitoria_supervisao/monitor/ebook.php?menu=ebook&texto=".$txtpesq."&orientacao=".$idorientacao."', '');\n";
        echo "}\n";
    echo "</script>";
}
else {
    
}
?>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>
    <body>
        <div id="tudo">
            <?php
            if($_SESSION['user_tabela'] == "user_adm") { // verifica valor da session para incluir menu
                include_once("menuadmin.php");
            }
            if($_SESSION['user_tabela'] == "user_web") { // verifica valor da session para incluir menu
                include_once('menuweb.php');
            }
            if($_SESSION['user_tabela'] == "monitor") {
                include_once('menumonitor.php');
            }
            ?>
            <div id="rodape">
                <table align="center" width="1024">
                    <tr>
                        <td align="center" style="color:#FFF"><strong>VECTOR DSI TEC - atualizado: <?php versao();?></strong></td>
                    </tr>
                    <tr>
                        <td height="15" align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
                    </tr>
                </table>
            </div>
            </div>
        </div>
    </body>
</html>