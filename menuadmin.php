<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.monitoria.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');
include_once($rais.'/monitoria_supervisao/users/function_filtros.php');

$cor = new CoresSistema();

$iduser = $_SESSION['usuarioID'];
$seluser = "SELECT * FROM user_adm WHERE iduser_adm='$iduser'";
$eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
$sellogin = "SELECT *, COUNT(*) as result FROM login_adm WHERE iduser_adm='$iduser'";
$esellogin = $_SESSION['fetch_array']($_SESSION['query']($sellogin)) or die (mysql_error());
$qtdelogin = $esellogin['result'];
$selfiltros = "SELECT * FROM useradmfiltro WHERE iduser_adm='".$_SESSION['usuarioID']."'";
$efiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta dos filtros vinculados ao usuário");
while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
    $idfiltros[] = $lfiltros['idrel_filtros'];
}

if($qtdelogin == 1) {
    $datalogin = $esellogin['data'];
    $horalogin = $esellogin['hora'];
}
if($qtdelogin > 1) {
    if(eregi("mssql", $_SESSION['query'])) {
        $login = "SELECT *, convert(varchar(10),data,126) as data, convert(varchar(8),hora,108) as hora FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY id DESC) as row FROM login_adm) T1 WHERE iduser_adm='$iduser' and row > 1 and row <= 2";
    }
    if(eregi("mysql", $_SESSION['query'])) {
        $login = "SELECT * FROM login_adm WHERE iduser_adm='$iduser' ORDER BY idlogin_adm DESC LIMIT 2";
    }
    $elogin = $_SESSION['query']($login) or die (mysql_error());
    $data = array();
    $hora = array();
    while($llogin = $_SESSION['fetch_array']($elogin)) {
        $data[] = $llogin['data'];
        $hora[] = $llogin['hora'];
    }
    if(eregi("mssql", $_SESSION['query'])) {
        $datalogin = $data[0];
        $horalogin = $hora[0];
    }
    if(eregi("mysql", $_SESSION['query'])) {
        $datalogin = $data[1];
        $horalogin = $hora[1];
    }
}

$dtlogin = date('Y-m-d');
$login = "SELECT (TIME_TO_SEC('".date('H:i:s')."') - TIME_TO_SEC(hora)) as tempo FROM login_adm ul WHERE ul.iduser_adm='$iduser' AND ul.data='$dtlogin' ORDER BY idlogin_adm DESC LIMIT 1";
$ehlogin = $_SESSION['fetch_array']($_SESSION['query']($login)) or die ("erro na consulta do tempo logado");
$hlogin = $ehlogin['tempo'];
$idper = periodo();

?>

<link href="stylemenuini.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/highcharts/highcharts.js"></script>
<script src="/monitoria_supervisao/js/highcharts/mootools-adapter.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<link type="text/css" href="/monitoria_supervisao/js/jquery.countdown.css" rel="stylesheet" />
<script src="/monitoria_supervisao/js/jquery.countdown.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jqModal.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/thickbox/thickbox.js"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/thickbox/thickbox.css" type="text/css" />
<!--<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.dialog.js"></script>-->
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("body").click(function(event) {
            var id = event.target.id;
            if(id != "auser") {
                $("#dduser").hide();
            }
        });
        
        $("#auser").click(function() {
            if($("#dduser").is(":visible")) {
                $("#dduser").animate({opacity: "hide"}, "fast");
                
            }
            else {
                $("#dduser").animate({opacity: "show"}, "fast");
            }
        });
        
        $('#logado').countdown({since: -<?php echo $hlogin;?>, compact: true, format: 'HMS'});
        <?php
        if($_GET['menu'] != "") {
            $cormenu = array('monitoria' => 'monitoria','calibragem' => 'calib','importa' => 'importa','resultados' => 'rela','semanal' => 'sema','contmonitor' => 'monitor','producao' => 'prod','selecao' => 'selecao');
            if($_GET['menu'] == "idmoni" OR $_GET['idreg']) {
                echo "$('#monitoria').css('background-color','#264BDE');\n";
                echo "$('#amonitoria').css('color','#FFF');\n";
            }
            else {
                echo "$('#".$cormenu[$_GET['menu']]."').css('background-color','#264BDE');\n";
                echo "$('#a".$cormenu[$_GET['menu']]."').css('color','#FFF');\n";
            }
        }
        else {
        }
        ?>
        $('li a').click(function() {
            var val = $(this).attr('id');
            var a = val.substr(1);
            $('li.admin').css('background-color','#264BDE');
            $('#'+a).css('background-color','#264BDE');
        })
        
        $("#logout").click(function() {
           $.post("/monitoria_supervisao/logout.php",function(ret) {
            window.location = '/monitoria_supervisao/index.php?msg='+ret;
           }); 
        });
        
    });
</script>
<?php
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
?>
<div style="width:1024px;">
    <div id="divcabecalho" style="background-image: url(images/logo/clientis_<?php echo strtolower($_SESSION['nomecli']).".png)";?>">
        <ul id="cabecalho">
            <li style="text-align: left; font-size: 16px;margin: 5px;">
                <img name="user" src="/monitoria_supervisao/images/user.png" width="35px" height="35px" id="auser"/>
                <div id="dduser" class="divrodape" style="padding: 10px">
                    <span style="color: #F90; font-size: 13px; font-weight: bold">Informações do Usuário</span><br/><br/>
                    <span style="color: #F90; font-weight: bold">Nome:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $_SESSION['usuarioNome'];?></span><br/>
                    <span style="color: #F90; font-weight: bold">IP:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $_SESSION['ip'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Email:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $eseluser['email'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Ult. Login:</span> <span style="color: #FFF;margin-left: 10px"><?php $data = banco2data($datalogin); echo $data." - ".$horalogin;?></span><br/>
                    <span style="color:#F90; font-weight: bold">Cliente:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $_SESSION['nomecli'];?></span><br/>
                    <div><div style="float:left"><span style="color:#F90; font-weight: bold">Logado: </span></div> <div id="logado" style="font-size:13px; color: #FFF; background-color: #000; width: 100px; float: left; margin-left: 10px"></div></div>
                </div>
            </li>            
            <li style="text-align: center; font-size: 16px; width:48%; margin: 0px; margin:auto; text-align: center"><img src="/monitoria_supervisao/images/img_logo.jpg" width="70px" height="35px" /></li>
            <li style="text-align: right;"><img name="logout" id="logout" src="/monitoria_supervisao/images/sair.png" width="35px" height="35px" style=" border: 0px; padding: 0px" /></li>
        </ul>
    </div><br/>
    <div style="float:right; width:1024px; align:center; text-align: center">
        <!--//if($_GET['menu'] == "contmonitor" OR $_GET['menu'] == "producao") {
        //}
        //else {-->
        <?php
        if($idper == 0) {
            ?>
            <marquee style="align:center; font-size: 14px" behavior="scroll" direction="up" HEIGHT=120 onmousemove="this.stop()" onmouseout="this.start()" scrollamount="3">
            <ul>
            <li style="color:#EA071E; text-align:center; font-size"><strong>Atenção, o período para este mês não está cadastrado!!!</strong></li>
            </ul>
            </marquee>
            <?php
        }
        else {
            $datas = "SELECT * FROM periodo WHERE idperiodo='$idper'";
            $edatas = $_SESSION['fetch_array']($_SESSION['query']($datas)) or die ("erro na query de consulta das datas do periodo");
            $dtiniper = $edatas['dataini'];
            $dtfimper = $edatas['datafim'];
            if($edatas['dataini'] != "" && $edatas['datafim'] != "") {
                $dtinictt = $edatas['dtinictt'];
                $dtfimctt = $edatas['dtfimctt'];
            }
            else {
                $dtinictt = substr($dtiniper,0,4)."-".substr($dtiniper,5,2)."-01";
                $dtfimctt = ultdiames(substr($dtiniper,8,2).substr($dtiniper,5,2).substr($dtiniper,0,4));
            }
            $dtloop = $dtiniper;
            $dt = 0;
            for(;$dt == 0;) {
                  $comdt = "SELECT '$dtloop' < '$dtfimper' as menor, '$dtloop' = '$dtfimper' as igual";
                  $ecomdt = $_SESSION['fetch_array']($_SESSION['query']($comdt)) or die ("erro na query de comparação das datas");
                  if($ecomdt['menor'] >= 1 OR $ecomdt['igual'] >= 1) {
                      $verifdt = jddayofweek ( cal_to_jd(CAL_GREGORIAN, substr($dtloop,5,2),substr($dtloop,8,2), substr($dtloop,0,4)), 0);
                      if($verifdt == 0 OR $verifdt == 6) {
                      }
                      else {
                          $compatu = "SELECT '$dtloop' > '".date('Y-m-d')."' as dtatu";
                          $ecompatu = $_SESSION['fetch_array']($_SESSION['query']($compatu)) or die ("erro na query de comparação da data com a data atual");
                          if($ecompatu['dtatu'] >= 1) {
                          }
                          else {
                            $diaspass[] = $dtloop;
                          }
                      }
                      if($ecomdt['menor'] == 1) {
                          $dt = 0;
                          $adddt = "SELECT ADDDATE('$dtloop',interval 1 day) as data";
                          $eadddt = $_SESSION['fetch_array']($_SESSION['query']($adddt)) or die ("erro na query para acrescentar 1 dia na data");
                          $dtloop = $eadddt['data'];
                      }
                      if($ecomdt['igual'] == 1) {
                          $dt = 1;
                      }
                  }
                  else {
                  }
            }
            $diasmes = $edatas['diasprod'];
            $metax = 0;
            foreach($idfiltros as $idf) {
                $seldimen = "SELECT COUNT(DISTINCT(m.idmonitoria)) as qtde, dm.estimativa, limita_meta FROM dimen_mod dm
                                    INNER JOIN monitoria m ON m.idrel_filtros=dm.idrel_filtros
                                    INNER JOIN conf_rel cr ON cr.idrel_filtros = dm.idrel_filtros
                                    WHERE dm.idrel_filtros='$idf' AND m.datactt BETWEEN '$dtinictt' AND '$dtfimctt' AND dm.idperiodo='$idper'";
                $eseldimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
                $limite = explode(",",$eseldimen['limita_meta']);
                $meta = (($eseldimen['estimativa'] / $diasmes) * count($diaspass));
                $perc = round(($eseldimen['qtde'] / $meta) * 100,0);
                if($limite[0] == "S" AND $limite[1] < $perc) {
                    $metaex++;
                    $limeta[] = "<li style=\"color:#EA071E; text-align:center; font-size\"><strong>".nomevisu($idf)." - meta excedida</strong></li>";
                }
                
            }
            if($metaex == 0) {
                ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $("#alertas").hide();
                    })
                </script>
                <?php
                if($_SESSION['imgbanner'] != "") {
                    ?>
                    <img src="images/logo/<?php echo $_SESSION['imgbanner'];?>" />
                    <?php
                }
            }
            else {
                ?>
                <marquee id="alertas" style="align:center; font-size: 14px" behavior="scroll" direction="up" HEIGHT=120 onmousemove="this.stop()" onmouseout="this.start()" scrollamount="3">
                <ul>
                <?php echo implode(" ",$limeta);?>
                </ul>
                </marquee>
                <?php
            }
        }
        ?>
        <?php
        if($_SESSION['tipotopo'] == "") {
        }
        if($_SESSION['tipotopo'] == "BANNER") {
        }
        if($_SESSION['tipotopo'] == "ORIENTACAO") {
        }
        if($_SESSION['tipotopo'] == "META") {
        }
        //}
        ?>
    </div>
</div>
    <div style="float:left; width:1024px; background-color: #FFFFFF; padding-bottom: 10px;">
    <ul id="admin">
        <?php
        $apres = explode(",",$eseluser['confapres']);
        $calib = array_search("CALIB", $apres);
        $count = 6;
        if($eseluser['import'] == "S") {
            $_SESSION['import'] = "S";
            $count++;
        }
        if($eseluser['selecaoaudios'] == "S") {
            $_SESSION['selecao'] = "S";
            $count++;
        }
        if($calib == 0) {
            $count++;
        }
        if($count > 6) {
            $valor = "985";
        }
        if($count <= 6) {
            $valor = "1001";
        }
        $tam = $valor / $count;
        $style= "style=\"width:".floor($tam)."px; border-top-left-radius: 10px;border-bottom-right-radius: 10px; padding-top:7px\"";
        ?>
        <a id="aadministrador" style="text-decoration:none; color:#FFF" href="admin/admin.php"><li id="administrador" class="admin" <?php echo $style; ?>><strong>ADMIN</strong></li></a>
        <a id="amonitoria" style="text-decoration:none; color:#FFF" href="inicio.php?menu=monitoria"><li id="monitoria" class="admin" <?php echo $style; ?>><strong>MONITORIA</strong></li></a>
        <a id="aprod" style="text-decoration:none; color:#FFF" href="inicio.php?menu=producao"><li id="prod" class="admin" <?php echo $style; ?>><strong>PRODUCAO</strong></li></a>
        <?php
        if($calib == 0) {
            echo "<a id=\"acalib\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=calibragem\"><li id=\"calib\" class=\"admin\" $style ><strong>CALIBRAGEM</strong></li></a>";
        }
        else {
        }
        if($eseluser['import'] == "S") {
            echo "<a id=\"aimporta\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=importa\"><li id=\"importa\" class=\"admin\" $style><strong>IMPORTAÇÃO</strong></li></a>";
        }
        if($eseluser['selecaoaudios'] == "S") {
            echo "<a id=\"aselecao\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=selecao\"><li id=\"selecao\" class=\"admin\" $style><strong>SELEÇÃO</strong></li></a>";
        }
        ?>
        <a id="asema" style="text-decoration:none; color:#FFF" href="inicio.php?menu=semanal"><li id="sema" class="admin" <?php echo $style; ?>><strong> RELATÓRIOS</strong></li></a>
        <!--<a id="arela" style="text-decoration:none; color:#FFF" href="inicio.php?menu=resultados"><li id="rela" class="admin" <?php //echo $style; ?>><strong>RELATÓRIOS</strong></li></a>-->
        <a id="amonitor" style="text-decoration:none; color:#FFF" href="inicio.php?menu=contmonitor"><li id="monitor" class="admin" <?php echo $style; ?>><strong>MONITOR</strong></li></a>
        <a id="aebook" style="text-decoration:none; color:#FFF" href="/monitoria_supervisao/monitor/ebook.php" target="_blank"><li id="ebook" class="admin" <?php echo $style; ?>><strong>EBOOK</strong></li></a>
    </ul>
</div>
<div id="continicio" class="corfd_pag">
<script type="text/javascript">
    $(document).ready(function() {
        $("div[id*='dados0018']").show();
    })
</script>
<?php
if(isset($_SESSION['bloq'])) {
    $menu = "relcalib";
}
else {
    if(isset($menu)) {

    }
    else {
        $menu = $_GET['menu'];
    }
    if($_GET['reset'] == "1") {
        unset($_SESSION['varsconsult']);
    }
    switch ($menu) {
        case "resultados":
            if(!file_exists('users/resultados.php')) {
                include_once 'erro.php';
            }
            else {
                include_once "users/resultados.php";
            }
        break;
        
        case "semanal":
            if(!file_exists('users/semanal.php')) {
                include_once 'erro.php';
            }
            else {
                include_once "users/semanal.php";
            }
        break;

        case "monitoria":
            if(!file_exists('users/monitoria.php')) {
                include_once 'erro.php';
            }
            else {
                include_once "users/monitoria.php";
            }
        break;

        case "idmoni":
        $moni = new Planilha();
        $moni->iduser = $iduser;
        $moni->jsdefin = 0;
        $moni->perfiluser = $_SESSION['usuarioperfil'];
        $moni->tabuser = $_SESSION['user_tabela'];
        $moni->pagina = "MONITORIA";
        $moni->DadosPlanVisualiza();
        $moni->MontaPlan_corpovisu();
        ?>
        <!--<script type="text/javascript">
            $(window).load("/monitoria_supervisao/delaudiotmp.php?caminho=<?php echo $moni->caminho;?>")
        </script>-->
        <?php
        break;
   
        case "idreg";
             if(!file_exists('users/registro.php')) {
                  include_once 'erro.php';
             }
             else {
                  include_once "users/registro.php";
             }
             break;
    
        case "grafico":
            if(!file_exists('users/grafico.php')) {
                include_once 'erro.php';
            }
            else {
                include_once "users/grafico.php";
            }
        break;

        case "calibragem":
            if($eseluser['calibragem'] == "S") {
                if(!file_exists('users/calibragem.php')) {
                    include_once 'erro.php';
                }
                else {
                    unset($_SESSION['varsconsult']);
                    include_once "users/calibragem.php";
                }
            }
            if($eseluser['calibragem'] == "N") {
                if(!file_exists('users/relcalib.php')) {
                    include_once 'erro.php';
                }
                else {
                    unset($_SESSION['varsconsult']);
                    include_once "users/relcalib.php";
                }
            }
        break;

        case "importa":
            if(!file_exists('admin/imp.php')) {
                include_once 'erro.php';
            }
            else {
                unset($_SESSION['varsconsult']);
                include_once "admin/imp.php";
            }
        break;
        
        case "selecao":
            if(!file_exists('admin/selecao.php')) {
                include_once 'erro.php';
            }
            else {
                unset($_SESSION['varsconsult']);
                include_once "admin/selecao.php";
            }
        break;
		
        case "contmonitor";
            if(!file_exists('admin/contmonitor.php')) {
                include_once 'erro.php';
            }
            else {
                unset($_SESSION['varsconsult']);
                include_once "admin/contmonitor.php";
            }
        break;

        case "producao";
            if(!file_exists('users/producao.php')) {
                include_once 'erro.php';
            }
            else {
                unset($_SESSION['varsconsult']);
                include_once "users/producao.php";
            }
        break;
    }
}
?>
</div>