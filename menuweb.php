<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.monitoria.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');
include_once($rais.'/monitoria_supervisao/users/function_filtros.php');

$cor = new CoresSistema();

$iduser = $_SESSION['usuarioID'];
$seluser = "SELECT * FROM user_web uw 
            INNER JOIN perfil_web pw ON pw.idperfil_web = uw.idperfil_web WHERE uw.iduser_web='$iduser';";
$eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
$sellogin = "SELECT *, COUNT(*) as result FROM login_web WHERE iduser_web='$iduser'";
$esellogin = $_SESSION['fetch_array']($_SESSION['query']($sellogin)) or die (mysql_error());
$qtdelogin = $esellogin['result'];
if($qtdelogin == 1) {
	$datalogin = $esellogin['data'];
	$horalogin = $esellogin['hora'];
}
if($qtdelogin > 1) {
        if(eregi("mssql", $_SESSION['query'])) {
		$login = "SELECT *, convert(varchar(10),data,126) as data, convert(varchar(8),hora,108) as hora FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY idlogin_web DESC) as row FROM login_web) T1 WHERE iduser_web='$iduser' and row > 1 and row <= 2";
	}
	if(eregi("mysql", $_SESSION['query'])) {
		$login = "SELECT * FROM login_web WHERE iduser_web='$iduser' ORDER BY idlogin_web DESC LIMIT 2";
	}
	$elogin = $_SESSION['query']($login) or die (mysql_error());
	$data = array();
	$hora = array();
	while($llogin = $_SESSION['fetch_array']($elogin)) {
		$data[] = $llogin['data'];
		$hora[] = $llogin['hora'];
	}
	if(eregi("mssql", $_SESSION['query'])) {
		$datalogin = $data[0];
		$horalogin = $hora[0];
	}
	if(eregi("mysql", $_SESSION['query'])) {
		$datalogin = $data[1];
		$horalogin = $hora[1];
	}
}

$dtlogin = date('Y-m-d');
$login = "SELECT (TIME_TO_SEC('".date('H:i:s')."') - TIME_TO_SEC(hora)) as tempo FROM login_web ul WHERE ul.iduser_web='$iduser' AND ul.data='$dtlogin' ORDER BY idlogin_web DESC";
$ehlogin = $_SESSION['fetch_array']($_SESSION['query']($login)) or die ("erro na consulta do tempo logado");
$hlogin = $ehlogin['tempo'];
?>
<link href="stylemenuini.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<link type="text/css" href="/monitoria_supervisao/js/jquery.countdown.css" rel="stylesheet"/>
<script src="/monitoria_supervisao/js/jquery.countdown.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jqModal.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/thickbox/thickbox.js"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/thickbox/thickbox.css" type="text/css" />
<!--<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.dialog.js"></script>-->
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("body").click(function(event) {
            var id = event.target.id;
            if(id != "auser") {
                $("#dduser").hide();
            }
        });
        
        $("#auser").click(function() {
            if($("#dduser").is(":visible")) {
                $("#dduser").animate({opacity: "hide"}, "fast");
                
            }
            else {
                $("#dduser").animate({opacity: "show"}, "fast");
            }
        });
        
         $('#logado').countdown({since: -<?php echo $hlogin;?>, compact:true, format:'HMS'});
         <?php
        if($_GET['menu'] != "") {
            $cormenu = array('monitoria' => 'monitoria','calibragem' => 'calib','importa' => 'importa','resultados' => 'rela','semanal' => 'sema','contmonitor' => 'monitor','producao' => 'prod','selecao' => 'selecao');
            if($_GET['menu'] == "idmoni" OR $_GET['idreg']) {
                echo "$('#monitoria').css('background-color','#264BDE');\n";
                echo "$('#amonitoria').css('color','#FFF');\n";
            }
            else {
                echo "$('#".$cormenu[$_GET['menu']]."').css('background-color','#264BDE');\n";
                echo "$('#a".$cormenu[$_GET['menu']]."').css('color','#FFF');\n";
            }
        }
        else {
        }
        ?>
        $('li a').click(function() {
            var val = $(this).attr('id');
            var a = val.substr(1);
            $('li.admin').css('background-color','#264BDE');
            $('#'+a).css('background-color','#264BDE');
        });
        
        $("#logout").click(function() {
           $.post("/monitoria_supervisao/logout.php",function(ret) {
            window.location = '/monitoria_supervisao/index.php?msg='+ret;
           }); 
        });
    })
</script>
<?php
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
?>
<div style="width:1024px">
    <div id="divcabecalho" style="background-image: url(images/logo/clientis_<?php echo strtolower($_SESSION['nomecli']).".png)";?>">
        <ul id="cabecalho">
            <li style="text-align: left; font-size: 16px;margin: 5px;">
                <img name="user" src="/monitoria_supervisao/images/user.png" width="35px" height="35px" id="auser"/>
                <div id="dduser" class="divrodape" style="padding: 10px">
                    <span style="color: #F90; font-size: 13px; font-weight: bold">Informações do Usuário</span><br/><br/>
                    <span style="color: #F90; font-weight: bold">Nome:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $_SESSION['usuarioNome'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Email:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $eseluser['email'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Ult. Login:</span> <span style="color: #FFF;margin-left: 10px"><?php $data = banco2data($datalogin); echo $data." - ".$horalogin;?></span><br/>
                    <span style="color:#F90; font-weight: bold">Cliente:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $_SESSION['nomecli'];?></span><br/>
                    <div><div style="float:left"><span style="color:#F90; font-weight: bold">Logado: </span></div> <div id="logado" style="font-size:13px; color: #FFF; background-color: #000; width: 100px; float: left; margin-left: 10px"></div></div>
                </div>
            </li>
            <li style="text-align: center; font-size: 16px; width:48%; margin: 0px; margin:auto; text-align: center"><img src="/monitoria_supervisao/images/img_logo.jpg" width="70px" height="35px" /></li>
            <li style="text-align: right;"><img name="logout" id="logout" src="/monitoria_supervisao/images/sair.png" width="35px" height="35px" style=" border: 0px; padding: 0px" /></li>
        </ul>
    </div>
    <div style="float:right; width: 1024px; text-align: center">
    	<?php
        if($_SESSION['tipotopo'] == "") {
        }
        if($_SESSION['tipotopo'] == "BANNER") {
            ?>
            <img src="images/logo/<?php echo $_SESSION['imgbanner'];?>"alt="logo" />
            <?php
        }
        if($_SESSION['tipotopo'] == "ORIENTACAO") {
        }
        if($_SESSION['tipotopo'] == "META") {
        }
        ?>
    </div>
</div>
<div style="float:left; width:1024px; padding-bottom: 10px;z-index: 1">
    <br/>
    <ul id="web">
        <?php
    	$count = 0;
        $listareas = explode(",",$eseluser['areas']);
        foreach($listareas as $a) {
            if($a == "IMPORTACAO" && $eseluser['import'] == "N") {
            }
            else {
                $areas[] = $a;
            }
        }
        if($areas[0] == "") {
            $count = 0;
        }
        else {
            $count = count($areas);
        }
        if($count > 5) {
            $valor = "998";
        }
        if($count <= 5) {
            $valor = "1004";
        }
        $tam = $valor / $count;
        $style= "style=\"width:".floor($tam)."px; border-top-left-radius: 10px;border-bottom-right-radius: 10px;padding-top:7px\"";
        if($count == 0) {
            echo "<li id=\"admin\" style=\"width: 1010px; \"><strong>SEU PERFIL NÃƒO POSSUI AUTORIZAÇÃO PARA VISUALIZAÇÃO DE NENHUMA Ã�REA, FAVOR CONTATAR O ADMINISTRADOR</strong></li>";
        }
        else {
            foreach($areas as $area) {
                if($area == "RELATORIOS") {
                    echo "<a id=\"asema\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=semanal\"><li id=\"sema\" class=\"admin\" $style><strong>RELATÓRIOS</strong></li></a>";
                    //echo "<a id=\"arela\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=resultados\"><li id=\"rela\" class=\"admin\" $style><strong>RELATÓRIOS</strong></li></a>";
                }
                if($area == "MONITORIAS") {
                    echo "<a id=\"amonitoria\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=monitoria\"><li id=\"monitoria\" class=\"admin\" $style><strong>MONITORIA</strong></li></a>";
                }
                if($area == "PRODUCAO") {
                    echo "<a id=\"aprod\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=producao\"><li id=\"prod\" class=\"admin\" $style><strong>PRODUÇÃO</strong></li></a>";
                }
                if($area == "CALIBRAGEM") {
                    echo "<a id=\"acalib\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=calibragem\"><li id=\"calib\" class=\"admin\" $style><strong>CALIBRAGEM</strong></li></a>";
                }
                if($area == "IMPORTACAO") {
                    if($eseluser['import'] == "S") {
                        $_SESSION['import'] = "S";
                        echo "<a id=\"aimporta\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=importa\"><li id=\"importa\" class=\"admin\" $style><strong>IMPORTAÇÃO</strong></li></a>";
                    }
                    else {
                    }
                }
                if($area == "SELECAO") {
                    if($eseluser['selecaoaudios'] == "S") {
                        $_SESSION['selecao'] = "S";
                    }
                    echo "<a id=\"aselecao\" style=\"text-decoration:none; color:#FFF\" href=\"inicio.php?menu=selecao\"><li id=\"selecao\" class=\"admin\" $style><strong>SELEÇÃO</strong></li></a>";
                }
                if($area == "EBOOK") {
                    echo "<a id=\"aebook\" style=\"text-decoration:none; color:#FFF\" href=\"/monitoria_supervisao/monitor/ebook.php\" target=\"_blank\"><li id=\"ebook\" class=\"admin\" $style><strong>EBOOK</strong></li></a>";
                }
            }
        }
        ?>
    </ul>
</div>
<div id="continicio" class="corfd_pag">
<?php
$menu = $_GET['menu'];
switch ($menu) {
    case "resultados":
        if(!file_exists('users/resultados.php')) {
            include_once 'erro.php';
        }
        else {
            include_once "users/resultados.php";
        }
    break;
    
    case "semanal":
        if(!file_exists('users/semanal.php')) {
            include_once 'erro.php';
        }
        else {
            include_once "users/semanal.php";
        }
    break;

    case "monitoria":
        if(!file_exists('users/monitoria.php')) {
            include_once 'erro.php';
        }
        else {
            include_once "users/monitoria.php";
        }
    break;

    case "idmoni";
    $moni = new Planilha();
    $moni->iduser = $iduser;
    $moni->perfiluser = $_SESSION['usuarioperfil'];
    $moni->tabuser = $_SESSION['user_tabela'];
    $moni->pagina = "MONITORIA";
    $moni->DadosPlanVisualiza();
    $moni->MontaPlan_corpovisu();
    ?>
    <!--<script type="text/javascript" src="delaudiotmp.php?caminho=<?php //echo $moni->caminho;?>"></script>-->
    <?php
    break;

     case "idreg";
          if(!file_exists('users/registro.php')) {
               include_once 'erro.php';
          }
          else {
               include_once "users/registro.php";
          }
          break;

    case "grafico":
          if(!file_exists('users/grafico.php')) {
              include_once 'erro.php';
          }
          else {
              unset($_SESSION['varsconsult']);
              include_once "users/grafico.php";
          }
    break;

    case "calibragem":
        if($eseluser['calibragem'] == "S") {
            if(!file_exists('users/calibragem.php')) {
                include_once 'erro.php';
            }
            else {
                unset($_SESSION['varsconsult']);
                include_once "users/calibragem.php";
            }
        }
        if($eseluser['calibragem'] == "N") {
            if(!file_exists('users/relcalib.php')) {
                include_once 'erro.php';
            }
            else {
                unset($_SESSION['varsconsult']);
                include_once "users/relcalib.php";
            }
        }
    break;

    case "importa";
        if(!file_exists('admin/imp.php')) {
            include_once 'erro.php';
        }
        else {
            $_SESSION['varsconsult'];
            include_once "admin/imp.php";
        }
    break;

    case "selecao":
        if(!file_exists('admin/selecao.php')) {
            include_once 'erro.php';
        }
        else {
            unset($_SESSION['varsconsult']);
            include_once "admin/selecao.php";
        }
        break;

    case "producao";
        if(!file_exists('users/producao.php')) {
            include_once 'erro.php';
        }
        else {
            include_once "users/producao.php";
        }
    break;
}
?>
</div>