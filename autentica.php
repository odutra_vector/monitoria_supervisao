<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
$ip = $_SERVER['REMOTE_ADDR'];

if(!file_exists('config/conexao.php')) {
    if(file_exists('config/indexmod.php')) {
        $msg = "O sistema está inconsistente, arquivos essenciais ao seu funcionamento foram apagados, favor contatar a área Comercial!!!";
        header("location:index.php?msg=".$msg);
    }
    else {
        $msg = "A configuração inicial do sistema não foi executada, favor seguir passos de inicialização do sistema!!!";
        header("location:config/index.php?msg=".$msg);
    }
}
else {

    // Inclui o arquivo com o sistema de seguranÃ§a
    include_once("seguranca.php");
    comandossql();
    include_once('config/conexao.php');
    include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
    include_once'securimage/securimage.php';
    $img = new Securimage();

    // Verifica se um formulÃ¡rio foi enviado

    if(isset($_POST['logar'])) {
        if ($_SERVER['REQUEST_METHOD']=='POST') {
            // Salva duas variÃ¡veis com o que foi digitado no formulÃ¡rio
            // Detalhe: faz uma verificaÃ§Ã£oo com isset() pra saber se o campo foi preenchido
            $usuario = (isset($_POST["user"])) ? $_POST["user"] : '';
            $senha = (isset($_POST["senha"])) ? $_POST["senha"] : '';

                // Utiliza uma função criada no seguranca.php pra validar os dados digitados
                if (validaUsuario($usuario, $senha) == true) {

                    $_SESSION['sessionid'] = session_id();
                    $data = date('Y-m-d');
                    $hora = date('H:i:s');
                    $_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
                    $ip = $_SESSION['ip'];

                    // O usuÃ¡rio e a senha digitados foram validados, manda pra pÃ¡gina interna
                    if(count($_SESSION['userbancos']) > 1) {
                      $msg = "Login efetuado, bem vindo! Favor selecionar o cliente.";
                      header("location:selcliente.php?msg=".$msg);
                    }
                    if(count($_SESSION['userbancos']) == 1) {
                        // verificação de monitorias inconsistentes e exclusão
                        foreach($_SESSION['userbancos'] as $bd => $tab) {
                            $seldb = $_SESSION['seldb']($bd);
                            $verifmoni = "SELECT m.idmonitoria, m.idfila, m.tabfila FROM monitoria m
                                         WHERE m.idmonitoria NOT IN (SELECT DISTINCT(idmonitoria) FROM moniavalia)";
                            $everif = $_SESSION['query']($verifmoni) or die ("erro na query para consultar monitorias inconsistentes");
                            $nverif = $_SESSION['num_rows']($everif);
                            if($nverif >= 1) {
                                while($lverif = $_SESSION['fetch_array']($everif)) {
                                    $delmoni = "DELETE FROM monitoria WHERE idmonitoria='".$lverif['idmonitoria']."'";
                                    $edel = $_SESSION['query']($delmoni) or die ("erro na query que apaga monitorias inconsistentes");
                                    $atufila = "UPDATE ".$lverif['tabfila']." SET monitorando='0', monitorado='0' AND idmonitor=NULL WHERE id".$lverif['tabfila']."='".$lverif['idfila']."'";
                                    $eatufila = $_SESSION['query']($atufila) or die ("erro na query para atualizar a fila");
                                }
                            }
                        }

                        atualizafluxo($_SESSION['user_tabela'],$_SESSION['usuarioID']);

                        $id_user = $_SESSION['usuarioID'];
                        $nome_user = $_SESSION['usuarioNome'];
                        if($_SESSION['ass'] == "S") {
                                if($_SESSION['assuser'] == "N" OR $_SESSION['assuser'] == "") {
                                  header("location:ass.php");
                                }
                                if($_SESSION['assuser'] == "S") {
                                    if($_SESSION['user_tabela'] == "monitor") {
                                        $horamoni = "SELECT * FROM monitor WHERE idmonitor='$id_user'";
                                        $ehora = $_SESSION['fetch_array']($_SESSION['query']($horamoni)) or die ("erro na query de consulta do horÃ¡rio do monitor");
                                        if($ehora['entrada'] > $hora OR $ehora['saida'] < $hora) {
                                            unsetSESSION();
                                            header("location:index.php?msg=VOCÊ NÃO POSSUI AUTORIZAÇÃO PARA INICIAR OS TRABALHOS FORA DO SEU HORÁRIO!!!");
                                        }
                                        if($ehora['status'] != "OFFLINE") {
                                            unsetSESSION();
                                            header("location:index.php?msg=O MONITOR JÁ ESTÃO LOGADO EM SISTEMA, FAVOR VERIFICAR COM A SUPERVISÃO!!!");
                                        }
                                        else {
                                            $countlog = "SELECT COUNT(*) as result FROM moni_login WHERE idmonitor='$id_user'";
                                            $ecountlog = $_SESSION['fetch_array']($_SESSION['query']($countlog)) or die ("erro na query de contagem dos logs do usuÃ¡rio");
                                            if($ecountlog['result'] >= 1) {
                                              $sellog = "SELECT * FROM moni_login WHERE idmonitor='$id_user' ORDER BY data DESC, login DESC, logout DESC LIMIT 1";
                                              $esellog = $_SESSION['fetch_array']($_SESSION['query']($sellog)) or die ("erro na query de verificaÃ§Ã£o de log");
                                              if($esellog['logout'] == "00:00:00" OR $esellog['logout'] == "") {
                                                $horafim = $ehora['saida'];
                                                $horaini = $esellog['login'];
                                                $verifdt = "SELECT '".$esellog['data']."' < '$data' as result";
                                                $everifdt = $_SESSION['fetch_array']($_SESSION['query']($verifdt));
                                                if($everifdt['result'] == 1) {
                                                    $calctempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$horafim') - TIME_TO_SEC('$horaini')) as result";
                                                }
                                                else {
                                                    $calctempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$hora') - TIME_TO_SEC('$horaini')) as result";
                                                }
                                                $ecalctempo = $_SESSION['fetch_array']($_SESSION['query']($calctempo)) or die ("erro na query de contagem do tempo de login");
                                                $atureg = "UPDATE moni_login SET logout='$horafim', tempo='".$ecalctempo['result']."' WHERE idmoni_login='".$esellog['idmoni_login']."'";
                                                $eatureg = $_SESSION['query']($atureg) or die ("erro na query de atualizaÃ§Ã£o do log");
                                              }
                                            }
                                            $tabs = array('monitor','moni_login','fila_grava','fila_oper');
                                            $registro = $_SESSION['query']("INSERT INTO moni_login (idmonitor, ip, data, login) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')");
                                            $status = $_SESSION['query']("UPDATE monitor SET status='AGUARDANDO', hstatus='$hora' WHERE idmonitor='$id_user'");
                                            $filas = array('fila_oper' => 'fo','fila_grava' => 'fg');
                                            foreach($filas as $kf => $f) {
                                                $upfila = "UPDATE $kf SET monitorando='0', idmonitor=NULL WHERE idmonitor='".$_SESSION['usuarioID']."' AND monitorando='0' AND monitorado='0'";
                                                $eup = $_SESSION['query']($upfila) or die (mysql_error());
                                                $upfila = "UPDATE $kf SET monitorando='0', idmonitor=NULL WHERE idmonitor='".$_SESSION['usuarioID']."' AND monitorando='1' AND monitorado='0'";
                                                $eup = $_SESSION['query']($upfila) or die (mysql_error());
                                            }
                                            if($_SESSION['confapres'][0] == "") {
                                                header("location:inicio.php");
                                            }
                                            else {
                                                header("location:apresentacao.php");
                                            }
                                        }
                                    }
                                    if($_SESSION['user_tabela'] == "user_web") {
                                        $registro = $_SESSION['query']("INSERT INTO login_web (iduser_web, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuÃ¡rio

                                        if($_SESSION['confapres'][0] == "") {
                                            header("location:inicio.php");
                                        }
                                        else {
                                            header("location:apresentacao.php");
                                        }
                                    }
                                    if($_SESSION['user_tabela'] == "user_adm") {
                                        $registro = $_SESSION['query']("INSERT INTO login_adm (iduser_adm, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuÃ¡rio
                                        if($_SESSION['confapres'][0] == "") {
                                            header("location:inicio.php?session=".$_SESSION['sessionid']);
                                        }
                                        else {
                                            header("location:apresentacao.php?session=".$_SESSION['sessionid']);
                                        }
                                    }
                                }
                        }
                        if($_SESSION['ass'] == "N") {
                            if($_SESSION['user_tabela'] == "monitor") {
                                $horamoni = "SELECT * FROM monitor WHERE idmonitor='$id_user'";
                                $ehora = $_SESSION['fetch_array']($_SESSION['query']($horamoni)) or die ("erro na query de consulta do horário do monitor");
                                if($ehora['entrada'] > $hora OR $ehora['saida'] < $hora) {
                                    unsetSESSION();
                                    header("location:index.php?msg=VOCÊ NÃO POSSUI AUTORIZAÇÃO PARA INICIAR OS TRABALHOS FORA DO SEU HORÁRIO!!!");
                                }
                                else {
                                    $countlog = "SELECT COUNT(*) as result FROM moni_login WHERE idmonitor='$id_user'";
                                    $ecountlog = $_SESSION['fetch_array']($_SESSION['query']($countlog)) or die ("erro na query de contagem dos logs do usuÃ¡rio");
                                    if($ecountlog['result'] >= 1) {
                                      $sellog = "SELECT * FROM moni_login WHERE idmonitor='$id_user' ORDER BY data DESC, login DESC, logout DESC LIMIT 1";
                                      $esellog = $_SESSION['fetch_array']($_SESSION['query']($sellog)) or die ("erro na query de verificaÃ§Ã£o de log");
                                      if($esellog['logout'] == "00:00:00" OR $esellog['logout'] == "") {
                                        $horafim = $ehora['saida'];
                                        $horaini = $esellog['login'];
                                        $verifdt = "SELECT '".$esellog['data']."' < '$data' as result";
                                        $everifdt = $_SESSION['fetch_array']($_SESSION['query']($verifdt));
                                        if($everifdt['result'] == 1) {
                                            $horafimatu = $ehora['saida'];
                                            $calctempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$horafim') - TIME_TO_SEC('$horaini')) as result";
                                        }
                                        else {
                                            $horafimatu = $hora;
                                            $calctempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$hora') - TIME_TO_SEC('$horaini')) as result";
                                        }
                                        $ecalctempo = $_SESSION['fetch_array']($_SESSION['query']($calctempo)) or die ("erro na query de contagem do tempo de login");
                                        $atureg = "UPDATE moni_login SET logout='$horafimatu', tempo='".$ecalctempo['result']."' WHERE idmoni_login='".$esellog['idmoni_login']."'";
                                        $eatureg = $_SESSION['query']($atureg) or die ("erro na query de atualizaÃ§Ã£o do log");
                                      }
                                    }
                                    $registro = "INSERT INTO moni_login (idmonitor, ip, data, login) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')";
                                    $eregistro = $_SESSION['query']($registro) or die ("erro na query para inserir o login do usuÃ¡rio");
                                    $status = $_SESSION['query']("UPDATE monitor SET status='AGUARDANDO', hstatus='$hora' WHERE idmonitor='$id_user'");

                                    $filas = array('fila_grava' => 'fg','fila_oper' => 'fo');
                                    foreach($filas as $kf => $f) {
                                        $upfila = "UPDATE $kf SET monitorando='0', idmonitor=NULL WHERE idmonitor='".$_SESSION['usuarioID']."' AND monitorando='0' AND monitorado='0'";
                                        $eup = $_SESSION['query']($upfila) or die (mysql_error());
                                        $upfila = "UPDATE $kf SET monitorando='0', idmonitor=NULL WHERE idmonitor='".$_SESSION['usuarioID']."' AND monitorando='1' AND monitorado='0'";
                                        $eup = $_SESSION['query']($upfila) or die (mysql_error());
                                    }

                                    if($_SESSION['confapres'][0] == "") {
                                        header("location:inicio.php");
                                    }
                                    else {
                                        header("location:apresentacao.php");
                                    }
                                }
                            }
                            if($_SESSION['user_tabela'] == "user_web") {
                                $registro = $_SESSION['query']("INSERT INTO login_web (iduser_web, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuÃ¡rio
                                if($_SESSION['confapres'][0] == "") {
                                    header("location:inicio.php");
                                }
                                else {
                                    header("location:apresentacao.php");
                                }
                            }
                            if($_SESSION['user_tabela'] == "user_adm") {
                                $registro = $_SESSION['query']("INSERT INTO login_adm (iduser_adm, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuÃ¡rio
                                if($_SESSION['confapres'][0] == "") {
                                    header("location:inicio.php");
                                }
                                else {
                                    header("location:apresentacao.php");
                                }
                            }
                        }
                    }
                }
                else {
                    // O usuÃ¡rio e/ou a senha sÃ£o invÃ¡lidos, manda de volta pro form de login
                    // Para alterar o endereÃ§o da pÃ¡gina de login, verifique o arquivo seguranca.php
                    expulsaVisitante();
                }
        }
    }

    if(isset($_POST['altsenha'])) {
        header("location:altsenha.php");
    }

    if(isset($_POST['ok'])) {
        if($_POST['cliente'] == "") {
            $msg = "Favor selecionar um cliente para acessar o sistema!!!";
            header("location:selcliente.php?msg=".$msg);
        }
        else {
            if(isset($_POST['cliente'])) {
                $part = explode("-",$_POST['cliente']);
                $tabela = $part[1];
                $list[] = $part[0];
                $_SESSION['selbanco'] = $part[0]; // cria session do cliente que o usuÃ¡rio selecionou acesso
            }
            else {
                $_SESSION['selbanco'] = $_SESSION['selbanco'];
            }
            foreach($_SESSION['userbancos'] as $bd => $tab) {
                $seldb = $_SESSION['seldb']($bd);
                $verifmoni = "SELECT m.idmonitoria, m.idfila, m.tabfila FROM monitoria m
                             WHERE m.idmonitoria NOT IN (SELECT DISTINCT(idmonitoria) FROM moniavalia)";
                $everif = $_SESSION['query']($verifmoni) or die ("erro na query para consultar monitorias inconsistentes");
                $nverif = $_SESSION['num_rows']($everif);
                if($nverif >= 1) {
                    while($lverif = $_SESSION['fetch_array']($everif)) {
                        $delmoni = "DELETE FROM monitoria WHERE idmonitoria='".$lverif['idmonitoria']."'";
                        $edel = $_SESSION['query']($delmoni) or die ("erro na query que apaga monitorias inconsistentes");
                        $atufila = "UPDATE ".$lverif['tabfila']." SET monitorando='0', monitorado='0' AND idmonitor=NULL WHERE id".$lverif['tabfila']."='".$lverif['idfila']."'";
                        $eatufila = $_SESSION['query']($atufila) or die ("erro na query para atualizar a fila");
                    }
                }
            }
            $_SESSION['sessionid'] = session_id();
            $_SESSION['userbancos'] = array($part[0] => 0);
            include_once('config/conexao.php');
            $selbanco = $_SESSION['seldb']($_SESSION['selbanco']);
            $_SESSION['user_tabela'] = $tabela;
            if($tabela == "user_adm" OR $tabela == "user_web") {
                $aliasperf = explode("_",$tabela);
                $aliasperf = $aliasperf[1];
            }
            else {
                $aliasperf = $tabela;
            }
            $dadoscli = "SELECT * FROM dados_cli WHERE nomedados_cli='".str_replace("monitoria_","", $_SESSION['selbanco'])."'";
            $edadoscli = $_SESSION['fetch_array']($_SESSION['query']($dadoscli)) or die ("erro na query para obter as configuraÃ§Ãµes do cliente");
            $_SESSION['idcli'] = $edadoscli['iddados_cli'];
            $_SESSION['nomecli'] = $edadoscli['nomedados_cli'];
            $_SESSION['site'] = $edadoscli['site'];
            $_SESSION['imagem'] = $edadoscli['imagem'];
            $_SESSION['ass'] = $edadoscli['assdigital'];
            $_SESSION['tipotopo'] = $edadoscli['tipotopo'];
            $_SESSION['imgbanner'] = $edadoscli['imgbanner'];
            $seluser = "SELECT * FROM $tabela WHERE usuario='".$_SESSION['usuarioLogin']."' AND senha='".$_SESSION['usuarioSenha']."'";
            $myquery = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta dos dados do usuÃ¡rio");
            $_SESSION['usuarioID'] =  $myquery['id'.$tabela]; // Pega o valor da coluna 'id do registro encontrado no MySQL
            $_SESSION['usuarioNome'] = $myquery['nome'.$tabela]; // Pega o valor da coluna 'nome' do registro encontrado no MySQL
            $_SESSION['usuarioemail'] = $myquery['email']; //Pega o valor da coluna e-mail do Mysql
            $_SESSION['usuarioperfil'] = $myquery['idperfil_'.$aliasperf]; //Pega o valor da coluna perfil do Mysql
            $_SESSION['hlogin'] = date('H:i:s');
            $_SESSION['tmonitor'] = $myquery['tmonitor'];
            $_SESSION['confapres'] = explode(",",$myquery['confapres']);
            $_SESSION['participante'] = $_SESSION['usuarioID']."-".$_SESSION['user_tabela'];
            $_SESSION['assuser'] = $myquery['ass'];
            $_SESSION['calibragem'] = $myquery['calibragem'];
            $selcoloper = "SELECT MIN(idcoluna_oper), COUNT(*) as result, nomecoluna FROM coluna_oper WHERE incorpora='OPERADOR'";
            $eselcoloper = $_SESSION['fetch_array']($_SESSION['query']($selcoloper)) or die ("erro na consulta da coluna de identificaÃ§Ã£o do operador");
            if($eselcoloper['result'] >= 1) {
                $_SESSION['operador'] = $eselcoloper['nomecoluna'];
            }
            else {
                $_SESSION['operador'] = "";
            }
            $selcolsuper = "SELECT MIN(idcoluna_oper), COUNT(*) as result, nomecoluna FROM coluna_oper WHERE incorpora='SUPER_OPER'";
            $eselcolsuper = $_SESSION['fetch_array']($_SESSION['query']($selcolsuper)) or die ("erro na consulta da coluna de identificaÃ§Ã£o do operador");
            if($eselcolsuper['result'] >= 1) {
                 $_SESSION['super'] = $eselcolsuper['nomecoluna'];
            }
            else {
                $_SESSION['super'] = "";
            }
            $id_user = $_SESSION['usuarioID'];
            $nome_user = $_SESSION['usuarioNome'];
            $data = date('Y-m-d');
            $hora = date('H:i:s');
            atualizafluxo($_SESSION['user_tabela'],$_SESSION['usuarioID']);

            if($_SESSION['user_tabela'] == "monitor") {
              $registro = "INSERT INTO moni_login (idmonitor, ip, data, login) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')";
              $ereg = $_SESSION['query']($registro) or die ("erro na query de inserÃ§Ã£o do login");
              $status = "UPDATE monitor SET status='AGUARDANDO', hstatus='".date('H:i:s')."' WHERE idmonitor='$id_user'";
              $estatus = $_SESSION['query']($status) or die ("erro na query de inserÃ§Ã£o do status");
            }
            if($_SESSION['user_tabela'] == "user_web") {
              $registro = $_SESSION['query']("INSERT INTO login_web (iduser_web, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuÃ¡rio
            }
            if($_SESSION['user_tabela'] == "user_adm") {
              $registro = $_SESSION['query']("INSERT INTO login_adm (iduser_adm, ip, data, hora) VALUES ('$id_user', '".$_SESSION['ip']."', '$data', '$hora')") or die (mysql_error()); //registra acesso do usuÃ¡rio
            }
            if($_SESSION['confapres'][0] != "") {
                header("location:apresentacao.php");
            }
            else {
                header("location:inicio.php");
            }
        }
    }

    if(isset($_POST['cancel'])) {
        header("location:index.php");
    }
}
?>
