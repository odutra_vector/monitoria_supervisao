<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once('relmoni.class.php');
include_once('inner.class.php');

class Grafico {
    
    /* Gráfico */

    public $tipograf;
    public $backgroundColor;
    public $corlabel;
    public $margin;
    public $heigth;
    public $width;
    public $render;

    /*geral*/
    public $sql;
    public $idrel;
    public $seriesnome;
    public $seriescor;
    public $seriesdados;
    public $seriestack;
    public $seriestype;
    public $categorias;
    public $stack;
    public $titulo;
    public $max_y;
    public $pointinter_y;
    public $pointwidth;
    public $subtitulo;
    public $tituloX;
    public $yaxis;
    public $tituloY;
    public $labelsX;
    public $labelsXrotation;
    public $labelsY;
    public $labelsYrotation;

    /*legenda*/
    public $legend;
    public $layoutlegend;
    public $colorlegend;
    public $alignlegend;
    public $verticalalign;
    public $legendX;
    public $legendY;

    /*visualiza dados*/
    public $aprestooltip;
    public $returntool;
    public $dataLabels;

    function Dados($sql, $idrel, $colunas) {
        $this->sql = $sql;
        $this->idrel = $idrel;
        $selrel = "SELECT * FROM relmoni r INNER JOIN grafrelmoni g ON g.idrelmoni = r.idrelmoni WHERE r.idrelmoni='$idrel'";
        $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relatório");
        $groupby = $eselrel['agrupa'];
        $this->tipograf = $eselrel['tipograf'];
        $this->dataLabels = $eselrel['valores'];
        $this->backgroundColor = $eselrel['background'];
        $this->corlabel = $eselrel['cordados'];
        $this->pointwidth = $eselrel['pointwidth'];
        $this->margin = $eselrel['margin'];
        if($eselrel['marginbottom'] == "") {
            $this->marginbottom = "100";
        }
        else {
            $this->marginbottom = $eselrel['marginbottom'];
        }
        $this->heigth = $eselrel['heigth'];
        $this->width = $eselrel['width'];
        $this->aprestooltip = "SIM";
        $this->max_y = $eselrel['maxpoint_y'];
        $this->pointinter_y = $eselrel['interpoint_y'];
        $this->labelsXrotation = "-".$eselrel['rotacaocat'];
        if($eselrel['legenda'] == "SIM") {
            $this->legend = true;
        }
        else {
            $this->legend = false;
        }
        $this->colorlegend = $eselrel['bgcolorleg'];
        if($eselrel['direcaoleg'] == "H") {
            $this->layoutlegend = "horizontal";
        }
        if($eselrel['direcaoleg'] == "V") {
            $this->layoutlegend = "vertical";
        }
        if($eselrel['posicaoleg_x'] == "") {
            $posileg_x = "''";
        }
        else {
            $posileg_x = $eselrel['posicaoleg_x'];
        }
        if($eselrel['posicaoleg_y'] == "") {
            $posileg_y = "''";
        }
        else {
            $posileg_y = $eselrel['posicaoleg_y'];
        }
        $this->legendX = $posileg_x;
        $this->legendY = $posileg_y;
        $selcolgraf = "SELECT * FROM relmonicol r INNER JOIN grafrelmoni g ON g.idrelmoni = r.idrelmoni WHERE g.idrelmoni='$idrel' AND serie IS NOT NULL";
        $ecolgraf = $_SESSION['query']($selcolgraf) or die ("erro na query de consulta das colunas do gráfico");
        while($lcolgraf = $_SESSION['fetch_array']($ecolgraf)) {
            $colgraf[$lcolgraf['nomeapresenta']] = $lcolgraf['tipo'];
            $this->tituloY[] = $lcolgraf['nomeapresenta'];
            $this->seriesnome[$lcolgraf['nomeapresenta']] = $lcolgraf['nomeapresenta'];
            $this->seriescor[$lcolgraf['nomeapresenta']] = $lcolgraf['cor'];
            $this->seriestype[$lcolgraf['nomeapresenta']] = $lcolgraf['grafico'];
        }

        $this->titulo = $eselrel['nomerelmoni'];
        $dadosquery = $_SESSION['query']($sql) or die ("erro na query de consulta do relatório");
        while($ldados = $_SESSION['fetch_array']($dadosquery)) {
            foreach($colunas as $kdados => $dados) {
                foreach($dados as $tipo) {
                    foreach($tipo as $colapres => $colquery) {
                        if($colapres == $groupby) {
                        }
                        else {
                            if($this->tipograf == "pie") {
                                if($kdados == 1) {
                                    $this->categorias[] = $ldados[$colapres];
                                }
                                else {
                                    if(array_key_exists($colapres,$colgraf)) {
                                        if($colapres == "PERCENTUAL") {
                                            $this->seriesdados[$colapres][] = round($ldados[$colapres],2);
                                        }
                                        else {
                                            $this->seriesdados[$colapres][] = "$ldados[$colapres]";
                                        }
                                    }
                                    else {
                                    }
                                }
                            }
                            else {
                                if($kdados == 1) {
                                    $categorias[] = $ldados[$colapres];
                                }
                                else {
                                    if(array_key_exists($colapres,$colgraf)) {
                                        if($colapres == "PERCENTUAL") {
                                            $this->seriesdados[$colapres][] = round($ldados[$colapres],2);
                                        }
                                        else {
                                            $this->seriesdados[$colapres][] = $ldados[$colapres];
                                        }
                                    }
                                    else {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if($this->tipograf == "pie") {
        }
        else {
            $this->categorias = implode("','",$categorias);
        }
        $this->subtitulo = implode(", ",$this->seriesnome);
    }

    function tooltip () {
        if($this->tipograf == "pie") {
             $this->returntool = "plotOptions: { $this->tipograf: { allowPointSelect: true, cursor: 'pointer', dataLabels: { enabled: true, formatter: function() { return '<b>'+ this.point.name +'</b>: '+ this.y;
 } } } },";
        }
        else {
            if($this->tipograf == "bar" OR $this->tipograf == "column" OR $this->tipograf == "line" OR $this->tipograf == "spline") {
                if($this->stack == "S") {
                    $this-> returntool = "plotOptions: { $this->tipograf: { stacking: 'normal' } },";
                }
                else {
                    $this-> returntool = "plotOptions: { $this->tipograf: { dataLabels: { enabled: true } } },";
                }
            }
        }
        echo $this->returntool;
    }

    function geragrafico ($filtros,$colunas) {
        echo "<script type=\"text/javascript\">\n";
        echo "var chart;\n";
        echo "$(document).ready(function() {\n";
        echo "chart = new Highcharts.Chart({\n";
        echo "chart: {\n";
        echo "renderTo: '$this->render',\n";
        echo "backgroundColor: '#".$this->backgroundColor."',\n";
        echo "align:	'center',\n";
        echo "margin: ".$this->margin.",\n";
        if($this->marginbottom == "") {
            $margembase = "";
        }
        else {
            $margembase = "marginBottom: $this->marginbottom,";
        }
        echo "marginLeft: 100, marginRight: 100,$margembase\n";
        echo "width: '".$this->width."',\n";
        echo "height: '".$this->heigth."'\n";
        echo "},\n";
        echo "title: {\n";
        echo "text:'".$this->titulo."'\n";
        echo "},";
        echo "subtitle: {\n";
        echo "text:'".$this->subtitulo."'\n";
        echo "},\n";
        if($this->tipograf == "pie") {

        }
        else {
            echo "xAxis: { categories: ['".$this->categorias."'], labels: {rotation: '".$this->labelsXrotation."', align: 'right', style: {font: 'normal 13px Verdana, sans-serif'}}},\n";
        }
        if($this->yaxis != "N") {
            echo "yAxis: [{min: 0, ";
            if(is_array($this->tituloY) && count($this->tituloY) >= 2) {
                $ctit = count($this->tituloY);
                $t = 0;
                foreach($this->tituloY as $titulo) {
                    $t++;
                    if($t != $ctit) {
                        echo "title: { text: '".strtoupper($titulo)."'}},";
                    }
                    else {
                        echo "{ title: { text: '".strtoupper($titulo)."'}, opposite: true, min: 0";
                    }
                }
            }
            else {
                echo "title: { text: '".$this->tituloY."' }";
            }
            echo "}],\n";
        }
        else {
            echo "yAxis: [{ title: { text: '' } }],\n";
        }
        if($this->legend == "true") {
            echo "legend: {layout: '".$this->layoutlegend."', backgroundColor: '#".$this->colorlegend."', align: '".$this->alignlegend."', verticalAlign: '".$this->verticalalign."', x: ".$this->legendX.", y: ".$this->legendY."},";
        }
        else {
            echo "legend: { enabled: false },";
        }
        if($this->tipograf == "pie") {
            echo "tooltip: { formatter: function() { return this.point.name +': '+ this.y;}},\n";
        }
        else {
            echo "tooltip: { formatter: function() { return this.series.name +': '+ this.y;}},\n";
        }
        if($this->aprestooltip == "SIM" OR $this->stack == "S") {
            $this->tooltip();
        }
        else {
        }
        echo "series: [";
        $i = 0;
        if($this->tipograf == "pie") {
            $countcat = count($this->seriesnome);
            foreach($this->seriesnome as $labeldados) {
                $i++;
                echo "{";
                    echo "type: '".$this->seriestype[$labeldados]."',";
                    echo "name: '".$labeldados."',";
                    //echo "color: '#".$this->seriescor[$labeldados]."',";
                    if($i == 1) {
                        echo "size: '45%',";
                        echo "innersize: '20%',";
                    }
                    else {
                        echo "innersize: '45%',";
                    }
                    echo "data: [";
                    foreach($this->seriesdados as $ndados => $dadospie) {
                        if($ndados == $labeldados) {
                            $cdados = count($dadospie);
                            $ic = 0;
                            $cores = array('9EFFA1','FF4747','A6C9FF');
                            for(;list($kcat,$dcat) = each($this->categorias), list($kserie,$dserie) = each($dadospie), list(,$cor) = each($cores);) {
                                $ic++;
                                echo "{name: '$dcat',";
                                echo "y: $dserie,";
                                if($ic < $cdados) {
                                    echo "color: '#$cor'},";
                                }
                                else {
                                    echo "color: '#$cor'}";
                                }
                            }
                            reset($this->categorias);
                        }
                        else {
                        }
                    }
                    echo "],";
                    if($i == 1) {
                        echo "dataLabels: { enabled: false}";
                    }
                    else {
                        echo "dataLabels: { enabled: true}";
                    }
                    if($i < $countcat) {
                        echo "},";
                    }
                    else {
                        echo "}";
                    }
            }
        }
        else {
            $countseries = count($this->seriesdados);
            if($filtros == "") {
                foreach($this->seriesdados as $ndados => $sdados) {
                    $i++;
                    echo "{";
                    echo "name: '".$this->seriesnome[$ndados]."',";
                    echo "color: '#".$this->seriescor[$ndados]."',";
                    echo "type: '".$this->seriestype[$ndados]."',";
                    echo "stack: '".$this->seriestack[$ndados]."',";
                    if($this->pointwidth != "") {
                        if($this->seriestype[$ndados] == "bar" OR $this->seriestype[$ndados] == "column") {
                            echo "pointWidth: $this->pointwidth,";
                        }
                        if($this->seriestype[$ndados] == "line" OR $this->seriestype[$ndados] == "spline") {
                            $line = substr($this->pointwidth, 0, 1);
                            echo "lineWidth: $line,";
                        }
                    }
                    if($this->yaxis != "N" && count($this->tituloY) >= 2) {
                        $tituloY = "yAxis: ".array_search($ndados, $this->tituloY).",";
                    }
                    else {
                        $tituloY = "";
                    }
                    echo "data: [".implode(",",$sdados)."], $tituloY";
                    if($this->dataLabels == "S") {
                        echo "dataLabels: { enabled: true, x: 3, y: -10, color: '#$this->corlabel' }";
                    }
                    else {
                    }
                    if($i < $countseries) {
                        echo "},";
                    }
                    else {
                        echo "}";
                    }
                }
            }
            else {
                $countseries = count($filtros) * (count($colunas) - 1);
                foreach($this->seriesdados as $freq => $filtros) {
                    foreach($filtros as $kdados => $ndados) {
                        $i++;
                        ?>
                        {name:'<?php echo $this->seriesnome[$freq][$kdados];?>',
                        type:'<?php echo $this->seriestype[$freq][$kdados];?>',
                        color:'#<?php echo $this->seriescor[$freq][$kdados];?>',
                        <?php
                        if($this->pointwidth != "") {
                            if($this->seriestype[$freq][$kdados] == "bar" OR $this->seriestype[$freq][$kdados] == "column") {
                                echo "pointWidth: $this->pointwidth,";
                            }
                            if($this->seriestype[$freq][$kdados] == "line" OR $this->seriestype[$freq][$kdados] == "spline") {
                                $line = substr($this->pointwidth, 0, -1);
                                echo "lineWidth: $line,";
                            }
                        }
                        ?>
                        <?php
                        if($this->yaxis == "N" && count($this->tituloY) < 2) {
                            $tituloY = "";
                        }
                        else {
                            $tituloY = "yAxis: ".array_search($kdados, $this->tituloY).",";
                        }
                        if($this->seriestack[$freq][$kdados] == "") {
                        }
                        else {
                            ?>
                            stack:'<?php echo $this->seriestack[$freq][$kdados];?>',
                            <?php
                        }
                        ?>
                        data: [<?php echo implode(",",$ndados);?>], <?php echo $tituloY;?> <?php if($this->dataLabels == "S") { ?> dataLabels: { enabled: true, x: 3, y: -10, color: '<?php echo "#$this->corlabel";?>' } <?php } else {}?>
                        <?php
                        if($i < $countseries) {
                            echo "},";
                        }
                        else {
                            echo "}";
                        }
                    }
                }
            }
        }
        echo "]";
        echo "});";
        echo "});";
        echo "</script>";
    }

    function visudados($filtros,$colunas) {
        echo "var chart;\n";
        echo "$(document).ready(function() {\n";
        echo "chart = new Highcharts.Chart({\n";
        echo "chart: {\n";
        echo "renderTo: '$this->render',\n";
        echo "backgroundColor: '#".$this->backgroundColor."',\n";
        echo "align:	'center',\n";
        echo "margin: ".$this->margin.",\n";
        echo "marginLeft: 100, marginRight: 100,";
        echo "width: '".$this->width."',\n";
        echo "height: '".$this->heigth."'\n";
        echo "},\n";
        echo "title: {\n";
        echo "text:'".$this->titulo."'\n";
        echo "},";
        echo "subtitle: {\n";
        echo "text:'".$this->subtitulo."'\n";
        echo "},\n";
        if($this->tipograf == "pie") {

        }
        else {
            echo "xAxis: { categories: ['".$this->categorias."'], labels: {rotation: '".$this->labelsXrotation."', align: 'right', style: {font: 'normal 13px Verdana, sans-serif'}}},\n";
        }
        if($this->yaxis != "N") {
            echo "yAxis: [{min: 0,";
            if(is_array($this->tituloY) && count($this->tituloY) >= 2) {
                $ctit = count($this->tituloY);
                $t = 0;
                foreach($this->tituloY as $titulo) {
                    $t++;
                    if($t != $ctit) {
                        echo "title: { text: '".strtoupper($titulo)."'}},";
                    }
                    else {
                        echo "{ title: { text: '".strtoupper($titulo)."'}, opposite: true, min: 0";
                    }
                }
            }
            else {
                echo "title: { text: '".$this->tituloY."' }";
            }
            echo "}],\n";
        }
        else {
            
        }
        if($this->legend == "true") {
            echo "legend: {layout: '".$this->layoutlegend."', backgroundColor: '#".$this->colorlegend."', align: '".$this->alignlegend."', verticalAlign: '".$this->verticalalign."', x: ".$this->legendX.", y: ".$this->legendY."},";
        }
        else {
            echo "legend: { enabled: false },";
        }
        if($this->tipograf == "pie") {
            echo "tooltip: { formatter: function() { return this.point.name +': '+ this.y;}},\n";
        }
        else {
            echo "tooltip: { formatter: function() { return this.series.name +': '+ this.y;}},\n";
        }
        if($this->aprestooltip == "SIM" OR $this->stack == "S") {
            $this->tooltip();
        }
        else {
        }
        echo "series: [";
        $i = 0;
        if($this->tipograf == "pie") {
            $countcat = count($this->seriesnome);
            foreach($this->seriesnome as $labeldados) {
                $i++;
                echo "{";
                    echo "type: '".$this->seriestype[$labeldados]."',";
                    echo "name: '".$labeldados."',";
                    //echo "color: '#".$this->seriescor[$labeldados]."',";
                    if($i == 1) {
                        echo "size: '45%',";
                        echo "innersize: '20%',";
                    }
                    else {
                        echo "innersize: '45%',";
                    }
                    echo "data: [";
                    foreach($this->seriesdados as $ndados => $dadospie) {
                        if($ndados == $labeldados) {
                            $cdados = count($dadospie);
                            $ic = 0;
                            $cores = array('9EFFA1','FF4747','A6C9FF');
                            for(;list($kcat,$dcat) = each($this->categorias), list($kserie,$dserie) = each($dadospie), list(,$cor) = each($cores);) {
                                $ic++;
                                echo "{name: '$dcat',";
                                echo "y: $dserie,";
                                if($ic < $cdados) {
                                    echo "color: '#$cor'},";
                                }
                                else {
                                    echo "color: '#$cor'}";
                                }
                            }
                            reset($this->categorias);
                        }
                        else {
                        }
                    }
                    echo "],";
                    if($i == 1) {
                        echo "dataLabels: { enabled: false}";
                    }
                    else {
                        echo "dataLabels: { enabled: true}";
                    }
                    if($i < $countcat) {
                        echo "},";
                    }
                    else {
                        echo "}";
                    }
            }
        }
        else {
            $countseries = count($this->seriesdados);
            if($filtros == "") {
                foreach($this->seriesdados as $ndados => $sdados) {
                    $i++;
                    echo "{";
                    echo "name: '".$this->seriesnome[$ndados]."',";
                    echo "color: '#".$this->seriescor[$ndados]."',";
                    echo "type: '".$this->seriestype[$ndados]."',";
                    echo "stack: '".$this->seriestack[$ndados]."',";
                    if($this->pointwidth != "") {
                        if($this->seriestype[$ndados] == "bar" OR $this->seriestype[$ndados] == "column") {
                            echo "pointWidth: $this->pointwidth,";
                        }
                        if($this->seriestype[$ndados] == "line" OR $this->seriestype[$ndados] == "spline") {
                            $line = substr($this->pointwidth, 0, 1);
                            echo "lineWidth: $line,";
                        }
                    }
                    if($this->yaxis != "N" && count($this->tituloY) >= 2) {
                        $tituloY = "yAxis: ".array_search($ndados, $this->tituloY).",";
                    }
                    else {
                        $tituloY = "";
                    }
                    echo "data: [".implode(",",$sdados)."], $tituloY";
                    if($this->dataLabels == "S") {
                        echo "dataLabels: { enabled: true, x: 3, y: -10, color: '#$this->corlabel' }";
                    }
                    else {
                    }
                    if($i < $countseries) {
                        echo "},";
                    }
                    else {
                        echo "}";
                    }
                }
            }
            else {
                $countseries = count($filtros) * (count($colunas) - 1);
                foreach($this->seriesdados as $freq => $filtros) {
                    foreach($filtros as $kdados => $ndados) {
                        $i++;
                        ?>
                        {name:'<?php echo $this->seriesnome[$freq][$kdados];?>',
                        type:'<?php echo $this->seriestype[$freq][$kdados];?>',
                        color:'#<?php echo $this->seriescor[$freq][$kdados];?>',
                        <?php
                        if($this->pointwidth != "") {
                            if($this->seriestype[$ndados] == "bar" OR $this->seriestype[$ndados] == "column") {
                                echo "pointWidth: $this->pointwidth,";
                            }
                            if($this->seriestype[$ndados] == "line" OR $this->seriestype[$ndados] == "spline") {
                                $line = substr($this->pointwidth, 0, 1);
                                echo "lineWidth: $line,";
                            }
                        }
                        ?>
                        <?php
                        if($this->yaxis == "N" && count($this->tituloY) < 2) {
                            $tituloY = "";
                        }
                        else {
                            $tituloY = "yAxis: ".array_search($ndados, $this->tituloY).",";
                        }
                        if($this->seriestack[$freq][$kdados] == "") {
                        }
                        else {
                            ?>
                            stack:'<?php echo $this->seriestack[$freq][$kdados];?>',
                            <?php
                        }
                        ?>
                        data: [<?php echo implode(",",$ndados);?>], <?php echo $tituloY;?> <?php if($this->dataLabels == "S") { ?> dataLabels: { enabled: true, x: 3, y: -10, color: '<?php echo "#$this->corlabel";?>' } <?php } else {}?>
                        <?php
                        if($i < $countseries) {
                            echo "},";
                        }
                        else {
                            echo "}";
                        }
                    }
                }
            }
        }
        echo "]";
        echo "});";
        echo "});";
    }
}
?>