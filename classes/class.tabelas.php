<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
require_once ($rais.'/monitoria_supervisao/admin/functionsadm.php');

class TabelasSql {

    public $tabelas;
    public $colunastabelas;
    public $coltabrel;
    public $innertabelas;
    public $ontabelas;
    public $include_once;
    public $descop;

    function AliasTab($tab) {
        if(eregi("_",$tab)) {
            $partes = explode("_",$tab);
            $parte1 = substr($partes[0],0,1);
            $parte2 = substr($partes[1],0,2);
            $alias = $parte1.$parte2;
        }
        else {
            $tam = strlen(trim($tab));
            $alias = substr($tab,0,5).substr($tab, ($tam - 1),1);
        }
        return $alias;
    }

    function Tabelas() {
        $this->tabelas = array('agcalibragem' => 'ag','aval_plan' => 'av','camposparam' => 'cp','cat_ebook' => 'ce','chat' => 'ch','coluna_oper' => 'co',
            'conf_calib' => 'cfca','conf_chat' => 'cfch','confemailenv' => 'cfee','conf_mail' => 'cfem','conf_rel' => 'cfrl','corsistema' => 'cs',
            'dados_cli' => 'dc','dados_monitoria' => 'dm','definicao' => 'df','diasdescon' => 'dd','dimen_estim' => 'dme','dimen_mod' => 'dmo',
            'dimen_moni' => 'dmm','ebook' => 'eb','fila_grava' => 'fg','fila_oper' => 'fo','filtro_dados' => 'fd','filtro_nomes' => 'fm','fluxo' => 'fx',
            'formulas' => 'fl','grupo' => 'g','interperiodo' => 'ip', 'indice' => 'i','intervalo_notas' => 'in','moni_dimen' => 'md','moni_login' => 'ml','moni_pausa' => 'mp',
            'moniavalia' => 'ma','moniavaliaadm' => 'mad','moniavaliacalib' => 'mac','monitor' => 'mo','monitoria' => 'm','monitoria_fluxo' => 'mf',
            'monitoriaadm' => 'md','monitoriacalib' => 'mc','motivo' => 'mt','operador' => 'o','param_book' => 'pm','param_moni' => 'pm',
            'perfil_adm' => 'pad','perfil_web' => 'pwe','pergunta' => 'p','periodo' =>  'pe','planilha' => 'pla','qtdaudios' => 'qa','regmonitoria' => 'rm',
            'rel_aval'=> 'ra','rel_filtros' => 'rf','rel_fluxo' => 'rfl','relemailfluxo' => 'ref','relemailimp' => 'rei','relmoni' => 'rlm',
            'relmonicol' => 'rlc','status' => 'st','sub_ebook' => 'se','subgrupo' => 's','super_oper' => 'so','tipo_motivo' => 'tm','upload' =>'u',
            'user_adm' => 'ua','login_adm' => 'ul','login_web' => 'ulw','user_web' => 'uw','useradmfiltro' => 'udf','userwebfiltro' => 'uwf',
            'vinc_plan' => 'vp');
    }

    function Paginclude_once() {
        $this->include_once = array('cliente' => 'clientes.php', 'filtros' => 'filtros.php', 'estrutura' => 'estrufinal.php', 'confemail' => 'confemail.php', 'calendario' => 'calendario.php', 'parammoni' => 'parammoni.php',
        'peradm' => 'perfiladm.php', 'useradm' => 'useradm.php', 'perweb' => 'perfilweb.php', 'userweb' => 'userweb.php', 'monitor' => 'monitor.php', 'estrufluxo' => 'estrufluxo.php', 'fluxo' => 'fluxo.php',
        'motivo' => 'motivo.php', 'dimensiona' => 'dimensiona.php', 'aval_plan' => 'avalplan.php', 'formulas' => 'formulas.php', 'perguntas' => 'perguntas.php', 'subgrupo' => 'subgrupo.php', 'grupo' => 'grupo.php',
        'planilha' => 'planilha.php', 'indice' => 'indice.php', 'calib' => 'calibragem.php', 'operador' => 'operador.php','listaoperador' => 'listaoperador.php', 'supervisor' => 'supervisor.php','monitorias' => 'monitorias.php', 'paramebook' => 'paramebook.php', 'orienta' => 'orientacoes.php', 'editaperfadm' => 'edperfiladm.php', 'eduseradm' => 'eduseradm.php',
        'relestru' => 'relestru.php', 'relsub' => 'rel_sub.php', 'tabulacao' => 'tabulacao.php','perg_resptab' => 'perg_resptab.php', 'relgrupo' => 'rel_grupo.php', 'relplan' => 'rel_plan.php', 'valoraval' => 'valor_aval.php', 'visu' => 'visualiza.php', 'eduserweb' => 'eduserweb.php',
        'edmonitor' => 'edmonitor.php', 'altrelfluxo' => 'altfluxo.php', 'grupodet' => 'grupodetalhe.php', 'subdet' => 'subgrupodetalhe.php', 'pergdet' => 'pergdetalhe.php', 'relestrudet' => 'relestrudet.php',
        'userrelfiltro' => 'useradmfiltro.php', 'userwebfiltro' => 'userwebfiltro.php', 'edparammoni' => 'edparammoni.php', 'camposparam' => 'camposparam.php', 'altorienta' => 'altorienta.php',
        'relenvemail' => 'relenvemail.php', 'relmoni' => 'relmoni.php','relespe' => 'relespecifico.php', 'relmonicol' => 'relmonicol.php', 'filamonitoria' => 'filamonitoria.php', 'relresult' => 'relresult.php', 'respostas' => 'respostas.php',
        'fechamento' => 'fechamento.php','exportacao' => 'exportacaobase.php','exportacaocalib' => 'exportacaocalib.php','exportacaoimp' => 'exportacaoimp.php','log' => 'log.php','improdutivas' => 'improdutivas.php','alertas' => 'alertas.php','selecao' => 'confselecao.php','relacionausers' => 'relacionausers.php');
    }

    function Pagconfig() {
        $this->pagina = array('cliente' => 'CLIENTES','filtros' => 'FILTROS','estrutura' => 'ESTRUTURA','confemail' => 'CONFIG. ENVIO E-MAIL' ,'calendario' => 'CALENDARIO','parammoni' => 'PARAMETROS MONITORIA',
            'camposparam' => 'PARAMETROS MONITORIA','edparammoni' => 'PARAMETROS MONITORIA','peradm' => 'PERFIL ADM','editaperfadm' => 'PERFIL ADM','useradm' => 'USUARIO ADM','eduseradm' => 'USUARIO ADM',
            'userrelfiltro' => 'USUARIO ADM','perweb' => 'PERFIL WEB','userweb' => 'USUARIO WEB','eduserweb' => 'USUARIO WEB','userwebfiltro' => 'USUARIO WEB','monitor' => 'MONITOR','edmonitor' => 'MONITOR',
            'estrufluxo' => 'ESTRUTURA FLUXO','fluxo' => 'FLUXO','altrelfluxo' => 'FLUXO','operador' => 'OPERADOR','listaoperador' => 'LISTA OPERADOR','supervisor' => 'SUPERVISOR','monitorias' => 'MONITORIAS','improdutivas' => 'IMPRODUTIVAS','relestru' => 'RELACIONAMENTO ESTRUTURA','relestrudet' => 'RELACIONAMENTO ESTRUTURA','relenvemail' => 'RELACIONAMENTO ESTRUTURA',
            'motivo' => 'MOTIVO','dimensiona' => 'DIMENSIONAMENTO','tabulacao' => 'TABULACAO','perg_resptab' => 'PERGUNTA/RESPOSTA TABULA', 'aval_plan' => 'AVALIACOES PLANILHA','formulas' => 'FORMULAS','perguntas' => 'PERGUNTAS','pergdet' => 'PERGUNTAS','respostas' => 'PERGUNTAS',
            'subgrupo' => 'SUB-GRUPO','subdet' => 'SUB-GRUPO','relsub' => 'SUB-GRUPO','grupo' => 'GRUPO','grupodet' => 'GRUPO','relgrupo' => 'GRUPO','planilha' => 'PLANILHA','valoraval' => 'PLANILHA','relplan' => 'PLANILHA',
            'indice' => 'INDICE','calib' => 'CALIBRAGEM','paramebook' => 'CADASTRO E-BOOK','orienta' => 'ORIENTACOES','altorienta' => 'ORIENTACOES','relmoni' => 'RELATORIOS MONITORIA','relmonicol' => 'RELATORIOS MONITORIA', 'relresult' => 'RELATORIOS RESULTADOS','relespe' => 'RESPOSTAS ESPECIFICAS', 'filamonitoria' => 'FILA MONITORIA','fechamento' => 'FECHAMENTO',
            'exportacao' => 'EXPORTACAO','exportacaocalib' => 'EXPORTACAO CALIB','exportacaoimp' => 'EXPORTACAO IMP','log' => 'LOG','alertas' => 'ALERTAS','selecao' => 'SELECAO');
    }

    function ColunasTabelas() {
        $this->colunastabelas = array(
            array('agcalibragem' => array('idagcalibragem','dataini','datafim','participantes','usercont','idmonitoria','idrel_filtros','gravacao','finalizado')),
            array('aval_plan' => array('idaval_plan','nomeaval_plan','nivel')),
            array('camposparam' =>array('idcamposparam','idparam_moni','idcoluna_oper','posicao','classifica','tipoclass','visumoni','ativo')),
            array('cat_ebook' => array('idcat_ebook','nomecat_ebook','iduser','tabuser','ativo')),
            array('chat' => array('idchat','iduser','tabuser','iduser_para','tabuser_para','assunto','msg')),
            array('coluna_oper' => array('idcoluna_oper','incorpora','nomecoluna','unico','tipo','tamanho','caracter')),
            array('conf_calib' => array('idconf_calib','bloq','tempobloq','idconfemailenv','intercambio','idusers_acesso')),
            array('conf_chat' => array('idconf_chat','assunto','iduser', 'tabuser')),
            array('confemailenv' => array('idconfemailenv','tipo','titulo','msg','campo','iduser_adm')),
            array('conf_mail' => array('idconf_mail','email','senha','host','porta','tls','iduser_adm')),
            array('conf_rel' => array('idconf_rel','idrel_filtros','idparam_moni','idfluxo','idconf_calib','idplanilha_moni','idplanilha_web','tipoacesso','trocanome','trocafiltro','vdn','ativo')),
            array('corsistema' => array('idcorsistema','objeto','class','cor','tipo')),
            array('dados_cli' => array('iddados_cli','nomedados_cli','site','imagem','assdigita','textoass')),
            array('dados_monitoria' => array('iddados_monitoria','nomedb','hpa','hmonitor')),
            array('definicao' => array('iddefinicao','nomedefinicao','ativo','idfluxo')),
            array('diasdescon' => array('iddiasdescon','idperiodo','data','iduser')),
            array('dimen_estim' => array('iddimen_estim','idrel_filtros','qnt_grava','qnt_oper','multiplicador','estimativa','tma','tta','tm','ttm','idperiodo','divperiodo','qnt_pad','qnt_monitores','hs_monitor','meta_monitor','percpartpa','percpartmeta','datacad','horacad','iduser')),
            array('dimen_mod' => array('iddimen_mod','idrel_filtros','qnt_grava','qnt_oper','multiplicador','estimativa','tma','tta','tm','ttm','idperiodo','divperiodo','qnt_pad','qnt_monitores','hs_monitor','meta_monitor','percpartpa','percpartmeta','datacad','horacad','iduser')),
            array('dimen_moni' => array('idmonen_moni','idrel_filtros','iddimen_mod','idmonitor','idperiodo','meta_m','meta_s','meta_d','ativo')),
            array('ebook' => array('idebook','arquivo','titulo','palavra','descri','procedimento','datareceb','iduser_env','tabuser_env','datacad','horacad','dataalt','horaalt','tporienta','tipo','categoria','subcategoria','ativo','iduser','tabuser')),
            array('fila_grava' => array('idfila_grava','idupload','idoperador','idsuper_oper','idrel_filtros','monitorado','camaudio','monitorando')),
            array('fila_oper' => array('idfila_oper','idupload','idoperador','idsuper_oper','idrel_filtros','dataini','datafim','qtdmoni','monitorando','monitorado')),
            array('filtro_dados' => array('idfiltro_dados','nomefiltro_dados','idfiltro_nomes','nivel','ativo')),
            array('filtro_nomes' => array('idfiltro_nomes','nomefiltro_nomes','nivel','trocafiltro','ativo')),
            array('fluxo' => array('idfluxo','nomefluxo','ativo')),
            array('formulas' => array('idformulas','nomeformulas','tipo','idvinc','formula')),
            array('graficocorpo' => array('idgraficocorpo','idrelresultcorpo','cor','campo','tipograf')),
            array('grafrelmoni' => array('idgrafrelmoni','idrelmoni','tipograf','background','width','heigth','valores','rotacaoval','posicaoval_x','posicaoval_y','maxpoint_y','legenda','direcaoleg','posicaoleg_y','posicaoleg_x','bgcolorleg','bdcolorleg','rotacaoleg','posicaocat')),
            array('grupo' => array('idgrupo','descrigrupo','complegrupo','valor_grupo','ativo','filtro_vinc','idrel','posicao','tab ')),
            array('indice' => array('idindice','nomeindice','inicio','fim')),
            array('intervalo_notas' => array('idintervalo_notas','idindice','numini','numfim','nomeintervalo_notas')),
            array('moni_login' => array('idmoni_login','idmonitor','ip','data','tempo','login','logout')),
            array('moni_pausa' => array('idmoni_pausa','idmonitor','idmotivo','data','horaini','horafim','tempo','lib_super','iduser','tabuser')),
            array('moniavalia' => array('idmonitoria','idplanilha','idaval_plan','valor_aval','valor_final_aval','valor_fg','idgrupo','valor_grupo','valor_final_grup','idsubgrupo','valor_sub','valor_final_sub','tab_sub','idpergunta','tipo_resp','valor_perg','valor_final_perg','tab_perg','idresposta','avalia','valor_resp','obs')),
            array('moniavaliaadm' => array('idmonitoriaadm','idplanilha','idaval_plan','valor_aval','valor_final_aval','valor_fg','idgrupo','valor_grupo','valor_final_grup','idsubgrupo','valor_sub','valor_final_sub','tab_sub','idpergunta','tipo_resp','valor_perg','valor_final_perg','tab_perg','idresposta','avalia','valor_resp','obs')),
            array('moniavaliacalib' => array('idmonitoriacalib','idplanilha','idaval_plan','valor_aval','valor_final_aval','valor_fg','idgrupo','valor_grupo','valor_final_grup','idsubgrupo','valor_sub','valor_final_sub','tab_sub','idpergunta','tipo_resp','valor_perg','valor_final_perg','tab_perg','idresposta','avalia','valor_resp','obs')),
            array('monitor' => array('idmonitor','nomemonitor','CPF','iduser_resp','tabuser','usuario','senha','entrada','saida','datacad','ativo','senha_ini','ass','status','hstatus','meta_m','meta_s','meta_d','ebook','confapres')),
            array('monitoria' => array('idmonitoria','idmonitoriavinc','idrel_filtros','idfila','tabfila','idoperador','idsuper_oper','idmonitor','data','horaini','horafim','tmpaudio','idplanilha','qtdefg','qtdefgm','idmonitoria_fluxo','gravacao','obs','checkfg')),
            array('monitoria_fluxo' => array('idmonitoria_fluxo','idmonitoria','iduser','tabuser','idstatus','iddefinicao','data','hora','obs')),
            array('monitoriaadm' => array('idmonitoriaadm','idmonitoriavinc','idrel_filtros','idfila','idoperador','idsuper_oper','idmonitor','data','horaini','horafim','tmpaudio','idplanilha','qtdefg','qtdefgm','idmonitoria_fluxo','gravacao')),
            array('monitoriacalib' => array('idmonitoriacalib','idagcalibragem','idmonitoriavinc','idrel_filtros','idfila','tabfila','idoperador','idsuper_oper','iduser','idmonitor','data','horaini','horafim','tmpaudio','idplanilha','qtdefg','qtdefgm','gravacao')),
            array('motivo' => array('idmotivo','nomemotivo','idtipo_motivo','qtde','tempo','ativo')),
            array('operador' => array('idoperador','operador','idsuper_oper','dtbase','horabase')),
            array('param_book' => array('idparam_book','camarq','tipoarq','tamanho','dias')),
            array('param_moni' => array('idparam_moni','idindice','tipomoni','checkfg', 'tiposervidor','endereco','loginftp','senhaftp','tipoimp','tpaudio','tpcompress', 'tparquivo', 'camaudio','ccampos','cpartes','conf','hpa','hmonitor')),
            array('perfil_adm' => array('idperfil_adm','nomeperfil_adm','tabelas','ativo')),
            array('perfil_web' => array('idperfil_web','nomeperfil_web','nivel','areas','ativo')),
            array('pergunta' => array('idpergunta','descripergunta','complepergunta','valor_perg','ativo','tipo_resp','idresposta','descrirepsosta','compleresposta','avalia','valor_resp','posicao','ativo_resp')),
            array('periodo' => array('idperiodo','mes','nomemes','dataini','datafim','dias','datacad','hora','iduser','tabuser')),
            array(`interperiodo` => array('idinterperiodo','idperiodo','dataini','datafim')),
            array('planilha' => array('idplanilha','descriplanilha','compleplanilha','tabplanilha','ativoweb','ativomoni','idgrupo','posicao','ativo','idaval_plan','tab')),
            array('regmonitoria' => array('idregmonitoria','idrel_filtros','idfila','idmotivo','idoperador','idsuper_oper','idmonitor','data','horaini','horafim','tmpaudio','gravacao')),
            array('rel_aval' => array('idrel_aval','idplanilha','idaval','valor')),
            array('rel_filtros' => array('idrel_filtros','ativo')),
            array('rel_fluxo' => array('idrel_fluxo','idfluxo','idstatus','iddefinicao','idatustatus','tipo','visadm','visweb','respadm','respweb','ativo')),
            array('relemailfluxo' => array('idrelmailfluxo','idrel_filtros','idconfemailenv','idrel_fluxo','idfluxo','idstatus','iddefinicao','usersadm','usersweb')),
            array('relemailimp' => array('idrelmailimp','idconfemailenv','idrel_filtros','usersadm','usersweb')),
            array('relmoni' => array('idrelmoni','nomerelmoni','qtdecol','tipo','filtros','idsperfiladm','idsperfilweb','tabpri','vincrel','ativo')),
            array('relmonicol' => array('idrelmonicol','idrelmoni','tabela','coluna','nomeapresenta','posicao','ordena','formula')),
            array('relresult' => array('idrelresult','nomerelresult','filtros','descricao','idsperfadm','idsperfweb','ativo')),
            array('relresultcab' => array('idrelresultcab','idrelresult','qtdelogos','caminhologo','posilogo','campos','posicampos')),
            array('relresultcampos' => array('idrelresultcampos','idrelresult','campos','idformula','descricampo','valor','posicao')),
            array('relresultcorpo' => array('idrelresultcorpo','idrelresult','tipodados','frequenciadados','dados','idformula','agrupadados','apresdados','tipografico','corfdgraf','margem','widthgraf','heightgraf','textrotacao','legenda','posileg','orientaleg','posicaoX','posicaoY')),
            array('status' => array('idstatus','nomestatus','fase','dias','iddefiniauto','idfluxo','ativo')),
            array('subebook' => array('idsubebook','nomesubebook','iduser','tabuser','ativo')),
            array('subgrupo' => array('idsubgrupo','descrisubgrupo','complesubgrupo','valor_sub','ativo','idpergunta','posicao','tab')),
            array('super_oper' => array('idsuper_oper','super_oper','dtbase','horabase')),
            array('tipo_motivo' => array('idtipo_motivo','nometipo_motivo','vincula','ativo')),
            array('upload' => array('idupload','tabfila', 'idrel_filtros','iduser','tabuser','camupload','dataini','datafim','dataup','horaup','imp','dataimp','horaimp')),
            array('user_adm' => array('iduser_adm','nomeuser_adm','CPF','email','idperfil_adm','usuario','senha','import','ativo','senha_ini','ass','datacad','calibragem','confapres','dataalt','horalt')),
            array('login_adm' => array('idlogin_adm','iduser_adm','ip','data','hora')),
            array('login_web' => array('idlogin_web','iduser_web','ip','data','hora')),
            array('user_web' => array('iduser_web','nomeuser_web','CPF','email','idperfil_web','usuario','senha','ativo','senha_ini','ass','import','datacad','calibragem','confapres','dataalt','hoaraalt')),
            array('useradmfiltro' => array('iduseradmfiltro','iduser_adm','idrel_filtros')),
            array('userwebfiltro' => array('iduserwebfiltro','iduser_web','idrel_filtros')),
            array('vinc_plan' => array('idvinc_plan','idplanilha','planvinc','idrel_filtros','ativo'),
            array('tabulacao' => array('idtabulacao','nometabualacao','idperguntatab','idrespostatab','ativo')),
            array('perguntatab' => array('idperguntatab','descriperguntatab','ativo')),
            array('respostatab' => array('idrespostatab','descrirespostatab','idproxpergunta','ativo')),
            array('histresp' => array('idhistresp','idmonitoria','idmonitoria_fluxo','idplanilha','idaval_plan','idgrupo','idpergunta','idresposta','obs')),
            array('itenscontest' => array('iditenscontest','idmonitoria','idmonitoria_fluxo','idspergunta')))
        );
    }

    function Tabvar($tabsec) {
        $this->colvar = array('ID' => 'idmonitoria', 'dtinimoni' => 'data', 'dtfimmoni' => 'data','dtinictt' => 'datactt','dtfimctt' => 'datactt','fg' => 'qtdefg','fgm' => 'qtdefgm',
                              'super_oper' => 'super_oper','oper' => 'operador','nomemonitor' => 'nomemonitor','plan' => 'idplanilha','aval' => 'idaval_plan','grupo' => 'idgrupo','sub' => 'idsubgrupo',
                              'perg' => 'idpergunta','resp' => 'idresposta','atustatus' => 'idatustatus','intervalo' => 'idintervalo_notas','indice' => 'idindice','fluxo' => 'idfluxo','tmonitoria' => 'tmonitoria','valor_fg' => 'valor_fg','alertas' => 'alertas');
        if($tabsec == "monitoria") {
            $this->tabvar = array('ID' => 'monitoria', 'dtinimoni' => 'monitoria', 'dtfimmoni' => 'monitoria','dtinictt' => 'monitoria','dtfimctt' => 'monitoria','fg' => 'monitoria','fgm' => 'monitoria',
                                  'super_oper' => 'super_oper','oper' => 'operador','nomemonitor' => 'monitor','plan' => 'moniavalia',
                                  'aval' => 'moniavalia','grupo' => 'moniavalia','sub' => 'moniavalia','perg' => 'moniavalia','resp' => 'moniavalia',
                                  'atustatus' => 'rel_fluxo','intervalo' => 'intervalo_notas','indice' => 'indice', 'fluxo' => 'fluxo','tmonitoria' => 'monitoria','valor_fg' => 'moniavalia','alertas' => 'monitoria','search' => array('monitoria','moniavalia'));
        }
        if($tabsec == "monitoriacalib") {
            $this->tabvar = array('dtinimoni' => 'agcalibragem', 'dtfimmoni' => 'agcalibragem','dtinictt' => 'monitoria','dtfimctt' => 'monitoria','plan' => 'monitoria','idagcalibragem' => 'agcalibragem');
            $this->colvar = array('dtinimoni' => 'data', 'dtfimmoni' => 'data','dtinictt' => 'datactt','dtfimctt' => 'datactt','plan' => 'idplanilha','idagcalibragem' => 'idagcalibragem');
        }
        if($tabsec == "regmonitoria") {
            $this->descop = array('plan','aval','grupo','sub','perg','resp','valor_fg'); // filtros que sÃƒÂ£o desconsiderados em consulta de resgistros
            $this->tabvar = array('ID' => 'monitoria', 'dtinimoni' => 'regmonitoria', 'dtfimmoni' => 'regmonitoria','dtinictt' => 'fila_grava','dtfimctt' => 'fila_grava','fg' => 'monitoria','fgm' => 'monitoria',
                                  'super_oper' => 'super_oper','oper' => 'operador','nomemonitor' => 'monitor','plan' => 'moniavalia','aval' => 'moniavalia','grupo' => 'moniavalia','sub' => 'moniavalia','perg' => 'moniavalia','resp' => 'moniavalia');
        }
        $selfiltro = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
        $efiltro = $_SESSION['query']($selfiltro) or die ("erro na query de consulta dos filtros cadastrados");
        while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
            $this->tabvar['filtro_'.strtolower(trim($lfiltros['nomefiltro_nomes']))] = 'rel_filtros';
            $this->colvar['filtro_'.strtolower(trim($lfiltros['nomefiltro_nomes']))] = "id_".strtolower(trim($lfiltros['nomefiltro_nomes']));
        }
    }

    function ColunasTabRel() {
        $selfila = "SHOW COLUMNS FROM fila_grava";
        $eselfila = $_SESSION['query']($selfila) or die ("erro na query de consulta das colunas da fila");
        while($lselfila = $_SESSION['fetch_array']($eselfila)) {
            $colfila[] = $lselfila['Field'];
        }
        $this->coltabrel = array(
            array('alertas' => array('idalerta','nomealerta')),
            array('aval_plan' => array('idaval_plan','nomeaval_plan')),
            array('conf_rel' => array('tipoacesso', 'trocanome', 'trocafiltro', 'vdn')),
            array('definicao' => array('nomedefinicao','iddefinicao')),
            array('dimen_mod' => array('qnt_grava','qnt_oper','multiplicador','estimativa','tma','tta','tm','ttm','qnt_pad','qnt_monitores','hs_monitor','meta_monitor','percpartpa','percpartmeta')),
            array('dimen_moni' => array('meta_m','meta_s','meta_d')),
            array('fluxo' => array('nomefluxo')),
            array('fila_grava' => $colfila),
            array('formulas' => array('idformulas','formula')),
            array('grupo' => array('idgrupo','descrigrupo','valor_grupo','tab')),
            array('indice' => array('nomeindice','inicio','fim')),
            array('intervalo_notas' => array('numini','numfim','nomeintervalo_notas','idintervalo_notas')),
            array('moni_login' => array('ip','data','tempo','login','logout')),
            array('moni_pausa' => array('data','horaini','horafim','tempo')),
            array('moniavalia' => array('idaval_plan','idgrupo','idpergunta','idsubgrupo','idpergunta','idresposta','valor_aval','valor_final_aval','valor_fg','valor_grupo','valor_final_grup','valor_sub','valor_final_sub','tab_sub','tipo_resp','valor_perg','valor_final_perg','tab_perg','avalia','valor_resp')),
            array('moniavaliacalib' => array('valor_aval','valor_final_aval','valor_fg','valor_grupo','valor_final_grup','valor_sub','valor_final_sub','tab_sub','tipo_resp','valor_perg','valor_final_perg','tab_perg','avalia','valor_resp')),
            array('monitor' => array('nomemonitor','CPF','meta_m','meta_s','meta_d')),
            array('monitoria' => array('idmonitoria','idmonitoriavinc','data','datactt','horaini','horafim','tmpaudio','qtdefg','qtdefgm','gravacao','obs','checkfg','alertas')),
            array('monitoriacalib' => array('idmonitoriacalib','idagcalibragem','idmonitoriavinc','data','horaini','horafim','tmpaudio','qtdefg','qtdefgm','gravacao')),
            array('monitoria_fluxo' => array('idmonitoria_fluxo','idmonitoria','idstatus','iddefinicao','data','hora','obs')),
            array('motivo' => array('nomemotivo','qtde','tempo')),
            array('operador' => array('operador','cpfoper')),
            array('param_moni' => array('tipomoni','tipoimp')),
            array('pergunta' => array('idpergunta','descripergunta','ativo','tipo_resp','idresposta','descrirepsosta','avalia')),
            array('periodo' => array('mes','nomemes','dataini','datafim','dias')),
            array('planilha' => array('descriplanilha','tabplanilha')),
            array('regmonitoria' => array('idregmonitoria','data','horaini','horafim','tmpaudio','gravacao')),
            array('rel_fluxo' => array('idrel_fluxo','idatustatus')),
            array('rel_filtros' => relfiltros()),
            array('relmoni' => array('nomerelmoni','qtdecol','tipo','filtros','tabpri','vincrel','ativo')),
            array('relmonicol' => array('tabela','coluna','nomeapresenta','posicao','ordena','formula')),
            array('status' => array('nomestatus','fase','dias')),
            array('subgrupo' => array('idsubgrupo','descrisubgrupo','valor_sub')),
            array('super_oper' => array('super_oper','dtbase','horabase')),
            array('tipo_motivo' => array('nometipo_motivo','vincula')),
            array('user_adm' => array('nomeuser_adm','CPF','email','usuario','import','ativo','senha_ini','ass','datacad','calibragem','confapres')),
            array('user_web' => array('nomeuser_web','CPF','email','usuario','ativo','senha_ini','ass','import','datacad','calibragem','confapres')),
         );
    }

    function TabsVinc($tab, $tabpri, $tabsec, $where) {
        // a chave do array representa a tabela e coluna que serÃƒÂ¡ vincula como ON no INNER JOIN e o valor serÃƒÂ¡ a tabela INNER JOIN
        if($tab == "alertas") {
            $this->tabvinc = array($this->AliasTab(monitoria).':alertas' => 'alertas');
        }
        if($tab == "aval_plan") {
            $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(moniavalia).':aval_plan' => 'aval_plan');
        }
        if($tab == "conf_rel") {
            if($tabsec == "monitoria") {
                if($tabpri == "intervalo_notas") {
                    $this->tabvinc = array($this->AliasTab(indice).':indice' => 'conf_rel');
                }
                else {
                    $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel');
                }
            }
            if($tabsec == "regmontioria") {
                $this->tabvinc = array($this->AliasTab(regmonitoria).':rel_filtros' => 'conf_rel');
            }
        }
        if($tab == "definicao") {
            if($tabpri == "monitoria_fluxo") {
                $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':fluxo' => 'status',$this->AliasTab(monitoria_fluxo).':definicao' => 'definicao',$this->AliasTab(monitoria_fluxo).':definicao' => 'definicao');
            }
            else {
                if($tabpri == "definicao") {
                    $this->tabvinc = array($this->AliasTab(definicao).':definicao' => 'monitoria_fluxo',$this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':fluxo' => 'status',$this->AliasTab(conf_rel).':fluxo' => 'definicao',$this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo');
                }
                else {
                    $this->tabvinc = array($this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo',$this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(rel_fluxo).':atustatus' => 'status',$this->AliasTab(monitoria_fluxo).':definicao' => 'definicao');
                }
            }
        }
        if($tab == "dimen_mod") {
            $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'dimen_mod',$this->AliasTab(dimen_mod).':periodo' => 'periodo');
        }
        if($tab == "dimen_moni") {
            $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'dimen_mod',$this->AliasTab(monitoria).':rel_filtros' => 'dimen_moni',$this->AliasTab(dimen_mod).':periodo' => 'periodo');
        }
        if($tab == "fila_grava") {
            $this->tabvinc = array($this->AliasTab($tabpri).':'.$tabpri => 'fila_grava');
        }
        if($tab == "filtro_dados") {
            $selfiltros = "SELECT fn.nomefiltro_nomes FROM filtro_nomes fn
                           INNER JOIN filtro_dados fd ON fd.idfiltro_nomes = fn.idfiltro_nomes
                           WHERE fn.ativo='S' GROUP BY fn.idfiltro_nomes";
            $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consutla dos filtros");
            while($lself = $_SESSION['fetch_array']($eselfiltros)) {
                $tabvinc[] = $this->AliasTab(rel_filtros).':id'.strtolower($lself['nomefiltro_nomes'])." => 'filtro_nomes'";
            }
            $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'rel_filtros',  implode(",",$tabvinc), $this->AliasTab(filtro_nomes).':filtro_nomes' => 'filtros_dados');
        }
        if($tab == "filtro_nomes") {
            $selfiltros = "SELECT fn.nomefiltro_nomes FROM filtro_nomes fn
                           INNER JOIN filtro_dados fd ON fd.idfiltro_nomes = fn.idfiltro_nomes
                           WHERE fn.ativo='S' GROUP BY fn.idfiltro_nomes";
            $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consutla dos filtros");
            while($lself = $_SESSION['fetch_array']($eselfiltros)) {
                $tabvinc[] = $this->AliasTab(rel_filtros).':id'.strtolower($lself['nomefiltro_nomes'])." => 'filtro_nomes'";
            }
            $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'rel_filtros',implode(",",$tabvinc));
        }
        if($tab == "fluxo") {
            $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':fluxo' => 'rel_fluxo',$this->AliasTab(rel_fluxo).':fluxo' => 'fluxo');
        }
        if($tab == "formulas") {
            $this->tabvinc = array($this->AliasTab(relmoni).':relmoni' => 'formulas', 'vazio' => 'relmoni');
        }
        if($tab == "grupo") {
            $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(moniavalia).':grupo' => 'grupo');
        }
        if($tab == "indice") {
            if($tabpri == "intervalo_notas") {
                $this->tabvinc = array($this->AliasTab(param_moni).':param_moni' => 'conf_rel',$this->AliasTab(intervalo_notas).':indice' => 'indice');
            }
            else {
                $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':param_moni' => 'param_moni',$this->AliasTab(param_moni).':indice' => 'indice');
            }
        }
        if($tab == "intervalo_notas") {
            if($tabpri == "intervalo_notas") {
                $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(param_moni).':param_moni' => 'conf_rel',$this->AliasTab(indice).':indice' => 'param_moni',$this->AliasTab(intervalo_notas).':indice' => 'indice');
            }
            else {
                $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':param_moni' => 'param_moni',$this->AliasTab(param_moni).':indice' => 'indice',$this->AliasTab(indice).':indice' => 'intervalo_notas');
            }
        }
        if($tab == "moniavalia") {
            $this->tabvinc[$this->AliasTab(monitoria).':monitoria'] =  'moniavalia';
            if($where['plan'] != "") {
                $this->tabvinc[$this->AliasTab(moniavalia).':planilha'] = 'planilha';
            }
            if($where['aval'] != "") {                
                $this->tabvinc[$this->AliasTab(moniavalia).':aval_plan'] =  'aval_plan';
            }
            if($where['grupo'] != "") {
                $this->tabvinc[$this->AliasTab(moniavalia).':grupo'] = 'grupo';
            }
            if($where['perg'] != "") {
                $this->tabvinc[$this->AliasTab(moniavalia).':pergunta'] = 'pergunta';
            }
            if($where['sub'] != "") {
                $this->tabvinc[$this->AliasTab(moniavalia).':subgrupo'] = 'subgrupo';
            }
        }
        if($tab == "moniavaliacalib") {
            $this->tabvinc = array($this->AliasTab(monitoriacalib).':monitoriacalib' => 'moniavaliacalib',$this->AliasTab(moniavaiacalib).':aval_plan' => 'aval_plan',$this->AliasTab(moniavaiacalib).':planilha' => 'planilha',$this->AliasTab(moniavaiacalib).':grupo' => 'grupo',$this->AliasTab(moniavaiacalib).':subgrupo' => 'subgrupo',$this->AliasTab(moniavaiacalib).':pergunta' => 'pergunta');
        }
        if($tab == "monitor") {
            if($tabsec == "monitoria") {
                $this->tabvinc = array($this->AliasTab(monitoria).':monitor' => 'monitor');
            }
            if($tabsec == "regmonitoria") {
                $this->tabvinc = array($this->AliasTab(regmonitoria).':monitor' => 'monitor');
            }
            if($tabsec == "moni_pausa") {
                $this->tabvinc = array($this->AliasTab(moni_pausa).':monitor' => 'monitor');
            }
        }
        if($tab == "monitoria") {
            if($tabpri == "status") {
                $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'rel_filtros',$this->AliasTab(monitoria).':operador' => 'operador',$this->AliasTab(monitoria).':super_oper' => 'super_oper',$this->AliasTab(monitoria).':monitor' => 'monitor',$this->AliasTab(rel_fluxo).':rel_fluxo' => 'monitoria_fluxo',$this->AliasTab(monitoria_fluxo).':monitoria' => 'monitoria',$this->AliasTab(status).':atustatus' => 'rel_fluxo');
            }
            if($tabpri == "monitoria_fluxo") {
                $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'rel_filtros',$this->AliasTab(monitoria).':operador' => 'operador',$this->AliasTab(monitira).':super_oper' => 'super_oper',$this->AliasTab(monitoria).':monitor' => 'monitor');
            }
            if($tabpri != "status" && $tabpri != "monitoria_fluxo") {
                $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'rel_filtros',$this->AliasTab(monitoria).':operador' => 'operador',$this->AliasTab(monitoria).':super_oper' => 'super_oper',$this->AliasTab(monitoria).':monitor' => 'monitor',$this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo');
            }
        }
        if($tab == "monitoria_fluxo") {
            if($tabpri == "monitoria_fluxo") {
                $this->tabvinc = array($this->AliasTab(monitoria_fluxo).':monitoria' => 'monitoria', $this->AliasTab(monitoria_fluxo).':status' => 'status');
            }
            else {
                $this->tabvinc = array($this->AliasTab(monitoria_fluxo).':monitoria_fluxo' => 'monitoria', $this->AliasTab(rel_fluxo).':atustatus' => 'status');
            }
        }
        if($tab == "monitoriacalib") {
            $this->tabvinc = array($this->AliasTab(monitoriacalib).':agcalibragem' => 'agcalibragem',$this->AliasTab(monitoriacalib).':rel_filtros' => 'rel_filtros',$this->AliasTab(monitoriacalib).':operador' => 'operador',$this->AliasTab(monitoriacalib).':super_oper' => 'super_oper',$this->AliasTab(monitoriacalib).':monitor' => 'monitor',$this->AliasTab(monitoriacalib).':planilha' => 'planilha');
        }
        if($tab == "motivo") {
            $this->tabvinc = array($this->AliasTab(moni_pausa).':motivo' => 'motivo');
        }
        if($tab == "operador") {
            if($tabsec == "monitoria") {
                $this->tabvinc = array($this->AliasTab(monitoria).':operador' => 'operador');
            }
            if($tabsec == "regmonitoria") {
                $this->tabvinc = array($this->AliasTab(regmonitoria).':operador' => 'operador');
            }
        }
        if($tab == "param_moni") {
            if($tabsec == "monitoria") {
                if($tabpri == "intervalo_notas") {
                    $this->tabvinc = array($this->AliasTab(indice).':indice' => 'param_moni');
                }
                else {
                    $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':param_moni' => 'param_moni');
                }
            }
            if($tabsec == "regmonitoria") {
                $this->tabvinc = array($this->AliasTab(regmonitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(conf_rel).':param_moni' => 'param_moni');
            }
        }
        if($tab == "pergunta") {
            $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(moniavalia).':pergunta' => 'pergunta');
        }
        if($tab == "periodo") {
            $this->tabvinc = array('periodo');
        }
        if($tab == "planilha") {
            $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(moniavalia).':planilha' => 'planilha');
        }
        if($tab == "regmonitoria") {
            $this->tabvinc = array($this->AliasTab(regmonitoria).':rel_filtros' => 'rel_filtros',$this->AliasTab(regmonitoria).':motivo' => 'motivo',$this->AliasTab(regmonitoria).':operador' => 'operador',$this->AliasTab(regmonitoria).':monitor' => 'monitor',$this->AliasTab(regmonitoria).':super_oper' => 'super_oper');
        }
        if($tab == "rel_filtros") {
            $this->tabvinc[$this->AliasTab(monitoria).':rel_filtros'] = 'rel_filtros';
            $this->tabvinc[$this->AliasTab(rel_filtros).':'.implode(",",  relfiltros())] = 'filtro_dados';
        }
        if($tab == "rel_fluxo") {
            if($tabpri == "status") {
                $this->tabvinc = array($this->AliasTab(rel_fluxo).':rel_fluxo' => 'monitoria_fluxo',$this->AliasTab(status).':atustatus' => 'rel_fluxo');
            }
            else {
                if($tabpri == "monitoria_fluxo") {
                    $this->tabvinc = array($this->AliasTab(monitoria_fluxo).':status' => 'status');
                }
                else {
                    $this->tabvinc = array($this->AliasTab(monitoria_fluxo).':rel_fluxo' => 'rel_fluxo',$this->AliasTab(rel_fluxo).':atustatus' => 'status');
                }
            }
        }
        if($tab == "status") {
            if($tabsec == "monitoria" && $tabpri == "operador") {
                $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(status).':fluxo' => 'fluxo',$this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo',$this->AliasTab(rel_fluxo).':atustatus' => 'status');
            }
            else {
                if($tabpri == "status") {
                    $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(status).':fluxo' => 'fluxo',$this->AliasTab(rel_fluxo).':rel_fluxo' => 'monitoria_fluxo',$this->AliasTab(status).':atustatus' => 'rel_fluxo');
                }
                if($tabpri == "monitoria_fluxo") {
                    $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(status).':fluxo' => 'fluxo',$this->AliasTab(monitoria_fluxo).':status' => 'status');
                }
                if($tabpri != "monitoria_fluxo" && $tabpri != "status") {
                    $this->tabvinc = array($this->AliasTab(monitoria).':rel_filtros' => 'conf_rel',$this->AliasTab(status).':fluxo' => 'fluxo',$this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo',$this->AliasTab(monitoria_fluxo).':rel_fluxo' => 'rel_fluxo',$this->AliasTab(rel_fluxo).':atustatus' => 'status');
                }
            }
        }
        if($tab == "subgrupo") {
            $this->tabvinc = array($this->AliasTab(monitoria).':monitoria' => 'moniavalia',$this->AliasTab(moniavalia).':subgrupo' => 'subgrupo');
        }
        if($tab == "super_oper") {
            if($tabsec == 'monitoria') {
                $this->tabvinc = array($this->AliasTab(operador).':super_oper' => 'super_oper');
            }
            if($tabsec == "regmonitoria") {
                $this->tabvinc = array($this->AliasTab(regmonitoria).':super_oper' => 'super_oper');
            }
        }
        if($tab == "tipo_motivo") {
            $this->tabvinc = array($this->AliasTab(moni_pausa).':motivo' => 'motivo',$this->AliasTab(motivo).':tipo' => 'tipo_motivo');
        }
        if($tab == "user_adm") {
            if($tabsec == "monitoria") {
                $this->tabvinc = array($this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo',$this->AliasTab(monitoria_fluxo).':user' => 'user_adm', $this->AliasTab(user_adm).':perfil_adm' => 'perfil_adm');
            }
            if($tabsec == "moni_pausa") {
                $this->tabvinc = array($this->AliasTab(moni_pausa).':user_adm' => 'user_adm', $this->AliasTab(user_adm).':perfil_adm' => 'perfil_adm');
            }
        }
        if($tab == "user_web") {
            if($tabpri == "monitoria") {
                $this->tabvinc = array($this->AliasTab(monitoria).':monitoria_fluxo' => 'monitoria_fluxo',$this->AliasTab(monitoria_fluxo).':user' => 'user_web', $this->AliasTab(user_adm).':perfil_web' => 'perfil_web');
            }
        }
    }

    function InnerTabelas($tabpri, $atabpri, $tabsec, $atabsec, $tab, $alias, $atabon, $coltab, $tipoinner) {
        $ctab = explode(",",$coltab);
        if($tab == $tabpri) {
        }
        if($tab == $tabsec) {
        }
        if($atabon == "") {
            $on = "INNER JOIN $tab $alias";
            $this->innertabelas[$on] = $on;
        }
        if($tab != $tabpri && $tab != $tabsec AND $atabon != "") {
            foreach($ctab as $c) {
                if(eregi("id", $c)) {
                    if(count($ctab) > 1) {
                        if($tab == "filtro_dados") {
                            $filt = explode("_",$c);
                            $alias = $alias.$this->AliasTab($filt[1]);
                                $on = "$tipoinner JOIN $tab $alias ON $alias.id$tab = $atabon.$c";
                        }
                        else {
                            $alias = $alias.$this->AliasTab($filt[1]);
                            $on = "$tipoinner JOIN $tab $alias ON $alias.$c = $atabon.$c";
                        }
                        $alias = $this->AliasTab($tab);
                    }
                    else {
                        $on = "$tipoinner JOIN $tab $alias ON $alias.$c = $atabon.$c";
                    }
                }
                else {
                    if($tab == "rel_fluxo" && $atabon == $this->AliasTab(status)) {
                        $on = "$tipoinner JOIN $tab $alias ON $alias.idatustatus = $atabon.idstatus";
                    }
                    else {
                        if(($tabpri == "intervalo_notas" && $tab == "moniavalia") OR ($tab == "intervalo_notas" && $tabsec == "monitoria")) {
                            if($tab == "intervalo_notas" && ($tabpri == "monitoria" OR $tabsec == "monitoria")) {
                                $on = "$tipoinner JOIN $tab $alias ON $alias.id$c = $atabon.id$c AND ".$this->AliasTab(moniavalia).".valor_fg >= $alias.numini AND ".$this->AliasTab(moniavalia).".valor_fg <= $alias.numfim";
                            }
                            else {
                                $on = "$tipoinner JOIN $tab $alias ON $alias.id$c = $atabon.id$c AND $alias.valor_fg >= $atabpri.numini AND $alias.valor_fg <= $atabpri.numfim";
                            }
                        }
                        else {
                            if($tab == "status" && $atabon == $this->AliasTab(rel_fluxo)) {
                                $on = "$tipoinner JOIN $tab $alias ON $alias.idstatus = $atabon.id$c";
                            }
                            else {
                                if($tab == "fila_grava" OR $tab == "fila_oper") {
                                    if($tab == "fila_grava") {
                                        $on = "INNER JOIN $tab $alias ON $alias.idfila_grava = $atabsec.idfila";
                                    }
                                    else {
                                        $on = "INNER JOIN $tab $alias ON $alias.idfila_oper = $atabsec.idfila";
                                    }
                                }
                                else {
                                    if(($tabpri == "aval_plan" OR $tabpri == "grupo" OR $tabpri == "pergunta" OR $tabpri == "subgrupo")  && $tab == "moniavalia") {
                                        if($tabpri == "pergunta") {
                                            $on = "$tipoinner JOIN $tab $alias ON $alias.id$tabpri = $atabpri.id$tabpri AND $alias.idresposta = $atabpri.idresposta";
                                        }
                                        if($tabpri == "grupo") {
                                            $on = "$tipoinner JOIN $tab $alias ON $alias.id$tabpri = $atabpri.id$tabpri AND ($alias.idpergunta = $atabpri.idrel OR $alias.idsubgrupo = $atabpri.idrel)";
                                        }
                                        if($tabpri == "subgrupo") {
                                            $on = "$tipoinner JOIN $tab $alias ON $alias.id$tabpri = $atabpri.id$tabpri AND $alias.idpergunta = $atabpri.idpergunta";
                                        }
                                        if($tabpri == "aval_plan") {
                                            $on = "$tipoinner JOIN $tab $alias ON $alias.id$tabpri = $atabpri.id$tabpri";
                                        }
                                    }
                                    else {
                                        $on = "$tipoinner JOIN $tab $alias ON $alias.id$c = $atabon.id$c";
                                    }
                                }
                            }
                        }
                    }
                }
                $inner = explode("ON",$on);
                $inner = trim($inner[0]);
                if(key_exists($inner, $this->innertabelas)) {
                }
                else {
                    $this->innertabelas[trim($inner)] = $on;
                }
            }
        }
    }

    function InnerOn($tabpri, $colpri, $tabsec, $colsec) {
        $this->ontabelas = "ON ".key($this->tabelas[$tabpri]).".".$colpri." = ".key($this->tabelas[$tabsec]).".".$colsec;
    }
}


?>
