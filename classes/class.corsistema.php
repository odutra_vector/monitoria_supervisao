<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class CoresSistema {
    public $corfd_pagdb;
    public $corfd_ntabdb;
    public $corfd_coltextodb;
    public $corfd_colcamposdb;
    public $corfd_pag;
    public $corfd_ntab;
    public $corfd_coltexto;
    public $corfd_colcampos;

    function Cores($dbcli, $idcli, $tipo) {
        if($dbcli == "") {
            $cores = array('corfd_pag' => 'EAEAEA', 'corfd_ntab' => '6699CC', 'corfd_coltexto' => '999999', 'corfd_colcampos' => 'FFFFFF');
            echo "<style type=\"text/css\">";
            foreach ($cores as $class => $cor) {
                $this->$class = $cor;
                echo ".".$class." {\n";
                echo "background-color: #".$cor.";\n";
                echo "}\n";
            }
            echo "</style>";
        }
        else {
            $countcor = "SELECT COUNT(*) as result FROM corsistema WHERE tipo = '$tipo' AND iddados_cli='$idcli'";
            $ecount = $_SESSION['fetch_array']($_SESSION['query']($countcor)) or die ("erro na query de consulta da quantidade de linhas cadastradas");
            if($ecount['result'] == 0) {
                $cores = array('corfd_pag' => 'EAEAEA', 'corfd_ntab' => '6699CC', 'corfd_coltexto' => '999999', 'corfd_colcampos' => 'FFFFFF');
                echo "<style type=\"text/css\">";
                foreach ($cores as $class => $cor) {
                    $this->$class = $cor;
                    echo ".".$class." {\n";
                    echo "background-color: #".$cor.";\n";
                    echo "}\n";
                }
                echo "</style>";
            }
            else {
                $selcor = "SELECT * FROM corsistema WHERE tipo='$tipo' AND iddados_cli='$idcli'";
                $eselcor = $_SESSION['query']($selcor) or die ("erro na query de consulta da tabela de cores");
                while($lselcor = $_SESSION['fetch_array']($eselcor)) {
                    $cores[$lselcor['class']] = $lselcor['cor'];
                }

                echo "<style type=\"text/css\">";
                foreach ($cores as $class => $cor) {
                    $this->$class = $cor;
                    echo ".".$class." {\n";
                    echo "background-color: #".$cor.";\n";
                    echo "}\n";
                }
                echo "</style>";
            }
        }
    }

    function AltCorCli($dbcli, $idcli, $tipo) {
        if($dbcli == "") {
            $cores = array('corfd_pagdb' => 'EAEAEA', 'corfd_ntabdb' => '6699CC', 'corfd_coltextodb' => '999999', 'corfd_colcamposdb' => 'FFFFFF');
            foreach($cores as $class => $cor) {
                $this->$class = $cor;
            }
        }
        else {
            $selbanco = $_SESSION['seldb']($dbcli);
            $countcor = "SELECT COUNT(*) as result FROM corsistema WHERE tipo = '$tipo' AND iddados_cli='$idcli'";
            $ecount = $_SESSION['fetch_array']($_SESSION['query']($countcor)) or die ("erro na query de consulta da quantidade de linhas cadastradas");
            if($ecount['result'] == 0) {
                $cores = array('corfd_pagdb' => 'EAEAEA', 'corfd_ntabdb' => '6699CC', 'corfd_coltextodb' => '999999', 'corfd_colcamposdb' => 'FFFFFF');
                foreach($cores as $class => $cor) {
                    $this->$class = $cor;
                }
            }
            else {
                $selcordb = "SELECT * FROM corsistema WHERE tipo='$tipo' AND iddados_cli='$idcli'";
                $eseldb = $_SESSION['query']($selcordb) or die ("erro na query de consulta das cores cadastradas no banco");
                while($lseldb = $_SESSION['fetch_array']($eseldb)) {
                    $class = $lseldb['class']."db";
                    $this->$class = $lseldb['cor'];
                }
            }
            $seldb = $_SESSION['seldb']($_SESSION['selbanco']);
        }
    }
}

?>
