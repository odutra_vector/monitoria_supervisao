<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once ($rais.'/monitoria_supervisao/admin/functionsadm.php');

class Salva_Moni {

    public $gravacao;
    public $type;
    public $idrelfiltro;
    public $idfila;
    public $idupload;
    public $idsplan;
    public $idplan;
    public $plan;
    public $horactt;
    public $datactt;
    public $tipomoni;
    public $idfluxo;
    public $iddefini;
    public $obsdefini;
    public $sqltab;
    public $idoper;
    public $idsuper;
    public $idsaval;
    public $vaval;
    public $idsgrup;
    public $vgrup;
    public $idssub;
    public $vsub;
    public $idsperg;
    public $vperg;
    public $idsresp;
    public $idspergtab;
    public $idmonitor;
    public $iduser;
    public $nperg;
    public $nresp;
    public $dados;
    public $monicol;
    public $monivalor;
    public $cadobs;
    public $idcalib;
    public $idmoni;
    public $idsmoni;
    public $qtdmoni;
    public $checkfg;
    public $cfg;
    public $cfgt;
    public $cfgm;
    public $cfgmt;
    public $fgmsg;
    public $valoraval;
    public $idsmonied;
    public $acao;
    public $idsplaned;
    public $tipoplan;
    public $tabmoni;
    public $tabavalia;
    public $pergpontua;
    public $descriresp;
    public $retheader;
    public $altera; //variÃ¡vel que informa se a planilha deve ser alterada ou nÃ£o
    public $veriftab;
    public $obs;
    public $semdados;
    public $dadosdig;
    public $idsmonifg;

    function validafg($idsresp) {
        foreach($idsresp as $ids) {
            foreach($ids as $id) {
                $selfg = "SELECT idpergunta,descripergunta,avalia, count(*) FROM pergunta WHERE idresposta='$id'";
                $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selfg)) or die (mysql_error());
                if($eselfg['avalia'] == "FG" OR $eselfg['avalia'] == "FGM") {
                    $pergfg[$eselfg['idpergunta']] = $eselfg['descripergunta'];
                }
                else {
                }
            }
        }
        if(isset($pergfg)) {
            $this->retheader = "VOCÊ DESEJA SALVAR A MONITORIA, EXISTEM NCG APONTADAS NAS PERGUNTAS: \n\n".implode("\n",$pergfg);
        }
        else {
            $this->retheader = "VOCÊ DESEJA SALVAR A MONITORIA? LEMBRE-SE DE CHECAR A CORRETA MARCACÃO DOS ALERTAS !!!";
        }
        
    }
    
    function vf_consistencia($idmoni,$idsperguntas,$idsrespostas,$idstabula,$idsdescri) {
        foreach($this->idsplan as $idplan) {
            $idsresptemp = "";
            $idsresp = "";
            $errotab = 0;
            $vobsvazio = 0;
            $idsresptemp = $idsrespostas['idresp'.$idplan];
            $selplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$idplan'";
            $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta do nome da planilha");
            $nomeplan = $eselplan['descriplanilha'];
            if($this->acao == "cadastra") {
                $idmoni = $this->idsmonied[$idplan];
                if($idmoni == "0" OR $idmoni == "") {
                    $seltabplan = "SELECT DISTINCT(idplanilha), idtabulacao FROM planilha WHERE idplanilha='$idplan' GROUP BY idplanilha";
                }
                else {
                    $seltabplan = "SELECT * FROM monitabulacao WHERE idmonitoria='$idmoni' GROUP BY idtabulacao";
                }
            }
            else {
                $idmoni = $this->idsmonied[$idplan];
                if($idmoni == "0" OR $idmoni == "") {
                    $seltabplan = "SELECT DISTINCT(idplanilha), idtabulacao FROM planilha WHERE idplanilha='$idplan' GROUP BY idplanilha";
                }
                else {
                    $seltabplan = "SELECT * FROM monitabulacao WHERE idmonitoria='$idmoni' GROUP BY idtabulacao";
                }
            }
            $etabplan = $_SESSION['query']($seltabplan) or die ("erro na query de consulta das tabulaÃ§Ãµes relacionadas a planilha");
            if($_SESSION['num_rows']($etabplan) == 0) {
            }
            else {
                while($ltabplan = $_SESSION['fetch_array']($etabplan)) {
                    if($this->acao == "cadastra") {
                        $idstab = explode(",",$ltabplan['idtabulacao']);
                    }
                    else {
                        $idstab[] = $ltabplan['idtabulacao']; 
                    }
                }
            }
            foreach($idstab as $tab) {
                $seltab = "SELECT * FROM tabulacao WHERE idtabulacao='$tab' GROUP BY idperguntatab";
                $eseltab = $_SESSION['query']($seltab) or die ("erro na query de consulta das perguntas da tabulacao");
                while($lseltab = $_SESSION['fetch_array']($eseltab)) {
                    if($lseltab['obriga'] == "S") {
                        if($idstabula['idpergtab'.$lseltab['idperguntatab']."_".$tab] == "") {
                            $errotab++;
                        }
                        else {
                            $this->idspergtab[$idplan][$tab][$lseltab['idperguntatab']] = $idstabula['idpergtab'.$lseltab['idperguntatab']."_".$tab];
                        }
                    }
                    else {
                        if($idstabula['idpergtab'.$lseltab['idperguntatab']."_".$tab] == "") {
                        }
                        else {
                            $this->idspergtab[$idplan][$tab][$lseltab['idperguntatab']] = $idstabula['idpergtab'.$lseltab['idperguntatab']."_".$tab];
                        }
                    }
                }
            }
            $errovazio = 0;
            foreach($idsresptemp as $idtemp) {
                if($idtemp != "") {
                    $nresp[] = $idtemp;
                    $selperg = "SELECT * FROM pergunta WHERE idresposta='$idtemp'";
                    $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da pergunta relacionada a resposta");
                    $idsresp[$idtemp] = $eselperg['idpergunta'];
                }
                else {
                    $errovazio++;
                }
            }

            foreach($idsresp as $idresp => $idperg) {
                if($idresp != "") {
                    $idsrespsoma = array();
                    $vresp = "";
                    $grupofg = "";
                    $verifresp = "SELECT * FROM pergunta WHERE idresposta='$idresp'";
                    $everif = $_SESSION['fetch_array']($_SESSION['query']($verifresp)) or die (mysql_error());
                    $checkfgp = "select COUNT(*) as result, fg from pergunta p inner join grupo g on g.idrel = p.idpergunta where idresposta='$idresp'";
                    $echeckfgp = $_SESSION['fetch_array']($_SESSION['query']($checkfgp)) or die ("erro na query de verificação da pergunta se está em um grupo FG");
                    $checkfgs = "select COUNT(*) as result, fg from pergunta p 
                                inner join subgrupo s on s.idpergunta = p.idpergunta 
                                inner join grupo g on g.idrel = s.idsubgrupo
                                where idresposta='$idresp'";
                    $echeckfgs = $_SESSION['fetch_array']($_SESSION['query']($checkfgs)) or die ("erro na query de de verificação da pergunta se é FG no subgrupo");
                    if($echeckfgp['result'] != 0) {
                          $grupofg = $echeckfgp['fg'];
                    }
                    if($echeckfgs['result'] != 0) {
                          $grupofg = $echeckfgs['fg'];
                    }
                    
                    // condição que permite que a campo observação fique diponível definitivamente
                    $descri = $idsdescri['descri'.$idperg];
                    if($descri != "") {
                        if(!in_array($everif['idpergunta'],$this->pergpontua[$idplan])) {
                            $this->pergpontua[$idplan][] = $everif['idpergunta'];
                            $this->descriresp[$idplan][] = $descri;
                        }
                    }
                }
            }
            foreach($idsresp as $idrespvf => $idverif) {
                if($idrespvf != "") {
                    $verifresp = "SELECT * FROM pergunta WHERE idresposta='$idrespvf'";
                    $everif = $_SESSION['fetch_array']($_SESSION['query']($verifresp)) or die ("erro na query de consulta dos dados da resposta");
                    $checkfgp = "select COUNT(*) as result, fg from pergunta p inner join grupo g on g.idrel = p.idpergunta where idresposta='$idrespvf'";
                    $echeckfgp = $_SESSION['fetch_array']($_SESSION['query']($checkfgp)) or die ("erro na query de verificação da pergunta se é ou não FG");
                    $checkfgs = "select COUNT(*) as result, fg from pergunta p inner join subgrupo s on s.idpergunta = p.idpergunta inner join grupo g on g.idrel = s.idsubgrupo
                                          where idresposta='$idrespvf'";
                    $echeckfgs = $_SESSION['fetch_array']($_SESSION['query']($checkfgs)) or die ("erro na query de de verificação da pergunta se é FG no subgrupo");
                    if($everif['avalia'] != "FG" && $everif['avalia'] != "FGM") {
                        $respconflitos[$everif['idpergunta']."-".$everif['valor_perg']][] = $everif['valor_resp'];
                    }
                    if($everif['avalia'] == "AVALIACAO_OK") {
                        if(in_array($everif['idpergunta'], $pergpontua)) {
                            $vobsconflict++;
                        }
                    }
                }
            }
            foreach($respconflitos as $idpergk => $respval) {
                $somarespostas = 0;
                $checkperg = explode("-", $idpergk);
                foreach($respval as $val) {
                    $somarespostas += $val; 
                }
                if($somarespostas > $checkperg[1]) {
                    $vobsconflict++;
                }
            }
            if($vobsconflict >= 1) {
                $selplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$idplan'";
                $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta do nome da planilha");
                $conflictplan[] = $nomeplan;
            }

            $this->nperg = count($idsperguntas['idperg'.$idplan]);
            $this->nresp = count($nresp);

            if($this->nresp < $this->nperg OR $errovazio >= 1) {
                if($this->tipoplan == "MONITOR" OR $this->tipoplan ==  "EDITAMONI") {
                    $this->retheader[] = "TODAS AS PERGUNTAS DA PLANILHA ''$nomeplan'' DEVEM ESTAR PREENCHIDAS PARA QUE A MONITORIA SEJA SALVA!!!";
                    if($this->tipoplan == "EDITAMONI") {
                        //$this->retheader = "location:/monitoria_supervisao/inicio.php?&menu=idmoni&idmoni=$this->idmoni&msg=".$msg;
                    }
                    if($this->tipoplan == "MONITOR") {
                        //$this->retheader = "location:/monitoria_supervisao/monitor/planmoni.php?&planmoni=".$this->idplan."&idfila=".$this->idfila."&tipomoni=".$this->tipomoni."&msg=".$msg;
                    }
                }
                if($this->tipoplan == "CALIBRAGEM") {
                    $this->retheader[] = "TODAS AS PERGUNTAS DA PLANILHA ''$nomeplan'' DEVEM ESTAR PREENCHIDAS PARA QUE A MONITORIA SEJA SALVA!!!";
                    //$this->retheader = "location:/monitoria_supervisao/inicio.php?&menu=calibragem&opmenu=REAL. CALIBRAGEM&submenu=OK&idcalib=".$this->idcalib."&idmoni=".$idmoni."&msg=".$msg;
                }
            }
        }

        if($vobsvazio != 0) {
            if($vobsvazio == 1) {
                $msg1 = "EXISTE $vobsvazio RESPOSTA QUE RETIRA VALOR DA AVALIAÇÃO MAS QUE NÃƒO POSSUE OBSERVAÇÃO NA PLANILHA ".implode(", ",$vazioplan)." !!!";
            }
            else {
                $msg1 = "EXISTEM $vobsvazio RESPOSTAS QUE RETIRAM VALOR DA AVALIAÇÃO MAS QUE NÃƒO POSSUEM OBSERVAÇÃO NAS PLANILHAS ".implode(", ",$vazioplan)." !!!";
            }
        }
        if($vobsconflict != 0) {
            if($vobsconflict == 1) {
                $msg2 = "EXISTE $vobsconflict PERGUNTA COM SUAS RESPOSTAS EM CONFLITO NA PLANILHA ".implode(", ",$conflictplan)." !!!";
            }
            else {
                $msg2 = "EXISTEM $vobsconflict PERGUNTAS COM SUAS RESPOSTAS EM CONFLITO NAS PLANILHAS ".implode(", ",$conflictplan)." !!!";
            }
        }
        if($errotab >= 1) {
            $msg3 = "A TABULAÇÃO NÃO ESTÁ COMPLETA, FALTAM RESPOSTAS A SEREM PREENCHIDAS";
        }
        if(isset($msg1) OR isset($msg2) OR isset($msg3)) {
            if($msg1 != "") {
                $this->retheader[] = $msg1;
            }
            if($msg2 != "") {
                $this->retheader[] = $msg2;
            }
            if($msg3 != "") {
                $this->retheader[] = $msg3;
            }
        }
        if($this->retheader == "") {
            $this->veriftab = 1;
            return true;            
        }
        else {
            $this->veriftab = 1;
            return false;
        }
    }

    function DadosMoni($pagina,$idsaval,$idsgrupo,$idssub,$idsperg,$idsresp) {
        //$this->idrelfiltro = $_POST['idrelfiltro'.$this->plan];
        //$this->idupload = $_POST['idupload'];
        if($this->tipomoni == "G") {
            $this->sqltab = "fila_grava";
        }
        if($this->tipomoni == "O") {
            $this->sqltab = "fila_oper";
        }
        if($this->sqltab == "fila_grava") {
            $seldados = "SELECT fg.idoperador, fg.idsuper_oper, fg.camaudio, fg.arquivo, cr.idparam_moni, cr.idfluxo, pm.tpaudio, u.camupload, fg.datactt,fg.horactt, pm.checkfg,tipoimp FROM fila_grava fg
                        INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros
                        INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                        INNER JOIN upload u ON u.idupload = fg.idupload
                        WHERE fg.idfila_grava='".$this->idfila."'";
        }
        if($this->sqltab == "fila_oper") {
            $seldados = "SELECT fo.idoperador, fo.idsuper_oper, fo.qtdmoni, cr.idparam_moni, cr.idfluxo, pm.camaudio, u.camupload,pm.checkfg,tipoimp FROM fila_oper fo
                        INNER JOIN conf_rel cr ON cr.idrel_filtros = fo.idrel_filtros
                        INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                        INNER JOIN upload u ON u.idupload = fo.idupload
                        WHERE fo.idfila_oper='".$this->idfila."'";
        }
        $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta dos dados da fila");
        $this->dados = $eseldados;
        $this->checkfg = $eseldados['checkfg'];
        $this->datactt = $eseldados['datactt'];
        $this->horactt = $eseldados['horactt'];
        $this->qtdmoni = $eseldados['qtdmoni'];
        $this->semdados = $eseldados['semdados'];
        $this->type = $eseldados['tpaudio'];
        if($eseldados['tipoimp'] == "I") {
            $this->gravacao = $eseldados['arquivo'];
        }
        else {
            $this->gravacao = $eseldados['camaudio'] . "/" . $eseldados['arquivo'];
        }
        $selfluxo = "SELECT cr.idfluxo FROM conf_rel cr WHERE cr.idrel_filtros='$this->idrelfiltro'";
        $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die ("erro na query de consulta do fluxo relacionado ao relacionamento do filtro");
        $this->idfluxo = $eselfluxo['idfluxo'];
        if($this->tipoplan == "MONITOR") {
            $this->idmonitor = $_SESSION['usuarioID'];
        }
        if($this->tipoplan == "CALIBRAGEM") {
            $this->iduser = $_SESSION['participante'];
        }
        $this->idsaval = $idsaval['idaval'.$this->plan];
        $this->vaval = $_POST['valor_aval'];
        $this->idsgrup = $idsgrupo['idgrup'.$this->plan];
        $this->vgrup = $_POST['valor_grup'];
        $this->idssub = $idssub['idsub'.$this->plan];
        $this->vsub = $_POST['valor_sub'];
        $this->idsperg = $idsperg['idperg'.$this->plan];
        $this->vperg = $_POST['valor_perg'];
        $this->idsresp = $idsresp['idresp'.$this->plan];
        $vobsvazio = 0;
        $vobsconflict = 0;
        $pergpontua = array();

        $this->cadobs = array_combine($this->pergpontua[$this->plan], $this->descriresp[$this->plan]);
    }

    function SqlMoni($horaini, $horafim, $inilig, $tmpaudio, $gravacao, $data, $type,$alertas) {
            if(isset($this->tipoplan) AND $this->tipoplan == "CALIBRAGEM") {
                $tabmoni = "monitoriacalib";
            }
            if(isset($this->tipoplan) AND $this->tipoplan == "MONITOR" OR $this->tipoplan ==  "EDITAMONI") {
                $tabmoni = "monitoria";
            }
            $this->monicol = array();
            $this->monivalor = array();
            $verifoper = "SELECT * FROM operador WHERE operador='NAO LOCALIZADO'";
            $everifoper = $_SESSION['fetch_array']($_SESSION['query']($verifoper)) or die ("erro na query de consulta do operaor NAO LOCALIZADO");
            /* if($this->semdados == "S" && $this->idoper != $everifoper['idoperador']) {
                $listaoper = "SELECT *,count(*) as r FROM listaoperador WHERE idlistaoperador='$this->idoper'";
                $elistaoper = $_SESSION['fetch_array']($_SESSION['query']($listaoper)) or die ("erro na query de consulta da lista de operadores");
                if($elistaoper['r'] >= 1) {
                $seloper = "SELECT *, COUNT(*) as result FROM operador WHERE operador='".$elistaoper['operador']."'";
                $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die ("erro na query de consulta do operador");
                if($eseloper['result'] >= 1) {
                    $this->idoper = $eseloper['idoperador'];
                    $this->idsuper = $eseloper['idsuper_oper'];
                }
                else {
                    $dataoper = date('Y-m-d');
                    $horaoper = date('H:i:s');
                    $taboper = array('operador','super_oper');
                    lock($taboper);
                    $selsuper = "SELECT *, COUNT(*) as result FROM super_oper WHERE super_oper='".$elistaoper['super_oper']."'";
                    $eselsuper = $_SESSION['fetch_array']($_SESSION['query']($selsuper)) or die ("erro na query de consulta do supervisor");
                    if($eselsuper['result'] >= 1) {
                        $this->idsuper = $eselsuper['idsuper_oper'];
                    }
                    else {
                        $insertsuper = "INSERT INTO super_oper (super_oper,cpfsuper,dtbase,horabase) VALUES ('".$elistaoper['super_oper']."','".$elistaoper['cpfsuper']."','$dataoper','$horaoper')";
                        $einsertsuper = $_SESSION['query']($insertsuper) or die ("erro na query de inserção do supervisor");
                        $this->idsuper = $_SESSION['insert_id']();
                    }
                    $insertoper = "INSERT INTO operador (operador,cpfoper,idsuper_oper,dtbase,horabase) VALUES ('".$elistaoper['operador']."','".$elistaoper['cpfoper']."','$this->idsuper','$dataoper','$horaoper')";
                    $einsertoper = $_SESSION['query']($insertoper) or die ("erro na query para inserir operador");
                    $this->idoper = $_SESSION['insert_id']();
                    unlock();
                }
            }
            else {
                    $this->idoper = $everifoper['idoperador'];
                    $this->idsuper = $everifoper['idsuper_oper'];
                //}
            }*/
            //else {
                if($this->idsuper == "") {
                    $this->idsuper = $everifoper['idsuper_oper'];
                }
            //}
            $selcolum = "SHOW COLUMNS FROM $tabmoni";
            $ecolum = $_SESSION['query']($selcolum) or die (mysql_error());
            while($lcolum = $_SESSION['fetch_array']($ecolum)) {
              if($lcolum['Field'] != 'id'.$tabmoni) {
              }
              if($lcolum['Field'] == 'idagcalibragem') {
                  $this->monicol[] = $lcolum['Field'];
                  $this->monivalor[] = "'$this->idcalib'";
              }
              if($lcolum['Field'] == 'idrel_filtros') {
                  $this->monicol[] = $lcolum['Field'];
                  $this->monivalor[] = "'$this->idrelfiltro'";
              }
              if($lcolum['Field'] == 'iduser') {
                  $this->monicol[] = $lcolum['Field'];
                  $this->monivalor[] = "'$this->iduser'";
              }
              if($lcolum['Field'] == 'idmonitor') {
                  if($this->tipoplan == "CALIBRAGEM") {
                      $this->monicol[] = $lcolum['Field'];
                      $this->monivalor[] = "'0'";
                  }
                  else {
                    $this->monicol[] = $lcolum['Field'];
                    $this->monivalor[] = "'$this->idmonitor'";
                  }
              }
              if($lcolum['Field'] == 'idoperador') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$this->idoper'";
              }
              if($lcolum['Field'] == 'idsuper_oper') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$this->idsuper'";
              }
              if($lcolum['Field'] == 'horaini') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$horaini'";
              }
              if($lcolum['Field'] == "datactt") {
                  $this->monicol[] = $lcolum['Field'];
                  if($this->sqltab == "fila_oper") {
                    $this->monivalor[] = "'$data'";  
                  }
                  else {
                    $this->monivalor[] = val_vazio($this->datactt);
                  }
              }
              if($lcolum['Field'] == "horactt") {
                  $this->monicol[] = $lcolum['Field'];
                  if($this->sqltab == "fila_oper") {
                    $this->monivalor[] = "'$hora'";  
                  }
                  else {
                    $this->monivalor[] = val_vazio($this->horactt);
                  }
              }
              if($lcolum['Field'] == 'horafim') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$horafim'";
              }
              if($lcolum['Field'] == 'inilig') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$inilig'";
              }
              if($lcolum['Field'] == 'tmpaudio') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$tmpaudio'";
              }
              if($lcolum['Field'] == 'idplanilha') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$this->plan'";
              }
              if($lcolum['Field'] == 'idmonitoriavinc') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'0'";
              }
              if($lcolum['Field'] == 'idmonitoriacalibvinc') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'0'";
              }
              if($lcolum['Field'] == 'gravacao') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$gravacao'";
              }
              if($lcolum['Field'] == 'data') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$data'";
              }
              if($lcolum['Field'] == 'idfila') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$this->idfila'";
              }
              if($lcolum['Field'] == 'tabfila') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'$this->sqltab'";
              }
              if($lcolum['Field'] == 'obs') {
                $this->monicol[] = $lcolum['Field'];
                $this->monivalor[] = "'".str_replace("+"," ",$this->obs[$this->plan])."'";
              }
              if($lcolum['Field'] == 'checkfg') {
                  $this->monicol[] = $lcolum['Field'];
                  if($this->checkfg == "S") {
                        $this->monivalor[] = "NULL";
                  }
                  else {
                        $this->monivalor[] = "'NCG OK'";
                  }
              }
              if($lcolum['Field'] == "tmonitoria") {
                  $this->monicol[] = $lcolum['Field'];
                  if($_SESSION['user_tabela'] == "user_adm" OR $_SESSION['user_tabela'] == "user_adm") {
                        $this->monivalor[] = "'I'";
                  }
                  else {
                        $this->monivalor[] = "'".$_SESSION['tmonitor']."'";
                  }
              }
              if($lcolum['Field'] == "alertas") {
                  $this->monicol[] = $lcolum['Field'];
                  if(implode(",",$alertas["idalerta".$this->plan]) == "") {
                      $this->monivalor[] = "NULL";
                  }
                  else {
                      $this->monivalor[] = "'".implode(",",$alertas["idalerta".$this->plan])."'";
                  }
              }
            //}
        }
    }

    function SalvaMoni($tipo, $data, $horafim) {
        if(isset($this->tipoplan) AND $this->tipoplan == "CALIBRAGEM") {
            $this->tabmoni = "monitoriacalib";
            $this->tabavalia = "moniavaliacalib";
            //lock(array($this->tabmoni,$this->tabavalia,'rel_fluxo','planilha pa','rel_aval ra','pergunta p','grupo g','subgrupo s','monicalibtabulacao',
            //    'fila_grava','fila_grava fg','fila_oper','fila_oper fo','camposparam cp','conf_rel cr','coluna_oper co','param_moni pm'));
        }
        if(!isset($this->tipoplan) OR $this->tipoplan == "MONITOR" OR $this->tipoplan ==  "EDITAMONI") {
            $this->tabmoni = "monitoria";
            $this->tabavalia = "moniavalia";
            //lock(array($this->tabmoni,$this->tabavalia,'moniavalia ma','rel_fluxo','monitoria_fluxo','histresp','planilha pa',
            //    'rel_aval ra','pergunta p','grupo g','subgrupo s','monitabulacao','fila_grava','fila_grava fg','fila_oper','fila_oper fo','camposparam cp',
            //    'conf_rel cr','coluna_oper co','param_moni pm'));
        }
          $pergssub = array();
          $pergsgrup = array();
          foreach($this->idsresp as $idresp) {
            $selperg = "SELECT DISTINCT(idpergunta) as id_perg FROM pergunta p WHERE idresposta='$idresp'";
            $epergrel = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die (mysql_error());
            $selsub = "SELECT COUNT(*) as result, idpergunta FROM subgrupo s WHERE idpergunta='".$epergrel['id_perg']."'";
            $esubrel = $_SESSION['fetch_array']($_SESSION['query']($selsub)) or die (mysql_error());
            if($esubrel['result'] >= 1) {
              $pergssub[$idresp] = $esubrel['idpergunta'];
            }
            else {
              $pergsgrup[$idresp] = $epergrel['id_perg'];
            }
          }
          $respgrup = array();
          $respsub = array();
          foreach($pergsgrup as $kpergresp => $perggrup) {
              if($this->acao == "edita") {
                  if(in_array($this->idmoni,$this->idsmoni)) {
                  }
                  else {
                    $this->idsmoni[] = $this->idmoni;
                  }
                  $selresp = "SELECT COUNT(*) as result, p.idpergunta FROM moniavalia ma
                              INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                              INNER JOIN pergunta p ON p.idpergunta = g.idrel AND p.idresposta = ma.idresposta
                              WHERE ma.idresposta='$kpergresp' AND ma.idplanilha='$this->plan' AND g.filtro_vinc='P' AND ma.idmonitoria='$this->idmoni'";
                  $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na query de consulta da planilha realacionada");
                  if($eselresp['result'] == 0) {
                      $respgrup[$kpergresp] = $perggrup;
                      if(in_array($perggrup,$histrespgrup)) {
                      }
                      else {
                        $histrespgrup[] = $perggrup;
                      }
                  }
              }
              else {
                  $selresp = "SELECT COUNT(*) as result, p.idpergunta FROM pergunta p
                             INNER JOIN grupo g ON g.idrel = p.idpergunta
                             INNER JOIN planilha pa ON pa.idgrupo = g.idgrupo
                             WHERE p.idresposta='$kpergresp' AND pa.idplanilha='$this->plan' AND g.filtro_vinc='P'";
                  $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na query de consulta da planilha realacionada");
                  if($eselresp['result'] == 0) {
                  }
                  else {
                      $respgrup[$kpergresp] = $perggrup;
                  }
              }
          }
          foreach($pergssub as $ksubresp => $pergsub) {
              if($this->acao == "edita") {
                  $selresps = "SELECT COUNT(*) as result, p.idpergunta FROM moniavalia ma
                              INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                              INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                              INNER JOIN pergunta p ON p.idpergunta = s.idpergunta AND p.idresposta = ma.idresposta
                              WHERE ma.idresposta='$ksubresp' AND ma.idplanilha='$this->plan' AND g.filtro_vinc='S' AND ma.idmonitoria='$this->idmoni'";
                  $eselresps = $_SESSION['fetch_array']($_SESSION['query']($selresps)) or die ("erro na query de consulta dos subgrupos realacionados");
                  if($eselresps['result'] == 0) {
                      $respsub[$ksubresp] = $pergsub;
                      $histrespsub[] = $persub;
                  }
                  else {
                  }

              }
              else {
                  $selresps = "SELECT COUNT(*) as result FROM pergunta p
                             INNER JOIN subgrupo s ON s.idpergunta = p.idpergunta
                             INNER JOIN grupo g ON g.idrel = s.idsubgrupo
                             INNER JOIN planilha pa ON pa.idgrupo = g.idgrupo
                             WHERE p.idresposta='$ksubresp' AND pa.idplanilha='$this->plan' AND g.filtro_vinc='S'";
                  $eselresps = $_SESSION['fetch_array']($_SESSION['query']($selresps)) or die ("erro na query de consulta da planilha realacionada");
                  if($eselresps['result'] == 0) {
                  }
                  else {
                      $respsub[$ksubresp] = $pergsub;
                  }
              }
          }

          if(!isset($this->tipoplan) OR $this->tipoplan == "MONITOR" OR $this->tipoplan ==  "EDITAMONI") {
              // verifica se alguma resposta tem que ser excluida
              
              $selrespbd = "SELECT ma.idresposta,ma.idpergunta FROM moniavalia ma WHERE ma.idplanilha='$this->plan' AND ma.idmonitoria='$this->idmoni'";
              $erespbd = $_SESSION['query']($selrespbd) or die ("erro na query de consulta das respostas da monitoria");
              while($lrespbd = $_SESSION['fetch_array']($erespbd)) {
                  $respgrupbd[$lrespbd['idresposta']] = $lrespbd['idpergunta'];
              }
              $exrespgrup = array_diff_key($respgrupbd,$pergsgrup);
              
              // levanta o id do status e definicao inicial para a monitoria, se for ediÃ§Ã£o no fluxo levanta o status para a definiÃ§Ã£o colocada
              if($this->acao == "edita") {
                  $selfluxo = "SELECT idrel_fluxo, idstatus, iddefinicao FROM rel_fluxo WHERE idfluxo='$this->idfluxo' AND iddefinicao='$this->iddefini' AND tipo<>'inicia'";
              }
              else {
                  $selfluxo = "SELECT idrel_fluxo, idstatus, iddefinicao FROM rel_fluxo WHERE idfluxo='$this->idfluxo' AND tipo='inicia'";
              }
              $lfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta do fluxo");
              $nfluxo = $_SESSION['num_rows']($lfluxo);
              if($nfluxo >= 1) {
                  $eselfluxo = $_SESSION['fetch_array']($lfluxo) or die ("erro na query de consulta do rel_fluxo");
                  if($this->acao == "edita") {
                      $idstatus = $eselfluxo['idstatus'];
                  }
                  else {
                      $idstatus = $eselfluxo['idstatus'];
                  }
                  $idrelfluxo = $eselfluxo['idrel_fluxo'];
                  $iddefini = $eselfluxo['iddefinicao'];
              }
          }

          $idperggrup = implode(",", array_keys($respgrup));
          $idpergsub = implode(",", array_keys($respsub));
          if($idperggrup == "" && $idpergsub == "" && $exrespgrup == "") {
              $this->altera = "N";
          }
          else {
              $this->altera = "S";
          }
          $sqlvar = implode(",",$this->monicol);
          $sqlval = implode(",",$this->monivalor);
          if($this->acao == "edita") {
              if($this->semdados == "S") {
                  $selcol = "SELECT co.nomecoluna,co.tipo,co.incorpora FROM camposparam cp
                            INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                            INNER JOIN param_moni pm ON pm.idparam_moni=cp.idparam_moni
                            INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                            WHERE cr.idrel_filtros='$this->idrelfiltro'";
                  $eselcol = mysql_query($selcol) or die ("erro na query de consulta das colunas da fila");
                  while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                      if($lselcol['nomecoluna'] == "operador") {
                          $valupfila[] = "idoperador=".val_vazio($this->idoper)."";
                      }
                      if($lselcol['nomecoluna'] == "super_oper") {
                          $valupfila[] = "idsuper_oper=".val_vazio($this->idsuper)."";
                      }
                      if($lselcol['nomecoluna'] != "super_oper" && $lselcol['nomecoluna'] != "operador") {
                          if($lselcol['incorpora'] == "DADOS") {
                            if($lselcol['tipo'] == "DATE") {  
                                $valupfila[] = $lselcol['nomecoluna']."=".val_vazio(data2banco($this->dadosdig[$lselcol['nomecoluna']]))."";
                            }
                            else {
                                $valupfila[] = $lselcol['nomecoluna']."=".val_vazio(addslashes($this->dadosdig[$lselcol['nomecoluna']]))."";
                            }
                          }
                      }
                      
                  }
                  $upfila = "UPDATE $this->sqltab SET ".implode(",",$valupfila)." WHERE id$this->sqltab='$this->idfila'";
                  $eupfila = $_SESSION['query']($upfila) or die ("erro na query de atualização dos dados da fila");                  
              }
          }
          else {
              $insertmoni = "INSERT INTO $this->tabmoni ($sqlvar) VALUE ($sqlval)";
              $einsertmoni = $_SESSION['query']($insertmoni) or die (mysql_error());
              $this->idmoni = $_SESSION['insert_id']();
              
              //vinculo da monitoria de retraçao do Cliente Itaú com a monitoria de venda, parte exclussiva.
              if($this->tipoplan == "CALIBRAGEM") {
              }
              else {
                $retgrava = "SELECT COUNT(*) as result FROM coluna_oper co
                             INNER JOIN camposparam cp on cp.idcoluna_oper = co.idcoluna_oper
                             INNER JOIN conf_rel cr ON cr.idparam_moni = cp.idparam_moni
                             INNER JOIN fila_grava fg ON fg.idrel_filtros = cr.idrel_filtros
                             WHERE fg.idfila_grava='$this->idfila' AND nomecoluna='idmonitoriavenda'";
                $eretgrava = $_SESSION['fetch_array']($_SESSION['query']($retgrava)) or die (mysql_error());
                $retoper = "SELECT COUNT(*) as result FROM coluna_oper co
                             INNER JOIN camposparam cp on cp.idcoluna_oper = co.idcoluna_oper
                             INNER JOIN conf_rel cr ON cr.idparam_moni = cp.idparam_moni
                             INNER JOIN fila_oper fo ON fo.idrel_filtros = cr.idrel_filtros
                             WHERE fo.idfila_oper='$this->idfila' AND nomecoluna='idmonitoriavenda'";
                $eretoper = $_SESSION['fetch_array']($_SESSION['query']($retoper)) or die (mysql_error());
                if($eretgrava['result'] >= 1 || $eretoper['result'] >= 1) {
                    $ck = 0;
                    $idmonialt = "";
                    if($eretgrava['result'] >= 1) {
                        $selidvenda = "SELECT idmonitoriavenda FROM fila_grava WHERE idfila_grava='$this->idfila'";
                        $eidvenda = $_SESSION['fetch_array']($_SESSION['query']($selidvenda)) or die (mysql_error());
                        if($eidvenda['idmonitoriavenda'] != "") {
                          $idmonicheck = $eidvenda['idmonitoriavenda'];
                          while($ck == 0) {
                              $checkmoni = "SELECT idmonitoria,idmonitoriavinc,count(*) as result FROM monitoria WHERE idmonitoria='$idmonicheck'";
                              $echeckmoni = $_SESSION['fetch_array']($_SESSION['query']($checkmoni)) or die (mysql_error());
                              if($echeckmoni['idmonitoriavinc'] == "0000000") {
                                  $ck = 1;
                                  $idmonialt = $echeckmoni['idmonitoria'];
                              }
                              else {
                                  $idmonicheck = $echeckmoni['idmonitoriavinc'];
                              }
                          }
                          $upmoniret = "UPDATE monitoria SET idmonitoriavinc='$this->idmoni' WHERE idmonitoria='$idmonialt'";
                          $eupmoniret = $_SESSION['query']($upmoniret) or die (mysql_error());
                        }
                    }
                    if($eretoper['result'] >= 1) {
                        if($eretoper['idmonitoriavenda'] != "") {
                          $selidvenda = "SELECT idmonitoriavenda FROM fila_grava WHERE idfila_grava='$this->idfila'";
                          $eidvenda = $_SESSION['fetch_array']($_SESSION['query']($selidvenda)) or die (mysql_error());
                          $idmonicheck = $eidvenda['idmonitoriavenda'];
                          while($ck == 0) {
                              $checkmoni = "SELECT idmonitoria,idmonitoriavinc,count(*) as result FROM monitoria WHERE idmonitoria='$idmonicheck'";
                              $echeckmoni = $_SESSION['fetch_array']($_SESSION['query']($checkmoni)) or die (mysql_error());  
                              if($echeckmoni['idmonitoriavinc'] == "0000000") {
                                  $ck = 1;
                                  $idmonialt = $echeckmoni['idmonitoria'];
                              }
                              else {
                                  $idmonicheck = $echeckmoni['idmonitoriavinc'];
                              }
                          }
                          $upmoniret = "UPDATE monitoria SET idmonitoriavinc='$this->idmoni' WHERE idmonitoria='$idmonialt'";
                          $eupmoniret = $_SESSION['query']($upmoniret) or die (mysql_error());
                        }
                    }
                }
              }
              //
              
              $this->idsmoni[] = $this->idmoni;
              if($this->semdados == "S") {
                  $selcol = "SELECT co.nomecoluna,co.tipo,co.incorpora FROM camposparam cp
                            INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                            INNER JOIN param_moni pm ON pm.idparam_moni=cp.idparam_moni
                            INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                            WHERE cr.idrel_filtros='$this->idrelfiltro'";
                  $eselcol = mysql_query($selcol) or die ("erro na query de consulta das colunas da fila");
                  while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                      if($lselcol['nomecoluna'] == "operador") {
                          $valupfila[] = "idoperador=".val_vazio($this->idoper)."";
                      }
                      if($lselcol['nomecoluna'] == "super_oper") {
                          $valupfila[] = "idsuper_oper=".val_vazio($this->idsuper)."";
                      }
                      if($lselcol['nomecoluna'] != "super_oper" && $lselcol['nomecoluna'] != "operador") {
                          if($lselcol['incorpora'] == "DADOS") {
                            if($lselcol['tipo'] == "DATE") {  
                                $valupfila[] = $lselcol['nomecoluna']."=".val_vazio(data2banco($this->dadosdig[$lselcol['nomecoluna']]))."";
                            }
                            else {
                                $valupfila[] = $lselcol['nomecoluna']."=".val_vazio(addslashes($this->dadosdig[$lselcol['nomecoluna']]))."";
                            }
                          }
                      }
                      
                  }
                  $upfila = "UPDATE $this->sqltab SET ".implode(",",$valupfila)." WHERE id$this->sqltab='$this->idfila'";
                  $eupfila = $_SESSION['query']($upfila) or die ("erro na query de atualização dos dados da fila");                  
              }
              unset($_SESSION['horaini']);
              unset($_SESSION['horafim']);
              unset($_SESSION['inilig']);
              unset($_SESSION['codini']);
              unset($_SESSION['codfim']);
              $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
              //$delgrava = unlink($rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($this->gravacao));
          }

          // insere tabulaÃ§Ã£o caso nÃ£o exista ou modifica a taubulaÃ§Ã£o em monitorias em estado de ediÃ§Ã£o
          if($this->idspergtab != "") {
              if($this->tipoplan == "CALIBRAGEM") {
                  $tabcad = "monicalibtabulacao";
                  $colmoni = "monitoriacalib";
              }
              else {
                  $tabcad = "monitabulacao";
                  $colmoni = "monitoria";
              }
              foreach($this->idspergtab[$this->plan] as $idtab => $arraytab) {
                  $idspergtab = "";
                  if($this->tipoplan != "CALIBRAGEM") {
                      $selmonitab = "SELECT * FROM $tabcad WHERE idmonitoria='$this->idmoni' AND idtabulacao='$idtab'";
                      $eseltab = $_SESSION['query']($selmonitab) or die ("erro na query de consulta da tabulaÃ§Ã£o");
                      $nseltab = $_SESSION['num_rows']($eseltab);
                      if($nseltab >= 1) {
                          while($lseltab = $_SESSION['fetch_array']($eseltab)) {
                              $idspergtab[$lseltab['idperguntatab']] = $lseltab['idrespostatab'];
                          }
                      }
                  }
                  
                  // fase insert ou update
                  foreach($arraytab as $idpergtab => $idresptab) {
                      if($idspergtab == "") {
                          $inserttab = "INSERT INTO $tabcad (id$colmoni,idtabulacao,idperguntatab,idrespostatab) VALUES ($this->idmoni,$idtab,$idpergtab,$idresptab)";
                          $einserttab = $_SESSION['query']($inserttab) or die ("erro na query de cadastro da tabulacao");
                      }
                      else {
                          if(key_exists($idpergtab,$idspergtab)) {
                              if($idresptab == $idspergtab[$idpergtab]) {
                              }
                              else {
                                  $updatetab = "UPDATE $tabcad SET idrespostatab=".val_vazio($idresptab)." WHERE id$colmoni='$this->idmoni' AND idtabulacao='$idtab' AND idperguntatab='$idpergtab'";
                                  $eupdatetab = $_SESSION['query']($updatetab) or die ("erro na query de atualizaÃ§Ã£o da tabulaÃ§Ã£o");
                              }
                          }
                          else {
                              $inserttab = "INSERT INTO $tabcad (id$icolmoni,idtabulacao,idperguntatab,idrespostatab) VALUES ($this->idmoni,$idtab,$idpergtab,$idresptab)";
                              $einserttab = $_SESSION['query']($inserttab) or die ("erro na query de cadastro da tabulacao");
                          }                          
                      }
                  }
                  
                  // fase delete
                  foreach($idspergtab as $idp => $idr) {
                      if(key_exists($idp,$this->idspergtab[$this->plan][$idtab])) {
                      }
                      else {
                          $delpergtab = "DELETE FROM monitabulacao WHERE idmonitoria='$this->idmoni' AND idtabulacao='$idtab' AND idperguntatab='$idp' AND idrespostatab='$idr'";
                          $edelperg = $_SESSION['query']($delpergtab) or die ("erro na query para apagar a pergunta da tabulaÃ§Ã£o respondida");
                      }
                  }
              }
          }
          
          if((!isset($this->tipoplan) OR $this->tipoplan == "MONITOR" OR $this->tipoplan ==  "EDITAMONI") && $this->acao != "edita") {
                $insertfluxo = "INSERT INTO monitoria_fluxo (idmonitoria, tabuser, iduser, idrel_fluxo, idstatus, iddefinicao, data, hora, obs) VALUES ('$this->idmoni', '$tipo', '".$_SESSION['usuarioID']."','$idrelfluxo', '$idstatus',
                '$iddefini', '$data', '$horafim', '$this->obsdefini')";
                $einsertfluxo = $_SESSION['query']($insertfluxo) or die ("erro na query de inserÃ§Ã£o do fluxo inicial");
                $insertfluxo = $_SESSION['insert_id']();
                $upmoni = "UPDATE monitoria SET idmonitoria_fluxo='$insertfluxo' WHERE idmonitoria='$this->idmoni'";
                $eupmoni = $_SESSION['query']($upmoni) or die ("erro na query de atualizaÃ§Ã£o da monitoria");
          }

          // levanta campos da tabela moniavalia
          $camposmoni = array();
          $campmoni = "SHOW COLUMNS FROM $this->tabavalia";
          $ecampos = $_SESSION['query']($campmoni) or die (mysql_error());
          while($lcampos = $_SESSION['fetch_array']($ecampos)) {
              $camposmoni[] = $lcampos['Field'];
          }
          $listcampmoni = implode(",",$camposmoni);
          $monialt = 0;

          // criar o histÃ³rico das respostas das perguntas da monitoiria alterada
          if($this->acao == "edita") {
              $del = 0;
              if($histrespgrup != "") {
                  $selfluxohist ="SELECT idmonitoria_fluxo FROM monitoria WHERE idmonitoria='$this->idmoni'";
                  $eselfluxoh = $_SESSION['fetch_array']($_SESSION['query']($selfluxohist)) or die ("erro na query de consulta do fluxo da monitoria");
                  foreach($histrespgrup as $hrespgrup) {
                      $delhist = "DELETE FROM histresp WHERE idmonitoria='$this->idmoni' AND idpergunta='$hrespgrup'";
                      $edelhist = $_SESSION['query']($delhist) or die ("erro na query para apagar o histÃ³rico");
                      $atuincre = "ALTER TABLE histresp AUTO_INCREMENT=1";
                      $eatuincre = $_SESSION['query']($atuincre) or die ("erro na query de atualizaÃ§Ã£o da tabela");
                      $selhist = "SELECT idaval_plan,idgrupo,idpergunta,idresposta FROM moniavalia WHERE idmonitoria='$this->idmoni' AND idplanilha='$this->plan' AND idpergunta='$hrespgrup'";
                      $eselhist = $_SESSION['query']($selhist) or die ("erro na query de consulta da pergunta historica");
                      while($lselhis = $_SESSION['fetch_array']($eselhist)) {
                          $del++;
                          $obs = val_vazio($lselhis['obs']);
                          $inserthist = "INSERT INTO histresp (idmonitoria_fluxo,idmonitoria,idplanilha,idaval_plan,idgrupo,idpergunta,idresposta,obs,data,hora,iduser_adm) VALUE ('".$eselfluxoh['idmonitoria_fluxo']."','$this->idmoni',
                                        '$this->plan','".$lselhis['idaval_plan']."','".$lselhis['idgrupo']."','".$lselhis['idpergunta']."','".$lselhis['idresposta']."',$obs,'".date('Y-m-d')."','".date('H:i:s')."','".$_SESSION['usuarioID']."')";
                          $einserthist = $_SESSION['query']($inserthist) or die ("erro na query para inserir a resposta histÃ³rico");
                      }
                  }
              }
              if($histrespsub != "") {
                  foreach($histrespsub as $hrespsub) {
                      if($del >= 1) {
                      }
                      else {
                          $delhist = "DELETE FROM histresp WHERE idmonitoria='$this->idmoni' AND idpergunta='$hrespsub'";
                          $edelhist = $_SESSION['query']($delhist) or die ("erro na query para apagar o histÃ³rico");
                          $atuincre = "ALTER TABLE histresp AUTO_INCREMENT=1";
                          $eatuincre = $_SESSION['query']($atuincre) or die ("erro na query de atualizaÃ§Ã£o da tabela");
                      }
                      $selhist = "SELECT idaval_plan,idgrupo,idpergunta,idresposta FROM moniavalia WHERE idmonitoria='$this->idmoni' AND idplanilha='$this->plan' AND idpergunta='$hrespsub'";
                      $eselhist = $_SESSION['query']($selhist) or die ("erro na query de consulta da pergunta historica");
                      while($lselhis = $_SESSION['fetch_array']($eselhist)) {
                          $obs = val_vazio($lselhis['obs']);
                          $inserthist = "INSERT INTO histresp (idmonitoria_fluxo,idmonitoria,idplanilha,idaval_plan,idgrupo,idsubgrupo,idpergunta,idresposta,obs,data,hora,iduser_adm) VALUE (''".$eselfluxoh['idmonitoria_fluxo']."'','$this->idmoni',
                                        '$this->plan','".$lselhis['idaval_plan']."','".$lselhis['idgrupo']."','".$lselhis['idsubgrupo']."','".$lselhis['idpergunta']."','".$lselhis['idresposta']."',$obs,'".date('Y-m-d')."','".date('H:i:s')."','".$_SESSION['usuarioID']."')";
                          $einserthist = $_SESSION['query']($inserthist) or die ("erro na query para inserir a resposta histÃ³rico");
                      }
                  }
              }
          }

          // criar ou edita a tabela moniavalia com todos os itens da monitoria, relacionando com planilha, avaliacao, grupo e pergunta
          if($idperggrup == "") {
          }
          else {
              if($this->acao == "edita") {
                   $tabsub = "SELECT DISTINCT(p.idresposta) as id_resp, pa.idplanilha as id_plan, pa.idaval_plan, ra.valor as valor_aval,
                              g.idgrupo as id_grup, g.valor_grupo,pa.tab as tab_grup,
                              p.idpergunta as id_perg, g.tab as tab_perg, p.valor_perg,
                              p.valor_resp, p.tipo_resp, p.avalia
                              FROM planilha pa
                              INNER JOIN rel_aval ra ON ra.idplanilha = pa.idplanilha
                              INNER JOIN grupo g ON g.idgrupo = pa.idgrupo
                              INNER JOIN pergunta p ON p.idpergunta = g.idrel
                              WHERE p.idresposta
                              IN ( $idperggrup ) AND pa.idplanilha=".$this->plan." AND filtro_vinc='P' GROUP BY p.idresposta ORDER BY p.idpergunta";
                   $etabsub = $_SESSION['query']($tabsub) or die (mysql_error());
                   while($ltabsub = $_SESSION['fetch_array']($etabsub)) {
                       $insert = array_keys($pergsgrup,$ltabsub['id_perg']);
                       if(count($insert) > 1) {
                           $idplan = $ltabsub['id_plan'];
                           $idaval = $ltabsub['idaval_plan'];
                           if($ltabsub['valor_aval'] == "") {
                               $vlaval = "NULL";
                           }
                           else {
                               $vlaval = $ltabsub['valor_aval'];
                           }
                           $idgrup = $ltabsub['id_grup'];
                           if($ltabsub['valor_grupo'] == "") {
                               $vlgrup = "NULL";
                           }
                           else {
                               $vlgrup = $ltabsub['valor_grupo'];
                           }
                           $tabg = $ltabsub['tab_grup'];
                           $idperg = $ltabsub['id_perg'];
                           $tiporesp = $ltabsub['tipo_resp'];
                           if($ltabsub['valor_perg'] == "") {
                               $vlperg = "NULL";
                               $vlfperg = "NULL";
                           }
                           else {
                               $vlperg = $ltabsub['valor_perg'];
                               $vlfperg = 0;
                           }
                           $tabp = $ltabsub['tab_perg'];
                           $id_resp = $ltabsub['id_resp'];
                           $avalia = $ltabsub['avalia'];
                           if($ltabsub['valor_resp'] == "") {
                               $vlresp = "NULL";
                           }
                           else {
                               $vlresp = $ltabsub['valor_resp'];
                           }
                           /* faz insert das perguntas da planilha na ptabela moniavaliacao, agrupando por id_resp */
                           $avaliasub = "INSERT INTO $this->tabavalia ($listcampmoni) VALUES ('".$this->idmoni."', '$idplan', '$idaval', $vlaval, 0, 0, '$idgrup',
                           $vlgrup, 0, '$tabg', NULL, NULL, NULL, NULL, '$idperg', '$tiporesp', $vlperg, $vlfperg,
                           '$tabp', '$id_resp', '$avalia', $vlresp,'')";
                           $eavaliasub = $_SESSION['query']($avaliasub) or die (mysql_error());
                       }
                       if(count($insert) == 1) {
                           if($ltabsub['valor_resp'] == "") {
                               $vlresp = "NULL";
                           }
                           else {
                               $vlresp = $ltabsub['valor_resp'];
                           }
                           $idrespalt = array_keys($exrespgrup,$ltabsub['id_perg']);
                           $idrespalt = $idrespalt[0];
                           $upavaliap = "UPDATE moniavalia SET valor_resp=$vlresp, idresposta='".$ltabsub['id_resp']."', avalia='".$ltabsub['avalia']."'
                                         WHERE idpergunta='".$ltabsub['id_perg']."' AND idplanilha='".$this->plan."' AND id$this->tabmoni='".$this->idmoni."' AND idresposta='$idrespalt'";
                           $eupavalia = $_SESSION['query']($upavaliap) or die ("erro na query de update do valor da resposta");
                       }
                       if($eupavalia OR $eavaliasub) {
                           $monialt++;
                       }
                   }
              }
              else {
                  $tabsub = "SELECT DISTINCT(p.idresposta) as id_resp, pa.idplanilha as id_plan, pa.idaval_plan, ra.valor as valor_aval,
                              g.idgrupo as id_grup, g.valor_grupo,pa.tab as tab_grup,
                              p.idpergunta as id_perg, g.tab as tab_perg, p.valor_perg,
                              p.valor_resp, p.tipo_resp, p.avalia
                              FROM planilha pa
                              INNER JOIN rel_aval ra ON ra.idplanilha = pa.idplanilha
                              INNER JOIN grupo g ON g.idgrupo = pa.idgrupo
                              INNER JOIN pergunta p ON p.idpergunta = g.idrel
                              WHERE p.idresposta
                              IN ( $idperggrup ) AND pa.idplanilha=".$this->plan." AND filtro_vinc='P' GROUP BY p.idresposta";
                   $etabsub = $_SESSION['query']($tabsub) or die (mysql_error());
                   while($ltabsub = $_SESSION['fetch_array']($etabsub)) {
                       $idplan = $ltabsub['id_plan'];
                       $idaval = $ltabsub['idaval_plan'];
                       if($ltabsub['valor_aval'] == "") {
                           $vlaval = "NULL";
                       }
                       else {
                           $vlaval = $ltabsub['valor_aval'];
                       }
                       $idgrup = $ltabsub['id_grup'];
                       if($ltabsub['valor_grupo'] == "") {
                           $vlgrup = "NULL";
                       }
                       else {
                           $vlgrup = $ltabsub['valor_grupo'];
                       }
                       $tabg = $ltabsub['tab_grup'];
                       $idperg = $ltabsub['id_perg'];
                       $tiporesp = $ltabsub['tipo_resp'];
                       if($ltabsub['valor_perg'] == "") {
                           $vlperg = "NULL";
                           $vlfperg = "NULL";
                       }
                       else {
                           $vlperg = $ltabsub['valor_perg'];
                           $vlfperg = 0;
                       }
                       $tabp = $ltabsub['tab_perg'];
                       $id_resp = $ltabsub['id_resp'];
                       $avalia = $ltabsub['avalia'];
                       if($ltabsub['valor_resp'] == "") {
                           $vlresp = "NULL";
                       }
                       else {
                           $vlresp = $ltabsub['valor_resp'];
                       }
                       $avaliasub = "INSERT INTO $this->tabavalia ($listcampmoni) VALUES ('".$this->idmoni."', '$idplan', '$idaval', $vlaval, 0, 0, '$idgrup',
                       $vlgrup, 0, '$tabg', NULL, NULL, NULL, NULL, '$idperg', '$tiporesp', $vlperg, $vlfperg, '$tabp', '$id_resp', '$avalia', $vlresp,'')";
                       $eavaliasub = $_SESSION['query']($avaliasub) or die (mysql_error());
                   }
              }
          }
          if($idpergsub == "") {
          }
          else {
              if($this->acao == "edita") {
                   $tabsub = "SELECT DISTINCT(p.idresposta) as id_resp, pa.idplanilha as id_plan, pa.idaval_plan, ra.valor as valor_aval,
                              g.idgrupo as id_grup, g.valor_grupo, pa.tab as tab_grup,
                              s.idsubgrupo as id_sub, s.valor_sub, g.tab as tab_sub,
                              p.idpergunta as id_perg, p.valor_perg, s.tab as tab_perg,
                              p.valor_resp, p.tipo_resp, p.avalia, g.filtro_vinc
                              FROM planilha pa
                              INNER JOIN rel_aval ra ON ra.idplanilha = pa.idplanilha
                              INNER JOIN grupo g ON g.idgrupo = pa.idgrupo
                              INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                              INNER JOIN pergunta p ON p.idpergunta = s.idpergunta
                              WHERE p.idresposta
                              IN ( $idpergsub ) AND pa.idplanilha=".$this->plan." AND filtro_vinc='S' GROUP BY p.idresposta ORDER BY p.idpergunta";
                   $etabsub = $_SESSION['query']($tabsub) or die (mysql_error());
                   while($ltabsub = $_SESSION['fetch_array']($etabsub)) {
                       $insert = array_keys($pergssub, $ltabsub['id_perg']);
                       if(count($insert) > 1) {
                           $idplan = $ltabsub['id_plan'];
                           $idaval = $ltabsub['idaval_plan'];
                           if($ltabsub['valor_aval'] == "") {
                               $vlaval = "NULL";
                           }
                           else {
                               $vlaval = $ltabsub['valor_aval'];
                           }
                           $idgrup = $ltabsub['id_grup'];
                           if($ltabsub['valor_grupo'] == "") {
                               $vlgrup = "NULL";
                           }
                           else {
                               $vlgrup = $ltabsub['valor_grupo'];
                           }
                           $tabg = $ltabsub['tab_grup'];
                           $idsub = $ltabsub['id_sub'];
                           if($ltabsub['valor_sub'] == "") {
                               $vlsub = "NULL";
                           }
                           else {
                               $vlsub = $ltabsub['valor_sub'];
                           }
                           $tabs = $ltabsub['tab_sub'];
                           $idperg = $ltabsub['id_perg'];
                           $tiporesp = $ltabsub['tipo_resp'];
                           if($ltabsub['valor_perg'] == "") {
                               $vlperg = "NULL";
                               $vlfperg = "NULL";
                           }
                           else {
                               $vlperg = $ltabsub['valor_perg'];
                           }
                           $tabp = $ltabsub['tab_perg'];
                           $id_resp = $ltabsub['id_resp'];
                           $avalia = $ltabsub['avalia'];
                           if($ltabsub['valor_resp'] == "") {
                               $vlresp = "NULL";
                           }
                           else {
                               $vlresp = $ltabsub['valor_resp'];
                           }
                           /* faz insert das perguntas da planilha relacionadas a sub-grupo, agrupando por id_resp */
                          $avaliagrup = "INSERT INTO $this->tabavalia ($listcampmoni) VALUES ('".$this->idmoni."', '$idplan', '$idaval', $vlaval, 0, 0, '$idgrup',
                          $vlgrup, 0, '$tabg', '$idsub', $vlsub, 0, '$tabs', '$idperg','$tiporesp', $vlperg, $vlfperg, '$tabp', '$id_resp', '$avalia', $vlresp,'')";
                          $egrupavalia = $_SESSION['query']($avaliagrup) or die (mysql_error("erro no processamento da query"));
                       }
                       if(count($insert) == 1) {
                           if($ltabsub['valor_resp'] == "") {
                               $vlresp = "NULL";
                           }
                           else {
                               $vlresp = $ltabsub['valor_resp'];
                           }
                           $idrespalt = array_keys($exrespgrup,$ltabsub['id_perg']);
                           $idrespalt = $idrespalt[0];
                           $upavaliap = "UPDATE moniavalia SET valor_resp=$vlresp, idresposta='".$ltabsub['id_resp']."', avalia='".$ltabsub['avalia']."'
                                         WHERE idpergunta='".$ltabsub['id_perg']."' AND idplanilha='".$this->plan."' AND id$this->tabmoni='".$this->idmoni."' AND idresposta='$idrespalt'";
                           $eupavalia = $_SESSION['query']($upavaliap) or die ("erro na query de update do valor da resposta");
                       }
                       if($eupavalia OR $egrupavalia) {
                           $monialt++;
                       }
                   }
              }
              else {
                  $tabgrup = "SELECT DISTINCT(p.idresposta) as id_resp, pa.idplanilha as id_plan, pa.idaval_plan, ra.valor as valor_aval,
                              g.idgrupo as id_grup, g.valor_grupo, pa.tab as tab_grup,
                              s.idsubgrupo as id_sub, s.valor_sub, g.tab as tab_sub,
                              p.idpergunta as id_perg, p.valor_perg, s.tab as tab_perg,
                              p.valor_resp, p.tipo_resp, p.avalia, g.filtro_vinc
                              FROM planilha pa
                              INNER JOIN rel_aval ra ON ra.idplanilha = pa.idplanilha
                              INNER JOIN grupo g ON g.idgrupo = pa.idgrupo
                              INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                              INNER JOIN pergunta p ON p.idpergunta = s.idpergunta
                              WHERE p.idresposta
                              IN ( $idpergsub ) AND pa.idplanilha=".$this->plan." AND filtro_vinc='S' GROUP BY p.idresposta";
                  $etabgrup = $_SESSION['query']($tabgrup) or die (mysql_error());
                  while($ltabgrup = $_SESSION['fetch_array']($etabgrup)) {
                       $idplan = $ltabgrup['id_plan'];
                       $idaval = $ltabgrup['idaval_plan'];
                       if($ltabgrup['valor_aval'] == "") {
                           $vlaval = "NULL";
                       }
                       else {
                           $vlaval = $ltabgrup['valor_aval'];
                       }
                       $idgrup = $ltabgrup['id_grup'];
                       if($ltabgrup['valor_grupo'] == "") {
                           $vlgrup = "NULL";
                       }
                       else {
                           $vlgrup = $ltabgrup['valor_grupo'];
                       }
                       $tabg = $ltabgrup['tab_grup'];
                       $idsub = $ltabgrup['id_sub'];
                       if($ltabgrup['valor_sub'] == "") {
                           $vlsub = "NULL";
                       }
                       else {
                           $vlsub = $ltabgrup['valor_sub'];
                       }
                       $tabs = $ltabgrup['tab_sub'];
                       $idperg = $ltabgrup['id_perg'];
                       $tiporesp = $ltabgrup['tipo_resp'];
                       if($ltabgrup['valor_perg'] == "") {
                           $vlperg = "NULL";
                           $vlfperg = "NULL";
                       }
                       else {
                           $vlperg = $ltabgrup['valor_perg'];
                       }
                       $tabp = $ltabgrup['tab_perg'];
                       $id_resp = $ltabgrup['id_resp'];
                       $avalia = $ltabgrup['avalia'];
                       if($ltabgrup['valor_resp'] == "") {
                           $vlresp = "NULL";
                       }
                       else {
                           $vlresp = $ltabgrup['valor_resp'];
                       }
                       /* faz insert das perguntas da planilha relacionadas a sub-grupo, agrupando por id_resp */
                       $avaliagrup = "INSERT INTO $this->tabavalia ($listcampmoni) VALUES ('".$this->idmoni."', '$idplan', '$idaval', $vlaval, 0, 0, '$idgrup',
                       $vlgrup, 0, '$tabg', '$idsub', $vlsub, 0, '$tabs', '$idperg','$tiporesp', $vlperg, $vlfperg, '$tabp', '$id_resp', '$avalia', $vlresp,'')";
                       $egrupavalia = $_SESSION['query']($avaliagrup) or die (mysql_error("erro no processamento da query"));
                  }
              }
          }

          //exclui respostas
          if($exrespgrup != "") {
              foreach($exrespgrup as $ex => $key) {
                $exresp = "DELETE FROM moniavalia WHERE idmonitoria='$this->idmoni' AND idresposta='$ex'";
                $delresp = $_SESSION['query']($exresp) or die ("erro na query para exclusÃ£o de respostas da montioria");
              }
          }
          unlock();
          
          /* insere as observaÃ§Ãµes em suas respectivas perguntas */
          if(isset($this->cadobs)) {
            foreach($this->cadobs as $id => $obs) {
              $relperg = "SELECT COUNT(*) as result FROM grupo g WHERE idrel='$id' AND filtro_vinc='P'";
              $erelperg = $_SESSION['fetch_array']($_SESSION['query']($relperg)) or die ("erro na query de consulta do filtro de vinculo da pergunta");
              if($erelperg['result'] >= 1) {
                  $verifplan = "SELECT COUNT(*) as result FROM $this->tabavalia ma WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idplanilha='$this->plan' AND ma.idpergunta='$id'";
                  $everifplan = $_SESSION['fetch_array']($_SESSION['query']($verifplan)) or die ("erro na query de consulta da pergunta na planilha");
                  if($everifplan['result'] >= 1) {
                      lock($this->tabavalia);
                      $cadobs = "UPDATE $this->tabavalia SET obs='".str_replace("+"," ",$obs)."' WHERE id$this->tabmoni='".$this->idmoni."' AND idpergunta='$id'";
                      $ecadobs = $_SESSION['query']($cadobs);
                      $obsalt[] = $id;
                      unlock();
                  }
              }
              if($erelperg['result'] == 0) {
                  $verifplan = "SELECT COUNT(*) as result FROM $this->tabavalia ma WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idplanilha='$this->plan' AND ma.idpergunta='$id'";
                  $everifplan = $_SESSION['fetch_array']($_SESSION['query']($verifplan)) or die ("erro na query de consulta da pergunta na planilha");
                  if($everifplan['result'] >= 1) {
                      lock($this->tabavalia);
                      $cadobs = "UPDATE $this->tabavalia SET obs='".str_replace("+"," ",$obs)."' WHERE id$this->tabmoni='".$this->idmoni."' AND idpergunta='$id'";
                      $ecadobs = $_SESSION['query']($cadobs);
                      $obsalt[] = $id;
                      unlock();
                  }
              }
            }
          }
          
          if($this->tipoplan != "CALIBRAGEM") {
              $selobs = "SELECT idpergunta, obs FROM $this->tabavalia WHERE obs<>'' AND idmonitoria='$this->idmoni' GROUP BY idpergunta";
              $eselobs = $_SESSION['query']($selobs) or die ("erro na query de consulta das observaÃ§Ãµes");
              while($lselobs = $_SESSION['fetch_array']($eselobs)) {
                  if(!in_array($lselobs['idpergunta'],$obsalt)) {
                      $upobs = "UPDATE $this->tabavalia SET obs='' WHERE idpergunta='".$lselobs['idpergunta']."' AND idmonitoria='$this->idmoni'";
                      $eupobs = $_SESSION['query']($upobs) or die ("erro na query de atualizaÃ§Ã£o das observaÃ§Ãµes para apagar");
                  }
              }
          }
          
          /* faz update na monitoria anterior se existir monitoria vinculada */
          if(count($this->idsmoni) > 1) {
              end($this->idsmoni);
              prev($this->idsmoni);
              $idmoniant = current($this->idsmoni);
              lock($this->tabmoni);
              $monivinc = "UPDATE $this->tabmoni SET id".$this->tabmoni."vinc='".$this->idmoni."' WHERE id$this->tabmoni='$idmoniant'";
              $emonivinc = $_SESSION['query']($monivinc) or die ("erro na query de atualizaÃ§Ã£o da monitoria vinculada");
              unlock();
          }
          return $monialt;
    }

    /* funÃ§Ã£o que irÃ¡ executar a rotina de calculo dos valores que cada pergunta e grupo terÃ£o caso nÃ£o sejam FG ou FGM */
    function CalcValorSemFG() {
        
          if($this->acao == "edita" && $this->altera == "N") {
          }
          else {
              /*$percperda = 0;
              $tpavalia = "SELECT avalia FROM $this->tabavalia WHERE id$this->tabmoni='$this->idmoni'";
              $etpavalia = $_SESSION['query']($tpavalia) or die ("erro na query de consulta dos tipos de avaliação da cada pergunta");
              while($lavalia = $_SESSION['fetch_array']($etpavalia)) {
                  if($lavalia['avalia'] == "PERC") {
                      $percperda++;
                  }
                  else {
                  }
              }
              if($percperda >= 1) {
                  $selmoniav = "SELECT ma.idpergunta,p.modcalc,p.percperda,p.maxrep,p.valor_perg,ma.avalia FROM $this->tabavalia ma
                                INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta 
                                WHERE id$this->tabmoni='$this->idmoni' AND ma.avalia='PERC' GROUP BY idpergunta";
                  $eselav = $_SESSION['query']($selmoniav) or die ("erro na query de consulta das perguntas");
                  while($lselav = $_SESSION['fetch_array']($eselav)) {
                      $p = 0;
                      $valorperg = $lselav['valor_perg'];
                      $selresp = "SELECT * FROM $this->tabavalia WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lselav['idpergunta']."' AND (avalia<>'FG' && avalia<>'FGM')";
                      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
                      while($lresp = $_SESSION['fetch_array']($eselresp)) {
                          if($lresp['avalia'] == "AVALIACAO_OK") {
                              $atuperg = "UPDATE $this->tabavalia SET valor_final_perg='".($valorperg - $lresp['valor_resp'])."', valor_resp='".$lresp['valor_resp']."' WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lselav['idpergunta']."'";
                              $eatuperg = $_SESSION['query']($atuperg) or die ("erro na query de atulização do valor da pergunta e resposta");
                          }
                          else {
                            $p++;
                          }
                      }
                      if($p == 0) {
                      }
                      else {
                          if($p > $lselav['maxrep']) {
                              $atuperg = "UPDATE $this->tabavalia SET valor_final_perg='0', valor_resp='$valorperg' WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lselav['idpergunta']."'";
                              $eatuperg = $_SESSION['query']($atuperg) or die ("erro na query de atulização do valor da pergunta e resposta");
                          }
                          else {
                              for($i = 1;$i <= $p; $i++) {
                                  $valorperg = $valorperg + ($lresp['valor_perg'] * $lselav['percperda']);
                              }
                              $upperg = "UPDATE $this->tabavalia SET valor_final_perg='".($lresp['valor_perg'] - $valorperg)."',valor_resp='".($valorperg / $p)."' WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lselav['idpergunta']."'";
                              $eupperg = $_SESSION['query']($upperg) or die ("erro na query de atualização dos valores da pergunta e resposta");
                          }
                      }
                  }
                  
                  // atualiza valores dos grupos
                  $selsub = "SELECT DISTINCT(idsubgrupo), valor_sub FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='PERC' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NOT NULL";
                  $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos grupos");
                  while($lselsub = $_SESSION['fetch_array']($eselsub)) {
                    $selsubgrup = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as soma FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND idsubgrupo='".$lselsub['idsubgrupo']."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC')";
                    $eselsubgrup = $_SESSION['query']($selsubgrup) or die ("erro na query de consulta do valor dos subgrupos");
                    while($lselsubgrup = $_SESSION['fetch_array']($eselsubgrup)) {
                      $somas += ($lselsubgrup['valor_sub'] - $lselsubgrup['soma']);
                    }
                    $cadvfgrupsub = "UPDATE $this->tabavalia SET valor_final_sub='$somas' WHERE id$this->tabmoni='".$this->idmoni."' AND idsubgrupo='".$lselsub['idsubgrupo']."'";
                    $ecadvfgrupsub = $_SESSION['query']($cadvfgrupsub) or die (mysql_error());
                    $somas = 0;
                  }
                  $selgrup = "SELECT DISTINCT(ma.idgrupo), filtro_vinc, ma.valor_grupo FROM $this->tabavalia ma INNER JOIN grupo g ON g.idgrupo = ma.idgrupo WHERE id$this->tabmoni='".$this->idmoni."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='PERC' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NULL AND filtro_vinc='P'";
                  $eselgrup = $_SESSION['query']($selgrup) or die ("erro na query de consulta dos grupos da monitoria");
                  while($lselgrup = $_SESSION['fetch_array']($eselgrup)) {
                    $selperggrup = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as soma FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrup['idgrupo']."' AND (avalia='AVALIACAO' OR avalia='PERC' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC')";
                    $eselperg = $_SESSION['query']($selperggrup) or die ("erro na query de consulta do valor das perguntas");
                    while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                      $somap += ($lselperg['valor_grupo'] - $lselperg['soma']);
                    }
                    $cadvfgrupperg = "UPDATE $this->tabavalia SET valor_final_grup='$somap' WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrup['idgrupo']."'";
                    $ecadvfgrupperg = $_SESSION['query']($cadvfgrupperg) or die (mysql_error());
                    $somap = 0;
                  }
                  $selgrupsub = "SELECT DISTINCT(ma.idgrupo), filtro_vinc, ma.valor_grupo FROM $this->tabavalia ma INNER JOIN grupo g ON g.idgrupo = ma.idgrupo WHERE ma.id$this->tabmoni='$this->idmoni' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='PERC' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NOT NULL AND filtro_vinc='S'";
                  $eselgrupsub = $_SESSION['query']($selgrupsub) or die ("erro na query de consulta dos grupos");
                  while($lselgrupsub = $_SESSION['fetch_array']($eselgrupsub)) {
                      $vfgrup = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as soma, idgrupo FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrupsub['idgrupo']."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NOT NULL";
                      $evfgrup = $_SESSION['query']($vfgrup) or die (mysql_error());
                      while($lvfgrup = $_SESSION['fetch_array']($evfgrup)) {
                          if($lvfgrup['soma'] == "") {
                          }
                          else {
                            $cadvfgrup = "UPDATE $this->tabavalia SET valor_final_grup='".($lvfgrup['valor_grupo'] - $lvfgrup['soma'])."' WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lvfgrup['idgrupo']."'";
                            $ecadvfgrup = $_SESSION['query']($cadvfgrup) or die (mysql_error());
                          }
                      }
                  }
              }
              //else {*/
                  $vfperg = "SELECT DISTINCT(m.idpergunta), SUM(m.valor_resp) as valor_resp, m.valor_perg, modcalc, m.avalia, maxrep, percperda, id$this->tabmoni FROM $this->tabavalia m 
                            INNER JOIN pergunta p ON p.idpergunta = m.idpergunta
                            WHERE id$this->tabmoni='".$this->idmoni."' AND (m.avalia='AVALIACAO' OR m.avalia='AVALIACAO_OK' OR m.avalia='REDIST_GRUPO' OR m.avalia='SUPERACAO' OR m.avalia='PERC') GROUP BY m.idpergunta";
                  $evfperg = $_SESSION['query']($vfperg) or die (mysql_error());
                  while($lvfperg = $_SESSION['fetch_array']($evfperg)) {
                      if($lvfperg['modcalc'] == "PERC") {
                            $p = 0;
                            $valorperg = $lvfperg['valor_perg'];
                            $selresp = "SELECT * FROM $this->tabavalia WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lvfperg['idpergunta']."' AND (avalia<>'FG' && avalia<>'FGM')";
                            $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
                            while($lresp = $_SESSION['fetch_array']($eselresp)) {
                                if($lresp['avalia'] == "AVALIACAO_OK") {
                                    $atuperg = "UPDATE $this->tabavalia SET valor_final_perg='".($valorperg - $lresp['valor_resp'])."', valor_resp='".$lresp['valor_resp']."' WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lvfperg['idpergunta']."'";
                                    $eatuperg = $_SESSION['query']($atuperg) or die ("erro na query de atulização do valor da pergunta e resposta");
                                }
                                else {
                                  $p++;
                                }
                            }
                            if($p == 0) {
                            }
                            else {
                                if($p > $lvfperg['maxrep']) {
                                    $atuperg = "UPDATE $this->tabavalia SET valor_final_perg='0', valor_resp='".($valorperg/$p)."' WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lvfperg['idpergunta']."'";
                                    $eatuperg = $_SESSION['query']($atuperg) or die ("erro na query de atulização do valor da pergunta e resposta");
                                }
                                else {
                                    $valorp = $valorperg;
                                    $valorsoma = 0;
                                    $valordiv = 0;
                                    for($i = 1;$i <= $p; $i++) {
                                        $valorp = ($valorp * $lvfperg['percperda']);
                                        $valorsoma = $valorsoma + $valorp;
                                    }
                                    for($i = 0;$i < $p;$i++) {
                                        $valordiv += round(($valorsoma / $p),2);
                                    }
                                    $upperg = "UPDATE $this->tabavalia SET valor_final_perg='".($lvfperg['valor_perg'] - $valordiv)."',valor_resp='".($valorsoma / $p)."' WHERE id$this->tabmoni='$this->idmoni' AND idpergunta='".$lvfperg['idpergunta']."'";
                                    $eupperg = $_SESSION['query']($upperg) or die ("erro na query de atualização dos valores da pergunta e resposta");
                                }
                            }
                      }
                      if($lvfperg['modcalc'] == "ABS") {
                          $respsomas = "SELECT DISTINCT(m.idpergunta), SUM(m.valor_resp) as valor_resp, m.valor_perg FROM $this->tabavalia m 
                                        WHERE id$this->tabmoni='".$this->idmoni."' AND m.idpergunta='".$lvfperg['idpergunta']."' AND (m.avalia='AVALIACAO' OR m.avalia='AVALIACAO_OK' OR m.avalia='REDIST_GRUPO' OR m.avalia='SUPERACAO' OR m.avalia='PERC')";
                          $erespsomas = $_SESSION['fetch_array']($_SESSION['query']($respsomas)) or die (mysql_error());
                          $cadvfperg = "UPDATE $this->tabavalia SET valor_final_perg='".($erespsomas['valor_perg'] - $erespsomas['valor_resp'])."' WHERE id$this->tabmoni='".$this->idmoni."' AND idpergunta='".$lvfperg['idpergunta']."'";
                          $ecadvfperg = $_SESSION['query']($cadvfperg) or die (mysql_error());
                      }
                  }
                  $selsub = "SELECT DISTINCT(idsubgrupo), valor_sub FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NOT NULL";
                  $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos grupos");
                  while($lselsub = $_SESSION['fetch_array']($eselsub)) {
                    $selsubgrup = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as soma FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND idsubgrupo='".$lselsub['idsubgrupo']."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC')";
                    $eselsubgrup = $_SESSION['query']($selsubgrup) or die ("erro na query de consulta do valor dos subgrupos");
                    while($lselsubgrup = $_SESSION['fetch_array']($eselsubgrup)) {
                      $somas += ($lselsub['valor_sub'] - $lselsubgrup['soma']);
                    }
                    $cadvfgrupsub = "UPDATE $this->tabavalia SET valor_final_sub='$somas' WHERE id$this->tabmoni='".$this->idmoni."' AND idsubgrupo='".$lselsub['idsubgrupo']."'";
                    $ecadvfgrupsub = $_SESSION['query']($cadvfgrupsub) or die (mysql_error());
                    $somas = 0;
                  }
                  $selgrup = "SELECT DISTINCT(ma.idgrupo), filtro_vinc, ma.valor_grupo FROM $this->tabavalia ma INNER JOIN grupo g ON g.idgrupo = ma.idgrupo WHERE id$this->tabmoni='".$this->idmoni."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NULL AND filtro_vinc='P'";
                  $eselgrup = $_SESSION['query']($selgrup) or die ("erro na query de consulta dos grupos da monitoria");
                  while($lselgrup = $_SESSION['fetch_array']($eselgrup)) {
                    $selperggrup = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as soma FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrup['idgrupo']."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC')";
                    $eselperg = $_SESSION['query']($selperggrup) or die ("erro na query de consulta do valor das perguntas");
                    while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                      $somap += ($lselgrup['valor_grupo'] - $lselperg['soma']);
                    }
                    $cadvfgrupperg = "UPDATE $this->tabavalia SET valor_final_grup='$somap' WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrup['idgrupo']."'";
                    $ecadvfgrupperg = $_SESSION['query']($cadvfgrupperg) or die (mysql_error());
                    $somap = 0;
                  }
                  $selgrupsub = "SELECT DISTINCT(ma.idgrupo), filtro_vinc, ma.valor_grupo FROM $this->tabavalia ma INNER JOIN grupo g ON g.idgrupo = ma.idgrupo WHERE ma.id$this->tabmoni='$this->idmoni' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NOT NULL AND filtro_vinc='S'";
                  $eselgrupsub = $_SESSION['query']($selgrupsub) or die ("erro na query de consulta dos grupos");
                  while($lselgrupsub = $_SESSION['fetch_array']($eselgrupsub)) {
                      $vfgrup = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as soma FROM $this->tabavalia m WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrupsub['idgrupo']."' AND (avalia='AVALIACAO' OR avalia='AVALIACAO_OK' OR avalia='REDIST_GRUPO' OR avalia='SUPERACAO' OR avalia='PERC') AND idsubgrupo IS NOT NULL";
                      $evfgrup = $_SESSION['query']($vfgrup) or die (mysql_error());
                      while($lvfgrup = $_SESSION['fetch_array']($evfgrup)) {
                          if($lvfgrup['soma'] == "") {
                          }
                          else {
                            $cadvfgrup = "UPDATE $this->tabavalia SET valor_final_grup='".($lselgrupsub['valor_grupo'] - $lvfgrup['soma'])."' WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lselgrupsub['idgrupo']."'";
                            $ecadvfgrup = $_SESSION['query']($cadvfgrup) or die (mysql_error());
                          }
                      }
                  }
              //}
              return true;
          }
    }

    /* funÃ§Ã£o que irÃ¡ recalcular o valor da planilha caso tenha alguma FG ou FGM registrada na tabela moniavaliacao */
    function CalcValorFG() {

          if($this->acao == "edita" && $this->altera == "N") {
          }
          else {
              $this->cfg = 0;
              $this->cfgm = 0;
              $this->cfgmt = 0;
              $this->cfgt = 0;
              /* levanta o valor final das avaliaÃ§Ãµes para fazer o levantamento de "FG" e "FGM" e fazer a nova contabilizaÃ§Ã£o da avaliaÃ§Ã£o*/
              $selaval = "SELECT ra.idaval_plan, ra.valor FROM rel_aval ra WHERE ra.idplanilha='".$this->plan."'";
              $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliÃ§Ãµes relacionadas a planilha");
              while($lselaval = $_SESSION['fetch_array']($eselaval)) {
                  $valor_aval = 0;
                  $valor_fg = 0;
                  $count_rsfg = "SELECT COUNT(*) as result, valor_aval FROM $this->tabavalia m WHERE id$this->tabmoni='$this->idmoni' AND idaval_plan='".$lselaval['idaval_plan']."' AND (avalia<>'FG' AND avalia<>'FGM')";
                  $ecount_sfg = $_SESSION['fetch_array']($_SESSION['query']($count_rsfg)) or die ("erro na query de consulta da quantidade de respostas sem FG");
                  $countrsfg = $ecount_sfg['result'];
                  $vfaval = "SELECT ma.idaval_plan, ma.valor_resp, ma.avalia, ma.valor_aval FROM $this->tabavalia ma WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idaval_plan='".$lselaval['idaval_plan']."'";
                  $evfaval = $_SESSION['query']($vfaval) or die (mysql_error());
                  while($lvfaval = $_SESSION['fetch_array']($evfaval)) {
                      if($lvfaval['avalia'] != "FG" && $lvfaval['avalia'] != "FGM" && $lvfaval['avalia'] != "REDIST_GRUPO" && $lvfaval['avalia'] != "REDIST_AVAL" && $lvfaval['avalia'] != "SUPERACAO" && $lvfaval['avalia'] != "AVALIACAO_FG") {
                          $valor_aval = $valor_aval + $lvfaval['valor_resp'];
                          $valor_fg = $valor_fg + 0;
                      }
                      if(($lvfaval['avalia'] == "FG" OR $lvfaval['avalia'] == "FGM") && $lvfaval['avalia'] != "AVALIACAO" && $lvfaval['avalia'] != "AVALIACAO_OK" && $lvfaval['avalia'] != "PERC" && $lvfaval['avalia'] != "TABULACAO" && $lvfaval['avalia'] != "REDIST_GRUPO" && $lvfaval['avalia'] != "REDIST_AVAL" && $lvfaval['avalia'] != "SUPERACAO" && $lvfaval['avalia'] != "AVALIACAO_FG") {
                          if($countrsfg == 0) {
                              $valor_aval = $lvfaval['valor_aval'];
                              $valor_fg = $valor_fg + $lvfaval['valor_resp'];
                          }
                          else {
                              $valor_aval = $valor_aval + 0;
                              $valor_fg = $valor_fg + $lvfaval['valor_resp'];
                          }
                      }
                  }
                    //$vfg = "SELECT idresposta, SUM(valor_resp) as soma, idaval_plan FROM moniavalia WHERE id$this->tabmoni='".$this->idmoni."' AND idaval_plan='".$lselaval['idaval_plan']."' AND (avalia='FG' OR avalia='FGM')";
                    //$evfg = $_SESSION['fetch_array']($_SESSION['query']($vfg)) or die ("erro na query para somar o valor das F.G");
                    $valaval = $ecount_sfg['valor_aval'] - ($valor_aval + $valor_fg);
                    if($valaval <= 0) {
                      $valaval = 0;
                    }
                    if($valaval > 0) {
                    }
                    $this->valoraval[] = $valaval;
                    $countfg = "SELECT COUNT(*) as result FROM $this->tabavalia ma WHERE ma.id$this->tabmoni='".$this->idmoni."' AND (ma.avalia='FG' OR ma.avalia='FGM')";
                    $ecountfg = $_SESSION['fetch_array']($_SESSION['query']($countfg)) or die ("erro na query de consulta das Falhas Graves");
                    $cadaval = "UPDATE $this->tabavalia SET valor_fg ='$valaval', valor_final_aval='".($ecount_sfg['valor_aval'] - $valor_aval)."' WHERE id$this->tabmoni='".$this->idmoni."' AND idaval_plan='".$lselaval['idaval_plan']."'";
                    $ecadaval = $_SESSION['query']($cadaval) or die (mysql_error());
                    if($ecountfg['result'] >= 1) {
                        $mail = new ConfigMail();
                        $fg = "SELECT ma.idaval_plan, ma.valor_final_aval, ma.idgrupo, ma.valor_final_grup, ma.idsubgrupo, ma.valor_sub, ma.valor_final_sub, ma.idpergunta,
                              ma.valor_final_perg, ma.idresposta, ma.avalia FROM $this->tabavalia ma
                              INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                              WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idaval_plan = '".$lselaval['idaval_plan']."' AND (ma.avalia='FG' OR ma.avalia='FGM')";
                        $efg = $_SESSION['query']($fg) or die ("erro na query de consulta das ''FG'' e ''FGM''");
                        while($lfg = $_SESSION['fetch_array']($efg)) {
                            if($lfg['valor_sub'] == "") {
                                $atufgperg = "UPDATE $this->tabavalia SET valor_final_grup='0', valor_final_perg='0'
                                              WHERE id$this->tabmoni='".$this->idmoni."' AND idresposta='".$lfg['idresposta']."'";
                                $eatufgperg = $_SESSION['query']($atufgperg) or die ("erro na query de atualizaÃ§Ã£o do valor da avaliação");
                                $atugrupo = "UPDATE $this->tabavalia SET valor_final_grup='0'
                                              WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lfg['idgrupo']."'";
                                $eatugrupo = $_SESSION['query']($atugrupo) or die ("erro na query de atualizaÃ§Ã£o do valor da avaliação");
                                //$atufg = "UPDATE moniavalia SET valor_fg='0' WHERE id$this->tabmoni='".$this->idmoni."'";
                            }
                            if($lfg['valor_sub'] != "") {
                                $atufgperg = "UPDATE $this->tabavalia SET valor_final_grup='0', valor_final_sub='0', valor_final_perg='0'
                                              WHERE id$this->tabmoni='".$this->idmoni."' AND idresposta='".$lfg['idresposta']."'";
                                $eatufgperg = $_SESSION['query']($atufgperg) or die ("erro na query de atualizaÃ§Ã£o do valor da avaliação");
                                $atugrupo = "UPDATE $this->tabavalia SET valor_final_grup='0'
                                              WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lfg['idgrupo']."'";
                                $eatugrupo = $_SESSION['query']($atugrupo) or die ("erro na query de atualizaÃ§Ã£o do valor da avaliação");
                                //$atufg = "UPDATE moniavalia SET valor_fg='0' WHERE id$this->tabmoni='".$this->idmoni."'";
                            }
                            //$eatufg = $_SESSION['query']($atufg) or die ("erro na query de UPDATE da avaliaÃ§Ã£o da monitoria com Falha Grave");
                            if($lfg['avalia'] == "FG") {
                                 if(!array_key_exists($this->idmoni, $this->idsmonifg)) {
                                   $this->idsmonifg[$this->idmoni]['checkfg'] = $this->checkfg;
                                   $this->idsmonifg[$this->idmoni]['acao'] = $this->acao;
                                   $this->idsmonifg[$this->idmoni]['tipoplan'] = $this->tipoplan;
                                   $this->idsmonifg[$this->idmoni]['idrel_filtros'] = $this->idrelfiltro;
                                 }
                                $this->cfg++;
                                $this->cfgt++;
                            }
                            if($lfg['avalia'] == "FGM") {
                                if(!array_key_exists($this->idmoni, $this->idsmonifg)) {
                                   $this->idsmonifg[$this->idmoni]['checkfg'] = $this->checkfg;
                                   $this->idsmonifg[$this->idmoni]['acao'] = $this->acao;
                                   $this->idsmonifg[$this->idmoni]['tipoplan'] = $this->tipoplan;
                                   $this->idsmonifg[$this->idmoni]['idrel_filtros'] = $this->idrelfiltro;
                                }
                                $this->cfgm++;
                                $this->cfgmt++;
                            }
                        }
                        $this->idsmonifg[$this->idmoni]['qtde'] = $this->cfg;
                        $qtdefg = "UPDATE monitoria SET qtdefg='$this->cfg', qtdefgm='$this->cfgm' WHERE idmonitoria='".$this->idmoni."'";
                        $eqtdefg = $_SESSION['query']($qtdefg) or die ("erro na query de atualizaÃ§Ã£o da monitoria");
                    }
                    else {
                        $qtdefg = "UPDATE monitoria SET qtdefg='$this->cfg', qtdefgm='$this->cfgm' WHERE idmonitoria='".$this->idmoni."'";
                        $eqtdefg = $_SESSION['query']($qtdefg) or die ("erro na query de atualizaÃ§Ã£o da monitoria");
                    }
                }                               
          }
    }
    
    function emailfg() {
         //envio de e-mails
         $mail = new ConfigMail();
         $mail->IsSMTP();
         $mail->IsHTML(true);
         $mail->SMTPAuth = true;
         
         foreach($this->idsmonifg as $idmoni => $dados) {
              if($dados['checkfg'] != "S") {
               if($dados['acao'] != "edita" && $dados['tipoplan'] != "CALIBRAGEM") {
                     $this->fgmsg[] = $dados['qtde'];
                     $relaud = "SELECT idrel_filtros, COUNT(*) as result FROM conf_rel WHERE idrel_aud='".$dados['idrel_filtros']."'";
                     $erelaud = $_SESSION['fetch_array']($_SESSION['query']($relaud)) or die (mysql_error());
                     $confemail = "SELECT count(*) as result FROM relemailfg WHERE idrel_filtros='".$dados['idrel_filtros']."'";
                     $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail)) or die (mysql_error());
                     if($erelaud['result'] >= 1 && $econfemail['result'] == 0) {
                         $sconfemail = "SELECT idconfemailenv, user_adm, user_web FROM relemailfg rm 
                                         WHERE idrel_filtros='".$erelaud['idrel_filtros']."'";
                     }
                     else {
                         $sconfemail = "SELECT idconfemailenv, user_adm, user_web FROM relemailfg rm 
                                         WHERE idrel_filtros='".$dados['idrel_filtros']."'";
                     }
                     $lcon = $_SESSION['query']($sconfemail) or die ("erro na query de consulta dos dados para envio de e-mail");
                     $nconf = $_SESSION['num_rows']($lcon);
                     if($nconf >= 1) {
                         $econfmail = $_SESSION['fetch_array']($lcon);
                         $conf = $econfmail['idconfemailenv'];
                         $mail->idconf = $conf;
                         $mail->tipouser = "user_adm";
                         $mail->idmonitoria = $idmoni;
                         $mail->config();

                         if($econfmail['user_adm'] != "") {
                             $usersadm = explode(",",$econfmail['user_adm']);
                         }
                         else {
                         }
                         foreach($usersadm as $ua) {
                             if(in_array($ua,$users['user_adm'])) {
                             }
                             else {
                                 $users['user_adm'][] = $ua;
                             }
                         }

                         if($this->checkfg == "S") {
                         }
                         else {
                             if($econfmail['user_web'] != "") {
                                 $usersweb = explode(",",$econfmail['user_web']);
                             }
                             else {
                             }
                             foreach($usersweb as $uw) {
                                 if(in_array($uw,$users['user_web'])) {
                                 }
                                 else {
                                     $users['user_web'][] = $uw;
                                 }
                             }
                         }

                         foreach($users as $ktab => $userstab) {
                             foreach($userstab as $k => $idusermail) {
                                 $seluser = "SELECT nome$ktab, email, COUNT(*) as result FROM $ktab WHERE id$ktab='$idusermail' and ativo='S'";
                                 $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do usuário");
                                 if($eseluser['result'] == 0) {
                                 }
                                 else {
                                     $cemail++;
                                     $email = $eseluser['email'];
                                     $nome = $eseluser['nome'.$ktab];
                                     $mail->AddAddress("$email","$nome");
                                 }
                             }
                         }
                         if(count($users) >= 1) {
                             if($mail->Send()) {
                                 $count++;
                             }
                             else {
                             }
                             $mail->ClearAddresses();
                             $mail->ClearAllRecipients();
                        }
                    }
                }
            }
        }
    }

    /* funÃ§Ã£o que calcula a redistribuiÃ§Ã£o de valores por grupo */
    function CalcValorRedisGP() {

          if($this->acao == "edita" && $this->altera == "N") {
          }
          else {
              /* levantar se existem perguntas com respostas que redistribuem valor dentro do grupo, existindo, o sistema faz todo processo de redistribuiÃ§Ã£o*/
              $redist_grupo = "SELECT COUNT(*) as result FROM $this->tabavalia ma WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.avalia='REDIST_GRUPO'";
              $ergrupo = $_SESSION['fetch_array']($_SESSION['query']($redist_grupo)) or die ("erro na query de consulta da redistribuiÃ§Ã£o por grupo");
              if($ergrupo['result'] >= 1 ) {
                  $rgrupo = "SELECT ma.idaval_plan, g.filtro_vinc, ma.idgrupo FROM $this->tabavalia ma
                             INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                             WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.valor_final_grup<>'0' AND ma.avalia='REDIST_GRUPO'
                             GROUP BY ma.idgrupo";
                  $erg = $_SESSION['query']($rgrupo) or die ("erro na consulta das repostas que possuem avaliaÃ§Ã£o ''REDIST_GRUPO''");
                  while($lrgrupo = $_SESSION['fetch_array']($erg)) {
                      $vdist = 0;
                      $credist = "SELECT CASE ma.avalia WHEN 'REDIST_GRUPO' THEN 1 ELSE 0 END as redist FROM $this->tabavalia ma
                                      INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                      INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                      WHERE ma.id$this->tabmoni='".$this->idmoni."' AND g.idgrupo='".$lrgrupo['idgrupo']."'
                                      GROUP BY ma.idpergunta";
                      $ecredist = $_SESSION['query']($credist) or die ("erro na query para contagem de itens de redistribuicao");
                      while($lcredist = $_SESSION['fetch_array']($ecredist)) {
                          if($lcredist['redist'] == 0) {
                              $avalia++;
                          }
                          if($lcredist['redist'] >= 1) {
                              $redist++;
                          }
                      }

                      if($avalia == 0) {
                          if($lrgrupo['filtro_vinc'] == "P") {
                              /* query que identidifica a quantidade de perguntas respondidas com redistribuiÃ§Ã£o e quantas avaliadas */
                              $valordist = "SELECT ma.valor_resp as vdist, ma.idgrupo, ma.idresposta FROM $this->tabavalia ma
                                        INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                        INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                        WHERE ma.id$this->tabmoni='".$this->idmoni."' AND g.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia='REDIST_GRUPO' GROUP BY ma.idresposta";
                              $evalordist = $_SESSION['query']($valordist) or die ("erro na query de consulta do valor Ã  redistribuir");
                              while ($lvdist = $_SESSION['fetch_array']($evalordist)) {
                                  $updatena = "UPDATE $this->tabavalia SET valor_resp='".$lvdist['vdist']."', valor_final_perg='".$lvdist['vdist']."'
                                               WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lvdist['idgrupo']."' AND idresposta='".$lvdist['idresposta']."'";
                                  $eupdate = $_SESSION['query']($updatena) or die ("erro na query de atualizaÃ§Ã£o da tabela moniavalia");
                              }
                          }
                          if($lrgrupo['filtro_vinc'] == "S") {
                              $sdistsub = "SELECT ma.valor_resp as vdist, ma.idgrupo, s.id FROM $this->tabavalia ma
                                           INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                           INNER JOIN subgrupo s ON s.idsubgrupo = ma.idsubgrupo
                                           WHERE ma.id$this->tabmoni='".$this->idmoni."' AND g.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia='REDIST_GRUPO'
                                           GROUP BY ma.idsubgrupo";
                              $edistsub = $_SESSION['query']($sdistsub) or die ("erro na query de consulta do valor Ã  redistribuir");
                              while ($ldistsub = $_SESSION['fetch_array']($edistsub)) {
                                  $updatena = "UPDATE $this->tabavalia SET valor_resp='".$ldistsub['vdist']."', valor_final_perg='".$ldistsub['vdist']."'
                                               WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$ldistsub['idgrupo']."' AND idsubgrupo='".$ldistsub['id']."'";
                                  $eupdate = $_SESSION['query']($updatena) or die ("erro na query de atualizaÃ§Ã£o da tabela moniavalia");
                              }
                          }
                      }
                      else {
                          if($lrgrupo['filtro_vinc'] == "P") {
                              /* executa redistribuiÃ§Ã£o para grupos vinculados a perguntas */
                              $avalia = 0;
                              $redist = 0;

                                  /* query para identificar o valor da redistribuiÃ§Ã£o que deverÃ¡ ser executada */
                                  $valordist = "SELECT ma.valor_resp as vdist, ma.idgrupo, ma.idresposta FROM $this->tabavalia ma
                                                INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                                INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                                WHERE ma.id$this->tabmoni='".$this->idmoni."' AND g.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia='REDIST_GRUPO' GROUP BY ma.idresposta";
                                  $evalordist = $_SESSION['query']($valordist) or die ("erro na query de consulta do valor Ã  redistribuir");
                                  while ($lvdist = $_SESSION['fetch_array']($evalordist)) {
                                      $vdist = $vdist + $lvdist['vdist'];
                                      $updatena = "UPDATE $this->tabavalia SET valor_resp='0', valor_final_perg='0'
                                                   WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lvdist['idgrupo']."' AND idresposta='".$lvdist['idresposta']."'";
                                      $eupdate = $_SESSION['query']($updatena) or die ("erro na query de atualizaÃ§Ã£o da tabela moniavalia");
                                  }

                                  /* query que identificar quais perguntas terÃ£o seu valor incrementado */
                                  $qtdeperg = "SELECT ma.idpergunta, ma.idresposta FROM $this->tabavalia ma
                                              INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                              INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                              WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia<>'REDIST_GRUPO' AND ma.avalia<>'TABULACAO' AND (ma.avalia<>'FG' AND ma.avalia<>'FGM') AND ma.valor_resp<>'0' GROUP BY ma.idpergunta";
                                  $eqtdeperg = $_SESSION['query']($qtdeperg) or die ("erro na query de contagem da quantidade de perguntas vinculadas ao grupo");
                                  while($lqtdeperg = $_SESSION['fetch_array']($eqtdeperg)) {
                                      $sperg = "SELECT ma.idpergunta, ma.idresposta, ma.valor_resp, (ma.valor_grupo - $vdist) as new_vgrup, ma.valor_perg FROM $this->tabavalia ma
                                                INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                                WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idpergunta='".$lqtdeperg['idpergunta']."' AND ma.idresposta='".$lqtdeperg['idresposta']."' AND ma.valor_resp<>'0' GROUP BY ma.idresposta";
                                      $esperg = $_SESSION['fetch_array']($_SESSION['query']($sperg)) or die ("erro na query de consulta da pergunta");
                                      $vdistperg = number_format(($esperg['valor_perg'] / $esperg['new_vgrup']) * $vdist, 2);
                                      $vp = $esperg['valor_perg'] + $vdistperg; //caculo para incremento no valor da pergunta, considerando seu peso dentro do grupo que pertence
                                      $vr = $esperg['valor_resp'] + $vdistperg; // calculo para incremento no valor da resposta, considerando seu peso dentro da pergunta que pertence
                                      $updatemoni = "UPDATE $this->tabavalia SET valor_resp='$vr', valor_final_perg='$vp'
                                                     WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lrgrupo['idgrupo']."' AND idpergunta='".$lqtdeperg['idpergunta']."' AND idresposta='".$lqtdeperg['idresposta']."' AND valor_resp<>'0'";
                                      $eupdatemoni = $_SESSION['query']($updatemoni) or die ("erro na query de atualizaÃ§Ã£o dos valores re-distribuÃ­dos");
                                  }
                              }
                              if($lrgrupo['filtro_vinc'] == "S") {
                                  /* executa redistribuiÃ§Ã£o para grupos vinculados a sub-grupos*/



                                  /* query que obtem quais os sub-grupos pertencentes ao grupo, que contÃ©m perguntas com redistribuiÃ§Ã£o */
                                  $sdistsub = "SELECT ma.valor_resp as vdist, ma.idgrupo, s.idsubgrupo FROM $this->tabavalia ma
                                               INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                               INNER JOIN subgrupo s ON s.idsubgrupo = ma.idsubgrupo
                                               WHERE ma.id$this->tabmoni='".$this->idmoni."' AND g.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia='REDIST_GRUPO'
                                               GROUP BY ma.idsubgrupo";
                                  $edistsub = $_SESSION['query']($sdistsub) or die ("erro na query de consulta do valor Ã  redistribuir");
                                  while ($ldistsub = $_SESSION['fetch_array']($edistsub)) {
                                      $vsub = 0;

                                      /* query que obtem as perguntas e o valor das respostas para contabilizar o valor da redistribuiÃ§Ã£o total */
                                      $vdistsub = "SELECT ma.valor_resp as vdist, ma.idgrupo, ma.idsubgrupo, ma.idresposta FROM $this->tabavalia ma
                                                  INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                                  INNER JOIN subgrupo s ON s.idsubgrupo = ma.idsubgrupo
                                                  INNER JOIN pergunta p ON p.idpergunta = s.idpergunta AND p.idresposta = ma.idresposta
                                                  WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idgrupo='".$ldistsub['idgrupo']."' AND ma.idsubgrupo='".$ldistsub['idsubgrupo']."' AND ma.avalia='REDIST_GRUPO' GROUP BY ma.idresposta";
                                      $edistsub = $_SESSION['query']($vdistsub) or die ("erro na query para consultar o valor da distribuicao");
                                      while($vldistsub = $_SESSION['fetch_array']($edistsub)) {
                                          $vsub = $vsub + $vldistsub['vdist'];

                                          /* faz update na tabela moniavalia das perguntas de resdistribuiÃ§Ã£o que jÃ¡ foram contabilizadas na somatÃ³ria e agora terÃ£o seu valor alterado para 0 */
                                          $updatena = "UPDATE $this->tabavalia SET valor_resp='0', valor_final_perg='0'
                                                       WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$vldistsub['idgrupo']."' AND idresposta='".$vldistsub['idresposta']."'";
                                          $eupdate = $_SESSION['query']($updatena) or die ("erro na query de atualizaÃ§Ã£o da tabela moniavalia");
                                      }

                                      /* query que identificar quais perguntas dentro do sub-grupo terÃ£o seu valor incrementado */
                                      $qtdesub = "SELECT ma.idsubgrupo, ma.valor_sub, ma.idpergunta, ma.idresposta FROM $this->tabavalia ma
                                                  INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                                  INNER JOIN subgrupo s ON s.idsubgrupo = ma.idsubgrupo
                                                  INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                                  WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia<>'REDIST_GRUPO' AND ma.avalia<>'TABULACAO' AND (ma.avalia<>'FG' AND ma.avalia<>'FGM') GROUP BY ma.idsubgrupo";
                                      $eqtdesub = $_SESSION['query']($qtdesub) or die ("erro na query que obtem as linhas");
                                      while($lqtdesub = $_SESSION['fetch_array']($eqtdesub)) {
                                              $sperg = "SELECT (ma.valor_sub - $vsub) as new_sub, ma.idpergunta, ma.valor_final_perg, ma.idresposta, ma.valor_resp FROM $this->tabavalia ma
                                                        INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta
                                                        WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idpergunta='".$lqtdesub['idpergunta']."' AND ma.idresposta='".$lqtdesub['idresposta']."' AND ma.valor_resp<>'0' AND ma.avalia<>'TABULACAO' AND (p.avalia<>'FG' AND p.avalia<>'FGM') GROUP BY ma.idresposta";
                                              $esperg = $_SESSION['fetch_array']($_SESSION['query']($sperg)) or die ("erro na query de consulta da pergunta");
                                              $vdistsub = number_format(($esperg['valor_perg'] / $esperg['new_sub']) * $vsub, 2);
                                              $vp = $esperg['valor_perg'] + $vdistsub;
                                              $vr = number_format(($esperg['valor_resp'] / $esperg['valor_perg']) * $vdistsub, 2);
                                              $updatemoni = "UPDATE $this->tabavalia SET valor_resp='$vr', valor_final_perg='$vp'
                                                             WHERE id$this->tabmoni='".$this->idmoni."' AND idgrupo='".$lrgrupo['idgrupo']."' AND idpergunta='".$lqtdesub['idpergunta']."' AND idresposta='".$lqtdesub['idresposta']."' AND valor_resp<>'0'";
                                              $eupdatemoni = $_SESSION['query']($updatemoni) or die ("erro na query de atualizaÃ§Ã£o dos valores re-distribuÃ­dos");
                                      }
                                  }
                              }
                      }
                  }
              }
              else {
              }

              if($eupdatemoni) {
                  return true;
              }
              else {
                  return false;
              }
          }
    }

    function CalcValorRedistAval() {
        
          if($this->acao == "edita" && $this>altera == "N") {
          }
          else {
              $valor_redist = 0;
              $selaval = "SELECT ra.idaval_plan, ra.valor FROM rel_aval ra WHERE ra.idplanilha='".$this->plan."'";
              $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliÃ§Ãµes relacionadas a planilha");
              while($lselaval = $_SESSION['fetch_array']($eselaval)) {
                  /* levantar se existem perguntas com respostas que redistribuem valor dentro da avaliaÃ§Ã£o, existindo, o sistema faz todo processo de redistribuiÃ§Ã£o*/
                  $redist_grupo = "SELECT SUM(ma.valor_resp) as soma, COUNT(*) as result FROM $this->tabavalia ma
                                   INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                   WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.avalia='REDIST_AVAL' AND ma.idaval_plan='".$lselaval['idaval_plan']."'";
                  $ergrupo = $_SESSION['fetch_array']($_SESSION['query']($redist_grupo)) or die ("erro na query de consulta da redistribuiÃ§Ã£o por grupo");
                  $count = $ergrupo['result'];
                  if($count >= 1 ) {
                      $valor_dist = $ergrupo['soma'];
                      $newvaval = $lselaval['valor'] - $valor_dist;
                      $rgrupo = "SELECT ma.idaval_plan, ma.valor_final_aval, g.filtro_vinc, ma.idgrupo, ma.valor_grupo, (ma.valor_grupo / ma.valor_aval) as perc, ma.avalia FROM $this->tabavalia ma
                                 INNER JOIN grupo g ON g.idgrupo = ma.idgrupo
                                 WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.avalia<>'TABULACAO' AND ma.avalia<>'REDIST_AVAL' AND ma.idaval_plan='".$lselaval['idaval_plan']."'
                                 GROUP BY ma.idgrupo";
                      $erg = $_SESSION['query']($rgrupo) or die ("erro na consulta das repostas que nÃ£o possuem avaliaÃ§Ã£o ''REDIST_AVAL, FG, FGM, TABULAÇÃO''");
                      while($lrgrupo = $_SESSION['fetch_array']($erg)) {
                          $vperc = $lrgrupo['valor_grupo'] / $newvaval;
                          $vgrupo = round(($vperc * $valor_dist) + $lrgrupo['valor_grupo'], 2);
                          if($lrgrupo['filtro_vinc'] == "P") {
                              $recalc = "SELECT (ma.valor_perg / ma.valor_grupo) as percperg, (ma.valor_resp / ma.valor_perg) as percresp, ma.valor_grupo, ma.valor_resp, ma.valor_perg, ma.idpergunta, ma.idresposta 
                                        FROM $this->tabavalia ma 
                                        WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idaval_plan='".$lrgrupo['idaval_plan']."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia<>'FG' AND ma.avalia<>'FGM' AND ma.avalia<>'TABULACAO' AND ma.avalia<>'REDIST_AVAL'";
                              $erecalc = $_SESSION['query']($recalc) or die ("erro na query de consulta do grupo para re-calculo");
                              while($lrecalc = $_SESSION['fetch_array']($erecalc)) {
                                  $newvperg = round($lrecalc['percperg'] * $vgrupo, 2);
                                  $newvresp = round($lrecalc['percresp'] * $newvperg, 2);
                                  if($lrecalc['valor_resp'] == 0) {
                                      $upgrupo = "UPDATE $this->tabavalia SET valor_final_grup='".$vgrupo."', valor_final_perg='$newvperg' WHERE idgrupo='".$lrgrupo['idgrupo']."' AND idresposta='".$lrecalc['idresposta']."' AND idaval_plan='".$lrgrupo['idaval_plan']."' AND id$this->tabmoni='".$this->idmoni."'";
                                      $eupgrupo = $_SESSION['query']($upgrupo) or die ("erro na query de atualizaÃ§Ã£o dos valores dos grupos");
                                  }
                                  else {
                                          if($lrecalc['valor_resp'] > 0 && $lrgrupo['avalia'] != 'FG' && $lrgrupo['avalia'] != 'FGM') {
                                              $upgrupo = "UPDATE $this->tabavalia SET valor_final_grup='$vgrupo', valor_final_perg='$newvperg', valor_resp='$newvresp' WHERE idgrupo='".$lrgrupo['idgrupo']."' AND idresposta='".$lrecalc['idresposta']."' AND idaval_plan='".$lrgrupo['idaval_plan']."' AND id$this->tabmoni='".$this->idmoni."'";
                                              $eupgrupo = $_SESSION['query']($upgrupo) or die ("erro na query de atualizaÃ§Ã£o dos valores dos grupos");
                                          }
                                          if($lrecalc['valor_resp'] > 0 && ($lrgrupo['avalia'] == 'FG' || $lrgrupo['avalia'] == 'FGM')) {
                                              $upgrupo = "UPDATE $this->tabavalia SET valor_final_grup='".$vgrupo."', valor_final_perg='$newvperg' WHERE idgrupo='".$lrgrupo['idgrupo']."' AND idresposta='".$lrecalc['idresposta']."' AND idaval_plan='".$lrgrupo['idaval_plan']."' AND id$this->tabmoni='".$this->idmoni."'";
                                              $eupgrupo = $_SESSION['query']($upgrupo) or die ("erro na query de atualizaÃ§Ã£o dos valores dos grupos");
                                          }
                                  }
                              }
                          }
                          if($lrgrupo['filtro_vinc'] == "S") {
                              $resub = "SELECT (ma.valor_sub / ma.valor_grupo) as percsub, (ma.valor_perg / ma.valor_sub) as percperg, (ma.valor_resp / ma.valor_perg) as percresp, ma.idgrupo, ma.valor_grupo, ma.idsubgrupo, ma.valor_sub, ma.idpergunta, ma.valor_perg, ma.idresposta, ma.valor_resp FROM $this->tabavalia ma 
                                        WHERE ma.id$this->tabmoni='".$this->idmoni."' AND ma.idaval_plan='".$lrgrupo['idaval_plan']."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND ma.avalia<>'TABULACAO' AND ma.avalia<>'REDIST_AVAL' AND ma.avalia<>'FG' AND ma.avalia<>'FGM'";
                              $ersub = $_SESSION['query']($resub) or die ("erro na query de consulta dos subgrupos vinculados ao grupo");
                              while($lresub = $_SESSION['fetch_array']($ersub)) {
                                  $newvsub = round($lresub['percsub'] * $vgrupo, 2);
                                  $newvperg = round($lresub['percperg'] * $newvsub, 2);
                                  $newvresp = round($lresub['percresp'] * $newvperg, 2);
                                  if($lresub['valor_resp'] == 0) {
                                      $upgrupo = "UPDATE $this->tabavalia ma SET valor_final_grup='".$vgrupo."', valor_final_sub='".$newvsub."', valor_final_perg='".$newvperg."'
                                                  WHERE ma.idaval_plan='".$lrgrupo['idaval_plan']."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND idsubgrupo='".$lresub['idsubgrupo']."' AND idpergunta='".$lresub['idpergunta']."' AND idresposta='".$lresub['idresposta']."' AND id$this->tabmoni='".$this->idmoni."'";
                                      $eupgrupo = $_SESSION['query']($upgrupo) or die ("erro na query de atualizaÃ§Ã£o dos valores dos grupos");
                                  }
                                  else {
                                          if($lresub['valor_resp'] > 0 && $lrgrupo['avalia'] != 'FG' && $lrgrupo['avalia'] != 'FGM') {
                                              $upgrupo = "UPDATE $this->tabavalia ma SET valor_final_grup='".$vgrupo."', valor_final_sub='".$newvsub."', valor_final_perg='".$newvperg."', valor_resp='".$newvresp."'
                                                          WHERE ma.idaval_plan='".$lrgrupo['idaval_plan']."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND idsubgrupo='".$lresub['idsubgrupo']."' AND idpergunta='".$lresub['idpergunta']."' AND idresposta='".$lresub['idresposta']."' AND id$this->tabmoni='".$this->idmoni."'";
                                              $eupgrupo = $_SESSION['query']($upgrupo) or die ("erro na query de atualizaÃ§Ã£o dos valores dos grupos");
                                          }
                                          if($lresub['valor_resp'] > 0 && ($lrgrupo['avalia'] == 'FG' || $lrgrupo['avalia'] == 'FGM')) {
                                              $upgrupo = "UPDATE $this->tabavalia ma SET valor_final_grup='".$vgrupo."', valor_final_sub='".$newvsub."', valor_final_perg='".$newvperg."'
                                                          WHERE ma.idaval_plan='".$lrgrupo['idaval_plan']."' AND ma.idgrupo='".$lrgrupo['idgrupo']."' AND idsubgrupo='".$lresub['idsubgrupo']."' AND idpergunta='".$lresub['idpergunta']."' AND idresposta='".$lresub['idresposta']."' AND id$this->tabmoni='".$this->idmoni."'";
                                              $eupgrupo = $_SESSION['query']($upgrupo) or die ("erro na query de atualizaÃ§Ã£o dos valores dos grupos");
                                          }
                                  }
                              }
                          }
                      }
                      if($count >= 1) {
                          $selredist = "SELECT idresposta, idpergunta, idgrupo, idaval_plan FROM $this->tabavalia WHERE avalia='REDIST_AVAL' AND id$this->tabmoni='".$this->idmoni."' AND idaval_plan='".$lselaval['idaval_plan']."'";
                          $eredist = $_SESSION['query']($selredist) or die ("erro na query de consutla das perguntas que tem seu valor redistribuido");
                          while($lredist = $_SESSION['fetch_array']($eredist)) {
                              $upredist = "UPDATE $this->tabavalia SET valor_resp='0' WHERE id$this->tabmoni='".$this->idmoni."' AND idaval_plan='".$lselaval['idaval_plan']."' AND idresposta='".$lredist['idresposta']."'";
                              $eupredist = $_SESSION['query']($upredist) or die ("erro na query de atualizaÃ§Ã£o dos valores redistribuidos");
                          }
                      }
                  }
              }
          }
    }
    
    function supera() {
        $selpergs = "SELECT * FROM $this->tabavalia WHERE id$this->tabmoni='$this->idmoni' AND avalia='SUPERACAO' GROUP BY idresposta";
        $eselperg = $_SESSION['query']($selpergs) or die ("erro na query de consulta dos itens de superação");
        $nperg = $_SESSION['num_rows']($eselperg);
        if($nperg >= 1) {
            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                $valorsup[$lselperg['idgrupo']][$lselperg['idpergunta']] = $valorsup + $lselperg['valor_resp'];
                $valormoni = $lselperg['valor_fg'];
                $valorfinal = $lselperg['valor_final_aval'];
            }
            foreach($valorsup as $grupo => $perg) {
                 foreach($perg as $kl => $valor) {
                      $ttsup = $ttsup + $valor;
                      $vgrupo[$grupo] = $vgrupo[$grupo] + $valor;
                      $vperg[$kl] = $vperg[$kl] + $valor;
                 }
            }
            foreach($vgrupo as $idgrupo => $vgrup) {
               $atugrupo = "UPDATE $this->tabavalia SET valor_final_grup = (valor_final_grup + $vgrup) WHERE idgrupo='$idgrupo' and id$this->tabmoni='$this->idmoni'";
               $exeatu = $_SESSION['query']($atugrupo) or die ("erro na query de atualização do valor da monitoria");
            }
            foreach($vperg as $idperg => $vperg) {
               $atuperg = "UPDATE $this->tabavalia SET valor_final_perg = (valor_final_perg + $vperg) WHERE idpergunta='$idperg' and id$this->tabmoni='$this->idmoni'";
               $exeatu = $_SESSION['query']($atuperg) or die ("erro na query de atualização do valor da monitoria");
            }
            $newvalor = $valormoni + $ttsup;
            $newfinal = $valorfinal + $ttsup;
            $upmonni = "UPDATE $this->tabavalia SET valor_final_aval='$newfinal', valor_fg='$newvalor' WHERE id$this->tabmoni='$this->idmoni'";
            $eupmoni = $_SESSION['query']($upmonni) or die ("erro na query de atualização do valor da monitoria");
        }
    }
}

?>
