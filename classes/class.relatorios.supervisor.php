<?php

include_once 'class.tabelas.php';

class Relatorios extends TabelasSql {
    public $tipo;
    public $check;
    public $nomerelatorio;
    
    //mensal
    public $filtrom;
    public $fmdados;
    public $paretoncg;
    public $incidencia;
    public $incidenciagrup;
    public $interconfianca;
    
    //semanal e mensal
    public $meses;
    public $cat;
    public $qtdemoni;
    public $qtdemonifg;
    public $filtros;
    public $vars;
    public $tabelasrel;
    public $aliasrel;
    public $vidados;
    public $pareto;
    public $peso;
    public $notas;
    public $evolutivo;
    public $cons;
    public $sqlcons;
    public $nplan;
    public $arvore;
    public $valarvore;
    public $interferencia;
    public $impactoncg;
    public $wheresql;
    public $wherectt;
    public $ncg;
    public $coreps;
    public $dados;
    public $semanas;
    public $duracao;
    public $tempos;
    public $intervalo;
    public $evolutivoncg;
    public $erroamostra;
    public $filtroamostra;
    public $relresp;
    public $relrespsem;
    public $peapont;
    
    //variaveis relatório especifico
    public $operadores;
    public $opernota;
    public $mesoper;
    
    //variáveis relatório grupo específico
    public $gruposespec;
    public $respostaperg;
    public $respostas;
    public $top;
    public $plangrupunif;
    
    //operador
    public $idmonitorias;
    public $media;
    public $dadosoper;
    public $notas_obs;
    public $indice;
    public $histoperador;
    public $carteira;
    
    function Estrutura () {
        $this->filtros = array('ID' => 'idmonitoria','dtinimoni' => 'data','dtfimmoni' => 'data','dtinictt' => 'datactt','dtfimctt' => 'datactt',
              'super_oper' => 'super_oper','oper' => 'operador','nomemonitor' => 'nomemonitor','fg' => 'qtdefg','plan' => 'idplanilha',
              'aval' => 'idaval_plan','grupo' => 'idgrupo','sub' => 'idsubgrupo','perg' => 'idpergunta','resp' => 'idresposta',
              'fluxo' => 'idfluxo','atustatus' => 'idatustatus','indice' => 'idindice','intervalo' => 'idintervalo_notas','tmonitoria' => 'tmonitoria','idalerta' => 'alertas');
        $this->tabelasrel = array('idmonitoria' => 'monitoria','data' => 'monitoria','data' => 'monitoria','datactt' => 'monitoria',
                         'datactt' => 'monitoria','super_oper' => 'super_oper','operador' => 'operador','nomemonitor' => 'monitor',
                         'qtdefg' => 'monitoria','idplanilha' => 'moniavalia','idaval_plan' => 'moniavalia','idgrupo' => 'moniavalia','idsubgrupo' => 'moniavalia',
                         'idpergunta' => 'moniavalia','idresposta' => 'moniavalia','idgrupo' => 'moniavalia','grupo' => 'grupo','pergunta' => 'pergunta', 'idplanilha' => 'moniavalia','idaval_plan' => 'moniavalia','idfluxo' => 'fluxo','idatustatus' => 'rel_fluxo',
                         'idindice' => 'indice','idintervalo_notas' => 'intervalo_notas','idrel_filtros' => 'rel_filtros',
                         'conf_rel' => 'conf_rel','param_moni' => 'param_moni','monitoria_fluxo' => 'monitoria_fluxo','tmonitoria' => 'monitoria','alertas' => 'monitoria');
        $this->cabecalho = array('ID' => 'ID MONITORIA','dtinimoni' => 'DATA MONITORIA','dtfimmoni' => 'DATA MONITORIA','dtinictt' => 'DATA CONTATO','dtfimctt' => 'DATA CONTATO',
                          'super_oper' => 'SUPERVISOR','oper' => 'OPERADOR','nomemonitor' => 'MONITOR','fg' => 'NCG','plan' => 'PLANILHA',
                          'aval' => 'AVALIAÇÃO','grupo' => 'GRUPO','sub' => 'SUBGRUPO','perg' => 'PERGUNTA','resp' => 'RESPOSTA',
                          'fluxo' => 'FLUXO','atustatus' => 'STATUS','indice' => 'INDICE','intervalo' => 'INTERVALO DE NOTA','idalerta' => "ALERTAS");
        $this->meses = array('01' => 'Jan', '02' => 'Fev','03' => 'Mar','04' => 'Abril','05' => 'Maio','06' => 'Jun','07' => 'Jul','08' => 'Ago','09' => 'Set','10' => 'Out','11' => 'Nov','12' => 'Dez');
    }
    
    function coreps () {
        $cores = array('#EA2A3D','#3844EA','#66BD40','#FFEE00','#FF8533','#8C007A','#9C4E4E','#BADDFF','#C7C7C7');
        reset($cores);
        $c = count($cores);
        $seleps = "SELECT idfiltro_dados FROM filtro_dados fd"
                . " INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.ativo='S' AND fd.nivel='1'";
        $eseleps = $_SESSION['query']($seleps) or die (mysql_error());
        while($lseleps = $_SESSION['fetch_array']($eseleps)) {
            $this->coreps[$lseleps['idfiltro_dados']] = current($cores);
            next($cores);
            if(key($cores) == ($c - 1)) {
                reset($cores);
            }
        }
    }
    
    function Geradados () {
        
        //levantamento das variavaies
        $datactt = explode(",",$this->vars['datactt']);
        $verifdt = "SELECT '".$datactt[0]."' >= '2012-01-01' as result";
        $everifdt = $_SESSION['fetch_array']($_SESSION['query']($verifdt)) or die ("erro na query de verificação da data de contato");
        $wherectt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$datactt[0]' AND '$datactt[1]'";
        $this->wherectt = $wherectt;
        foreach($this->vars as $tab => $val) {
            if($tab == "datactt") {
            }
            else {
                if($tab == "data") {
                    $datas[] = explode(",",$val);
                    $where['data'] = $this->aliasrel[$this->tabelasrel[$tab]].".$tab BETWEEN '".$datas[0][0]."' AND '".$datas[0][1]."'";
                }
                else {
                    if($tab == "idrel_filtros") {
                        if($val == "") {
                        }
                        else {
                            $where['idrel_filtros'] = $this->aliasrel[$this->tabelasrel[$tab]].".$tab IN ($val)";
                            $idsrelf['idrel_filtros'] = explode(",",$val);
                        }
                    }
                    else {
                        if($tab == "idintervalo_notas") {
                            $selinter = "SELECT * FROM intervalo_notas WHERE idintervalo_notas='$val'";
                            $eselinter = $_SESSION['fetch_array']($_SESSION['query']($selinter)) or die (mysql_error());
                            $valor = $this->aliasrel[moniavalia].".valor_fg BETWEEN '".$eselinter['numini']."' AND '".$eselinter['numfim']."'";
                            $where['idintervalo_notas'] = $valor;
                            $innerinter = "INNER JOIN intervalo_notas ".$this->aliasrel[intervalo_notas]." ON ".$this->aliasrel[intervalo_notas].".idindice=".$this->aliasrel[indice].".idindice";
                        }
                        else {
                            if($tab == "tmonitoria") {
                                if(count($val) > 1) {
                                    $where['tmonitoria'] = $this->aliasrel[$this->tabelasrel[$tab]].".$tab IN ('".implode("','",$val)."')";
                                }
                                else {
                                    $where['tmonitoria'] = $this->aliasrel[$this->tabelasrel[$tab]].".$tab='".implode("",$val)."'";
                                }
                            }
                            else {
                                if($tab == "qtdefg") {
                                    $where[$tab] = $this->aliasrel[$this->tabelasrel[$tab]].".$tab>='$val'";
                                }
                                else {
                                    $where[$tab] = $this->aliasrel[$this->tabelasrel[$tab]].".$tab='$val'";
                                }
                            }
                        }
                    }
                }
            }
        }
        if($this->tipo != "mensal" && $this->tipo != "semanal" && $this->tipo != "exefluxo" && $this->tipo != "contesta" && $this->tipo != "operador" && $this->tipo != "histoperador") {
            if($this->vars['idplanilha'] != "") {
                $planesp = "AND re.idplanilha='".$this->vars['idplanilha']."'";
            }
            else {
            }
            $selresp = "SELECT nome,idplanilha, re.idresposta,idrel_filtros,p.descriresposta,rr.qtde FROM resp_especificas re
                        INNER JOIN rel_resp_especifica rr ON rr.idrel_resp_especifica = re.idrel_resp_especifica 
                        INNER JOIN pergunta p ON p.idresposta = re.idresposta
                        WHERE re.idrel_resp_especifica='$this->tipo' $planesp";
            $eselresp = $_SESSION['query']($selresp) or die (mysql_error());
            while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                $this->nomerelatorio = $lselresp['nome'];
                $respespe[$lselresp['idresposta']] = $lselresp['idplanilha'];
                $descriresp[] =$lselresp['descriresposta'];
                $idsrelespe = explode(",",$lselresp['idrel_filtros']);
                $qtdeitens = $lselresp['qtde'];
            }
            $descriresp = array_unique($descriresp);
            $idsplanilha = array_values($respespe);
            $idsplanilha = array_unique($respespe);
            if($this->vars['idplanilha'] == "") {
                $whereespe = $this->aliasrel[moniavalia].".idresposta IN (".implode(",",array_keys($respespe)).") AND ".$this->aliasrel[moniavalia].".idplanilha IN (".implode(",",  array_unique(array_values($respespe))).")";
            }
            else {
                $whereespe = $this->aliasrel[moniavalia].".idresposta IN (".implode(",",array_keys($respespe)).")";
            }
            
            if($idsrelespe[0] == "") {
                foreach(explode(",",$this->vars['idrel_filtros']) as $newidrel) {
                    $selconf = "SELECT idplanilha_web FROM conf_rel WHERE idrel_filtros='$newidrel'";
                    $eselconf = $_SESSION['fetch_array']($_SESSION['query']($selconf)) or die ("erro na query de consulta da configuração do relacionamento");
                    $idsplanrel = explode(",",$eselconf['idplanilha_web']);
                    foreach($idsplanrel as $planrel) {
                        if(in_array($planrel,$idsplanilha)) {
                            $relinclui[] = $newidrel;
                        }
                    }
                }
                $newidsrel[] = implode(",",$relinclui);
            }
            else {
                foreach($idsrelf as $chave => $idrel) {
                    foreach($idrel as $newidrel) {
                        $selconf = "SELECT idplanilha_web FROM conf_rel WHERE idrel_filtros='$newidrel'";
                        $eselconf = $_SESSION['fetch_array']($_SESSION['query']($selconf)) or die ("erro na query de consulta da configuração do relacionamento");
                        $idsplanrel = explode(",",$eselconf['idplanilha_web']);
                        foreach($idsplanrel as $planrel) {
                            if(in_array($planrel,$idsplanilha)) {
                                $relinclui[] = $newidrel;
                            }
                            else {

                            }
                        }
                    }
                    $newidsrel[] = implode(",",array_intersect($idsrelespe,$relinclui));
                }
            }
            $where['idrel_filtros'] = $this->aliasrel['rel_filtros'].".idrel_filtros IN (".$newidsrel[0].")";
            $wheresql = implode(" AND ",$where);
            $this->wheresql = $wheresql;
        }
        else {
            $newidsrel[0] = $this->vars['idrel_filtros'];
            $where['idrel_filtros'] = $this->aliasrel['rel_filtros'].".idrel_filtros IN (".$this->vars['idrel_filtros'].")";
            $wheresql = implode(" AND ",$where);
            $this->wheresql = $wheresql;
        }
        
        $check = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                    WHERE $wheresql $wherectt";
        if($newidsrel[0] == "") {
            $echeck['qtde'] = 0;
        }
        else {
            $echeck = $_SESSION['fetch_array']($_SESSION['query']($check)) or die ("erro na query de consulta da quantidade");
        }
        if($echeck['qtde'] == 0) {
            $this->check = 0;
        }
        else {
            if($this->tipo != "operador" && $this->tipo != "histoperador") {
                if(($this->tipo == "mensal" && $this->vars['idplanilha'] == "") OR ($this->tipo != "mensal" && $this->tipo != "semanal" && $this->tipo != "exefluxo" && $this->tipo != "contesta" && $this->vars['idplanilha'] == "")) {
                    $scountf = "SELECT count(*) as result FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                    $escountf = $_SESSION['fetch_array']($_SESSION['query']($scountf)) or die (mysql_error());
                    $self = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                    $eself = $_SESSION['query']($self) or die (mysql_error());
                    while($lself = $_SESSION['fetch_array']($eself)) {
                        $i++;
                        if($i == $escountf['result']) {
                            $arrayult = $i;
                            $ultfiltro[$lself['nomefiltro_nomes']] = $lself['idfiltro_nomes'];
                            $filtros[$i][$lself['idfiltro_nomes']] = $lself['nomefiltro_nomes'];
                        }
                        else {
                            if($i == 1) {
                            }
                            else {
                                $filtros[$i][$lself['idfiltro_nomes']] = $lself['nomefiltro_nomes'];
                            }
                        }
                    }
                    $selgrupo = "select * from filtro_nomes where nivel=((select max(nivel) from filtro_nomes) - 1)";
                    $eselgrupo = $_SESSION['fetch_array']($_SESSION['query']($selgrupo)) or die ("erro na query de consulta do agrupamento");
                    $grupo[$eselgrupo['idfiltro_nomes']] = $eselgrupo['nomefiltro_nomes'];
                    foreach($filtros as $knivel => $nivel) {
                        foreach($nivel as $ft => $nft) {
                            $selfd = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='$ft'";
                            $eselfd = $_SESSION['query']($selfd) or die (mysql_error());
                            while($lselfd = $_SESSION['fetch_array']($eselfd)) {
                                $checkrel = "SELECT COUNT(DISTINCT(id_".strtolower(key($ultfiltro)).")) as result FROM rel_filtros WHERE $this->filtrom AND id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."'";
                                $echeck = $_SESSION['fetch_array']($_SESSION['query']($checkrel)) or die (mysql_error());
                                $checkmoni = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM rel_filtros ".$this->aliasrel[rel_filtros]."
                                            INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idrel_filtros = ".$this->aliasrel[rel_filtros].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE ".$this->aliasrel[rel_filtros].".$this->filtrom 
                                            AND ".$this->aliasrel[rel_filtros].".id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."' AND "//$this->aliasrel[monitoria].".idrel_filtros IN (".$newidsrel[0].") 
                                            ." $wheresql $wherectt";
                                $echeckmoni = $_SESSION['fetch_array']($_SESSION['query']($checkmoni)) or die ("erro na query de consulta das monitorias");
                                if($echeckmoni['result'] >= 1) {
                                    if($knivel != $arrayult) {
                                        $this->fmdados[$nft][$lselfd['nomefiltro_dados']] = $lselfd['idfiltro_dados'];
                                        $variantes[$lselfd['nomefiltro_dados']] = $lselfd['idfiltro_dados'];
                                    }
                                    else {
                                        if($echeck['result'] == 1) {
                                            $selval = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                        WHERE $wheresql $this->wherectt AND ".$this->aliasrel[rel_filtros].".id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."'";
                                            $eselval = $_SESSION['fetch_array']($_SESSION['query']($selval)) or die ("erro na query de consulta da quantidade");
                                            if($eselval['qtde'] >= 1) {
                                                $checkgrupo = "SELECT nomefiltro_dados FROM rel_filtros rf
                                                            INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower($eselgrupo['nomefiltro_nomes'])."
                                                            WHERE $this->filtrom AND id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."' GROUP BY id_".strtolower($eselgrupo['nomefiltro_nomes'])."";
                                                $echeckgrupo = $_SESSION['fetch_array']($_SESSION['query']($checkgrupo)) or die ("erro na query de consulta do agrupamento");
                                                $this->fmdados[$nft."#".$echeckgrupo['nomefiltro_dados']][$lselfd['nomefiltro_dados']] = $lselfd['idfiltro_dados'];
                                                $variantes[$lselfd['nomefiltro_dados']] = $lselfd['idfiltro_dados'];
                                            }
                                        }
                                        else {
                                            $selval = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                        WHERE $wheresql $this->wherectt AND ".$this->aliasrel[rel_filtros].".id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."'";
                                            $eselval = $_SESSION['fetch_array']($_SESSION['query']($selval)) or die ("erro na query de consulta da quantidade");
                                            if($eselval['qtde'] >= 1) {
                                                $selof = "SELECT DISTINCT(idfiltro_dados), nomefiltro_dados, rf.idrel_filtros, fn.nomefiltro_nomes, COUNT(DISTINCT(idmonitoria)) FROM rel_filtros rf
                                                            INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower(key($ultfiltro))."
                                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes
                                                            INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idrel_filtros = rf.idrel_filtros
                                                            WHERE $this->filtrom $this->wherectt AND id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."' GROUP BY id_".strtolower(key($ultfiltro))."";
                                                $eselof = $_SESSION['query']($selof) or die ("erro na query de consulta dos filtros do quinto nível");
                                                $nselof = $_SESSION['num_rows']($eselof);
                                                while($lselof = $_SESSION['fetch_array']($eselof)) {
                                                    $selval = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                                WHERE $wheresql $this->wherectt AND id_".strtolower($nft)."='".$lselfd['idfiltro_dados']."' AND ".$this->aliasrel[rel_filtros].".id_".strtolower($lselof['nomefiltro_nomes'])."='".$lselof['idfiltro_dados']."'";
                                                    $eselval = $_SESSION['fetch_array']($_SESSION['query']($selval)) or die ("erro na query de consulta da quantidade");
                                                    if($eselval['qtde'] >= 1) {
                                                        $checkgrupo = "SELECT nomefiltro_dados FROM rel_filtros rf
                                                                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower($eselgrupo['nomefiltro_nomes'])."
                                                                                    WHERE idrel_filtros='".$lselof['idrel_filtros']."' GROUP BY id_".strtolower($eselgrupo['nomefiltro_nomes'])."";
                                                        $echeckgrupo = $_SESSION['fetch_array']($_SESSION['query']($checkgrupo)) or die ("erro na query de consulta do agrupamento");
                                                        $this->fmdados[$nft."#".$echeckgrupo['nomefiltro_dados']][$lselfd['nomefiltro_dados']."#".$lselof['nomefiltro_dados']] = $lselfd['idfiltro_dados']."#".$lselof['idfiltro_dados'];
                                                        $variantes[$lselfd['nomefiltro_dados']."#".$lselof['nomefiltro_dados']] = $lselfd['idfiltro_dados']."#".$lselof['idfiltro_dados'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    if($this->tipo != "exefluxo" && $this->tipo != "contesta") {
                        $nomeplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='".$this->vars['idplanilha']."'";
                        $eplan = $_SESSION['fetch_array']($_SESSION['query']($nomeplan)) or die ("erro na query de consulta do nome da planilha");
                        $this->nplan = $eplan['descriplanilha'];

                        $selult = "SELECT * FROM filtro_nomes ORDER BY nivel LIMIT 2";
                        $eselult = $_SESSION['query']($selult) or die ("erro na query de consulta dos dois ultimos filtros");
                        while($lselult = $_SESSION['fetch_array']($eselult)) {
                            $fquebras[] = $lselult['nomefiltro_nomes'];
                        }
                        $colunas = $this->AliasTab(rel_filtros).".id_status_do_negociador,".$this->AliasTab(rel_filtros).".id_carteira_ccusto,";
                        $relplan = "SELECT DISTINCT(".$this->AliasTab(rel_filtros).".idrel_filtros) as idrel,$colunas ".$this->AliasTab('filtro_'.strtolower($fquebras[0])).".nomefiltro_dados as ".strtolower($fquebras[0]).", ".$this->AliasTab('filtro_'.strtolower($fquebras[1])).".nomefiltro_dados as ".strtolower($fquebras[1]).", ".$this->AliasTab(conf_rel).".idplanilha_web FROM conf_rel ".$this->AliasTab(conf_rel)."
                                            INNER JOIN rel_filtros ".$this->AliasTab(rel_filtros)." ON ".$this->AliasTab(rel_filtros).".idrel_filtros = ".$this->AliasTab(conf_rel).".idrel_filtros
                                            INNER JOIN filtro_dados ".$this->AliasTab('filtro_'.strtolower($fquebras[0]))." ON ".$this->AliasTab('filtro_'.strtolower($fquebras[0])).".idfiltro_dados = ".$this->AliasTab(rel_filtros).".id_".strtolower($fquebras[0])."
                                            INNER JOIN filtro_dados ".$this->AliasTab('filtro_'.strtolower($fquebras[1]))." ON ".$this->AliasTab('filtro_'.strtolower($fquebras[1])).".idfiltro_dados = ".$this->AliasTab(rel_filtros).".id_".strtolower($fquebras[1])."
                                            WHERE ".$this->AliasTab(rel_filtros).".idrel_filtros IN (".$this->vars['idrel_filtros'].")";
                        $erelplan = $_SESSION['query']($relplan) or die ("erro na query de consulta da configuração do relacionamento");
                        while($lrelplan = $_SESSION['fetch_array']($erelplan)) {
                            $plans = explode(",",$lrelplan['idplanilha_web']);
                            if(in_array($this->vars['idplanilha'],$plans)) {
                                $idsrel[] = $lrelplan['idrel'];
                                if(eregi("PARCEIROS",$this->nplan) && $_POST['filtro_'.strtolower($fquebras[1])] == "") {
                                    $cons = $this->aliasrel[rel_filtros].".id_".strtolower($fquebras[1])."='".$lrelplan['id_'.strtolower($fquebras[1])]."'";
                                    $this->sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($fquebras[1])."=";
                                }
                                else {
                                    $cons = $this->aliasrel[rel_filtros].".id_".strtolower($fquebras[0])."='".$lrelplan['id_'.strtolower($fquebras[0])]."'";
                                    $this->sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($fquebras[0])."=";
                                }
                                if($everifdt['result'] >= 1) {
                                    $sqlncg = "";
                                }
                                else {
                                    $sqlncg = "AND ".$this->aliasrel[monitoria].".qtdefg = 0";
                                }
                                $checkval = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $this->wherectt AND $cons $sqlncg";
                                $echeckval = $_SESSION['fetch_array']($_SESSION['query']($checkval)) or die ("erro na query de verificação de monitorias");
                                if($echeckval['qtde'] == 0) {
                                }
                                else {
                                    if(eregi("PARCEIROS",$this->nplan) && $_POST['filtro_'.strtolower($fquebras[1])] == "") {
                                        $this->cons[$lrelplan[strtolower($fquebras[1])]] = $lrelplan['id_'.strtolower($fquebras[1])];
                                        $variantes[$lrelplan[strtolower($fquebras[1])]] = $lrelplan['id_'.strtolower($fquebras[1])];
                                    }
                                    else {
                                        $this->cons[$lrelplan[strtolower($fquebras[0])]] = $lrelplan['id_'.strtolower($fquebras[0])];
                                        $variantes[$lrelplan[strtolower($fquebras[0])]] = $lrelplan['id_'.strtolower($fquebras[0])];
                                    }
                                }
                            }
                            else {
                            }
                        }
                    }
                }
            }
            $this->check = 1;
            
            if($this->tipo == "operador") {
                //cria tabela temporária para notas
                $dtctt = explode(",",$this->vars['datactt']);
                $data = explode(",",$this->vars['data']);
                $selperiodo = "SELECT idperiodo FROM periodo WHERE dataini='".$data[0]."' AND datafim='".$data[1]."' AND dtinictt='".$dtctt[0]."' AND dtfimctt='".$dtctt[1]."'";
                $eperiodo = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_error());
                $selant = "SELECT * FROM periodo WHERE idperiodo < '".$eperiodo['idperiodo']."' ORDER BY idperiodo DESC LIMIT 1";
                $eselant = $_SESSION['query']($selant) or die (mysql_error());
                $nperant = $_SESSION['num_rows']($eselant);
                $create = "CREATE TEMPORARY TABLE tmp_avg (idmonitoria int(7) unsigned zerofill,valor_final_aval decimal(10,2),valor_fg decimal(10,2),idoperador int (7)) ENGINE=MEMORY";
                $ecreate = $_SESSION['query']($create) or die ("erro na query de criação da tabela temporária");
                
                $selmoni = "SELECT ".$this->aliasrel[monitoria].".idmonitoria, ".$this->aliasrel[monitoria].".idplanilha, valor_fg,".$this->aliasrel[monitoria].".obs,".$this->aliasrel[param_moni].".idindice,
                            ".$this->aliasrel[monitoria].".idoperador, ".$this->aliasrel[operador].".operador, ".$this->aliasrel[fila_grava].".data_de_admissao,
                            ".$this->aliasrel[fila_grava].".tipo_contrato,".$this->aliasrel[fila_grava].".turno FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN fila_grava ".$this->aliasrel[fila_grava]." ON ".$this->aliasrel[fila_grava].".idfila_grava = ".$this->aliasrel[monitoria].".idfila
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE $wheresql $this->wherectt group by ".$this->aliasrel[monitoria].".idmonitoria ORDER BY ".$this->aliasrel[monitoria].".data";
                $eselmoni = $_SESSION['query']($selmoni) or die (mysql_error());
                while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                    $idmoni = $lselmoni['idmonitoria'];
                    $this->idmonitorias[$lselmoni['idoperador']][$lselmoni['idplanilha']][] = $lselmoni['idmonitoria'];
                    $this->notas_obs[$lselmoni['idoperador']][$lselmoni['idmonitoria']]['nota'] = $lselmoni['valor_fg'];
                    $this->notas_obs[$lselmoni['idoperador']][$lselmoni['idmonitoria']]['obs'] = $lselmoni['obs'];
                    $indices[$lselmoni['idoperador']][] = $lselmoni['idindice'];
                    $this->dadosoper[$lselmoni['idoperador']]['operador'] = $lselmoni['operador'];
                    $this->dadosoper[$lselmoni['idoperador']]['dataadm'] = banco2data($lselmoni['data_de_admissao']);
                    $this->dadosoper[$lselmoni['idoperador']]['tipo_contrato'] = $lselmoni['tipo_contrato'];
                    $this->dadosoper[$lselmoni['idoperador']]['turno'] = $lselmoni['turno'];
                }
                foreach($indices as $indoper => $ind) {
                    $this->indice[$indoper] = array_unique($ind);
                }
                
                if($nperant >= 1) {
                    $lselant = $_SESSION['fetch_array']($eselant);
                    $wheredtant = $this->aliasrel[monitoria].".data BETWEEN '".$lselant['dataini']."' AND '".$lselant['datafim']."'";
                    $wherecttant = " AND ".$this->aliasrel[monitoria].".datactt BETWEEN '".$lselant['dtinictt']."' AND '".$lselant['dtfimctt']."'";
                }
                $create = "CREATE TEMPORARY TABLE tmp_avgant (idmonitoria int(7) unsigned zerofill,valor_final_aval decimal(10,2),valor_fg decimal(10,2),idoperador int(7)) ENGINE=MEMORY";
                $ecreate = $_SESSION['query']($create) or die ("erro na query de criação da tabela temporária");
                
                $notastt = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg, ".$this->aliasrel[monitoria].".idoperador FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE $wheredtant $wherecttant";
                $inserttmp = "INSERT INTO tmp_avgant (idmonitoria, valor_final_aval,valor_fg,idoperador) $notastt";
                $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                foreach($this->dadosoper as $idoper => $dados) {
                    if($nperant == 0) {
                        $this->media['anterior'] = "-0";
                        $this->dadosoper[$idoper]['mediaant'] = "";
                    }
                    else {
                        $avg = "SELECT COUNT(idmonitoria) as qtde, AVG(valor_fg) as media FROM tmp_avgant WHERE idoperador='$idoper'";
                        $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                        $this->media['anterior'] = number_format(round($eavg['media'],2),2)."-".$eavg['qtde'];
                        $this->dadosoper[$idoper]['mediaant'] = number_format(round($eavg['media'],2),2);
                    }
                }
                $set = "SET SQL_SAFE_UPDATES=0;";
                $eset =$_SESSION['query']($set) or die (mysql_error());
                $limpa = "DELETE FROM tmp_avg";
                $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                
                $notastt = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg, ".$this->aliasrel[monitoria].".idoperador FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE $wheresql $this->wherectt";
                $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg,idoperador) $notastt";
                $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                foreach($this->dadosoper as $idoper => $dados) {
                    $avg = "SELECT COUNT(idmonitoria) as qtde, AVG(valor_fg) as media FROM tmp_avg WHERE idoperador='$idoper'";
                    $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                    $this->media['atual'] = number_format(round($eavg['media'],2),2)."-".$eavg['qtde'];
                    $this->dadosoper[$idoper]['mediaatual'] = number_format(round($eavg['media'],2),2);
                }
                $set = "SET SQL_SAFE_UPDATES=0;";
                $eset =$_SESSION['query']($set) or die (mysql_error());
                $limpa = "DELETE FROM tmp_avg";
                $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
            }
            
            if($this->tipo == "histoperador") {
                $dtctt = explode(",",$this->vars['datactt']);
                $data = explode(",",$this->vars['data']);
                $selperiodo = "SELECT idperiodo,nmes,ano FROM periodo WHERE dataini='".$data[0]."' AND datafim='".$data[1]."' AND dtinictt='".$dtctt[0]."' AND dtfimctt='".$dtctt[1]."'";
                $eperiodo = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_error());
                $datash[$eperiodo['nmes']."-".$eperiodo['ano']]['data'] = explode(",",$this->vars['data']);
                $datash[$eperiodo['nmes']."-".$eperiodo['ano']]['dtctt'] = explode(",",$this->vars['datactt']); 
                $selant = "SELECT * FROM periodo WHERE idperiodo < '".$eperiodo['idperiodo']."' ORDER BY datafim desc";
                $eselant = $_SESSION['query']($selant) or die (mysql_error());
                $nperant = $_SESSION['num_rows']($eselant);
                if($nperant >= 1) {
                    while($lselant = $_SESSION['fetch_array']($eselant)) {
                        $p++;
                        $datash[$lselant['nmes']."-".$lselant['ano']]['data'][] = $lselant['dataini'];
                        $datash[$lselant['nmes']."-".$lselant['ano']]['data'][] = $lselant['datafim'];
                        $datash[$lselant['nmes']."-".$lselant['ano']]['dtctt'][] = $lselant['dtinictt'];
                        $datash[$lselant['nmes']."-".$lselant['ano']]['dtctt'][] = $lselant['dtfimctt'];
                        if($p > 12) {
                            break;
                        }
                    }
                }
                $create = "CREATE TEMPORARY TABLE tmp_avg (idmonitoria int(7) unsigned zerofill,valor_final_aval decimal(10,2),valor_fg decimal(10,2),idoperador int (7)) ENGINE=MEMORY";
                $ecreate = $_SESSION['query']($create) or die ("erro na query de criação da tabela temporária");
                
                foreach($datash as $mes => $intervalo) {
                    $meses[] = $mes;
                    $where['data'] = "".$this->aliasrel[monitoria].".data BETWEEN '".$intervalo['data'][0]."' AND '".$intervalo['data'][1]."'";
                    $wherecttant = " AND ".$this->aliasrel[monitoria].".datactt BETWEEN '".$intervalo['dtctt'][0]."' AND '".$intervalo['dtctt'][1]."'";
                    $notastt = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg, ".$this->aliasrel[monitoria].".idoperador FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE ".implode(" AND ",$where)." $wherecttant";
                    $enotastt = $_SESSION['query']($notastt) or die (mysql_error());
                    $nnotas = $_SESSION['num_rows']($enotastt);
                    if($nnotas >= 1) {
                        $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg,idoperador) $notastt";
                        $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção da tabela temporária");
                        $seloper = "SELECT idoperador,ROUND(AVG(valor_fg),2) as nota FROM tmp_avg GROUP BY idoperador";
                        $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consulta dos operadores noa tabela temporária");
                        while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                            $selult = "SELECT idmonitoria FROM monitoria WHERE idoperador='".$lseloper['idoperador']."' ORDER BY idmonitoria DESC LIMIT 1";
                            $eselult = $_SESSION['fetch_array']($_SESSION['query']($selult)) or die (mysql_error());
                            $selmoni = "SELECT operador, super_oper, rf.id_carteira_ccusto,data_de_admissao,nomemonitor,m.idrel_filtros FROM monitoria m "
                                    . " INNER JOIN fila_grava fg ON fg.idfila_grava = m.idfila"
                                    . " INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros"
                                    . " INNER JOIN operador o ON o.idoperador = m.idoperador"
                                    . " INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper"
                                    . " INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor"
                                    . " WHERE idmonitoria='".$eselult['idmonitoria']."'";
                            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_error());
                            if($this->carteira != "") {
                                $selfiltro = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='$this->carteira'";
                            }
                            else {
                                $selfiltro = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$eselmoni['id_carteira_ccusto']."'";
                            }
                            $eselfilto = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die (mysql_error());
                            $this->indice[$eselmoni['operador']][$mes] = $eselmoni['idrel_filtros'];
                            $this->histoperador[$eselmoni['operador']][$mes] = $lseloper['nota'];
                            $this->dadosoper[$eselmoni['operador']]['nome'] = $eselmoni['operador'];
                            $this->dadosoper[$eselmoni['operador']]['supervisor'] = $eselmoni['super_oper'];
                            $this->dadosoper[$eselmoni['operador']]['monitor'] = $eselmoni['nomemonitor'];
                            $this->dadosoper[$eselmoni['operador']]['carteira'] = $eselfilto['nomefiltro_dados'];
                            $this->dadosoper[$eselmoni['operador']]['admissao'] = banco2data($eselmoni['data_de_admissao']);
                        }
                    }
                    $apagadados = "DELETE FROM tmp_avg";
                    $eapaga = $_SESSION['query']($apagadados) or die ("erro para apagar os dados da tabela temporária");
                }
                $this->meses = array_reverse($meses);
                ksort($this->histoperador);
                $deltmp = "DROP TABLE tmp_avg";
                $eseltmp = $_SESSION['query']($deltmp) or die (mysql_error());
            }

            if($this->tipo == "mensal" OR $this->tipo == "semanal") {

            //cria tabela temporária para notas
            $create = "CREATE TEMPORARY TABLE tmp_avg (idmonitoria int(7) unsigned zerofill,valor_final_aval decimal(10,2),valor_fg decimal(10,2)) ENGINE=MEMORY";
            $ecreate = $_SESSION['query']($create) or die ("erro na query de criação da tabela temporária");

            // levanta quantidade de monitorias
            $fgs = array('comfg' => "",'semfg' => " AND ".$this->aliasrel[monitoria].".qtdefg=0");
            $variantes["GLOBAL"] = "";
            foreach($variantes as $varqtde => $idvarqtde) {
                if($varqtde == "GLOBAL") {
                    $wherevarqtde = "";
                    $sqlcons = "";
                }
                else {
                    $colsf = array();
                    $chave = explode("#",$varqtde);
                    $valchave = explode("#",$idvarqtde);
                    if(count($chave) == 1) {
                        $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                        INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$varqtde'";
                        $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta do nome do filtro");
                        $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($eselcol['nomefiltro_nomes'])."='$idvarqtde'";
                        //$ideps = $fd;
                    }
                    else {
                        foreach($chave as $kchave => $vchave) {
                            $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                            while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                $colsf[] = $lselcol['nomefiltro_nomes'];
                            }
                        }
                        $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                    }
                }
                $wherevarqtde = $sqlcons;
                foreach($fgs as $cfg => $paramfg) {
                    $qtde = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria),valor_final_aval,valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt $paramfg $wherevarqtde";
                    $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $qtde";
                    $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                    $avg = "SELECT COUNT(idmonitoria) as qtde, AVG(valor_final_aval) as valor, AVG(valor_fg) as valor_fg FROM tmp_avg";
                    $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                    $this->qtde[$varqtde][$cfg]['qtde'] = $eavg['qtde'];
                    $this->qtde[$varqtde][$cfg]['media_'.$cfg] = $eavg['valor'];
                    $this->qtde[$varqtde][$cfg]['media_fg'] = $eavg['valor_fg'];
                    $set = "SET SQL_SAFE_UPDATES=0;";
                    $eset =$_SESSION['query']($set) or die (mysql_error());
                    $limpa = "DELETE FROM tmp_avg";
                    $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                }
            }

            //grafico nota média EPS
            $interncg = array("AND qtdefg=0" => "valor_final_aval","AND ((".$this->aliasrel[monitoria].".checkfg = 'NCG OK') OR (".$this->aliasrel[monitoria].".checkfg='NCG NOK' AND qtdefg='0') OR (".$this->aliasrel[monitoria].".checkfg IS NULL AND qtdefg=0))" => "valor_fg");
            $calc = "valor_fg";
            $sqlmediancg = "";

            $notastt = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                        WHERE $wheresql $this->wherectt $sqlmediancg";
            $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $notastt";
            $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
            $avg = "SELECT COUNT(idmonitoria) as qtde, AVG($calc) as $calc FROM tmp_avg";
            $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
            $this->notas['total'][] = round($eavg[$calc],1);
            $set = "SET SQL_SAFE_UPDATES=0;";
            $eset =$_SESSION['query']($set) or die (mysql_error());
            $limpa = "DELETE FROM tmp_avg";
            $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
            
            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
                foreach($this->fmdados as $kfiltro => $n1) {
                    $c = count($n1);
                    $cloop = 0;
                    $ids = array();
                    foreach($n1 as $kfd => $fd) {
                        $cloop++;
                        $calc = "valor_fg";
                        $sqlmediancg = "";
                        $sqlcons = "";
                        $colsf = array();
                        $chave = explode("#",$kfd);
                        $valchave = explode("#",$fd);
                        if(count($chave) == 1) {
                            $col = explode("#",$kfiltro);
                            $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($col[0])."='$fd'";
                            $ids[] = $fd;
                            //$ideps = $fd;
                        }
                        else {
                            foreach($chave as $kchave => $vchave) {
                                $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                        INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                    $colsf[] = $lselcol['nomefiltro_nomes'];
                                }
                            }
                            $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                            $ids[] = $valchave[0];
                        }

                        if(count($ids) == $c) {
                            if(count($chave) == 1) {
                                $coltt = explode("#",$kfiltro);
                            }
                            else {
                                $coltt = $colsf; 
                            }
                            $sqlmncgtt = "";
                            $sqlconstt = $this->aliasrel[rel_filtros].".id_".strtolower($coltt[0])." IN ('".implode("','",$ids)."')";
                            $selnotastt = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $this->wherectt AND $sqlconstt $sqlmncgtt";
                            $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selnotastt";
                            $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                            $avg = "SELECT COUNT(idmonitoria) as qtde, AVG(valor_fg) as media FROM tmp_avg";
                            $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                            $this->notas[$kfiltro]['total'] = round($eavg['media'],1);
                            $set = "SET SQL_SAFE_UPDATES=0;";
                            $eset =$_SESSION['query']($set) or die (mysql_error());
                            $limpa = "DELETE FROM tmp_avg";
                            $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                        }

                        $selnotas = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $wheresql $this->wherectt AND $sqlcons $sqlmediancg";
                        $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selnotas";
                        $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                        $avg = "SELECT COUNT(idmonitoria) as qtde, MIN($calc) as min, MAX($calc) as max, AVG($calc) as media FROM tmp_avg";
                        $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                        if($eavg['qtde'] == 0) {
                            //$this->notas[$kfiltro]['minima'][] = 0;
                            //$this->notas[$kfiltro]['media'][] = 0;
                            //$this->notas[$kfiltro]['maxima'][] = 0;
                        }
                        else {
                            $this->notas[$kfiltro]['minima'][] = round($eavg['min'],1);
                            $this->notas[$kfiltro]['media'][] = round($eavg['media'],1);
                            $this->notas[$kfiltro]['maxima'][] = round($eavg['max'],1);
                        }
                        $set = "SET SQL_SAFE_UPDATES=0;";
                        $eset =$_SESSION['query']($set) or die (mysql_error());
                        $limpa = "DELETE FROM tmp_avg";
                        $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                    }
                }
            }
            else {
                foreach($this->cons as $nome => $ideps) {
                    $calc = "valor_fg";
                    $sqlmediancg = "";
                    $selnotas = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt AND $this->sqlcons'$ideps' $sqlmediancg";
                    $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selnotas";
                    $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                    $avg = "SELECT COUNT(idmonitoria) as qtde, MIN($calc) as min, MAX($calc) as max, AVG($calc) as media FROM tmp_avg";
                    $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                    if($eavg['qtde'] == 0) {
                    }
                    else {
                        $this->notas['minima'][] = round($eavg['min'],1);
                        $this->notas['media'][] = round($eavg['media'],1);
                        $this->notas['maxima'][] = round($eavg['max'],1);
                    }
                    $set = "SET SQL_SAFE_UPDATES=0;";
                    $eset =$_SESSION['query']($set) or die (mysql_error());
                    $limpa = "DELETE FROM tmp_avg";
                    $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");

                    foreach($interncg as $wherencg => $colncg) {
                        if($colncg == "valor_final_aval") {
                            $tpcol = "media";
                        }
                        if($colncg == "valor_fg") {
                            $tpcol = "mediancg";
                        }
                        $selnotasncg = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND $this->sqlcons'$ideps' $wherencg";
                        $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selnotasncg";
                        $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                        $avg = "SELECT COUNT(idmonitoria) as qtde, MIN($colncg) as min, MAX($colncg) as max, AVG($colncg) as media FROM tmp_avg";
                        $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                        $this->impactoncg[$nome][$tpcol] = $eavg['media'];
                        $set = "SET SQL_SAFE_UPDATES=0;";
                        $eset =$_SESSION['query']($set) or die (mysql_error());
                        $limpa = "DELETE FROM tmp_avg";
                        $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                    }
                }
            }

            //levantamento semanas
            if($this->tipo == "mensal") {
                $s = 0;
                $dtini = explode(",",$this->vars['data']);
                $dtinivar = $dtini[0];
                $timeinivar = mktime(0, 0, 0, substr($dtinivar,5,2), substr($dtinivar,8,2), substr($dtinivar,0,4));
                $seldt = "SELECT idperiodo,dataini,datafim,dtinictt,dtfimctt FROM periodo WHERE '$dtini[0]' between dataini and datafim";
                $eselper = $_SESSION['fetch_array']($_SESSION['query']($seldt)) or die (mysql_error());
                $selmeses = "SELECT * FROM periodo WHERE dataini <= '".$eselper['dataini']."' ORDER BY dataini DESC";
                $eselmeses = $_SESSION['query']($selmeses) or die (mysql_error());
                while($lselmeses = $_SESSION['fetch_array']($eselmeses)) {
                    $s++;
                    if($s <= 12) {
                        if($s <= 3) {
                            $semanas[$lselmeses['idperiodo']] =  $lselmeses['dataini'].",".$lselmeses['datafim']."#".$lselmeses['dtinictt'].",".$lselmeses['dtfimctt'];
                        }
                        $this->semanas[$lselmeses['idperiodo']] = $lselmeses['dataini'].",".$lselmeses['datafim']."#".$lselmeses['dtinictt'].",".$lselmeses['dtfimctt'];
                    }
                }
                ksort($this->semanas);
            }
            else {
                $dt = explode(",",$this->vars['data']);
                $diaini = substr($dt[0],0,4)."-".substr($dt[0],5,2)."-".substr($dt[0],8,2);
                $diafim = substr($dt[1],0,4)."-".substr($dt[1],5,2)."-".substr($dt[1],8,2);
                $data = $diaini;
                $s = 0;
                $i = 0;
                $f = 1;
                for(;$i <= $f;) {
                    $s++;
                    $data = $diaini;
                    $sem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($data,5,2),substr($data,8,2),substr($data,0,4)));
                    $acres = 6 - $sem;
                    $compdt = "SELECT DATE_ADD('$data',INTERVAL $acres DAY) as fimsem";
                    $ecomp = $_SESSION['fetch_array']($_SESSION['query']($compdt)) or die ("erro na query de consulta das datas");
                    $fimmes = "SELECT '".$ecomp['fimsem']."' > '$diafim' as result";
                    $efimmes = $_SESSION['fetch_array']($_SESSION['query']($fimmes)) or die ("verifica o fim do mes");
                    if($efimmes['result'] >= 1) {
                        $messem[$s] = $data.",".$diafim;
                    }
                    else {
                        $messem[$s] = $data.",".$ecomp['fimsem'];
                    }
                    $acresdt = "SELECT DATE_ADD('".$ecomp['fimsem']."',INTERVAL 1 DAY) as fimsem";
                    $eacres = $_SESSION['fetch_array']($_SESSION['query']($acresdt)) or die ("erro na query de consulta das datas");
                    $diaini = $eacres['fimsem'];
                    $datas = "SELECT '$diaini' <= '$diafim' as result";
                    $edatas = $_SESSION['fetch_array']($_SESSION['query']($datas)) or die ("erro na query de comparação das datas");
                    if($edatas['result'] >= 1) {
                        $i = 0;
                    }
                    else {
                        $i = 2;
                    }
                }
            }
            
            /** dados do gráfico evolutivo especifico por grupo "PLANILHA INTERNA - NEGOCIAÇÃO", "PLANILHA INTERNA - RECADO" e "PLANILHA ITAU"
             * @gruposespecificos "NEGOCIAÇÃO", "RECADO" e "CONTATO
             */
            if($this->tipo == "mensal" && $this->vars['idplanilha'] != "") {
                $pergfora = array("0008" => "000134,000135,000136,000137,000138,000139");
                $gruposesp = array('0002' => '000005','0003' => '000009','0004' => '000016','0005' => '000002','0008' => '000032','0007' => '000026,000027','0010' => '000036,000037','0009' => '000048','0011' => '000055,000056','0012'  => '000060,000062,000063','0013' => '000067,000068,000069');
                $this->plangrupunif = array('0002','0003','0004','0005','0008','0007','0010','0009','0013');
                
                $selpergs = "SELECT pa.idgrupo,idrel,filtro_vinc,p.descripergunta,p.idpergunta,s.descrisubgrupo,idsubgrupo, descrigrupo FROM planilha pa
                            INNER JOIN grupo gp ON gp.idgrupo = pa.idgrupo
                            LEFT JOIN pergunta p ON p.idpergunta = gp.idrel
                            LEFT JOIN subgrupo s ON s.idsubgrupo = gp.idrel
                            WHERE idplanilha='".$this->vars['idplanilha']."' AND p.ativo='S' GROUP BY idrel";
                $eselpergs = $_SESSION['query']($selpergs) or die (mysql_error());
                while($lselpergs = $_SESSION['fetch_array']($eselpergs)) {
                    if($this->vars['idplanilha'] == "0008" && in_array($lselpergs['idpergunta'],explode(",",$pergfora[$this->vars['idplanilha']]))) {
                    }
                    else {
                        $checkgrupo = "SELECT count(*) as r FROM grupo gp
                                    LEFT JOIN pergunta p ON p.idpergunta = gp.idrel
                                    LEFT JOIN subgrupo s ON s.idsubgrupo = gp.idrel
                                    WHERE idgrupo='".$lselpergs['idgrupo']."' AND p.ativo='S' AND p.avalia IN ('AVALIACAO','PERC','FGM','FG')";
                        $echeckgp = $_SESSION['fetch_array']($_SESSION['query']($checkgrupo)) or die (mysql_error());
                        if($echeckgp['r'] >= 1) {
                            $grupos[$lselpergs['idgrupo']][] = $lselpergs['idpergunta'];
                            $perggrupo[$lselpergs['idpergunta']] = $lselpergs['idgrupo'];
                            $top10[$lselpergs['idpergunta']] = $lselpergs['descripergunta'];
                            if(in_array($lselpergs['idgrupo'],explode(",",$gruposesp[$this->vars['idplanilha']]))) {
                                if($lselpergs['filtro_vinc'] == "P") {
                                    $selperg = "SELECT * FROM pergunta WHERE idpergunta='".$lselpergs['idrel']."' AND valor_perg is not null GROUP BY idpergunta";
                                    $eselperg = $_SESSION['query']($selperg) or die (mysql_error());
                                    $nperg = $_SESSION['num_rows']($eselperg);
                                    if($nperg >= 1) {
                                        $perguntas[$lselpergs['idpergunta']] = $lselpergs['descripergunta'];
                                        $selop = "SELECT descriresposta, idpergunta,idresposta FROM pergunta WHERE idpergunta='".$lselpergs['idpergunta']."'";
                                        $eselop = $_SESSION['query']($selop) or die (mysql_error());
                                        while($lselop = $_SESSION['fetch_array']($eselop)) {
                                            $respostas[$lselop['descriresposta']][$lselpergs['idpergunta']] = $lselop['idresposta'];
                                        }                            
                                    }
                                }
                            }
                        }
                    }
                }
                
                foreach($this->semanas as $seminci) {
                    $newhere = array();
                    $datasem = explode("#",$seminci);
                    $sem = explode(",",$datasem[0]);
                    $semctt = explode(",",$datasem[1]);
                    $wheredt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$semctt[0]' AND '$semctt[1]'";
                    foreach($where as $ch => $w) {
                        if($ch == "data") {
                            $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$sem[0]' AND '$sem[1]'";
                        }
                        else {
                            $newhere[] = $w;
                        }
                    }
                    
                    $this->incidenciagrup['categoria'][] = $this->meses[substr($semctt[1],5,2)]."/".substr($semctt[1],0,4);
                    $totalmoni = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE ".implode(" AND ",$newhere)." $wheredt";
                   $etotalmoni = $_SESSION['fetch_array']($_SESSION['query']($totalmoni)) or die (mysql_error());
                   foreach($grupos as $idgrupo => $idpergs) {
                       $totalgrupo = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde, descrigrupo FROM moniavalia ".$this->aliasrel[moniavalia]."
                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[grupo].".idgrupo='$idgrupo' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','FG','FGM')";
                       $etotalgrupo = $_SESSION['fetch_array']($_SESSION['query']($totalgrupo)) or die (mysql_error());
                       $this->incidenciagrup['grupos'][$idgrupo][] = round(($etotalgrupo['qtde']/$etotalmoni['qtde']) * 100,2);
                       foreach($idpergs as $kp => $idp) {
                           $totalperg = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde, descripergunta FROM moniavalia ".$this->aliasrel[moniavalia]."
                                    INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                    INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                    INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[pergunta].".idpergunta='$idp' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','FG','FGM')";
                           $etotalperg = $_SESSION['fetch_array']($_SESSION['query']($totalperg)) or die (mysql_error());
                           $this->incidenciagrup['gruposperg'][$idgrupo][$idp][] = round(($etotalperg['qtde']/$etotalmoni['qtde']) * 100,2);
                       }
                   }
                }
                
                $loop = 3;
                ksort($semanas);
                foreach($semanas as $ksem => $se) {
                    $newhere = array();
                    $totalpareto = 0;
                    $selnmes = "SELECT nmes FROM periodo WHERE idperiodo='$ksem'";
                    $eselnmes = $_SESSION['fetch_array']($_SESSION['query']($selnmes)) or die (mysql_error());
                    $newhere = array();
                    $datasem = explode("#",$se);
                    $sem = explode(",",$datasem[0]);
                    $semctt = explode(",",$datasem[1]);
                    $wheredt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$semctt[0]' AND '$semctt[1]'";
                    foreach($where as $ch => $w) {
                        if($ch == "data") {
                            $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$sem[0]' AND '$sem[1]'";
                        }
                        else {
                            $newhere[] = $w;
                        }
                    }
                    
                    $totalmoni = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE ".implode(" AND ",$newhere)." $wheredt";
                   $etotalmoni = $_SESSION['fetch_array']($_SESSION['query']($totalmoni)) or die (mysql_error());
                    
                    if($loop >= 0) {
                        foreach($perguntas as $item => $descri) {
                            if(in_array($this->vars['idplanilha'],$this->plangrupunif)) {
                                if(!in_array($descri,$this->gruposespec['acompanhamento'][1]['perguntas'])) {
                                    $this->gruposespec['acompanhamento'][1]['perguntas'][] = $descri;
                                }
                            }
                            else {
                                if(!in_array($descri,$this->gruposespec['acompanhamento'][$perggrupo[$item]]['perguntas'])) {
                                    $this->gruposespec['acompanhamento'][$perggrupo[$item]]['perguntas'][] = $descri;
                                }
                            }
                            $total = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                     INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                     INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                     INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                     INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                     INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                     INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                     INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                     INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                     INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                     INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                     INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                     INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                     INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                     WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','PERC','FG') AND ".$this->aliasrel[moniavalia].".idpergunta='$item'";
                            $etotal = $_SESSION['fetch_array']($_SESSION['query']($total)) or die ("erro na query de consulta do total de monitorias");
                            $gppareto[$descri] = $etotal['qtde'];
                            $totalpareto = $totalpareto + $etotal['qtde'];
                            if(in_array($this->vars['idplanilha'],$this->plangrupunif)) {
                                $this->gruposespec['acompanhamento'][1]['comparativo'][$eselnmes['nmes']][] = round(($etotal['qtde'] / $etotalmoni['qtde']) * 100,2);
                            }
                            else {
                                $this->gruposespec['acompanhamento'][$perggrupo[$item]]['comparativo'][$eselnmes['nmes']][] = round(($etotal['qtde'] / $etotalmoni['qtde']) * 100,2);
                            }
                            if(end($semanas) == $se) {
                                //somatório peso final da pergunta
                                $paretogrupo = "SELECT SUM(".$this->aliasrel[moniavalia].".valor_perg - ".$this->aliasrel[moniavalia].".valor_final_perg) as valortt, SUM(".$this->aliasrel[moniavalia].".valor_perg) as valor FROM moniavalia ".$this->aliasrel[moniavalia]."
                                          INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                          INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                          INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                          INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                          INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                          INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                          INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                          INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                          INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                          INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                          INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                          INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                          INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                          WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[moniavalia].".idpergunta='$item'";
                                $eparetogrupo = $_SESSION['fetch_array']($_SESSION['query']($paretogrupo)) or die ("erro na query de consulta do total de monitorias");
                                
                                //somatório do valor final das perguntas de todas perguntas
                                $ptgrupott = "SELECT SUM(".$this->aliasrel[moniavalia].".valor_perg - ".$this->aliasrel[moniavalia].".valor_final_perg) as valortt FROM moniavalia ".$this->aliasrel[moniavalia]."
                                          INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                          INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                          INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                          INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                          INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                          INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                          INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                          INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                          INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                          INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                          INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                          INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                          INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                          WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[moniavalia].".idgrupo IN (".$gruposesp[$this->vars['idplanilha']].")";
                                $eptgrupott = $_SESSION['fetch_array']($_SESSION['query']($ptgrupott)) or die ("erro na query de consulta do total de monitorias");
                                
                                $grupopareto[$descri] = round(($eparetogrupo['valortt']/$eptgrupott['valortt']) * 100,2);
                                foreach($respostas as $descrip => $id) {
                                    $totalresp = "SELECT descriresposta,".$this->aliasrel[pergunta].".avalia, COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                            INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                            INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                            INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[moniavalia].".idresposta='$id[$item]'";
                                    $etotalresp = $_SESSION['fetch_array']($_SESSION['query']($totalresp)) or die ("erro na query de consulta do total de monitorias");
                                    if($etotalresp['avalia'] == "AVALIACAO" OR $etotalresp['avalia'] == "FG" OR $etotalresp['avalia'] == "PERC") {
                                        if(in_array($this->vars['idplanilha'],$this->plangrupunif)) {
                                            $this->respostas[1][$descrip][] = $etotalresp['qtde'];
                                            $this->respostaperg[1][$descrip][] = $etotalresp['qtde'];
                                        }
                                        else {
                                            $this->respostas[$perggrupo[$item]][$descrip][] = $etotalresp['qtde'];
                                            $this->respostaperg[$perggrupo[$item]][$descrip][] = $etotalresp['qtde'];
                                        }
                                    }
                                    else {
                                        if(in_array($this->vars['idplanilha'],$this->plangrupunif)) {
                                            $this->respostas[1][$descrip][] = $etotalresp['qtde'];
                                            $this->respostaperg[1][$descrip][] = $etotalresp['qtde'];
                                        }
                                        else {
                                            $this->respostas[$perggrupo[$item]][$descrip][] = $etotalresp['qtde'];
                                            $this->respostaperg[$perggrupo[$item]][$descrip][] = $etotalresp['qtde'];
                                        }
                                    }
                                }
                            }
                        }
                        $loop = $loop--;
                        
                        //top 10 apontamentos
                        foreach($this->cons as $keps => $ideps) {
                            foreach($top10 as $idp => $desc) {
                                $totalresp = "SELECT descriresposta,".$this->aliasrel[pergunta].".avalia, COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                            INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                            INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                            INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE ".implode(" AND ",$newhere)." $wheredt AND ".$this->aliasrel[moniavalia].".idpergunta='$idp' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','FG','FGM')";
                                $etotalresp = $_SESSION['fetch_array']($_SESSION['query']($totalresp)) or die ("erro na query de consulta do total de monitorias");
                                $top['GLOBAL'][$desc] = $etotalresp['qtde'];
                                $totalresp = "SELECT descriresposta,".$this->aliasrel[pergunta].".avalia, COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                            INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                            INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                            INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE ".implode(" AND ",$newhere)." $wheredt AND $this->sqlcons'$ideps' AND ".$this->aliasrel[moniavalia].".idpergunta='$idp' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','FG','FGM')";
                                $etotalresp = $_SESSION['fetch_array']($_SESSION['query']($totalresp)) or die ("erro na query de consulta do total de monitorias");
                                $top[$keps][$desc] = $etotalresp['qtde'];
                            }
                        }
                    }
                }
                arsort($grupopareto);
                arsort($top);
                foreach($top as $filtro => $topdados) {
                    $ctop = 0;
                    arsort($topdados); 
                    foreach($topdados as $perg => $qt) {
                        if($ctop <= 9) {
                            $this->top[$filtro][$perg] = $qt;
                        }
                        else {
                            break;
                        }
                        $ctop++;
                    }
                }
                foreach($grupopareto as $gr => $vpareto) {
                    $acum = $acum + $vpareto;
                    if($acum >= 99.8 && $acum <= 100.3) {
                        $acum = 100;
                    }
                    $this->gruposespec['paretopeso']['perc'][$gr] = $vpareto;
                    $this->gruposespec['paretopeso']['acum'][$gr] = $acum;
                }
                arsort($gppareto);
                foreach($gppareto as $item => $val) {
                    $ac = $ac + round(($val / $totalpareto) * 100,1);
                    if($ac >= 99.8 && $ac <= 100.3) {
                        $ac = 100;
                    }
                    $this->gruposespec['pareto']['perc'][$item] = round(($val / $totalpareto) * 100,1);
                    $this->gruposespec['pareto']['acum'][$item] = $ac;
                }
            }
            
            /** @tabela_nota_media operadores comparativo de 2 meses
             * 
             */
            if($this->tipo == "mensal") {
                krsort($semanas);
                $loop = 3;
                $create = "CREATE TEMPORARY TABLE nota_media (idoperador int(7) unsigned zerofill,idmonitoria int(7) unsigned zerofill,valor_fg decimal(10,2)) ENGINE=MEMORY";
                $ecreate = $_SESSION['query']($create) or die (mysql_error());
                foreach($semanas as $ksem => $sem) {
                    if($loop >= 1) {
                        $selnmes = "SELECT nmes FROM periodo WHERE idperiodo='$ksem'";
                        $eselnmes = $_SESSION['fetch_array']($_SESSION['query']($selnmes)) or die (mysql_error());
                        $this->mesoper[] = $eselnmes['nmes'];
                        $newhere = array();
                        $datasem = explode("#",$sem);
                        $semtrab = explode(",",$datasem[0]);
                        $semctt = explode(",",$datasem[1]);
                        $wheredt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$semctt[0]' AND '$semctt[1]'";
                        foreach($where as $ch => $w) {
                            if($ch == "data") {
                                $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$semtrab[0]' AND '$semtrab[1]'";
                            }
                            else {
                                $newhere[] = $w;
                            }
                        }
                        $notastt = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria), ".$this->aliasrel[monitoria].".idoperador, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE ".implode(" AND ",$newhere)." $wheredt";
                        $inserttmp = "INSERT INTO nota_media (idmonitoria,idoperador,valor_fg) $notastt";
                        $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                        $selnotas = "SELECT idoperador,AVG(valor_fg) as vl FROM nota_media GROUP BY idoperador";
                        $eselnotas = $_SESSION['query']($selnotas) or die (mysql_error());
                        while($lselnotas = $_SESSION['fetch_array']($eselnotas)) {
                            $this->opernota[$eselnmes['nmes']][$lselnotas['idoperador']] = round($lselnotas['vl'],2);
                            $seloper = "SELECT * FROM operador WHERE idoperador=".$lselnotas['idoperador']."";
                            $loper = $_SESSION['fetch_array']($_SESSION['query']($seloper));
                            $operador[$loper['idoperador']] = $loper['operador'];
                        }
                        $limpatab = "DELETE FROM nota_media";
                        $elimpa = $_SESSION['query']($limpatab) or die (mysql_error());
                    }
                    $loop--;
                }
                $delnota = "DROP TABLE nota_media";
                $edelnota = $_SESSION['query']($delnota) or die (mysql_error());
                arsort($this->opernota[$this->mesoper[0]]);
                ksort($semanas);
                foreach($this->opernota as $mes => $opers) {
                    foreach($opers as $oper => $nt) {
                        foreach($semanas as $ksem => $sem) {
                            $newhere = array();
                            $separa = explode("#",$sem);
                            $dtctt = explode(',',$separa[1]);
                            $semtrab = explode(",",$separa[0]);
                            $newdt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$dtctt[0]' AND '$dtctt[1]'";
                            foreach($where as $ch => $w) {
                                if($ch == "data") {
                                    $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$semtrab[0]' AND '$semtrab[1]'";
                                }
                                else {
                                    $newhere[] = $w;
                                }
                            }
                            if($oper == "0000619") {
                                $check = "OK";
                            }
                            $seldados = "SELECT count(*) as r,operador,super_oper,data_de_admissao,".$this->aliasrel[monitoria].".idrel_filtros,".$this->aliasrel[monitoria].".idplanilha FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN fila_grava ".$this->aliasrel[fila_grava]." ON ".$this->aliasrel[fila_grava].".idfila_grava = ".$this->aliasrel[monitoria].".idfila
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE ".implode(" AND ",$newhere)." $newdt AND ".$this->aliasrel[monitoria].".idoperador='$oper' ORDER BY ".$this->aliasrel[monitoria].".idmonitoria DESC LIMIT 1";
                            $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die (mysql_error());
                            if($eseldados['r'] >= 1) {
                                $this->operadores[$oper]['operador'] = $eseldados['operador'];
                                $this->operadores[$oper]['super_oper'] = $eseldados['super_oper'];
                                $this->operadores[$oper]['data_de_admissao'] = $eseldados['data_de_admissao'];
                                $this->operadores[$oper]['emp3_emp4'] = $eseldados['emp3_emp4'];
                                $this->operadores[$oper]['idplanilha'] = $eseldados['idplanilha'];
                                $this->operadores[$oper]['idrel_filtros'] = $eseldados['idrel_filtros'];
                            }
                        }
                    }
                }                
            }
            
            

            //dados evolutivo semanal
            if($this->tipo == "mensal") {
            }
            else {
                foreach($messem as $sem) {
                    $dtsem = explode(",",$sem);
                    $vdtini = "SELECT '$datactt[0]' BETWEEN '$dtsem[0]' AND '$dtsem[1]' as result";
                    $evdtini = $_SESSION['fetch_array']($_SESSION['query']($vdtini)) or die ("erro na queru de comparação da data inicial");
                    $vdtfim = "SELECT '$datactt[1]' BETWEEN '$dtsem[0]' AND '$dtsem[1]' as result";
                    $evdtfim = $_SESSION['fetch_array']($_SESSION['query']($vdtfim)) or die ("erro na queru de comparação da data inicial");
                    $vdtinifim = "SELECT '$dtsem[0]' BETWEEN '$datactt[0]' AND '$datactt[1]' data1,'$dtsem[1]' BETWEEN '$datactt[0]' AND '$datactt[1]' data2";
                    $evdinifim = $_SESSION['fetch_array']($_SESSION['query']($vdtinifim)) or die ("erro na verficação da semana");
                    if($evdtini['result'] >= 1 && $evdtfim['result'] >= 1) {
                        $this->semanas[] = $this->vars['data']."#".$datactt[0].",".$datactt[1];
                    }
                    if($evdtini['result'] >= 1 && $evdtfim['result'] == 0) {
                        $this->semanas[] = $datactt[0].",".$dtsem[1]."#".$datactt[0].",".$datactt[1];
                    }
                    if($evdtini['result'] == 0 && $evdtfim['result'] >= 1) {
                        $this->semanas[] = $dtsem[0].",".$datactt[1]."#".$datactt[0].",".$datactt[1];
                    }
                    if($evdinifim['data1'] >= 1 && $evdinifim['data2'] >= 1 && $datactt[0] != $dtsem[0] && $datactt[1] != $dtsem[1]) {
                        $this->semanas[] = $dtsem[0].",".$dtsem[1]."#".$datactt[0].",".$datactt[1];
                    }
                }
            }
            foreach($this->semanas as $semana) {
                $newhere = array();
                $datasem = explode("#",$semana);
                $sem = explode(",",$datasem[0]);
                $semctt = explode(",",$datasem[1]);
                $wheredt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$semctt[0]' AND '$semctt[1]'";
                foreach($where as $ch => $w) {
                    if($ch == "data") {
                        $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$sem[0]' AND '$sem[1]'";
                    }
                    else {
                        $newhere[] = $w;
                    }
                }
                $sqlevott = "";
                $calc = "valor_fg";
                $selmedia = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria) as result, valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE ".implode(" AND ",$newhere)." $wheredt $sqlevott";
                $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selmedia";
                $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                $avg = "SELECT COUNT(idmonitoria) as qtde, MIN($calc) as min, MAX($calc) as max, AVG($calc) as media FROM tmp_avg";
                $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                //$emedia = $_SESSION['fetch_array']($_SESSION['query']($selmedia)) or die ("erro na query de consulta da nota media");
                if($eavg['qtde'] == 0) {
                }
                else {
                    $this->evolutivo['GLOBAL'][$datasem[1]] = round($eavg['media'],1);
                    $set = "SET SQL_SAFE_UPDATES=0;";
                    $eset =$_SESSION['query']($set) or die (mysql_error());
                    $limpa = "DELETE FROM tmp_avg";
                    $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                }

                if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
                    foreach($this->fmdados as $smfiltro => $smdados) {
                        foreach($smdados as $smcol => $smid) {
                            $calc = "valor_fg";
                            $sqlmediancg = "";
                            $sqlcons = "";
                            $chave = explode("#",$smcol);
                            $valchave = explode("#",$smid);
                            if(count($chave) == 1) {
                                $col = explode("#",$smfiltro);
                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($col[0])."='$smid'";
                                //$ideps = $fd;
                            }
                            else {
                                foreach($chave as $kchave => $vchave) {
                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                    $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                        $colsf[] = $lselcol['nomefiltro_nomes'];
                                    }
                                }
                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                            }
                            $selmediaeps = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria) as result, valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE ".implode(" AND ",$newhere)." $wheredt AND $sqlcons $sqlmediancg";
                            $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selmediaeps";
                            $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                            $avg = "SELECT COUNT(idmonitoria) as qtde, MIN($calc) as min, MAX($calc) as max, AVG($calc) as media FROM tmp_avg";
                            $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                            //$emediaeps = $_SESSION['fetch_array']($_SESSION['query']($selmediaeps)) or die ("erro na query de consulta da nota media");
                            if($eavg['qtde'] >= 1) {
                                $this->evolutivo[$smcol][$datasem[1]] = round($eavg['media'],1);
                                $set = "SET SQL_SAFE_UPDATES=0;";
                                $eset =$_SESSION['query']($set) or die (mysql_error());
                                $limpa = "DELETE FROM tmp_avg";
                                $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                            }
                        }
                    }
                }
                else {
                    foreach($this->cons as $nomeeps => $idepsm) {
                        $calc = "valor_fg";
                        $sqlmediancg = "";
                        $selmediaeps = "SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria) as result, valor_final_aval, valor_fg FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE ".implode(" AND ",$newhere)." $wheredt AND $this->sqlcons'$idepsm' $sqlmediancg";
                        $inserttmp = "INSERT INTO tmp_avg (idmonitoria, valor_final_aval,valor_fg) $selmediaeps";
                        $einserttmp = $_SESSION['query']($inserttmp) or die ("erro na query de inserção dos dados na tabela temporária");
                        $avg = "SELECT COUNT(idmonitoria) as qtde, MIN($calc) as min, MAX($calc) as max, AVG($calc) as media FROM tmp_avg";
                        $eavg = $_SESSION['fetch_array']($_SESSION['query']($avg)) or die ("erro na consulta da média da tabela tmp_avg");
                        //$emediaeps = $_SESSION['fetch_array']($_SESSION['query']($selmediaeps)) or die ("erro na query de consulta da nota media");
                        if($eavg['qtde'] >= 1) {
                            $this->evolutivo[$nomeeps][$datasem[1]] = round($eavg['media'],1);
                            $set = "SET SQL_SAFE_UPDATES=0;";
                            $eset =$_SESSION['query']($set) or die (mysql_error());
                            $limpa = "DELETE FROM tmp_avg";
                            $elimpa = $_SESSION['query']($limpa) or die ("erro para limpar a tabela tmp_avg");
                        }
                    }
                }
            }
            $drop = "DROP TABLE tmp_avg";
            $edrop = $_SESSION['query']($drop) or die ("erro na query para apagar a tabela tmp_avg");

            // gráfico de pareto
            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
            }
            else {
                $sgrupos = "SELECT p.idgrupo, g.descrigrupo, g.filtro_vinc FROM planilha p
                            INNER JOIN grupo g ON g.idgrupo = p.idgrupo 
                            WHERE p.idplanilha='".$this->vars['idplanilha']."' AND p.ativo='S' AND g.tab='N' GROUP BY p.idgrupo ORDER BY p.posicao";
                $esgrupos = $_SESSION['query']($sgrupos) or die ("erro na query de consulta dos grupos vinculados a planilha");
                while($lselgrupo = $_SESSION['fetch_array']($esgrupos)) {
                    $this->cat[$lselgrupo['idgrupo']] = $lselgrupo['descrigrupo'];
                }
                
                //
                $total = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                         INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                         INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                         INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                         INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                         INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                         INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                         INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                         INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                         INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                         INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                         INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                         INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                         INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                         WHERE $wheresql $this->wherectt AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','PERC') AND ".$this->aliasrel[grupo].".fg='N'";
                $etotal = $_SESSION['fetch_array']($_SESSION['query']($total)) or die ("erro na query de consulta do total de monitorias");
                foreach($this->cat as $kidgrupo => $nomeg) {
                    $verifperg = "SELECT g.fg FROM planilha p
                                 INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                                 WHERE p.idplanilha='".$this->vars['idplanilha']."' AND g.idgrupo='$kidgrupo'
                                 GROUP BY g.idgrupo";
                    $everifperg = $_SESSION['fetch_array']($_SESSION['query']($verifperg)) or die ("erro na query de consulta da pergunta se pertence ao grupo de FG");
                    if($everifperg['fg'] == "S") {
                    }
                    else {
                        $qtde = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='$kidgrupo' 
                                AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','PERC')";
                        $eqtde = $_SESSION['fetch_array']($_SESSION['query']($qtde));
                        $percpa[$nomeg] = $eqtde['qtde'];
                    }
                }
                arsort($percpa);
                foreach($percpa as $nval => $percd) {
                    $qp++;
                    if($qp > 6) {
                        $pdem = $pdem + $percd;
                    }
                    else {
                        $percs[$nval] = round(($percd / $etotal['qtde']) * 100,1);
                        $valores[$nval] = $percd;
                    }
                }
                arsort($valores);
                arsort($percs);
                $valores['DEMAIS'] = $pdem;
                $percs['DEMAIS'] = round(($pdem / $etotal['qtde']) * 100,1);
                foreach($percs as $kidperc => $perc) {
                    $perct = round(($perct + $perc),1);
                    if($perct == "99.9" OR $perct == "100.1" OR $perct == "100.2") {
                        $perct = round($perct,0);
                    }
                    else {
                        $perct = round($perct,1);
                    }
                    $this->pareto['GLOBAL']['percacu'][$kidperc] = $perct;
                    $this->pareto['GLOBAL']['qtde'][$kidperc] = $valores[$kidperc];
                    $this->pareto['GLOBAL']['perc'][$kidperc] = $perc;
                }
                foreach($this->cons as $neps => $eps) {
                    $qpc = 0;
                    $perct = "";
                    $pdemcons = "";
                    $percscon = array();
                    $percs = array();
                    $valores = array();
                    $totaleps = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','PERC') AND ".$this->aliasrel[grupo].".fg='N'
                                AND $this->sqlcons'$eps'";
                    $etotaleps = $_SESSION['fetch_array']($_SESSION['query']($totaleps)) or die ("erro na query de consulta do total de monitorias");
                    foreach($this->cat as $kidgrupo => $nomeg) {
                        $verifperg = "SELECT g.fg FROM planilha p
                                            INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                                            WHERE p.idplanilha='".$this->vars['idplanilha']."' AND g.idgrupo='$kidgrupo'
                                            GROUP BY g.idgrupo";
                        $everifperg = $_SESSION['fetch_array']($_SESSION['query']($verifperg)) or die ("erro na query de consulta da pergunta se pertence ao grupo de FG");
                        if($everifperg['fg'] == "S") {
                        }
                        else {
                            $qtde = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                    INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                    INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                    INNER JOIN grupo ".$this->aliasrel[grupo]." ON ".$this->aliasrel[grupo].".idgrupo = ".$this->aliasrel[moniavalia].".idgrupo AND ".$this->aliasrel[grupo].".idrel = ".$this->aliasrel[moniavalia].".idpergunta
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='$kidgrupo' 
                                    AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','PERC')
                                    AND $this->sqlcons'$eps'";
                            $eqtde = $_SESSION['fetch_array']($_SESSION['query']($qtde));
                            $percscon[$nomeg] = $eqtde['qtde'];
                        }
                    }
                    arsort($percscon);
                    foreach($percscon as $npacom => $valpa) {
                        $qpc++;
                        if($qpc > 6) {
                            $pdemcons = $pdemcons + $valpa;
                        }
                        else {
                            $percs[$npacom] = round(($valpa / $etotaleps['qtde']) * 100,1);
                            $valores[$npacom] = $valpa;
                        }
                    }
                    arsort($valores);
                    arsort($percs);
                    $valores['DEMAIS'] = $pdemcons;
                    $percs['DEMAIS'] = round(($pdemcons / $etotaleps['qtde']) * 100,1);
                    foreach($percs as $kidperc => $perc) {
                        $perct = round(($perct + $perc),1);
                        if($perct == "99.8" OR $perct == "99.9" OR $perct == "100.1" OR $perct == "100.2") {
                            $perct = round($perct,0);
                        }
                        else {
                            $perct = round($perct,1);
                        }
                        $this->pareto[$neps]['percacu'][$kidperc] = $perct;
                        $this->pareto[$neps]['qtde'][$kidperc] = $valores[$kidperc];
                        $this->pareto[$neps]['perc'][$kidperc] = $perc;
                    }
                }
            }

            // pareto por peso de resposta
            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
            }
            else {
                foreach($this->cat as $pidgrupo => $pdescri) {
                    $selitem = "SELECT COUNT(*) as result FROM pergunta WHERE idpergunta='$pidgrupo' AND avalia='FG'";
                    $eselitem = $_SESSION['fetch_array']($_SESSION['query']($selitem)) or die (mysql_error());
                    $sqlpesoncg = "";
                    if(($eselitem['result'] >= 0 && $everifdt['result'] >= 1) OR ($eselitem['result'] == 0 && $everifdt['result'] == 0)) {
                        $qtdep = "SELECT SUM(".$this->aliasrel[moniavalia].".valor_perg - ".$this->aliasrel[moniavalia].".valor_final_perg) as valor , COUNT(".$this->aliasrel[moniavalia].".idmonitoria) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                 INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                 INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                 INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                 INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                 INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                 INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                 INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                 INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                 INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                 INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                 INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                 INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                 WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='$pidgrupo' 
                                 AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','FG','FGM','PERC') $sqlpesoncg";
                        $eqtdep = $_SESSION['query']($qtdep);
                        while($lqtdep = $_SESSION['fetch_array']($eqtdep)) {
                            $pesoqtde[$pdescri] = $lqtdep['qtde'];
                            $pesogrupo[$pdescri] = $lqtdep['valor'];
                            $tpeso = $tpeso + $lqtdep['valor'];
                        }
                        foreach($this->cons as $pideps => $peps) {
                            $qtdepeps = "SELECT SUM(".$this->aliasrel[moniavalia].".valor_perg - ".$this->aliasrel[moniavalia].".valor_final_perg) as valor , COUNT(".$this->aliasrel[moniavalia].".idmonitoria) as qtde FROM moniavalia ".$this->aliasrel[moniavalia]."
                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='$pidgrupo' AND $this->sqlcons'$peps' 
                                AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','FG','FGM','PERC') $sqlpesoncg";
                            $eqtdepeps = $_SESSION['fetch_array']($_SESSION['query']($qtdepeps)) or die (mysql_error());
                            $pesoqteps[$pideps][$pdescri] = $eqtdepeps['qtde'];
                            $pesovalor[$pideps][$pdescri] = $eqtdepeps['valor'];
                            $tpespeps[$pideps] = $tpespeps[$pideps] + $eqtdepeps['valor'];
                        }
                    }
                    else {
                    }
                }
                arsort($pesogrupo);
                //arsort($pesovalor);
                foreach($pesogrupo as $g => $v) {
                    $qtp++;
                    $vperc = round(($v / $tpeso) * 100,1);
                    if($qtp > 6) {
                        $pesodem = round(($pesodem + $vperc),1);
                        if($pesodem == "99.8" OR $pesodem == "99.9" OR $pesodem == "100.1" OR $pesodem == "100.2") {
                            $pesodem = round($pesodem,0);
                        }
                    }
                    else {
                        $vpercacu = round(($vpercacu + $vperc),1);
                        if($vpercacu == "99.8" OR $vpercacu == "99.9" OR $vpercacu == "100.1" OR $vpercacu == "100.2") {
                            $vpercacu = round($vpercacu,0);
                        }
                        $this->peso['GLOBAL']['perc'][$g] = $vperc;
                        $this->peso['GLOBAL']['percacu'][$g] = $vpercacu;
                    }
                }
                $this->peso['GLOBAL']['perc']['DEMAIS'] = $pesodem;
                $this->peso['GLOBAL']['percacu']['DEMAIS'] = round(($vpercacu + $pesodem),0);
                foreach($pesovalor as $veps => $vdescri) {
                    $qtpcon = 0;
                    $vpdem = 0;
                    arsort($vdescri);
                    $vacu = 0;
                    foreach($vdescri as $descri => $vpeso) {
                        $qtpcon++;
                        $vperceps = round(($vpeso / $tpespeps[$veps]) * 100,1);
                        if($qtpcon > 6) {
                            $vpdem = round(($vpdem + $vperceps),1);
                        }
                        else {
                        $vacu = round(($vacu + $vperceps),1);
                            if($vacu == "99.8" OR $vacu == "99.9" OR $vacu == "100.1" OR $vacu == "100.2") {
                                $vacu = round($vacu,0);
                            }
                            else {
                            }
                            $this->peso[$veps]['perc'][$descri] = $vperceps;
                            $this->peso[$veps]['percacu'][$descri] = $vacu;
                        }
                    }
                    $this->peso[$veps]['perc']['DEMAIS'] = $vpdem;
                    $this->peso[$veps]['percacu']['DEMAIS'] = round(($vacu + $vpdem),0);
                }
            }

            // arvore planilha
            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
            }
            else {
                $aplan = $this->AliasTab(planilha);
                $agrupo = $this->AliasTab(grupo);
                $aperg = $this->AliasTab(pergunta);
                
                $tab = "SELECT $aplan.idgrupo,$agrupo.descrigrupo FROM planilha $aplan
                            INNER JOIN grupo $agrupo ON $agrupo.idgrupo = $aplan.idgrupo
                            WHERE $aplan.ativo='S' AND $aplan.idplanilha='".$this->vars['idplanilha']."' GROUP BY $aplan.idgrupo ORDER BY $aplan.posicao";
                $etab = $_SESSION['query']($tab) or die (mysql_error());
                while($ltab = $_SESSION['fetch_array']($etab)) {
                    $idsgrupotab[$ltab['idgrupo']] = $ltab['descrigrupo'];
                }
                foreach($idsgrupotab as $idtab => $ntab) {
                    $seltipo = "SELECT filtro_vinc, idrel FROM grupo WHERE idgrupo='$idtab' GROUP BY idrel ORDER BY posicao";
                    $eseltipo = $_SESSION['query']($seltipo) or die ("erro na query de consulta do grupo");
                    while($lseltipo = $_SESSION['fetch_array']($eseltipo)) {
                        if($lseltipo['filtro_vinc'] == "S") {
                            $selsub = "SELECT * FROM subgrupo $asub
                                    INNER JOIN pergunta $aperg ON $aperg.idpergunta = $asub.idpergunta
                                    WHERE idsubgrupo='".$lseltipo['idrel']."' AND $aperg.avalia='TABULACAO' WHERE GROUP BY $asub.idpergunta ORDER BY $asub.posicao";
                            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta do subgrupo");
                            while($lselsub = $_SESSION['fetch_array']($eselsub)) {
                                $selperg = "SELECT * FROM pergunta WHERE idpergunta='".$lselsub['idperg']."' AND avalia='TABULACAO'";
                                $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta da pergunta");
                                $nperg = $_SESSION['num_rows']($eselperg);
                                if($nperg == 0) {
                                }
                                else {
                                    $this->arvoretab[$lselsub['idpergunta']][] = $eselperg['idresposta'];
                                }
                            }
                        }
                        if($lseltipo['filtro_vinc'] == "P") {
                            $i = 0;
                            $selperg = "SELECT * FROM pergunta WHERE idpergunta='".$lseltipo['idrel']."' AND avalia='TABULACAO'";
                            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta da pergunta");
                            $nperg = $_SESSION['num_rows']($eselperg);
                            if($nperg == 0) {
                            }
                            else {
                                while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                                    $this->arvoretab[$lselperg['idpergunta']][] = $lselperg['idresposta'];
                                }
                            }
                        }
                    }
                }
                
                foreach($this->cons as $keps  => $neps) {
                    foreach($this->arvoretab as $idptab => $resptab) {
                        $sresp = "SELECT * FROM pergunta WHERE idpergunta='$idptab'";
                        $eselresp = $_SESSION['query']($sresp) or die (mysql_error());
                        while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                            $svalor = "SELECT COUNT(*) as result FROM moniavalia ".$this->aliasrel[moniavalia]."
                                       INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                       INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                       INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                       INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                       INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                       INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                       INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                       INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                       INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                       INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                       INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                       INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                       WHERE $wheresql $wherectt AND ".$this->aliasrel[moniavalia].".idresposta='".$lselresp['idresposta']."' AND $this->sqlcons'$neps'";
                            $esvalor = $_SESSION['fetch_array']($_SESSION['query']($svalor)) or die ("erro na query de consulta da quantidades de respostas");
                            $this->valtab[$idptab][$lselresp['idresposta']][$keps] = $esvalor['result']."#".round((($esvalor['result'] / $this->qtde[$keps]['comfg']['qtde']) * 100),1)."#".$this->qtde[$keps]['comfg']['qtde'];
                            $this->valtab[$idptab]['total'] = $this->valtab[$idptab]['total'] + $esvalor['result'];
                        }
                    }
                }
                
                $sgrupo = "SELECT $aplan.idgrupo,$agrupo.descrigrupo FROM planilha $aplan
                            INNER JOIN grupo $agrupo ON $agrupo.idgrupo = $aplan.idgrupo
                            WHERE $aplan.ativo='S' AND $aplan.idplanilha='".$this->vars['idplanilha']."' AND $agrupo.tab='N' 
                            GROUP BY $aplan.idgrupo ORDER BY $aplan.posicao";
                $esgrupo = $_SESSION['query']($sgrupo) or die ("erro na consulta das perguntas");
                while($lsgrupo = $_SESSION['fetch_array']($esgrupo)) {
                    $idsgrupo[$lsgrupo['idgrupo']] = $lsgrupo['descrigrupo'];
                }
                foreach($idsgrupo as $idgrupo => $ngrupo) {
                    $seltipo = "SELECT filtro_vinc, idrel FROM grupo WHERE idgrupo='$idgrupo' GROUP BY idrel ORDER BY posicao";
                    $eseltipo = $_SESSION['query']($seltipo) or die ("erro na query de consulta do grupo");
                    while($lseltipo = $_SESSION['fetch_array']($eseltipo)) {
                        if($lseltipo['filtro_vinc'] == "S") {
                            $selsub = "SELECT * FROM subgrupo $asub
                                    INNER JOIN pergunta $aperg ON $aperg.idpergunta = $asub.idpergunta
                                    WHERE idsubgrupo='".$lseltipo['idrel']."' WHERE GROUP BY $asub.idpergunta ORDER BY $asub.posicao";
                            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta do subgrupo");
                            while($lselsub = $_SESSION['fetch_array']($eselsub)) {
                                $this->arvore[$idgrupo][$lseltipo['idrel']][$lselsub['idpergunta']] = $eselsub['descripergunta'];
                            }
                        }
                        if($lseltipo['filtro_vinc'] == "P") {
                            $i = 0;
                            $selperg = "SELECT *,COUNT(*) as result FROM pergunta WHERE idpergunta='".$lseltipo['idrel']."' AND avalia<>'TABULACAO'";
                            $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da pergunta");
                            if($eselperg['result'] == 0) {
                            }
                            else {
                                $this->arvore[$idgrupo]['0'][$eselperg['idpergunta']] = $eselperg['descripergunta'];
                            }

                        }
                    }
                }

                foreach($this->cons as $keps  => $neps) {
                    foreach($this->arvore as $idg => $sub) {
                        foreach($sub as $pergs) {
                            foreach($pergs as $idp => $np) {                                
                                $vresp = "SELECT idresposta,avalia FROM pergunta WHERE idpergunta='$idp'";
                                $evresp = $_SESSION['query']($vresp) or die ("erro na query de consulta da quantidade de respostas FG");
                                while($lresp = $_SESSION['fetch_array']($evresp)) {
                                    $selgrupo = "SELECT * FROM grupo WHERE idrel='$idp' GROUP BY idgrupo";
                                    $eselgrupo = $_SESSION['fetch_array']($_SESSION['query']($selgrupo)) or die (mysql_error());
                                    $stvalor = "SELECT COUNT(*) as result FROM moniavalia ".$this->aliasrel[moniavalia]."
                                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='".$eselgrupo['idgrupo']."'
                                                AND $this->sqlcons'$neps'";
                                    $estvalor = $_SESSION['fetch_array']($_SESSION['query']($stvalor)) or die ("erro na query de consulta da quantidade de respostas da pergunta");
                                    $svalor = "SELECT COUNT(*) as result FROM moniavalia ".$this->aliasrel[moniavalia]."
                                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idpergunta='$idp' AND ".$this->aliasrel[moniavalia].".idresposta = '".$lresp['idresposta']."'
                                                AND $this->sqlcons'$neps'";
                                    $esvalor = $_SESSION['fetch_array']($_SESSION['query']($svalor)) or die ("erro na query de consulta da quantidades de respostas");
                                    $this->valarvore[$idp][$lresp['idresposta']][$keps] = $esvalor['result']."#".round(($esvalor['result'] / $estvalor['result']) * 100,1)."#".$estvalor['result'];
                                }
                            }
                        }
                    }
                }
                
                foreach($this->arvore as $idg => $sub) {
                        foreach($sub as $pergs) {
                            foreach($pergs as $idp => $np) {
                                // apontamentos por pergunta
                                $pergapont = "SELECT COUNT(*) as result FROM moniavalia ".$this->aliasrel[moniavalia]."
                                                INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                                INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idpergunta='$idp' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','FG','FGM')";
                                $eapont = $_SESSION['fetch_array']($_SESSION['query']($pergapont));
                                if($eapont['result'] >= 1) {
                                    $this->peapont['pergunta'][$idp] = $eapont['result'];
                                }
                                $this->peapont['totalmo'] = $echeck['qtde'];
                                asort($operador);
                                foreach($operador as $idope => $noper) {
                                    $peapontop = "SELECT COUNT(*) as result FROM moniavalia ".$this->aliasrel[moniavalia]."
                                                    INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                                    INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta = ".$this->aliasrel[moniavalia].".idresposta
                                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                    WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idpergunta='$idp' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','FG','FGM') AND ".$this->aliasrel[monitoria].".idoperador=$idope";
                                    $eapontop = $_SESSION['fetch_array']($_SESSION['query']($peapontop));
                                    if($eapontop['result'] >= 1) {
                                        $this->peapont['operador'][$noper][$idp] = $eapontop['result'];                                                              
                                    }
                                }
                            }
                        }
                }
                
                foreach($this->peapont['operador'] as $oper => $dados) {
                    arsort($dados);
                }
            }

            // interferencia por grupo
            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
            }
            else {
                foreach($this->cons as $ideps => $ieps) {
                    foreach($this->arvore as $idgrupo => $perg) {
                        $sgrupo = "SELECT fg, COUNT(*) as result FROM grupo WHERE idgrupo='$idgrupo' AND tab='N'";
                        $esgrupo = $_SESSION['fetch_array']($_SESSION['query']($sgrupo)) or die ("erro na query de consulta do grupo de ncg");
                        if($esgrupo['fg'] == "S" OR $esgrupo['result'] == "0") {
                        }
                        else {
                            $somag = 0;
                            $soma = 0;
                            $i = 0;
                            $selper = "SELECT (".$this->aliasrel[moniavalia].".valor_grupo - ".$this->aliasrel[moniavalia].".valor_final_grup) as result,".$this->aliasrel[moniavalia].".idmonitoria  FROM moniavalia ".$this->aliasrel[moniavalia]."
                                        INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                        INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='$idgrupo' AND ".$this->aliasrel[moniavalia].".avalia IN ('AVALIACAO','PERC','AVALIACAO_OK') AND $this->sqlcons'$ieps' GROUP BY ".$this->aliasrel[monitoria].".idmonitoria";
                            $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta dos valores por pergunta");
                            while($lselper = $_SESSION['fetch_array']($eselper)) {
                                $selfg = "SELECT COUNT(*) as result FROM moniavalia WHERE idmonitoria='".$lselper['idmonitoria']."' AND idgrupo='$idgrupo' AND avalia='FG'"; // novo conceito, para grupos que possuem algum item de FG
                                $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selfg)) or die (mysql_error());
                                if($eselfg['result'] >= 1) {  
                                }
                                else {
                                    $somag = $somag + $lselper['result'];
                                }
                            }
                            $selvalor = "SELECT ".$this->aliasrel[moniavalia].".valor_final_aval FROM moniavalia ".$this->aliasrel[moniavalia]."
                                        INNER JOIN monitoria ".$this->aliasrel[monitoria]." ON ".$this->aliasrel[monitoria].".idmonitoria=".$this->aliasrel[moniavalia].".idmonitoria
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idgrupo='$idgrupo' AND $this->sqlcons'$ieps' GROUP BY ".$this->aliasrel[monitoria].".idmonitoria";
                            $eselvalor = $_SESSION['query']($selvalor) or die ("erro na query de consulta dos valores acumulados");
                            while($lselvalor = $_SESSION['fetch_array']($eselvalor)) {
                                $i++;
                                $soma = $soma + $lselvalor['valor_final_aval'];
                            }
                            $this->interferencia[$ideps][$idgrupo] = round((($soma + $somag) / $i),2);
                        }
                    }
                }
            }

            // dados grafico NCG
            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
            }
            else {
                foreach($this->cons as $idepsncg => $nepsncg) {
                    $selmoni = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt AND $this->sqlcons'$nepsncg'";
                    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta do total de monitoria");
                    $selncg = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtdencg FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt AND $this->sqlcons'$nepsncg' AND qtdefg >= 1";
                    $eselncg = $_SESSION['fetch_array']($_SESSION['query']($selncg)) or die ("erro na query de consulta da quantidade de NCG");
                    $this->ncg['qtdencg'][] = $eselncg['qtdencg'];
                    $this->ncg['qtde'][] = $eselmoni['qtde'];
                    $this->ncg['perc'][] = round(($eselncg['qtdencg'] / $eselmoni['qtde']) * 100,1);
                }
            }

            //duracao das chamadas
            if($this->tipo == "mensal") {
                $duratt = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE $wheresql $this->wherectt";
                $eduratt = $_SESSION['fetch_array']($_SESSION['query']($duratt)) or die (mysql_error());
                $this->tempos = array('até 5min' => '00:00:00,00:05:00','5min a 7,5min' => '00:05:01,00:07:30','7,5min a 10min' => '00:07:31,00:10:00','10min a 12,5min' => '00:10:00,00:12:30','12,5min a 15min' => '00:12:31,00:15:00','15min a 17,5min' => '00:15:01,00:17:30','maior 17,5min' => '00:17:31,99:59:59');
                foreach($this->tempos as $tglobal) {
                    $tmp = explode(",",$tglobal);
                    $dura = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE $wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".tmpaudio BETWEEN '$tmp[0]' AND '$tmp[1]'";
                    $edura = $_SESSION['fetch_array']($_SESSION['query']($dura)) or die ("erro na query de consulta da duração das ligações");
                    $this->duracao['GLOBAL'][] = round(($edura['qtde'] / $eduratt['qtde']) * 100,1);
                }

                if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
                    foreach($this->fmdados as $kduraglob => $duraglob) {
                        foreach($duraglob as $kfduraglob => $fduraglob) {
                            $sqlcons = "";
                            $colsf = array();
                            $chave = explode("#",$kfduraglob);
                            $valchave = explode("#",$fduraglob);
                            if(count($chave) == 1) {
                                $col = explode("#",$kduraglob);
                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($col[0])."='$fduraglob'";
                                //$ideps = $fd;
                            }
                            else {
                                foreach($chave as $kchave => $vchave) {
                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                    $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                        $colsf[] = $lselcol['nomefiltro_nomes'];
                                    }
                                }
                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                            }
                            $ideps = $fduraglob;
                            $durattcons = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND $sqlcons";
                            $edurattcons = $_SESSION['fetch_array']($_SESSION['query']($durattcons)) or die ("erro na query de consulta da quantidade de NCG");
                            foreach($this->tempos as $tcons) {
                                $tmpcons = explode(",",$tcons);
                                $duracons = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND $sqlcons AND ".$this->aliasrel[monitoria].".tmpaudio BETWEEN '$tmpcons[0]' AND '$tmpcons[1]'";
                                $eduracons = $_SESSION['fetch_array']($_SESSION['query']($duracons)) or die ("erro na query de consulta da duração das ligações");
                                $this->duracao[$kfduraglob][] = round(($eduracons['qtde'] / $edurattcons['qtde']) * 100,1);
                            }
                        }
                    }
                }
                else {
                    foreach($this->cons as $idconsdura => $consdura) {
                        $durattcons = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $wheresql $this->wherectt AND $this->sqlcons'$consdura'";
                        $edurattcons = $_SESSION['fetch_array']($_SESSION['query']($durattcons)) or die ("erro na query de consulta da quantidade de NCG");
                        foreach($this->tempos as $tcons) {
                            $tmpcons = explode(",",$tcons);
                            $duracons = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $wheresql $this->wherectt AND $this->sqlcons'$consdura' AND ".$this->aliasrel[monitoria].".tmpaudio BETWEEN '$tmpcons[0]' AND '$tmpcons[1]'";
                            $eduracons = $_SESSION['fetch_array']($_SESSION['query']($duracons)) or die ("erro na query de consulta da duração das ligações");
                            $this->duracao[$idconsdura][] = round(($eduracons['qtde'] / $edurattcons['qtde']) * 100,1);
                        }
                    }
                }
            }

                //ngc pareto
                if($this->vars['idplanilha'] != "") {
                    $ncgplan = "select g.filtro_vinc, pe.descripergunta as descripe, pe.idresposta as idpe, ps.descriresposta as descrips, ps.idresposta as idps,
                                pe.ativo as ativope, ps.ativo as ativops from planilha p
                                inner join grupo g on g.idgrupo = p.idgrupo
                                left join pergunta pe on pe.idpergunta = g.idrel
                                left join subgrupo s on s.idsubgrupo = g.idrel
                                left join pergunta ps on ps.idpergunta = s.idpergunta 
                                where idplanilha='".$this->vars['idplanilha']."' and pe.avalia='FG'group by pe.idresposta, ps.idresposta";
                    $encgplan = $_SESSION['query']($ncgplan) or die (mysql_error());
                    while($lncgplan = $_SESSION['fetch_array']($encgplan)) {
                        if($lncgplan['filtro_vinc'] == "P" && $lncgplan['ativope']) {
                            $pergsncg[$lncgplan['descripe']] = $lncgplan['idpe'];
                        }
                        if($lncgplan['filtro_vinc'] == "S" && $lncgplan['ativops']) {
                            $pergsncg[$lncgplan['descrips']] = $lncgplan['idps'];
                        }
                    }
                    foreach($variantes as $nparetoncg => $idpncg) {
                        $sqlcons = "";
                        $colsf = array();
                        $chave = explode("#",$nparetoncg);
                        $valchave = explode("#",$idpncg);
                        if(count($chave) == 1) {
                            if($nparetoncg == "GLOBAL") {
                            }
                            else {
                                $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                    INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$nparetoncg'";
                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta do nome do filtro");
                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($eselcol['nomefiltro_nomes'])."='$idpncg'";
                            }
                            //$ideps = $fd;
                        }
                        else {
                            foreach($chave as $kchave => $vchave) {
                                $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                        INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                    $colsf[] = $lselcol['nomefiltro_nomes'];
                                }
                            }
                            $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                        }
                        $perctt = array();
                        $qtdencgs = array();
                        $acuncg = 0;
                        $ttncg = 0;
                        $ttdem = 0;
                        if($nparetoncg == "GLOBAL") {
                        }
                        else {
                            $seldado = "SELECT fn.nomefiltro_nomes FROM filtro_dados fd
                                        INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes
                                        WHERE idfiltro_dados='$idpncg'";
                            $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldado)) or die ("erro na query de consulta do código do filtro");
                        }
                        $this->sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($eseldados['nomefiltro_nomes'])."=";
                        $ideps = $idpncg;
                        if($nparetoncg == "GLOBAL") {
                            $wherencg = "";
                        }
                        else {
                            $wherencg = "AND $this->sqlcons'$ideps'";
                        }

                        $ncgtt = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".qtdefg >= 1
                                    AND ".$this->aliasrel[moniavalia].".avalia IN ('FG') $wherencg";
                        $encgtt = $_SESSION['fetch_array']($_SESSION['query']($ncgtt)) or die ("erro na query de consulta do total das ncg");
                        foreach($pergsncg as $npergncg => $idpergncg) {
                            $ncgglob = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND ".$this->aliasrel[moniavalia].".idresposta='$idpergncg' 
                                        AND ".$this->aliasrel[monitoria].".qtdefg >= 1 AND ".$this->aliasrel[moniavalia].".avalia IN ('FG') $wherencg";
                            $encgglob = $_SESSION['fetch_array']($_SESSION['query']($ncgglob)) or die ("erro na consulta do total da NCG Global");
                            //$perc$tt[$npergncg] = round(($encgglob['qtde'] / $encgtt['qtde']) * 100,1);
                            $qtdencgs[$npergncg] = $encgglob['qtde'];
                        }
                        $q = 0;
                        arsort($qtdencgs);
                        foreach($qtdencgs as $nqtde => $qtdencg) {
                            $q++;
                            if($q > 6) {
                                $ttncg = $ttncg + $qtdencg;
                                $ttdem = $ttdem + $qtdencg;
                            }
                            else {
                                $perctt[$nqtde] = $qtdencg;
                                $ttncg = $ttncg + $qtdencg;
                            }
                        }
                        arsort($perctt);
                        $perctt['Demais'] = $ttdem;
                        foreach($perctt as $ncg => $percg) {
                            $percent = round(($percg / $ttncg) * 100,1);
                            $acuncg = round(($acuncg + $percent),1);
                            if($acuncg == "99.8" OR $acuncg == "99.9" OR $acuncg == "100.1" OR $acuncg == "100.2") {
                                $acuncg = round($acuncg,0);
                            }
                            else {
                            }
                            $this->paretoncg[$nparetoncg]['perc'][$ncg] = $percent;
                            $this->paretoncg[$nparetoncg]['percacu'][$ncg] = $acuncg;
                        }
                    }

                }
                else {
                }   

                // distribuição de notas
                if($this->tipo == "mensal") {
                    $selfaixas = "SELECT pm.idindice FROM param_moni pm
                                INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                                INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros
                                WHERE rf.idrel_filtros IN (".$this->vars['idrel_filtros'].") GROUP BY pm.idindice";
                    $eselfaixas = $_SESSION['query']($selfaixas) or die ("erro na query de consulta dos intervalos");
                    while($lselfaixas = $_SESSION['fetch_array']($eselfaixas)) {
                        $faixas[] = $lselfaixas['idindice'];
                    }
                    $notastt = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt";
                     $enotastt = $_SESSION['fetch_array']($_SESSION['query']($notastt)) or die ("erro na query de consulta da quantidade total de monitorias"); 
                    foreach($faixas as $fx) {
                        $selinter = "SELECT * FROM intervalo_notas WHERE idindice='$fx' ORDER BY numini";
                        $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta do intervalo");
                        while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                            //global
                            $notatt = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND valor_fg BETWEEN '".$lselinter['numini']."' AND '".$lselinter['numfim']."'";
                            $enotatt = $_SESSION['fetch_array']($_SESSION['query']($notatt)) or die ("erro na consulta da quantidade de monitorias por intervalor");
                            $this->intervalo['GLOBAL'][str_replace(".00","",$lselinter['numini'])." a ".str_replace(".00","",$lselinter['numfim'])] = round(($enotatt['qtde'] / $enotastt['qtde']) * 100,1);

                            //por filtro
                            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
                                foreach($this->fmdados as $knotaglob => $notaglob) {
                                        foreach($notaglob as $kfdnotaglob => $fdnotaglob) {
                                            $sqlcons = "";
                                            $colsf = array();
                                            $chave = explode("#",$kfdnotaglob);
                                            $valchave = explode("#",$fdnotaglob);
                                            if(count($chave) == 1) {
                                                $col = explode("#",$knotaglob);
                                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($col[0])."='$fdnotaglob'";
                                                //$ideps = $fd;
                                            }
                                            else {
                                                foreach($chave as $kchave => $vchave) {
                                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                                    $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                                    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                                        $colsf[] = $lselcol['nomefiltro_nomes'];
                                                    }
                                                }
                                                $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                                            }
                                            $notacons = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND valor_fg BETWEEN '".$lselinter['numini']."' AND '".$lselinter['numfim']."'AND $sqlcons";
                                            $enotacons = $_SESSION['fetch_array']($_SESSION['query']($notacons)) or die ("erro na query de consulta da quatidade de monitorias por intervalor filtros");
                                            $this->intervalo[$kfdnotaglob][str_replace(".00","",$lselinter['numini'])." a ".str_replace(".00","",$lselinter['numfim'])] = round(($enotacons['qtde'] / $this->qtde[$kfdnotaglob]['comfg']['qtde']) * 100,1);
                                        }
                                }
                            }
                            else {
                                foreach($this->cons as $consnota => $idconsnota) {
                                    $notacons = "SELECT COUNT(DISTINCT(".$this->aliasrel[moniavalia].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND valor_fg BETWEEN '".$lselinter['numini']."' AND '".$lselinter['numfim']."'AND $this->sqlcons'$idconsnota'";
                                    $enotacons = $_SESSION['fetch_array']($_SESSION['query']($notacons)) or die ("erro na query de consulta da quatidade de monitorias por intervalor filtros");
                                    $this->intervalo[$consnota][str_replace(".00","",$lselinter['numini'])." a ".str_replace(".00","",$lselinter['numfim'])] = round(($enotacons['qtde'] / $this->qtde[$consnota]['comfg']['qtde']) * 100,1);
                                }
                            }
                        }
                    }
                }
                else {
                }

                // evolutivo NCG
                if($this->tipo == "mensal") {
                    foreach($this->semanas as $sncg) {
                        $newhere = array();
                        $datasem = explode("#",$sncg);
                        $sem = explode(",",$datasem[0]);
                        $semctt = explode(",",$datasem[1]);
                        $wheredt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$semctt[0]' AND '$semctt[1]'";
                        foreach($where as $ch => $w) {
                            if($ch == "data") {
                                $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$sem[0]' AND '$sem[1]'";
                            }
                            else {
                                $newhere[] = $w;
                            }
                        }
                        if($this->tipo == "mensal") {
                            $cat = $this->meses[substr($semctt[1],5,2)]."/".substr($semctt[1],0,4);
                        }
                        else {
                            $cat = $dtncg[0]." a ".$dtncg[1];
                        }
                        foreach($variantes as $varnsg => $idvarncg) {
                            $sqlcons = "";
                            $colsf = array();
                            $chave = explode("#",$varnsg);
                            $valchave = explode("#",$idvarncg);
                            if(count($chave) == 1) {
                                if($varnsg == "GLOBAL") {
                                }
                                else {
                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$varnsg'";
                                    $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta do nome do filtro");
                                    $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($eselcol['nomefiltro_nomes'])."='$idvarncg'";
                                }
                                //$ideps = $fd;
                            }
                            else {
                                foreach($chave as $kchave => $vchave) {
                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                    $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                        $colsf[] = $lselcol['nomefiltro_nomes'];
                                    }
                                }
                                $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                            }
                            if($this->tipo == "mensal" && $this->vars['idplanilha'] == "") {
                                if($varnsg == "GLOBAL") {
                                }
                                else {
                                    $seldado = "SELECT fn.nomefiltro_nomes FROM filtro_dados fd
                                                INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes
                                                WHERE idfiltro_dados='$idvarncg'";
                                    $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldado)) or die ("erro na query de consulta do código do filtro");
                                    $this->sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($eseldados['nomefiltro_nomes'])."=";
                                }
                            }
                            else {
                            }
                            if($varnsg == "GLOBAL") {
                                $wherevar = "";
                            }
                            else {
                                $wherevar = $sqlcons;
                            }
                            $consncg = array('qtde' => "",'qtdencg' => " AND ".$this->aliasrel[monitoria].".qtdefg >= 1");
                            foreach($consncg as $tpdado => $vardados) {
                                $paramfg = $vardados;
                                $selncgs = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE ".implode(" AND ",$newhere)." $paramfg $wherevar $wheredt";
                                $eselncgs = $_SESSION['fetch_array']($_SESSION['query']($selncgs)) or die ("erro na query de consulta da quantidade de NCG");
                                if($tpdado == "qtde") {
                                    $qtdett = $eselncgs['qtde'];
                                }
                                else {
                                    $qtdencg = $eselncgs['qtde'];
                                }
                            }
                            if($qtdett == 0) {
                            }
                            else {
                                $this->evolutivoncg[$varnsg][$cat] = round(($qtdencg / $qtdett) * 100,1);
                                $qtdett = 0;
                                $qtdencg = 0;
                            }
                        }
                    }
                }
                else {
                }

                // evolutivo incidencia
                if($this->tipo == "mensal") {
                    foreach($this->semanas as $seminci) {
                        $newhere = array();
                        $datasem = explode("#",$seminci);
                        $sem = explode(",",$datasem[0]);
                        $semctt = explode(",",$datasem[1]);
                        $wheredt = "AND ".$this->aliasrel[monitoria].".datactt BETWEEN '$semctt[0]' AND '$semctt[1]'";
                        foreach($where as $ch => $w) {
                            if($ch == "data") {
                                $newhere[] = $this->aliasrel[monitoria].".data BETWEEN '$sem[0]' AND '$sem[1]'";
                            }
                            else {
                                $newhere[] = $w;
                            }
                        }                   
                        
                        foreach($variantes as $ninci => $idinci) {
                            $sqlcons = "";
                            $colsf = array();
                            $chave = explode("#",$ninci);
                            $valchave = explode("#",$idinci);
                            if(count($chave) == 1) {
                                if($ninci == "GLOBAL") {
                                }
                                else {
                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$ninci'";
                                    $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta do nome do filtro");
                                    $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($eselcol['nomefiltro_nomes'])."='$idinci'";
                                }
                                //$ideps = $fd;
                            }
                            else {
                                foreach($chave as $kchave => $vchave) {
                                    $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                    $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                        $colsf[] = $lselcol['nomefiltro_nomes'];
                                    }
                                }
                                $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                            }
                            if($ninci == "GLOBAL") {
                            }
                            else {
                                $seldado = "SELECT fn.nomefiltro_nomes FROM filtro_dados fd
                                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes
                                            WHERE idfiltro_dados='$idinci'";
                                $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldado)) or die ("erro na query de consulta do código do filtro");
                            }
                            if($ninci == "GLOBAL") {
                                $whereinci = "";
                            }
                            else {
                                $whereinci = $sqlcons;
                            }
                            $monincitt = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE ".implode(" AND ",$newhere)." $wheredt $whereinci";
                            $emonincitt = $_SESSION['fetch_array']($_SESSION['query']($monincitt)) or die ("erro na query de consulta da quantidade de monitorias");
                            $moniinci = "SELECT COUNT(".$this->aliasrel[moniavalia].".idresposta) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN pergunta ".$this->aliasrel[pergunta]." ON ".$this->aliasrel[pergunta].".idresposta=".$this->aliasrel[moniavalia].".idresposta
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE ".implode(" AND ",$newhere)." $wheredt $whereinci AND ".$this->aliasrel[pergunta].".avalia IN ('AVALIACAO','FG','FGM','PERC')";
                            $emoniinci = $_SESSION['fetch_array']($_SESSION['query']($moniinci)) or die ("erro na query de consulta das incidencias de sinalizações");
                            if($emonincitt['qtde'] == 0) {
                            }
                            else {
                                $this->incidencia[$ninci][$this->meses[substr($semctt[1],5,2)]."/".substr($semctt[1],0,4)] = round(($emoniinci['qtde'] / $emonincitt['qtde']),1);
                            }
                        }
                    }
                }

                // intervalo de confiança
                $cv = 0;
                foreach($variantes as $vardesvio => $iddesvio) {
                    $sqlcons = "";
                    $colsf = array();
                    $chave = explode("#",$vardesvio);
                    $valchave = explode("#",$iddesvio);
                    if(count($chave) == 1) {
                        if($vardesvio == "GLOBAL") {
                        }
                        else {
                            $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                    INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vardesvio'";
                            $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta do nome do filtro");
                            $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($eselcol['nomefiltro_nomes'])."='$iddesvio'";
                        }
                        //$ideps = $fd;
                    }
                    else {
                        foreach($chave as $kchave => $vchave) {
                            $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                    INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                            while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                $colsf[] = $lselcol['nomefiltro_nomes'];
                            }
                        }
                        $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                    }
                    $desvio = 0;
                    if($vardesvio == "GLOBAL") {
                    }
                    else {
                        $wheredesvio = $sqlcons;
                        if($this->qtde[$vardesvio]['semfg']['media_semfg'] == "") {
                            $this->interconfianca[$vardesvio]['intervalo máximo'] = 0;
                            $this->interconfianca[$vardesvio]['media'] = 0;
                            $this->interconfianca[$vardesvio]['intervalo mínimo'] = 0;
                        }
                        else {
                            if(eregi("JV",$vardesvio) OR $everifdt['result'] >= 1) {
                                $calcinter = "valor_fg";
                                $sqlinter = "";
                                $notamedia = $this->qtde[$vardesvio]['comfg']['media_fg'];
                            }
                            else {
                                $calcinter = "valor_final_aval";
                                $sqlinter = "AND qtdefg=0";
                                $notamedia = $this->qtde[$vardesvio]['semfg']['media_semfg'];
                            }
                            $monidesvio = "SELECT ($calcinter - $notamedia) as desvio FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt $wheredesvio $sqlinter GROUP BY ".$this->aliasrel[monitoria].".idmonitoria";
                            $edesvio = $_SESSION['query']($monidesvio) or die (mysql_error());
                            $ndesvio = $_SESSION['num_rows']($edesvio);
                            if($ndesvio == 0) {
                                $this->interconfianca[$vardesvio]['intervalo máximo'] = 0;
                                $this->interconfianca[$vardesvio]['media'] = 0;
                                $this->interconfianca[$vardesvio]['intervalo mínimo'] = 0;
                            }
                            else {
                                while($ldesvio = $_SESSION['fetch_array']($edesvio)) {
                                    $desvio = $desvio + pow($ldesvio['desvio'],2);
                                }
                                if(eregi("JV",$vardesvio) OR $everifdt['result'] >= 1) {
                                    $dp = sqrt(($desvio / $this->qtde[$vardesvio]['comfg']['qtde']));
                                    $this->interconfianca[$vardesvio]['intervalo máximo'] = round($this->qtde[$vardesvio]['comfg']['media_fg'] + (1.96 * ($dp / sqrt($this->qtde[$vardesvio]['comfg']['qtde'] - 1))),1);
                                    $this->interconfianca[$vardesvio]['media'] = round($this->qtde[$vardesvio]['comfg']['media_fg'],1);
                                    $this->interconfianca[$vardesvio]['intervalo mínimo'] = round($this->qtde[$vardesvio]['comfg']['media_fg'] - (1.96 * ($dp / sqrt($this->qtde[$vardesvio]['comfg']['qtde'] - 1))),1);
                                }
                                else {
                                    $dp = sqrt(($desvio / $this->qtde[$vardesvio]['semfg']['qtde']));
                                    $this->interconfianca[$vardesvio]['intervalo máximo'] = round($this->qtde[$vardesvio]['semfg']['media_semfg'] + (1.96 * ($dp / sqrt($this->qtde[$vardesvio]['semfg']['qtde'] - 1))),1);
                                    $this->interconfianca[$vardesvio]['media'] = round($this->qtde[$vardesvio]['semfg']['media_semfg'],1);
                                    $this->interconfianca[$vardesvio]['intervalo mínimo'] = round($this->qtde[$vardesvio]['semfg']['media_semfg'] - (1.96 * ($dp / sqrt($this->qtde[$vardesvio]['semfg']['qtde'] - 1))),1);
                                }
                            }
                        }
                    }
                }

                // erro amostral
                if($this->tipo == "mensal") {
                    $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel LIMIT 1";
                    $eselfiltros = $_SESSION['fetch_array']($_SESSION['query']($selfiltros)) or die ("erro na query de consulta dos filtros");
                    $filtrosam[$eselfiltros['idfiltro_nomes']] = $eselfiltros['nomefiltro_nomes'];
                    $selcod = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".key($filtrosam)."'";
                    $eselcod = $_SESSION['query']($selcod) or die ("erro na query de consulta dos dados do filtro");
                    while($lselcod = $_SESSION['fetch_array']($eselcod)) {
                        $varfiltros[$lselcod['nomefiltro_dados']] = $lselcod['idfiltro_dados'];
                    }
                    foreach($varfiltros as $nam => $id) {
                        $wheream = "AND ".$this->aliasrel[rel_filtros].".id_".strtolower(current($filtrosam))."='".$id."'";
                        $selmoni = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt $wheream";
                        $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da quantidade de monitorias da amostra");
                        if($eselmoni['qtde'] == 0) {
                        }
                        else {
                            $this->erroamostra[$eselfiltros['nomefiltro_nomes']][$nam]['qtde'] = $eselmoni['qtde'];
                            $this->erroamostra[$eselfiltros['nomefiltro_nomes']][$nam]['erro'] = round(1.96*(sqrt(0.25/$eselmoni['qtde'])) * 100,1);
                        }
                    }
                }
                
                foreach($this->fmdados as $vnivel => $operacoes) {
                    foreach($operacoes as $nomeoper => $idoper) {
                        $sqlcons = "";
                        $colsf = array();
                        $chave = explode("#",$nomeoper);
                        $valchave = explode("#",$idoper);
                        if(count($chave) == 1) {
                            if($varam == "GLOBAL") {
                            }
                            else {
                                $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                        INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$nomeoper'";
                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta do nome do filtro");
                                $colsf[] = $eselcol['nomefiltro_nomes'];
                                $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($eselcol['nomefiltro_nomes'])."='$idoper'";
                            }
                            //$ideps = $fd;
                        }
                        else {
                            foreach($chave as $kchave => $vchave) {
                                $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                        INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                    $colsf[] = $lselcol['nomefiltro_nomes'];
                                }
                            }
                            $sqlcons = " AND ".$this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                        }
                        if($varam == "GLOBAL") {
                        }
                        else {
                            $wheream = "AND ".$this->aliasrel[rel_filtros].".id_".strtolower($nivelam)."='".$idfiltroam."'";
                            $selmoni = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt $sqlcons";
                            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_error());
                            if($eselmoni['qtde'] == 0) {
                            }
                            else {
                                $this->erroamostra[$vnivel][$nomeoper]['qtde'] = $eselmoni['qtde'];
                                $this->erroamostra[$vnivel][$nomeoper]['erro'] = round(1.96*(sqrt(0.25/$eselmoni['qtde'])) * 100,1); 
                            }
                        }
                    }
                }
            }
            
            //relatório consolidado do fluxo de contestação
            if($this->tipo == "exefluxo") {
                $alias = new TabelasSql();
                $idsrel = arvoreescalar('', '');
                $selsql = "SELECT ".$this->aliasrel[monitoria].".idrel_filtros, COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                            WHERE $this->wheresql $this->wherectt GROUP BY idrel_filtros";
                $eselsql = $_SESSION['query']($selsql) or die (mysql_error());
                while($lselsql = $_SESSION['fetch_array']($eselsql)) {
                    $relwhere[] = $lselsql['idrel_filtros'];
                }
                array_unique($relwhere);
                foreach($idsrel as $rel) {
                    if(in_array($rel, $relwhere)) {
                        $newrels[] = $rel;
                        $selfluxo = "SELECT idfluxo FROM conf_rel WHERE idrel_filtros='$rel'";
                        $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo));
                        $fluxos[$eselfluxo['idfluxo']][] = $rel;
                    }
                }
                foreach($fluxos as $fl => $opfluxo) {
                    $this->dados .= "<div style=\"float:left; font-size:12px;\">";
                        $this->dados .= "<table width=\"100%\" cellspacing=\"0\" style=\"border-collapse: collapse; border: 1xp solid #CCC\" class=\"tabfluxo\">";
                        $this->dados .= "<tr bgcolor=\"#EAD6A8\">";
                            $this->dados .= "<td style=\"font-weight: bold;padding: 5px; width: 200px\">USUÁRIO</td>";
                            $this->dados .= "<td style=\"font-weight: bold;padding: 5px; width: 200px\">PERFIL</td>";
                            $this->dados .= "<td style=\"font-weight: bold;padding: 5px\">CARTEIRA</td>";
                            $this->dados .= "<td style=\"font-weight: bold; text-align: center;padding: 5px\">QTDE.</td>";
                            $this->dados .= "<td style=\"font-weight: bold; text-align: center;padding: 5px\">QTDE. FG</td>";
                            $selstatus = "SELECT s.idstatus,nomestatus FROM rel_fluxo rf"
                                        . " INNER JOIN status s ON s.idstatus = rf.idstatus"
                                        . " INNER JOIN definicao d ON d.iddefinicao = rf.iddefinicao"
                                        . " WHERE rf.idfluxo='$fl' GROUP BY s.idstatus ORDER BY fase";
                            $eselstatus = $_SESSION['query']($selstatus) or die (mysql_error());
                            while($lstatus = $_SESSION['fetch_array']($eselstatus)) {
                                $relfluxo = "SELECT idrel_fluxo,rf.idstatus,rf.iddefinicao,nomedefinicao,nomestatus FROM rel_fluxo rf"
                                        . " INNER JOIN status s ON s.idstatus = rf.idstatus"
                                        . " INNER JOIN definicao d ON d.iddefinicao = rf.iddefinicao"
                                        . " WHERE rf.idfluxo='$fl' AND rf.idstatus='".$lstatus['idstatus']."' ORDER BY fase";
                                $erelfluxo = $_SESSION['query']($relfluxo) or die (mysql_error());
                                while($lfluxo = $_SESSION['fetch_array']($erelfluxo)) {
                                    $fluxo[$fl][$lstatus['idstatus']][] = $lfluxo['iddefinicao'];
                                    $this->dados .= "<td style=\"text-align: center; padding: 5px\">".$lfluxo['nomedefinicao']."</td>";
                                }
                                $this->dados .= "<td style=\"text-align: center; font-weight: bold\">TOTAL - ".$lstatus['nomestatus']."</td>";
                            }
                        $this->dados .= "</tr>";
                    $selusers = "SELECT * FROM user_web uw"
                            . " INNER JOIN perfil_web pw ON pw.idperfil_web = uw.idperfil_web"
                            . " WHERE uw.ativo='S' ORDER BY nomeuser_web";
                    $eselusers = $_SESSION['query']($selusers) or die (mysql_error());
                    while($lselusers = $_SESSION['fetch_array']($eselusers)) {
                        $opuser = array();
                        $qtderel = array();
                        $l = 0;
                        $selfilt = "SELECT uf.idrel_filtros FROM userwebfiltro uf"
                                . " INNER JOIN rel_filtros rf ON rf.idrel_filtros = uf.idrel_filtros "
                                . " INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros WHERE iduser_web='".$lselusers['iduser_web']."' AND cr.ativo='S'";
                        $eselfilt = $_SESSION['query']($selfilt) or die (mysql_error());
                        while($lselfilt = $_SESSION['fetch_array']($eselfilt)) {
                            $selcont = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $this->wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".idrel_filtros='".$lselfilt['idrel_filtros']."'";
                            $eselcont = $_SESSION['fetch_array']($_SESSION['query']($selcont)) or die ("erro na consulta das monitorias do relacionamento");
                            if($eselcont['qtde'] >= 1) {
                                $qtderel[$lselfilt['idrel_filtros']] = $eselcont['qtde'];
                            }
                            $opuser[] = $lselfilt['idrel_filtros'];
                        }
                        $operacoes = array_intersect($newrels,$opuser);
                            
                        foreach($operacoes as $oprel) {
                            if($qtderel[$oprel] >= 1) {
                                $l++;
                                $this->dados .= "<tr>";
                                $this->dados .= "<td style=\"border: 1px solid #CCC\">".$lselusers['nomeuser_web']."</td>";
                                $this->dados .= "<td style=\"border: 1px solid #CCC\">".$lselusers['nomeperfil_web']."</td>";
                                $this->dados .= "<td style=\"padding: 5px\">".nomevisu($oprel)."</td>";
                                    $qtde = $qtderel[$oprel];
                                    $selcont = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as qtde FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $this->wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".idrel_filtros='$oprel' AND ".$this->aliasrel[monitoria].".qtdefg >= 1";
                                    $eselcont = $_SESSION['fetch_array']($_SESSION['query']($selcont)) or die ("erro na consulta das monitorias do relacionamento");
                                $this->dados .= "<td style=\"text-align: center; padding: 5px\">$qtde</td>";
                                $this->dados .= "<td style=\"text-align: center;padding: 5px\">".$eselcont['qtde']."</td>";
                                foreach($fluxo[$fl] as $rf => $definicao) {
                                    $qtdest = 0;
                                    foreach($definicao as $df) {
                                        $checkdef = "SELECT * FROM definicao WHERE iddefinicao='$df'";
                                        $echeckdef = $_SESSION['fetch_array']($_SESSION['query']($checkdef)) or die (mysql_error());
                                        if($echeckdef['tipodefinicao'] == "automatico") {
                                            $moniflu = "SELECT count(*) as r FROM monitoria_fluxo WHERE idmonitoria IN (SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria) FROM monitoria ".$this->aliasrel[monitoria]."
                                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                        WHERE $this->wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".idrel_filtros='$oprel') AND idstatus='$rf' AND iddefinicao='$df'";
                                        }
                                        else {
                                            $moniflu = "SELECT count(*) as r FROM monitoria_fluxo WHERE idmonitoria IN (SELECT DISTINCT(".$this->aliasrel[monitoria].".idmonitoria) FROM monitoria ".$this->aliasrel[monitoria]."
                                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                        WHERE $this->wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".idrel_filtros='$oprel') AND idstatus='$rf' AND iddefinicao='$df' AND iduser='".$lselusers['iduser_web']."' AND tabuser='user_web'";
                                        }
                                        $emoniflux = $_SESSION['fetch_array']($_SESSION['query']($moniflu)) or die (mysql_error());
                                        $qtdest = $qtdest + $emoniflux['r'];
                                        $this->dados .= "<td style=\"text-align: center;\">".$emoniflux['r']."</td>";
                                    }
                                    $this->dados .= "<td style=\"text-align: center;\">$qtdest</td>";
                                }
                                $this->dados .= "</tr>";
                            }
                        }
                    }
                    $this->dados .= "</table>";
                    $this->dados .= "</div>";
                }
            }
                
                // relatório analitico das monitorias contestadas
            if($this->tipo == "contesta") {
                $this->dados .= "<table>";    
                    $this->dados .= "<tr bgcolor=\"#CCCCCC\">";
                        $this->dados .= "<td align=\"center\"><strong>ID</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>Data</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>Data Ctt</td>";
                        $this->dados .= "<td align=\"center\"><strong>Operador</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>CNPJ_CPF</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>Operacao</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>Data</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>Definicao</strong></td>";
                        $this->dados .= "<td align=\"center\"><strong>OBS</strong></td>";
                    $this->dados .= "</tr>";
                $selmoni = "SELECT ".$this->aliasrel[monitoria].".idrel_filtros,".$this->aliasrel[monitoria].".idmonitoria,".$this->aliasrel[monitoria].".data as dtmoni,".$this->aliasrel[monitoria].".datactt,".$this->aliasrel[operador].".operador,".$this->aliasrel[monitoria].".obs as obsmoni,".$this->aliasrel[fila_grava].".ra_cnpj_cpf_idcontrato
                                    FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN fila_grava ".$this->aliasrel[fila_grava]." ON ".$this->aliasrel[fila_grava].".idfila_grava = ".$this->aliasrel[monitoria].".idfila
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $this->wheresql $this->wherectt AND ".$this->aliasrel[monitoria_fluxo].".iddefinicao='3' GROUP BY ".$this->aliasrel[monitoria].".idmonitoria ORDER BY ".$this->aliasrel[monitoria].".idmonitoria";
                $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta das monitorias");
                while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                    $selfluxo = "SELECT s.nomestatus,d.nomedefinicao,mf.obs,mf.data FROM monitoria_fluxo mf
                                        INNER JOIN status s ON s.idstatus = mf.idstatus
                                        INNER JOIN definicao d ON d.iddefinicao = mf.iddefinicao
                                        WHERE idmonitoria='".$lselmoni['idmonitoria']."' ORDER BY idmonitoria_fluxo";
                    $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta do fluxo");
                    while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                        $this->dados .= "<tr bgcolor=\"#EAC798\">";
                            $this->dados .= "<td align=\"center\">".$lselmoni['idmonitoria']."</td>";
                            $this->dados .= "<td align=\"center\">".banco2data($lselmoni['dtmoni'])."</td>";
                            $this->dados .= "<td align=\"center\">".banco2data($lselmoni['datactt'])."</td>";
                            $this->dados .= "<td align=\"center\">".$lselmoni['operador']."</td>";
                            $this->dados .= "<td align=\"center\">".$lselmoni['ra_cnpj_cpf_idcontrato']."</td>";
                            $this->dados .= "<td align=\"center\">".nomevisu($lselmoni['idrel_filtros'])."</td>";
                            $this->dados .= "<td align=\"center\">".banco2data($lselfluxo['data'])."</td>";
                            $this->dados .= "<td align=\"center\">".$lselfluxo['nomedefinicao']."</td>";
                            if($lselmoni['iddefinicao'] == "1") {
                                $this->dados .= "<td align=\"center\">".$lselmoni['obsmoni']."</td>";
                            }
                            else {
                                $this->dados .= "<td align=\"center\">".$lselfluxo['obs']."</td>";
                            }
                        $this->dados .= "</tr>";
                    }
                }
                $this->dados .= "</table>";
            }
                
                // relatório especifico por resposta
            if($this->tipo != "mensal" && $this->tipo != "semanal" && $this->tipo != "exefluxo" && $this->tipo != "contesta" && $this->tipo != "operador" && $this->tipo != "histoperador") {
                    
                    //levanta semanas
                    $diaini = substr($datactt[0], 0,4)."-".substr($datactt[0],5,2)."-01";
                    $diafim = ultdiames(substr($diaini,8,2).substr($diaini,5,2).substr($diaini,0,4));
                    $data = $diaini;
                    $s = 0;
                    $i = 0;
                    $f = 1;
                    for(;$i <= $f;) {
                        $s++;
                        $data = $diaini;
                        $sem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($data,5,2),substr($data,8,2),substr($data,0,4)));
                        $acres = 6 - $sem;
                        $compdt = "SELECT DATE_ADD('$data',INTERVAL $acres DAY) as fimsem";
                        $ecomp = $_SESSION['fetch_array']($_SESSION['query']($compdt)) or die ("erro na query de consulta das datas");
                        $fimmes = "SELECT '".$ecomp['fimsem']."' > '$diafim' as result";
                        $efimmes = $_SESSION['fetch_array']($_SESSION['query']($fimmes)) or die ("verifica o fim do mes");
                        if($efimmes['result'] >= 1) {
                            $messem[$s] = $data.",".$diafim;
                        }
                        else {
                            $messem[$s] = $data.",".$ecomp['fimsem'];
                        }
                        $acresdt = "SELECT DATE_ADD('".$ecomp['fimsem']."',INTERVAL 1 DAY) as fimsem";
                        $eacres = $_SESSION['fetch_array']($_SESSION['query']($acresdt)) or die ("erro na query de consulta das datas");
                        $diaini = $eacres['fimsem'];
                        $datas = "SELECT '$diaini' <= '$diafim' as result";
                        $edatas = $_SESSION['fetch_array']($_SESSION['query']($datas)) or die ("erro na query de comparação das datas");
                        if($edatas['result'] >= 1) {
                            $i = 0;
                        }
                        else {
                            $i = 2;
                        }
                    }
                    foreach($messem as $sem) {
                        $dtsem = explode(",",$sem);
                        $vdtini = "SELECT '$datactt[0]' BETWEEN '$dtsem[0]' AND '$dtsem[1]' as result";
                        $evdtini = $_SESSION['fetch_array']($_SESSION['query']($vdtini)) or die ("erro na queru de comparação da data inicial");
                        $vdtfim = "SELECT '$datactt[1]' BETWEEN '$dtsem[0]' AND '$dtsem[1]' as result";
                        $evdtfim = $_SESSION['fetch_array']($_SESSION['query']($vdtfim)) or die ("erro na queru de comparação da data inicial");
                        $vdtinifim = "SELECT '$dtsem[0]' BETWEEN '$datactt[0]' AND '$datactt[1]' data1,'$dtsem[1]' BETWEEN '$datactt[0]' AND '$datactt[1]' data2";
                        $evdinifim = $_SESSION['fetch_array']($_SESSION['query']($vdtinifim)) or die ("erro na verficação da semana");
                        if($evdtini['result'] >= 1 && $evdtfim['result'] >= 1) {
                            $this->semanas[] = $datactt[0].",".$datactt[1];
                        }
                        if($evdtini['result'] >= 1 && $evdtfim['result'] == 0) {
                            $this->semanas[] = $datactt[0].",".$dtsem[1];
                        }
                        if($evdtini['result'] == 0 && $evdtfim['result'] >= 1) {
                            $this->semanas[] = $dtsem[0].",".$datactt[1];
                        }
                        if($evdinifim['data1'] >= 1 && $evdinifim['data2'] >= 1 && $datactt[0] != $dtsem[0] && $datactt[1] != $dtsem[1]) {
                            $this->semanas[] = $dtsem[0].",".$dtsem[1];
                        }
                    }
                    /* -- fim semanas --*/
                    
                    //inicia a coleta de dados
                    if(eregi("PARCEIROS",$this->nplan)) {
                        foreach($this->cons as $nome => $idoferta) {
                            $selespe = "SELECT COUNT(".$this->aliasrel[monitoria].".idmonitoria) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND $this->sqlcons'$idoferta' AND $whereespe";
                            $eselespe = $_SESSION['fetch_array']($_SESSION['query']($selespe)) or die (mysql_error());
                            $selespeft = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                    INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                    INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                    INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                    WHERE $wheresql $this->wherectt AND $this->sqlcons'$idoferta'";
                            $eselespeft = $_SESSION['fetch_array']($_SESSION['query']($selespeft));
                            $this->relresp['total'][] = $eselespeft['result'];
                            $this->relresp['perc'][] = round(($eselespe['result'] / ($eselespeft['result'] * $qtdeitens)) *100,2);
                        }
                        
                        foreach($this->semanas as $semana) {
                            $datas = explode(",",$semana);
                            $wheredt = " AND datactt BETWEEN '$datas[0]' AND '$datas[1]'";
                            foreach($this->cons as $kfiltro => $idfitlro) {
                                $selespe = "SELECT COUNT(".$this->aliasrel[monitoria].".idmonitoria) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $wheredt AND $this->sqlcons'$idfitlro' AND $whereespe";
                                $eselespe = $_SESSION['fetch_array']($_SESSION['query']($selespe)) or die (mysql_error());
                                $selespeft = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $wheredt AND $this->sqlcons'$idfitlro'";
                                $eselespeft = $_SESSION['fetch_array']($_SESSION['query']($selespeft));
                                $this->relrespsem [$kfiltro]['total'][] = $eselespeft['result'];
                                $this->relrespsem[$kfiltro]['perc'][] = round(($eselespe['result'] / ($eselespeft['result'] * $qtdeitens)) *100,2);
                            }
                        }
                    }
                    else {
                        if($this->vars['idplanilha'] == "") {
                            foreach($this->fmdados as $kfiltro => $n1) {
                                $c = count($n1);
                                $cloop = 0;
                                $ids = array();
                                foreach($n1 as $kfd => $fd) {
                                    $cloop++;
                                    $sqlcons = "";
                                    $colsf = array();
                                    $chave = explode("#",$kfd);
                                    $valchave = explode("#",$fd);
                                    if(count($chave) == 1) {
                                        $col = explode("#",$kfiltro);
                                        $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($col[0])."='$fd'";
                                        $ids[] = $fd;
                                        //$ideps = $fd;
                                    }
                                    else {
                                        foreach($chave as $kchave => $vchave) {
                                            $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                                    INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                            while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                                $colsf[] = $lselcol['nomefiltro_nomes'];
                                            }
                                        }
                                        $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                                        $ids[] = $valchave[0];
                                    }

                                    if(count($ids) == $c) {
                                        if(count($chave) == 1) {
                                            $coltt = explode("#",$kfiltro);
                                        }
                                        else {
                                            $coltt = $colsf; 
                                        }
                                        $sqlconstt = $this->aliasrel[rel_filtros].".id_".strtolower($coltt[0])." IN ('".implode("','",$ids)."')";
                                        $selespett = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND $sqlconstt";
                                        $eselespett = $_SESSION['fetch_array']($_SESSION['query']($selespett));
                                    }
                                    else {
                                    }

                                    $selespe = "SELECT COUNT(".$this->aliasrel[monitoria].".idmonitoria) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND $sqlcons AND $whereespe";
                                    $eselespe = $_SESSION['fetch_array']($_SESSION['query']($selespe)) or die (mysql_error());
                                    $selespeft = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $this->wherectt AND $sqlcons";
                                    $eselespeft = $_SESSION['fetch_array']($_SESSION['query']($selespeft));
                                    $this->relresp [$kfiltro]['total'][] = $eselespeft['result'];
                                    $this->relresp[$kfiltro]['perc'][] = round(($eselespe['result'] / ($eselespeft['result'] * $qtdeitens)) *100,2);
                                }
                            }
                        }
                        else {
                            foreach($this->cons as $nome => $idoferta) {
                                $selespe = "SELECT COUNT(".$this->aliasrel[monitoria].".idmonitoria) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $this->wherectt AND $this->sqlcons'$idoferta' AND $whereespe";
                                $eselespe = $_SESSION['fetch_array']($_SESSION['query']($selespe)) or die (mysql_error());
                                $selespeft = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $this->wherectt AND $this->sqlcons'$idoferta'";
                                $eselespeft = $_SESSION['fetch_array']($_SESSION['query']($selespeft));
                                $this->relresp['total'][] = $eselespeft['result'];
                                $this->relresp['perc'][] = round(($eselespe['result'] / ($eselespeft['result'] * $qtdeitens)) *100,2);
                            }
                        }
                        
                        //evolutivo
                        foreach($this->semanas as $semana) {
                            $datas = explode(",",$semana);
                            $wheredt = " AND datactt BETWEEN '$datas[0]' AND '$datas[1]'";
                            foreach($this->cons as $kfiltro => $idfitlro) {
                                $selespe = "SELECT COUNT(".$this->aliasrel[monitoria].".idmonitoria) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $wheredt AND $this->sqlcons'$idfitlro' AND $whereespe";
                                $eselespe = $_SESSION['fetch_array']($_SESSION['query']($selespe)) or die (mysql_error());
                                $selespeft = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                        INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                        INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                        INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                        INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                        INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                        INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                        INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                        INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                        INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                        INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                        WHERE $wheresql $wheredt AND $this->sqlcons'$idfitlro'";
                                $eselespeft = $_SESSION['fetch_array']($_SESSION['query']($selespeft));
                                $this->relrespsem [$kfiltro]['total'][] = $eselespeft['result'];
                                $this->relrespsem[$kfiltro]['perc'][] = round(($eselespe['result'] / ($eselespeft['result'] * $qtdeitens)) *100,2);
                            }
                            foreach($this->fmdados as $kfiltro => $n1) {
                                $c = count($n1);
                                $cloop = 0;
                                $ids = array();
                                foreach($n1 as $kfd => $fd) {
                                    $cloop++;
                                    $sqlcons = "";
                                    $colsf = array();
                                    $chave = explode("#",$kfd);
                                    $valchave = explode("#",$fd);
                                    if(count($chave) == 1) {
                                        $col = explode("#",$kfiltro);
                                        $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($col[0])."='$fd'";
                                        $ids[] = $fd;
                                        //$ideps = $fd;
                                    }
                                    else {
                                        foreach($chave as $kchave => $vchave) {
                                            $selcol = "SELECT nomefiltro_nomes FROM filtro_dados fd
                                                    INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fd.nomefiltro_dados='$vchave'";
                                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta do nome do filtro");
                                            while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                                                $colsf[] = $lselcol['nomefiltro_nomes'];
                                            }
                                        }
                                        $sqlcons = $this->aliasrel[rel_filtros].".id_".strtolower($colsf[0])."='$valchave[0]' AND ".$this->aliasrel[rel_filtros].".id_".  strtolower($colsf[1])."='$valchave[1]'";
                                        $ids[] = $valchave[0];
                                    }

                                    if(count($ids) == $c) {
                                        if(count($chave) == 1) {
                                            $coltt = explode("#",$kfiltro);
                                        }
                                        else {
                                            $coltt = $colsf; 
                                        }
                                        $sqlconstt = $this->aliasrel[rel_filtros].".id_".strtolower($coltt[0])." IN ('".implode("','",$ids)."')";
                                        $selespett = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $wheredt AND $sqlconstt";
                                        $eselespett = $_SESSION['fetch_array']($_SESSION['query']($selespett));
                                    }
                                    
                                    $selespe = "SELECT COUNT(".$this->aliasrel[monitoria].".idmonitoria) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $wheredt AND $sqlcons AND $whereespe";
                                    $eselespe = $_SESSION['fetch_array']($_SESSION['query']($selespe)) or die (mysql_error());
                                    $selespeft = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                            INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                            INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                            INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                            INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                            INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                            INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                            INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                            INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                            INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                            WHERE $wheresql $wheredt AND $sqlcons";
                                    $eselespeft = $_SESSION['fetch_array']($_SESSION['query']($selespeft));
                                    $this->relrespsem[$kfd]['total'][] = $eselespeft['result'];
                                    $this->relrespsem[$kfd]['perc'][] = round(($eselespe['result'] / ($eselespeft['result'] * $qtdeitens)) *100,2);
                                }
                            }       
                        }
                    }
                    
                    //lista operadores
                    $seloper = "SELECT ".$this->aliasrel[monitoria].".idoperador, operador, ".$this->aliasrel[monitoria].".idrel_filtros, SUM(if(qtdefg >= 1,1,0)) as qtdefg, ROUND(AVG(valor_fg),2) as media, COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                WHERE $wheresql $this->wherectt GROUP BY ".$this->aliasrel[monitoria].".idoperador ORDER BY ".$this->aliasrel[operador].".operador";
                    $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consutla dos operadores");
                    while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                        $selqtdefg = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND ".$this->aliasrel[monitoria].".idoperador='".$lseloper['idoperador']."' AND qtdefg > 0";
                        $eselfg = $_SESSION['fetch_array'] ($_SESSION['query']($selqtdefg)) or die ("erro na query de consulta da qtde de FG");
                        $selespoper = "SELECT COUNT(DISTINCT(".$this->aliasrel[monitoria].".idmonitoria)) as result FROM monitoria ".$this->aliasrel[monitoria]."
                                                INNER JOIN rel_filtros ".$this->aliasrel[rel_filtros]." ON ".$this->aliasrel[rel_filtros].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN operador ".$this->aliasrel[operador]." ON ".$this->aliasrel[operador].".idoperador=".$this->aliasrel[monitoria].".idoperador
                                                INNER JOIN super_oper ".$this->aliasrel[super_oper]." ON ".$this->aliasrel[super_oper].".idsuper_oper=".$this->aliasrel[monitoria].".idsuper_oper
                                                INNER JOIN monitor ".$this->aliasrel[monitor]." ON ".$this->aliasrel[monitor].".idmonitor=".$this->aliasrel[monitoria].".idmonitor
                                                INNER JOIN moniavalia ".$this->aliasrel[moniavalia]." ON ".$this->aliasrel[moniavalia].".idmonitoria=".$this->aliasrel[monitoria].".idmonitoria
                                                INNER JOIN conf_rel ".$this->aliasrel[conf_rel]." ON ".$this->aliasrel[conf_rel].".idrel_filtros=".$this->aliasrel[monitoria].".idrel_filtros
                                                INNER JOIN param_moni ".$this->aliasrel[param_moni]." ON ".$this->aliasrel[param_moni].".idparam_moni=".$this->aliasrel[conf_rel].".idparam_moni
                                                INNER JOIN fluxo ".$this->aliasrel[fluxo]." ON ".$this->aliasrel[fluxo].".idfluxo=".$this->aliasrel[conf_rel].".idfluxo
                                                INNER JOIN monitoria_fluxo ".$this->aliasrel[monitoria_fluxo]." ON ".$this->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$this->aliasrel[monitoria].".idmonitoria_fluxo
                                                INNER JOIN rel_fluxo ".$this->aliasrel[rel_fluxo]." ON ".$this->aliasrel[rel_fluxo].".idrel_fluxo=".$this->aliasrel[monitoria_fluxo].".idrel_fluxo
                                                INNER JOIN indice ".$this->aliasrel[indice]." ON ".$this->aliasrel[indice].".idindice=".$this->aliasrel[param_moni].".idindice
                                                WHERE $wheresql $this->wherectt AND $whereespe AND ".$this->aliasrel[monitoria].".idoperador='".$lseloper['idoperador']."'";
                        $eselespeoper = $_SESSION['fetch_array']($_SESSION['query']($selespoper)) or die (mysql_error());
                        $this->operadores[$lseloper['idoperador']]['operador'] = $lseloper['operador'];
                        $this->operadores[$lseloper['idoperador']]['idrel_filtros'] = $lseloper['idrel_filtros'];
                        $this->operadores[$lseloper['idoperador']]['qtde'] = $lseloper['result'];
                        $this->operadores[$lseloper['idoperador']]['qtdefg'] = $eselfg['result'];
                        $this->operadores[$lseloper['idoperador']]['media'] = str_replace(".",",",round($lseloper['media'],2));
                        $this->operadores[$lseloper['idoperador']]['qtdeespe'] = $eselespeoper['result'];
                        $this->operadores[$lseloper['idoperador']]['perc'] = str_replace(".",",",round(($eselespeoper['result'] / $lseloper['result']) *100,2));
                    }                    
                }
        }
    }
    
    function Montadados () {
        $this->coreps();
    }
}

?>
