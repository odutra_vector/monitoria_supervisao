<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once ($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once ($rais.'/monitoria_supervisao/classes/class.tabelas.php');

class ConfigMail extends PHPMailer {
    public $tipoacao;
    public $idconf;
    public $monitor;
    public $idmf;
    public $idmonitoria;
    public $idmonitor;
    public $obscont;
    public $iduser;
    public $nomeuser;
    public $tipouser;
    public $resp;
    public $qtdeimp;
    public $qtdelido;
    public $to;
    public $nometo;
    public $senha;
    public $relapres;
    public $upload;
    public $idrel;
    public $idcalib;
    
    function teste() {
        try {
            if($_POST['seg'] == "SIM") {
                $this->SMTPSecure = "tls";
            }
            $this->SMTPDebug  = 2;
            $host = $_POST['host'];
            $this->Host = "$host";
            $this->Port = $_POST['porta'];
            if($_POST['autentica'] == 1) {
                $this->SMTPAuth = true;
                $this->Username = $_POST['email'];
                $this->Password = $_POST['senha'];
            }
            else {
                $this->SMTPAuth = false;
            }
            $this->SetFrom($_POST['email'], $_SESSION['nomecli']);
            $this->AddAddress($_POST['email'], "ADMIN");
            $this->Subject = 'PHPMailer Test Subject via mail(), advanced';
            $msg = "<html>";
            $msg .= "<header>";
            $msg .= "</header>";
            $msg .= "<body>";
            $msg .= "<table width=\"800\" border=\"0\">";
            $msg .= "<tr>";
            $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>TESTE</strong></td>";
            $msg .= "<td>TESTE</td>";
            $msg .= "</table>";
            $msg .= "</body>";
            $msg .= "</html>";            
            $this->Body = $msg;
            if(!$this->Send()) {
            ?>
            <script type="text/javascript">
                alert('Erro no envio da mensagem');
                window.location = 'admin.php?menu=confemail';
            </script>
            <?php
            }
            else {
                ?>
                <script type="text/javascript">
                alert('Mensagem enviada com sucesso');
                window.location = 'admin.php?menu=confemail';
                </script>
                <?php
            }
            //break;
        }
        catch (phpmailerException $e) {
            ?>
            <script type="text/javascript">
                alert('<?php echo $e->errorMessage();?>'); //Pretty error messages from PHPMailer
            </script>
            <?php
        } 
        catch (Exception $e) {
            ?>
            <script type="text/javascript">
                alert('<?php echo $e->getMessage();?>'); //Boring error messages from anything else!
            </script>
            <?php
        }  
    }
    
    function config($id) {
        $alias = new TabelasSql();
        $confmail = "SELECT * FROM conf_mail";
        $econfmail = $_SESSION['fetch_array']($_SESSION['query']($confmail)) or die ("erro na consulta da configuraÃ§Ã£o para envio de e-mail");
        if($econfmail['tls'] == "SIM") {
            $this->SMTPSecure = "tls";
        }
        $this->CharSet = "UTF-8";
        $this->Timeout = 30;
        $host = $econfmail['host'];
        $this->Host = "$host";
        $this->Port = $econfmail['porta'];
        if($econfmail['autentica'] == 1) {
            $this->SMTPAuth = true;
            $this->Username = $econfmail['email'];
            $this->Password = $econfmail['senha'];
        }
        else {
            $this->SMTPAuth = false;
        }
        $this->From = $econfmail['email'];
        $this->FromName = $_SESSION['nomecli'];

        $selconf = "SELECT * FROM confemailenv WHERE idconfemailenv='".$this->idconf."'";
        $eselconf = $_SESSION['fetch_array']($_SESSION['query']($selconf)) or die ("erro na query de consulta da configuraÃ§Ã£o cadastrada");
        
        if($eselconf['tipo'] == "FG" OR $eselconf['tipo'] == "IMPRODUTIVA") {
             $dt = date('d/m/Y');
             $m = date('m/Y');
             $db = strtoupper($_SESSION['db']);
             $this->Subject = $eselconf['titulo'];
             if(strstr($eselconf['titulo'],'$d')) {
                  $div = explode('$d',$this->Subject);
                  $this->Subject = $div[0].$dt.$div[1];
             }
             if(strstr($eselconf['titulo'],'$m')) {
                  $div = explode('$m',$this->Subject);
                  $this->Subject = $div[0].$m.$div[1];
             }
             if(strstr($eselconf['titulo'],'$cli')) {
                 $div = explode('$cli',$this->Subject);
                  $this->Subject = $div[0]." - ".$db.$div[1];
             }
             $this->Subject = $this->Subject;
            $dadosemail = explode(",",$eselconf['campo']);
            if(in_array("idrel_filtros",$dadosemail)) {
                if($eselconf['tipo'] == "IMPRODUTIVA") {
                    $selrel = "SELECT idrel_filtros FROM regmonitoria WHERE idregmonitoria='$id'";
                    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relacionamento");
                    $this->Subject = $this->Subject." - ".nomevisu($eselrel['idrel_filtros']);
                }
                else {
                    $selrel = "SELECT idrel_filtros FROM monitoria WHERE idmonitoria='$this->idmonitoria'";
                    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relacionamento");
                    $this->Subject = $this->Subject." - ".nomevisu($eselrel['idrel_filtros']);
                }
            }
            else {
                $this->Subject = $this->Subject;
            }
        }
        else { 
            $dt = date('d/m/Y');
             $m = date('m/Y');
             $db = strtoupper($_SESSION['db']);
             $this->Subject = $eselconf['titulo'];
             if(strstr($eselconf['titulo'],'$d')) {
                  $div = explode('$d',$this->Subject);
                  $this->Subject = $div[0].$dt.$div[1];
             }
             if(strstr($eselconf['titulo'],'$m')) {
                  $div = explode('$m',$this->Subject);
                  $this->Subject = $div[0].$m.$div[1];
             }
             if(strstr($eselconf['titulo'],'$cli')) {
                 $div = explode('$cli',$this->Subject);
                  $this->Subject = $div[0]." - ".$db.$div[1];
             }
             $this->Subject = $this->Subject;
        }
        $msg = "<html>";
        $msg .= "<header>";
        $msg .= "</header>";
        $msg .= "<body ";
        if($eselconf['fundoimg'] != "") {
            $msg .= "background=\"http://".$_SESSION['site']."/monitoria_supervisao/images/email/".$eselconf['fundoimg']."\"";
        }
        $msg .= ">";

        if($eselconf['tipo'] == "MONITOR") {
            $msg .= "Monitor $this->monitor,<p></p>";
            $msg .= $eselconf['msg']."<br/><br/>";

            $msg .= "<table width=\"800\" border=\"0\" style=\"font-family:'Courier New', Courier, monospace; font-size:10px\">";
            $campos = explode(",",$eselconf['campo']);
            foreach($campos as $campo) {
                  $msg .= "<tr>";
                  $seluser = "SELECT * FROM monitor WHERE idmonitor='".$this->idmonitor."'";
                  $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na consulta do usuÃ¡rio criado");
                  if($campo == "senha") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                    $msg .= "<td>$this->senha</td>";
                  }
                  else {
                      if($campo == "nome") {
                          $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                          $msg .= "<td>".$eseluser[$campo.'monitor']."</td>";
                      }
                      if($campo == "datacad") {
                          $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                          $msg .= "<td>".banco2data($eseluser[$campo])."</td>";
                      }
                      if($campo != "nome" && $campo != "datacad") {
                          $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                          $msg .= "<td>$eseluser[$campo]</td>";
                      }
                  }
                  $msg .= "</tr>";
            }
        }
        if($eselconf['tipo'] == "IMPORTACAO") {
            $columns = explode(",",$eselconf['campo']);
            if($this->tipoacao == "upload") {
                $msg .= $eselconf['msg']."<p></p><p></p>";
                $msg .= "<table width=\"800\" border=\"0\" style=\"font-family:'Courier New', Courier, monospace; font-size:10px\">";
                foreach($columns as $coluna) {
                    $msg .= "<tr>";
                    $seluload = "SELECT * FROM upload u
                                 WHERE u.idupload='".$this->upload."'";
                    $eselupload = $_SESSION['fetch_array']($_SESSION['query']($seluload)) or die ("erro na query de consulta do upload");
                    if($coluna == "usuario") {
                        $seluser = "SELECT nome".$eselupload['tabuser']." FROM ".$eselupload['tabuser']." WHERE id".$eselupload['tabuser']."='".$eselupload['iduser']."'";
                        $enomeuser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do nome do usuÃ¡rio");
                        $nomeuser = $enomeuser['nome'.$eselupload['tabuser']];
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                        $msg .= "<td>$nomeuser</td>";
                    }
                    if($coluna == "relacionamento") {
                        $relapres = nomeapres($eselupload['idrel_filtros']);
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                        $msg .= "<td>$relapres</td>";
                    }
                    if($coluna != "usuario" && $coluna != "relacionamento") {
                        if(eregi("data",$coluna)) {
                            $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                            $msg .= "<td>".banco2data($eselupload[$coluna])."</td>";
                        }
                        else {
                            $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                            $msg .= "<td>$eselupload[$coluna]</td>";
                        }
                    }
                    $msg .= "</tr>";
                }
            }
            if($this->tipoacao == "importacao") {
                $seluload = "SELECT * FROM upload u
                                    WHERE u.idupload='".$this->upload."'";
                $eselupload = $_SESSION['fetch_array']($_SESSION['query']($seluload)) or die ("erro na query de consulta do upload");

                $msg .= $eselconf['msg']."<p></p><p></p>";
                $msg .= "<table width=\"800\" border=\"0\">";
                foreach($columns as $coluna) {
                    $msg .= "<tr>";
                    if($coluna == "usuario") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                        $msg .= "<td>$this->nomeuser</td>";
                    }
                    if($coluna == "dataini" OR $coluna == "datafim" OR $coluna == "dataimp" OR $coluna == "dataup") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                        $msg .= "<td>".banco2data($eselupload[$coluna])."</td>";
                    }
                    if($coluna == "relacionamento") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>RELACIONAMENTO</strong></td>";
                        $msg .= "<td>".nomeapres($eselupload['idrel_filtros'])."</td>";
                    }
                    if($coluna == "qtdeimp") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>IMPORTAÇÕES</strong></td>";
                        $msg .= "<td>$this->qtdeimp</td>";
                    }
                    if($coluna == "qtdeerro") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>ERROS</strong></td>";
                        $msg .= "<td>".($this->qtdelido - $this->qtdeimp)."</td>";
                    }
                    if($coluna == "idrel_filtros" OR $coluna == "tabuser") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                        $msg .= "<td>$eselupload[$coluna]</td>";
                    }
                    if($coluna == "idupload") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                        $msg .= "<td>".$eselupload[$coluna]."</td>";
                    }
                    $msg .= "</tr>";
                }
            }
        }
        if($eselconf['tipo'] == "FLUXO") {
            /*nomeuser, nomefluxo, nomestatus, nomedefinicao, obs, idmonitoria, idmonitor, nomemonitor, idrel_filtros, relacionamento, data, 
            horaini, idplanilha, descriplanilha, idaval_plan, nomeaval_plan, valor_final_aval, idgrupo, descrigrupo, idsubgrupo, descrisubgrupo, 
            idpergunta, descripergunta, idresposta, descriresposta*/
            $msg .= $eselconf['msg']."<br><br/>____________________________________________________<br><br/>";
            $campos = explode(",",$eselconf['campo']);
            $aliascamp = array('SUPER_OPER' => 'so','OPERADOR' => 'o','DADOS' => 'fg');
            $selcamp = "SELECT * FROM coluna_oper";
            $eselcamp = $_SESSION['query']($selcamp) or die ("erro na query de consulta dos campos do sistema");
            while($lselcamp = $_SESSION['fetch_array']($eselcamp)) {
                $campdados[] = $aliascamp[$lselcamp['incorpora']].".".$lselcamp['nomecoluna'];
            }
            $selcamp = "SHOW COLUMNS FROM fila_grava";
            $eselcamp = $_SESSION['query']($selcamp) or die ("erro na query de consulta dos campos do sistema");
            while($lselcamp = $_SESSION['fetch_array']($eselcamp)) {
                $campdados[] = $aliascamp["DADOS"].".".$lselcamp['Field'];
            }
            $selmoni = "SELECT f.nomefluxo,mf.tabuser,mf.iduser,s.nomestatus,d.nomedefinicao,mo.obs,mo.idmonitoria,mo.idmonitor,m.nomemonitor,mo.idrel_filtros,mo.data,
                        mo.horaini,mo.idplanilha,p.descriplanilha,ma.idaval_plan,ap.nomeaval_plan,ma.valor_final_aval,fg.narquivo,".implode(",",$campdados)." FROM monitoria mo
                        INNER JOIN monitor m ON m.idmonitor = mo.idmonitor
                        INNER JOIN operador o ON o.idoperador = mo.idoperador
                        INNER JOIN super_oper so ON so.idsuper_oper = mo.idsuper_oper
                        INNER JOIN fila_grava fg ON fg.idfila_grava = mo.idfila
                        INNER JOIN planilha p ON p.idplanilha = mo.idplanilha
                        INNER JOIN moniavalia ma ON ma.idmonitoria = mo.idmonitoria
                        INNER JOIN aval_plan ap ON ap.idaval_plan = ma.idaval_plan
                        INNER JOIN rel_filtros rf ON rf.idrel_filtros = mo.idrel_filtros
                        INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                        INNER JOIN fluxo f ON f.idfluxo = cr.idfluxo
                        INNER JOIN monitoria_fluxo mf ON mf. idmonitoria_fluxo = mo.idmonitoria_fluxo
                        INNER JOIN status s ON s.idstatus = mf.idstatus
                        INNER JOIN definicao d ON d.iddefinicao = mf.iddefinicao
                        WHERE mo.idmonitoria='$this->idmonitoria' AND mo.idmonitoria_fluxo='$this->idmf' GROUP BY mo.idmonitoria";
            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da monitoria e do fluxo");
            $msg .= "<table width=\"800\" border=\"0\">";
            foreach($campos as $campo) {
                $msg .= "<tr>";
                if($campo == "nomeuser") {
                    if($eselmoni['tabuser'] == "user_adm") {
                        $aliasuser = ",apelido";
                    }
                    else {
                        $aliasuser = "";
                    }
                    $seluser = "SELECT nome".$eselmoni['tabuser']."$aliasuser FROM ".$eselmoni['tabuser']." WHERE id".$eselmoni['tabuser']."='".$eselmoni['iduser']."'";
                    $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query para consulta o nome do usuÃ¡rio");
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>NOME USUÁRIO<strong></td>";
                    if($eselmoni['tabuser'] == "user_adm") {
                        $nome = $eseluser['apelido'];
                    }
                    else {
                        $nome = $eseluser['nome'.$eselmoni['tabuser']];
                    }
                    $msg .= "<td>".$nome."</td>";
                }
                if($campo == "relacionamento") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>RELACIONAMENTO<strong></td>";
                    $msg .= "<td>".nomeapres($eselmoni['idrel_filtros'])."</td>";
                }
                if($campo == "nomefluxo" OR $campo == "nomestatus" OR $campo == "nomedefinicao" OR $campo == "nomemonitor") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper(str_replace("nome","",$campo))."</strong></td>";
                    $msg .= "<td>".$eselmoni[$campo]."</td>";
                }
                if($campo == "data") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                    $msg .= "<td>".banco2data($eselmoni[$campo])."</td>";
                }
                if($campo == "valor_final_aval") {
                    $a = 0;
                    $selaval = "SELECT valor_fg, a.nomeaval_plan FROM moniavalia m INNER JOIN aval_plan a ON a.idaval_plan = m.idaval_plan WHERE m.idmonitoria='$this->idmonitoria' GROUP BY m.idaval_plan";
                    $eselaval = $_SESSION['query']($selaval) or die ("erro na query de consulta das avaliaÃ§Ãµes relacionadas a monitoria");
                    while($lselaval = $_SESSION['fetch_array']($eselaval)) {
                        if($a == 0) {
                        }
                        else {
                            $msg .= "<tr>";
                        }
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($lselaval['nomeaval_plan'])."</strong></td>";
                        $msg .= "<td>".$lselaval['valor_fg']."</td>";
                        $msg .= "</tr>";                     
                        $a++;
                    }
                }
                if($campo != "obs" && $campo != "data" && $campo != "valor_final_aval" && $campo != "nomefluxo" && $campo != "nomestatus" && $campo != "nomedefinicao" && $campo != "nomemonitor" && $campo != "relacionamento" && $campo != "nomeuser") {
                    if($campo == "descrigrupo" OR $campo == "descrisubgrupo" OR $campo == "descripergunta" OR $campo == "descriresposta") {
                    }
                    else {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                        if($campo == "datactt") {
                            $msg .= "<td>".banco2data($eselmoni[$campo])."</td>";
                        }
                        else {
                            $msg .= "<td>$eselmoni[$campo]</td>";
                        }
                    }
                }
                if($campo == "valor_final_aval") {
                }
                else {
                    $msg .= "</tr>";
                }
            }
            if($this->obscont != "") {
                $msg .= "<tr><td bgcolor=\"#CCCCCC\" width=\"200\"><strong>OBS AVALIAÇÃO</strong></td><td>$this->obscont</td></tr><tr height=\"30px\"></tr>";
            }
            $msg .= "<tr><td colspan=\"2\" bgcolor=\"#CCCCCC\" align=\"center\"><strong>OBS MONITORIA</strong></td></tr>";
            $msg .= "<tr><td colspan=\"2\">".$eselmoni['obs']."</td></tr>";
            $msg .= "</table><br/>";
            $selitens = "SELECT *,count(*) as r FROM itenscontest WHERE idmonitoria='$this->idmonitoria'";
            $eselitem = $_SESSION['fetch_array']($_SESSION['query']($selitens)) or die ("erro na query de consulta dos itens contestados");
            if($eselitem['r'] >= 1) {
                $itens = explode(",",$eselitem['idspergunta']);
            }
            $descri = array('descrigrupo','descripergunta','descriresposta','descrisubgrupo');
            $msg .= "<table width=\"800\" border=\"0\">";
            foreach($itens as $it) {
                $sperg = "SELECT * FROM pergunta WHERE idpergunta='$it'";
                $esperg = $_SESSION['query']($sperg) or die ("erro na query de consulta da pergunta");
                while($lsperg = $_SESSION['fetch_array']($esperg)) {
                    $srespmoni = "SELECT COUNT(*) as result, descripergunta, descriresposta FROM moniavalia ma
                                            INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                                            WHERE idmonitoria='$this->idmonitoria' AND ma.idresposta='".$lsperg['idresposta']."'";
                    $erespmoni = $_SESSION['fetch_array']($_SESSION['query']($srespmoni)) or die ("erro na query de consulta da resposta");
                    $spergsub = "SELECT COUNT(*) as result, descrisubgrupo, idsubgrupo FROM subgrupo WHERE idpergunta='$it'";
                    $espergsub = $_SESSION['fetch_array']($_SESSION['query']($spergsub)) or die ("erro na consulta do subgrupo");
                    if($espergsub['result'] >= 1) {
                        $sgrupo = "SELECT COUNT(*) as result, descrigrupo FROM grupo WHERE idrel='".$espergsub['idsubgrupo']."'";
                    }
                    else {
                        $sgrupo = "SELECT COUNT(*) as result, descrigrupo FROM grupo WHERE idrel='$it'";
                        $esgrupo = $_SESSION['fetch_array']($_SESSION['query']($sgrupo)) or die ("erro na query de consulta do grupo");
                    }
                    if(in_array('descrisubgrupo',$campos) && $espergsub['result'] >= 1) {
                        if($erespmoni['result'] >= 1) {
                            $itenscontest[$esgrupo['descrigrupo']][$espergsub['descrisubgrupo']][$erespmoni['descripergunta']][] =$erespmoni['descriresposta'];
                        }
                        else {
                        }
                    }
                    else {
                        if($erespmoni['result'] >= 1) {
                            $itenscontest[$esgrupo['descrigrupo']][$erespmoni['descripergunta']][] =$erespmoni['descriresposta'];
                        }
                        else {
                        }
                    }
                }
            }
            if(in_array('descrigrupo',$campos) OR in_array('descrisubgrupo',$campos) OR in_array('descripergunta',$campos) OR in_array('descriresposta',$campos)) {
                if($espergsub['result']  >= 1) {
                    foreach($itenscontest as $grupo => $subs) {
                        $msg .= "<tr bgcolor=\"#999999\" align=\"center\"><td><strong>$grupo</strong></td></tr>";
                        foreach($subs as $sub => $pergs) {
                            $msg .= "<tr bgcolor=\"#73C2FF\" align=\"center\"><td><strong>$sub</strong></td></tr>";
                            foreach($pergs as $perg  => $resps) {
                                $msg .= "<tr bgcolor=\"#FFC3A1\" align=\"center\"><td><strong>$perg</strong></td></tr>";
                                foreach($resps as $resp) {
                                    $msg .= "<tr bgcolor=\"#FFFFFF\" align=\"center\"><td>$resp</td></tr>";
                                }
                            }
                        }
                    }
                }
                else {
                    foreach($itenscontest as $grupo => $pergs) {
                        $msg .= "<tr bgcolor=\"#999999\" align=\"center\"><td><strong>$grupo</strong></td></tr>";
                        foreach($pergs as $perg => $resps) {
                            $msg .= "<tr bgcolor=\"#FFC3A1\" align=\"center\"><td><strong>$perg</strong></td></tr>";
                            foreach($resps as $resp) {
                                $msg .= "<tr bgcolor=\"#FFFFFF\" align=\"center\"><td>$resp</td></tr>";
                            }
                        }
                    }
                }
            }
        }
        if($eselconf['tipo'] == "USUARIO") {
            $msg .= $eselconf['msg']."<p></p>";
            $msg .= "<table width=\"800\" border=\"0\">";
            if($this->tipouser == "user_adm") {
                $campos = explode(",",$eselconf['campo']);
                foreach($campos as $campo) {
                    $msg .= "<tr>";
                    if($campo == "senha") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                        $msg .= "<td>$this->senha</td>";
                    }
                    else {
                        if(eregi("data",$campo)) {
                              $seluser = "SELECT * FROM user_adm WHERE iduser_adm='".$this->iduser."'";
                              $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>$campo</strong></td>";
                              $msg .= "<td>".banco2data($eseluser[$campo])."</td>";
                        }
                        else {
                              $seluser = "SELECT * FROM user_adm WHERE iduser_adm='".$this->iduser."'";
                              $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
                              if($campo == "nome") {
                                  $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                                  $msg .= "<td>".$eseluser[$campo.'user_adm']."</td>";
                              }
                              else {
                                  $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                                  $msg .= "<td>$eseluser[$campo]</td>";
                              }
                        }
                    }
                    $msg .= "</tr>";
                }
            }
            if($this->tipouser == "user_web") {
                $campos = explode(",",$eselconf['campo']);
                foreach($campos as $campo) {
                    $msg .= "<tr>";
                    if($campo == "senha") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                        $msg .= "<td>$this->senha</td>";
                    }
                    else {
                        if(eregi("data",$campo)) {
                            $seluser = "SELECT * FROM user_web WHERE iduser_web='".$this->iduser."'";
                            $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na consulta do usuÃ¡rio criado");
                            $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>$campo</strong></td>";
                            $msg .= "<td>".banco2data($eseluser[$campo])."</td>";
                        }
                        else {
                          $seluser = "SELECT * FROM user_web WHERE iduser_web='".$this->iduser."'";
                          $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na consulta do usuÃ¡rio criado");
                          if($campo == "nome") {
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                              $msg .= "<td>".$eseluser[$campo.'user_web']."</td>";
                          }
                          else {
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($campo)."</strong></td>";
                              $msg .= "<td>$eseluser[$campo]</td>";
                          }
                        }
                    }
                    $msg .= "</tr>";
                }
            }
        }
        if($eselconf['tipo'] == "CALIB") {
            $selcalib = "SELECT * FROM agcalibragem WHERE idagcalibragem='$this->idcalib' AND idrel_filtros='$this->idrel' AND participantes='$this->iduser'";
            $eselcalib = $_SESSION['fetch_array']($_SESSION['query']($selcalib)) or die ("erro na query de consulta de calibragem agendada");
            $columns = explode(",",$eselconf['campo']);
            $msg .= $eselconf['msg']."<p></p><p></p>";
            $msg .= "<table width=\"800\" border=\"0\">";
            foreach($columns as $coluna) {
                $msg .= "<tr>";
                if($coluna == "idrel_filtros") {
                    $nomerel = nomeapres($eselcalib[$coluna]);
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>RELACIONAMENTO</strong></td>";
                    $msg .= "<td>$nomerel</td>";
                }
                if($coluna == "participantes") {
                    $part = explode("-",$eselcalib[$coluna]);
                    $tab = $part[1];
                    $iduser = $part[0];
                    if($tab == "monitor") {
                        $seluser = "SELECT ua.iduser_adm, ua.nomeuser_adm, ua.email FROM $tab INNER JOIN user_adm ua ON ua.iduser_adm = $tab.iduser_resp WHERE $tab.id$tab='$iduser'";
                        $tab = "user_adm";
                    }
                    else {
                        $seluser = "SELECT id$tab, nome$tab, email FROM $tab WHERE id$tab='$iduser'";
                    }
                    $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do nome do usuÃ¡rio");
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>USUÁRIO</strong></td>";
                    $msg .= "<td>".$eseluser['nome'.$tab]."</td>";
                }
                if($coluna == "idmoni") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>ID MONITORIA</strong></td>";
                    $msg .= "<td>".$eselcalib['idmonitoria']."</td>";
                }
                if($coluna == "dataini" OR $coluna == "datafim") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                    $msg .= "<td>".banco2data($eselcalib[$coluna])."</td>";
                }
                $msg .= "</tr>";
            }

        }
        if($eselconf['tipo'] == "FG") {
            $msg .= $eselconf['msg']."<p></p>";
            $msg .= "<table width=\"800\" border=\"0\">";
            $cols = explode(",",$eselconf['campo']);
            foreach($cols as $col) {
                if($col == "idpergunta") {
                    $ult = $col;
                }
                else {
                    $columns[] = $col;
                }
            }
            $columns[] = $ult;
            $aliascamp = array('SUPER_OPER' => 'so','OPERADOR' => 'o','DADOS' => 'fg');
            $selcamp = "SELECT * FROM coluna_oper";
            $eselcamp = $_SESSION['query']($selcamp) or die ("erro na query de consulta dos campos do sistema");
            while($lselcamp = $_SESSION['fetch_array']($eselcamp)) {
                $campdados[] = $aliascamp[$lselcamp['incorpora']].".".$lselcamp['nomecoluna'];
            }
            $selcamp = "SHOW COLUMNS FROM fila_grava";
            $eselcamp = $_SESSION['query']($selcamp) or die ("erro na query de consulta dos campos do sistema");
            while($lselcamp = $_SESSION['fetch_array']($eselcamp)) {
                $campdados[] = $aliascamp["DADOS"].".".$lselcamp['Field'];
            }
            $selmoni = "SELECT m.idmonitoria,m.idmonitoriavinc,m.idrel_filtros,m.data,m.qtdefg,m.qtdefgm,m.obs,fg.narquivo,m.idmonitor,mo.nomemonitor,".implode(",",$campdados)." FROM monitoria m
                        INNER JOIN operador o ON o.idoperador = m.idoperador
                        INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor
                        INNER JOIN super_oper so ON so.idsuper_oper = m.idsuper_oper
                        INNER JOIN fila_grava fg ON fg.idfila_grava = m.idfila
                        WHERE idmonitoria='$this->idmonitoria'";
            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta dos dados da monitoria");
            $selitens = "SELECT ma.idpergunta, p.descripergunta, ma.idresposta,p.descriresposta, p.avalia, obs 
                        FROM moniavalia ma
                        INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta 
                        WHERE idmonitoria='$this->idmonitoria' AND (p.avalia='FG' OR p.avalia='FGM')";
            $eselitens = $_SESSION['query']($selitens) or die ("erro na query e consulta das perguntas com Falha Grave");
            $contitens = "SELECT ma.idpergunta, p.descripergunta, ma.idresposta,p.descriresposta, p.avalia, obs, COUNT(*) as result 
                        FROM moniavalia ma
                        INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta 
                        WHERE idmonitoria='$this->idmonitoria' AND (p.avalia='FG' OR p.avalia='FGM')";
            $econt = $_SESSION['fetch_array']($_SESSION['query']($contitens)) or die ("erro na query e consulta das perguntas com Falha Grave");
            
            foreach($columns as $coluna) {
                if($coluna == "idpergunta") {
                }
                else {
                    $msg .= "<tr>";
                }
                if($coluna == "idmonitoria") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>ID MONITORIA</strong></td>";
                    $msg .= "<td bgcolor=\"#FFFFFF\"><a href=\"http://".$_SESSION['site']."/inicio.php?menu=idmoni&idmoni=".$eselmoni['idmonitoria']."\">".$eselmoni['idmonitoria']."</a></td>";
                    if($eselmoni['idmonitoriavinc'] != "0000000") {
                         $selmonivinc = "SELECT descriplanilha FROM monitoria m
                                         INNER JOIN planilha p ON p.idplanilha = m.idplanilha
                                         WHERE idmonitoria='".$eselmoni['idmonitoriavinc']."'";
                         $eselmonivinc = $_SESSION['fetch_array']($_SESSION['query']($selmonivinc)) or die (mysql_error());
                         $msg .= "</tr>";
                         $msg .= "<tr>";
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".$eselmonivinc['descriplanilha']."</strong></td>";
                              $msg .= "<td bgcolor=\"#FFFFFF\"><a href=\"http://".$_SESSION['site']."/inicio.php?menu=idmoni&idmoni=".$eselmoni['idmonitoriavinc']."\">".$eselmoni['idmonitoriavinc']."</a></td>";
                         $msg .= "</tr>";
                    }
                }
                if($coluna == "data" OR $coluna == "datactt") {
                    if($coluna == "data") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>DATA MONITORIA</strong></td>";
                        $msg .= "<td bgcolor=\"#FFFFFF\">".banco2data($eselmoni['data'])."</td>";
                    }
                    if($coluna == "datactt") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>DATA CONTATO</strong></td>";
                        $msg .= "<td bgcolor=\"#FFFFFF\">".banco2data($eselmoni['datactt'])."</td>";
                    }
                }
                if($coluna == "idrel_filtros") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>RELACIONAMENTO</strong></td>";
                    $msg .= "<td bgcolor=\"#FFFFFF\">".nomeapres($eselmoni['idrel_filtros'])."</td>";
                }
                if($coluna == "idpergunta") {
                    $perg = "";
                    $p = 0;
                    while($lselitens = $_SESSION['fetch_array']($eselitens)) {
                        if($perg == "" OR $perg != $lselitens['idpergunta']) {
                            $msg .= "<tr>";
                            $msg .= "<td colspan=\"2\" align=\"center\">";
                            $msg .= "<table width=\"800px\">";
                                $msg .= "<tr>";
                                    $msg .= "<td bgcolor=\"#CCCCCC\" align=\"center\" width=\"100%\">".$lselitens['descripergunta']."</td>";
                                $msg .= "</tr>";
                                $perg = $lselitens['idpergunta'];
                                $msg .= "<tr>";
                                    $msg .= "<td bgcolor=\"#FFC3A1\" align=\"center\" width=\"100%\">".$lselitens['descriresposta']."</td>";
                                $msg .= "</tr>";
                                $p++;
                        }
                        else {
                                $msg .= "<tr>";
                                    $msg .= "<td bgcolor=\"#FFC3A1\" align=\"center\" width=\"100%\">".$lselitens['descriresposta']."</td>";
                                $msg .= "</tr>";
                                $p++;
                        }
                        if($p == $econt['result']) {
                                $msg .= "<tr>";
                                    $msg .= "<td><textarea style=\"width:100%; height:30px;\">".$lselitens['obs']."</textarea></td>";
                                $msg .= "</tr>";
                           $msg .= "</table>";
                           $msg .= "</td>";
                           $msg .= "</tr>";
                        }
                        else {
                        }
                    }
                }
                if($coluna == "idmonitor") {
                    $msg .= "<td bgcolor=\"#CCCCCC\"><strong>MONITOR</strong></td>";
                    $msg .= "<td>".$eselmoni['nomemonitor']."</td>";
                }
                if($coluna != "idpergunta" && $coluna != "idrel_filtros" && $coluna != "datactt" && $coluna != "data" && $coluna != "idmonitoria" && $coluna != "idmonitor") {
                    $msg .= "<td bgcolor=\"#CCCCCC\"><strong>".strtoupper($coluna)."</strong></td>";
                    $msg .= "<td>".$eselmoni[$coluna]."</td>";
                }
                if($coluna != "idpergunta") {
                    $msg .= "</tr>";
                }
            }
            $msg .= "<tr><td colspan=\"2\"><textarea style=\"width:800px; height:200px;\">".$eselmoni['obs']."</textarea></td></tr>";
        }
        if($eselconf['tipo'] == "ALERTA") {
            $msg .= $eselconf['msg']."<p></p>";
            $msg .= "<table width=\"800\" border=\"0\">";
            $cols = explode(",",$eselconf['campo']);
            foreach($cols as $col) {
                $columns[] = $col;
            }
            $aliascamp = array('SUPER_OPER' => 'so','OPERADOR' => 'o','DADOS' => 'fg');
            $selcamp = "SELECT * FROM coluna_oper";
            $eselcamp = $_SESSION['query']($selcamp) or die ("erro na query de consulta dos campos do sistema");
            while($lselcamp = $_SESSION['fetch_array']($eselcamp)) {
                $campdados[] = $aliascamp[$lselcamp['incorpora']].".".$lselcamp['nomecoluna'];
            }
            $selmoni = "SELECT m.idmonitoria,m.idrel_filtros,m.data,m.qtdefg,m.qtdefgm,m.obs,fg.narquivo,".implode(",",$campdados)." FROM monitoria m
                        INNER JOIN operador o ON o.idoperador = m.idoperador
                        INNER JOIN super_oper so ON so.idsuper_oper = m.idsuper_oper
                        INNER JOIN fila_grava fg ON fg.idfila_grava = m.idfila
                        WHERE idmonitoria='$this->idmonitoria'";
            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta dos dados da monitoria");
            foreach($columns as $coluna) {
                $msg .= "<tr>";
                if($coluna == "idmonitoria") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>ID MONITORIA</strong></td>";
                    $msg .= "<td bgcolor=\"#FFFFFF\"><a href=\"http://".$_SESSION['site']."/inicio.php?menu=idmoni&idmoni=".$eselmoni['idmonitoria']."\">".$eselmoni['idmonitoria']."</a></td>";
                }
                if($coluna == "data" OR $coluna == "datactt") {
                    if($coluna == "data") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>DATA MONITORIA</strong></td>";
                        $msg .= "<td bgcolor=\"#FFFFFF\">".banco2data($eselmoni['data'])."</td>";
                    }
                    if($coluna == "datactt") {
                        $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>DATA CONTATO</strong></td>";
                        $msg .= "<td bgcolor=\"#FFFFFF\">".banco2data($eselmoni['datactt'])."</td>";
                    }
                }
                if($coluna == "idrel_filtros") {
                    $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>RELACIONAMENTO</strong></td>";
                    $msg .= "<td bgcolor=\"#FFFFFF\">".nomeapres($eselmoni['idrel_filtros'])."</td>";
                }
                if($coluna != "idpergunta" && $coluna != "idrel_filtros" && $coluna != "datactt" && $coluna != "data" && $coluna != "idmonitoria") {
                    $msg .= "<td bgcolor=\"#CCCCCC\"><strong>".strtoupper($coluna)."</strong></td>";
                    $msg .= "<td>".$eselmoni[$coluna]."</td>";
                }
                $msg .= "</tr>";
            }
            $msg .= "<tr><td colspan=\"2\"><textarea style=\"width:800px; height:200px;\">".$eselmoni['obs']."</textarea></td></tr>";
        }
        if($eselconf['tipo'] == "IMPRODUTIVA") {
             array('idrel_filtros','narquivo','datactt','horactt','idregmonitoria','idfila','nomemotivo','operador','super_oper',
                 'nomemonitor','data','hora','horaini','horafim','tmpaudio','gravacao');
             $tabelas = array("narquivo" => "fila_grava",
                              "datactt" => "fila_grava",
                              "horactt" => "fila_grava",
                              "idrel_filtros" => "regmonitoria",
                              "idregmonitoria" => "regmonitoria",
                              "idfila" => "fila_grava",
                              "nomemotivo" => "motivo",
                              "operador" => "operador",
                              "super_oper" => "super_oper",
                              "nomemonitor" => "monitor",
                              "data" => "regmonitoria",
                              "horaini" => "regmonitoria",
                              "horafim" => "regmonitoria",
                              "tmpaudio" => "regmonitoria",
                              "gravacao" => "regmonitoria");
             $msg .= $eselconf['msg']."<p></p>";
             $msg .= "<table width=\"800\" border=\"0\">";
             $cols = explode(",",$eselconf['campo']);
             foreach($cols as $col) {
                  $colsql[$col] = $alias->AliasTab($tabelas[$col]).".".$col;
             }
             $seldados = "SELECT ".implode(",",array_values($colsql)).",obscontest FROM regmonitoria ".$alias->AliasTab("regmonitoria")."
                         INNER JOIN operador ".$alias->AliasTab("operador")." ON ".$alias->AliasTab("operador").".idoperador = ".$alias->AliasTab("regmonitoria").".idoperador
                         LEFT JOIN super_oper ".$alias->AliasTab("super_oper")." ON ".$alias->AliasTab("super_oper").".idsuper_oper = ".$alias->AliasTab("operador").".idsuper_oper
                         INNER JOIN motivo ".$alias->AliasTab("motivo")." ON ".$alias->AliasTab("motivo").".idmotivo = ".$alias->AliasTab("regmonitoria").".idmotivo
                         INNER JOIN fila_grava ".$alias->AliasTab("fila_grava")." ON ".$alias->AliasTab("fila_grava").".idfila_grava = ".$alias->AliasTab("regmonitoria").".idfila
                         INNER JOIN contestreg ".$alias->AliasTab("contestreg")." ON ".$alias->AliasTab("contestreg").".idfila = ".$alias->AliasTab("fila_grava").".idfila_grava
                         WHERE idregmonitoria='$id'";
             $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die (mysql_error());
             foreach($colsql as $coluna => $sql) {
                  if(strstr($coluna, "data")) {
                       if($coluna == "data") {
                         $msg .= "<tr>";
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>DATA IMPRODUTIVA</strong></td>";
                              $msg .= "<td bgcolor=\"#FFFFFF\"><strong>".banco2data($eseldados[$coluna])."</strong></td>";
                         $msg .= "</tr>";
                       }
                       else {
                            $msg .= "<tr>";
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                              $msg .= "<td bgcolor=\"#FFFFFF\"><strong>".banco2data($eseldados[$coluna])."</strong></td>";
                            $msg .= "</tr>";
                       }
                  }
                  else {
                       if($coluna == "idrel_filtros") {
                            $msg .= "<tr>";
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>RELACIONAMENTO</strong></td>";
                              $msg .= "<td bgcolor=\"#FFFFFF\"><strong>".nomeapres($eseldados[$coluna])."</strong></td>";
                            $msg .= "</tr>";
                       }
                       else {
                         $msg .= "<tr>";
                              $msg .= "<td bgcolor=\"#CCCCCC\" width=\"200\"><strong>".strtoupper($coluna)."</strong></td>";
                              $msg .= "<td bgcolor=\"#FFFFFF\"><strong>".$eseldados[$coluna]."</strong></td>";
                         $msg .= "</tr>";
                       }
                  }
             }
             $msg .= "<tr><td colspan=\"2\"><textarea style=\"width:800px; height:200px;\">".$eseldados['obscontest']."</textarea></td></tr>";
        }
        $msg .= "</table>";
        $msg .= "</body>";
        $msg .= "</html>";
        $this->Body = $msg;
    }

    function config_to() {
        return $this->From;
    }

    function config_nometo() {
        return $this->FromName;
    }
}

?>
