<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Var_Calib_Pesq {
    public $meses;
    public $sqldatas;
    public $users;
    public $filtros;
    public $sqlfiltros;
    public $final;

    function Valores_Meses($ano) {
        $meses = array('JANEIRO' => '1', 'FEVEREIRO' => '2', 'MARÇO' => '3', 'ABRIL' => '4', 'MAIO' => '5','JUNHO' => '6','JULHO' => '7','AGOSTO' => '8','SETEMBRO' => '9', 'OUTUBRO' => '10','NOVEMBRO' => '11', 'DEZEMBRO' => '12');
        if($this->meses[0] == "") {
             $dataini = $ano."-01-01";
             $datafim = $ano."-12-31";
             $this->sqldatas[] = array($dataini,$datafim);
        }
        else {
            if(count($this->meses) == 1) {
                $mes = implode("",$this->meses);
                $dataini = $ano."-".$mes."-01";
                if($mes == "12") {
                    $ultdia = $ano."-".$mes."-31";
                }
                else {
                    $ultdia = (mktime(0,0,0,($mes+1),1,$ano) - 86400);
                    $ultdia = date("Y-m-d", $ultdia);
                }
                $datafim = $ultdia;
                $this->sqldatas[] = array($dataini, $datafim);
            }
            if(count($this->meses) > 1) {
                foreach($this->meses as $m) {
                    $mes = $m;
                    $dataini = $ano."-".$mes."-01";
                    if($mes == "12") {
                        $ultdia = $ano."-".$mes."-31";
                    }
                    else {
                        $ultdia = (mktime(0,0,0,($mes+1),1,$ano) - 86400);
                        $ultdia = date("Y-m-d", $ultdia);
                    }
                    $datafim = $ultdia;
                    $this->sqldatas[] = array($dataini, $datafim);
                }
            }
        }
    }

    function Valores_Filtros() {
        $selfiltro = "SELECT idfiltro_nomes, nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
        $efiltro = $_SESSION['query']($selfiltro) or die ("erro na query de consulta dos nomes dos filtros ativos");
        while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
            if(isset($_POST['pesq'])) {
                if($_POST["filtro_".strtolower($lfiltros['nomefiltro_nomes'])] != "") {
                    $filtros["id_".strtolower($lfiltros['nomefiltro_nomes'])] = $_POST["filtro_".strtolower($lfiltros['nomefiltro_nomes'])];
                }
                else {
                }
            }
            if($_GET['filtros'] != "" AND !isset($_POST['pesq'])) {
                $filtros = explode(",",$_GET['filtros']);
            }
            if($_GET['filtros'] == "" AND !isset($_POST['pesq'])) {
                $filtros = "";
            }
        }
        if($filtros != "") {
            if($_POST['pesq']) {
                foreach($filtros as $nome => $value) {
                    $sqlwhere[] = $nome."='".$value."'";
                }
            }
            else {
                $sqlwhere = explode(",", $_GET['filtros']);
            }
            $this->sqlfiltros = implode(",",$sqlwhere);
            $sqlwhere = implode(" AND ", $sqlwhere);
            $selrel = "SELECT idrel_filtros FROM rel_filtros WHERE $sqlwhere";
            $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relacionamentos");
            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                $this->filtros[] = $lselrel['idrel_filtros'];
            }
        }
        else {
            $this->filtros = "";
        }
    }

    function Monta_Tab_Pesq($eagenda, $pagcalib, $finalpesq, $ano) {
        ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $("input[id*='eletacalib']").click(function() {
                    var idcalib = $(this).attr('name');
                    var apagar = confirm("Voce deseja apagar esta calibragem!!!");
                    if (apagar == true) {
                        window.location = '/monitoria_supervisao/users/delcalib.php?idcalib='+idcalib;
                    }
                    else {
                        return false;
                    }
                })
            })
        </script>
        <?php
        $data = date('Y-m-d');
        while($lagenda = $_SESSION['fetch_array']($eagenda)) {
          $datafim = $lagenda['datafim'];
        $status = "SELECT SUM(CASE finalizado WHEN 'N' THEN 1 WHEN 'S' THEN 0 END) as soma FROM agcalibragem WHERE idagcalibragem='".$lagenda['idagcalibragem']."'";
        $estatus = $_SESSION['fetch_array']($_SESSION['query']($status)) or die ("erro na query para definicao do status da calibragem");
        if($estatus['soma'] == 0) {
                $final = "FINALIZADO";
                $bgcolor = "#99CCFF";
                $status = "S";
        }
        if($estatus['soma'] >= 1) {
            $final = "EM ABERTO";
            $status = "N";
            $verifdata = "SELECT (adddate($data,-1) <= '$datafim') as result1,('$data' < adddate('$datafim',-2)) as result2,('$data' > '$datafim') as result3";
            $everifdata = $_SESSION['fetch_array']($_SESSION['query']($verifdata)) or die ('erro na verificação da data');
            if($everifdata['result1'] == 1) {
                $bgcolor = "#FFFFCC";
            }
            if($everifdata['result3'] == 1) {
                $bgcolor = "#EF9294";
            }
            if($everifdata['result2'] == 1) {
               $bgcolor = "#FFFFFF";
            }
        }
        if($finalpesq == "") {
            $verif = "OK";
        }
        if($finalpesq != "") {
            if($status == $finalpesq) {
                $verif = "OK";
            }
            else {
                $verif = "NOK";
            }
        }
        if($verif == "NOK") {
        }
        else {
          echo "<tr id=\"".$final."\" bgcolor=\"".$bgcolor."\" onMouseOver=\"javascript:this.style.backgroundColor='#CCCCCC'\" onMouseOut=\"javascript:this.style.backgroundColor='$bgcolor'\">";
              if($pagcalib == "estatistica") {
                  $link = "<a onclick=\"javascript:estatistica('users/visuestatistica.php?tipoestatistica=pcalibragem&calib=".$lagenda['idagcalibragem']."','1024','768');\" style=\"text-decoration:none; color:#000\">";
              }
              if($pagcalib == "agenda") {
                  $link = "<a href=\"inicio.php?menu=calibragem&opmenu=AGENDA&submenu=OK&calib=".$lagenda['idagcalibragem']."&meses=".implode(",",$this->meses)."&usuarios=".$this->users."&ano=".$ano."&filtros=".$this->sqlfiltros."&final=".$finalpesq."#anc".$lagenda['idagcalibragem']."\" name=\"anc".$lagenda['idagcalibragem']."\" style=\"text-decoration:none; color:#000\">";
              }
                echo "<th align=\"center\">$link".$lagenda['idagcalibragem']."</a></th>";
                echo "<th align=\"center\">$link".banco2data($lagenda['dataini'])."</a></th>";
                echo "<th align=\"center\">$link".banco2data($lagenda['datafim'])."</a></th>";
                $qtdeusers = "SELECT COUNT(*) as result FROM agcalibragem WHERE idagcalibragem='".$lagenda['idagcalibragem']."'";
                $eqtdeusers = $_SESSION['fetch_array']($_SESSION['query']($qtdeusers)) or die ("erro na query de consulta da quantidade de usuários relacionados a calibragem");
                $qtde = $eqtdeusers['result'];
                echo "<th align=\"center\">$link".$qtde."</a></th>";
                $part = explode("-",$lagenda['usercont']);
                $agenda = explode("-",$lagenda['useragenda']);
                $iduser = $part[0];
                $tabuser = $part[1];
                $selnome = "SELECT nome$tabuser FROM $tabuser WHERE id$tabuser='$iduser'";
                $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die ("erro na query de consulta do nome do usuáriode controle");
                $selnagenda = "SELECT nome$agenda[1] FROM $agenda[1] WHERE id$agenda[1]='$agenda[0]'";
                $eselagenda = $_SESSION['fetch_array']($_SESSION['query']($selnagenda)) or die ("erro na query de consulta do nome do usuários que agendou a calibragem");
                echo "<th align=\"center\">$link".$eselnome['nome'.$tabuser]."</a></th>";
                echo "<th align=\"center\">$link".$eselagenda['nome'.$agenda[1]]."</a></th>";
                echo "<th align=\"center\">$link".  nomevisu($lagenda['idrel_filtros'])."</a></td>";
                echo "<th align=\"center\">$link".$lagenda['idmonitoria']."</a></th>";
                echo "<th align=\"center\">$link".$final."</a></th>";
                if($pagcalib == "estatistica") {
                }
                else {
                    if($_SESSION['participante'] == $lagenda['useragenda']) {
                        echo "<th align=\"center\"><input type=\"button\" style=\"border:0px;width:15px;height:15px;background-image:url(/monitoria_supervisao/images/exit.png)\" name=\"".$lagenda['idagcalibragem']."\" id=\"deletacalib\"></th>";
                    }
                    else {
                    }
                }
        echo "</tr>";
        }
    }
  }
}

?>
