<?php

/* classe que cria a query para executar um relatório na área de monitoria*/

class RelatorioMoni {

  /* parametros enviados pela pagina monitoria.php*/

  public $variaveis;
  public $nomerel;
  public $idrel;
  public $relant;
  public $periodo;
  public $tabpri;
  public $tipo;
  public $colunas;
  public $vincrel;
  public $relatu;

  /* campos que formam o select do relatório*/
  public $sqlwhere;
  public $nomesapres;
  public $alias;
  public $tabela;
  public $colselect;
  public $nomescol;
  public $ordena;
  public $sqlinner;
  public $sqltabinner;
  public $sqlinner1;
  public $sqlgroup;
  public $vinccoluna;
  public $count;
  public $varant;
  public $idmonitor;
  public $innerant;
  public $whereant;
  public $inner;
  public $where;
  /* -----*/

      function setvarinterno () {
	if(isset($this->nomerel)) {
	  $selrelmoni = "SELECT * FROM relmoni WHERE nome='".$this->nomerel."'";
	  $eselrelmoni = $_SESSION['fetch_array']($_SESSION['query']($selrelmoni)) or die ("erro na query de consulta do relatório");
	}
	if(isset($this->idrel)) {
	  $selrelmoni = "SELECT * FROM relmoni WHERE id='".$this->idrel."'";
	  $eselrelmoni = $_SESSION['fetch_array']($_SESSION['query']($selrelmoni)) or die ("erro na query de consulta do relatório");
          $this->nomerel = $eselrelmoni['nome'];
	}
	$this->tabpri = $eselrelmoni['tabpri'];
	$this-> tipo = $eselrelmoni['tipo'];
	$this-> coluna = $eselrelmoni['qtdecol']; 
	$this-> vincrel = $eselrelmoni['vincrel'];
	$this-> relatu = $eselrelmoni['id'];
      }

      /* função que identifica as variáveis enviadas pelos combos e input e coloca os valores no objeto $this->sqlwhere para montar a parte do WHERE da query final */
      function setavar($variaveis) {
          $countcheck = 0;
          foreach($variaveis as $checkvar) {
              if($this->tabpri == "regmonitoria" && (eregi("fg", $checkvar) || eregi("fgm",$checkvar) || eregi("plan",$checkvar))) {
                  $checkvar = explode("=",$checkvar);
                  if($checkvar[1] != "") {
                      $countcheck++;
                  }
                  if($countcheck >= 1){
                    return 1;
                  }
                  else {
                  }
              }
              else {
              }
          }
          $this->inner = array();
          $this->sqlinner1 = array();
          $this->sqlwhere = array();
          foreach($variaveis as $var) {
              if(eregi("ID",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {
                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $ID = "monitoria.idmonitoria='".$valor[1]."'";
                      }
                      if($this->tabpri == "regmonitoria") {
                          $ID = "regmonitoria.idreg='".$valor[1]."'";
                      }
                  }
              }
              if(eregi("dtinimoni",$var)) {
                  $valor = explode("=",$var);
                  $dtinimoni = $valor[1];
              }
              if(eregi("dtfimmoni",$var)) {
                  $valor = explode("=",$var);
                  $dtfimmoni = $valor[1];
              }
              if(eregi("dtinictt",$var)) {
                  $valor = explode("=",$var);
                  $dtinictt = $valor[1];
              }
              if(eregi("dtfimctt",$var)) {
                  $valor = explode("=",$var);
                  $dtfimctt = $valor[1];
              }
              if(eregi("filtro_",$var)) {
                  $selfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes";
                  $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta dos nomes dos filtros");
                  while($lselfiltros = $_SESSION['fetch_array']($eselfiltros)) {
                    if(eregi("filtro_".strtolower($lselfiltros['nomefiltro_nomes']),$var)) {
                        $valor = explode("=",$var);
                        if($valor[1] == "") {

                        }
                        else {
                            $countrel = 0;
                            if(isset($this->nomerel)) {
                                $selrel = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.nome='".$this->nomerel."' ORDER BY relmonicol.posicao";
                            }
                            if(isset($this->idrel)) {
                                $selrel = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.id='".$this->idrel."' ORDER BY relmonicol.posicao";
                            }
                            $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relatório");
                            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                                if($lselrel['tabpri'] == "monitoria" && $lselrel['tipo'] == "SINTETICO" && $lselrel['tabela'] == "rel_filtros") {
                                    $countrel++;
                                }
                                else {
                                }
                            }
                            if($countrel >= 1) {
                            }
                            else {
                                if(in_array("INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = monitoria.idrel_filtros", $this->sqlinner1) OR in_array("INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = regmonitoria.idrel_filtros", $this->sqlinner)) {
                                    $coldados = explode("=",$var);
                                    $coldados = str_replace("filtro_","id_", trim($coldados[0]));
                                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner) OR key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                                            $valinner = $this->sqlinner['INNER JOIN filtro_dados']." AND rel_filtros.".$coldados."";
                                        }
                                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                                            $valinner = $this->sqlinner1['INNER JOIN filtro_dados']." AND rel_filtros.".$coldados."";
                                        }
                                        $innerdados = array("INNER JOIN filtro_dados" => $valinner);
                                        $this->sqlinner1 = array_replace($this->sqlinner1, $innerdados);
                                    }
                                    else {
                                        $this->sqlinner1['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$coldados."";
                                    }
                                }
                                else {
                                    if($this->tabpri == "monitoria") {
                                        $tabinner = "monitoria";
                                    }
                                    if($this->tabpri == "regmonitoria") {
                                        $tabinner = "regmonitoria";
                                    }
                                    if(key_exists("INNER JOIN rel_filtros", $this->sqlinner1)) {
                                    }
                                    else {
                                        $this->sqlinner1['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = $tabinner.idrel_filtros";
                                    }
                                    $coldados = explode("=",$var);
                                    $coldados = str_replace("filtro_","id_", trim($coldados[0]));
                                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner) OR key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                                            $valinner = $this->sqlinner['INNER JOIN filtro_dados']." AND rel_filtros.".$coldados."";
                                        }
                                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                                            $valinner = $this->sqlinner1['INNER JOIN filtro_dados']." AND rel_filtros.".$coldados."";
                                        }
                                        $innerdados = array("INNER JOIN filtro_dados" => $valinner);
                                        $this->sqlinner1 = array_replace($this->sqlinner1, $innerdados);
                                    }
                                    else {
                                        $this->sqlinner1['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$coldados."";
                                    }
                                }
                                if(key_exists("INNER JOIN filtro_dados", $this->inner)) {
                                    $valinner = $this->inner['INNER JOIN filtro_dados']." AND rel_filtros.".$coldados."";
                                    $innerdados = array("INNER JOIN filtro_dados" => $valinner);
                                    $this->inner = array_replace($this->inner, $innerdados);
                                }
                                else {
                                    $this->inner['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$coldados."";
                                }
                                if(key_exists("INNER JOIN rel_filtros", $this->inner)) {
                                }
                                else {
                                    $this->inner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = $tabinner.idrelfiltro";
                                }

                            }
                            $this->sqlwhere[] = "rel_filtros.".str_replace("filtro_","id_",$valor[0])."='".$valor[1]."'";
                        }
                    }
                  }
              }
              if($var == "fg=fg") {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {
                  }
                  else {
                      if(key_exists("INNER JOIN moniavalia", $this->sqlinner1)) {
                      }
                      else {
                            $this->sqlinner1['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = monitoria.idmonitoria";
                      }
                      $this->sqlwhere[] = "moniavalia.avalia='FG'";
                      if(key_exists("INNER JOIN moniavalia", $this->inner)) {
                      }
                      else {
                          $this->inner['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = monitoria.idmonitoria";
                      }
                  }
              }
              if($var == "fgm=fgm") {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if(key_exists("INNER JOIN moniavalia",$this->sqlinner1)) {
                      }
                      else {
                        $this->sqlinner1['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = monitoria.idmonitoria";
                      }
                      $this->sqlwhere[] = "moniavalia.avalia='FGM'";
                      if(key_exists("INNER JOIN moniavalia", $this->inner)) {
                      }
                      else {
                          $this->inner['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = monitoria.idmonitoria";
                      }
                  }
              }
              if(eregi("super",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $tsuper = "monitoria";
                      }
                      if($this->tabpri == "regmonitoria") {
                          $tsuper = "regmonitoria";
                      }
                      if(key_exists("INNER JOIN super_oper",$this->sqlinner1)) {
                      }
                      else {
                        $this->sqlinner1['INNER JOIN super_oper'] = "INNER JOIN super_oper ON super_oper.idsuper_oper = $tsuper.idsuper";
                      }
                      $this->sqlwhere[] = "super_oper.supervisor LIKE '%$valor[1]%'";
                      if(key_exists("INNER JOIN super_oper", $this->inner)) {
                      }
                      else {
                          $this->inner['INNER JOIN super_oper'] = "INNER JOIN super_oper ON super_oper.idsuper_oper = $tsuper.idsuper";
                      }
                  }
              }
              if(eregi("oper",$var) && !eregi("filtro_",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $toper = "monitoria";
                      }
                      if($this->tabpri == "regmonitoria") {
                          $toper = "regmonitoria";
                      }
                      if(key_exists("INNER JOIN operador",$this->sqlinner1)) {
                      }
                      else {
                          if(isset($this->nomerel)) {
                            $selcol = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.nome='".$this->nomerel."' AND posicao='1'";
                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta das colunas do relatório");
                          }
                          if(isset($this->idrel)) {
                            $selcol = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.id='".$this->idrel."' AND posicao='1'";
                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta das colunas do relatório");
                          }
                          while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                              $coluna = $lselcol['coluna'];
                          }
                          if($this->tipo == "SINTETICO" && $coluna == "idoper") {
                          }
                          else {
                              $this->sqlinner1['INNER JOIN operador'] = "INNER JOIN operador ON operador.idoperador = $toper.idoperador";
                          }
                      }
                      if(key_exists("INNER JOIN operador", $this->inner)) {
                      }
                      else {
                          if(isset($this->nomerel)) {
                            $selcol = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.nome='".$this->nomerel."' AND posicao='1'";
                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta das colunas do relatório");
                          }
                          if(isset($this->idrel)) {
                            $selcol = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.id='".$this->idrel."' AND posicao='1'";
                            $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta das colunas do relatório");
                          }
                          while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                              $coluna = $lselcol['coluna'];
                          }
                          if($this->tipo == "SINTETICO" && $coluna == "idoper") {
                          }
                          else {
                            $this->inner['INNER JOIN operador'] = "INNER JOIN operador ON operador.idoperador = $toper.idoperador";
                          }
                      }
                      $this->sqlwhere[] = "operador.operador LIKE '%$valor[1]%'";
                  }
              }
              if(eregi("monitor",$var)) {
                  $valor = explode("=",$var);
                  if($this->tabpri == "monitoria") {
                      $tinner = "monitoria";
                  }
                  if($this->tabpri == "regmonitoria") {
                      $tinner = "regmonitoria";
                  }
                  if($valor[1] == "") {

                  }
                  else {
                      if(key_exists("INNER JOIN monitor", $this->sqlinner1)) {
                      }
                      else {
                          if($this->tabpri == "monitor") {
                          }
                          else {
                            $this->sqlinner1['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = $tinner.monitor";
                          }
                      }
                      if(key_exists("INNER JOIN monitor", $this->inner)) {
                      }
                      else {
                          $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = $tinner.monitor";
                      }
                      $this->sqlwhere[] = "monitor.nomemonitor LIKE '%$valor[1]%'";
                  }
              }
              if(eregi("plan",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          if(key_exists("INNER JOIN moniavalia", $this->sqlinner1)) {
                          }
                          else {
                            $this->sqlinner1['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = monitoria.idmonitoria";
                          }
                          $this->sqlwhere[] = "moniavalia.idplanilha='".$valor[1]."'";
                      }
                      if(key_exists("INNER JOIN moniavalia", $this->inner)) {
                      }
                      else {
                          $this->inner['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = monitoria.idmonitoria";
                      }
                  }
              }
              if(eregi("grupo",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $this->sqlwhere[] = "moniavalia.idgrupo='".$valor[1]."'";
                      }
                  }
              }
              if(eregi("sub",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $this->sqlwhere[] = "moniavalia.idsubgrupo='".$valor[1]."'";
                      }
                  }
              }
              if(eregi("perg",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $vperg = "SELECT COUNT(*) as result FROM subgrupo WHERE idpergunta='$valor[1]'";
                          $evperg = $_SESSION['fetch_array']($_SESSION['query']($vperg)) or die ("erro na query de consulta do subgrupo");
                          if($evperg['result'] >= 1) {
                              $this->sqlwhere[] = "moniavalia.idpergunta='".$valor[1]."'";
                          }
                          else {
                              $this->sqlwhere[] = "moniavalia.idpergunta='".$valor[1]."'";
                          }
                      }
                  }
              }
              if(eregi("resp",$var)) {
                  $valor = explode("=",$var);
                  if($valor[1] == "") {

                  }
                  else {
                      if($this->tabpri == "monitoria") {
                          $vperg = "SELECT COUNT(*) as result FROM subgrupo WHERE idpergunta='$valor[1]'";
                          $evperg = $_SESSION['fetch_array']($_SESSION['query']($vperg)) or die ("erro na query de consulta do subgrupo");
                          if($evperg['result'] >= 1) {
                              $this->sqlwhere[] = "moniavalia.idresposta='".$valor[1]."'";
                          }
                          else {
                              $this->sqlwhere[] = "moniavalia.idresposta='".$valor[1]."'";
                          }
                      }
                  }
              }
          }
          if($this->varant == "data") {
              array_unshift($this->sqlwhere, $this->whereant);
          }
          if($this->varant == "datactt") {
              array_unshift($this->sqlwhere, $this->whereant);
          }
          if($dtinimoni == "" AND $dtfimmoni == "" && $this->varant != "data") {
              $seldata = "SELECT dataini, datafim FROM periodo WHERE idperiodo='".$this->periodo."'";
              $eseldata = $_SESSION['fetch_array']($_SESSION['query']($seldata)) or die ("erro na query de seleção de datas");
              array_unshift($this->sqlwhere, $this->tabpri.".data BETWEEN '".$eseldata['dataini']."' AND '".$eseldata['datafim']."'");
          }
          if($dtinimoni != "" AND $dtfimmoni != "" && $this->varant != "data") {
              array_unshift($this->sqlwhere, $this->tabpri.".data BETWEEN '".$dtinimoni."' AND '".$dtfimmoni."'");
          }
          if($_POST['dtinictt'] == "" AND $_POST['dtfimctt'] == "") {
          }
          if($_POST['dtinictt'] != "" AND $_POST['dtfimctt'] != "") {
              if(key_exists("INNER JOIN fila_grava", $this->sqlinner1)) {
              }
              else {
                  if($this->tabpri == "monitoria") {
                      $tabdt = "monitoria";
                  }
                  if($this->tabpri == "regmonitoria") {
                      $tabdt = "regmonitoria";
                  }
                $this->sqlinner1['INNER JOIN fila_grava'] = "INNER JOIN fila_grava ON fila_grava.idfila_grava = $tabdt.idfila";
              }
              array_unshift($this->sqlwhere, "fila_grava.data_contato BETWEEN '".data2banco($_POST['dtinictt'])."' AND '".data2banco($_POST['dtfimctt'])."'");
          }

          /* caso o ID tenha sido informado limpa as demais variáis do objeto $this->sqlwhere */
          if($ID == "") {
          }
          else {
              $this->sqlwhere = array();
              $this->sqlwhere[] = $ID;
          }
          $variaveis = implode("*",$variaveis);
          return $variaveis;
     }
      
    function estruturasql () {
      $this->sqlinner = array();
      $this->colselect = array();
      $this->nomesapres = array();
      $this->nomescol = array();
      if(isset($this->nomerel)) {
	$selrela = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.nome='".$this->nomerel."' ORDER BY relmonicol.posicao";
	$eselrela = $_SESSION['query']($selrela) or die ("erro na query de consulta das colunas do relatório");
      }
      if(isset($this->idrel)) {
	$selrela = "SELECT * FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.id='".$this->idrel."' ORDER BY relmonicol.posicao";
	$eselrela = $_SESSION['query']($selrela) or die ("erro na query de consulta das colunas do relatório");
        $dadosrelatnt = "SELECT * FROM relmoni INNER JOIN relmonicol ON relmonicol.idrelmoni = relmoni.id WHERE relmoni.id='".$this->relant."' AND tabela<>'DADOS'";
        $erelant = $_SESSION['fetch_array']($_SESSION['query']($dadosrelatnt)) or die ("erro na query para consulta os dados do relatório anterior");
        $tipoant = $erelant['tipo'];
        $tabant = $erelant['tabela'];
        $colant = $erelant['coluna'];
      }
	while($lselrela = $_SESSION['fetch_array']($eselrela)) {
	  if($lselrela['coluna'] == "QTDE" OR $lselrela['coluna'] == "PERC") {
	    if(in_array("idmoni",$this->nomescol) OR in_array("idreg",$this->nomescol)) {
	    }
	    else {
	      $this->nomesapres[] = $lselrela['nomeapres'];
	    }
	  }
	  else {
	    $tab = $lselrela['tabela'];
	    $col = $lselrela['coluna'];
	    //if($lselrela['ordena'] == "S") {
	      //$this->ordena = "ODER BY ".$lselrel['coluna']."";
	    //}
	    //else {
	    //}
	    if($this->tipo == "ANALITICO") {
              if($this->tabpri == "monitoria") {
                  $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
              }
              if($this->tabpri == "regmonitoria") {
                  $this->count = "COUNT(DISTINCT(regmonitoria.idreg))";
              }
	      if($tab == "monitoria") {
		if($col == "idmonitoria") {
		  array_unshift($this->colselect,"DISTINCT(".$tab.".".$col.")");
		  $this->nomescol[] = $col;
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "idoperador") {
                    if(key_exists("INNER JOIN operador", $this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "operador") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN operador'] = "INNER JOIN operador ON operador.idoperador = ".$this->tabpri.".idoperador";
                        }
                    }
		    $this->colselect[] = "operador.operador as operador".substr($tab,0,4);
		    $this->nomescol[] = "operador".substr($tab,0,4);
		    $this->nomesapres[] = $lselrela['nomeapres'];
                    if(key_exists("INNER JOIN operador", $this->inner)) {
                    }
                    else {
                        if($this->tabpri == "operador") {
                        }
                        else {
                            $this->inner['INNER JOIN operdor'] = "INNER JOIN operador ON operador.idoperador = ".$this->tabpri.".idoperador";
                        }
                    }
		}
		if($col == "idsuper_oper") {
                    if(key_exists("INNER JOIN super_oper",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "super_oper") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN super_oper'] = "INNER JOIN super_oper ON super_oper.idsuper_oper = ".$this->tabpri.".idsuper_oper";
                        }
                    }
		    $this->colselect[] = "super_oper.supervisor as super".substr($tab,0,4);
		    $this->nomescol[] = "super".substr($tab,0,4);
		    $this->nomesapres[] = $lselrela['nomeapres'];
                    //$this->inner
                    if(key_exists("INNER JOIN super_oper",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "super_oper") {
                        }
                        else {
                            $this->inner['INNER JOIN super_oper'] = "INNER JOIN super_oper ON super_oper.idsuper_oper = ".$this->tabpri.".idsuper_oper";
                        }
                    }
		}
		if($col == "idmonitor") {
                    if(key_exists("INNER JOIN monitor",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitor") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = ".$this->tabpri.".idmonitor";
                        }
                    }
                    $this->colselect[] = "monitor.nomemonitor as ".$col.substr($tab,0,4);
                    $this->nomescol[] = $col.substr($tab,0,4);
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    //$this->inner
                    if(key_exists("INNER JOIN monitor",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "monitor") {
                        }
                        else {
                            $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = ".$this->tabpri.".idmonitor";
                        }
                    }
		}
                if($col == "data") {
                    $this->colselect[] = "monitoria.data as ".$col.substr($tab,0,4);
                    $this->nomescol[] = $col.substr($tab,0,4);
                    $this->nomesapres[] = $lselrela['nomeapres'];
                }
                if($col == "horaini") {
                    $this->colselect[] = "monitoria.horaini as ".$col.substr($tab,0,4);
                    $this->nomescol[] = $col.substr($tab,0,4);
                    $this->nomesapres[] = $lselrela['nomeapres'];
                }
	      }
	      if($tab == "rel_filtros") {
		if($this->tabpri == "monitoria") {
                    if(key_exists("INNER JOIN rel_filtros",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "rel_filtros") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = monitoria.idrel_filtros";
                        }
                    }
                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner) OR key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                            $valinner = $this->sqlinner['INNER JOIN filtro_dados']." AND rel_filtros.".$col."";
                        }
                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                            $valinner = $this->sqlinner1['INNER JOIN filtro_dados']." AND rel_filtros.".$col."";
                        }
                        $innerdados = array("INNER JOIN filtro_dados" => $valinner);
                        $this->sqlinner = array_replace($this->sqlinner, $innerdados);
                    }
                    else {
                        if($this->tabpri == "filtro_dados") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$col."";
                        }
                    }
                    //$this->inner
                    if(key_exists("INNER JOIN rel_filtros",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "rel_filtros") {
                        }
                        else {
                            $this->inner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = monitoria.idrel_filtros";
                        }
                    }
		}
		if($this->tabpri == "regmonitoria") {
                    if(key_exists("INNER JOIN rel_filtros",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "rel_filtros") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = regmonitoria.idrel_filtros";
                        }
                    }
                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner) OR key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                            $valinner = $this->sqlinner['INNER JOIN filtro_dados']." AND rel_filtros.".$col."";
                        }
                        if(key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                            $valinner = $this->sqlinner1['INNER JOIN filtro_dados']." AND rel_filtros.".$col."";
                        }
                        $innerdados = array("INNER JOIN filtro_dados" => $valinner);
                        $this->sqlinner = array_replace($this->sqlinner, $innerdados);
                    }
                    else {
                        if($this->tabpri == "filtro_dados") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$col."";
                        }
                    }
                    //$this->inner
                    if(key_exists("INNER JOIN rel_filtros",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "rel_filtros") {
                        }
                        else {
                            $this->inner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = regmonitoria.idrel_filtros";
                        }
                    }
		}
		$tab = "filtro_dados";
		$this->colselect[] = $tab.".nomefiltro_dados as ".str_replace("id_","",$col).substr($tab,0,4);
		$this->nomescol[] = str_replace("id_","",$col).substr($tab,0,4);
		$this->nomesapres[] = $lselrela['nomeapres'];
                //$this->inner
                if(key_exists("INNER JOIN filtro_dados", $this->inner)) {
                }
                else {
                    if($this->tabpri == "filtro_dados") {
                    }
                    else {
                        $this->inner['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$col."";
                    }
                }
	      }
	      if($tab == "monitoria_fluxo") {
                  if(key_exists("INNER JOIN monitoria_fluxo",$this->sqlinner)) {
                  }
                  else {
                      if($this->tabpri == "monitoria_fluxo") {
                      }
                      else {
                            $this->sqlinner['INNER JOIN monitoria_fluxo'] = "INNER JOIN monitoria_fluxo ON monitoria_fluxo.idmonitoria = ".$this->tabpri.".idmoni";
                      }
                  }
                  //$this->inner
                  if(key_exists("INNER JOIN monitoria_fluxo",$this->inner)) {
                  }
                  else {
                      if($this->tabpri == "monitoria_fluxo") {
                      }
                      else {
                          $this->inner['INNER JOIN monitoria_fluxo'] = "INNER JOIN monitoria_fluxo ON monitoria_fluxo.idmonitoria = ".$this->tabpri.".idmoni";
                      }
                  }
		if($col == "iduser") {
		  if($lselrela['tipouser'] == "monitor") {
                      if(key_exists("INNER JOIN monitor",$this->sqlinner)) {
                      }
                      else {
                          if($this->tabpri == "monitor") {
                          }
                          else {
                                $this->sqlinner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = monitoria_fluxo.iduser";
                          }
                      }
                      $tab = "monitor";
                      $this->colselect[] = $tab.".nomemonitor as ".$col.substr($tab,0,4);
                      $this->nomescol[] = $col.substr($tab,0,4);
                      $this->nomesapres[] = $lselrela['nomeapres'];
                      //$this->inner
                      if(key_exists("INNER JOIN monitor",$this->inner)) {
                      }
                      else {
                          if($this->tabpri == "monitor") {
                          }
                          else {
                                $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = monitoria_fluxo.iduser";
                          }
                      }
		  }
		  if($lselrela['tipouser'] == "user_adm") {
                      if(key_exists("INNER JOIN user_adm",$this->sqlinner)) {
                      }
                      else {
                          if($this->tabpri == "user_adm") {
                          }
                          else {
                                $this->sqlinner['INNER JOIN user_adm'] = "INNER JOIN user_adm ON user_adm.iduser_adm = monitoria_fluxo.iduser";
                          }
                      }
                      $tab = "user_adm";
                      $this->colselect[] = $tab.".nomeuser_adm as ".$col.substr($tab,0,4);
                      $this->nomescol[] = $col.substr($tab,0,4);
                      $this->nomesapres[] = $lselrela['nomeapres'];
                      //$this->inner
                      if(key_exists("INNER JOIN user_adm",$this->inner)) {
                      }
                      else {
                           if($this->tabpri == "user_adm") {
                          }
                          else {
                            $this->inner['INNER JOIN user_adm'] = "INNER JOIN user_adm ON user_adm.iduser_adm = monitoria_fluxo.iduser";
                          }
                      }
		  }
		  if($lselrela['tipouser'] == "user_web") {
                      if(key_exists("INNER JOIN user_web",$this->sqlinner)) {
                      }
                      else {
                          if($this->tabpri == "user_web") {
                          }
                          else {
                                $this->sqlinner['INNER JOIN user_web'] = "INNER JOIN user_web ON user_web.iduser_web = monitoria_fluxo.iduser";
                          }
                      }
                      $tab = "user_web";
                      $this->colselect[] = $tab.".nomeuser_web as ".$col.substr($tab,0,4);
                      $this->nomescol[] = $col.substr($tab,0,4);
                      $this->nomesapres[] = $lselrela['nomeapres'];
                      //$this->inner
                      if(key_exists("INNER JOIN user_web",$this->inner)) {
                      }
                      else {
                          if($this->tabpri == "user_web") {
                          }
                          else {
                            $this->inner['INNER JOIN user_web'] = "INNER JOIN user_web ON user_web.iduser_web = monitoria_fluxo.iduser";
                          }
                      }
		  }
		}
		if($col == "idstatus") {
                    if(key_exists("INNER JOIN status",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "status") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN status'] = "INNER JOIN status ON status.id = monitoria_fluxo.idstatus";
                        }
                    }
                    $this->colselect[] = $tab.".nome as status".substr($tab,0,4);
                    $this->nomescol[] = "status".substr($tab,0,4);
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    //$this->inner
                    if(key_exists("INNER JOIN status",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "status") {
                        }
                        else {
                            $this->inner['INNER JOIN status'] = "INNER JOIN status ON status.id = monitoria_fluxo.idstatus";
                        }
                    }
		}
		if($col == "iddefinicao") {
                    if(key_exists("INNER JOIN definicao",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "definicao") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN definicao'] = "INNER JOIN definicao ON definicao.iddefinicao = monitoria_fluxo.iddefinicao";
                        }
                    }
                    $this->colselect[] = $tab.".nome as definicao".substr($tab,0,4);
                    $this->nomescol[] = "definicao".substr($tab,0,4);
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    //$this->inner
                    if(key_exists("INNER JOIN definicao",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "definicao") {
                        }
                        else {
                            $this->inner['INNER JOIN definicao'] = "INNER JOIN definicao ON definicao.iddefinicao = monitoria_fluxo.iddefinicao";
                        }
                    }
		}
		if($col == "data") {
		  $this->colselect[] = $tab.".".$col." as datastatus".substr($tab,0,4);
		  $this->nomescol[] = "datastatus".substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "hora") {
		  $this->colselect[] = $tab.".".$col." as horastatus".substr($tab,0,4);
		  $this->nomescol[] = "horastatus".substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "obs") {
		  $this->colselect[] = $tab.".".$col." as obsstatus".substr($tab,0,4);
		  $this->nomescol[] = "obsstatus".substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
	      }
	      if($tab == "moniavalia") {
		if($col == "idmonitoria") {
		  array_unshift($this->colselect,"DISTINCT(".$this->tabpri.".".$col.")");
		  $this->nomescol[] = $col;
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "idaval") {
		  $this->colselect[] = "aval_plan.nomeaval_plan as aval".substr($tab,0,4);
		  $this->nomescol[] = "aval".substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "valor_final_aval") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
                if(key_exists("INNER JOIN moniavalia",$this->sqlinner)) {
                }
                else {
                    if($this->tabpri == "moniavalia") {
                    }
                    else {
                        $this->sqlinner['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = ".$this->tabpri.".idmoni";
                    }
                }
                if(key_exists("LEFT JOIN aval_plan",$this->sqlinner)) {
                }
                else {
                    if($this->tabpri == "aval_plan") {
                    }
                    else {
                        $this->sqlinner['LEFT JOIN aval_plan'] = "LEFT JOIN aval_plan ON aval_plan.idaval_plan = moniavalia.idaval";
                    }
                }
                //$this->inner
                if(key_exists("INNER JOIN moniavalia",$this->inner)) {
                }
                else {
                    if($this->tabpri == "moniavalia") {
                    }
                    else {
                        $this->inner['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ON moniavalia.idmonitoria = ".$this->tabpri.".idmoni";
                    }
                }
                if(key_exists("LEFT JOIN aval_plan",$this->inner)) {
                }
                else {
                    if($this->tabpri == "aval_plan") {
                    }
                    else {
                        $this->inner['LEFT JOIN aval_plan'] = "LEFT JOIN aval_plan ON aval_plan.idaval_plan = moniavalia.idaval";
                    }
                }
	      }
	      if($tab == "planilha") {
		if(in_array("DISTINCT(".$this->tabpri.".idmoni)",$this->colselect)) {
		}
		else {
		  array_unshift($this->colselect,"DISTINCT(".$this->tabpri.".idmoni)");
		  $this->nomescol[] = "idmoni";
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "id") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "descri") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "comple") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
                if(key_exists("INNER JOIN planilha", $this->sqlinner)) {
                }
                else {
                    if($this->tabpri == "planilha") {
                    }
                    else {
                        $this->sqlinner['INNER JOIN planilha'] = "INNER JOIN planilha ON planilha.id = ".$this->tabpri.".id_plan";
                    }
                }
                //$this->inner
                if(key_exists("INNER JOIN planilha", $this->inner)) {
                }
                else {
                    if($this->tabpri == "planilha") {
                    }
                    else {
                        $this->inner['INNER JOIN planilha'] = "INNER JOIN planilha ON planilha.id = ".$this->tabpri.".id_plan";
                    }
                }
	      }
	      if($tab == "motivo") {
		if($col == "nome") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "tipo") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
                if(key_exists("INNER JOIN motivo",$this->sqlinner)) {
                }
                else {
                    if($this->tabpri == "motivo") {
                    }
                    else {
                        $this->sqlinner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = regmonitoria.idmotivo";
                    }
                }
                //$this->inner
                if(key_exists("INNER JOIN motivo",$this->inner)) {
                }
                else {
                    if($this->tabpri == "motivo") {
                    }
                    else {
                        $this->inner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = regmonitoria.idmotivo";
                    }
                }
	      }
	      if($tab == "regmonitoria") {
                if($col == "idreg") {
                    if(in_array("DISTINCT(".$this->tabpri.".idreg)",$this->colselect)) {
                    }
                    else {
                      array_unshift($this->colselect,"DISTINCT(".$this->tabpri.".idreg)");
                      $this->nomescol[] = "idreg";
                      $this->nomesapres[] = $lselrela['nomeapres'];
                    }
                }
		if($col == "idmotivo") {
                    if(key_exists("INNER JOIN motivo",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "motivo") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = ".$this->tabpri.".idmotivo";
                        }
                    }
                    $this->colselect[] = "motivo.nomemotivo as motivo".substr($tab,0,4);
                    $this->nomescol[] = "motivo".substr($tab,0,4);
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    //$this->inner
                    if(key_exists("INNER JOIN motivo",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "motivo") {
                        }
                        else {
                            $this->inner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = ".$this->tabpri.".idmotivo";
                        }
                    }
		}
		if($col == "monitor") {
                    if(key_exists("INNER JOIN monitor",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitor") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = ".$this->tabpri.".monitor";
                        }
                    }
		    $this->colselect[] = "monitor.nomemonitor as ".$col.substr($tab,0,4);
		    $this->nomescol[] = $col.substr($tab,0,4);
		    $this->nomesapres[] = $lselrela['nomeapres'];
                    //$this->inner
                    if(key_exists("INNER JOIN monitor",$this->inner)) {
                    }
                    else {
                        if($this->tabpri == "monitor") {
                        }
                        else {
                            $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = ".$this->tabpri.".monitor";
                        }
                    }
		}
		if($col == "data") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "horaini") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "horafim") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
		if($col == "tmpaudio") {
		  $this->colselect[] = $tab.".".$col." as ".$col.substr($tab,0,4);
		  $this->nomescol[] = $col.substr($tab,0,4);
		  $this->nomesapres[] = $lselrela['nomeapres'];
		}
	      }
	    }
	    if($this->tipo == "SINTETICO") {
              $this->where = array();
	      if($tab == "rel_filtros") {
                  $this->tabpri = "filtro_dados";
                  if(key_exists("INNER JOIN rel_filtros",$this->sqlinner)) {

                  }
                  else {
                      if($this->tabpri == "rel_filtros") {
                      }
                      else {
                        $this->sqlinner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.$col = filtro_dados.idfiltro_dados";
                      }
                  }
                  if(key_exists("LEFT JOIN monitoria",$this->sqlinner)) {
                  }
                  else {
                      if($this->tabpri == "monitoria") {
                      }
                      else {
                            $this->sqlinner['LEFT JOIN monitoria'] = "LEFT JOIN monitoria ON monitoria.idrel_filtros = rel_filtros.idrel_filtros";
                      }
                  }
                  $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, filtro_dados.nomefiltro_dados as nome".substr($tab,0,3).", filtro_dados.idfiltro_dados as idfiltro ";
                  $this->nomescol[] = "nome".substr($tab,0,3);
                  $this->nomesapres[] = $lselrela['nomeapres'];
                  $this->sqlgroup = "GROUP BY filtro_dados.idfiltro_dados";
                  $this->vinccoluna = "idfiltro";
                  $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                  if(isset($this->idrel)) {
                        $seltab = "SELECT COUNT(*) as result FROM relmonicol INNER JOIN relmoni ON relmoni.id = relmonicol.idrelmoni WHERE relmoni.id='".$this->idrel."' AND tabela='rel_filtros'";
                        $eseltab = $_SESSION['fetch_array']($_SESSION['query']($seltab)) or die ("erro na query de consulta da tabela rel_filtros");
                          if($eseltab['result'] >= 1) {
                              $this->inner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = monitoria.idrel_filtros";
                          }
                          else {
                            if(isset($this->varant)) {
                                if(key_exists("INNER JOIN monitor",$this->inner)) {
                                }
                                else {
                                      $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = monitoria.idmonitor";
                                }
                                if(key_exists("INNER JOIN filtro_dados",$this->inner) OR key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                                        $valinner = $this->sqlinner['INNER JOIN filtro_dados']." AND rel_filtros.".$col."";
                                    }
                                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner1)) {
                                        $valinner = $this->sqlinner1['INNER JOIN filtro_dados']." AND rel_filtros.".$col."";
                                    }
                                    $innerdados = array("INNER JOIN filtro_dados" => $valinner);
                                    $this->sqlinner = array_replace($this->sqlinner, $innerdados);
                                }
                                else {
                                      $this->inner['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$col."";
                                }
                            }
                            else {
                                if(key_exists("INNER JOIN rel_filtros",$this->inner)) {
                                }
                                else {
                                      $this->inner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = monitoria.idrel_filtros";
                                }
                            }
                          }
                          if(isset($this->varant)) {
                            $this->where[] = "monitor.id='".$this->idmonitor."'";
                            $this->where[] = "rel_filtros.".$col."=";
                          }
                          else {
                            $this->where[] = "rel_filtros.".$col."=";
                          }
                    }
                    else {
                        if(key_exists("INNER JOIN rel_fitlros",$this->inner)) {
                        }
                        else {
                              $this->inner['INNER JOIN rel_filtros'] = "INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = monitoria.idrel_filtros";
                        }
                        if(key_exists("INNER JOIN filtro_dados",$this->inner)) {
                        }
                        else {
                              $this->inner['INNER JOIN filtro_dados'] = "INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.".$col."";
                        }
                        $this->where[] = "rel_filtros.".$col."=";
                    }
	      }
	      if($tab == "rel_fluxo") {
		if($col == "id_status") {
                    if(key_exists("INNER JOIN monitoria_fluxo",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitoria_fluxo") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN monitoria_fluxo'] = "INNER JOIN monitoria_fluxo ON monitoria_fluxo.idmonitoria = ".$this->tabpri.".idmoni";
                        }
                    }
                    if(key_exists("INNER JOIN status",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "status") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN status'] = "INNER JOIN status ON status.id = monitoria_fluxo.idstatus";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, status.nome as nomestatus, monitoria_fluxo.idstatus ";
                    $this->nomescol[] = "nomestatus";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY monitoria_fluxo.idstatus";
                    $this->vinccoluna = "idstatus";
                    $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                    if(key_exists("INNER JOIN monitoria_fluxo",$this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN monitoria_fluxo'] = "INNER JOIN monitoria_fluxo ON monitoria_fluxo.idmonitoria = monitoria.idmonitoria";
                    }
                    $this->where[] = "monitoria_fluxo.idstatus=";

		}
		if($col == "id_defini") {
                    if(key_exists("INNER JOIN monitoria_fluxo", $this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitoria_fluxo") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN monitoria_fluxo'] = "INNER JOIN monitoria_fluxo ON monitoria_fluxo.idmonitoria = ".$this->tabpri.".idmoni";
                        }
                    }
                    if(key_exists("INNER JOIN definicao",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "definicao") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN definicao'] = "INNER JOIN definicao ON definicao.iddefinicao = monitoria_fluxo.iddefinicao";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, definicao.nomedefinicao as nomedefini, monitoria_fluxo.iddefinicao";
                    $this->nomescol[] = "nomedefini";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY definicao.iddefinicao";
                    $this->vinccoluna = "iddefinicao";
                    $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                    if(key_exists("INNER JOIN monitoria_fluxo", $this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN monitoria_fluxo'] = "INNER JOIN monitoria_fluxo ON monitoria_fluxo.idmonitoria = monitoria.idmonitoria";
                    }
                    $this->where[] = "monitoria_fluxo.iddefinicao=";
		}
	      }
	      if($tab == "motivo") {
		if($col == "nome") {
                    if(key_exists("INNER JOIN motivo",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "motivo") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = regmonitoria.idmotivo";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(regmonitoria.idregmonitoria)) as QTDE, motivo.idmotivo, motivo.nomemotivo as nomemotivo, motivo.tipo";
                    $this->nomescol[] = "nomemotivo";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY motivo.idmotivo";
                    $this->vinccoluna = "idmotivo";
                    $this->count = "COUNT(DISTINCT(regmonitoria.idregmonitoria))";
                    if(key_exists("INNER JOIN motivo", $this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = regmonitoria.idmotivo";
                    }
                    $this->where[] = "motivo.idmotivo=";
		}
	      }
	      if($tab == "tipo_motivo") {
		if($col == "nome") {
                    $this->tabpri = "tipo_motivo";
                    if(key_exists("INNER JOIN motivo",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "motivo") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.tipo = tipo_motivo.id";
                        }
                    }
                    if(key_exists("LEFT JOIN regmonitoria",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "regmonitoria") {
                        }
                        else {
                            $this->sqlinner['LEFT JOIN regmonitoria'] = "LEFT JOIN regmonitoria ON regmonitoria.idmotivo = motivo.idmotivo";
                        }
                    }
                    if(key_exists("LEFT JOIN moni_pausa",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "moni_pausa") {
                        }
                        else {
                            $this->sqlinner['LEFT JOIN moni_pausa'] = "LEFT JOIN moni_pausa ON moni_pausa.idmotivo = motivo.idmotivo";
                        }
                    }
                    $this->colselect[] = "COUNT(regmonitoria.idregmonitoria OR moni_pausa.idmoni_pausa) as QTDE, motivo.tipo, tipo_motivo.nome as nometipo, tipo_motivo.vincula, tipo_motivo.id as idtipo";
                    $this->nomescol[] = "nometipo";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY motivo.tipo";
                    $this->vinccoluna = "idtipo";
                    $this->count = "COUNT(DISTINCT(regmonitoria.idregmonitoria))";
                    $this->where[] = "motivo.tipo=";
		}
	      }
	      if($tab == "regmonitoria") {
		if($col == "idreg") {
		  $this->colselect[] = "COUNT(DISTINCT(regmonitoria.idregmonitoria)) as result";
		  $this->nomescol[] = "idregmonitoria";
		  $this->nomesapres[] = "REGISTROS";
		  $this->vinccoluna = "";
                  $this->count = "COUNT(DISTINCT(regmonitoria.idregmonitoria))";
		}
		if($col == "idmotivo") {
                    $this->tabpri = "motivo";
                    if(key_exists("INNER JOIN regmonitoria",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "regmonitoria") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN regmonitoria'] = "INNER JOIN regmonitoria ON regmonitoria.idmotivo = motivo.idmotivo";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(regmonitoria.idregmonitoria)) as QTDE, motivo.nomemotivo as nomemotivo, motivo.idmotivo as idmotivo";
                    $this->nomescol[] = "nomemotivo";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY r.idmotivo";
                    $this->vinccoluna = "idmotivo";
                    $this->count = "COUNT(DISTINCT(regmonitoria.idregmonitoria))";
                    if(key_exists("INNER JOIN motivo",$this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN motivo'] = "INNER JOIN motivo ON motivo.idmotivo = regmonitoria.idmotivo";
                    }
                    $this->where[] = "motivo.idmotivo=";
		}
		if($col == "monitor") {
                    $this->tabpri = "monitor";
                    if(key_exists("INNER JOIN regmonitoria", $this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "regmonitoria") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN regmonitoria'] = "INNER JOIN regmonitoria ON regmonitoria.monitor = monitor.id";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(regmonitoria.idreg)) as QTDE, monitor.nome as nomemonitor, monitor.id";
                    $this->nomescol[] = "nomemonitor";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY monitor.id";
                    $this->vinccoluna = "id";
                    $this->count = "COUNT(DISTINCT(regmonitoria.idregmonitoria))";
                    if(key_exists("INNER JOIN monitor",$this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = regmonitor.monitor";
                    }
                    $this->where[] = "monitor.id=";
		}
		if($col == "data") {
		  $this->colselect[] = "COUNT(DISTINCT(regmonitoria.idregmonitoria)) as QTDE, regmonitoria.data";
		  $this->nomescol[] = "data";
		  $this->nomesapres[] = $lselrela['nomeapres'];
		  $this->sqlgroup = "GROUP BY data";
		  $this->vinccoluna = "data";
                  $this->count = "COUNT(DISTINCT(regmonitoria.idregmonitoria))";
                  $this->where[] = "regmonitoria.data=";
		}
	      }
	      if($tab == "monitoria") {
		if($col == "idmonitoria") {
		  $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as result";
		  $this->nomescol[] = "idmonitoria";
		  $this->nomesapres[] = "MONITORIAS";
		  $this->vinccoluna = "";
                  $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
		}
		if($col == "idoper") {
                    $this->tabpri = "operador";
                    if(key_exists("LEFT JOIN monitoria",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitoria") {
                        }
                        else {
                            $this->sqlinner['LEFT JOIN monitoria'] = "LEFT JOIN monitoria ON monitoria.idoperador = operador.idoperador";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, operador.operador as nomeoper, operador.idoperador";
                    $this->nomescol[] = "nomeoper";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY operador.operador";
                    $this->vinccoluna = "idoperador";
                    $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                    if(key_exists("INNER JOIN operador",$this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN operador'] = "INNER JOIN operador ON operador.idoperador = monitoria.idoperador";
                    }
                    $this->where[] = "operador.idoperador=";
		}
		if($col == "idsuper_oper") {
                    $this->tabpri = "super_oper";
                    if(key_exists("LEFT JOIN monitoria", $this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitoria") {
                        }
                        else {
                            $this->sqlinner['LEFT JOIN monitoria'] = "LEFT JOIN monitoria ON monitoria.idsuper_oper = super_oper.idsuper_oper";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, super_oper.supervisor as nomesuper, super_oper.idsuper_oper";
                    $this->nomescol[] = "nomesuper";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY monitoria.idsuper_oper";
                    $this->vinccoluna = "idsuper_oper";
                    $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                    if(key_exists("INNER JOIN super_oper",$this->inner)) {
                    }
                    else {
                        $this->inner['INNER JOIN super_oper'] = "INNER JOIN super_oper ON super_oper.idsuper_oper = monitoria.idsuper_oper";
                    }
                    $this->where[] = "super_oper.idsuper=";
		}
		if($col == "monitor") {
                    $this->tabpri = "monitor";
                    if(key_exists("INNER JOIN monitoria",$this->sqlinner)) {
                    }
                    else {
                        if($this->tabpri == "monitoria") {
                        }
                        else {
                            $this->sqlinner['INNER JOIN monitoria'] = "INNER JOIN monitoria ON monitoria.idmonitor = monitor.id";
                        }
                    }
                    $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, monitor.nome as nomemonitor, monitor.id";
                    $this->nomescol[] = "nomemonitor";
                    $this->nomesapres[] = $lselrela['nomeapres'];
                    $this->sqlgroup = "GROUP BY monitoria.idmonitor";
                    $this->vinccoluna = "id";
                    $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                    if(key_exists("INNER JOIN monitor", $this->inner)) {
                    }
                    else {
                          $this->inner['INNER JOIN monitor'] = "INNER JOIN monitor ON monitor.idmonitor = monitoria.idmonitor";
                    }
                    $this->where[] = "monitor.id=";
		}
		if($col == "data") {
		  $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE, monitoria.data";
		  $this->nomescol[] = "data";
		  $this->nomesapres[] = $lselrela['nomeapres'];
		  $this->sqlgroup = "GROUP BY data";
		  $this->vinccoluna = "data";
                  $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
                  $this->where[] = "monitoria.data=";
		}
		if($col == "horaini") {
		  for($i = 0; $i <= 24; $i++) {
		    if($i < 10) {
			    $horas[] = "0".$i;
		    }
		    else {
		    $horas[] = $i;
		    }
		  }
		  $this->colselect[] = "COUNT(DISTINCT(monitoria.idmonitoria)) as QTDE";
		  $this->nomesapres[] = "HORAS";
		  $this->nomescol[] = "horaini";
                  $this->count = "COUNT(DISTINCT(monitoria.idmonitoria))";
		  $this->vinccoluna = "hora";
		}
	      }
	    }
	  $this->tabela[] = $tab;
	  }
	}
        if(isset($this->idrel)) {
            $count = 0;
            if($this->whereant == "") {
            }
            else {
                if(eregi('data', $this->whereant)) {
                    //$this->sqlwhere = array();
                    $this->sqlwhere[] = $this->whereant;
                }
                else {
                    $this->sqlwhere[] = $this->whereant;
                }
            }
            $innerant = array();
            foreach($this->innerant as $ant) {
                $partinner = explode("ON",$ant);
                if(eregi("filtro_dados",$partinner[0])) {
                    $innerdados = explode("=",$partinner[1]);
                    $innerdados = trim($innerdados[1]);
                    $trocainner = array("INNER JOIN filtro_dados" => $this->sqlinner['INNER JOIN filtro_dados']." AND ".$innerdados."");
                    if(key_exists("INNER JOIN filtro_dados", $this->sqlinner)) {
                        $this->sqlinner = array_replace($trocainner, $this->sqlinner);
                    }
                    else {
                    }
                }
                else {
                    $innerjoin = trim($partinner[0]);
                    if(key_exists($innerjoin, $innerant)) {
                    }
                    else {
                        if($this->tabpri == "filtro_dados" AND $innerjoin == "INNER JOIN rel_filtros") {
                        }
                        else {
                            $innerant[$innerjoin] = $ant;
                        }
                    }
                }
            }
            $innerdiff = array_diff($innerant,$this->sqlinner);
            $this->sqlinner = array_merge($this->sqlinner, $innerdiff);

        }
        if(isset($this->sqlinner1)) {
            $this->sqlinner= array_diff_key($this->sqlinner, $this->sqlinner1);
            $this->sqlinner = array_merge($this->sqlinner, $this->sqlinner1);
        }
        else {
        }
        $newinner = array_unique($this->sqlinner);
        $countinner = 0;
        $newsqlinner = array();
        $this->sqlinner = array_unique($this->sqlinner);
        $new = new Inner;
        $this->sqlinner = $new->sqlinner($this->sqlinner, $this->tabpri);
	$this->sqlinner = implode(" ",$this->sqlinner);
	$this->colselect = implode(", ",$this->colselect);
        $this->sqlwhere = array_unique($this->sqlwhere);
	if(count($this->sqlwhere) == 1) {
	  $this->sqlwhere = "WHERE ".implode(" ",$this->sqlwhere);
	}
	if(count($this->sqlwhere) > 1) {
	  $this->sqlwhere = "WHERE ".implode(" AND ",$this->sqlwhere);
	}
      }

      function relvinc() {
		  return $this->vinccoluna;
      }

      function nomescol () {
		  return $this->nomescol;
      }
      
      function nomesapres () {
		  return $this->nomesapres;
      }

      function innerant () {
          $innerdif = array_diff($this->innerant, $this->inner);
          $this->inner = array_merge($this->inner, $innerdif);
          $this->inner = array_unique($this->inner);
          $this->inner = implode("*",$this->inner);
          return $this->inner;
      }

      function  whereant() {
          if($this->whereant == "") {
            $this->where = implode(" AND ",$this->where);
            return $this->where;
          }
          else {
              if($this->tipo == "ANALITICO") {
                  return $this->whereant;
              }
              else {
                  $arrayant = explode(" AND ", trim($this->whereant));
                  $this->where = array_diff($this->where, $arrayant);
                  if(count($this->where) == 1) {
                      foreach($this->where as $where) {
                          if($where == "") {
                              $this->where = $this->whereant;
                              return $this->where;
                          }
                          else {
                              $this->where = implode(" AND ",$this->where);
                              $this->where = $this->whereant." AND ".$this->where;
                              return $this->where;
                          }
                      }
                  }
                  if(count($this->where) > 1) {
                      $this->where = implode(" AND ",$this->where);
                      $this->where = $this->whereant." AND ".$this->where;
                      return $this->where;
                  }
              }
          }
      }

      function sqlcount () {
		$count = "SELECT ".$this->count." as result FROM ".$this->tabpri." ".$this->sqlinner." ".$this->sqlwhere."";
		return $count;
      }

      function sqlresult() {
	  	$result = "SELECT ".$this->colselect." FROM ".$this->tabpri." ".$this->sqlinner." ".$this->sqlwhere." ".$this->sqlgroup." ".$this->ordena."";
	  	echo $result;
	  	return $result;
      }

}

?>