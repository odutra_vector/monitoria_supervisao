<?php
//$rais = $_SESSION['DOCUMENT_ROOT'];
include_once 'class.inner.php';
include_once 'class.formulas.php';
include_once 'class.export.php';
include_once 'class.grafico.php';

/**
 * Description of class
 *
 * @author otto
 * Arquivo de Classe para Gerar relatÃ³rios dinÃ¢micos, Ã© possÃ­vel utilizar 4 classes possÃ­veis, uma para criar os INNERS
   necessÃ¡rios a query de consulta e as outras duas para retornar o resultado dos relatÃ³rios *
 * Primeira Classe = InnersMoni
 * Segunda Classe = RelMonitoria
 * Terceira Classe = RelGerenciais
 * Quarta Classe = RelProducao
 */



class InnersMoni extends TabelasSql {
    public $idrel; /* id do relatÃ³rio chamado pela classe */
    public $nomerel; /*nome do relatÃ³rio*/
    public $proxrel; /* id do proximo relatÃ³rio que estÃ¡ vinculado ao chamado */
    public $tabpri; /* tabela promaria do relatÃ³rio, ou seja, base da criaÃ§Ã£o da tabela de informaÃ§Ã£o visual */
    public $aliastabpri; /* alias da tabela primÃ¡ria */
    public $colpri; /* coluna primaria, ou group by */
    public $tabsec; /* tabela secundÃ¡ria */
    public $colsec; /* coluna secundÃ¡ria */
    public $aliastabsec; /* alias da tabela secundÃ¡ria */
    public $colunas; /* colunas que vÃ£o compor o relatÃ³rio */
    public $colunasapres; /* texto aparente das colunas */
    public $inners; /* inners criados na chamada da classe */
    public $sql; /* comando sql gerado */
    public $groupby; /* causula group by do sql */
    public $opcoes; /* variÃ¡veis enviadas via POST para criaÃ§Ã£o da query */
    public $r; /* variÃ¡vel que determina se a classe serÃ¡ chamada mais de 1 vez */
    public $wheresql; /* clausula where para criaÃ§Ã£o do comando sql */
    public $whereops; /* conjunto das opÃ§Ãµes selecionadas para ser passado como GET entre os relatÃ³rios */
    public $addwhere; /* valor where que serÃ¡ acrescentado a clasula WHERE quando um relatÃ³rio Ã© chamado por vinculo */
    public $total; /*total da consulta*/
    public $linhas; /*total de linhas da consulta*/
    public $pags;
    public $wherelimit;

    function TabsInner($proxrel, $addwhere, $valadd, $tabadd, $groupby,$rel,$limit) {
        $tabsql = new TabelasSql();
        $idproxrel = $proxrel;
        $rel = $rel;
        $wherelimit = explode(",",$limit);
        if($limit != "") {
            $this->wherelimit = "LIMIT ".$wherelimit[0].",".$wherelimit[1];
        }
        else {
            $this->wherelimit = "LIMIT 0,500";
        }
        if($idproxrel != "" && $rel == "") {
            $selrel = "SELECT * FROM relmoni rm WHERE rm.idrelmoni='".$idproxrel."'";
        }
        else {
            if($rel != "") {
                $selrel = "SELECT * FROM relmoni rm WHERE rm.nomerelmoni='$rel'";
            }
            else {
                $selrel = "SELECT * FROM relmoni rm WHERE rm.nomerelmoni='".$_POST['rel']."'";
            }

        }
        $lselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
        $tabsql->Tabvar($lselrel['tabsec']);
        $tabsql->tabvar;
        $this->nomerel = $lselrel['nomerelmoni'];
        $this->idrel = $lselrel['idrelmoni'];
        $tabs[$lselrel['tabpri']] = $tabsql->AliasTab($lselrel['tabpri']);
        $tabs[$lselrel['tabsec']] = $tabsql->AliasTab($lselrel['tabsec']);
        $this->tabpri = $lselrel['tabpri'];
        $this->aliastabpri = $tabsql->AliasTab($lselrel['tabpri']);
        $this->tabsec = $lselrel['tabsec'];
        $this->aliastabsec = $tabsql->AliasTab($lselrel['tabsec']);
        $this->groupby = $lselrel['agrupa'];
        if($addwhere != "" && $valadd != "") {
            $aliasadd = explode(".",$addwhere);
            if(isset($_SESSION['wheresql']) AND $_SESSION['wheresql'] != "") {
                $_SESSION['wheresql'][$tabadd] = $addwhere."='".$valadd."'";
            }
            else {
                unset($_SESSION['wheresql']);
                $this->wheresql[$tabadd] = $addwhere."='".$valadd."'";
            }
            $cols[$groupby] = $tabadd;
        }
        else {
            unset($_SESSION['wheresql']);
            foreach($this->opcoes as $kop => $opcoes) {
                if($opcoes == "" || $opcoes == "undefined" || $opcoes == "null") {
                }
                else {
                    if($this->opcoes['ID'] != "") {
                        if($this->tabsec == "monitoria") {
                            if(!array_key_exists("ID", $this->wheresql)) {
                                $this->wheresql[$kop] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop]."='".$opcoes."'";
                            }
                        }
                    }
                    else {
                        if(eregi('dtinimoni',$kop) && $this->opcoes['dtfimmoni'] != "") {
                            if(array_key_exists('data', $this->wheresql)) {
                            }
                            else {
                                $this->wheresql['data'] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop]." BETWEEN '".$opcoes."' AND '".$this->opcoes['dtfimmoni']."'";
                            }
                        }
                        if(eregi('dtinictt', $kop) && $this->opcoes['dtfimctt'] != "") {
                            if($this->tabsec == "monitoria" OR $this->tabsec == "regmonitoria") {
                                if(array_key_exists('datactt', $this->wheresql)) {
                                }
                                else {
                                    $this->wheresql['datactt'] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop]." BETWEEN '".$opcoes."' AND '".$this->opcoes['dtfimctt']."'";
                                }
                            }
                            else {
                            }
                        }
                        if(!eregi('dtinimoni',$kop) && !eregi('dtfimmoni',$kop) && !eregi('dtinictt',$kop) && !eregi('dtfimctt',$kop)) {
                            if(array_key_exists($kop, $this->wheresql)) {
                            }
                            else {
                                if($kop == "fg" OR $kop == "fgm") {
                                    $this->wheresql[$kop] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop].">='".$opcoes."'";
                                }
                                else {
                                    if($kop == "tmonitoria") {
                                        $tmoni = explode(",",$opcoes);
                                        if(count($tmoni) > 1) {
                                            $this->wheresql[$kop] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop]." IN ('".implode("','",$tmoni)."')";

                                        }
                                        else {
                                            $this->wheresql[$kop] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop]."='".$opcoes."'";
                                        }
                                    }
                                    else {
                                        if($kop == "idalerta") {
                                            foreach($opcoes as $op) {
                                                $or[] = $tabsql->AliasTab($tabsql->tabvar["alertas"]).".".$tabsql->colvar["alertas"]." LIKE '%$op%'";
                                            }
                                            $this->wheresql[$kop] = "(".implode(" OR ",$or).")";
                                        }
                                        else {
                                            if($kop == "search") {
                                                if($this->tabsec == "monitoria") {
                                                    $this->wheresql[$kop] = "(".$tabsql->AliasTab('monitoria').".obs LIKE '%".  mysql_real_escape_string($opcoes)."%' OR ".$tabsql->AliasTab('moniavalia').".obs LIKE '%".mysql_real_escape_string($opcoes)."%')";
                                                }
                                            }
                                            else {
                                                $this->wheresql[$kop] = $tabsql->AliasTab($tabsql->tabvar[$kop]).".".$tabsql->colvar[$kop]."='".$opcoes."'";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //seleciona relacionamentos do usuário
        $userfiltro = array('user_adm' => 'useradmfiltro','user_web' => 'userwebfiltro');
        $tabfiltro = $userfiltro[$_SESSION['user_tabela']];
        $selreluser = "SELECT * FROM $tabfiltro WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
        $eselreluser = $_SESSION['query']($selreluser) or die ("erro na query de consulta dos relacionamentos do usuário");
        while($lselreluser = $_SESSION['fetch_array']($eselreluser)) {
            $idsrel[] = $lselreluser['idrel_filtros'];
        }
        $whererelfiltro = " AND ".$this->AliasTab(rel_fitlros).".idrel_filtros IN ('".implode("','",$idsrel)."')";

        //cria where para ser usado na query de resultado
        if(isset($_SESSION['wheresql']) AND $_SESSION['wheresql'] != "") {
            $where = "WHERE ".implode(" AND ",$_SESSION['wheresql']);
        }
        else {
            if($this->wheresql != "") { // cria o WHERE para ser utilizado na query de consulta
                $where = "WHERE ".implode(" AND ",$this->wheresql)."";
                $_SESSION['wheresql'] = $this->wheresql;
            }
        }

        //vincula as tabelas do relatÃ³rio
        if($lselrel['tabterc'] != "") {
            $tabterc = explode(",",$lselrel['tabterc']);
            foreach($tabterc as $t) { // associa as tabelas terciÃ¡rias se existirem as possÃ­veis tabelas que precisam ser consultadas na query
                if(key_exists($t, $tabs)) {
                }
                else {
                    $tabs[$t] = $tabsql->AliasTab($t);
                }
            }
        }

        if($lselrel['vincrel'] != "00") {
            $this->r = 1;
            if($this->tabpri == "status") {
                $this->addwhere = $tabsql->AliasTab(rel_fluxo).".idatustatus";
            }
            else {
                if($this->tabpri == "aval_plan" OR $this->tabpri == "grupo" OR $this->tabpri == "subgrupo" OR $this->tabpri == "pergunta") {
                    $this->addwhere = $tabsql->AliasTab(moniavalia).".".$this->groupby;
                }
                else {
                    $this->addwhere = $tabsql->AliasTab($this->tabpri).".".$this->groupby;
                }
            }
            $this->proxrel = $lselrel['vincrel'];
        }
        else {
            $this->r = 0;
        }

        //cria as variÃ¡veis de coluna para query de resultado e coluna de apresentanÃ§Ã£o no relatÃ³rio
        foreach($tabs as $tab => $alias) { // faz loop das tabelas relacionadas ao relatÃ³rio para identificar as colunas que precisam ser criadas e iniciar a montagem da query de consulta
            $relmonicol = "SELECT * FROM relmonicol WHERE idrelmoni='$this->idrel' AND tabela='$tab' ORDER BY posicao";
            $eselcol = $_SESSION['query']($relmonicol) or die ($_SESSION['mysql_error']);
            while($lselcols = $_SESSION['fetch_array']($eselcol)) {
                if($tab == "monitoria" OR $tab == "regmonitoria") {
                    $this->colsec[$lselcols['coluna']] = $tab;
                }
                else {
                    $this->colpri[$lselcols['coluna']] = $tab;
                }
                $cols[$lselcols['coluna']] = $tab;
                $verifc = 0;
                if($lselcols['tipo'] == "QUANTIDADE" OR $lselcols['tipo'] == "PERCENTUAL") {
                    if($lselcols['tipo'] == "QUANTIDADE") {
                        if($lselcols['coluna'] == "qtdefg" OR $lselcols['coluna'] == "qtdefgm") {
                            $this->colunasapres[$lselcols['posicao']] = $lselcols['nomeapresenta'];
                            if($this->tabpri == "monitoria") {
                                $this->colunas[$lselcols['posicao']][$lselcols['tipo']]['QTDEFG'] = "$this->aliastabsec.".$lselcols['coluna']."";
                            }
                            else {
                                $this->colunas[$lselcols['posicao']][$lselcols['tipo']]['QTDEFG'] = "SUM(".$this->aliastabsec.".".$lselcols['coluna'].")";
                            }
                        }
                        else {
                            $this->colunasapres[$lselcols['posicao']] = $lselcols['nomeapresenta'];
                            $this->colunas[$lselcols['posicao']][$lselcols['tipo']]['QUANTIDADE'] = "COUNT(DISTINCT(".$this->aliastabsec.".".$lselcols['coluna']."))";
                        }
                    }
                    if($lselcols['tipo'] == "PERCENTUAL") {
                        $this->colunasapres[$lselcols['posicao']] = $lselcols['nomeapresenta'];
                        $this->colunas[$lselcols['posicao']][$lselcols['tipo']]['PERCENTUAL'] = "PERCENTUAL";
                        $colreplace['posicao'] = $lselcols['posicao'];
                        $colreplace['tipo'] = $lselcols['tipo'];
                        $colreplace['coluna'] = "PERCENTUAL";
                        $colperc = $lselcols['coluna'];
                    }
                }
                else {
                    if($tab == "rel_filtros" && $lselcols['coluna'] != "idrel_fitlros") {
                       $this->colunasapres[$lselcols['posicao']] = $lselcols['nomeapresenta'];
                        $part = explode("_",$lselcols['coluna']);
                        $alias = $this->AliasTab(filtro_dados).$this->Aliastab($part[1]);
                        $this->colunas[$lselcols['posicao']][$lselcols['tipo']][$lselcols['nomeapresenta']] = $alias.".nomefiltro_dados";
                    }
                    else {
                        $this->colunasapres[$lselcols['posicao']] = $lselcols['nomeapresenta'];
                        $this->colunas[$lselcols['posicao']][$lselcols['tipo']][$lselcols['nomeapresenta']] = $alias.".".$lselcols['coluna'];
                    }
                }
            }
        }
        if($this->tabpri == "status") {
            $this->colunas['groupby'][$this->groupby] = $tabsql->AliasTab(rel_fluxo).".idatustatus";
        }
        else {
            $this->colunas['groupby'][$this->groupby] = $tabsql->AliasTab($this->tabpri).".".$this->groupby;
        }
        ksort($this->colunas);
        ksort($this->colunasapres);
        if($this->tabpri == "operador" OR $this->tabpri == "super_oper" OR $this->tabpri == "monitor") {
            $tipoinner = "LEFT";
        }
        else {
            $tipoinner = "INNER";
        }
        foreach($cols as $col => $tabcol) { // faz loop das colunas relacionadas ao relatÃ³rio e suas tabelas, para poder ser consultado na FUNCTION 'Tabsvinc'
            $tabsql->ColunasTabRel(); // obtem as colunas possivelmente relacionadas por tabelas
            foreach($tabsql->coltabrel as $key => $tabsrel) { // faz loop da colunas para identificar quais colunas do relatÃ³rio possuem ligaÃ§Ã£o com qual tabela, para ser associada as tabelas vinculadas
                foreach($tabsrel as $colrel) {
                    if(in_array($col, $colrel) AND $tabcol == key($tabsrel)) {
                        $nometab = key($tabsrel);
                        $aliastab = $tabsql->Aliastab($nometab);
                    }
                }
            }
            $tabsql->TabsVinc($nometab, $this->tabpri, $this->tabsec); // obtem as tabelas vinculadas Ã  cada tabela relacionada ao relatÃ³rio
            $tabon = "";
            if($this->tabsec != $this->tabpri) { //  verificar se a tabela secundÃ¡ria Ã© diferente da primÃ¡ria, pois se for, faz o INNER das tabelas 'monitoria, regonitoria e moni_pausa' que sÃ£o as tabelas principais
                if(!key_exists($this->tabsec, $tabsql->innertabelas)) {
                    if($this->tabpri == "status" OR $this->tabpri == "definicao" OR $this->tabpri == "rel_filtros" OR $this->tabpri == "fluxo"
                    OR $this->tabpri == "operador" OR $this->tabpri == "super_oper" OR $this->tabpri == "monitor" OR $this->tabpri == "motivo"
                    OR $this->tabpri == "monitoria_fluxo" OR $this->tabpri == "ava_plan" OR $this->tabpri == "grupo" OR $this->tabpri == "pergunta"
                    OR $this->tabpri == "subgrupo") {
                        if($this->tabpri == "status" OR $this->tabpri == "definicao" OR $this->tabpri == "fluxo") {
                            $tvincsec = "monitoria_fluxo";
                            $tabsql->innertabelas[$this->tabsec] = "$tipoinner JOIN $this->tabsec $this->aliastabsec ON $this->aliastabsec.id$tvincsec = ".$this->AliasTab($tvincsec).".id$tvincsec";
                        }
                        else {
                            if($this->tabpri == "monitoria_fluxo") {
                                $tabsql->innertabelas[$this->tabsec] = "$tipoinner JOIN $this->tabsec $this->aliastabsec ON $this->aliastabsec.id$this->tabsec = ".$this->AliasTab($this->tabpri).".id$this->tabsec";
                            }
                            else {
                                if($this->tabpri == "ava_plan" OR $this->tabpri == "grupo" OR $this->tabpri == "pergunta" OR $this->tabpri == "subgrupo") {
                                    $tabsql->innertabelas[$this->tabsec] = "$tipoinner JOIN $this->tabsec $this->aliastabsec ON $this->aliastabsec.id$this->tabsec = ".$this->AliasTab(moniavalia).".id$this->tabsec";
                                }
                                else {
                                    $tabsql->innertabelas[$this->tabsec] = "$tipoinner JOIN $this->tabsec $this->aliastabsec ON $this->aliastabsec.id$this->tabpri = $this->aliastabpri.id$this->tabpri";
                                }
                            }
                        }
                    }
                    else {
                        if($this->tabpri == "intervalo_notas") {
                            $tabsql->innertabelas[$this->tabsec] = "$tipoinner JOIN $this->tabsec $this->aliastabsec ON $this->aliastabsec.idrel_filtros = ".$this->AliasTab(conf_rel).".idrel_filtros";
                        }
                        else {
                            $tabsql->innertabelas[$this->tabsec] = "$tipoinner JOIN $this->tabsec $this->aliastabsec";
                        }
                    }
                }
            }

            foreach($tabsql->tabvinc as $tabson => $tabsvinc) { // faz o loop das tabelas vinculadas para criar os possÃ­veis INNERS necessÃ¡rios a consulta no banco
                $arrtabvinc = explode(",",$tabsvinc);
                $tabval = explode(":",$tabson);
                $coltab = $tabval[1];
                if($tabson == "vazio") {
                    $atabon = "";
                }
                else {
                    $atabon = $tabval[0];
                }
                foreach($arrtabvinc as $tbvinc) {
                    $atabvinc = $tabsql->Aliastab($tbvinc);
                    if($atabon == "" OR $atabon == "vazio") {
                        $search = "INNER JOIN $tabsvinc $atabvinc";
                    }
                    else {
                        $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                    }
                    if(key_exists($search, $tabsql->innertabelas)) {
                    }
                    else {
                        $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tbvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                    }
                }
            }
        }

        // procura as possÃ­veis tabelas que serÃ£o vinculadas a query pelas opÃ§Ãµes selecionadas
        if(isset($_SESSION['wheresql']) AND $_SESSION['wheresql'] != "") {
            foreach($_SESSION['opcoes'] as $var => $valor) {
                if($valor != "") {
                    if(is_array($tabsql->tabvar[$var])) {
                        $tabs = $tabsql->tabvar[$var];
                        foreach($tabs as $tabvar) {
                            $atabvar = $tabsql->AliasTab($tabvar);
                            $tabsql->TabsVinc($tabvar, $this->tabpri, $this->tabsec);
                            foreach($tabsql->tabvinc as $tvar => $tabsvinc) {
                                $tabval = explode(":",$tvar);
                                $coltab = $tabval[1];
                                $atabon = $tabval[0];
                                $atabvinc = $tabsql->Aliastab($tabsvinc);
                                if($atabon == "" OR $atabon == "vazio") {
                                    $search = "INNER JOIN $tabsvinc $atabvinc";
                                }
                                else {
                                    $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                                }
                                if(key_exists($search, $tabsql->innertabelas)) {
                                }
                                else {
                                    $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tabsvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                                }
                            }
                        }
                    }
                    else {
                        $tabvar = $tabsql->tabvar[$var];
                        $atabvar = $tabsql->AliasTab($tabvar);
                        $tabsql->TabsVinc($tabvar, $this->tabpri, $this->tabsec);
                        foreach($tabsql->tabvinc as $tvar => $tabsvinc) {
                            $tabval = explode(":",$tvar);
                            $coltab = $tabval[1];
                            $atabon = $tabval[0];
                            $atabvinc = $tabsql->Aliastab($tabsvinc);
                            if($atabon == "" OR $atabon == "vazio") {
                                $search = "INNER JOIN $tabsvinc $atabvinc";
                            }
                            else {
                                $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                            }
                            if(!key_exists($search, $tabsql->innertabelas)) {
                                $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tabsvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                            }
                        }
                    }
                }
            }
        }
        else {
            foreach($this->opcoes as $var => $valor) { // faz loop das opÃ§Ãµes escolhidas na pÃ¡gina de consulta para verificar se ainda precisa adiconar algum INNER a query
                $tabvar = $tabsql->tabvar[$var];
                if($lselrel['tabsec'] == "regmonitoria" AND in_array($var,$tabsql->descop)) {
                }
                else {
                    if($valor != "") {
                        if(is_array($tabsql->tabvar[$var])) {
                            foreach($tabs as $tabvar) {
                                $atabvar = $tabsql->AliasTab($tabvar);
                                $tabsql->TabsVinc($tabvar, $this->tabpri, $this->tabsec);
                                foreach($tabsql->tabvinc as $tvar => $tabsvinc) {
                                    $tabval = explode(":",$tvar);
                                    $coltab = $tabval[1];
                                    $atabon = $tabval[0];
                                    $atabvinc = $tabsql->Aliastab($tabsvinc);
                                    if($atabon == "" OR $atabon == "vazio") {
                                        $search = "INNER JOIN $tabsvinc $atabvinc";
                                    }
                                    else {
                                        $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                                    }
                                    if(key_exists($search, $tabsql->innertabelas)) {
                                    }
                                    else {
                                        $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tabsvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                                    }
                                }
                            }
                        }
                        else {
                            $atabvar = $tabsql->AliasTab($tabvar);
                            $tabsql->TabsVinc($tabvar, $this->tabpri, $this->tabsec);
                            foreach($tabsql->tabvinc as $tvar => $tabsvinc) {
                                $tabval = explode(":",$tvar);
                                $coltab = $tabval[1];
                                $atabon = $tabval[0];
                                $atabvinc = $tabsql->Aliastab($tabsvinc);
                                if($atabon == "" OR $atabon == "vazio") {
                                    $search = "INNER JOIN $tabsvinc $atabvinc";
                                }
                                else {
                                    $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                                }
                                if(key_exists($serach, $tabsql->innertabelas)) {
                                }
                                else {
                                    $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tabsvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                                }
                            }
                        }
                    }
                }
            }
        }

        // procura as possÃ­veis tabelas relacionadas a clausula WHERE enviada de links anteriores
        if($_SESSION['wheresql'] != "") {
            foreach($_SESSION['wheresql'] as $kwhere => $wheretab) {
                $separa1 = explode(".",$wheretab);
                $separa2 = explode("=",$separa1[1]);
                $coltabw = trim($separa2[0]);
                foreach($tabsql->coltabrel as $key => $tabsrel) { // faz loop da colunas para identificar quais colunas do relatÃ³rio possuem ligaÃ§Ã£o com qual tabela, para ser associada as tabelas vinculadas
                    foreach($tabsrel as $colrelw) {
                        if(in_array($coltabw, $colrelw) AND $kwhere == key($tabsrel)) {
                            $ntabw = key($tabsrel);
                            $atabw = $tabsql->Aliastab($ntabw);
                        }
                        else {
                        }
                    }
                }
                if($ntabw == "") {
                }
                else {
                    $tabsql->TabsVinc($ntabw, $this->tabpri, $this->tabsec);
                    foreach($tabsql->tabvinc as $tvar => $tabsvinc) {
                        $tabval = explode(":",$tvar);
                        $coltab = $tabval[1];
                        $atabon = $tabval[0];
                        $atabvinc = $tabsql->Aliastab($tabsvinc);
                        if($atabon == "" OR $atabon == "vazio") {
                            $search = "INNER JOIN $tabsvinc $atabvinc";
                        }
                        else {
                            $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                        }
                        if(key_exists($serach, $tabsql->innertabelas)) {
                        }
                        else {
                            $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tabsvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                        }
                    }
                }
            }
        }
        else {
            foreach($this->wheresql as $kwhere => $wheretab) {
                $separa1 = explode(".",$wheretab);
                $separa2 = explode("=",$separa1[1]);
                $coltabw = trim($separa2[0]);
                foreach($tabsql->coltabrel as $key => $tabsrel) { // faz loop da colunas para identificar quais colunas do relatÃ³rio possuem ligaÃ§Ã£o com qual tabela, para ser associada as tabelas vinculadas
                    foreach($tabsrel as $colrelw) {
                        if(in_array($coltabw, $colrelw) AND $kwhere == key($tabsrel)) {
                            $ntabw = key($tabsrel);
                            $atabw = $tabsql->Aliastab($ntabw);
                        }
                        else {
                        }
                    }
                }
                $tabsql->TabsVinc($ntabw, $this->tabpri, $this->tabsec);
                foreach($tabsql->tabvinc as $tvar => $tabsvinc) {
                    $tabval = explode(":",$tvar);
                    $coltab = $tabval[1];
                    $atabon = $tabval[0];
                    $atabvinc = $tabsql->Aliastab($tabsvinc);
                    if($atabon == "" OR $atabon == "vazio") {
                        $search = "INNER JOIN $tabsvinc $atabvinc";
                    }
                    else {
                        $search = $tipoinner." JOIN $tabsvinc $atabvinc";
                    }
                    if(key_exists($serach, $tabsql->innertabelas)) {
                    }
                    else {
                        $tabsql->InnerTabelas($this->tabpri, $this->aliastabpri, $this->tabsec, $this->aliastabsec, $tabsvinc, $atabvinc, $atabon, $coltab, $tipoinner);
                    }
                }
            }
        }

        if($_SESSION['wheresql'] == "") {
            $this->whereops = implode("#",$_SESSION['wheresql']);
        }
        else {
            $this->whereops = implode("#",$this->wheresql);
        }

        $count = $this->aliastabsec.".id".$this->tabsec;
        $inner = new Inner();
        $search = $tipoinner." JOIN conf_rel ".$this->AliasTab(conf_rel);
        $tabsql->innertabelas = $inner->sqlinner($tabsql->innertabelas, $this->aliastabpri);

        // verifica se a consulta inclui a tabela de monitoria e pode ser liberada para visualização quando contem falha grave
        if($this->tabpri == "monitoria") {
            $wherecheck = "";
        }
        else  {
            if($this->tabpri != "regmonitoria" && $this->tabsec != "regmonitoria") {
                foreach($tabsql->innertabelas as $innertab) {
                    if(eregi('monitoria',$innertab)) {
                        $wherecheck = "";
                    }
                }
            }
        }
        if(in_array("INNER JOIN relmoni ".$this->Aliastab(relmoni),$tabsql->innertabelas)) {
            $whererel = "AND ".$this->Aliastab(relmoni).".idrelmoni='$this->idrel'";
        }

        if($_SESSION['user_tabela'] == "user_web" && ($this->tabpri == "regmonitoria" || $this->tabpri == "motivo")) {
            $selmotivos = "SELECT * FROM motivo WHERE acesso='E'";
            $eselmotivos = $_SESSION['query']($selmotivos) or die (mysql_error());
            while($lselmotivos = $_SESSION['fetch_array']($eselmotivos)) {
                $idsmotivos[] = $lselmotivos['idmotivo'];
            }
            $whereimp = " AND ".$this->AliasTab(regmonitoria).".idmotivo IN (".implode(",",$idsmotivos).")";
        }
        else {
            $whereimp = "";
        }

        $sqlperc = "SELECT COUNT(DISTINCT($this->aliastabsec.id$this->tabsec)) as result FROM $this->tabpri $this->aliastabpri ".implode(" ", $tabsql->innertabelas)." $where $wherecheck $whererelfiltro $whereimp";
        $esqlperc = $_SESSION['fetch_array']($_SESSION['query']($sqlperc)) or die ('erro na query de consulta do total');
        $sqllinhas = "SELECT DISTINCT($this->aliastabpri.id$this->tabpri) FROM $this->tabpri $this->aliastabpri ".implode(" ", $tabsql->innertabelas)." $where $wherecheck $whererelfiltro";
        $esqllinhas = $_SESSION['query']($sqllinhas) or die ('erro na query de consulta do total');
        $this->linhas = $_SESSION['num_rows']($esqllinhas);
        $this->total = $esqlperc['result'];
        if($this->total > 500) {
            if(is_int($this->total / 500)) {
                $this->pags = ($this->total / 500);
            }
            else {
                $num = ($this->total / 500);
                $this->pags = intval($num) + 1;
            }
        }
        else {
            $this->pags = 1;
        }
        if($colreplace == "") {
        }
        else {
            if($this->tabpri == "aval_plan" || $this->tabpri == "grupo" || $this->tabpri == "subgrupo" || $this->tabpri == "pergunta") {
                $colmoniavalia = array('aval_plan' => 'valor_final_aval', 'grupo' => 'valor_final_grup', 'subgrupo' => 'valor_final_sub','pergunta' => 'valor_final_perg');
                $colplan = array('aval_plan' => 'valor', 'grupo' => 'valor_grupo', 'subgrupo' => 'valor_sub','pergunta' => 'valor_perg');
                $colarrayreplace[$colreplace['posicao']][$colreplace['tipo']][$colreplace['coluna']] = "((SUM(".$this->AliasTab(moniavalia).".valor_resp) / COUNT(DISTINCT(".$this->AliasTab(moniavalia).".".$colperc."))) / ".$this->aliastabpri.".".$colplan[$this->tabpri].") * 100";
            }
            else {
                $colarrayreplace[$colreplace['posicao']][$colreplace['tipo']][$colreplace['coluna']] = "(COUNT(DISTINCT(".$this->aliastabsec.".".$colperc.")) / ($sqlperc) * 100)";
            }
            $this->colunas = array_replace($this->colunas, $colarrayreplace);
        }

        foreach($this->colunas as $kordem => $ordem) { // faz loop das colunas que serão utilizadas no resultado query, organiza a apresentação das colunas
            if($kordem === "groupby") {
                if($this->groupby == "idmonitoria") {
                    $order = "ORDER BY ".$this->AliasTab(monitoria).".idmonitoria DESC";
                }
                else {
                }
                $colsinner[] = $ordem[$this->groupby];
            }
            else {
                foreach($ordem as $tipo) {
                    foreach($tipo as $nomefield => $field) {
                        $colsinner[] = $field." as '".$nomefield."'";
                    }
                }
            }
        }

        $this->sql = "SELECT ".implode(",",  $colsinner)." FROM $this->tabpri $this->aliastabpri ".implode(" ", $tabsql->innertabelas)." $where $wherecheck $whererel $whererelfiltro $whereimp GROUP BY $this->aliastabpri.$this->groupby $order";
    }
}

class RelMonitoria extends InnersMoni { // Classe que Ã© utilizada para colocar os dados nas tabelas geradas na Ã¡rea de RelatÃ³rios de MONITORIA
    public $dadosexp;
    public $dados;

    function MontaTabela() { // funÃ§Ã£o que monta a tabela para os relatÃ³rios na Ã¡rea de MONITORIA, dentro da funÃ§Ã£o Ã© chamada a Classe Relmonitoria
                             // para colocar os dados da query montada com os INNERS
        $_SESSION['colunas'] = $this->colunas;
        $sql = $this->sql;

        $_SESSION['sqlurl'] = base64_encode($sql);
        //echo $sql;
        $tam = "SELECT tipograf, width, heigth FROM grafrelmoni WHERE idrelmoni='$this->idrel'";
        $etam = $_SESSION['fetch_array']($_SESSION['query']($tam)) or die ("erro na query de consulta do grÃ¡fico relacionado ao relatÃ³rio");
        //$rel = new RelMonitoria();
        if($this->tabpri == "aval_plan" OR $this->tabpri == "grupo" OR $this->tabpri == "subgrupo" Or $this->tabpri == "pergunta") {
            $tabadd = "moniavalia";
        }
        else {
            if($this->tabpri == "status") {
                $tabadd = "rel_fluxo";
            }
            else {
                $tabadd = $this->tabpri;
            }
        }
        $this->RelMoniSQL($sql, $this->colunas, $this->idrel, $this->addwhere, $tabadd, $this->groupby, $this->proxrel, $this->r, $this->whereops, $this->colunasapres);
        $options = array("output-xhtml" => true, "clean" => true);
        $_SESSION['dadosexp'] = tidy_parse_string($this->dados, $options,'UTF8');
        $_SESSION['dadosexp'] = tidy_get_output($_SESSION['dadosexp']);
        ?>
        <script type="text/javascript">
            function popupgraf (largura,altura) {
                window.open("<?php echo "/monitoria_supervisao/users/grafico.php?idrel=$this->idrel";?>",'GRAFICO','width='+largura+',height='+altura+',scrollbars=NO,Resizable=NO,Menubar=NO,Scrollbars=NO, Location=NO');
            }
        </script>
        <?php
        echo "<br />";
        if($this->pags > 1) {
            echo "<table width=\"auto\" style=\"margin:auto\" id=\"tabpag\">";
            echo "<tr bgcolor=\"#FFFFFF\">";
                for($i = 1;$i <= $this->pags;$i++) {
                    if($i == 1) {
                        $limit = "0,500";
                    }
                    else {
                        $limit = (($i - 1) * 500) .",500";
                    }
                    echo "<td style=\"background-color:#FFF; border:2px solid #3CF; text-align:center ;width:30px; height:20px; font-size:12px\"><a id=\"relhref\" href=\"#\" name=\"inicio.php?menu=monitoria&rel=$this->nomerel&pag=$i&limit=$limit\"><li style=\"list-style:none;\">$i</li></a></td>";
                }
            echo "</tr>";
            echo "</table>";
            echo "<br />";
        }

        echo "<table width=\"1024\" id=\"visualiza\">";
            echo "<tr>";
                echo "<td width=\"15\"></td>";
                if($etam['tipograf'] == "") {
                    $width = "20";
                }
                else {
                    $width = "6";
                    echo "<td align=\"center\" width=\"240\" $style><input onclick=\"javascript:popupgraf('".($etam['width']+10)."','".($etam['heigth']+10)."')\" type=\"button\" style=\"width:200px; height: 17px; background-color: #FFCC33; border: 1px solid #333\" value=\"GRÁFICO\" /></td>";
                }
                echo "<td width=\"".$width."\"></td>";
                echo "<td align=\"center\" width=\"240\"><a style=\"text-decoration:none; coloR: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=$this->nomerel\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333\">EXCEL</div></a></td>";
                echo "<td width=\"".$width."\"></td>";
                echo "<td align=\"center\" width=\"240\"><a style=\"text-decoration:none; coloR: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=WORD&nome=$this->nomerel\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333\">WORD</div></a></td>";
                echo "<td width=\"".$width."\"></td>";
                echo "<td align=\"center\" width=\"240\"><a style=\"text-decoration:none; coloR: #000;\" target=\"blank\" href=\"/monitoria_supervisao/users/export.php?tipo=PDF&nome=$this->nomerel\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333\">PDF</div></a></td>";
                echo "<td width=\"15\"></td>";
            echo "</tr>";
        echo "</table><br />";
		?>
        <table width="400">
            <tr>
            	<td width="97" class="corfd_coltexto"><strong>quantidade:</strong></td>
                <td width="99" class="corfd_colcampos" align="center"><strong><?php echo $this->total;?></strong></td>
                <td width="97" class="corfd_coltexto"><strong>linhas:</strong></td>
                <td width="99" class="corfd_colcampos" align="center"><strong><?php echo $this->linhas;?></strong></td>
            </tr>
        </table>
        <!-- <script type="text/javascript">
            $(document).ready(function() {
              $("tablesorter").tablesorter();
            })
        </script>-->
        <?php

        $this->PrintRelmoni();
        $this->r = 0;
        $i++;
    }

    function RelMoniSQL($sql, $colunas, $idrel, $addwhere, $tabadd, $groupby, $proxrel, $r, $whereop, $colapres) { // funÃ§Ã£o chamada dentro da funÃ§Ã£o MotaTabela da Classe InnesrMoni
        $tabmoni = $this->AliasTab($this->tabsec);
        $rel = $sql;
        $linhas = 0;
        //echo $rel;
        $this->dados .= "<table width=\"1000\" id=\"tabrel\" class=\"tablesorter\">";
            $this->dados .= "<thead>";
            $tam = 1000 / count($colunas);
            foreach($colapres as $nhead) {
                $this->dados .= "<th width=\"".$tam."px\" align=\"center\" class=\"corfd_coltexto\">".$nhead."</th>";
            }
            $this->dados .= "</thead>";
            $this->dados .= "<tbody>";
                $erel = $_SESSION['query']($rel) or  die ("erro na query de consulta do resultado do relatório");
                while($lerel = $_SESSION['fetch_array']($erel)) {
                    $linhas++;
                    $col = 0;
                    if($this->tabpri == "status") {
                        $valadd = $lerel['idatustatus'];
                    }
                    else {
                        $valadd = $lerel[$groupby];
                    }
                    if($this->tabpri == "regmonitoria") {
                         $contest = 1;
                         $selcontest = "SELECT count(*) as r FROM contestreg c
                                        INNER JOIN regmonitoria r ON r.idfila = c.idfila
                                        WHERE idregmonitoria='".$lerel['idregmonitoria']."'";
                         $eselcontest = $_SESSION['fetch_array']($_SESSION['query']($selcontest)) or die (mysql_error());
                         if($eselcontest['r'] >= 1) {
                              $cor = "style=\"background-color:#EF9294\"";
                              $onover = "EF9294";
                         }
                         else {
                              if(array_search("data", $colapres)) {
                                   $coldata = "data";
                              }
                              if(array_search("DATA", $colapres)) {
                                   $coldata = "DATA";
                              }
                              $data = $lerel[$coldata];
                              $sqldata = "SELECT DATEDIFF(CURDATE(),'$data') as r";
                              $esqldata = $_SESSION['fetch_array']($_SESSION['query']($sqldata)) or die (mysql_error());
                              if($esqldata['r'] >= 2) {
                                   $contest = 0;
                              }
                              else {
                                   $contest = 1;
                              }
                              $cor = "class=\"corfd_colcampos\"";
                              $onover = "FFFFFF";
                         }
                    }
                    else {
                         $cor = "class=\"corfd_colcampos\"";
                         $onover = "FFFFFF";
                    }
                    $this->dados .= "<tr id=\"tr_".$idrel."_".$linhas."\"  $cor onmouseover=\"javascript:this.style.backgroundColor='#C2E9FF'\" onmouseout=\"javascript:this.style.backgroundColor='#$onover'\">";
                    foreach($colunas as $dados) {
                        foreach($dados as $tpcol => $tipo) {
                            if(eregi("idrel_filtros",$tipo)) {
                                $id = $lerel[$tpcol];
                                $f = 0;
                                $colfg = $this->Aliastab(rel_filtros).".idrel_filtros";
                                foreach($this->wheresql as $w) {
                                    if(eregi('filtro_',$w)) {
                                        $f++;
                                    }
                                    else {
                                    }
                                }
                                if($f >= 1) {
                                }
                                else {
                                    $wherefg = "AND ".$colfg."='$id'";
                                }
                            }
                            if(eregi("idoperador",$tipo)) {
                                $id = $lerel[$tpcol];
                                $colfg = $this->Aliastab(operador).".idoperador";
                                if(key_exists("oper",$this->wheresql)) {
                                }
                                else {
                                    $wherefg = "AND ".$colfg."='$id'";
                                }
                            }
                            if(eregi("idsuper_oper",$tipo)) {
                                $id = $lerel[$tpcol];
                                $colfg = $this->Aliastab(super_oper).".idsuper_oper";
                                if(key_exists("super_oper",$this->wheresql)) {
                                }
                                else {
                                    $wherefg = "AND ".$colfg."='$id'";
                                }
                            }
                            if(eregi("idmonitor",$tipo)) {
                                $id = $lerel[$tpcol];
                                $colfg = $this->Aliastab(operador).".idoperador";
                                if(key_exists("nomemonitor",$this->wheresql)) {
                                }
                                else {
                                    $wherefg = "AND ".$colfg."='$id'";
                                }
                            }
                            foreach($tipo as $colapres => $colquery) {
                                if($colapres != $groupby) {
                                    if(eregi("$tabmoni.idmonitoria",$colquery) && !eregi("$tabmoni.idmonitoriavinc",$colquery)) {
                                        $cidmoni = key($tipo);
                                        $idmoni = $lerel[$colapres];
                                    }
                                    if(key($dados) == "DATA") {
                                        $val = banco2data($lerel[$colapres]);
                                    }
                                    else {
                                        if($lerel[$colapres] == "" && !in_array($this->AliasTab(formulas).".formula",$tipo)) {
                                            $val = "--";
                                        }
                                        else {
                                            if(is_numeric($lerel[$colapres]) && !eregi("qtdefg",$colquery)) {
                                                if(eregi(".",$lerel[$colapres])) {
                                                    $val = round($lerel[$colapres],2);
                                                }
                                                else {
                                                    $val = $lerel[$colapres];
                                                }
                                            }
                                            else {
                                                if(eregi($this->AliasTab(formulas).".formula",$colquery)) {
                                                    $calc = new Formulas();
                                                    $calc->formula = $lerel[$colapres];
                                                    $calc->colcons = $this->Aliastab($this->tabpri).".$groupby";
                                                    $calc->idmonitoria = $lerel[$groupby];
                                                    $calc->tab = "monitoria";
                                                    $calc->tabavalia = "moniavalia";
                                                    if($groupby == "idmonitoria") {
                                                        $selplan = "SELECT idplanilha FROM moniavalia WHERE idmonitoria='$calc->idmonitoria'";
                                                        $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta do idplanilha da monitoria");
                                                    }
                                                    $val = $calc->Calc_formula('',$eselplan['idplanilha'],$this->wheresql);
                                                    $val = number_format($val,2);
                                                }
                                                else {
                                                    if(array_key_exists("QTDEFG",$tipo) && $this->tabpri != "monitoria") {
                                                        $partquery = explode("FROM",$sql);
                                                        $partgroup = explode("GROUP BY",$partquery[1]);
                                                        $selfg = "SELECT SUM(".$this->AliasTab(monitoria).".qtdefg) as qtde FROM ".$partgroup[0]." $wherefg AND ".$this->AliasTab(monitoria).".qtdefg>0";
                                                        $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selfg)) or die ("erro na consulta da quantidade de falhas graves");
                                                        $val = $eselfg['qtde'];
                                                    }
                                                    else {
                                                        $val = $lerel[$colapres];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    $col++;
                                    $pcolumn = explode(".",$colquery);
                                    $column = $pcolumn[1];
                                    if($column == "idmonitoria") {
                                        if($_SESSION['user_tabela'] == "user_web" && $_SESSION['usuarioperfil'] == "07" && $_SESSION['selbanco'] == "monitoria_itau") {
                                            $this->dados .= "<td align=\"center\">".wordwrap($val,100,"br />\n")."</td>";
                                        }
                                        else {
                                            $this->dados .= "<td align=\"center\"><a href=\"inicio.php?menu=idmoni&idmoni=".$val."\">".wordwrap($val,100,"br />\n")."</a></td>";
                                        }
                                    }
                                    else {
                                        if($r == 1 AND $col == 1) {
                                            $this->dados .= "<td align=\"center\"><a id=\"relhref\" href=\"#\" name=\"inicio.php?menu=monitoria&add=".$addwhere."&valadd=".$valadd."&tabadd=".$tabadd."&proxrel=".$proxrel."&group=".$groupby."&opcoes=".$whereop."\">".wordwrap($val,100,"br />\n")."</a></td>";
                                        }
                                        else {
                                            if(eregi("$tabmoni.gravacao",$colquery) || eregi($this->AliasTab('fila_grava').".camaudio",$colquery)) {
                                                $root = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                                                if($_SESSION['user_tabela'] == "user_web" && $_SESSION['usuarioperfil'] == "07") {
                                                    $this->dados .= "<td align=\"center\"><div>";
                                                    $audio = explode("/",$val);
                                                    $partaudio = $audio[count($audio) - 1];
                                                    $extaudio = pathinfo($partaudio,PATHINFO_EXTENSION);
                                                    if($extaudio == "wav" OR $extaudio == "WAV") {
                                                        //$this->dados .= "<a href=\"".str_replace($root, "", $val)."\" target=\"_blank\">DOWNLOAD AUDIO</a>";
                                                        $this->dados .= "<EMBED SRC=\"".str_replace($root, "", $val)."\" autostart=\"false\" loop=\"false\" WIDTH=\"300\" HEIGHT=\"40\">
                                                        <object type=\"application/x-shockwave-flash\" data=\"/monitoria_supervisao/audio-player/player.swf\" id=\"audioplayer1\" height=\"24\" width=\"280\">
                                                        <param name=\"movie\" value=\"/monitoria_supervisao/audio-player/player.swf\">
                                                        <param name=\"FlashVars\" value=\"playerID=audioplayer2&soundFile=".str_replace($root, "", $val)."\">
                                                        <param name=\"quality\" value=\"high\">
                                                        <param name=\"menu\" value=\"false\">
                                                        <param name=\"wmode\" value=\"transparent\"></object>";
                                                    }
                                                    else {
                                                        $this->dados .= "<object type=\"application/x-shockwave-flash\" data=\"/monitoria_supervisao/audio-player/player.swf\" id=\"audioplayer1\" height=\"24\" width=\"280\">
                                                        <param name=\"movie\" value=\"/monitoria_supervisao/audio-player/player.swf\">
                                                        <param name=\"FlashVars\" value=\"playerID=audioplayer2&soundFile=".str_replace($root, "", $val)."\">
                                                        <param name=\"quality\" value=\"high\">
                                                        <param name=\"menu\" value=\"false\">
                                                        <param name=\"wmode\" value=\"transparent\"></object>";
                                                    }
                                                    $this->dados .= "</div></td>";
                                                }
                                                else {
                                                    $this->dados .= "<td align=\"center\"><a href=\"".str_replace($root, "", $val)."\" target=\"_blank\">DOWNLOAD AUDIO</a></td>";
                                                }
                                            }
                                            else {
                                                if($column == "alertas") {
                                                    $this->dados .= "<td align=\"center\">";
                                                    if($val != "--") {
                                                        $selalertas = "SELECT * FROM alertas WHERE idalertas IN ($val)";
                                                        $ealertas = $_SESSION['query']($selalertas) or die ("erro na query de consulta dos alertas");
                                                        while($lalertas = $_SESSION['fetch_array']($ealertas)) {
                                                            $this->dados .= "<input name=\"alerta[]\" value=\"\" title=\"".$lalertas['nomealerta']."\" style=\"width:15px; height:15px;border:0px;background-color:#".$lalertas['coralerta']."\" type=\"text\"/>&nbsp ";
                                                        }
                                                    }
                                                    $this->dados .= "</td>";
                                                }
                                                else {
                                                     if($column == "idregmonitoria" && $_SESSION['selbanco'] == "monitoria_itau" && $contest == 1) { // limitando a permissão de contestaçao de improdutiva somente ao cliente Monitoria_itau
                                                       $this->dados .= "<td align=\"center\"><a style=\"text-decoration:none;\" href=\"inicio.php?menu=idreg&idreg=$val\">$val</a></td>";
                                                     }
                                                     else {
                                                       $newval = wordwrap($val,50,"<br />\n");
                                                       $this->dados .= "<td align=\"center\">".$newval."</td>";
                                                     }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $this->dados .= "</tr>";
                }
            $deltmp = "DROP TABLE IF EXISTS tmp_avg";
            $edeltmp = $_SESSION['query']($deltmp) or die (mysql_error());
            $this->dados .= "</tbody>";
        $this->dados .= "</table>";
    }

    function PrintRelmoni() {
        echo $this->dados;
    }
}

class RelGerenciais extends Grafico {
    public $where;
    public $sql;
    public $from;
    public $inners;
    public $idrelresult;
    public $idrelcab;
    public $idrelcorpo;
    public $vars;
    public $idsrelfiltros;
    public $idplan;
    public $sqlop;
    public $sqlsup;
    public $sqlinner;

    // variÃ¡veis da classe para motagem do relatÃ³rio
    public $dadosrel;
    public $dados;
    public $idsoper;
    public $idsuper;
    public $frequencia;
    public $apresdados;
    public $filtros;
    public $idtabulacao;

    function where_sql() {
        $tabsql = new TabelasSql();
        $alias = array('dtinimoni' => $tabsql->AliasTab(monitoria), 'dtfimmoni' => $tabsql->AliasTab(monitoria),'dtinictt' => $tabsql->AliasTab(monitoria),'dtfimctt' => $tabsql->AliasTab(monitoria),'idrel_filtros' => $tabsql->AliasTab(monitoria),'idplanilha' => $tabsql->AliasTab(moniavalia),'idaval_plan' => $tabsql->AliasTab(moniavalia), 'idgrupo' => $tabsql->AliasTab(moniavalia),
            'idsubgrupo' => $tabsql->AliasTab(moniavalia),'idpergunta' => $tabsql->AliasTab(moniavalia),'idresposta' => $tabsql->AliasTab(moniavalia),'operador' => $tabsql->AliasTab(operador),'super_oper' => $tabsql->AliasTab(super_oper),'monitor' => $tabsql->AliasTab(monitor),'qtdefg' => $tabsql->AliasTab(monitoria),'qtdefgm' => $tabsql->AliasTab(monitoria),'idmonitoria' => $tabsql->AliasTab(monitoria),'idfluxo' => $tabsql->AliasTab(fluxo),'idatustatus' => $tabsql->AliasTab(rel_fluxo),'idindice' => $tabsql->AliasTab(indice),'idintervalo_notas' => $tabsql->AliasTab(intervalo_notas));
        foreach($this->vars as $kv => $v) {
            if(eregi('dtinimoni',$kv) OR eregi('dtfimmoni',$kv)) {
                if(in_array('data',$this->where)) {
                }
                else {
                    $this->where['data'] = "$alias[$kv].data BETWEEN '".data2banco($this->vars['dtinimoni'])."' AND '".data2banco($this->vars['dtfimmoni'])."'";
                }
            }
            if(eregi('dtinictt',$kv) OR eregi('dtfimctt',$kv)) {
                if(in_array('datactt',$this->where)) {
                }
                else {
                    $this->where['datactt'] = "$alias[$kv].datactt BETWEEN '".data2banco($this->vars['dtinictt'])."' AND '".data2banco($this->vars['dtfimctt'])."'";
                }
            }
            if(!eregi('dtinimoni',$kv) && !eregi('dtfimmoni',$kv) && !eregi('dtinictt',$kv) && !eregi('dtfimctt',$kv)) {
                if(eregi('idrel_filtros',$kv)) {
                    $this->where[$kv] = $alias[$kv].".".$kv." IN (".$v.")";
                }
                else {
                    if(eregi("filtro_",$kv)) {
                    }
                    else {
                        if($kv == "nomemonitor") {
                            $this->where[$kv] = $alias['monitor'].".".$kv."='".$v."'";
                        }
                        else {
                            if($kv == "qtdefg" OR $kv == "qtdefgm") {
                                $this->where[$kv] = $alias[$kv].".".$kv.">='".$v."'";
                            }
                            else {
                                if($kv == "atustatus") {
                                    $this->where[$kv] = $alias['idatustatus'].".id".$kv."='".$v."'";
                                }
                                else {
                                    $this->where[$kv] = $alias[$kv].".".$kv."='".$v."'";
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    function valor_campo($campo,$idmonitoria,$formula) {
        $tabsql = new TabelasSql();
        $amoni = $tabsql->AliasTab(monitoria);
        $amoniava = $tabsql->AliasTab(moniavalia);
        $ainter = $tabsql->AliasTab(interperiodo);
        $aoper = $tabsql->AliasTab(operador);
        $asuper = $tabsql->AliasTab(super_oper);
        $amonitor = $tabsql->AliasTab(monitor);
        $aperiodo = $tabsql->AliasTab(periodo);
        $afilag = $tabsql->AliasTab(fila_grava);
        $afilao = $tabsql->AliasTab(fila_oper);
        $aplan = $tabsql->AliasTab(planilha);
        $arelaval = $tabsql->AliasTab(rel_aval);
        $aavalplan = $tabsql->AliasTab(aval_plan);
        $agrupo = $tabsql->AliasTab(grupo);
        $asubgrupo = $tabsql->AliasTab(subgrupo);
        $apergunta = $tabsql->AliasTab(pergunta);
        $arelfluxo = $tabsql->AliasTab(rel_fluxo);
        $amonifluxo = $tabsql->AliasTab(monitoria_fluxo);
        $aparam = $tabsql->AliasTab(param_moni);
        $astatus = $tabsql->AliasTab(status);
        $aconf = $tabsql->AliasTab(conf_rel);
        $afluxo = $tabsql->AliasTab(fluxo);
        $aindice = $tabsql->AliasTab(indice);
        $aintervalo = $tabsql->AliasTab(intervalo_notas);

        $inners = array(
                'data' => '',
                'datactt' => "INNER JOIN fila_grava $afilag ON $afilag.idfila_grava = $amoni.idfila",
                'idrel_filtros' => '',
                'idplanilha' => "INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria,INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha",
                'idaval_plan' => "INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria,INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha",
                'idgrupo' => "INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria,INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha",
                'idsubgrupo' => "INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria,INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha,",
                'idpergunta' => "INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria,INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha,",
                'idresposta' => "INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria,INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha,",
                'operador' => "INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador",
                'super_oper' => "INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper",
                'nomemonitor' => "INNER JOIN monitor $amonitor ON $amonitor.idmonitor = $amoni.idmonitor",
                'fg' => '',
                'fgm' => '',
                'idmonitoria' => '',
                'idfluxo' => "INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros,INNER JOIN fluxo $afluxo ON $afluxo.idfluxo = $aconf.idfluxo,
                INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni",
                'atustatus' => "INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros,INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni,
                INNER JOIN monitoria_fluxo $amonifluxo ON $amonifluxo.idmonitoria = $amoni.idmonitoria,INNER JOIN rel_fluxo $arelfluxo ON $arelfluxo.idrel_fluxo = $amonifluxo.idrel_fluxo,
                INNER JOIN status $astatus ON $astatus.idstatus = $arelfluxo.idatustatus",
                'idindice' => "INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros,INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni,
                INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice",
                'idintervalo_notas' => "INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros,INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni,
                INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice,INNER JOIN intervalo_notas $aintervalo ON $aintervalo.idindice = $aparam.idindice");

        $this->sqlinner = array();
        $this->sqlop = array();
        $this->sqlsup = array();
        $this->sqlop[] = "INNER JOIN monitoria $amoni ON $amoni.idoperador = $aoper.idoperador";
        $this->sqlsup[] = "INNER JOIN monitoria $amoni ON $amoni.idsuper_oper = $asuper.idsuper_oper";
        foreach($this->where as $kwhere => $whereinner) {
            if($whereinner != "") {
                $test = explode(",",$inners[$kwhere]);
                if(in_array($test, $innersql)) {
                }
                else {
                    if($test[0] == "") {
                    }
                    else {
                        $innersql[$kwhere] = $test;
                    }
                }
            }
            else {
            }
        }
        foreach($innersql as $knewop => $newop) {
            if($knewop == "operador") {
            }
            else {
                foreach($newop as $op) {
                    if(in_array(trim($op),$this->sqlop)) {
                    }
                    else {
                        $this->sqlop[] = trim($op);
                    }
                }
            }
        }
        foreach($innersql as $knewsup => $newsup) {
            if($knewsup == "super_oper") {
            }
            else {
                foreach($newsup as $sup) {
                    if(in_array(trim($sup),$this->sqlsup)) {
                    }
                    else {
                        $this->sqlsup[] = trim($sup);
                    }
                }
            }
        }
        foreach($innersql as $kisql => $isql) {
            foreach($isql as $is) {
                if(in_array(trim($is),$this->sqlinner)) {
                }
                else {
                    $this->sqlinner[] = trim($is);
                }
            }
        }

        if($campo == 'P') {
            if($idmonitoria != "") {
                $selmoni = "SELECT data FROM monitoria WHERE idmonitoria='$idmonitoria'";
                $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da data da monitoria");
                $datamoni = $eselmoni['data'];
                $selper = "SELECT dataini,datafim, nmes FROM periodo";
                $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta dos periodos");
                while($lselper = $_SESSION['fetch_array']($eselper)) {
                    $per = "SELECT ('$datamoni' >= '".$lselper['dataini']."' AND '$datamoni' <= '".$lselper['datafim']."') as result";
                    $eper = $_SESSION['fetch_array']($_SESSION['query']($per)) or die ("erro na query de comparaÃ§Ã£o da data da monitoria com o perÃ­odo");
                    if($eper['result'] >= 1) {
                        $valor[] = $lselper['nmes']."-".substr($lselper['datafim'],0,4);
                    }
                    else {
                    }
                }
            }
            else {
                $selper = "SELECT * FROM periodo WHERE dataini >= '".data2banco($this->vars['dtinictt'])."' AND dataini <= '".data2banco($this->vars['dtfimctt'])."'";
                $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta do periodo");
                while($lselper = $_SESSION['fetch_array']($eselper)) {
                    $valor[] = $lselper['nmes']."-".substr($lselper['datafim'],0,4);
                }
            }
            return implode(", ",$valor);
        }
        if($campo == 'F') {
            $selform = "SELECT formula FROM formulas WHERE idrelresult='$this->idrelresult'";
            $eselform = $_SESSION['fetch_array']($_SESSION['query']($selform)) or die ("erro na query para selecionar a formula utilizada no relatÃ³rio");
            if($this->vars['idplanilha'] != "") {
                $idsplan[] = $this->vars['idplanilha'];
            }
            else {
                $idsrel = explode(",",$this->vars['idrel_filtros']);
                $selplans = "SELECT idplanilha_moni FROM conf_rel cr INNER JOIN rel_filtros rf ON rf.idrel_filtros=cr.idrel_filtros WHERE cr.idrel_filtros IN (".$this->vars['idrel_filtros'].")";
                $eselplans = $_SESSION['query']($selplans) or die ("erro na query de consulta das planilhas relacionadas");
                while($lselplans = $_SESSION['fetch_array']($eselplans)) {
                    $plans = explode(",",$lselplans['idplanilha_moni']);
                    foreach($plans as $p) {
                        if(in_array($p,$idsplan)) {
                        }
                        else {
                            $idsplan[] = $p;
                        }
                    }
                }
            }
            $form = new Formulas();
            $form->formula = $formula;
            foreach($idsplan as $idplan) {
                $form->tab = "monitoria";
                $form->tabavalia = "moniavalia";
                $selnplan = "SELECT DISTINCT(idplanilha), descriplanilha FROM planilha WHERE idplanilha='$idplan'";
                $eselnplan = $_SESSION['fetch_array']($_SESSION['query']($selnplan)) or die ("erro na query de consulta do nome da planilha");
                $f[] = $eselnplan['descriplanilha']." - ".number_format($form->Calc_formula('', $idplan, $this->where),2)."<br />";
            }
            return implode("",$f);
        }
        if($campo == "QTMO") {
            $where = "WHERE ".implode(" AND ",$this->where);
            $selmoni = "SELECT COUNT(DISTINCT(".$amoni.".idmonitoria)) as result
                        FROM monitoria ".$amoni." ".implode(" ",$this->sqlinner)." $where";
            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query para consulta a quantidade de monitorias");
            return $eselmoni['result'];
        }
        if($campo == "QTMOP") {
            $where = "WHERE ".implode(" AND ",$this->where);
            $selmoni = "SELECT (COUNT(DISTINCT(".$amoni.".idmonitoria)) / (SELECT COUNT(DISTINCT(".$aoper.".idoperador)) as result FROM operador ".$aoper." ".
                        implode(" ",$this->sqlop)." $where)) as result FROM monitoria ".$amoni." ".implode(" ",$this->sqlinner)." $where";
            $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query para consulta a quantidade de monitorias");
            return number_format($eselmoni['result'],2);
        }
        if($campo == "QTOP") {
            $where = "WHERE ".implode(" AND ",$this->where);
            $seloper = "SELECT COUNT(DISTINCT(".$aoper.".idoperador)) as result FROM operador ".$aoper." ".implode(" ",$this->sqlop)." $where";
            $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die ("erro na query de consulta da quantidade de operadores");
            return $eseloper['result'];
        }
        if($campo == "QTSUPER") {
            $where = "WHERE ".implode(" AND ",$this->where);
            $selsuper= "SELECT COUNT(DISTINCT(".$asuper.".idsuper_oper)) as result FROM super_oper ".$asuper." ".implode(" ",$this->sqlsup)." $where";
            $eselsuper = $_SESSION['fetch_array']($_SESSION['query']($selsuper)) or die ("erro na query de consulta da quantidade de supervisores");
            return $eselsuper['result'];
        }
        if($campo == "MO") {
            $where = "WHERE ".implode(" AND ",$this->where);
            $selmoper = "SELECT DISTINCT(".$amoni.".idmonitoria), AVG(".$amoniava.".valor_final_aval) as valor FROM monitoria ".$amoni." ".implode(" ",$this->sqlinner)." $where";
            $emoper = $_SESSION['fetch_array']($_SESSION['query']($selmoper)) or die ("erro na query de consulta das notas dos operadores");
            return number_format($emoper['valor'],2);
        }
        if($campo == "QTFG" OR $campo == "QTFGM" OR $campo == "QTFGI" OR $campo == "QTFGMI") {
            if($campo == "QTFG" OR $campo == "QTFGI") {
                if($campo == "QTFG") {
                    $retorno = "(SELECT COUNT(DISTINCT(".$amoni.".idmonitoria)) as result";
                }
                else {
                    $retorno = "SELECT SUM(".$amoni.".qtdefg) as result FROM monitoria ".$amoni." WHERE idmonitoria IN (SELECT DISTINCT(".$amoni.".idmonitoria)";
                }
            }
            if($campo == "QTFGM" OR $campo == "QTFGMI") {
                if($campo == "QTFGM") {
                    $retorno = "(SELECT COUNT(DISTINCT(".$amoni.".idmonitoria)) as result";
                }
                else {
                    $retorno = "SELECT SUM(".$amoni.".qtdefg) as result FROM monitoria ".$amoni." WHERE idmonitoria IN (SELECT DISTINCT(".$amoni.".idmonitoria)";
                }
            }
            if(($campo == "QTFG" OR $campo == "QTFGI") && !key_exists("qtdefg", $this->where)) {
                $where = "WHERE ".implode(" AND ",$this->where)." AND ".$amoni.".qtdefg >= 1";
            }
            if(($campo == "QTFGM" OR $campo == "QTFGMI") && !key_exists("qtdefgm", $this->where)) {
                $where = "WHERE ".implode(" AND ",$this->where)." AND ".$amoni.".qtdefgm >= 1";
            }
            $selfg = "$retorno FROM monitoria ".$amoni." ".implode(" ",$this->sqlinner)." $where)";
            $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selfg)) or die (" erro na query de consulta da quantidade de Falhas Graves");
            if($eselfg['result'] == "") {
                $valor = 0;
            }
            else {
                $valor = $eselfg['result'];
            }
            return $valor;
        }
        if($campo == "QTFGOP" OR $campo == "QTFGMOP" OR $campo == "QTFGIOP" OR $campo == "QTFGMIOP") {
            if($campo == "QTFGOP" OR $campo == "QTFGIOP") {
                if($campo == "QTFGOP") {
                    $retorno = "(SELECT (COUNT(DISTINCT(".$amoni.".idmonitoria)) / (SELECT COUNT(DISTINCT(".$aoper.".idoperador)) as result FROM operador ".$aoper." ".implode(" ",$this->sqlop).")) as result";
                }
                else {
                    $retorno = "SELECT (SUM(".$amoni.".qtdefg) / (SELECT COUNT(DISTINCT(".$aoper.".idoperador)) as result FROM operador ".$aoper." ".implode(" ",$this->sqlop).")) as result
                        FROM monitoria ".$amoni." WHERE idmonitoria IN (SELECT DISTINCT(".$amoni.".idmonitoria)";
                }
            }
            if($campo == "QTFGMOP" OR $campo == "QTFGMIOP") {
                if($campo == "QTFGMOP") {
                    $retorno = "(SELECT (COUNT(DISTINCT(".$amoni.".idmonitoria)) / (SELECT COUNT(DISTINCT(".$aoper.".idoperador)) as result FROM operador ".$aoper." ".implode(" ",$this->sqlop).")) as result";
                }
                else {
                    $retorno = "SELECT (SUM(".$amoni.".qtdefg) / (SELECT COUNT(DISTINCT(".$aoper.".idoperador)) as result FROM operador ".$aoper." ".implode(" ",$this->sqlop).")) as result
                                FROM monitoria ".$amoni." WHERE idmonitoria IN (SELECT DISTINCT(".$amoni.".idmonitoria)";
                }
            }
            if(($campo == "QTFGOP" OR $campo == "QTFGIOP") && !key_exists("qtdefg", $this->where)) {
                $where = "WHERE ".implode(" AND ",$this->where)." AND ".$amoni.".qtdefg >= 1";
            }
            if(($campo == "QTFGMOP" OR $campo == "QTFGMIOP") && !key_exists("qtdefgm", $this->where)) {
                $where = "WHERE ".implode(" AND ",$this->where)." AND ".$amoni.".qtdefgm >= 1";
            }
            $selfg = "$retorno FROM monitoria ".$amoni." ".implode(" ",$this->sqlinner)." $where)";
            $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selfg)) or die (" erro na query de consulta da quantidade de Falhas Graves por operador");
            if($eselfg['result'] == "") {
                $valor = 0;
            }
            else {
                $valor = number_format($eselfg['result'],2);
            }
            return $valor;
        }
    }

    function dados_apres($frequencia,$idcorpo,$tipodados,$dados,$idformula,$agrupa,$apres,$tipograf) {
        $tabsql = new TabelasSql();

        // aliases das tabelas usadas
        $amoni = $tabsql->AliasTab(monitoria);
        $amonitab = $tabsql->AliasTab(monitabulacao);
        $atabula = $tabsql->AliasTab(tabulacao);
        $amoniava = $tabsql->AliasTab(moniavalia);
        $ainter = $tabsql->AliasTab(interperiodo);
        $aoper = $tabsql->AliasTab(operador);
        $asuper = $tabsql->AliasTab(super_oper);
        $amonitor = $tabsql->AliasTab(monitor);
        $aperiodo = $tabsql->AliasTab(periodo);
        $afilag = $tabsql->AliasTab(fila_grava);
        $afilao = $tabsql->AliasTab(fila_oper);
        $aplan = $tabsql->AliasTab(planilha);
        $arelaval = $tabsql->AliasTab(rel_aval);
        $aavalplan = $tabsql->AliasTab(aval_plan);
        $agrupo = $tabsql->AliasTab(grupo);
        $asubgrupo = $tabsql->AliasTab(subgrupo);
        $apergunta = $tabsql->AliasTab(pergunta);
        $apergtab = $tabsql->AliasTab(perguntatab);
        $aresptab = $tabsql->AliasTab(respostatab);
        $arelfiltros = $tabsql->AliasTab(rel_fitlros);
        $aupload = $tabsql->AliasTab(upload);
        $aconf = $tabsql->AliasTab(conf_rel);
        $aparam = $tabsql->AliasTab(param_moni);
        $arelfluxo = $tabsql->AliasTab(rel_fluxo);
        $amonifluxo = $tabsql->AliasTab(monitoria_fluxo);
        $afluxo = $tabsql->AliasTab(fluxo);
        $astatus = $tabsql->AliasTab(status);
        $aindice = $tabsql->AliasTab(indice);
        $aintervalo = $tabsql->AliasTab(intervalo_notas);
        $aform = $tabsql->AliasTab(formulas);


        $s = 1;
        $this->frequencia = array();
        $af = 0;
        if($tipodados == "CON") {
            $this->frequencia[$af]['PERIODO']['datas'] =  data2banco($this->vars['dtinictt']).",". data2banco($this->vars['dtfimctt']);
        }
        else {
            if($frequencia == "D") {
                $dtini = mktime(0, 0, 0, substr($this->vars['dtinictt'],3,2), substr($this->vars['dtinictt'],0,2), substr($this->vars['dtinictt'],6,4));
                $dtfim = mktime(0, 0, 0, substr($this->vars['dtfimctt'],3,2), substr($this->vars['dtfimctt'],0,2), substr($this->vars['dtfimctt'],6,4));
                for($d = 0; $dtini <= $dtfim; $d++) {
                    $dia = date('d/m/Y',$dtini);
                    $this->frequencia[$dia]['datas'] = data2banco($dia);
                    $dtini = +86400;
                }
            }
            if($frequencia == "S") {
                $nmesant = "";
                $dtini = data2banco($this->vars['dtinictt']);
                $dtfim = data2banco($this->vars['dtfimctt']);
                $selper = "SELECT ".$aperiodo.".nmes,".$ainter.".dataini,".$ainter.".datafim,".$aperiodo.".ano FROM interperiodo $ainter
                           INNER JOIN periodo ".$aperiodo." ON ".$aperiodo.".idperiodo = ".$ainter.".idperiodo
                           WHERE (".$ainter.".dataini <= '$dtini' AND ".$ainter.".datafim >= '$dtini')
                           OR (".$ainter.".dataini >= '$dtini' AND ".$ainter.".datafim <='$dtfim')";
                $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta do intervalo dos perÃ­odos");
                while($lselper = $_SESSION['fetch_array']($eselper)) {
                    if($dados == "IT") {
                        $numcol = 2;
                    }
                    else {
                        $numcol = 3;
                    }
                    if($f < $numcol) {
                    }
                    else {
                        if($apres == "GRAFICO") {
                        }
                        else {
                            $af++;
                            $f = 0;
                        }
                    }
                    $nmes = $lselper['nmes'];
                    if($nmes == $nmesant) {
                        $s++;
                    }
                    else {
                        $s = 1;
                    }
                    $nmesant = $nmes;
                    $this->frequencia[$af][$s."sem. ".$lselper['nmes']]['datas'] = $lselper['dataini'].",".$lselper['datafim'];
                    $f++;
                }
            }
            if($frequencia == "M") {
                $af = 0;
                $dtini = data2banco($this->vars['dtinictt']);
                $dtfim = data2banco($this->vars['dtfimctt']);
                $selper = "SELECT ".$aperiodo.".nmes,".$aperiodo.".dataini,".$aperiodo.".datafim,".$aperiodo.".ano FROM periodo $aperiodo";
                $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta do intervalo dos perÃ­odos");
                while($lselper = $_SESSION['fetch_array']($eselper)) {
                    $confere = "SELECT '$dtini' BETWEEN '".$lselper['dataini']."' AND '".$lselper['datafim']."' as dentro1,'$dtfim' BETWEEN '".$lselper['dataini']."' AND '".$lselper['datafim']."' as dentro2";
                    $econfere = $_SESSION['fetch_array']($_SESSION['query']($confere)) or die ("erro na query de comparaÃ§Ã£o das datas com o perÃ­odo");
                    if($econfere['dentro1'] >= 1 || $econfere['dentro2'] >= 1) {
                        if($dados == "IT") {
                            $numcol = 2;
                        }
                        else {
                            $numcol = 3;
                        }
                        if($f < $numcol) {
                        }
                        else {
                            if($apres == "GRAFICO") {
                            }
                            else {
                                $af++;
                                $f = 0;
                            }
                        }
                        if($econfere['dentro1'] >= 1 && $econfere['dentro2'] >= 1) {
                            $dtinic = $dtini;
                            $dtfimc = $dtfim;
                        }
                        if($econfere['dentro1'] >= 1 && $econfere['dentro2'] >= 0) {
                            $dtinic = $dtini;
                            $dtfimc = $lselper['datafim'];
                        }
                        if($econfere['dentro1'] >= 0 && $econfere['dentro2'] >= 1) {
                            $dtinic = $lselper['dataini'];
                            $dtfimc = $dtfim;
                        }
                        $periodos[$af][$lselper['nmes']] = array($dtinic,$dtfimc);
                        $f++;
                    }
                }
                foreach($periodos as $ka => $a) {
                    foreach($a as $mes => $mdatas) {
                        if($mdatas[0] >= $dtini) {
                            $cdtini = $mdatas[0];
                        }
                        else {
                            $cdtini = $dtini;
                        }
                        if($mdatas[1] <= $dtfim) {
                            $cdtfim = $mdatas[1];
                        }
                        else {
                            $cdtfim = $dtfim;
                        }
                        $this->frequencia[$ka][$mes]['datas'] = $cdtini.",".$cdtfim;
                    }
                }
            }
            if($frequencia == "A") {
                $dtini = data2banco($this->vars['dtinictt']);
                $dtfim = data2banco($this->vars['dtfimctt']);
                $anoini = substr($dtini, 0, 4);
                $anofim = substr($dtfim, 0, 4);
                if($anoini == $anofim) {
                    $this->frequencia[0][$anoini]['datas'] = $dtini.",".$dtfim;
                }
                else {
                    $ano = $anoini;
                    for($a = 0; $ano <= $anofim; $a++) {
                        $selper = "select MIN(dataini) as dataini, MAX(datafim) as datafim, COUNT(*) as result from periodo where YEAR(datafim)='$anoini'";
                        $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta do perÃ­odo do ano");
                        if($eselper['result'] >= 1) {
                            if($f < 3) {
                            }
                            else {
                                if($apres == "GRAFICO") {
                                }
                                else {
                                    $af++;
                                    $f = 0;
                                }
                            }
                            $this->frequencia[$af][$ano]['datas'] = $eselper['dataini'].",".$eselper['datafim'];
                            $ano++;
                        }
                        else {
                        }
                        $f++;
                    }
                }
            }
        }


        //sql para os filtros preenchidos na pagina que gera o relatÃ³rio
        $arrayoper = array('operador','super_oper');
        $arraydados = array('idmonitoria','idrel_filtros','idplanilha', 'idaval_plan','idgrupo','idsubgrupo','idpergunta','idresposta','nomemonitor','super_oper','operador','qtdefg','qtdefgm','indice','intervalo','fluxo','atustatus');
        foreach($this->where as $koper => $oper) {
            if(in_array($koper, $arrayoper)) {
                $woper[] = $oper;
            }
            if(in_array($koper, $arraydados)) {
                $wdados[] = $oper;
            }
        }
        if($woper[0] == "") {
        }
        else {
            $wopersql = "WHERE ".implode(" AND ",$woper);
        }
        if($wdados[0] == "") {
        }
        else {
            $wdadossql = "AND ".implode(" AND ",$wdados);
        }

        if($dados == "MO" OR $dados == "MS") {
            $amoni = $tabsql->AliasTab(monitoria);
            $arel = $tabsql->AliasTab(rel_filtros);
            $aparam = $tabsql->AliasTab(param_moni);
            $aaval = $tabsql->AliasTab(moniavalia);
            $aoper = $tabsql->AliasTab(operador);
            $asuper = $tabsql->AliasTab(super_oper);
            $aup = $tabsql->AliasTab(upload);
            $afluxo = $tabsql->AliasTab(fluxo);
            $amonifluxo = $tabsql->AliasTab(monitoria_fluxo);
            $arelfluxo = $tabsql->AliasTab(rel_fluxo);
            $aindice = $tabsql->AliasTab(indice);
            $anotas = $tabsql->AliasTab(intervalo_notas);
            $tab = array('MO' => 'operador','MS' => 'super_oper');
            if($apres == "GRAFICO") {
                $innero = "INNER JOIN monitoria ".$tabsql->AliasTab(monitoria)." ON ".$tabsql->AliasTab(monitoria).".id".$tab[$dados]." = ".$tabsql->AliasTab($tab[$dados]).".id".$tab[$dados]."";
                $innerf = "INNER JOIN monitoria ".$tabsql->AliasTab(monitoria)." ON ".$tabsql->AliasTab(monitoria).".idfila = ".$tabsql->AliasTab($fi)."id".$tabsql->AliasTab($fi)."";
            }
            else {
                $innero = "";
            }
            $this->idsoper = array();

            $seloper = "SELECT ".$tabsql->AliasTab($tab[$dados]).".id$tab[$dados], ".$tabsql->AliasTab($tab[$dados]).".$tab[$dados] FROM $tab[$dados] ".$tabsql->AliasTab($tab[$dados])." $innero ORDER BY ".$tab[$dados]."";
            $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consulta dos operadores");
            while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                $filas = array('fg' => 'fila_grava','fo' => 'fila_oper');
                foreach($filas as $kfi => $fi) {
                    if($apres == "GRAFICO") {
                        $selfila = "SELECT $amoni.id$tab[$dados], COUNT(*) as result from monitoria $amoni
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN moniavalia $aaval ON $aaval.idmonitoria = $amoni.idmonitoria
                                    INNER JOIN monitoria_fluxo $amonifluxo ON $amonifluxo.idmonitoria_fluxo = $amoni.idmonitoria_fluxo
                                    INNER JOIN rel_fluxo $arelfluxo ON $arelfluxo.idrel_fluxo = $amonifluxo.idrel_fluxo
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    WHERE ".implode(" AND ",$this->where)." AND $amoni.id$tab[$dados]='".$lseloper['id'.$tab[$dados]]."'";
                    }
                    else {
                        if($fi == "fila_grava") {
                            $cdata = "$kfi.datactt";
                        }
                        else {
                            $cdata = "$kfi.data";
                        }
                        $selfila = "SELECT $kfi.id$tab[$dados], COUNT(*) as result FROM $fi $kfi
                                    INNER JOIN monitoria $amoni ON $amoni.idfila = $kfi.id$fi
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN moniavalia $aaval ON $aaval.idmonitoria = $amoni.idmonitoria
                                    INNER JOIN monitoria_fluxo $amonifluxo ON $amonifluxo.idmonitoria_fluxo = $amoni.idmonitoria_fluxo
                                    INNER JOIN rel_fluxo $arelfluxo ON $arelfluxo.idrel_fluxo = $amonifluxo.idrel_fluxo
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    WHERE ".implode(" AND ",$this->where)." AND $kfi.id$tab[$dados]='".$lseloper['id'.$tab[$dados]]."'";
                    }
                    $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die ("erro na query de consulta da fila com operador");
                    if($eselfila['result'] >= 1) {
                        if(in_array($eselfila['id'.$tab[$dados]],$this->idsoper)) {
                        }
                        else {
                            $this->idsoper[$lseloper[$tab[$dados]]] = $lseloper['id'.$tab[$dados]];
                        }
                    }
                    else {
                    }
                }
            }
        }
        else {
        }

        $this->dadosrel = array();
        //if($apres == "TABELA") {
            if($dados == "F") {
                $this->idplan = $this->vars['idplanilha'];
                $this->apresdados = array('descri' => 'descri','qtde_moni' => 'qtde_moni','resultado' => 'resultado');
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            if($frequencia == "D") {
                                $datas = $amoni.".datactt='".$f."'";
                            }
                            else {
                                $between = explode(",",$f);
                                $datas = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                            }
                            $form = new Formulas();
                            $selform = "SELECT nomeformulas, formula FROM formulas WHERE idformulas='$idformula'";
                            $eselform = $_SESSION['fetch_array']($_SESSION['query']($selform)) or die ("erro na query de consulta da formula vinculada");
                            $form->formula = $eselform['formula'];
                            $w = $this->where;
                            if($tipodados == "C" AND ($agrupa != "MS" && $agrupa != "MO" && $agrupa != "")) {
                                $selfiltros = "SELECT nomefiltro_dados, idfiltro_dados FROM filtro_dados fd
                                               INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes
                                               WHERE nomefiltro_nomes LIKE '%".$agrupa."%'";
                                $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta do nome dos fitlros");
                                while($lselfiltros = $_SESSION['fetch_array']($eselfiltros)) {
                                    $this->filtros[$lselfiltros['idfiltro_dados']] = 'id_'.strtolower($agrupa).",".$lselfiltros['nomefiltro_dados'];
                                }
                            }
                            if($this->filtros != "") {
                                foreach($this->filtros as $kcol => $kval) {
                                    $field = explode(",",$kval);
                                    $idsrel = array();
                                    $idrel = "SELECT idrel_filtros FROM rel_filtros WHERE ".$field[0]."='".$kcol."'";
                                    $eidrel = $_SESSION['query']($idrel) or die ("erro na query de consulta dos filtros relacionados ao agrupamento");
                                    while($lidrel = $_SESSION['fetch_array']($eidrel)) {
                                        $seluser = "SELECT COUNT(*) as result FROM ".str_replace("_", "", $_SESSION['user_tabela'])."filtro WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."' AND idrel_filtros='".$lidrel['idrel_filtros']."'";
                                        $eselreluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ('erro na query de consulta dos filtros do usuÃ¡rio');
                                        if($eselreluser['result'] >= 1) {
                                            $idsrel[] = $lidrel['idrel_filtros'];
                                        }
                                        else {
                                        }
                                    }
                                    $idsrel = "$amoni.idrel_filtros IN ('".implode("','",$idsrel)."')";
                                    $w['idrel_filtros'] = $idsrel;
                                    $w['datactt'] = $datas;
                                    $where = implode(" AND ",$w);
                                    $form->tab = "monitoria";
                                    $form->tabavalia = "moniavalia";
                                    $calc = $form->Calc_formula('', $this->idplan, $w);
                                    $selqtde = "SELECT COUNT(DISTINCT($amoni.idmonitoria)) as result FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $where";
                                    $eselqtde = $_SESSION['fetch_array']($_SESSION['query']($selqtde)) or die ("erro na query de consulta da quantidade de monitorias");
                                    if($eselqtde['result'] == "") {
                                        $result = "--";
                                    }
                                    else {
                                        $result = $eselqtde['result'];
                                    }

                                    $chfiltro = $field[1];
                                    $this->dadosrel[$idformula][$kfreq][$chfiltro]['descri'] = $eselform['nomeformulas'];
                                    $this->dadosrel[$idformula][$kfreq][$chfiltro]['qtde_moni'] = $result;
                                    $this->dadosrel[$idformula][$kfreq][$chfiltro]['resultado'] = number_format($calc,2);
                                }
                            }
                            else {
                                $w['datactt'] = $datas;
                                $where = implode(" AND ",$w);
                                $form->tab = "monitoria";
                                $form->tabavalia = "moniavalia";
                                $calc = $form->Calc_formula('', $this->idplan, $w);
                                $selqtde = "SELECT COUNT(DISTINCT($amoni.idmonitoria)) as result FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $where";
                                $eselqtde = $_SESSION['fetch_array']($_SESSION['query']($selqtde)) or die ("erro na query de consulta da quantidade de monitorias");
                                if($eselqtde['result'] == "") {
                                    $result = "--";
                                }
                                else {
                                    $result = $eselqtde['result'];
                                }
                                $this->dadosrel[$idformula][$kfreq]['descri'] = $eselform['nomeformulas'];
                                $this->dadosrel[$idformula][$kfreq]['qtde_moni'] = $result;
                                $this->dadosrel[$idformula][$kfreq]['resultado'] = number_format($calc,2);
                            }
                        }
                    }
                }
            }

            if($dados == "MO" OR $dados == "MS") {
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            foreach($this->idsoper as $noper => $idoper) {
                                if($frequencia == "D") {
                                    $datas = $amoni.".datactt='".$f."'";
                                }
                                else {
                                    $between = explode(",",$f);
                                    $datas = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                                }
                                foreach($this->sqlinner as $ksql => $sqlt) {
                                    if(eregi("INNER JOIN moniavalia",$sqlt)) {
                                    }
                                    else {
                                        $newsqlmo[$ksql + 1] = $sqlt;
                                    }
                                }
                                if(in_array("INNER JOIN monitoria $amoni ON $amoni.idmonitoria = $amoniava.idmonitoria",$newsqlmo)) {
                                }
                                else {
                                    $newsqlmo[0] = "INNER JOIN monitoria $amoni ON $amoni.idmonitoria = $amoniava.idmonitoria";
                                }
                                ksort($newsqlmo);
                                $dadosrel = "SELECT FORMAT(((SUM(DISTINCT($amoniava.valor_final_aval)) / (SELECT COUNT(DISTINCT($amoni.idmonitoria)) FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $amoni.id$tab[$dados]='$idoper' $wdadossql AND $datas)) / 2),2) as media,
                                        (SELECT COUNT(DISTINCT($amoni.idmonitoria)) FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $amoni.id$tab[$dados]='$idoper' $wdadossql AND $datas) as qtde,
                                        (SELECT COUNT(DISTINCT($amoni.idmonitoria)) FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $amoni.id$tab[$dados]='$idoper' AND qtdefg >= 1 $wdadossql AND $datas) as qtdefg,
                                        FORMAT(((SELECT COUNT(DISTINCT($amoni.idmonitoria)) FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $amoni.id$tab[$dados]='$idoper' AND qtdefg >= 1 $wdadossql AND $datas) / (SELECT COUNT(DISTINCT($amoni.idmonitoria)) FROM monitoria $amoni INNER JOIN moniavalia $amoniava ON $amoniava.idmonitoria = $amoni.idmonitoria WHERE $amoni.id$tab[$dados]='$idoper' $wdadossql AND $datas)),2) * 100 as percfg,
                                        $amoni.idmonitoria, $amoni.idoperador FROM moniavalia $amoniava ".implode(" ",$newsqlmo)." WHERE $amoni.id$tab[$dados]='$idoper' $wdadossql AND $datas";
                                $edadosrel = $_SESSION['fetch_array']($_SESSION['query']($dadosrel)) or die ("erro na query de consulta da media das notas do operador");
                                if($frequencia == "D") {
                                    $this->$dadosrel[$idoper]['media'] = $edadosrel['media'];
                                    $this->$dadosrel[$idoper]['qtde'] = $edadosrel['qtde'];
                                    $this->$dadosrel[$idoper]['qtde_fg'] = $edadosrel['qtdefg'];
                                    $this->$dadosrel[$idoper]['perc_fg'] = $edadosrel['percfg'];
                                }
                                if($frequencia == "S" OR $frequencia == "M" OR $frequencia == "G") {
                                    $this->apresdados = array('descri' => 'descri','media' => 'media','qtde' => 'qtde','qtde_fg' => 'qtdefg','perc_fg' => 'percfg');
                                    foreach($this->apresdados  as $kvar => $var) {
                                        if($var == "descri") {
                                                $xy = $var;
                                                $x = $noper;
                                        }
                                        else {
                                            if($edadosrel[$var] == "") {
                                                $xy = $kvar;
                                                $x = "--";
                                            }
                                            else {
                                                $xy = $kvar;
                                                $x = $edadosrel[$var];
                                            }
                                        }
                                        $this->dadosrel[$idoper][$kfreq][$xy] = $x;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if($dados == "P") {
                $this->idplan = $this->vars['idplanilha'];
                $this->apresdados = array('descri' => 'descri','qtde_moni' => 'qtde_moni','perc_resp' => 'perc_resp','possivel' => 'possivel','media' => 'media');
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            if($frequencia == "D") {
                                $datas = $amoni.".datactt='".$f."'";
                            }
                            else {
                                $between = explode(",",$f);
                                $datas = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                            }
                            $w = $this->where;
                            $w['datactt'] = $datas;
                            if($tipodados == "C" AND ($agrupa != "MS" && $agrupa != "MO" && $agrupa != "")) {
                                $selfiltros = "SELECT nomefiltro_dados, idfiltro_dados FROM filtro_dados fd
                                               INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes
                                               WHERE nomefiltro_nomes LIKE '%".$agrupa."%'";
                                $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta do nome dos fitlros");
                                while($lselfiltros = $_SESSION['fetch_array']($eselfiltros)) {
                                    $this->filtros[$lselfiltros['idfiltro_dados']] = 'id_'.strtolower($agrupa).",".$lselfiltros['nomefiltro_dados'];
                                }
                            }

                            $where = implode(" AND ",$w);


                            // seleciona a qualidade de monitorias para utlizar no calculo do percentual
                            $selq = "SELECT COUNT(DISTINCT($amoniava.idmonitoria)) as qtdemoni FROM moniavalia $amoniava
                                    INNER JOIN monitoria $amoni ON $amoni.idmonitoria=$amoniava.idmonitoria
                                    INNER JOIN rel_aval $arelaval ON $arelaval.idaval_plan = $amoniava.idaval_plan
                                    INNER JOIN aval_plan $aavalplan ON $aavalplan.idaval_plan = $amoniava.idaval_plan
                                    INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha
                                    INNER JOIN monitor $amonitor ON $amonitor.idmonitor = $amoni.idmonitor
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN fluxo $afluxo ON $afluxo.idfluxo = $aconf.idfluxo
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    INNER JOIN intervalo_notas $aintervalo ON $aintervalo.idindice = $aparam.idindice
                                    INNER JOIN monitoria_fluxo $amonifluxo ON $amonifluxo.idmonitoria = $amoni.idmonitoria
                                    INNER JOIN rel_fluxo $arelfluxo ON $arelfluxo.idrel_fluxo = $amonifluxo.idrel_fluxo
                                    INNER JOIN status $astatus ON $astatus.idstatus = $arelfluxo.idatustatus
                                    WHERE $where";
                            $eselq = $_SESSION['fetch_array']($_SESSION['query']($selq)) or die ("erro na query de consulta da quantidade de monitorias");
                            $qtde = $eselq['qtdemoni'];

                            // seleciona avaliaÃ§Ãµes para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                            $saval = "SELECT nomeaval_plan,".$aplan.".idaval_plan FROM planilha ".$aplan."
                                      INNER JOIN aval_plan ".$aavalplan." ON ".$aavalplan.".idaval_plan = ".$aplan.".idaval_plan
                                      INNER JOIN rel_aval ".$arelaval." ON ".$arelaval.".idaval_plan = ".$aplan.".idaval_plan
                                      WHERE ".$aplan.".idplanilha='$this->idplan' GROUP BY ".$aplan.".idaval_plan ORDER BY ".$arelaval.".posicao";
                            $esaval = $_SESSION['query']($saval) or die ("erro na query de consulta das avaliaÃ§Ãµes");
                            while($lsaval = $_SESSION['fetch_array']($esaval)) {
                                $selaval = "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni, (COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc, ".$amoniava.".idplanilha,".$amoniava.".idaval_plan, ((SUM(".$amoniava.".valor_final_aval) / COUNT(DISTINCT(".$amoniava.".idaval_plan))) / COUNT(".$amoniava.".idaval_plan)) as media, valor_aval as valor
                                        FROM moniavalia ".$amoniava."
                                        INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                        WHERE ".$amoniava.".idplanilha='$this->idplan'AND $datas AND ".$amoniava.".idaval_plan='".$lsaval['idaval_plan']."'";
                                $eselaval = $_SESSION['query']($selaval) or die ("erro na query de consulta da avaliaÃ§Ã£o");
                                $naval = $_SESSION['num_rows']($eselaval);
                                if($naval >= 1) {
                                    $lselaval = $_SESSION['fetch_array']($eselaval);
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['descri'] = $lsaval['nomeaval_plan'];
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['qtde_moni'] = $lselaval['qtdemoni'];
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['perc_resp'] = number_format($lselaval['perc'],2);
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['possivel'] = $lselaval['valor'];
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['media'] = number_format($lselaval['media'],2);
                                }
                                else {
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['descri'] = $lsaval['nomeaval_plan'];
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['qtde_moni'] = "";
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['perc_resp'] = "";
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['possivel'] = "";
                                    $this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['media'] = "";
                                }

                                // seleciona grupos para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                                $sgrupo = "SELECT descrigrupo, ".$aplan.".idgrupo, filtro_vinc FROM planilha ".$aplan."
                                           INNER JOIN grupo ".$agrupo." ON ".$agrupo.".idgrupo = ".$aplan.".idgrupo
                                           WHERE ".$aplan.".idplanilha='$this->idplan' AND ".$aplan.".idaval_plan='".$lsaval['idaval_plan']."' GROUP BY ".$aplan.".idgrupo ORDER BY ".$aplan.".posicao";
                                $esgrupo = $_SESSION['query']($sgrupo) or die ("erro na query para listar os grupo partencentes Ã  avaliaÃ§Ã£o");
                                while($lsgrupo = $_SESSION['fetch_array']($esgrupo)) {
                                    $selgrup =  "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni,(COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc, ((SUM(".$amoniava.".valor_final_grup) / COUNT(DISTINCT(".$amoniava.".idgrupo))) / COUNT(".$amoniava.".idgrupo)) as media, valor_grupo as valor FROM moniavalia ".$amoniava."
                                                INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                                WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idaval_plan='".$lsaval['idaval_plan']."' AND ".$amoniava.".idgrupo='".$lsgrupo['idgrupo']."'";
                                    $eselgrup = $_SESSION['query']($selgrup) or die ("erro na query de consulta dos grupos");
                                    $ngrup = $_SESSION['num_rows']($eselgrup);
                                    if($ngrup >= 1) {
                                        $lselgrup = $_SESSION['fetch_array']($eselgrup);
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['descri'] = $lsgrupo['descrigrupo'];
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['qtde_moni'] = $lselgrup['qtdemoni'];
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['perc_resp'] = number_format($lselgrup['perc'],2);
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['possivel'] = $lselgrup['valor'];
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['media'] = number_format($lselgrup['media'],2);
                                    }
                                    else {
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['descri'] = $lsgrupo['descrigrupo'];
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['qtde_moni'] = "--";
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['perc_resp'] = "--";
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['possivel'] = "--";
                                        $this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['media'] = "--";
                                    }

                                    if($lsgrupo['filtro_vinc'] == "S") {
                                        // seleciona subgrupos para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                                        $ssub = "SELECT descrisubgrupo, ".$agrupo.".idrel FROM grupo ".$agrupo."
                                                INNER JOIN subgrupo ".$asubgrupo." ON ".$asubgrupo.".idsubgrupo = ".$agrupo.".idrel
                                                WHERE ".$agrupo.".idgrupo='".$lsgrupo['idgrupo']."' AND filtro_vinc='S' GROUP BY ".$agrupo.".idrel ORDER BY ".$agrupo.".posicao";
                                        $essub = $_SESSION['query']($ssub) or die ("erro na query de consulta dos subgrupos relacionados ao grupo");
                                        while($lssub = $_SESSION['fetch_array']($essub)) {
                                            $selsub = "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni,(COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc, ((SUM(".$amoniava.".valor_final_sub) / COUNT(DISTINCT(".$amoniava.".idsubgrupo))) / COUNT(".$amoniava.".idsubgrupo)) as media, valor_sub as valor
                                                   FROM moniavalia ".$amoniava."
                                                   INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                                   WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idgrupo='".$lsgrupo['idgrupo']."' AND ".$amoniava.".idsubgrupo='".$lssub['idrel']."'";
                                            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos subgrupos");
                                            $nsub = $_SESSION['num_rows']($eselsub);
                                            if($nsub >= 1) {
                                                $lselsub = $_SESSION['fetch_array']($eselsub);
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['descri'] = $lssub['descrisubgrupo'];
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['qtde_moni'] = $lselsub['qtdemoni'];
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['perc_resp'] = number_format($lselsub['perc'],2);
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['possivel'] = $lselsub['valor'];
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['media'] = number_format($lselsub['media'],2);
                                            }
                                            else {
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['descri'] = $lssub['descrisubgrupo'];
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['qtde_moni'] = "--";
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['perc_resp'] = "--";
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['possivel'] = "--";
                                                $this->dadosrel['subgrupo'.$lssub['idrel']][$kfreq]['media'] = "--";
                                            }

                                            // seleciona perguntas para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                                            $sperg = "SELECT descripergunta,".$asubgrupo.".idpergunta FROM subgrupo ".$asubgrupo."
                                                      INNER JOIN pergunta ".$apergunta." ON ".$apergunta.".idpergunta = ".$asubgrupo.".idpergunta
                                                      WHERE ".$asubgrupo.".idsubgrupo='".$lssub['idrel']."' GROUP BY ".$asubgrupo.".idpergunta ORDER BY ".$asubgrupo.".posicao";
                                            $esperg = $_SESSION['query']($sperg) or die ("erro na query de consulta das perguntas relacionadas ao subgrupo");
                                            while($lsperg = $_SESSION['fetch_array']($esperg)) {
                                                $selperg = "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni, (COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc, ((SUM(".$amoniava.".valor_final_perg) / COUNT(DISTINCT(".$amoniava.".idpergunta))) / COUNT(".$amoniava.".idpergunta)) as media, valor_perg as valor
                                                        FROM moniavalia ".$amoniava."
                                                        INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                                        WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idsubgrupo='".$lssub['idrel']."' AND ".$amoniava.".idpergunta='".$lsperg['idpergunta']."'";
                                                $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
                                                $nperg = $_SESSION['num_rows']($eselperg);
                                                if($nperg >= 1) {
                                                    $lselperg = $_SESSION['fetch_array']($eselperg);
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['descri'] = $lsperg['descripergunta'];
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['qtde_moni'] = $lselperg['qtdemoni'];
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['perc_resp'] = number_format($lselperg['perc'],2);
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['possivel'] = $lselperg['valor'];
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['media'] = number_format($lselperg['media'],2);
                                                }
                                                else {
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['descri'] = $lsperg['descripergunta'];
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['qtde_moni'] = "--";
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['perc_resp'] = "--";
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['possivel'] = "--";
                                                    $this->dadosrel['perg'.$lsperg['idpergunta']][$kfreq]['media'] = "--";
                                                }

                                                // seleciona respostas para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                                                $sresp = "SELECT descriresposta, ".$apergunta.".idresposta FROM pergunta ".$apergunta."
                                                          WHERE ".$apergunta.".idpergunta='".$lsperg['idpergunta']."' GROUP BY ".$apergunta.".idresposta ORDER BY ".$apergunta.".posicao";
                                                $esresp = $_SESSION['query']($sresp) or die ("erro na query de consulta das respostas vinculadas Ã  pergunta");
                                                while($lsresp = $_SESSION['fetch_array']($esresp)) {
                                                    $selresp = "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni, (COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc
                                                            FROM moniavalia ".$amoniava."
                                                            INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                                            WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idpergunta='".$lsperg['idpergunta']."' AND ".$amoniava.".idresposta='".$lsresp['idresposta']."'";
                                                    $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta da resposta");
                                                    $nresp = $_SESSION['num_rows']($eselresp);
                                                    if($nresp >= 1) {
                                                        $lselresp = $_SESSION['fetch_array']($eselresp);
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['descri'] = $lsresp['descriresposta'];
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['qtde_moni'] = $lselresp['qtdemoni'];
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['perc_resp'] = number_format($lselresp['perc'],2);
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['media'] = "";
                                                    }
                                                    else {
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['descri'] = $lsresp['descriresposta'];
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['qtde_moni'] = "--";
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['perc_resp'] = "--";
                                                        $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['media'] = "--";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        // seleciona perguntas para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                                        $sperg = "SELECT descripergunta, ".$agrupo.".idrel FROM grupo ".$agrupo."
                                                  INNER JOIN pergunta ".$apergunta." ON ".$apergunta.".idpergunta = ".$agrupo.".idrel
                                                  WHERE ".$agrupo.".idgrupo='".$lsgrupo['idgrupo']."' AND filtro_vinc='P' GROUP BY ".$agrupo.".idrel ORDER BY ".$agrupo.".posicao";
                                        $esperg = $_SESSION['query']($sperg) or die ("erro na query de consulta das perguntas relacionadas ao grupo");
                                        while($lsperg = $_SESSION['fetch_array']($esperg)) {
                                            $selperg = "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni, (COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc, ((SUM(".$amoniava.".valor_final_perg) / COUNT(DISTINCT(".$amoniava.".idpergunta))) / COUNT(".$amoniava.".idpergunta)) as media, valor_perg as valor FROM moniavalia ".$amoniava."
                                                   INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                                   WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idgrupo='".$lsgrupo['idgrupo']."' AND ".$amoniava.".idpergunta='".$lsperg['idrel']."'";
                                            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
                                            $nperg = $_SESSION['num_rows']($eselperg);
                                            if($nperg >= 1) {
                                                $lselperg = $_SESSION['fetch_array']($eselperg);
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['descri'] = $lsperg['descripergunta'];
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['qtde_moni'] = $lselperg['qtdemoni'];
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['perc_resp'] = number_format($lselperg['perc'],2);
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['possivel'] = $lselperg['valor'];
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['media'] = number_format($lselperg['media'],2);
                                            }
                                            else {
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['descri'] = $lsperg['descripergunta'];
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['qtde_moni'] = "--";
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['perc_resp'] = "--";
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['possivel'] = "--";
                                                $this->dadosrel['perg'.$lsperg['idrel']][$kfreq]['media'] = "--";
                                            }

                                            // seleciona respostas para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                                            $sresp = "SELECT descriresposta, ".$apergunta.".idresposta FROM pergunta ".$apergunta."
                                                      WHERE ".$apergunta.".idpergunta='".$lsperg['idrel']."' GROUP BY ".$apergunta.".idresposta ORDER BY ".$apergunta.".posicao";
                                            $esresp = $_SESSION['query']($sresp) or die ("erro na query de consulta das respostas vinculadas Ã  pergunta");
                                            while($lsresp = $_SESSION['fetch_array']($esresp)) {
                                                $selresp = "SELECT COUNT(DISTINCT(".$amoniava.".idmonitoria)) as qtdemoni, (COUNT(DISTINCT(".$amoniava.".idmonitoria)) / $qtde) * 100 as perc
                                                        FROM moniavalia ".$amoniava."
                                                        INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria=".$amoniava.".idmonitoria
                                                        WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idpergunta='".$lsperg['idrel']."' AND ".$amoniava.".idresposta='".$lsresp['idresposta']."'";
                                                $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta da resposta");
                                                $nresp = $_SESSION['num_rows']($eselresp);
                                                if($nresp >= 1) {
                                                    $lselresp = $_SESSION['fetch_array']($eselresp);
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['descri'] = $lsresp['descriresposta'];
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['qtde_moni'] = $lselresp['qtdemoni'];
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['perc_resp'] = number_format($lselresp['perc'],2);
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['media'] = "--";
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['possivel'] = "--";
                                                }
                                                else {
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['descri'] = $lsresp['descriresposta'];
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['qtde_moni'] = "--";
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['perc_resp'] = "--";
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['media'] = "--";
                                                    $this->dadosrel['resp'.$lsresp['idresposta']][$kfreq]['possivel'] = "--";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if($dados == "PG" OR $dados == "PI") {
                $this->idplan = $this->vars['idplanilha'];
                $this->apresdados = array('descri' => 'descri','qtde_erros' => 'qtde_erros','perc' => 'perc','perc_acumu' => 'perc_acumu');
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            if($frequencia == "D") {
                                $datas = $amoni.".datactt='".$f."'";
                            }
                            else {
                                $between = explode(",",$f);
                                $datas = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                            }

                            $w = $this->where;
                            if(key_exists('datactt', $this->where)) {
                            }
                            else {
                                $w['datactt'] = $datas;
                            }
                            //$w['idplanilha'] = $this->idplan;
                            $where = implode(" AND ",$w);

                            // seleciona avaliaÃ§Ãµes para criar array que serÃ¡ percorrido na pÃ¡gina de visualizaÃ§Ã£o dos dados
                            //$saval = "SELECT nomeaval_plan,".$aplan.".idaval_plan FROM planilha ".$aplan."
                              //        INNER JOIN aval_plan ".$aavalplan." ON ".$aavalplan.".idaval_plan = ".$aplan.".idaval_plan
                                //      INNER JOIN rel_aval ".$arelaval." ON ".$arelaval.".idaval_plan = ".$aplan.".idaval_plan
                                  //    WHERE ".$aplan.".idplanilha='$this->idplan' GROUP BY ".$aplan.".idaval_plan ORDER BY ".$arelaval.".posicao";
                            //$esaval = $_SESSION['query']($saval) or die ("erro na query de consulta das avaliaÃ§Ãµes");
                            //while($lsaval = $_SESSION['fetch_array']($esaval)) {
                                //$this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['descri'] = $lsaval['nomeaval_plan'];
                                //$this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['qtde_erros'] = "";
                                //$this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['perc'] = "";
                                //$this->dadosrel['aval'.$lsaval['idaval_plan']][$kfreq]['perc_acumu'] = "";
                                $perc = array();
                                $dadosrel = array();
                                $terros = 0;
                                $acumu = 0;
                                $erros = 0;
                                    //faz levantamento dos grupos e o percentual de acerto para montar os dados da tabela de pareto
                                    if($dados == "PG") {
                                        $sgrupos = "SELECT ".$agrupo.".descrigrupo, ".$agrupo.".idgrupo FROM planilha ".$aplan."
                                                    INNER JOIN grupo ".$agrupo." ON ".$agrupo.".idgrupo=".$aplan.".idgrupo
                                                    WHERE ".$aplan.".idplanilha='".$this->idplan."' GROUP BY ".$aplan.".idgrupo";
                                        $esgrupos = $_SESSION['query']($sgrupos) or die ("erro na query de consulta dos grupos vinculados a planilha");
                                        while($lsgrupos = $_SESSION['fetch_array']($esgrupos)) {
                                            $erros = 0;
                                            $sgrupo = "SELECT ".$amoniava.".idgrupo, if(".$amoniava.".valor_final_grup < ".$amoniava.".valor_grupo,COUNT(DISTINCT(".$amoniava.".idmonitoria)),0) as qtde FROM moniavalia ".$amoniava."
                                                        INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria = ".$amoniava.".idmonitoria
                                                        INNER JOIN grupo ".$agrupo." ON ".$agrupo.".idgrupo = moniaa.idgrupo
                                                        WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idgrupo='".$lsgrupos['idgrupo']."' GROUP BY ".$agrupo.".idgrupo, ".$amoniava.".idmonitoria";
                                            $esgrupo = $_SESSION['query']($sgrupo) or die ("erro na query para listar os grupo partencentes Ã  avaliaÃ§Ã£o");
                                            $ngrupo = $_SESSION['num_rows']($esgrupo);
                                            while($lsgrupo = $_SESSION['fetch_array']($esgrupo)) {
                                                if($lsgrupo['qtde'] >= 1) {
                                                    if($lsgrupo['idgrupo'] == $idgrupo) {
                                                        $idgrupo = $lsgrupo['idgrupo'];
                                                        $erros++;
                                                        $perc[$lsgrupo['idgrupo']] = $erros;
                                                    }
                                                    else {
                                                        $idgrupo = $lsgrupo['idgrupo'];
                                                        $erros = 1;
                                                        $perc[$lsgrupo['idgrupo']] = $erros;
                                                    }
                                                    $terros++;
                                                }
                                                else {
                                                    if($erros == 0) {
                                                        $perc[$lsgrupo['idgrupo']] = 0;
                                                    }
                                                    else {
                                                    }
                                                }
                                            }
                                            if($ngrupo == 0) {
                                                $perc[$lsgrupos['idgrupo']] = "NE";
                                            }
                                            else {
                                            }
                                        }
                                    }
                                    if($dados == "PI") {
                                        $spergs = "SELECT ".$apergunta.".descripergunta, ".$apergunta.".idpergunta FROM planilha ".$aplan."
                                                    INNER JOIN grupo ".$agrupo." ON ".$agrupo.".idgrupo = ".$aplan.".idgrupo
                                                    INNER JOIN pergunta ".$apergunta." ON ".$apergunta.".idpergunta=".$agrupo.".idrel AND ".$agrupo.".filtro_vinc='P'
                                                    WHERE ".$aplan.".idplanilha='".$this->idplan."' GROUP BY ".$apergunta.".idpergunta";
                                        $espergs = $_SESSION['query']($spergs) or die ("erro na query de consulta dos grupos vinculados a planilha");
                                        while($lspergs = $_SESSION['fetch_array']($espergs)) {
                                            $erros = 0;
                                            $sperg = "SELECT ".$amoniava.".idpergunta, if(".$amoniava.".valor_final_perg < ".$amoniava.".valor_perg,COUNT(DISTINCT(".$amoniava.".idmonitoria)),0) as qtde FROM moniavalia ".$amoniava."
                                                        INNER JOIN monitoria ".$amoni." ON ".$amoni.".idmonitoria = ".$amoniava.".idmonitoria
                                                        INNER JOIN pergunta ".$apergunta." ON ".$apergunta.".idpergunta = moniaa.idpergunta
                                                        WHERE ".$amoniava.".idplanilha='$this->idplan' AND $datas AND ".$amoniava.".idpergunta='".$lspergs['idpergunta']."' GROUP BY ".$apergunta.".idpergunta, ".$amoniava.".idmonitoria";
                                            $esperg = $_SESSION['query']($sperg) or die ("erro na query para listar os grupo partencentes Ã  avaliaÃ§Ã£o");
                                            while($lsperg = $_SESSION['fetch_array']($esperg)) {
                                                if($lsperg['qtde'] >= 1) {
                                                    if($lsperg['idpergunta'] == $idperg) {
                                                        $idperg = $lsperg['idpergunta'];
                                                        $erros++;
                                                        $perc[$lsperg['idpergunta']] = $erros;
                                                    }
                                                    else {
                                                        $idperg = $lsperg['idpergunta'];
                                                        $erros = 1;
                                                        $perc[$lsperg['idpergunta']] = $erros;
                                                    }
                                                    $terros++;
                                                }
                                                else {
                                                    if($erros == 0) {
                                                        $perc[$lsperg['idpergunta']] = "NE";
                                                    }
                                                    else {

                                                    }
                                                }
                                            }
                                        }
                                    }

                                foreach($perc as $kid => $qerros) {
                                    if($dados == "PI") {
                                        $selgrup =  "SELECT descripergunta FROM pergunta ".$apergunta."
                                                    WHERE ".$apergunta.".idpergunta='$kid'";
                                        $descri = "descripergunta";
                                    }
                                    else {
                                        $selgrup =  "SELECT descrigrupo FROM grupo ".$agrupo."
                                                     WHERE ".$agrupo.".idgrupo='$kid'";
                                        $descri = "descrigrupo";
                                    }
                                    $eselgrup = $_SESSION['fetch_array']($_SESSION['query']($selgrup)) or die ("erro na query de consulta dos grupos");
                                    if($qerros == "NE") {
                                        $vperc = "--";
                                        $vkey = "";
                                    }
                                    else {
                                        $vperc = number_format(($qerros / $terros) * 100,2);
                                        $vkey = ceil($qerros / $terros * 100);
                                    }
                                    $dadosrel[$vkey]['ggrupo'.$kid][$kfreq]['perc'] = $vperc;
                                    $dadosrel[$vkey]['ggrupo'.$kid][$kfreq]['descri'] = $eselgrup[$descri];
                                    $dadosrel[$vkey]['ggrupo'.$kid][$kfreq]['qtde_erros'] = $qerros;
                                    //$this->dadosrel['ggrupo'.$lsgrupo['idgrupo']][$kfreq]['perc.acumu'] = $valoracu;
                                }
                                $perc = array();

                                krsort($dadosrel);
                                foreach($dadosrel as $kv => $v) {
                                    foreach($v as $kg => $g) {
                                        foreach($g as $kf => $fe) {
                                            if($kv == "") {
                                                $acumu2 = "";
                                            }
                                            else {
                                                $acumu1 = $acumu1 + $fe['perc'];
                                            }
                                            if($apres == "GRAFICO" && $kv == "") {
                                            }
                                            else {
                                                $this->dadosrel[$kg][$kf]['descri'] = $fe['descri'];
                                                $this->dadosrel[$kg][$kf]['qtde_erros'] = $fe['qtde_erros'];
                                                $this->dadosrel[$kg][$kf]['perc'] = $fe['perc'];
                                                if($kv == "") {
                                                    $this->dadosrel[$kg][$kf]['perc_acumu'] = $acumu2;
                                                }
                                                else {
                                                    $this->dadosrel[$kg][$kf]['perc_acumu'] = $acumu1;
                                                }
                                            }
                                        }
                                    }
                                }
                                $acumu1 = "";
                            //}
                        }
                    }
                }
            }

            if($dados == "A") {
                $this->apresdados = array('descri' => 'descri', 'qtde_imp' => 'qtde_imp','qtde_real' => 'qtde_real', 'qtde_erro' => 'qtde_erro');
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            $wmoni = array();
                            if($frequencia == "D") {
                                $datas = $aupload.".dataini='".$f."'";
                                $datasm = $amoni.".datactt='".$f."'";
                            }
                            else {
                                $between = explode(",",$f);
                                $datas = "it.dataini BETWEEN '$between[0]' AND '$between[1]'";
                                $datasm = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                            }
                            //$w['idplanilha'] = $this->idplan;
                            if($this->where['idrel_filtros'] == "") {
                                $wmoni = $this->where;
                                $wmoni['datactt'] = $datasm;
                                $wmoni = implode(" AND ",$wmoni);
                                $w = $this->where;
                                $w['data'] = $datas;
                                $selrels = "SELECT rf.idrel_filtros, pm.tipomoni FROM rel_filtros rf
                                            INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                            INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                            WHERE cr.ativo='S'";
                                $eselrels = $_SESSSION['query']($selrels) or die ("erro na query de consulta dos relacionamentos ativos");
                                while($lselrels = $_SESSION['fetch_array']($eselrels)) {
                                    if($lselrels['tipomoni'] == "G") {
                                        $tab = "fila_grava";
                                        $atab = $afilag;
                                    }
                                    if($lselrels['tipomoni'] == "O") {
                                        $tab = "fila_oper";
                                        $atab = $afilao;
                                    }
                                    $w['idrel_filtros'] = $arelfiltros."idrel_filtros='".$lselrels['idrel_filtros']."'";
                                    $wmoni['idrel_filtros'] = $arelfiltros."idrel_filtros='".$lselrels['idrel_filtros']."'";
                                    $where = implode(" AND ",$w);
                                    $selamostra = "SELECT SUM($aupload.qtdeimp) as qtdeimp, SUM($aupload.qtdeerro) as qtdeerro
                                                   FROM upload $aupload
                                                   INNER JOIN interperiodo it ON it.idperiodo = $aupload.idperiodo
                                                   WHERE $where";
                                    $eamostra = $_SESSION['query']($selamostra) or die ("erro na query de consulta das importaÃ§Ãµes");
                                    $namostra = $_SESSION['num_rows']($eamostra);
                                    $lamostra = $_SESSION['fetch_array']($eamostra);
                                    $selmoni = "SELECT COUNT(DISTINCT($amoni.idmonitoria)) as qtdemoni FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE $wmoni";
                                    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta das monitorias");
                                    $this->dadosrel[$idrel][$kfreq]['descri'] = nomeapres($idrel);
                                    $this->dadosrel[$idrel][$kfreq]['qtde_real'] = $eselmoni['qtdemoni'];
                                    if($lamostra['qtdeerro'] == "") {
                                        $erro = 0;
                                    }
                                    else {
                                        $erro = $lamostra['qtdeerro'];
                                    }
                                    $this->dadosrel[$idrel][$kfreq]['descri'] = nomeapres($idrel);
                                    if($lamostra['qtdeimp'] == "") {
                                        $imp = 0;
                                    }
                                    else {
                                        $imp = $lamostra['qtdeimp'];
                                    }
                                    $this->dadosrel[$idrel][$kfreq]['qtde_imp'] = $imp;
                                    $this->dadosrel[$idrel][$kfreq]['qtde_erro'] = $erro;
                                }
                            }
                            else {
                                $wmoni = $this->where;
                                $wmoni['datactt'] = $datasm;
                                $w['data'] = $datas;
                                $where = implode(" AND ",$w);
                                $idrels = explode(",",$this->vars['idrel_filtros']);
                                foreach($idrels as $idrel) {
                                    $wmoni['idrel_filtros'] = $amoni.".idrel_filtros='".$idrel."'";
                                    $selrels = "SELECT rf.idrel_filtros, pm.tipomoni, COUNT(*) as result FROM rel_filtros rf
                                                INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                                WHERE cr.ativo='S' AND rf.idrel_filtros='$idrel'";
                                    $eselrels = $_SESSION['fetch_array']($_SESSION['query']($selrels)) or die ("erro na query de consulta dos relacionamentos ativos");
                                    if($eselrels['tipomoni'] == "G") {
                                        $tab = "fila_grava";
                                        $atab = $afilag;
                                    }
                                    if($eselrels['tipomoni'] == "O") {
                                        $tab = "fila_oper";
                                        $atab = $afilao;
                                    }
                                    $selamostra = "SELECT SUM($aupload.qtdeimp) as qtdeimp, SUM($aupload.qtdeerro) as qtdeerro
                                                   FROM upload $aupload
                                                   INNER JOIN interperiodo it ON it.idperiodo = $aupload.idperiodo AND it.semana = $aupload.semana
                                                   WHERE $where AND $aupload.idrel_filtros='$idrel'";
                                    $eamostra = $_SESSION['query']($selamostra) or die ("erro na query de consulta das importaÃ§Ãµes");
                                    $namostra = $_SESSION['num_rows']($eamostra);
                                    $selmoni = "SELECT COUNT(DISTINCT($amoni.idmonitoria)) as qtdemoni FROM monitoria $amoni ".implode(" ",$this->sqlinner)." WHERE ".implode(" AND ",$wmoni)."";
                                    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta das monitorias");
                                    $lamostra = $_SESSION['fetch_array']($eamostra);
                                    $this->dadosrel[$idrel][$kfreq]['descri'] = nomeapres($idrel);
                                    $this->dadosrel[$idrel][$kfreq]['qtde_real'] = $eselmoni['qtdemoni'];
                                    if($lamostra['qtdeerro'] == "") {
                                        $erro = 0;
                                    }
                                    else {
                                        $erro = $lamostra['qtdeerro'];
                                    }
                                    $this->dadosrel[$idrel][$kfreq]['descri'] = nomeapres($idrel);
                                    if($lamostra['qtdeimp'] == "") {
                                        $imp = 0;
                                    }
                                    else {
                                        $imp = $lamostra['qtdeimp'];
                                    }
                                    $this->dadosrel[$idrel][$kfreq]['qtde_imp'] = $imp;
                                    $this->dadosrel[$idrel][$kfreq]['qtde_erro'] = $erro;
                                }
                            }
                        }
                    }
                }
            }
            if($dados == "IT") {
                $w = $this->where;
                $this->apresdados = array();
                if($w['idplanilha'] != "") {
                    $selform = "SELECT * FROM formulas $aform WHERE ".str_replace($amoniava, $aform, $w['idplanilha'])."";
                    $eselform = $_SESSION['query']($selform) or die ("erro na query de consulta da formula");
                    while($lselform = $_SESSION['fetch_array']($eselform)) {
                        $formulasdados[ $lselform['formula']] = trim(str_replace(" ", "_", $lselform['nomeformulas']));
                    }
                }
                else {
                    $selform = "SELECT * FROM formulas $aform WHERE $aform.idplanilha IN (SELECT $amoni.idplanilha FROM monitoria $amoni WHERE ".$w['data']." AND ".$w['idrel_filtros']." GROUP BY $amoni.idplanilha)";
                    $eselform = $_SESSION['query']($selform) or die ("erro na query de consulta das formulas");
                    while($lselform = $_SESSSION['fetch_array']($eselform)) {
                        $formulasdados[$lselform['formula']] = trim(str_replace(" ", "_", $lselform['nomeformulas']));
                    }
                }
                $this->apresdados['descri'] = 'descri';
                foreach($formulasdados as $formdados) {
                    $this->apresdados[strtolower($formdados)] = strtolower($formdados);
                    $this->apresdados['perc_'.strtolower($formdados)] = 'perc_'.strtolower($formdados);
                    $this->tituloY[] = 'perc_'.strtolower($formdados);
                }
                if($w['idindice'] != "") {
                    $whereindi[] = str_replace($amoni, $arelfiltros, $w['idrel_filtros']);
                    $whereindi[] = $w['idindice'];
                }
                else {
                    $whereindi[] = str_replace($amoni, $arelfiltros, $w['idrel_filtros']);
                }
                $selindice = "SELECT $aindice.idindice FROM indice $aindice
                              INNER JOIN param_moni $aparam ON $aparam.idindice = $aindice.idindice
                              INNER JOIN conf_rel $aconf ON $aconf.idparam_moni = $aparam.idparam_moni
                              INNER JOIN rel_filtros $arelfiltros ON $arelfiltros.idrel_filtros = $aconf.idrel_filtros
                              WHERE ".implode(" AND ", $whereindi)." GROUP BY $aindice.idindice";
                $eselindice = $_SESSION['query']($selindice) or die ("erro na query de consulta dos indices");
                while($lselindice = $_SESSION['fetch_array']($eselindice)) {
                    $idsindice[] = $lselindice['idindice'];
                }
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            if($frequencia == "D") {
                                $datas = $amoni.".datactt='".$f."'";
                            }
                            else {
                                $between = explode(",",$f);
                                $datas = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                            }
                            $w['datactt'] = $datas;
                            //$w['idplanilha'] = $this->idplan;
                            $where = implode(" AND ",$w);
                            $in = 0;

                            foreach($this->sqlinner as $ksql => $sqlt) {
                                $newsqlit[$ksql + 3] = trim($sqlt);
                            }
                            $i = 0;
                            $insertin = array("INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $amoni.idrel_filtros","INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni","INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice");
                            foreach($insertin as $insert) {
                                if(in_array(trim($insert),$newsqlit)) {
                                }
                                else {
                                    $newsqlit[$i] = trim($insert);
                                }
                                $i++;
                            }
                            ksort($newsqlit);
                            foreach($idsindice as $idindice) {
                                $totalsel = "SELECT COUNT(DISTINCT($amoni.idmonitoria)) as result FROM monitoria $amoni ".implode(" ",$newsqlit)." WHERE $where AND $aindice.idindice='$idindice'";
                                $etotal = $_SESSION['fetch_array']($_SESSION['query']($totalsel)) or die ("erro na query de consulta do total de monitorias do perÃ­odo");
                                $total = $etotal['result'];

                                if($total == 0) {
                                    $selinter = "SELECT * FROM intervalo_notas WHERE idindice='$idindice' ORDER BY numini DESC";
                                    $eselinter = $_SESSION['query']($selinter) or die ('erro na query de consulta do intervalor');
                                    while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                                        foreach($this->apresdados as $apres) {
                                            if($apres == "descri") {
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq]['descri'] = $lselinter['nomeintervalo_notas'];
                                            }
                                            else {
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq][$apres] = "--";
                                            }
                                        }
                                    }
                                }
                                else {
                                    $selmoni = "SELECT DISTINCT($amoni.idmonitoria), $amoniava.idplanilha,$aparam.idindice,$aindice.idindice FROM monitoria $amoni ".implode(" ",$newsqlit)." WHERE $where AND $aindice.idindice='$idindice' GROUP BY $amoni.idmonitoria";
                                    $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query para levantar as monitorias");
                                    while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                                        $form = new Formulas();
                                        $form->idmonitoria = $lselmoni['idmonitoria'];
                                        foreach($formulasdados as $nform => $formula) {
                                            $in = 0;
                                            $form->formula = $nform;
                                            $form->tab = "monitoria";
                                            $form->tabavalia = "moniavalia";
                                            $calc = $form->Calc_formula('', $lselmoni['idplanilha'], $w);
                                            $selinter = "SELECT * FROM intervalo_notas WHERE idindice='$idindice' ORDER BY numini DESC";
                                            $eselinter = $_SESSION['query']($selinter) or die ('erro na query de consulta do intervalor');
                                            while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                                                if($calc >= $lselinter['numini'] && $calc <= $lselinter['numfim']) {
                                                    if(key_exists($lselinter['nomeintervalo_notas'], $dadosinter[$formula])) {
                                                        $dadosinter[$formula][$lselinter['nomeintervalo_notas']] = ++$in + $dadosinter[$formula][$lselinter['nomeintervalo_notas']];
                                                    }
                                                    else {
                                                        $dadosinter[$formula][$lselinter['nomeintervalo_notas']] = ++$in;
                                                    }
                                                }
                                                else {
                                                }
                                            }
                                        }
                                    }
                                    $selinter = "SELECT * FROM intervalo_notas WHERE idindice='$idindice' ORDER BY numini DESC";
                                    $eselinter = $_SESSION['query']($selinter) or die ('erro na query de consulta do intervalor');
                                    while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                                        foreach($formulasdados as $nform) {
                                            if(!key_exists($lselinter['nomeintervalo_notas'],$dadosinter[$nform])) {
                                                if(in_array($lselinter['nomeintervalo_notas'],$this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq]['descri'])) {
                                                }
                                                else {
                                                    $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq]['descri'] = $lselinter['nomeintervalo_notas'];
                                                }
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq][strtolower($nform)] = "--";
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq]['perc_'.strtolower($nform)] = "--";
                                            }
                                            else {
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq]['descri'] = $lselinter['nomeintervalo_notas'];
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq][strtolower($nform)] = $dadosinter[$nform][$lselinter['nomeintervalo_notas']];
                                                $this->dadosrel[$lselinter['nomeintervalo_notas']][$kfreq]['perc_'.strtolower($nform)] = round(($dadosinter[$nform][$lselinter['nomeintervalo_notas']] / $total) * 100,2);
                                            }
                                        }
                                    }
                                }
                                $dadosinter = array();
                                $total = 0;
                            }
                        }
                    }
                }
            }
            if($dados == "TAB") {
                $w = $this->where;
                $w[] = $amonitab.".idtabulacao='$this->idtabulacao'";
                $this->apresdados = array('descri' => 'descri','qtde' => 'qtde', 'perc' => 'perc');
                foreach($this->frequencia as $pf) {
                    foreach($pf as $kfreq => $freq ) {
                        foreach ($freq as $kf => $f) {
                            if($frequencia == "D") {
                                $datas = $amoni.".datactt='".$f."'";
                            }
                            else {
                                $between = explode(",",$f);
                                $datas = $amoni.".datactt BETWEEN '$between[0]' AND '$between[1]'";
                            }
                            $w['datactt'] = $datas;
                            //$w['idplanilha'] = $this->idplan;
                            $where = implode(" AND ",$w);

                            // consulta do total
                            if(in_array("INNER JOIN tabulacao $atabula ON $atabula.idtabulacao = $amonitab.idtabulacao",$this->sqlinner)) {
                            }
                            else {
                                $this->sqlinner[] = "INNER JOIN tabulacao $atabula ON $atabula.idtabulacao = $amonitab.idtabulacao";
                            }
                            if(in_array("LEFT JOIN perguntatab $apergtab ON $apergtab.idperguntatab = $amonitab.idperguntatab",$this->sqlinner)) {
                            }
                            else {
                                $this->sqlinner[] = "LEFT JOIN perguntatab $apergtab ON $apergtab.idperguntatab = $amonitab.idperguntatab";
                            }
                            if(in_array("LEFT JOIN respostatab $aresptab ON $aresptab.idrespostatab = $amonitab.idrespostatab",$this->sqlinner)) {
                            }
                            else {
                                $this->sqlinner[] = "LEFT JOIN respostatab $aresptab ON $aresptab.idrespostatab = $amonitab.idrespostatab";
                            }
                            foreach($this->sqlinner as $ksql => $sqlt) {
                                $newsqltab[$ksql + 1] = $sqlt;
                            }
                            if(in_array("INNER JOIN monitoria $amoni ON $amoni.idmonitoria = $amonitab.idmonitoria",$newsqltab)) {
                            }
                            else {
                                $newsqltab[0] = "INNER JOIN monitoria $amoni ON $amoni.idmonitoria = $amonitab.idmonitoria";
                            }
                            ksort($newsqltab);
                            $seltabtt = "SELECT COUNT(DISTINCT($amonitab.idmonitoria)) as qtde FROM monitabulacao $amonitab ".implode(" ",$newsqltab)." WHERE $where";
                            $eseltabtt = $_SESSION['query']($seltabtt) or die ("erro na query de consulta da quantidade total");
                            $ntt = $_SESSION['num_rows']($eseltabtt);
                            if($ntt >= 1) {
                                $lseltabtt = $_SESSION['fetch_array']($eseltabtt);
                                $tabtt = $lseltabtt['qtde'];
                            }
                            else {
                                $tabtt = 0;
                            }

                            // consulta da tabulaÃ§Ã£o
                            $montatab = "SELECT * FROM tabulacao t
                                         INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                                         INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab
                                         WHERE t.idtabulacao='$this->idtabulacao' GROUP BY t.idperguntatab ORDER BY t.posicao";
                            $emonta = $_SESSION['query']($montatab) or die ("erro na query de consulta da tabulacao");
                            while($lmonta = $_SESSION['fetch_array']($emonta)) {
                            $seltab = "SELECT $amonitab.idperguntatab, $apergtab.descriperguntatab,COUNT(DISTINCT($amonitab.idmonitoria)) as qtde FROM monitabulacao $amonitab ".implode(" ",$newsqltab)." WHERE $where AND $amonitab.idperguntatab='".$lmonta['idperguntatab']."'";
                            $eseltab = $_SESSION['fetch_array']($_SESSION['query']($seltab)) or die ("erro na query de consulta da tabulaÃ§Ã£o");
                            //while($lseltab = $_SESSION['fetch_array']($eseltab)) {
                            if($eseltab['qtde'] == 0) {
                                $this->dadosrel['perg'.$lmonta['idperguntatab']][$kfreq]['descri'] = $lmonta['descriperguntatab'];
                                $this->dadosrel['perg'.$lmonta['idperguntatab']][$kfreq]['qtde'] = "--";
                                $this->dadosrel['perg'.$lmonta['idperguntatab']][$kfreq]['perc'] = "--";
                            }
                            else {
                                $this->dadosrel['perg'.$lmonta['idperguntatab']][$kfreq]['descri'] = $eseltab['descriperguntatab'];
                                $this->dadosrel['perg'.$lmonta['idperguntatab']][$kfreq]['qtde'] = $eseltab['qtde'];
                                $calctab = ($eseltab['qtde'] / $tabtt) * 100;
                                $this->dadosrel['perg'.$lmonta['idperguntatab']][$kfreq]['perc'] = number_format($calctab,2);
                            }
                                $selresp = "SELECT t.idtabulacao,t.idperguntatab,t.idrespostatab,rt.descrirespostatab FROM tabulacao t
                                            INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab
                                            WHERE idtabulacao='$this->idtabulacao'AND idperguntatab='".$lmonta['idperguntatab']."'";
                                $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das perguntas relacionadas");
                                while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                    $selresptab = "SELECT $amonitab.idrespostatab, $aresptab.descrirespostatab,COUNT(DISTINCT($amonitab.idmonitoria)) as qtde
                                                   FROM monitabulacao $amonitab ".implode(" ",$newsqltab)."
                                                   WHERE $where AND $amonitab.idperguntatab='".$lmonta['idperguntatab']."' AND $amonitab.idrespostatab='".$lselresp['idrespostatab']."'
                                                   GROUP BY $amonitab.idrespostatab";
                                    $eselresptab = $_SESSION['query']($selresptab) or die ("erro na query de consulta da tabulaÃ§Ã£o");
                                    $nresp = $_SESSION['num_rows']($eselresptab);
                                    $chave = "resp".$lmonta['idperguntatab']."_".$lselresp['idrespostatab'];
                                    if($nresp == 0) {
                                        $this->dadosrel[$chave][$kfreq]['descri'] = $lselresp['descrirespostatab'];
                                        $this->dadosrel[$chave][$kfreq]['qtde'] = "--";
                                        $this->dadosrel[$chave][$kfreq]['perc'] = "--";
                                    }
                                    else {
                                        while($lresptab = $_SESSION['fetch_array']($eselresptab)) {
                                            $this->dadosrel[$chave][$kfreq]['descri'] = $lresptab['descrirespostatab'];
                                            $this->dadosrel[$chave][$kfreq]['qtde'] = $lresptab['qtde'];
                                            $calctab = ($lresptab['qtde'] / $tabtt) * 100;
                                            $this->dadosrel[$chave][$kfreq]['perc'] = number_format($calctab,2);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
    }
}

class RelProducao extends TabelasSql {
    public $periodo;
    public $tipo;
    public $datas;
    public $diaspass;
    public $topten;
    public $cdatas;
    public $dados;
    public $dadosmoni;
    public $vidados;
    public $tipooper;
    public $colunas;
    public $agrupa;
    public $micro;
    public $monitor;
    public $meta;

    function dadosprod ($tipo,$cdatas) {
        $this->colunas =( $cdatas * 4) + 2;
        if($tipo == "planningctt") {
            $where = "rf.id_".strtolower($this->agrupa)."='$this->micro'";
            $seldimen = "SELECT dm.idrel_filtros,(dm.estimativa / p.diasprod) as metadia,dm.estimativa FROM dimen_mod dm
                         INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                         INNER JOIN periodo p ON p.idperiodo=dm.idperiodo
                         WHERE dm.idperiodo='".$this->periodo['idperiodo']."' AND $where";
            $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento");
            while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                foreach($this->datas as $kdt => $dt) {
                        foreach($dt as $kd => $d) {
                            $selprod = "SELECT COUNT(DISTINCT(m.idmonitoria)) as prod FROM monitoria m
                                        WHERE m.datactt BETWEEN '".$d[0]."' AND '".$d[1]."' AND m.idrel_filtros='".$lseldimen['idrel_filtros']."'";
                            $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die ("erro na query de consulta da produtividade");
                            if($this->tipooper == "d") {
                                $data = banco2data($d[0]);
                            }
                            else {
                                $data = banco2data($d[0])."-".banco2data($d[1]);
                            }
                            $this->dados[$data][$lseldimen['idrel_filtros']][] = $eselprod['prod'];
                        }
                }
                $this->meta[$lseldimen['idrel_filtros']]['mes'] = $lseldimen['estimativa'];
                $this->meta[$lseldimen['idrel_filtros']]['dia'] = round($lseldimen['metadia']);
            }

        }
        if($tipo == "planning") {
            $where = "rf.id_".strtolower($this->agrupa)."='$this->micro'";
            $diaini = substr($this->periodo['dataini'],0,4)."-".substr($this->periodo['dataini'],5,2)."-01";
            $diafim = ultdiames(substr($diaini,8,2).substr($diaini,5,2).substr($diaini,0,4));
            $seldimen = "SELECT dm.idrel_filtros,(dm.estimativa / p.diasprod) as metadia,dm.estimativa FROM dimen_mod dm
                         INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                         INNER JOIN periodo p ON p.idperiodo=dm.idperiodo
                         WHERE dm.idperiodo='".$this->periodo['idperiodo']."' AND $where";
            $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento");
            while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                foreach($this->datas as $kdt => $dt) {
                    foreach($dt as $kd => $d) {
                        if($_SESSION['selbanco'] == "monitoria_pj_itau") {
                            $selprod = "SELECT COUNT(DISTINCT(m.idmonitoria)) as prod FROM monitoria m
                                        WHERE m.data BETWEEN '".$d[0]."' AND '".$d[1]."' AND m.idrel_filtros='".$lseldimen['idrel_filtros']."'";
                        }
                        else {
                            $selprod = "SELECT COUNT(DISTINCT(m.idmonitoria)) as prod FROM monitoria m
                                        WHERE m.data BETWEEN '".$d[0]."' AND '".$d[1]."' AND m.datactt BETWEEN '".$this->periodo['datacttini']."' AND '".$this->periodo['datacttfim']."' AND m.idrel_filtros='".$lseldimen['idrel_filtros']."'";
                        }
                        $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die ("erro na query de consulta da produtividade");
                        if($this->tipooper == "d") {
                            $data = banco2data($d[0]);
                        }
                        else {
                            $data = banco2data($d[0])."-".banco2data($d[1]);
                        }
                        $this->dados[$data][$lseldimen['idrel_filtros']][] = $eselprod['prod'];
                    }
                }
                $this->meta[$lseldimen['idrel_filtros']]['mes'] = $lseldimen['estimativa'];
                $this->meta[$lseldimen['idrel_filtros']]['dia'] = round($lseldimen['metadia']);
            }

        }
        if($tipo == "monitor") {
              $selperiodo = "SELECT * FROM periodo WHERE idperiodo='".$_POST['periodo']."'";
              $eperiodo = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_error());
              $p['dataini'] = $eperiodo['dataini'];
              $p['datafim'] = $eperiodo['datafim'];
              if($this->monitor == "TODOS") {
                  $where = "";
              }
              else {
                  $where = "WHERE m.idmonitor='$this->monitor'";
              }
              if($this->micro != "") {
                $whererel .= "AND rf.id_".strtolower($this->agrupa)."='$this->micro'";
              }
              $selmoni = "SELECT m.idmonitor,m.nomemonitor,m.ativo FROM monitor m $where ORDER BY nomemonitor";
              $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
              while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                  $tt = 0;
                  $monitorias = "SELECT COUNT(*) as result FROM monitoria m
                                INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                                WHERE idmonitor='".$lselmoni['idmonitor']."' AND data BETWEEN '".$p['dataini']."' AND '".$p['datafim']."' $whererel";
                  $emonitorias = $_SESSION['fetch_array']($_SESSION['query']($monitorias)) or die ("erro na query de consulta da quantidade de monitorias realizadas");
                  if($emonitorias['result'] == 0 && $lselmoni['ativo'] == "N") {
                  }
                  else {
                    $dados[$emonitorias['nomemonitor']]['somatt'] = 0;
                    $where = "m.idmonitor='".$lselmoni['idmonitor']."'";
                  //$monitorias = "SELECT COUNT(idmonitoria) as result, idrel_filtros FROM monitoria WHERE idmonitor='".$lselmoni['idmonitor']."' GROUP BY idrel_filtros";
                  //$emonitorias = $_SESSION['query']($monitorias) or die ("erro na query de consulta das monitorias por operacao");
                  //while($lmonitorias = $_SESSION['fetch_array']($emonitorias)) {
                    //$this->dados[$lselmoni['idmonitor']][$lmonitorias['idrel_filtros']]['total'] = $lmonitorias['result'];
                    foreach($this->datas as $td => $datas) {
                        foreach($datas as $kdata => $data) {
                            if($td == "d") {
                                $adata = banco2data($data[0]);
                                $wdatas = "AND m.data='$data[0]'";
                            }
                            if($td == "s") {
                                $adata = banco2data($data[0])." / ".banco2data($data[1]);
                                $wdatas = "AND m.data BETWEEN '$data[0]' AND '$data[1]'";
                            }
                            if($td == "m") {
                                $adata = $kdata;
                                $wdatas = "AND m.data BETWEEN '$data[0]' AND '$data[1]'";
                                $selper = "SELECT * FROM periodo WHERE dataini='".$data[0]."' AND datafim='".$data[1]."'";
                                $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die (mysql_error());
                            }
                            $monidt = "SELECT COUNT(*) as result, round(((SUM(time_to_sec(tmpaudio))/count(*))),'%H:%i:%s') as tma, round((SUM(time_to_sec(horafim)) - SUM(time_to_sec(horaini)))/count(*)) as tmm FROM monitoria m
                                        INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                                        WHERE $where $wdatas $whererel";
                            $emonidt = $_SESSION['fetch_array']($_SESSION['query']($monidt)) or die ("erro query de consulta das monitorias por dia");
                            $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#realizado'] = $emonidt['result'];
                            $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttrealizado'] + $emonidt['result'];
                            $selmeta = "SELECT *,count(*) as r FROM dimen_moni m WHERE $where AND idperiodo='".$eselper['idperiodo']."' AND ativo='S'";
                            $eselmeta = $_SESSION['fetch_array']($_SESSION['query']($selmeta)) or die (mysql_error());
                            if($eselmeta['r'] == 0) {
                                if($td == "d") {
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#projetado'] = "--";
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] + 0;
                                }
                                if($td == "s") {
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#projetado'] = "--";
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] + 0;
                                }
                                if($td == "m") {
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#projetado'] = "--";
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] + 0;
                                }
                            }
                            else {
                                if($td == "d") {
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#projetado'] = $eselmeta['meta_d'];
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] + $eselmeta['meta_d'];
                                }
                                if($td == "s") {
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#projetado'] = $eselmeta['meta_s'];
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] + $eselmeta['meta_s'];
                                }
                                if($td == "m") {
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#projetado'] = $eselmeta['meta_m'];
                                    $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttprojetado'] + $eselmeta['meta_m'];
                                }
                            }

                            //TMA
                            $this->dados[$lselmoni['nomemonitor']]['tma'][$adata.'#projetado'] = sec_hora($emonidt['tma']);
                            $this->dados[$lselmoni['nomemonitor']]['tma'][$adata.'#realizado'] = sec_hora($emonidt['tmm']);
                            $this->dados[$lselmoni['nomemonitor']]['tma']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['tma']['ttrealizado'] + $emonidt['tmm'];
                            $this->dados[$lselmoni['nomemonitor']]['tma']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['tma']['ttprojetado'] + $emonidt['tma'];

                            //contestação
                            $selcontesta = "SELECT count(*) as r FROM monitoria m"
                                    . " INNER JOIN monitoria_fluxo mf ON mf.idmonitoria = m.idmonitoria"
                                    . " WHERE idmonitor='".$lselmoni['idmonitor']."' $wdatas $whererel AND iddefinicao='4'";
                            $eselcontesta = $_SESSION['fetch_array']($_SESSION['query']($selcontesta)) or die (mysql_error());
                            $selimpro = "SELECT count(*) as r FROM monitoria m"
                                    . " INNER JOIN monitoria_fluxo mf ON mf.idmonitoria = m.idmonitoria"
                                    . " WHERE idmonitor='".$lselmoni['idmonitor']."' $wdatas $whererel AND iddefinicao='5'";
                            $lselimpro = $_SESSION['fetch_array']($_SESSION['query']($selimpro)) or die (mysql_error());
                            $this->dados[$lselmoni['nomemonitor']]['Qualidade'][$adata.'#realizado'] = $lselimpro['r'];
                            $this->dados[$lselmoni['nomemonitor']]['Qualidade'][$adata.'#projetado'] = $eselcontesta['r'];
                            $this->dados[$lselmoni['nomemonitor']]['Qualidade']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['Qualidade']['ttrealizado'] + $lselimpro['r'];
                            $this->dados[$lselmoni['nomemonitor']]['Qualidade']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['Qualidade']['ttprojetado'] + $eselcontesta['r'];

                            //feedback
                            $sfeedback = "SELECT count(*) as r FROM monitoria m"
                                    . " INNER JOIN monitoria_fluxo mf ON mf.idmonitoria = m.idmonitoria"
                                    . " WHERE idmonitor='".$lselmoni['idmonitor']."' $wdatas $whererel AND iddefinicao='0007'";
                            $efeedback = $_SESSION['fetch_array']($_SESSION['query']($sfeedback)) or die (mysql_error());
                            $this->dados[$lselmoni['nomemonitor']]['Feedback'][$adata.'#realizado'] = $efeedback['r'];
                            $this->dados[$lselmoni['nomemonitor']]['Feedback'][$adata.'#projetado'] = $this->dados[$lselmoni['nomemonitor']]['produtividade'][$adata.'#realizado'];
                            $this->dados[$lselmoni['nomemonitor']]['Feedback']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['Feedback']['ttrealizado'] + $efeedback['r'];
                            $this->dados[$lselmoni['nomemonitor']]['Feedback']['ttprojetado'] =  $this->dados[$lselmoni['nomemonitor']]['produtividade']['ttrealizado'];

                            //atrasos
                            $dados['soma'] = 0;
                            $dados['previsto'] = 0;
                            $login = "SELECT TIME_TO_SEC(m.tempo) as tempo,(TIME_TO_SEC(m.tempo) - mt.tempo) as diff FROM moni_pausa m"
                                    . " INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor "
                                    . " INNER JOIN motivo mt ON mt.idmotivo = m.idmotivo"
                                    . " WHERE $where $wdatas AND m.idmotivo IN (4,5,7,17,19,20)";
                            $elogin = $_SESSION['query']($login) or die (mysql_error());
                            $nlogin = $_SESSION['num_rows']($elogin);
                            if($nlogin == 0) {
                                $this->dados[$lselmoni['nomemonitor']]['atrasos'][$adata.'#projetado'] = "0";
                                $this->dados[$lselmoni['nomemonitor']]['atrasos'][$adata.'#realizado'] = "0";
                            }
                            else {
                                while($llogin = $_SESSION['fetch_array']($elogin)) {
                                    if($llogin['diff'] > 0) {
                                        $dados['soma'] = $dados['soma'] + $llogin['diff'];
                                        $dados['previsto'] = $dados['previsto'] + $llogin['tempo'];
                                    }
                                    else {
                                        if($llogin['tempo'] >= 0) {
                                            $dados['previsto'] = $dados['previsto'] + $llogin['tempo'];
                                        }
                                    }
                                }
                                if($dados['soma'] > 0) {
                                    $convert = "SELECT SEC_TO_TIME(".$dados['soma'].") as tempo";
                                    $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                    $atraso = $econvert['tempo'];
                                    $convert = "SELECT SEC_TO_TIME(".$dados['previsto'].") as tempo";
                                    $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                    $previsto = $econvert['tempo'];
                                }
                                else {
                                    $atraso = "0";
                                    $previsto = "0";
                                }
                                $this->dados[$lselmoni['nomemonitor']]['atrasos'][$adata.'#realizado'] = $atraso;
                                $this->dados[$lselmoni['nomemonitor']]['atrasos'][$adata.'#projetado'] = $previsto;
                                $this->dados[$lselmoni['nomemonitor']]['atrasos']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['atrasos']['ttrealizado'] + $dados['soma'];
                                $this->dados[$lselmoni['nomemonitor']]['atrasos']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['atrasos']['ttprojetado'] + $dados['previsto'];
                            }

                            //absenteismo
                            if($td == "d") {
                                if(jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($data[0],5,2),substr($data[0],8,2),substr($data[0],0,4))) == 0 OR jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($data[0],5,2),substr($data[0],8,2),substr($data[0],0,4))) == 6) {
                                    $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#projetado'] = "--";
                                    $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] = "--";
                                }
                                else {
                                    $selfalta = "SELECT count(*) as r FROM moni_login WHERE data='$data[0]'";
                                    $eselfalta = $_SESSION['fetch_array']($_SESSION['query']($selfalta)) or die (mysql_error());
                                    $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#projetado'] = 0;
                                    if($eselfalta['r'] == 0) {
                                        $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] = 1;
                                        $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] + 1;
                                    }
                                    else {
                                        $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] = 0;
                                        $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] + 0;
                                    }
                                }
                            }
                            else {
                                $dtcheck = $data[0];
                                $i = 0;
                                for(;$i == 0;) {
                                    $verifdt = "SELECT '$dtcheck' <= '$data[1]' as v, ADDDATE('$dtcheck',+1) as newdata";
                                    $everifdt = $_SESSION['fetch_array']($_SESSION['query']($verifdt)) or die (mysql_error());
                                    if($everifdt['v'] == 1) {
                                        if(jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($dtcheck,5,2),substr($dtcheck,8,2),substr($dtcheck,0,4))) != 0 && jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($dtcheck,5,2),substr($dtcheck,8,2),substr($dtcheck,0,4))) != 6) {
                                            $selfalta = "SELECT count(*) as r FROM moni_login m WHERE data='$dtcheck' AND $where";
                                            $eselfalta = $_SESSION['fetch_array']($_SESSION['query']($selfalta)) or die (mysql_error());
                                            $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#projetado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#projetado'] + 1;
                                            $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttprojetado'] =  $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttprojetado'] + 1;
                                            if($eselfalta['r'] == 0) {
                                                $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] + 1;
                                                $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] + 1;
                                            }
                                            else {
                                                $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] + 0;
                                                $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] = $this->dados[$lselmoni['nomemonitor']]['absenteísmo']['ttrealizado'] + 0;
                                            }
                                        }
                                        else {
                                            if($td == "d") {
                                                $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#projetado'] = "--";
                                                $this->dados[$lselmoni['nomemonitor']]['absenteísmo'][$adata.'#realizado'] = "--";
                                            }
                                        }
                                    }
                                    else {
                                        $i = 1;
                                    }
                                    $dtcheck = $everifdt['newdata'];
                                }
                            }

                            //monitoração
                            $dados['mes'] = 0;
                            $login = "SELECT TIME_TO_SEC(m.tempo) as tempo FROM moni_pausa m"
                                    . " INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor "
                                    . " INNER JOIN motivo mt ON mt.idmotivo = m.idmotivo"
                                    . " WHERE $where $wdatas AND m.idmotivo IN (8,24,25)";
                            $elogin = $_SESSION['query']($login) or die (mysql_error());
                            $nlogin = $_SESSION['num_rows']($elogin);
                            if($nlogin == 0) {
                                $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva'][$adata.'#projetado'] = "0";
                            }
                            else {
                                while($llogin = $_SESSION['fetch_array']($elogin)) {
                                    if($llogin['tempo'] >= 0) {
                                        $dados['mes'] = $dados['mes'] + $llogin['tempo'];
                                    }
                                }
                                if($dados['mes'] > 0) {
                                    $convert = "SELECT SEC_TO_TIME(".$dados['mes'].") as tempo";
                                    $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                    $produtivo = $econvert['tempo'];
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva'][$adata.'#projetado'] = $produtivo;
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva']['ttprojetado'] + $dados['mes'];
                                }
                                else {
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva'][$adata.'#projetado'] = 0;
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva']['ttprojetado'] + 0;
                                }
                            }

                            //ações monitoração
                            $dados['mes'] = 0;
                            $login = "SELECT TIME_TO_SEC(m.tempo) as tempo FROM moni_pausa m"
                                    . " INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor "
                                    . " INNER JOIN motivo mt ON mt.idmotivo = m.idmotivo"
                                    . " WHERE $where $wdatas AND m.idmotivo IN (6,16,23)";
                            $elogin = $_SESSION['query']($login) or die (mysql_error());
                            $nlogin = $_SESSION['num_rows']($elogin);
                            if($nlogin == 0) {
                                $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2'][$adata.'#projetado'] = "";
                            }
                            else {
                                while($llogin = $_SESSION['fetch_array']($elogin)) {
                                    if($llogin['tempo'] >= 0) {
                                        $dados['mes'] = $dados['mes'] + $llogin['tempo'];
                                    }
                                }
                                if($dados['mes'] > 0) {
                                    $convert = "SELECT SEC_TO_TIME(".$dados['mes'].") as tempo";
                                    $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                    $produtivo = $econvert['tempo'];
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2'][$adata.'#projetado'] = $produtivo;
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2']['ttprojetado'] + $dados['mes'];
                                }
                                else {
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2'][$adata.'#projetado'] = 0;
                                    $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2']['ttprojetado'] = $this->dados[$lselmoni['nomemonitor']]['monitoracao/produtiva2']['ttprojetado'] + 0;
                                }
                            }
                        }
                    }
                  //}
                  }
              }
              /*
              $selmoni = "SELECT m.nomemonitor, m.idmonitor, dm.idrel_filtros, dm.meta_m, dm.meta_s, dm.meta_d FROM dimen_moni dm
                      INNER JOIN monitor m ON m.idmonitor = dm.idmonitor
                      INNER JOIN dimen_mod dmo ON dmo.iddimen_mod = dm.iddimen_mod
                      WHERE dmo.idperiodo='".$this->periodo['idperiodo']."' $where ORDER BY m.nomemonitor";
              $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta do dimensionamento");
              while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                    $this->dados[$lselmoni['nomemonitor']][] = $lselmoni['idrel_filtros']."#".$lselmoni['meta_m']."#".$lselmoni['meta_s']."#".$lselmoni['meta_d'];
              }
              */
        }
        if($tipo == "relacional") {
            $csem = "SELECT diasprod, COUNT(*) as result FROM interperiodo it INNER JOIN periodo p ON p.idperiodo = it.idperiodo WHERE it.idperiodo='".$this->periodo['idperiodo']."'";
            $ecsem = $_SESSION['fetch_array']($_SESSION['query']($csem)) or die ("erro na query de consulta da quantidade de semanas");
            $seldimen = "SELECT idrel_filtros, estimativa as meta_m, (estimativa / ".$ecsem['result'].") as meta_s, (estimativa / ".$ecsem['diasprod'].") as meta_d FROM dimen_mod
                         WHERE idperiodo='".$this->periodo['idperiodo']."'";
            $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento");
            while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                foreach($this->datas as $kdt => $dt) {
                    foreach($dt as $kd => $d) {
                        $selprod = "SELECT COUNT(DISTINCT(m.idmonitoria)) as prod, SEC_TO_TIME(SUM(TIME_TO_SEC(horafim) - TIME_TO_SEC(horaini))) as tempo
                                            FROM monitoria m
                                            WHERE m.data BETWEEN '".$d[0]."' AND '".$d[1]."' AND m.idrel_filtros='".$lseldimen['idrel_filtros']."'";
                        $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die ("erro na query de consulta da produtividade");
                        if($eselprod['tempo'] == "") {
                            $tempo = "00:00:00";
                        }
                        else {
                            $tempo = $eselprod['tempo'];
                        }
                        $relapres = nomeapres($lseldimen['idrel_filtros']);
                        if($kdt == "d") {
                            $meta = round($lseldimen['meta_d']);
                        }
                        if($kdt == "s") {
                            $meta = round($lseldimen['meta_s']);
                        }
                        if($kdt == "m") {
                            $meta = round($lseldimen['meta_m']);
                        }
                        $this->dados[$relapres][$d[0]." ".$d[1]]['meta_'.$kdt] = $meta;
                        $this->dados[$relapres][$d[0]." ".$d[1]]['real'] = $eselprod['prod'];
                        $this->dados[$relapres][$d[0]." ".$d[1]]['perc %'] = round(($eselprod['prod'] / $lseldimen['meta_'.$kdt]) * 100, 2);
                        $this->dados[$relapres][$d[0]." ".$d[1]]['tempo'] = $tempo;
                    }
                }
            }
        }
        if($tipo == "filtro") {
            $csem = "SELECT diasprod, COUNT(*) as result FROM interperiodo it INNER JOIN periodo p ON p.idperiodo = it.idperiodo WHERE it.idperiodo='".$this->periodo['idperiodo']."'";
            $ecsem = $_SESSION['fetch_array']($_SESSION['query']($csem)) or die ("erro na query de consulta da quantidade de semanas");
            $seldados = "SELECT fd.idfiltro_dados, nomefiltro_dados FROM filtro_dados fd INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fn.nomefiltro_nomes='$this->agrupa'";
            $eseldados = $_SESSION['query']($seldados) or die ("erro na query de consulta dos nomes dos dados");
            while($lseldados = $_SESSION['fetch_array']($eseldados)) {
                $seldimen = "SELECT dm.idrel_filtros, estimativa as meta_m, (estimativa / ".$ecsem['result'].") as meta_s, (estimativa / ".$ecsem['diasprod'].") as meta_d FROM dimen_mod dm
                            INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                            WHERE dm.idperiodo='".$this->periodo['idperiodo']."' AND rf.id_".$this->agrupa."='".$lseldados['idfiltro_dados']."'";
                $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta dos dimensionamentos");
                while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                    foreach($this->datas as $kdt => $dt) {
                        foreach($dt as $kd => $d) {
                            $selprod = "SELECT COUNT(DISTINCT(m.idmonitoria)) as prod, SEC_TO_TIME(SUM(TIME_TO_SEC(horafim) - TIME_TO_SEC(horaini))) as tempo
                                        FROM monitoria m
                                        WHERE m.data BETWEEN '".$d[0]."' AND '".$d[1]."' AND m.idrel_filtros='".$lseldimen['idrel_filtros']."'";
                            $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die ("erro na query de consulta da produtividade");
                            if($eselprod['tempo'] == "") {
                                $tempo = "00:00:00";
                            }
                            else {
                                $tempo = $eselprod['tempo'];
                            }
                            $relapres = nomeapres($lseldimen['idrel_filtros']);
                            if($kdt == "d") {
                                $meta = round($lseldimen['meta_d']);
                            }
                            if($kdt == "s") {
                                $meta = round($lseldimen['meta_s']);
                            }
                            if($kdt == "m") {
                                $meta = round($lseldimen['meta_m']);
                            }
                            $this->dados[$lseldados['nomefiltro_dados']][$relapres][$d[0]." ".$d[1]] = 'meta_'.$kdt."#".$meta.",real#".$eselprod['prod'].",perc %#".round(($eselprod['prod'] / $lseldimen['meta_'.$kdt]) * 100, 2).",tempo#".$tempo;
                        }
                    }
                }
            }
        }
        if($tipo == "grafico") {
            $where = "rf.id_".strtolower($this->agrupa)."='$this->micro'";
            $selrel = "select SUM(estimativa) as qtde from dimen_mod dm
                       inner join conf_rel cr on cr.idrel_filtros = dm.idrel_filtros
                       inner join rel_filtros rf on rf.idrel_filtros = cr.idrel_filtros
                       where idperiodo='".$this->periodo['idperiodo']."' and ativo='S' and $where";
            $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta da quantidade");
            $diasfora = "SELECT data FROM diasdescon WHERE idperiodo='".$this->periodo['idperiodo']."'";
            $edias = $_SESSION['query']($diasfora) or die ("erro na query de consulta dos dias desconsiderados");
            while($ldias = $_SESSION['fetch_array']($edias)) {
                $diasdesc[$ldias['data']] = $ldias['data'];
            }
            $mes = "SELECT DATEDIFF('".$this->periodo['datafim']."','".$this->periodo['dataini']."') as mes";
            $emes = $_SESSION['fetch_array']($_SESSION['query']($mes)) or die ("erro na query de consulta da quantidade de dias do mês");
            $diasmes = ($emes['mes'] + 1) - count($diasdesc);
            $dtinictt = substr($this->periodo['dataini'], 0,4)."-".substr($this->periodo['dataini'],5,2)."-01";
            $dtfimctt = ultdiames(substr($this->periodo['dataini'],8,2).substr($this->periodo['dataini'],5,2).substr($this->periodo['dataini'],0,4));
            $qtde = $eselrel['qtde'];
            $dtfim = $this->periodo['datafim'];
            foreach($this->datas as $kdt => $dt) {
                foreach($dt as $kd => $d) {
                  if($this->tipooper == "d") {
                      $dtloop = $d[0];
                      $comdt = "SELECT '$dtloop' < '$dtfim' as menor, '$dtloop' = '$dtfim' as igual";
                      $ecomdt = $_SESSION['fetch_array']($_SESSION['query']($comdt)) or die ("erro na query de comparação das datas");
                      if($ecomdt['menor'] >= 1 OR $ecomdt['igual'] >= 1) {
                          $verifdt = jddayofweek ( cal_to_jd(CAL_GREGORIAN, substr($dtloop,5,2),substr($dtloop,8,2), substr($dtloop,0,4)), 0);
                          if($verifdt == 0 OR $verifdt == 6) {
                          }
                          else {
                              //$diasmes[$dtloop] = $dtloop;
                              $compatu = "SELECT '$dtloop' > '".date('Y-m-d')."' as dtatu";
                              $ecompatu = $_SESSION['fetch_array']($_SESSION['query']($compatu)) or die ("erro na query de comparação da data com a data atual");
                              if($ecompatu['dtatu'] >= 1) {
                              }
                              else {
                                $this->diaspass[$dtloop] = $dtloop;
                              }
                          }
                      }
                      else {
                      }
                  }
                  if($this->tipooper == "s" OR $this->tipooper == "m") {
                      $compdt = "SELECT '$d[0]' BETWEEN '".$this->periodo['dataini']."' AND '".date('Y-m-d')."' as result";
                      $ecompdt  = $_SESSION['fetch_array']($_SESSION['query']($compdt)) or die ("erro na query de comparaçao das datas");
                      if($ecompdt['result'] >= 1) {
                          $compfim = "SELECT '$d[1]' > '".date('Y-m-d')."' as result";
                          $ecompfim = $_SESSION['fetch_array']($_SESSION['query']($compfim)) or die ("erro na query de comparação da data final");
                          if($ecompfim['result'] >= 1) {
                              $dtfimcomp = date('Y-m-d');
                          }
                          else {
                              $dtfimcomp = $d[1];
                          }
                          $this->diaspass[$d[0].";".$dtfimcomp] = $d[0].";".$dtfimcomp;
                      }
                      else {
                      }
                  }
                }
            }
            $metad = floor(($qtde / $diasmes));
            foreach($this->diaspass as $dia) {
                $descsem = 0;
                if($this->tipooper == "d") {
                    $wheretipo = "WHERE data='$dia' AND datactt BETWEEN '$dtinictt' AND '$dtfimctt' AND";
                }
                else {
                    $dias = explode(";",$dia);
                    $wheretipo = "WHERE data BETWEEN '$dias[0]' AND '$dias[1]' AND datactt BETWEEN '$dtinictt' AND '$dtfimctt' AND";
                    $diassem ="SELECT DATEDIFF('$dias[1]','$dias[0]') as dif";
                    $ediassem = $_SESSION['fetch_array']($_SESSION['query']($diassem)) or die ("erro na query de consulta dos dias da semana");
                    $dsem = $ediassem['dif'] + 1;
                }
                $selqtde = "SELECT COUNT(*) as qtde FROM monitoria m
                                    INNER JOIN rel_filtros rf on rf.idrel_filtros = m.idrel_filtros
                                    $wheretipo $where";
                $eselqtde = $_SESSION['fetch_array']($_SESSION['query']($selqtde)) or die ("erro na query de consulta da quantidade de monitorias");
                if($this->tipooper == "d") {
                    $metaac = $metaac + $metad;
                }
                else {
                    foreach($diasdesc as $desc) {
                        $vdesc = "SELECT '$desc' BETWEEN '$dias[0]' AND '$dias[1]' as desconta";
                        $evdesc = $_SESSION['fetch_array']($_SESSION['query']($vdesc)) or die ("erro na query de consulta dos dias descontados");
                        if($evdesc['desconta'] >= 1) {
                            $descsem++;
                        }
                    }
                    $metaac = $metaac + ($metad * ($dsem - $descsem));
                }
                $prod = $prod +  $eselqtde['qtde'];
                $this->dados['acumulado'][] = round($metaac,0);
                $this->dados['produzido'][] = $prod;
                $this->dados['diferenca'][] = $metaac - $prod;
            }
            $seldimen = "select dm.idrel_filtros, estimativa from dimen_mod dm
                        inner join conf_rel cr on cr.idrel_filtros = dm.idrel_filtros
                        inner join rel_filtros rf on rf.idrel_filtros = cr.idrel_filtros
                        where idperiodo='".$this->periodo['idperiodo']."' and ativo='S' AND $where group by dm.idrel_filtros";
            $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento por relaconamento");
            while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                $metaacop = round(($lseldimen['estimativa'] / $diasmes) * count($this->diaspass));
                $selprod = "SELECT COUNT(*) as qtde FROM monitoria WHERE datactt BETWEEN '$dtinictt' AND '$dtfimctt' AND idrel_filtros='".$lseldimen['idrel_filtros']."'";
                $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die ("erro na query de consulta da produção");
                $percs[$lseldimen['idrel_filtros']] = round(($eselprod['qtde'] / $lseldimen['estimativa']) * 100,1);
                $dados[$lseldimen['idrel_filtros']]['nome'] = nomevisu($lseldimen['idrel_filtros']);
                $dados[$lseldimen['idrel_filtros']]['meta'] = $metaacop;
                $dados[$lseldimen['idrel_filtros']]['produzido'] = $eselprod['qtde'];
            }
            asort($percs);
            $topten = array_slice($percs,0,10);
            foreach($topten as $idperc => $perc) {
                $this->topten[$dados[$idperc]['nome']]['meta'] = ($dados[$idperc]['meta'] - $dados[$idperc]['produzido']);
                $this->topten[$dados[$idperc]['nome']]['qtde'] = $dados[$idperc]['produzido'];
            }
        }
    }

    function montadados() {
        if($this->tipo == "monitor") {
            $metricaitens = array('produtividade' => 'Qtde','tma' => 'Tempo','absenteísmo' => 'Qtde','atrasos' => 'Tempo','Qualidade' => 'Qtde','Feedback' => 'Qtde','monitoracao/produtiva' => 'Tempo',
                'monitoracao/produtiva2' => 'Tempo');
            $this->vidados .= "<div style=\"width: 100%; overflow: auto\">";
            $this->vidados .= "<table id=\"producao\" width=\"100%\" border=\"0px\" cellpadding=\"0px\">";
                foreach($this->dados as $monitor => $itens) {
                    $ttatrasos = 0;
                    $ttproj = 0;
                    $ttprod = 0;
                    $ttprod2 = 0;
                    $this->vidados .= "<tr bgcolor=\"#5270EA\">";
                        $this->vidados .= "<td colspan=\"".(count($this->datas[$this->tipooper]) + 4)."\" style=\"font-size: 14px; font-weight: bold; color:#FFF\" align=\"center\">";
                            $this->vidados .= "<div style=\"width: 100%; padding: 20px\">MONITORIA DE QUALIDADE<br/>Painel de Resultados do Monitor - $monitor";
                            $this->vidados .= "</div>";
                        $this->vidados .= "</td>";
                    $this->vidados .= "</tr>";
                    $this->vidados .= "<tr bgcolor=\"#EAB47E\" style=\"font-weight: bold\">";
                        $this->vidados .= "<td align=\"center\">ITEM CONTROLE</td>";
                        $this->vidados .= "<td align=\"center\">MÉTRICA</td>";
                        $this->vidados .= "<td align=\"center\"></td>";
                        $cdt = 0;
                        foreach($this->datas as $td => $datas) {
                            foreach($datas as $kdata => $data) {
                                $cdt++;
                                if($td == "d") {
                                    $adata = banco2data($data[0]);
                                }
                                if($td == "s") {
                                    $adata = banco2data($data[0])." / ".banco2data($data[1]);
                                }
                                if($td == "m") {
                                    $adata = $kdata;
                                }
                                $this->vidados .= "<td align=\"center\">$adata</td>";
                            }
                        }
                        $this->vidados .= "<td align=\"center\">ACUMULADO</td>";
                    $this->vidados .= "</tr>";
                    $complementos = array('produtividade' => ' (avaliações efetivadas)','tma' => '(avaliações efetivadas - Media)','Qualidade' => ' (na execução/contestações)','monitoracao/produtiva' => '(ações extras)',
                        'monitoracao/produtiva2' => '(ações monitoração)');
                    foreach($itens as $item => $dados) {
                        $c++;
                        if($c % 2) {
                            $bgcolor = "#FFFFFF";
                        }
                        else {
                            $bgcolor = "#D4ECFF";
                        }
                        $this->vidados .= "<tr bgcolor=\"$bgcolor\">";
                            if($item == 'monitoracao/produtiva' OR $item == 'monitoracao/produtiva2') {
                                if($item == "monitoracao/produtiva") {
                                    $this->vidados .= "<td align=\"center\">".strtoupper($item)." ".$complementos[$item]."</td>";
                                    $this->vidados .= "<td align=\"center\">$metricaitens[$item]</td>";
                                }
                                else {
                                    $this->vidados .= "<td align=\"center\">".str_replace("2","",strtoupper($item))." ".$complementos[$item]."</td>";
                                    $this->vidados .= "<td align=\"center\">$metricaitens[$item]</td>";
                                }
                            }
                            else {
                            $this->vidados .= "<td align=\"center\" rowspan=\"2\">".strtoupper($item)." ".$complementos[$item]."</td>";
                            $this->vidados .= "<td align=\"center\" rowspan=\"2\">$metricaitens[$item]</td>";
                            }
                            foreach($this->datas as $td => $datas) {
                                if($item == "Qualidade") {
                                    $this->vidados .= "<td align=\"center\">PROCEDENTE</td>";
                                }
                                else {
                                    if($item == 'monitoracao/produtiva' OR $item == 'monitoracao/produtiva2') {
                                        $this->vidados .= "<td align=\"center\">REALIZADO</td>";
                                    }
                                    else {
                                        $this->vidados .= "<td align=\"center\">PREVISTO</td>";
                                    }
                                }
                                foreach($datas as $kdata => $data) {
                                    if($td == "d") {
                                        $adata = banco2data($data[0]);
                                    }
                                    if($td == "s") {
                                        $adata = banco2data($data[0])." / ".banco2data($data[1]);
                                    }
                                    if($td == "m") {
                                        $adata = $kdata;
                                    }
                                    if($item == "atrasos" OR $item == "tma" OR $item == "monitoracao/produtiva" OR $item == "monitoracao/produtiva2") {
                                        if($dados[$adata.'#projetado'] == "") {
                                            $ttproj = $ttproj + 0;
                                            $ttprod = $ttprod + 0;
                                            $ttprod2 = $ttprod2 + 0;
                                        }
                                        else {
                                            $convert = "SELECT TIME_TO_SEC('".$dados[$adata.'#projetado']."') as tempo";
                                            $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                            if($item == "atrasos") {
                                                $ttproj = $ttproj + $econvert['tempo'];
                                            }
                                            else {
                                                if($item === "monitoracao/produtiva") {
                                                    $ttprod = $ttprod + $econvert['tempo'];
                                                }
                                                else {
                                                    if($item == "tma") {
                                                        $ttma = $ttma + $econvert['tempo'];
                                                    }
                                                    else {
                                                        $ttprod2 = $ttprod2 + $econvert['tempo'];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if($dados[$adata.'#projetado'] == "") {
                                        $this->vidados .= "<td align=\"center\">0</td>";
                                    }
                                    else {
                                        $this->vidados .= "<td align=\"center\">".$dados[$adata.'#projetado']."</td>";
                                    }
                                }

                            }
                            if($item == "atrasos" OR $item == "monitoracao/produtiva" OR $item == "monitoracao/produtiva2") {
                                if($itens[$item]['ttprojetado'] == "") {
                                    $this->vidados .= "<td align=\"center\">0</td>";
                                }
                                else {
                                    if($item == "atrasos") {
                                        $convert = "SELECT SEC_TO_TIME(".$ttproj.") as tempo";
                                    }
                                    else {
                                        if($item === "monitoracao/produtiva") {
                                            $convert = "SELECT SEC_TO_TIME(".$ttprod.") as tempo";
                                        }
                                        else {
                                            $convert = "SELECT SEC_TO_TIME(".$ttprod2.") as tempo";
                                        }
                                    }
                                    $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                    $this->vidados .= "<td align=\"center\">".$econvert['tempo']."</td>";
                                }
                            }
                            else {
                                if($item == "tma") {
                                    $this->vidados .= "<td align=\"center\">".sec_hora(round($itens[$item]['ttprojetado']/$cdt))."</td>";
                                }
                                else {
                                    $this->vidados .= "<td align=\"center\">".$itens[$item]['ttprojetado']."</td>";
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        if($item != "monitoracao/produtiva" && $item != "monitoracao/produtiva2") {
                        $this->vidados .= "<tr bgcolor=\"$bgcolor\">";
                            foreach($this->datas as $td => $datas) {
                                if($item == "Qualidade") {
                                    $this->vidados .= "<td align=\"center\">IMPROCEDENTE</td>";
                                }
                                else {
                                $this->vidados .= "<td align=\"center\">REALIZADO</td>";
                                }
                                foreach($datas as $kdata => $data) {
                                    if($td == "d") {
                                        $adata = banco2data($data[0]);
                                    }
                                    if($td == "s") {
                                        $adata = banco2data($data[0])." / ".banco2data($data[1]);
                                    }
                                    if($td == "m") {
                                        $adata = $kdata;
                                    }
                                    if($item == "atrasos") {
                                        if($dados[$adata.'#realizado'] == 0) {
                                            $ttatrasos = $ttatrasos + 0;
                                        }
                                        else {
                                            $convert = "SELECT TIME_TO_SEC('".$dados[$adata.'#realizado']."') as tempo";
                                            $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                            $ttatrasos = $ttatrasos + $econvert['tempo'];
                                        }
                                    }
                                    $this->vidados .= "<td align=\"center\">".$dados[$adata.'#realizado']."</td>";
                                }
                            }
                            if($item == "atrasos") {
                                if($itens[$item]['ttrealizado'] == "") {
                                    $this->vidados .= "<td align=\"center\">0</td>";
                                }
                                else {
                                    $convert = "SELECT SEC_TO_TIME(".$ttatrasos.") as tempo";
                                    $econvert = $_SESSION['fetch_array']($_SESSION['query']($convert)) or die (mysql_error());
                                    $this->vidados .= "<td align=\"center\">".$econvert['tempo']."</td>";
                                }
                            }
                            else {
                                if($item == "tma") {
                                    $this->vidados .= "<td align=\"center\">".sec_hora(round($itens[$item]['ttrealizado']/$cdt))."</td>";
                                }
                                else {
                                    $this->vidados .= "<td align=\"center\">".$itens[$item]['ttrealizado']."</td>";
                                }
                            }
                        $this->vidados .= "</tr>";
                        }
                    }
                    $this->vidados .= "<tr bgcolor=\"#FFFFFF\"><td colspan=\"60\" height=\"60px\"></td></tr>";
                }
            $this->vidados .= "</table>";
            $this->vidados .= "</div>";
        }
        if($this->tipo == "relacional") {
            $this->vidados = "<div style=\"width:1024px; overflow: auto\">";
            $this->vidados .= "<table width=\"auto\">";
                $this->vidados .= "<tr>";
                    $this->vidados .= "<td class=\"corfd_ntab\" colspan=\"".$this->colunas."\"><strong>PRODUÇÃO RELACIONAMENTO</strong></td>";
                $this->vidados .= "</tr>";
                  $cols = array('meta_'.$this->tipooper,"real","perc %","tempo");
                  foreach($this->dados as $krel => $rel) {
                     $this->vidados .= "<tr>";
                        $this->vidados .= "<td class=\"corfd_colcampos\" rowspan=\"4\" style=\"min-width:400px; width:100%\">$krel</td>";
                     $this->vidados .= "</tr>";
                     $this->vidados .= "<tr>";
                          foreach($this->datas as $ktipo => $tipodata) {
                              foreach($tipodata as $dts) {
                                  if($ktipo == "d") {
                                    $this->vidados .= "<td class=\"corfd_coltexto\" align=\"center\" colspan=\"4\" style=\"min-width:200px; width:100%\">".banco2data($dts[0])."</td>";
                                  }
                                  else {
                                    $this->vidados .= "<td class=\"corfd_coltexto\" align=\"center\" colspan=\"4\" style=\"min-width:200px; width:100%\">".banco2data($dts[0])."-".banco2data($dts[1])."</td>";
                                  }
                              }
                          }
                      $this->vidados .= "</tr>";
                      $this->vidados .= "<tr>";
                          foreach($this->datas as $kt => $tp) {
                              foreach($tp as $dtcol) {
                                  foreach($cols as $col) {
                                    $this->vidados .= "<td class=\"corfd_coltexto\" style=\"min-width:50px; width:100%\" align=\"center\">".$col."</td>";
                                  }
                              }
                          }
                      $this->vidados .= "</tr>";
                      $this->vidados .= "<tr>";
                      foreach($rel as $kreldt => $reldt) {
                          foreach($reldt as $ds) {
                          $this->vidados .= "<td class=\"corfd_colcampos\" align=\"center\" style=\"min-width:50px; width:100%\">$ds</td>";
                          }
                      }
                      $this->vidados .= "</tr>";
                  }
                $this->vidados .= "</table>";
            $this->vidados .= "</div>";
        }
        if($this->tipo == "filtro") {
            $this->vidados = "<div style=\"width:1024px; overflow: auto\">";
                $this->vidados .= "<table width=\"auto\">";
                $this->vidados .= "<tr>";
                    $this->vidados .= "<td class=\"corfd_ntab\" colspan=\"".$this->colunas."\"><strong>PRODUÇÃO ".strtoupper($this->agrupa)."</strong></td>";
                    $this->vidados .= "</tr>";
                    $cols = array('meta_'.$this->tipooper,"real","perc %","tempo");
                    foreach($this->dados as $kgrup => $grup) {
                      $this->vidados .= "<tr>";
                          $this->vidados .= "<td class=\"corfd_colcampos\" rowspan=\"".(count($grup) + 3)."\" style=\"min-width:200px; width:100%\">$kgrup</td>";
                          $this->vidados .= "</tr>";
                          $this->vidados .= "<tr>";
                          $this->vidados .= "<td class=\"corfd_coltexto\"></td>";
                          foreach($this->datas as $ktipo => $tipodata) {
                              foreach($tipodata as $dts) {
                                  if($ktipo == "d") {
                                    $this->vidados .= "<td class=\"corfd_coltexto\" align=\"center\" colspan=\"4\" style=\"min-width:200px; width:100%\">".banco2data($dts[0])."</td>";
                                  }
                                  else {
                                    $this->vidados .= "<td class=\"corfd_coltexto\" align=\"center\" colspan=\"4\" style=\"min-width:200px; width:100%\">".banco2data($dts[0])."-".banco2data($dts[1])."</td>";
                                  }
                              }
                          }
                      $this->vidados .= "</tr>";
                      $this->vidados .= "<tr>";
                          $this->vidados .= "<td class=\"corfd_coltexto\"></td>";
                          foreach($this->datas as $kt => $tp) {
                              foreach($tp as $dtcol) {
                                  foreach($cols as $col) {
                                    $this->vidados .= "<td class=\"corfd_coltexto\" align=\"center\" style=\"min-width:50px; width:100%\">$col</td>";
                                  }
                              }
                          }
                      $this->vidados .= "</tr>";
                      foreach($grup as $krel => $rel) {
                      $this->vidados .= "<tr>";
                          $this->vidados .= "<td class=\"corfd_colcampos\" style=\"min-width:400px; width:100%\">$krel</td>";
                          foreach($rel as $ds) {
                              $newds = explode(",",$ds);
                              foreach($newds as $nds) {
                              $part = explode("#",$nds);
                              $this->vidados .= "<td class=\"corfd_colcampos\" align=\"center\">".$part[1]."</td>";
                              }
                          }
                      $this->vidados .= "</tr>";
                      }
                    }
                $this->vidados .= "</table>";
            $this->vidados .= "</div>";
        }
        if($this->tipo == "planning" OR $this->tipo == "planningctt") {
            $this->vidados = "<div style=\"width:1024px; overflow: auto\">";
                $this->vidados .= "<table width=\"auto\">";
                    $this->vidados .= "<tr>";
                        $this->vidados .= "<td class=\"corfd_ntab\" colspan=\"".$this->colunas."\"><strong>PRODUÇÃO MONITOR</strong></td>";
                    $this->vidados .= "</tr>";
                    $arel = $this->AliasTab(rel_filtros);
                    $adimen = $this->AliasTab(dimen_mod);
                    $innerrel = "SELECT * FROM filtro_nomes WHERE ativo='S' AND visualiza='S' ORDER BY nivel DESC";
                    $selinner = $_SESSION['query']($innerrel) or die ("erro na query de consulta dos filtros");
                    while($linner = $_SESSION['fetch_array']($selinner)) {
                        $alias = $this->AliasTab($linner['nomefiltro_nomes']);
                        $colsrel[] = "$alias.nomefiltro_dados as nome$alias";
                        $resultcol[] = "nome$alias";
                        $order[] = "$alias.nomefiltro_dados";
                        $inner[] = "INNER JOIN filtro_dados $alias ON $alias.idfiltro_dados=$arel.id_".strtolower($linner['nomefiltro_nomes'])."";
                    }

                    if($this->micro != "") {
                        $selrel = "SELECT $arel.idrel_filtros,".implode(",",$colsrel)." FROM rel_filtros $arel
                                   ".implode(" ",$inner)." INNER JOIN dimen_mod $adimen ON $adimen.idrel_filtros=$arel.idrel_filtros
                                   WHERE $arel.id_".strtolower($this->agrupa)."='$this->micro' AND idperiodo='".$this->periodo['idperiodo']."' ORDER BY ".implode(",",$order)."";
                    }
                    else {
                        $selrel = "SELECT $arel.idrel_filtros,".implode(",",$colsrel)." FROM rel_filtros $arel
                                   ".implode(" ",$inner)." INNER JOIN dimen_mod $adimen ON $adimen.idrel_filtros=$arel.idrel_filtros
                                   WHERE idperiodo='".$this->periodo['idperiodo']."' ORDER BY ".implode(",",$order)."";
                    }
                    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relacionamentos");
                    $nrel = $_SESSION['num_rows']($eselrel);
                    if($nrel >= 1) {
                        while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                            foreach($resultcol as $rcol) {
                                $idsrel[$lselrel['idrel_filtros']][$rcol] = $lselrel[$rcol];
                                $repete[$rcol][] = $lselrel[$rcol];
                            }
                        }
                        $crepete = array_count_values($repete);
                        $this->vidados .= "<tr class=\"corfd_coltexto\" align=\"center\">";
                        $this->vidados .= "<td rowspan=\"".count($colsrel)."\"></td>";
                        $cr = 0;
                        foreach($resultcol as $coldados) {
                            $cr++;
                            foreach($idsrel as $ndados) {
                                $idr++;
                                $this->vidados .= "<td class=\"corfd_coltexto\" align=\"center\" colspan=\"".$crepete[$ndados[$coldados]]."\">$ndados[$coldados]</td>";
                            }
                            $this->vidados .= "<td bgcolor=\"#ffffff\"></td><td bgcolor=\"#ffffff\"></td><td bgcolor=\"#ffffff\"></td></tr>";
                            if($cr == count($resultcol)) {
                                $this->vidados .= "</tr>";
                            }
                            else {
                                $this->vidados .= "<tr class=\"corfd_coltexto\">";
                            }
                        }
                        $this->vidados .= "<tr class=\"corfd_coltexto\"><td></td>";
                        $smeta = 0;
                            foreach($idsrel as $idm => $nmeta) {
                                $this->vidados .= "<td align=\"center\">".$this->meta[$idm]['dia']."</td>";
                            }
                        $this->vidados .= "<td><strong>Planning</strong></td><td><strong>Monitorias/Dia</strong></td><td><strong>%Acumulado</strong></td></tr>";
                        $this->vidados .= "<tr bgcolor=\"#FFFFFF\">";
                        $cdt = 0;
                        $qtdeidrel = array();
                        foreach($this->dados as $data => $dadosrel) {
                            $smeta = 0;
                            $descon = 0;
                            if($this->tipooper == "d" OR $this->tipooper == "s") {
                                $interdt = explode("-",$data);
                                if(count($interdt) == 1) {
                                    $cdata = "SELECT DATEDIFF('".data2banco($interdt[0])."','".data2banco($interdt[0])."') as result;";
                                    $ecdata = $_SESSION['fetch_array']($_SESSION['query']($cdata));
                                    $diasper = ($ecdata['result'] + 1);
                                }
                                else {
                                    $diasdesc = "SELECT data FROM diasdescon WHERE idperiodo='".$this->periodo['idperiodo']."'";
                                    $edesc = $_SESSION['query']($diasdesc) or die ("erro na query consulta dos dias desconsiderados");
                                    while($ldesc = $_SESSION['fetch_array']($edesc)) {
                                        $verifdesc = "SELECT '".$ldesc['data']."' BETWEEN '".data2banco($interdt[0])."' AND '".data2banco($interdt[1])."' as result";
                                        $everifdesc = $_SESSION['fetch_array']($_SESSION['query']($verifdesc)) or die ("erro na query de comparação das datas");
                                        if($everifdesc['result'] >= 1) {
                                            $descon++;
                                        }
                                        else {
                                        }
                                    }
                                    $cdata = "SELECT DATEDIFF('".data2banco($interdt[1])."','".data2banco($interdt[0])."') as result;";
                                    $ecdata = $_SESSION['fetch_array']($_SESSION['query']($cdata));
                                    $diasper = ($ecdata['result'] + 1) - $descon;
                                }
                            }
                            else {
                                $dprod = "SELECT diasprod FROM periodo WHERE idperiodo='".$this->periodo['idperiodo']."'";
                                $edprod = $_SESSION['fetch_array']($_SESSION['query']($dprod)) or die ("erro na query de consulta dos dias produtivos");
                                $diasper = $edprod['diasprod'];
                            }
                            $somadt = 0;
                            $cdt++;
                            $this->vidados .= "<td align=\"center\">$data</td>";
                            foreach($idsrel as $id => $dados) {
                                $somadt = $somadt + $dadosrel[$id][0];
                                $qtdeidrel[$id] = $qtdeidrel[$id] + $dadosrel[$id][0];
                                $this->vidados .= "<td align=\"center\">".$dadosrel[$id][0]."</td>";
                                $smeta = $smeta + ($this->meta[$id]['dia'] * $diasper);
                            }
                            if($cdt == count($this->dados)) {
                                $this->vidados .= "<td align=\"center\">$smeta</td>";
                                $this->vidados .= "<td align=\"center\">$somadt</td>";
                                $this->vidados .= "<td align=\"center\">".str_replace(".",",",number_format((($somadt/$smeta) * 100),2))."</td>";
                                $this->vidados .= "</tr>";
                            }
                            else {
                                $this->vidados .= "<td align=\"center\">$smeta</td>";
                                $this->vidados .= "<td align=\"center\">$somadt</td>";
                                $this->vidados .= "<td align=\"center\">".str_replace(".",",",number_format((($somadt/$smeta) * 100),2))."</td>";
                                $this->vidados .= "</tr>";
                                $this->vidados .= "<tr bgcolor=\"#FFFFFF\">";
                            }
                        }
                        $this->vidados .= "</tr>";
                        $this->vidados .= "<tr colspan=\"".$this->colunas."\"><td colspan=\"".$this->colunas."\"><br/></td></tr>";
                        $this->vidados .= "<tr colspan=\"".$this->colunas."\"><td colspan=\"".$this->colunas."\"><br/></td></tr>";
                        $this->vidados .= "<tr><td class=\"corfd_coltexto\" colspan=\"".$this->colunas."\"><strong>REALIZADO VS TOTAL MÊS</strong></td></tr>";
                        $ttmes = "";
                        $ttrel = "";
                        $colespe = array('Total Esperado EPS','Relizadas','%');
                        foreach($colespe as $kespe => $nespe) {
                            $this->vidados .= "<tr><td class=\"corfd_coltexto\"><strong>$nespe</strong></td>";
                            foreach($idsrel as $idqm => $nqm) {
                                if($kespe == "0") {
                                    $ttmes = $ttmes + $this->meta[$idqm]['mes'];
                                    $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".$this->meta[$idqm]['mes']."</td>";
                                }
                                if($kespe == "1") {
                                    $ttrel = $ttrel + $qtdeidrel[$idqm];
                                    $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".$qtdeidrel[$idqm]."</td>";
                                }
                                if($kespe == "2") {
                                    $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".str_replace(".",",",number_format((($qtdeidrel[$idqm]/$this->meta[$idqm]['mes']) * 100),2))."</td>";
                                }
                            }
                            if($kespe == "0") {
                                $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">$ttmes</td>";
                            }
                            if($kespe == "1") {
                                $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">$ttrel</td>";
                            }
                            if($kespe == "2") {
                                $ttperc = number_format(($ttrel / $ttmes) * 100, 2);
                                $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">$ttperc</td>";
                            }
                            $this->vidados .= "</tr>";
                        }
                        $this->vidados .= "<tr colspan=\"".$this->colunas."\"><td colspan=\"".$this->colunas."\"><br/></td></tr>";
                        $this->vidados .= "<tr colspan=\"".$this->colunas."\"><td colspan=\"".$this->colunas."\"><br/></td></tr>";
                        $this->vidados .= "<tr><td class=\"corfd_coltexto\" colspan=\"".$this->colunas."\"><strong>REALIZADO VS ACUMULADO ATÉ A DATA</strong></td></tr>";
                        $colprop = array('Total até a data','Relizadas','%');
                        $dtatu = date('Y-m-d');
                        $compdatas = "SELECT '$dtatu' > '".$this->periodo['datafim']."' as result";
                        $ecomp = $_SESSION['fetch_array']($_SESSION['query']($compdatas)) or die ("erro na query de comparação das datas");
                        $calcdias = "SELECT DATEDIFF('$dtatu','".$this->periodo['dataini']."') as cdias";
                        $edias = $_SESSION['fetch_array']($_SESSION['query']($calcdias)) or die ("erro na query de calculo da quantidade de dias passados");
                        $diasdesc = "SELECT COUNT(*) as result FROM diasdescon WHERE idperiodo='".$this->periodo['idperiodo']."' and data <= '$dtatu'";
                        $ediasdesc = $_SESSION['fetch_array']($_SESSION['query']($diasdesc)) or die (mysql_error());
                        if($this->tipo == "planningctt") {
                            $anof = date('Y');
                            $feriados = getFeriados($anof);
                            foreach($feriados as $fdia) {
                                $compf = "SELECT '$anof-$fdia' BETWEEN '".$this->periodo['dataini']."' AND '$dtatu' as result";
                                $ecompf = $_SESSION['fetch_array']($_SESSION['query']($compf)) or die ("erro na query de comparação das datas com feriados");
                                if($ecompf['result'] >= 1) {
                                    $desc++;
                                }
                                else {
                                }
                            }
                        }
                        else {
                            $dias = ($edias['cdias'] + 1) - $ediasdesc['result'];
                        }
                        $dtinictt = $this->periodo['dataini'];
                        if($this->tipo == "planningctt") {
                            for(;$dl == 0;){
                                $compd = "SELECT '$dtinictt' <= '".date('Y-m-d')."' as compd, adddate('$dtinictt',1) as dataadd";
                                $ecompd = $_SESSION['fetch_array']($_SESSION['query']($compd)) or die ("erro na query de comparação das datas");
                                if($ecompd['compd'] != 1) {
                                    $dl = 1;
                                }
                                else {
                                    $dtinictt = $ecompd['dataadd'];
                                    $vdata = jddayofweek ( cal_to_jd(CAL_GREGORIAN, substr($dtinictt,5,2),substr($dtinictt,8,2), substr($dtinictt,0,4)), 0);
                                    if($vdata == 6 OR $vdata == 0) {
                                        $desc++;
                                    }
                                }
                            }
                            $dias = ($edias['cdias'] + 1) - $desc;
                        }
                        else {
                        }
                        $ttmes = "";
                        $ttrel = "";
                        foreach($colprop as $kprop => $nprop) {
                            $this->vidados .= "<tr><td class=\"corfd_coltexto\"><strong>$nprop</strong></td>";
                            foreach($idsrel as $idac => $nac) {
                                if($kprop == "0") {
                                    if($ecomp['result'] >= 1) {
                                        $ttmes = $ttmes + $this->meta[$idac]['mes'];
                                        $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".$this->meta[$idac]['mes']."</td>";
                                    }
                                    else {
                                        $ttmes = $ttmes + ($dias * $this->meta[$idac]['dia']);
                                        $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".($dias * $this->meta[$idac]['dia'])."</td>";
                                    }
                                }
                                if($kprop == "1") {
                                    $ttrel = $ttrel + $qtdeidrel[$idac];
                                    $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".$qtdeidrel[$idac]."</td>";
                                }
                                if($kprop == "2") {
                                    $ttperc = number_format(($ttrel / $ttmes) * 100, 2);
                                    $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">".str_replace(".",",",number_format((($qtdeidrel[$idac]/($dias * $this->meta[$idac]['dia'])) * 100),2))."</td>";
                                }
                            }
                            if($kprop == "0") {
                                $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">$ttmes</td>";
                            }
                            if($kprop == "1") {
                                $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">$ttrel</td>";
                            }
                            if($kprop == "2") {
                                $ttperc = number_format(($ttrel / $ttmes) * 100, 2);
                                $this->vidados .= "<td bgcolor=\"#FFFFFF\" align=\"center\">$ttperc</td>";
                            }
                            $this->vidados .= "</tr>";
                        }
                    }
                    else {
                    }
                $this->vidados .= "</table>";
            $this->vidados .= "</div>";
        }
        if($this->tipo == "grafico") {
            foreach($this->topten as $perc => $rels) {
                $qtde[] = $rels['qtde'];
                $meta[] = $rels['meta'];
                $apres[] = $perc;
            }
            foreach($this->diaspass as $dia) {
                if($this->tipooper == "d") {
                    $dias[] = substr($dia,8,2)."/".substr($dia,5,2);
                }
                else {
                    $quebra = explode(";",$dia);
                    $dias[] = substr($quebra[0],8,2)."/".substr($quebra[0],5,2)."-".substr($quebra[1],8,2)."/".substr($quebra[1],5,2);
                }
            }
            ?>
            <script type="text/javascript">
                var chart;
                $(document).ready(function() {
                chart = new Highcharts.Chart({
                  chart: {
                     renderTo: 'grafmeta',
                     defaultSeriesType: 'line',
                     marginTop: 100,
                     marginBottom: 75
                  },
                  title: {
                     text: 'META X PRODUÇÃO'
                  },
                  xAxis: {
                     categories: ['<?php echo implode("','",$dias);?>'],
                     labels: {
                        y: 40
                    }
                  },
                  yAxis: {
                     title: {
                        text: 'Quantidade'
                     },
                     labels: {
                         enabled: false
                     }
                  },
                  tooltip: {
                     formatter: function() {
                           return '<b>'+ this.series.name +'</b><br/>'+
                           this.x +': '+ this.y;
                     }
                  },
                  legend: {
                     layout: 'horizontal',
                     align: 'center',
                     verticalAlign: 'top',
                     y: 30,
                     borderWidth: 0
                  },
                  plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            color: '#000'
                        }
                    }
                  },
                  series: [{
                     name: 'META',
                     data: [<?php echo implode(",",$this->dados['acumulado']);?>]
                  }, {
                     name: 'PRODUCAO',
                     data: [<?php echo implode(",",$this->dados['produzido']);?>],
                     dataLabels: {
                         y:20
                     }
                  },
                  {
                     type: 'column',
                     name: 'DEFICIT',
                     data:[<?php echo implode(",",$this->dados['diferenca']);?>],
                     dataLabels: {
                         color: 'red',
                         y:30
                     }
                   }]
               })
               });
            </script>
            <script type="text/javascript">
                var chart;
                $(document).ready(function() {
                   chart = new Highcharts.Chart({
                      chart: {
                         renderTo: 'topten',
                         defaultSeriesType: 'column',
                         height: 700,
                         margintop: 100,
                         marginBottom: 400
                      },
                      title: {
                         text: 'TOP 10 - DEFICIT META'
                      },
                      xAxis: {
                         categories: ['<?php echo implode("','",$apres);?>'],
                         labels: {
                            rotation: 270,
                            align: 'right',
                            style: {
                                font: 'normal 13px Verdana, sans-serif'
                            }
                         }
                      },
                      yAxis: {
                         min: 0,
                         title: {
                            text: 'QUANTIDADE'
                         },
                         stackLabels: {
                            enabled: true,
                            style: {
                               fontWeight: 'bold',
                               color: '#000000'
                            }
                         }
                      },
                      legend: {
                         layout: 'horizontal',
                         align: 'center',
                         verticalAlign: 'top',
                         y: 30,
                         borderWidth: 0
                      },
                      tooltip: {
                         formatter: function() {
                            return '<b>'+ this.x +'</b><br/>'+ Highcharts.numberFormat(this.y, 1);
                         }
                      },
                      plotOptions: {
                         column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                color: "#000000"
                            }
                         }
                      },
                      series: [{
                         name: 'DEFICIT',
                         data: [<?php echo implode(",",$meta);?>]
                         },
                         {
                         name: 'QTDE',
                         data:[<?php echo implode(",",$qtde);?>]
                        }]
                   });


                });
            </script>
            <div id="grafmeta"></div><br/><br/><br/>
            <div id="topten"></div>
            <?php
        }
    }

    function visudados() {
        return $this->vidados;
    }
}


?>
