<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once $rais.'/monitoria_supervisao/classes/class.formulas.php';
include_once ($rais.'/monitoria_supervisao/getid3/getid3/getid3.php');

class Planilha {
    public $iduser;
    public $perfiluser;
    public $tabuser;
    public $pagina;
    public $idfila;
    public $semdados;
    public $idupload;
    public $idrel;
    public $idoperador;
    public $idauditor;
    public $nomerel;
    public $tipomoni;
    public $tipostatus;
    public $idplan;
    public $sqltab;
    public $idmoni;
    public $idcalibragem;
    public $filtro;
    public $trocafiltro;
    public $caminho;
    public $camid3;
    public $visu;
    public $visuweb;
    public $idsmoni;
    public $idsuper;
    public $idoper;
    public $data;
    public $horaini;
    public $horafim;
    public $tmpaudio;
    public $dados;
    public $plansdepende;
    public $edita;
    public $alteramoni;
    public $jsdefin;

    function DadosPlanMoni($pagina) {
        $url = $_SERVER['HTTP_REFERER'];
        if($pagina == "CALIBRAGEM") {

        }
        else {
            if(isset($_POST['idfila'])) {
              $this->idfila = $_POST['idfila'];
            }
            if(isset($_GET['idfila'])) {
              $this->idfila = mysql_real_escape_string($_GET['idfila']);
            }
            if(isset($_POST['tipomoni'])) {
              $this->tipomoni = $_POST['tipomoni'];
            }
            if(isset($_GET['tipomoni'])) {
              $this->tipomoni = mysql_real_escape_string($_GET['tipomoni']);
            }
            $this->semdados = $_POST['semdados'];
        }
        if($this->tipomoni == "G") {
          $this->sqltab = "fila_grava";
        }
        if($this->tipomoni == "O") {
          $this->sqltab = "fila_oper";
        }

        if($this->sqltab == "fila_oper") {
          $seldados = "SELECT * FROM $this->sqltab fo
                      INNER JOIN operador o ON o.idoperador = fo.idoperador
                      INNER JOIN super_oper s ON s.idsuper_oper = fo.idsuper_oper
                      INNER JOIN conf_rel cr ON cr.idrel_filtros = fo.idrel_filtros
                      INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                      WHERE fo.id$this->sqltab='$this->idfila'";
        }
        if($this->sqltab == "fila_grava") {
            if($this->semdados == "S") {
                $seldados = "SELECT *, fg.camaudio as caminho,aud.operador as auditor,o.idoperador as idoper,o.operador as oper FROM $this->sqltab fg
                            INNER JOIN operador o ON o.idoperador = fg.idoperador
                            LEFT JOIN operador aud ON aud.idoperador = fg.idauditor
                            INNER JOIN super_oper s ON s.idsuper_oper = fg.idsuper_oper
                            INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros
                            INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                            WHERE fg.id$this->sqltab='$this->idfila'";
            }
            else {
                if($_SESSION['selbanco'] == "monitoria_itau") {
                    $seldados = "SELECT *, fg.camaudio as caminho,aud.operador as auditor,o.idoperador as idoper,o.operador as oper FROM $this->sqltab fg
                                INNER JOIN operador o ON o.idoperador = fg.idoperador
                                LEFT JOIN operador aud ON aud.idoperador = fg.idauditor
                                INNER JOIN super_oper s ON s.idsuper_oper = fg.idsuper_oper
                                INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                WHERE fg.id$this->sqltab='$this->idfila'";
                }
                else {
                    $seldados = "SELECT *, fg.camaudio as caminho, aud.operador as auditor,o.idoperador as idoper,o.operador as oper FROM $this->sqltab fg
                                INNER JOIN operador o ON o.idoperador = fg.idoperador
                                LEFT JOIN operador aud ON aud.idoperador = fg.idauditor
                                INNER JOIN super_oper s ON s.idsuper_oper = fg.idsuper_oper
                                INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                WHERE fg.id$this->sqltab='$this->idfila'";
                }
            }
        }
        $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta dos dados da fila");
        $this->idupload = $eseldados['idupload'];
        $this->idoperador = $eseldados['idoper'];
        $this->idauditor = $eseldados['idauditor'];
        $this->idrel = $eseldados['idrel_filtros'];
        $selparam = "SELECT pm.tiposervidor,pm.endereco,pm.loginftp,pm.senhaftp,pm.camaudio FROM param_moni pm INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni WHERE cr.idrel_filtros='$this->idrel'";
        $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
        if($this->tipomoni == "O") {
            if($pagina == "CALIBRAGEM") {
                $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                $selcalib = "SELECT * FROM agcalibragem ag
                            INNER JOIN monitoria m ON m.idmonitoria = ag.idmonitoria
                            WHERE ag.idagcalibragem='$this->idcalibragem' AND participantes='".$_SESSION['participante']."'";
                $eselcalib = $_SESSION['fetch_array']($_SESSION['query']($selcalib)) or die ("erro na query de consulta da calibragem");
                $arquivo = str_replace($rais, "", $eselcalib['gravacao']);
                if($eselparam['tiposervidor'] == "FTP") {
                    $ftp = $eselparam['endereco'];
                    $user = $eselparam['loginftp'];
                    $senha = base64_decode($eselparam['senhaftp']);
                    $server = $eselparam['endereco'];
                    $arquivo = str_replace($eselparam['camaudio'],"",$arquivo);
                    $this->caminho = "ftp://$user:$senha@$server/".$_SESSION['nomecli']."$arquivo";
                    //$ftp = ftp_connect($server);
                    $ftplogin = ftp_login($ftp, $user, $senha);
                    //$ftpdir = ftp_chdir($ftp, $eseldados['camaudio']);
                    $tmp = ftp_get($ftp, $rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo']."", $eseldados['camaudio']."/".$eseldados['arquivo'], FTP_BINARY);
                    $this->camid3 = "$rais/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo'];
                    $this->caminho = "/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo'];
                }
                else {
                    $this->caminho = $arquivo;
                }
                $this->tmpaudio = $eselcalib['tmpaudio'];
            }
            else {
                $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                $arquivo = str_replace($rais, "", $eseldados['camaudio']."/".$eseldados['arquivo']);
                if($eselparam['tiposervidor'] == "FTP") {
                    $user = $eseldados['loginftp'];
                    $senha = base64_decode($eselparam['senhaftp']);
                    $server = $eselparam['endereco'];
                    $arquivo = str_replace($eselparam['camaudio'],"",$arquivo);
                    $this->caminho = "ftp://$user:$senha@$server/".$_SESSION['nomecli']."$arquivo";
                }
                else {
                    $this->caminho = $arquivo;
                }
            }
        }
        if($this->tipomoni == "G") {
            if($pagina == "CALIBRAGEM") {
                $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                $selcalib = "SELECT * FROM agcalibragem ag
                            INNER JOIN monitoria m ON m.idmonitoria = ag.idmonitoria WHERE ag.idagcalibragem='$this->idcalibragem' AND participantes='".$_SESSION['participante']."'";
                $eselcalib = $_SESSION['fetch_array']($_SESSION['query']($selcalib)) or die ("erro na query de consulta da calibragem");
                $arquivo = str_replace($rais, "", $eselcalib['gravacao']);
                if($eselparam['tiposervidor'] == "FTP") {
                    $ftp = ftp_connect($eselparam['endereco']);
                    $user = $eselparam['loginftp'];
                    $senha = base64_decode($eselparam['senhaftp']);
                    $server = $eselparam['endereco'];
                    $arquivo = str_replace($eselparam['caminho'],"",$arquivo);
                    $this->caminho = "ftp://$user:$senha@$server/".$_SESSION['nomecli']."$arquivo";
                    //$ftp = ftp_connect($server);
                    $ftplogin = ftp_login($ftp, $user, $senha);
                    //$ftpdir = ftp_chdir($ftp, $eseldados['caminho']);
                    if(file_exists($eseldados['caminho']."/".$eseldados['arquivo'])) {

                    }
                    else {
                        $tmp = ftp_get($ftp, $rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo']."", $eseldados['caminho']."/".$eseldados['arquivo'], FTP_BINARY);
                    }
                    $this->camid3 = "$rais/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo'];
                    $this->caminho = "/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo'];
                }
                else {
                    $this->caminho = $arquivo;
                }
                $this->tmpaudio = $eselcalib['tmpaudio'];
            }
            if($pagina == "MONITOR") {
                $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                if($eseldados['tipoimp'] == "I") {
                    $arquivo = str_replace($rais,"",$eseldados['arquivo']);
                }
                else {
                    $arquivo = str_replace($rais,"",$eseldados['caminho'])."/".$eseldados['arquivo'];
                }
                if($eselparam['tiposervidor'] == "FTP") {
                    $ftp = ftp_connect($eselparam['endereco']);
                    $user = $eselparam['loginftp'];
                    $senha = base64_decode($eselparam['senhaftp']);
                    $server = $eselparam['endereco'];
                    $arquivo = str_replace($eselparam['caminho'],"",$arquivo);
                    $this->caminho = "ftp://$user:$senha@$server/".$_SESSION['nomecli']."$arquivo";
                    //$ftp = ftp_connect($server);
                    $ftplogin = ftp_login($ftp, $user, $senha);
                    //$ftpdir = ftp_chdir($ftp, $eseldados['camaudio']);
                    $tmp = ftp_get($ftp, $rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo']."", $eseldados['caminho']."/".$eseldados['arquivo'], FTP_BINARY);
                    $this->camid3 = "$rais/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo'];
                    $this->caminho = "/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo'];
                }
                else {
                    $this->caminho = $arquivo;
                    if($eseldados['tipoimp'] == "I") {
                        $this->camid3 = str_replace("http://","",$eseldados['arquivo']);
                    }
                    else {
                        $this->camid3 = $eseldados['caminho']."/".$eseldados['arquivo'];
                    }
                }
                $getID3 = new getID3;
                $file = $this->camid3;

                set_time_limit(30);

                $FileInfo = $getID3->analyze($file);

                getid3_lib::CopyTagsToComments($FileInfo);

                if($eseldados['tipoimp'] == "I") {
                    if($eseldados['tmpaudio'] == "") {
                        $this->tmpaudio = "00:00:00";
                    }
                    else {
                        $this->tmpaudio = $eseldados['tmpaudio'];
                    }
                }
                else {
                    $this->tmpaudio = $FileInfo['playtime_string'];
                }
                if($eselparam['tiposervidor'] == "FTP") {
                }
                else {
                    //unlink($rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$eseldados['arquivo']);
                }
            }
        }
        if($eseldados['trocafiltro'] == "S") {
            $this->trocafiltro = "S";
        }
        else {
            $this->trocafiltro = "N";
        }

        if(isset($_POST['idplan'])) {
            if(isset($this->idplan)) {
            }
            else {
                $this->idplan = $_POST['idplan'];
            }
        }
        if(isset($_GET['planmoni'])) {
          $this->idplan = mysql_real_escape_string($_GET['planmoni']);
        }
        $sellinha = "SELECT * FROM $this->sqltab
                    INNER JOIN conf_rel ON conf_rel.idrel_filtros = $this->sqltab.idrel_filtros
                    WHERE id$this->sqltab='$this->idfila'";
        $elinha = $_SESSION['fetch_array']($_SESSION['query']($sellinha)) or die ("erro na query de consulta da linha");
        $plans = explode(",",$elinha['idplanilha_moni']);
        if($elinha['idplanilha_moni'] == "") {
          $msg="Não existe planilha relacionada, favor contatar o administrador!!!";
          if($this->alteramoni == "S") {
              header("location:/monitoria_supervisao/alteramonitoria.php?msg=".$msg);
          }
          else {
              header("location:/monitoria_supervisao/inicio.php?menu=sistema&msg=".$msg);
          }
        }
        if(count($plans) == 1) {
        }
        if(count($plans) > 1) {
            if($pagina == "MONITOR") {
              if(!eregi("planmoni",$url) && !eregi("exemoni.php",$url)) {
                  $msg="Favor selecionar uma planilha para iniciar a monitoria!!!";
                  header("location:/monitoria_supervisao/inicio.php?menu=sistema&msg=".$msg);
              }
              else  {
              }
            }
            if($pagina == "CALIBRAGEM") {
            }
        }
        $this->dados = $eseldados;
    }

    function MontaPlan_dados($pagina) {
        ?>
        <link href="/monitoria_supervisao/stylemonitoria.css" rel="stylesheet" type="text/css" />
        <?php
        if($pagina == "MONITOR" OR $_SESSION['user_tabela'] == "monitor") {
            ?>
            <script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
           <?php
        }
        if($pagina == "CALIBRAGEM" && $_SESSION['user_tabela'] != "monitor") {

        }
        ?>
        <script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
        <script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
        <link rel="stylesheet" href="tabs.css" type="text/css" />
        <link rel="stylesheet" href="/monitoria_supervisao/users/styleexp.css" type="text/css" />
        <script type="text/javascript" src="/monitoria_supervisao/audio-player/audio-player.js"></script>
        <link type="text/css" href="/monitoria_supervisao/js/jquery.countdown.css" />
        <script src="/monitoria_supervisao/js/jquery.countdown.js" type="text/javascript"></script>

        <?php

        $i = 1;
        $selplans = "SELECT DISTINCT(vp.idvinc_plan), vp.planvinc, p.descriplanilha, vp.idrel_filtros, p.idtabulacao
                    FROM vinc_plan vp
                    INNER JOIN planilha p ON p.idplanilha = vp.planvinc WHERE vp.idplanilha='$this->idplan' AND vp.ativo='S' GROUP BY p.idplanilha";
        $eselplans = $_SESSION['query']($selplans) or die ("erro na query de consulta do vinculo da planiha");
        while($lselplans = $_SESSION['fetch_array']($eselplans)) {
            $planilhas[$i]["idplan"] = $lselplans['planvinc'];
            $planilhas[$i]["nomeplan"] = $lselplans['descriplanilha'];
            if($lselplans['idrel_filtros'] == "") {
                $selrelvinc = "SELECT idrel_aud FROM conf_rel WHERE idrel_filtros='$this->idrel'";
                $eselrelvinc = $_SESSION['fetch_array']($_SESSION['query']($selrelvinc)) or die ("erro na query de consulta do relacionamento da auditoria");
                if($eselrelvinc['idrel_aud'] != "") {
                    $planilhas[$i]["relfiltro"] = $eselrelvinc['idrel_aud'];
                }
                else {
                    $planilhas[$i]["relfiltro"] = $this->idrel;
                }
            }
            else {
                    $planilhas[$i]["relfiltro"] = $lselplans['idrel_filtros'];
            }
            $planilhas[$i]["idtabulacao"] = explode(",",$enomeplan['idtabulacao']);
            $planilhas[$i]["plancondicional"] = "N";
            $i++;
        }

        $vincperg = "SELECT * FROM planilha p
                    INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                    LEFT JOIN subgrupo s ON s.idsubgrupo = g.idrel AND g.filtro_vinc='S'
                    LEFT JOIN pergunta ps ON ps.idpergunta = s.idpergunta AND g.filtro_vinc='S' OR ps.idpergunta = g.idrel AND g.filtro_vinc='P'
                    WHERE g.ativo='S' AND p.idplanilha='$this->idplan' AND avaliaplan<>'' AND (idrel_origem IS NULL OR idrel_origem='$this->idrel')
                    GROUP BY ps.idresposta ORDER BY g.idgrupo";
        $evincperg = $_SESSION['query']($vincperg) or die ("erro na query de consulta das planilhas vinculadas por pergunta");
        $nvincperg = $_SESSION['num_rows']($evincperg);
        if($nvincperg >= 1) {
            $p = 0;
            while($lvincperg = $_SESSION['fetch_array']($evincperg)) {
                foreach($planilhas as $posi => $campo) {
                    if($campo['idplan'] == $lvincperg['avaliaplan']) {
                        $p++;
                    }
                    else {
                        $p;
                    }
                }
                if($p >= 1) {
                }
                else {
                    $planilhas[$i]["idplan"] = $lvincperg['avaliaplan'];
                    $nomeplan = "SELECT descriplanilha,idtabulacao FROM planilha WHERE idplanilha='".$lvincperg['avaliaplan']."' GROUP BY idplanilha";
                    $enomeplan = $_SESSION['fetch_array']($_SESSION['query']($nomeplan)) or die ("erro na query de consulta do nome da planilha");
                    $planilhas[$i]["nomeplan"] = $enomeplan['descriplanilha'];
                    if($enomeplan['idtabulacao'] == "") {
                        $planilhas[$i]["idtabulacao"] = "";
                    }
                    else {
                        $planilhas[$i]["idtabulacao"] = explode(",",$enomeplan['idtabulacao']);
                    }
                    if($lvincperg['idrel_avaliaplan'] == "") {
                        $selrelvinc = "SELECT idrel_aud FROM conf_rel WHERE idrel_filtros='$this->idrel'";
                        $eselrelvinc = $_SESSION['fetch_array']($_SESSION['query']($selrelvinc)) or die ("erro na query de consulta do relacionamento da auditoria");
                        if($eselrelvinc['idrel_aud'] != "") {
                            $planilhas[$i]["relfiltro"] = $eselrelvinc['idrel_aud'];
                        }
                        else {
                            $planilhas[$i]["relfiltro"] = $this->idrel;
                        }
                    }
                    else {
                        $planilhas[$i]["relfiltro"] = $lvincperg['idrel_avaliaplan'];
                    }
                    $planilhas[$i]["plancondicional"] = "S";
                    $planilhas[$i]["idpergunta"] = $lvincperg['idpergunta'];
                    $planilhas[$i]["idresposta"] = $lvincperg['idresposta'];
                    $i++;
                }
            }
        }

        $selnome = "SELECT DISTINCT(idplanilha), descriplanilha,idtabulacao FROM planilha WHERE idplanilha='$this->idplan' GROUP BY idplanilha";
        $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selnome));
        $planilhas[0]["idplan"] = $this->idplan;
        $planilhas[0]["nomeplan"] = $eselmoni['descriplanilha'];
        if($eselmoni['idtabulacao'] == "") {
            $planilhas[0]["idtabulacao"] = "";
        }
        else {
            $planilhas[0]["idtabulacao"] = explode(",",$eselmoni['idtabulacao']);
        }
        $planilhas[0]["relfiltro"] = $this->idrel;
        $planilhas[0]["plancondicional"] = "N";

        ksort($planilhas);
        $this->plansdepende = $planilhas;
        ?>

        <script type="text/javascript">
            history.forward();
            <?php
            $checkretrata = "select * from fila_grava fg
                            where idfila_grava='$this->idfila'";
            $eretrata = $_SESSION['fetch_array']($_SESSION['query']($checkretrata)) or die ("erro na query de consulta da coluna de retratação");
            if($eretrata['idmonitoriavenda'] != "") {
            ?>
            window.open ("/monitoria_supervisao/monitor/monitoriaref.php?idfila=<?php echo $this->idfila;?>","MONITORIA VENDA",'width=720,height=500,scrollbars=YES,resizable=NO,Menubar=0,Status=0,Toolbar=no');
            <?php
            }
            ?>
        </script>

        <script type="text/javascript">
            $(window).unload(function() {
                return false;
            })
            $(document).ready(function() {
                var faval = false;
                $("textarea").live('keyup',function(){
                    var valor = $(this).val().replace('&','E');
                    $(this).val(valor);
                })
                $("div[id^='grupoaval']").hide();
                $("div[id*='aval_']").click(function() {
                    var aval = $(this).attr("id");
                    var IDaval = aval.substr(5);
                    if(faval == false) {
                        $('#grupoaval'+IDaval).show();
                        faval = true;
                    }
                    else {
                        $('#grupoaval'+IDaval).hide();
                        faval = false;
                    }
                });

                var fgrup = false;
                $("div[id^='subperg_']").hide();
                $("div[id*='grupo_']").click(function() {
                var grup = $(this).attr("id");
                var IDgrup = grup.substr(6,13);
                if(fgrup == false) {
                    $('#subperg_'+IDgrup).show();
                    fgrup = true;
                    }
                    else {
                    $('#subperg_'+IDgrup).hide();
                    fgrup = false;
                    }
                });

                $('#exp').click(function() {
                fgrup = false;
                faval = false;
                $("div[id^='subperg_']").show();
                $("div[id^='grupoaval']").show();
                });

                $('#rec').click(function() {
                    fgrup = true;
                    faval = true;
                    $("div[id^='subperg_']").hide();
                    $("div[id^='grupoaval']").hide();
                });

                var idfila = $('#idfila').val();
                var tipo = $('#tipomoni').val();
                $("select[id*='idrelfiltro']").change(function() {
                    var p = $(this).attr('id');
                    var idplan = p.substr(11,15);
                    $('#idoper'+idplan).html('<option value=\"0\">Carregando...</option>');
                    $.post("reloper.php", {idrel: $(this).val(), idfila: idfila, tipo: tipo},
                    function(valor) {
                        $('#idoper'+idplan).html(valor);
                    })
                });

                $("select[id*='idoper']").change(function() {
                    var p = $(this).attr('id');
                    var idplan = p.substr(6,13);
                    var idop = $(this).val();
                    var idfila = $('#idfila').val();
                    var tipo = $('#tipomoni').val();
                    var idrel = $('#idrelfiltro'+idplan).val();
                    $.post("relsuper.php", { idoper: idop, idfila: idfila, tipo: tipo, idrel: idrel}, function(valor) {
                        if(valor == "") {
                        }
                        else {
                            var informacoes = valor.split("-");
                            $('#idsuper'+idplan).val(informacoes[0]);
                            $('#supervisor'+idplan).val(informacoes[1]);
                        }
                    });
                })
            })
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
            <?php
            foreach($this->plansdepende as $jvinc) {
                if($jvinc['plancondicional'] == "S") {
                    echo "$('#idli".$jvinc['idplan']."').remove();\n";
                }
                else {
                }
            }
            foreach($this->plansdepende as $vinc) {
                if($vinc['plancondicional'] == "S") {
                    ?>
                    $('#idresp<?php echo $vinc['idpergunta'];?>').live('change', function() {
                    var idresp = $(this).val();
                    if(idresp == "<?php echo $vinc['idresposta'];?>") {
                        $('#abas').append('<li id=\"idli<?php echo $vinc['idplan'];?>\"><input type=\"hidden\" name=\"idli<?php echo $vinc['idplan'];?>\" value=\"S\" /><a id=\"linka<?php echo $vinc['idplan'];?>\" href=\"#plan<?php echo $vinc['idplan'];?>\"><strong><span><?php echo $vinc['nomeplan'];?></span></strong></a></li>');
                        <?php
                        foreach($vinc['idtabulacao'] as $tabli) {
                            $ntab = "SELECT nometabulacao FROM tabulacao WHERE idtabulacao='$tabli'";
                            $entab = $_SESSION['fetch_array']($_SESSION['query']($ntab)) or die ("erro na query de consulta do nome da tabulacao");
                            ?>
                            $('#abas').append('<li id=\"idlitab<?php echo $tabli;?>\"><input type=\"hidden\" name=\"idlitab<?php echo $tabli;?>\" /><a id=\"linka<?php echo $vinc['idplan']."_".$tabli;?>\" href=\"#plan<?php echo $vinc['idplan'];?>\"><?php echo $entab['nometabulacao'];?></a></li>');
                            <?php
                        }
                        ?>
                        var inputexist = $('#idsplan<?php echo $vinc['idplan'];?>').find();
                        if(inputexist != true) {
                            $('.idsplan<?php echo $vinc['idplan'];?>').append('<input name=\"idsplan[]\" type=\"hidden\" value=\"<?php echo $vinc['idplan'];?>\" id=\"idsplan<?php echo $vinc['idplan'];?>\" />');
                        }
                        else {
                        }
                    }
                    else {
                        $('#idli<?php echo $vinc['idplan'];?>').remove();
                        <?php
                        foreach($vinc['idtabulacao'] as $rmtab) {
                            ?>
                            $('#idlitab<?php echo $rmtab;?>').remove();
                            <?php
                        }
                        ?>
                        $('#idsplan<?php echo $vinc['idplan'];?>').remove();
                        $('name=idli<?php echo $vinc['idplan'];?>').attr('value','N');
                    }
                    })
                    <?php
                }
                else {
                }
            }
            ?>
            })
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('div.divabas').hide();
                $('div.divabas:first').show();
                $("div[id*='dados']").hide();
                $("div[id*='alertas']").hide();
                $("div[id*='arvtab']").hide();
                $("div[id*='divobs']").hide();
                $("div[id*='divobs']:first").show();
                $("div[id*='dados']:first").show();
                $("div[id*='alertas']:first").show();
                $('#linka a:first').addClass("selected");
                $("a[id*='linka']").live('click', function() {
                    $("[id*='idli']").css("background-color","#333333");
                    $("[id*='linka']").css("color","#FFFFFF");
                    var div = $(this).attr("id");
                    var iddiv = div.substr(5,4);
                    var tab = div.substr(10,2);
                    var len = div.length;
                    $('div.divabas').hide();
                    $("div[id*='divobs']").hide();
                    $("div[id*='arvtab']").hide();
                    $("div[id*='dados']").hide();
                    $("div[id*='alertas']").hide();
                    $('#abas a').removeClass("selected");
                    $('#linka'+iddiv).addClass("selected");
                    if(len == 12) {
                        $("#arvtab"+iddiv+'_'+tab).show();
                        $('#plan'+iddiv).hide();
                        $('#divobs'+iddiv).hide();
                        $('#idlitab'+tab).css("background-color","#C90");
                        $('#linka'+iddiv+'_'+tab).css("color","#000");
                    }
                    else {
                        $("#arvtab"+iddiv+'_'+tab).hide();
                        $('#plan'+iddiv).show();
                        $('#divobs'+iddiv).show();
                        $('#idli'+iddiv).css("background-color","#C90");
                        $('#linka'+iddiv).css("color","#000");
                    }
                    $('#dados'+iddiv).show();
                    $('#alertas'+iddiv).show();
                    return false;
                })
            })
        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#validar").live('click',function() {
                    //var idsfg = $('#idsfg').val().split("#");
                    var lenidsplan = $("[name*='idsplan']").serialize();
                    var arrayids = lenidsplan.split("&");
                    var tamanho = arrayids.length;
                    var idsplan = new Array();
                    var i = 0;
                    for(i = 0; i < tamanho; i++) {
                        var limpa = arrayids[i].replace("%5B%5D","");
                        idsplan[i] = limpa;
                    }
                    var vars = $("#valida").serialize("id");
                    var codini = $('#codini').val();
                    var codfim = $('#codfim').val();
                    var idmoni = $('#idmoni').val();
                    var tmpaudio = $('#tmpaudio').val();
                    if(codini == 0 || codfim == 0) {
                        alert('OS TEMPOS DA MONITORIA PRECISAM ESTAR PREENCHIDOS!!!');
                        return false;
                    }
                    else {
                        //alert(idsfg);
                        vars = decodeURIComponent(vars.replace('/\][()*+-$#&@!?|',""));
                        $.post("/monitoria_supervisao/exemoni.php", {vars: vars, idsplan: idsplan,vfg:'vfg'},function(ret) {
                            var dadosfg = ret.split("*");
                            var retfg = dadosfg[0];
                            var msg = dadosfg[1];
                            var salvar = confirm(msg);
                            if(salvar) {
                                $.blockUI({ message: '<strong>AGUARDE ANALISANDO MONITORIA...</strong>', css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: .5,
                                    color: '#fff'
                                    }
                                })

                                //vars = decodeURIComponent(vars.replace('/\][()*+-$#&@!?|',""));
                                $.post("/monitoria_supervisao/exemoni.php", {vars: vars, idsplan: idsplan,validar:'validar',tmpaudio:tmpaudio}, function(valor) {
                                    var dados = valor.split("*");
                                    var retorno = dados[0];
                                    var msg = dados[1];
                                    if(retorno >= 1) {
                                        $.unblockUI();
                                        $.blockUI({ message: '<strong>'+msg+'</strong>', css: {
                                            border: 'none',
                                            padding: '15px',
                                            backgroundColor: '#000',
                                            '-webkit-border-radius': '10px',
                                            '-moz-border-radius': '10px',
                                            color: '#fff'
                                            }
                                        })
                                        setTimeout($.unblockUI,4000);
                                        return false;
                                    }
                                    else {
                                        var direciona = dados[2];
                                        alert(msg);
                                        window.location = direciona;
                                    }
                                });
                             }
                             else {
                                return false;
                                $.unblockUI();
                             }
                         });
                    }
                });

                $('#voltar').live('click',function() {
                        var tipomoni = $('#tipomoni').val();
                        var idfila = $('#idfila').val();
                        var idplan = $('#idplan').val();
                        var tipoplan = $("#tipoplan").val();

                        $.post("/monitoria_supervisao/exemoni.php", {tipomoni: tipomoni, idfila: idfila,idplan:idplan,voltar:'voltar',tipoplan:tipoplan}, function(valor) {
                            window.location = valor;
                        });
                        return false;
                });

                $("#sair").live('click',function() {
                        var idsplan = $('#idsplan').val();
                        var vars = $("#valida").serialize("id");
                        var codini = $('#codini').val();
                        var codfim = $('#codfim').val();
                        var tipoplan = $('#tipoplan').val();
                        var inimoni = $('#inimoni').val();
                        var fimlig = $('#fimlig').val();
                        var idfila = $('#idfila').val();
                        var idupload = $('#idupload').val();
                        var tipomoni = $('#tipomoni').val();
                        var idplan = $('#idplan').val();
                        var idmoni = $('#idmoni').val();
                        var idcalib = $('#idcalib').val();


                        if(codini == 0 || codfim == 0) {
                            alert('OS TEMPOS DA MONITORIA PRECISAM ESTAR PREENCHIDOS!!!');
                            return false;
                        }
                        else {
                            $.post("/monitoria_supervisao/exemoni.php",{sair:'sair',idplan:idplan,codini:codini,codfim:codfim,tipoplan:tipoplan,inimoni:inimoni,fimlig:fimlig,idfila:idfila,idupload:idupload,tipomoni:tipomoni,idmoni:idmoni,idcalib:idcalib},function(valor) {
                                var msg = valor.split("*");
                                if(msg[0] == 1) {
                                    $.blockUI({ message: '<strong>'+msg[1]+'</strong>', css: {
                                        border: 'none',
                                        padding: '15px',
                                        backgroundColor: '#000',
                                        '-webkit-border-radius': '10px',
                                        '-moz-border-radius': '10px',
                                        color: '#fff'
                                        }
                                    })
                                    setTimeout($.unblockUI,4000);
                                    return false;
                                }
                                else {
                                    window.location = msg[1];
                                }
                            });
                        }
                })

                $('#voltarmoni').live('click',function() {
                    var idplan = $('#idplan').val();
                    var tipoplan = $('#tipoplan').val();
                    var idfila = $('#idfila').val();
                    var idupload = $('#idupload').val();
                    var tipomoni = $('#tipomoni').val();

                    $.post("/monitoria_supervisao/exemoni.php",{voltarmoni:'voltarmoni',idplan:idplan,idfila:idfila,idupload:idupload,tipomoni:tipomoni,tipoplan:tipoplan}, function(valor) {
                        window.location = valor;
                    })
                })

                $('#improdutivo').live('click',function() {
                    var user = $("#user").val();
                    var senha = $("#senha").val();
                    if(user != "" && senha != "") {
                         var salvar = confirm("VOCÊ DESEJA REALMENTE SALVAR ESTA MONITORIA COMO IMPRODUTIVA?");
                         if(salvar) {
                             <?php
                             $selcontest = "SELECT COUNT(*) as r FROM contestreg WHERE idfila='$this->idfila'";
                             $eselcontest = $_SESSION['fetch_array']($_SESSION['query']($selcontest)) or die (mysql_error());
                             if($eselcontest['r'] >= 1) {
                                  echo "$.post('/monitoria_supervisao/monitor/checksuper.php',{user:user,senha:senha},function(ret) {\n";
                                  echo "if(ret == 0) {\n";
                                  echo "alert('Dados do Supervisor: Senha ou Login inválido');\n";
                                  echo "return false";
                                  echo "}\n";
                                  echo "else {\n";
                             }
                             ?>
                             $.blockUI({ message: '<strong>AGUARDE ANALISANDO MONITORIA...</strong>', css: {
                                 border: 'none',
                                 padding: '15px',
                                 backgroundColor: '#000',
                                 '-webkit-border-radius': '10px',
                                 '-moz-border-radius': '10px',
                                 opacity: .5,
                                 color: '#fff'
                                 }
                             })
                             var idplan = $('#idplan').val();
                             var tipoplan = $('#tipoplan').val();
                             var idfila = $('#idfila').val();
                             var idupload = $('#idupload').val();
                             var tipomoni = $('#tipomoni').val();
                             var idmotivo = $('#idmotivo').val();
                             var idmoni = $('#idmoni').val();
                             var inimoni = $('#inimoni').val();
                             var tmpaudio = $('#tmpaudio').val();
                             var vars = $("#valida").serialize("id");
                             vars = decodeURIComponent(vars.replace('/\][()*+-$#&@!?|',""));

                             $.post("/monitoria_supervisao/exemoni.php",{vars:vars,inimoni:inimoni,idmoni:idmoni,idmotivo:idmotivo,improdutivo:'improdutivo',idplan:idplan,idfila:idfila,idupload:idupload,tipomoni:tipomoni,tipoplan:tipoplan,tmpaudio:tmpaudio}, function(valor) {
                                 var dados = valor.split("*");
                                 var msg = dados[1];
                                 var retorno = dados[0];
                                 if(retorno >= 1) {
                                     $.blockUI({ message: '<strong>'+msg+'</strong>', css: {
                                         border: 'none',
                                         padding: '15px',
                                         backgroundColor: '#000',
                                         '-webkit-border-radius': '10px',
                                         '-moz-border-radius': '10px',
                                         color: '#fff'
                                         }
                                     })
                                     setTimeout($.unblockUI,4000);
                                     return false;
                                 }
                                 else {
                                     var direciona = dados[2];
                                     alert(msg);
                                     window.location = direciona;
                                 }
                             });
                             <?php
                             if($eselcontest['r'] >= 1) {
                                  echo "}\n";
                                  echo "});\n";
                             }
                             ?>
                         }
                    }
                    else {
                         alert("Os dados do supervisor não podem estar vazios");
                         return false;
                    }
               });

                $("select[id^='idresp']").change(function() {
                    var id = $(this).attr("id");
                    var idperg = id.substr(6,6);
                    $.post("/monitoria_supervisao/af.php", {idresp:$(this).val(), idperg: idperg}, function(valor) {
                        var texto = valor;
                        var separa = texto.split(".");
                        var abre = separa[1];
                        var idpergn = separa[0];
                        if(abre == "F") {
                            $('#trdescri'+idpergn).hide();
                        }
                        if(abre == "A") {
                            $('#trdescri'+idpergn).show();
                        }
                        if(abre == "D") {
                            alert("Você não pode selecionar duas respostas conflitantes!!!");
                        }
                    })
                });

                $('#fimlig').mask('99:99:99');
                $('#buttonfim').live('click',function() {
                    var inimoni = $('#visinimoni').val();
                    var codfim = $('#codfim').val();
                    if(codfim == 0) {
                        if(inimoni == "") {
                            alert('É necessário iniciar a monitoria para finalizar a ligação!!!');
                        }
                        else {
                            var data = new Date();
                            var hora = data.getHours();
                            var min = data.getMinutes();
                            var sec = data.getSeconds();
                            var hora = hora+':'+min+':'+sec;
                            $('#codfim').remove();
                            $('.fimlig').append('<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" name=\"fimlig\" id=\"fimlig\" title=\"Fim Ligação\" type=\"hidden\" value=\"'+hora+'\" />');
                            $('.fimlig').append('<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"codfim\" id=\"codfim\" type=\"hidden\" value=\"1\" />');
                            $('#visfimlig').attr('value',hora);
                            $('#codfim').attr('value','1');
                        }
                    }
                    else {
                    }
                });

                $('#btini').live('click',function() {
                    var codini = $('#codini').val();
                    if(codini == 0) {
                        var data = new Date();
                        var hora = data.getHours();
                        var min = data.getMinutes();
                        var sec = data.getSeconds();
                        var hora = hora+':'+min+':'+sec;
                        $('#codini').remove();
                        $('.inimoni').append('<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"inimoni\" id=\"inimoni\" title=\"Inicio Monitoria\" type=\"hidden\" value=\"'+hora+'\" />');
                        $('.inimoni').append('<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"codini\" id=\"codini\" type=\"hidden\" value=\"1\" />');
                        $('#visinimoni').attr('value',hora);
                        $('#codini').attr('value','1');
                    }
                    else {
                    }
                });
            })
        </script>

        <?php
        if($pagina == "MONITOR") {
            $hora = date('H:i:s');
            $atustatus = "UPDATE monitor SET status='MONITORANDO', hstatus='$hora' WHERE idmonitor='".$_SESSION['usuarioID']."'";
            $eatustatus = $_SESSION['query']($atustatus) or die ("erro na query para atualização do status do monitor");
            //$monitorando = "UPDATE $this->sqltab SET monitorando='1', idmonitor='".$_SESSION['usuarioID']."' WHERE id$this->sqltab='".$this->idfila."'";
            //$emoitorando = $_SESSION['query']($monitorando) or die ("erro na query de atualização da fila");
        }
        if($pagina == "CALIBRAGEM") {
        }
        $selfila = "SELECT * FROM $this->sqltab WHERE id$this->sqltab='$this->idfila'";
        $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die ("erro na query que seleciona os dados da fila");
        $selplan = "SELECT * FROM planilha WHERE idplanilha='$this->idplan'";
        $eplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die (mysql_error());
    }

    function MontaCabecalho($pagina) {
        if($pagina == "MONITOR" OR $pagina == "CALIBRAGEM") {
        ?>
        <form id="valida">
        <div id="cabecalho">
        <?php
        foreach ($this->plansdepende as $val) {
                $fgs = "SELECT * FROM pergunta WHERE ativo='S' AND ativo_resp='S' AND (avalia='FG' OR avalia='FGM')";
                $efgs = $_SESSION['query']($fgs) or die (mysql_error());
                $nfgs = $_SESSION['num_rows']($efgs);
                if($nfgs >= 1) {
                    while($lfgs = $_SESSION['fetch_array']($efgs)) {
                        $idsfg[$lfgs['idresposta']] = $lfgs['descripergunta'];
                    }
                }

                $this->visu = array();
                $selcampos = "SELECT cp.visumoni, co.nomecoluna,posicao FROM rel_filtros rf
                                INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                INNER JOIN camposparam cp ON cp.idparam_moni = pm.idparam_moni
                                INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                                WHERE rf.idrel_filtros='".$val['relfiltro']."' AND cp.ativo='S' ORDER BY posicao";
                $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta aos campos do relacionamento");
                while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
                    if($lselcampos['visumoni'] == "S") {
                        if($val['plancondicional'] == "S" && $_SESSION['selbanco'] == "monitoria_itau" && $lselcampos['operador']) {
                                $this->visu[$lselcampos['posicao']] = "auditor";
                        }
                        else {
                                $this->visu[$lselcampos['posicao']] = $lselcampos['nomecoluna'];
                        }
                    }
                }
                ?>
	        <div style="width:98%; float: left; background-color: #CCCCCC; text-align: center; padding: 10px" id="dados<?php echo $val['idplan'];?>">
                <?php
	        if(!isset($_SESSION['horaini'])) {
	            $_SESSION['horaini'] = date('H:i:s');
	        }
	        if($this->tipomoni == "O") {
                    ?>
	            <table width="100%" align="center">
	                <tr>
	                    <td width="124" class="corfd_coltexto" align="center"><strong>INICIO LIGAÇÃO</strong></td>
	                    <td width="90" bgcolor="#FFFFFF" align="center"><input type="hidden" value="<?php echo $this->idplan;?>" name="idplan" id="idplan" /><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="inilig" title="Inicio Ligação" type="text" value="<?php echo $_SESSION['horaini'];?>" /></td>
	                    <td width="131" class="corfd_coltexto" align="center"><strong>INICIO MONITORIA</strong><?php if(isset($_GET['idmoni'])) { } else {?><input type="button" name="btini" id="btini" value="..." /><?php }?></td>
	                    <td width="90" bgcolor="#FFFFFF" class="inimoni" align="center"><?php if(isset($_SESSION['inimoni'])) { echo "<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" name=\"inimoni\" id=\"inimoni\" title=\"Inicio Monitoria\" type=\"hidden\" value=\"".$_SESSION['inimoni']."\" />"; } else { }?><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="visinimoni" id="visinimoni" title="Inicio Monitoria" type="text" value="<?php if(isset($_SESSION['inimoni'])) { echo $_SESSION['inimoni']; } else { }?>" /><?php if(isset($_SESSION['codini'])) { echo "<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"codini\" id=\"codini\" type=\"hidden\" value=\"".$_SESSION['codini']."\" />"; } else { echo "<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"codini\" id=\"codini\" type=\"hidden\" value=\"0\" />"; } ?></td>
	                    <td width="124" class="corfd_coltexto" align="center"><strong>FIM LIGAÇÃO</strong><?php if(isset($_GET['idmoni'])) {} else {?><input type="button" name="buttonfim" id="buttonfim" value="..." /><?php }?></td>
	                    <td width="90" bgcolor="#FFFFFF" class="fimlig" align="center"><?php if(isset($_SESSION['fimlig'])) { echo "<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" name=\"fimlig\" id=\"fimlig\" title=\"Fim Ligação\" type=\"hidden\" value=\"".$_SESSION['fimlig']."\" />"; } else {}?><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="visfimlig" id="visfimlig" title="Fim Ligação" type="text" value="<?php if(isset($_SESSION['fimlig'])) { echo $_SESSION['fimlig']; } else {}?>" /><?php if(isset($_SESSION['codfim'])) { echo "<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"codfim\" id=\"codfim\" type=\"hidden\" value=\"".$_SESSION['codfim']."\" />"; } else { echo "<input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"codfim\" id=\"codfim\" type=\"hidden\" value=\"0\" />"; } ?></td>
	                    <td width="190" bgcolor="#FFFFFF"><a href="<?php echo $this->caminho;?>">AUDIO</a>
                            <?php
                            $audio = explode("/",$this->caminho);
                            $partaudio = $audio[count($audio) - 1];
                            $extaudio = pathinfo($partaudio,PATHINFO_EXTENSION);
                            if($extaudio == "wav" OR $extaudio == "WAV") {
                                ?>
                                <EMBED SRC="<?php echo $this->caminho;?>" autostart='false' loop="false" WIDTH="300" HEIGHT="40">
                                <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $this->caminho;?>">
                                <param name="quality" value="high">
                                <param name="menu" value="false">
                                <param name="wmode" value="transparent"></object>
                                <?php
                            }
                            else {
                                ?>
                                <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $this->caminho;?>">
                                <param name="quality" value="high">
                                <param name="menu" value="false">
                                <param name="wmode" value="transparent"></object>
                                <?php
                            }
                            ?>
                            </td>
	                </tr>
	            </table><br />
                    <?php
	        }
	        if($this->tipomoni == "G") {
                    $selimp = "SELECT * FROM fila_grava fg"
                            . " INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros"
                            . " INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni"
                            . " WHERE idfila_grava='".$this->idfila."'";
                    $eselimp = $_SESSION['fetch_array']($_SESSION['query']($selimp)) or die (mysql_error());
                    ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#inicro').live('click',function() {
                                var ini = $('#setcro').val();
                                if(ini == "0") {
                                    $('#cronometro').countdown({since: 0, compact: true, format: 'HMS'});
                                    $('#setcro').attr('value','1');
                                }
                                else {
                                    $('#cronometro').countdown('destroy');
                                    $('#setcro').attr('value','0');
                                    $('#cronometro').countdown({since: 0, compact: true, format: 'HMS'});
                                }
                            });

                            $('#pausa').live('click',function() {
                                var pausa = $('#setpausa').val();
                                if(pausa == "0") {
                                    $('#cronometro').countdown('pause');
                                    $('#setpausa').attr('value','1');
                                }
                                else {
                                    $('#cronometro').countdown('resume');
                                    $('#setpausa').attr('value','0');
                                }
                            });
                        })
                    </script>

                    <table width="100%" align="center">
                        <tr>
                            <td class="corfd_coltexto"><strong>CRONOMETRO</strong></td>
                            <td class="corfd_colcampos" align="center">
                                <div id="cronometro" style="font-size:15px"></div>
                            </td>
                            <td class="corfd_colcampos" align="left" colspan="3"><input type="button" name="inicro" id="inicro" value="Inicio" /><input type="hidden" name="setcro" id="setcro" value="0" /><input type="button" name="pausa" id="pausa" value="Pausa" /><input type="hidden" name="setpausa" id="setpausa" value="0" /></td>
                        </tr>
                        <tr>
                            <td width="138" class="corfd_coltexto"><strong>INICIO MONITORIA</strong></td>
                            <td width="126" bgcolor="#FFFFFF" align="center"><input type="hidden" value="<?php echo $this->idplan;?>" name="idplan" id="idplan" /><input type="hidden" value="<?php echo implode("#",$idsfg);?>" name="idsfg" id="idsfg" /><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="inimoni" id="inimoni" title="Inicio Monitoria" type="text" value="<?php echo $_SESSION['horaini'];?>" /></td>
                            <td width="137" class="corfd_coltexto"><strong>TEMPO AUDIO</strong></td>
                            <td width="102" bgcolor="#FFFFFF" align="center"><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="tmpaudio" id="tmpaudio" title="Tamanho Audio" type="text" value="<?php echo $this->tmpaudio; ?>" /></td>
                            <td width="290" class="corfd_colcampos">
                            <?php
                            if(($pagina == "MONITOR" OR $pagina == "CALIBRAGEM") && (eregi("192.168",$_SESSION['ip']) OR eregi("127.0.0.1",$_SESSION['ip']) OR eregi("localhost",$_SESSION['ip']) OR eregi("::1",$_SESSION['ip']))) {
                                if($eselimp['tipoimp'] == "I") {
                                    ?>
                                    <a href="<?php echo $this->caminho;?>" target="_blank">AUDIO</a>
                                    <audio controls="true" style="width: 250px;" id="playaudio" preload="metadata"><source src="<?php echo $this->caminho;?>"></audio>
                                    <?php
                                }
                                else {
                                    ?>
                                    <a href="<?php echo $this->caminho;?>" target="_blank">AUDIO</a>
                                    <audio controls="true" style="width: 250px;" id="playaudio" preload="metadata"><source src="<?php echo $this->caminho;?>"></audio>
                                    <?php
                                }
                            }
                            else {
                                $selfila = "SELECT * FROM $this->sqltab WHERE id$this->sqltab='$this->idfila'";
                                $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die ("erro na query que seleciona os dados da fila");
                                $audio = explode("/",$this->caminho);
                                $partaudio = $audio[count($audio) - 1];
                                $extaudio = pathinfo($partaudio,PATHINFO_EXTENSION);
                                if($extaudio == "wav" OR $extaudio == "WAV") {
                                    ?>
                                    <EMBED SRC="<?php echo $this->caminho;?>" autostart='false' loop="false" WIDTH="300" HEIGHT="40">
                                    <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                    <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                    <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $this->caminho;?>">
                                    <param name="quality" value="high">
                                    <param name="menu" value="false">
                                    <param name="wmode" value="transparent"></object>
                                    <?php
                                }
                                else {
                                    ?>
                                    <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                    <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                    <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $this->caminho;?>">
                                    <param name="quality" value="high">
                                    <param name="menu" value="false">
                                    <param name="wmode" value="transparent"></object>
                                    <?php
                                }
                            }
                            ?>
                            </td>
                        </tr>
                    </table><br />
                    <?php
	        }
                ?>
                 <font color="#FF0000" style="text-align:center"><strong><?php echo mysql_real_escape_string($_GET['msg']);?></strong></font>
                 <table align="center" class="tbdados">
                    <tr>
                        <td width="100%" bgcolor="#FF9900" colspan="4" align="center"><input type="hidden" name="tipoplan" id="tipoplan" value="<?php echo $pagina;?>" />
                        <?php
                        if($pagina == "CALIBRAGEM") {
                            ?>
                            <input type="hidden" name="idcalib" id="idcalib" value="<?php if($_SESSION['user_tabela'] == "monitor") { echo $this->idcalibragem;} else { echo $_GET['idcalib']; }?>" />
                            <input type="hidden" name="idmoni" id="idmoni" value="<?php echo $_GET['idmoni'];?>" />
                            <?php
                        }
                        ?>
                        <input type="hidden" name="idsuper<?php echo $val['idplan'];?>" id="idsuper<?php echo $val['idplan'];?>" value="<?php echo $this->dados['idsuper_oper'];?>" /><strong>DADOS MONITORIA</strong></td>
                        </tr>
                        <?php
                        $qtde = count($this->visu);
                        $linhas = ceil($qtde / 2);
                        if($pagina != "CALIBRAGEM") {
                             $contestimp = "SELECT count(*) as r FROM contestreg WHERE idfila='$this->idfila'";
                              $econtest = $_SESSION['fetch_array']($_SESSION['query']($contestimp)) or die (mysql_error());
                              if($econtest['r'] >= 1) {
                                   $cordados = "#EF9294";
                              }
                              else {
                                   $cordados = "#FFFFFF";
                              }
                        }
                        else {
                             $cordados = "#FFFFFF";
                        }
                        ?>
                        <tr>
                            <td width="150" bgcolor="#999999" align="left"><strong><?php echo strtoupper(str_replace("id_","",$this->filtro));?></strong></td>
                            <td colspan="3" bgcolor="<?php echo $cordados;?>" width="700" align="left"><input style="width:20px;" type="hidden" name="idfila" id="idfila" value="<?php echo $this->idfila;?>" /><input style="width:20px;" type="hidden" name="semdados" id="semdados" value="<?php echo $this->semdados;?>" />
                            <input style="width:20px;" type="hidden" name="idupload" id="idupload" value="<?php echo $this->idupload;?>" /><input style="width:20px;" type="hidden" name="tipomoni" id="tipomoni" value="<?php echo $this->tipomoni;?>" />
                            <?php
                            $selnome = "SELECT * FROM rel_filtros INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$this->filtro
                                        WHERE rel_filtros.idrel_filtros='".$val['relfiltro']."'";
                            $enome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die ("erro na query que obtem o nome do relacionamento");
                            $this->nomerel = $enome['nomefiltro_dados'];
                            if($pagina == "CALIBRAGEM") {
                                ?>
                                <input type="hidden" name="idrelfiltro<?php echo $val['idplan'];?>" id="idrelfiltro<?php echo $val['idplan'];?>" value="<?php echo $val['relfiltro'];?>" /><input style="width:99%; border: 1px solid #FFF; background-color:#FFF; border: 2px" type="text" readonly="readonly" name="nomerel" value="<?php echo nomeapres($val['relfiltro']);?>" />
                                <?php
                            }
                            else {
                                if($this->trocafiltro == "N") {
                                    ?>
                                    <input type="hidden" name="idrelfiltro<?php echo $val['idplan'];?>" id="idrelfiltro<?php echo $val['idplan'];?>" value="<?php echo $val['relfiltro'];?>" /><input style="width:99%; border: 1px solid #FFF; background-color:#FFF;margin: 2px" type="text" readonly="readonly" name="nomerel" value="<?php echo nomeapres($val['relfiltro']);?>" />
                                    <?php
                                }
                                if($this->trocafiltro == "S") {
                                    $f = 0;
                                    ?>
                                    <select name="idrelfiltro<?php echo $val['idplan'];?>" id="idrelfiltro<?php echo $val['idplan'];?>" style="width:99%; border: 1px solid #FFF; background-color:#FFF;border: 2px">
                                        <option value="<?php echo $val['relfiltro'];?>" selected="selected"><?php echo nomeapres($val['relfiltro']);?></option>
                                        <?php
                                        $filtros = "SELECT COUNT(*) as result, nomefiltro_nomes FROM filtro_nomes WHERE trocafiltro='S'";
                                        $efiltros = $_SESSION['query']($filtros) or die ("erro na query de consulta dos filtros");
                                        while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                                        $nfiltros[] = "id_".strtolower($lfiltros['nomefiltro_nomes']);
                                        $f++;
                                        }
                                        if($f >= 1) {
                                            $selfiltros = "SELECT * FROM rel_filtros WHERE idrel_filtros='".$val['relfiltro']."'";
                                            $eselfiltros = $_SESSION['fetch_array']($_SESSION['query']($selfiltros)) or die ("erro na query de consulta dos filtros");

                                            $idfiltros = array();
                                            foreach($nfiltros as $nfiltro) {
                                                $col = $nfiltro;
                                                $idfiltros[] = $eselfiltros[$col];
                                            }

                                            $sql = array_combine($nfiltros, $idfiltros);
                                            $sqlquery = array();
                                            if(count($sql) == 1) {
                                            foreach($sql as $key => $value) {
                                                $sqlquery[] = $key."='".$value."'";
                                            }
                                            }
                                            if(count($sql) > 1) {
                                            foreach($sql as $key => $value) {
                                                $sqlquery[] = $key."='".$value."' AND";
                                            }
                                            }

                                            $sqlquery = implode(",",$sqlquery);

                                            $ultfilt = "SELECT *, MIN(nivel) as nivel FROM filtro_nomes";
                                            $eult = $_SESSION['fetch_array']($_SESSION['query']($ultfilt)) or die ("erro na query para obter o menor filtro");
                                            $ult = "id_".strtolower($eult['nomefiltro_nomes']);
                                            $selrel = "SELECT * FROM rel_filtros WHERE $sqlquery";
                                            $eselrel = $_SESSION['query']($selrel) or die ("erro na query dos filtros");
                                            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                                            $selnome = "SELECT * FROM filtro_dados WHERE idfiltro_dados in (".$lselrel[$ult].")";
                                            $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die ("erro na query para localizar nome do filtro");
                                            if($lselrel['idrel_filtros'] == $val['relfiltro']) {
                                            }
                                            else {
                                                ?>
                                                <option value="<?php echo $lselrel['idrel_filtros'];?>"><?php echo $eselnome['nomefiltro_dados'];?></option>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    </select>
                                    <?php
                                }
                            }
                            ?>
	                </td></tr>
                        <?php
                        for($i = 0; $i < $linhas; $i++) {
                                ?>
                                <tr>
                                    <?php
                                    if(!in_array("operador",$this->visu) && $_SESSION['selbanco'] == "monitoria_pj_itau") { //para parametros de monitoria sem operador
                                        ?>
                                        <input type="hidden" name="idoper<?php echo $val['idplan'];?>" value="<?php echo $this->idoperador;?>" />
                                        <?php
                                    }
                                    for($j = 0; $j < 2; $j++) {
                                        if(current($this->visu) == "") {
                                        }
                                        else {
                                            $valor = current($this->visu);
                                            $seltipodados = "SELECT tipo FROM coluna_oper WHERE nomecoluna='".current($this->visu)."'";
                                            $etipodados = $_SESSION['fetch_array']($_SESSION['query']($seltipodados)) or die ("erro na query da consulta do tipo da coluna");
                                            if($etipodados['tipo'] == "DATE") {
                                                if($pagina == "CALIBRAGEM" && $_SESSION['user_tabela'] == "monitor") {
                                                    $seldt = "SELECT dataini FROM agcalibragem WHERE idagcalibragem='$this->idcalibragem'";
                                                    $eseldt = $_SESSION['fetch_array']($_SESSION['query']($seldt)) or die ("erro na query de consulta da calibragem");
                                                    $data = $eseldt['dataini'];
                                                    $menosdt = "SELECT ADDDATE('$data',-1) as result";
                                                    $emenosdt = $_SESSION['fetch_array']($_SESSION['query']($menosdt)) or die ("erro na query de consulta da data da monitoria");
                                                    $newdata = $emenosdt['result'];
                                                    $check = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($newdata,5,2), substr($newdata,8,2), substr($newdata,0,4)));
                                                    if($check == 0) {
                                                        $menosdt = "SELECT ADDDATE('$newdata',-1) as result";
                                                        $emenosdt = $_SESSION['fetch_array']($_SESSION['query']($menosdt)) or die ("erro na query de consulta da data da monitoria");
                                                        $style = "width: 100%; border: 1px solid #FFF; background-color:#FFF";
                                                        $valcoluna = banco2data($emenosdt['result']);
                                                    }
                                                    else {
                                                        $style = "width: 100%; border: 1px solid #FFF; background-color:#FFF";
                                                        $valcoluna = banco2data($newdata);
                                                    }
                                                }
                                                else {
                                                    $style = "width: 99%; border: 1px solid #FFF; background-color:#FFF;border: 2px";
                                                    $valcoluna = banco2data($this->dados[current($this->visu)]);
                                                }
                                            }
                                            if($etipodados['tipo'] == "TEXT") {
                                                $positext[] = key($this->visu);
                                            }
                                            if($etipodados['tipo'] != "TEXT" && $etipodados['tipo'] != "DATE") {
                                                if(current($this->visu) != "operador" && current($this->visu) != "super_oper" && current($this->visu) != "auditor") {
                                                    $style = "width:99%; border: 1px solid #FFF; background-color:#FFF;border: 2px";
                                                    $valcoluna = ucfirst($this->dados[current($this->visu)]);
                                                }
                                                else {
                                                    $style = "width:99%; border: 1px solid #FFF; background-color:#FFF;border: 2px";
                                                    if(current($this->visu) == "operador") {
                                                        $valcoluna = ucfirst($this->dados["oper"]);
                                                    }
                                                    else {
                                                       if(current($this->visu) == "auditor" && ucfirst($this->dados[current($this->visu)]) == "") {
                                                           $valcoluna = ucfirst($this->dados["oper"]);
                                                       }
                                                       else {
                                                           $valcoluna = ucfirst($this->dados[current($this->visu)]);
                                                       }
                                                    }
                                                }
                                            }
                                            if(current($this->visu) == "super_oper" && $this->semdados == "S") {
                                            }
                                            else {
                                            ?>
                                            <td width="150" bgcolor="#999999" align="left"><strong><?php echo ucfirst(current($this->visu));?></strong></td>
                                            <td width="550" bgcolor="<?php echo $cordados;?>" align="left">
                                            <?php
                                            if(current($this->visu) == "operador") {
                                                    $id = "id=\"".current($this->visu)."\"";
                                                    if($pagina == "CALIBRAGEM") {
                                                        ?>
                                                        <input type="hidden" name="idoper<?php echo $val['idplan'];?>" value="<?php echo $this->idoperador;?>" /><input style="<?php echo $style;?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                        <?php
                                                    }
                                                    else {
                                                        if($this->dados['trocanome'] == "S") {
                                                            ?>
                                                            <select name="idoper<?php echo $val['idplan'];?>" id="idoper<?php echo $val['idplan'];?>" style="<?php echo $style;?>">
                                                                <?php
                                                                if($this->semdados == "S") {
                                                                    $opernl = "SELECT * FROM operador WHERE operador='NAO LOCALIZADO'";
                                                                    $eopernl = $_SESSION['fetch_array']($_SESSION['query']($opernl)) or die ("erro na query de consulta do operador não localizado");
                                                                    ?>
                                                                    <option value="<?php echo $eopernl['idoperador'];?>" selected="selected"><?php echo $eopernl['operador'];?></option>
                                                                    <?php
                                                                }
                                                                else {
                                                                    ?>
                                                                    <option value="<?php echo $this->idoperador;?>" selected="selected"><?php echo $valcoluna;?></option>
                                                                    <?php
                                                                }
                                                                if($this->semdados == "S") {
                                                                    $selfn = "SELECT * FROM filtro_nomes WHERE ativo='S'";
                                                                    $eselfn = $_SESSION['query']($selfn) or die ("erro na query de consulta dos filtros");
                                                                    while($lselfn = $_SESSION['fetch_array']($eselfn)) {
                                                                        $selflista = "SELECT idlistaoperador, COUNT(*) as result FROM listaoperador WHERE idfiltro_nomes='".$lselfn['idfiltro_nomes']."'";
                                                                        $eselflista = $_SESSION['fetch_array']($_SESSION['query']($selflista)) or die (mysql_error());
                                                                        if($eselflista['result'] >= 1) {
                                                                            $filtros[$lselfn['idfiltro_nomes']] = strtolower($lselfn['nomefiltro_nomes']);
                                                                        }
                                                                        else {
                                                                        }
                                                                    }
                                                                    foreach($filtros as $idfiltro => $nomefiltro) {
                                                                        $rel = "SELECT id_$nomefiltro, COUNT(*) as result FROM rel_filtros WHERE idrel_filtros='".$val['relfiltro']."'";
                                                                        $erel = $_SESSION['fetch_array']($_SESSION['query']($rel)) or die ("erro na query de consulta do relacionnamento do filtro");
                                                                        if($erel['result'] >= 1) {
                                                                            $sellista = "SELECT * FROM listaoperador WHERE idfiltro_dados='".$erel['id_'.$nomefiltro]."'";
                                                                            $esellista = $_SESSION['query']($sellista) or die ("erro na query de consulta da lista de operadores");
                                                                            while($lsellista = $_SESSION['fetch_array']($esellista)) {
                                                                                ?>
                                                                                <option value="<?php echo $lsellista['idlistaoperador'];?>"><?php echo $lsellista['operador'];?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        else {
                                                                        }
                                                                    }
                                                                }
                                                                else {
                                                                    $filas = array('G' => 'fila_grava', 'O' => 'fila_oper');
                                                                    $seloper = "SELECT DISTINCT(f.idoperador) FROM ".$filas[$this->tipomoni]." f
                                                                                INNER JOIN operador o ON o.idoperador = f.idoperador INNER JOIN upload u ON u.idupload = f.idupload
                                                                                WHERE f.idupload='".$this->idupload."'";
                                                                    $eseloper = $_SESSION['query']($seloper) or die ("erro na consulta dos operadores");
                                                                    while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                                                                        $selnoper = "SELECT operador FROM operador WHERE idoperador='".$lseloper['idoperador']."'";
                                                                        $eselnoper = $_SESSION['fetch_array']($_SESSION['query']($selnoper)) or die ("erro na query de consulta do operador");
                                                                        if($lseloper['idoperador'] != $this->idoperador) {
                                                                            ?>
                                                                            <option value="<?php echo $lseloper['idoperador'];?>"><?php echo $eselnoper['operador'];?></option>
                                                                            <?php
                                                                        }
                                                                        else {
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            </select>
                                                        <?php
                                                        }
                                                        else {
                                                            ?>
                                                            <input type="hidden" name="idoper<?php echo $val['idplan'];?>" value="<?php echo $this->idoperador;?>" /><input style="<?php echo $style;?>" name="<?php echo current($this->visu);?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                            <?php
                                                        }
                                                    }

                                            }
                                            if(current($this->visu) == "super_oper") {
                                                ?>
                                                <input style="<?php echo $style;?>" name="<?php echo current($this->visu).$val['idplan'];?>" id="<?php echo current($this->visu).$val['idplan'];?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                <?php
                                            }
                                            if(current($this->visu) == "auditor") {
                                                if($this->idauditor == "") {
                                                    ?>
                                                    <input type="hidden" name="idoper<?php echo $val['idplan'];?>" value="<?php echo $this->idoperador;?>" /><input style="<?php echo $style;?>" name="<?php echo current($this->visu);?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                    <?php
                                                }
                                                else {
                                                    ?>
                                                    <input type="hidden" name="idoper<?php echo $val['idplan'];?>" value="<?php echo $this->idauditor;?>" /><input style="<?php echo $style;?>" name="<?php echo current($this->visu);?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                    <?php
                                                }
                                            }
                                            if(current($this->visu) != "operador" && current($this->visu) != "super_oper" && current($this->visu) != "auditor") {
                                                if($this->semdados == "S") {
                                                    if($valcoluna != "") {
                                                        $read = "readonly=\"readonly\"";
                                                    }
                                                    else {
                                                        $read = "";
                                                    }
                                                    ?>
                                                    <input style="<?php echo $style;?>" name="<?php echo current($this->visu);?>" type="text" value="<?php echo $valcoluna;?>" <?php echo $read;?>/>
                                                    <?php
                                                }
                                                else {
                                                    ?>
                                                    <input style="<?php echo $style;?>" name="<?php echo current($this->visu);?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                    <?php
                                                }
                                            }
                                            }
                                            ?>
                                            <?php
                                            next($this->visu);
                                            ?>
                                            </td>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tr>
                            <?php
                            }
                            foreach($positext as $posi) {
                                ?>
                                <tr><td colspan="4" bgcolor="#999999" align="center"><strong><?php echo ucfirst($this->visu[$posi]);?></strong></td></tr>
                                <?php
                                if($this->semdados == "S") {
                                    ?>
                                    <tr><td colspan="4"><textarea style="width:100%; height: 70px" readonly="readonly" name="<?php echo $this->visu[$posi];?>"><?php echo $this->dados[$this->visu[$posi]];?></textarea><td></td></tr>
                                    <?php
                                }
                                else {
                                    ?>
                                    <tr><td colspan="4"><textarea style="width:100%; height: 70px" name="<?php echo $this->visu[$posi];?>"><?php echo $this->dados[$this->visu[$posi]];?></textarea><td></td></tr>
                                    <?php
                                }
                            }
                            ?>
	                </table>
	            </div>
                <?php
	        }
                ?>
            </div>
        <?php
        }
    }

    function MontaPlan_corpomoni($edita,$idplan) {
        ?>
        <div id="tabnav" style="width:1024px">
        <?php
        if($edita == "S") {
        }
        else {
        ?>
        <table style="margin:auto">
            <tr>
                <td style="padding:10px"><input type="button" id="exp" style="border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px;" value="EXPANDIR" /></td>
                <td style="padding:10px"><input type="button" style="border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px;" id="rec" value="RETRAIR" /></td>
            </tr>
        </table>
            <div align="center" id="tabs" style="background-color: #F0F0F6; width:1024px">
                <ul id="abas">
                <?php
                    if($this->plansdepende[0] == "") {
                    }
                    else {
                        foreach($this->plansdepende as $valores) {
                            if($valores['idplan'] == "") {
                            }
                            else {
                            	?>
                                <li id="idli<?php echo $valores['idplan'];?>"><input type="hidden" name="idli<?php echo $valores['idplan'];?>" value="S" /><a id="linka<?php echo $valores['idplan'];?>" href="#plan<?php echo $valores['idplan'];?>"><strong><?php echo $valores['nomeplan'];?></strong></a></li>
                                <?php
                                foreach($valores['idtabulacao'] as $idtab) {
                                    if($valores['plancondicional'] == "S") {
                                    }
                                    else {
                                        if($idtab == "") {
                                        }
                                        else {
                                            $seltab = "SELECT nometabulacao FROM tabulacao WHERE idtabulacao='$idtab' GROUP BY idtabulacao";
                                            $eseltab = $_SESSION['fetch_array']($_SESSION['query']($seltab)) or die ("erro na query de consutla da taulação");
                                        ?>
                                        <li id="idlitab<?php echo $idtab;?>"><input type="hidden" name="idlitab<?php echo $idtab;?>" /><a id="linka<?php echo $valores['idplan']."_".$idtab;?>" href="#plan<?php echo $valores['idplan'];?>"><?php echo $eseltab['nometabulacao'];?></a></li>
                                        <?php
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
         <?php
         }
         $tab = 0;
         foreach($this->plansdepende as $v) {
              if($v['idplan'] == $idplan || $edita == "") {
                $tab++;
            	?>
                <div align="center" id="plan<?php echo $v['idplan'];?>" class="divabas">
                  <table width="1024" align="center">
                        <tr>
                            <td align="center" bgcolor="#000000" colspan="2" style="color:#FFF"><strong>PLANILHA</strong></td>
                        </tr>
                        <tr>
                            <?php
                            if($v['plancondicional'] == "S") {
                                ?>
                                <td bgcolor="#7FB1E3" align="center" width="920" class="idsplan<?php echo $v['idplan'];?>"><strong><?php echo $v['nomeplan'];?></strong><input name="idsmoni[]" type="hidden" value="0" /></td>
                                <?php
                            }
                            else {
                                ?>
                                <td bgcolor="#7FB1E3" align="center" width="920" class="idsplan<?php echo $v['idplan'];?>"><strong><?php echo $v['nomeplan'];?></strong><input name="idsplan[]" type="hidden" value="<?php echo $v['idplan'];?>" id="idsplan<?php echo $v['idplan'];?>" /><input name="idsmoni[]" type="hidden" value="0" /></td>
                                <?php
                            }
                            ?>
                            <td align="center" bgcolor="#7FB1E3" width="100"><strong>Valor</strong></td>
                        </tr>
                  </table>
                  <?php
                  $relaval = "SELECT * FROM rel_aval ra INNER JOIN aval_plan av ON av.idaval_plan = ra.idaval_plan WHERE ra.idplanilha='".$v['idplan']."'";
                  $erelaval = $_SESSION['query']($relaval) or die (mysql_error());
                  while($laval = $_SESSION['fetch_array']($erelaval)) {
                      ?>
                    <div align="center" id="aval_<?php echo $v['idplan']."_".$laval['idaval_plan'];?>">
                        <table width="1024" align="center">
                            <tr>
                                <td bgcolor="#669966" align="center" width="920"><strong><?php echo $laval['nomeaval_plan'];?></strong><input name="idaval<?php echo $v['idplan'];?>[]" type="hidden" value="<?php echo $laval['idaval_plan'];?>" /></td>
                                <td align="cetner" width="100"><input style="width:98px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_aval[]" type="text" value="<?php echo $laval['valor'];?>" /></td>
                            <tr/>
                        </table>
                    </div>
                    <div align="center" id="grupoaval<?php echo $v['idplan']."_".$laval['idaval_plan'];?>">
                    <?php
                     $selgrup = "SELECT g.idgrupo, g.descrigrupo, g.complegrupo, g.valor_grupo, g.idrel FROM planilha p INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                                WHERE p.idplanilha='".$v['idplan']."' AND p.idaval_plan='".$laval['idaval_plan']."' AND p.ativo='S' AND g.ativo='S' GROUP BY g.idgrupo ORDER BY p.posicao"; // faz select dos grupos disponíveis na planilha pelo id da planilha
                      $egrup = $_SESSION['query']($selgrup) or die (mysql_error());
                          while($lgrup = $_SESSION['fetch_array']($egrup)) { // faz um loop dos grupos listados na planilha
                                  ?>
                                  <div align="center" id="grupo_<?php echo $lgrup['idgrupo'];?>">
                                      <table width="1024" align="center">
                                          <tr>
                                              <td bgcolor="#999999" align="center" width="920" title="<?php echo $lgrup['complegrupo'];?>"><strong><?php echo $lgrup['descrigrupo'];?></strong><input name="idgrup<?php echo $v['idplan'];?>[]" type="hidden" value="<?php echo $lgrup['idgrupo'];?>" /></td>
                                              <td align="center" width="100"><input style="width:98px; border: 1px solid #333;  text-align:center" readonly="readonly" name="valor_grup[]" id="valor_grup" type="tex" value="<?php echo $lgrup['valor_grupo'];?>" /></td>
                                          </tr>
                                      </table>
                                  </div>
                                  <?php
                                  $selsub = "SELECT idrel, filtro_vinc FROM grupo g INNER JOIN planilha p ON p.idgrupo = g.idgrupo WHERE p.idplanilha='".$v['idplan']."' AND g.idgrupo='".$lgrup['idgrupo']."' AND g.ativo='S' ORDER BY g.posicao"; // faz outro select do mesmo grupo para criar um loop das perguntas ou subgrupos relacionados
                                  $esub = $_SESSION['query']($selsub) or die (mysql_error());
                                  ?>
                                  <div id="subperg_<?php echo $lgrup['idgrupo'];?>" style="width:1024px;">
                                  <?php
                                          while($lsub = $_SESSION['fetch_array']($esub)) {
                                              ?>
                                              <table width="1024">
                                                  <?php
                                                  if($lgrup['idrel'] == 0) {
                                                  }
                                                  else {
                                                    if($lsub['filtro_vinc'] == 'S') { // após o loop verifica se o grupo selecionado tem o filro de subgrupo
                                                            $nsub = "SELECT idsubgrupo, descrisubgrupo, valor_sub FROM subgrupo WHERE idsubgrupo='".$lsub['idrel']."' AND ativo='S' ORDER BY posicao"; // faz a seleção do subgrupo baseado no id_rel (id de relacionamento do grupo)
                                                            $ensub = $_SESSION['fetch_array']($_SESSION['query']($nsub)) or die (mysql_error());
                                                            ?>
                                                            <tr>
                                                                <td bgcolor="#99CCFF" width="910" align="center"><strong><?php echo $ensub['descrisubgrupo'];?></strong><input name="idsub<?php echo $v['idplan'];?>[]" type="hidden" value="<?php echo $ensub['idsubgrupo'];?>" /></td>
                                                                <td align="center"><input style="width:99px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_sub[]" id="valor_sub" type="text" value="<?php echo $ensub['valor_sub'];?>" /></td>
                                                            </tr>
                                                            <?php
                                                            $cperg = "SELECT COUNT(idpergunta) as result FROM subgrupo WHERE idsubgrupo='".$lsub['idrel']."' AND ativo='S'"; // faz select para identificar a quantidade de perguntas relacionadas ao grupo
                                                            $ecperg = $_SESSION['fetch_array']($_SESSION['query']($cperg)) or die (mysql_error());
                                                            if($ecperg['result'] == 0) { // caso a quantidade seja 0, não insere nada na planilha
                                                            }
                                                            if($ecperg['result'] >= 1) { // caso seja igual ou maior que 1 seja com os parametros
                                                                $selperg = "SELECT * FROM subgrupo WHERE idsubgrupo='".$lsub['idrel']."' AND ativo='S'"; // faz novo select do subgrupo para criar loop das perguntas
                                                                $eperg = $_SESSION['query']($selperg) or die (mysql_error());
                                                                while($lperg = $_SESSION['fetch_array']($eperg)) {
                                                                        if($lperg['idpergunta'] == '0') { // verifica se o campo id_perg está zerado, pois se estiver não existe pergunta relacionada
                                                                        }
                                                                        else {
                                                                            $nperg = "SELECT idpergunta, descripergunta, valor_perg, tipo_resp, complepergunta FROM pergunta WHERE idpergunta='".$lperg['idpergunta']."' AND ativo='S' ORDER BY posicao";
                                                                            $enperg = $_SESSION['fetch_array']($_SESSION['query']($nperg)) or die (mysql_error());
                                                                            ?>
                                                                            <tr>
                                                                                <td bgcolor="#FFCC99" width="920" align="center" title="<?php echo $enperg['complepergunta'];?>"><strong><?php echo $enperg['descripergunta'];?></strong><input name="idperg<?php echo $v['idplan'];?>[]" id="idperg<?php echo $enperg['idpergunta'];?>" type="hidden" value="<?php echo $enperg['idpergunta'];?>" /></td>
                                                                                <td align="center"><input style="width:99px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_perg[]" id="valor_perg" type="text" value="<?php echo $enperg['valor_perg'];?>" /></td>
                                                                            </tr>
                                                                            <tr>
                                                                            <?php
                                                                            if($enperg['tipo_resp'] == "U") {
                                                                                $tipoinput = "";
                                                                            }
                                                                            if($enperg['tipo_resp'] == "M") {
                                                                                $tipoinput = "multiple=\"multiple\" size=\"5\"";
                                                                            }
                                                                            ?>
                                                                            <td align="left">
                                                                            <select style="width:900px; text-align:center" <?php echo $tipoinput;?> name="idresp<?php echo $v['idplan'];?>[]" id="idresp<?php echo $enperg['idpergunta'];?>">
                                                                            <?php
                                                                            $selpadrao = "SELECT count(*) as result FROM pergunta WHERE idpergunta='".$lperg['idpergunta']."' AND ativo='S' AND ativo_resp='S' AND padrao='S'";
                                                                            $eselpadrao = $_SESSION['fetch_array']($_SESSION['query']($selpadrao)) or die (mysql_error());
                                                                            if($eselpadrao['result'] == 0) {
                                                                                ?>
                                                                                <option value="" selected="selected"></option>
                                                                                <?php
                                                                            }
                                                                            $selresp = "SELECT idresposta,idrel_origem FROM pergunta WHERE idpergunta='".$lperg['idpergunta']."' AND ativo='S' AND ativo_resp='S' ORDER BY posicao";
                                                                            $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                            while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                                $nresp = "SELECT idresposta, descriresposta, padrao FROM pergunta WHERE idresposta='".$lresp['idresposta']."' AND ativo='S' AND ativo_resp='S' ORDER BY posicao";
                                                                                $enresp = $_SESSION['fetch_array']($_SESSION['query']($nresp)) or die (mysql_error());
                                                                                if($enresp['padrao'] == "S") {
                                                                                    $padrao = "selected=\"selected\"";
                                                                                }
                                                                                else {
                                                                                    $padrao = "";
                                                                                }
                                                                                if($lresp['idrel_origem'] != "" && $lresp['idrel_origem'] != $this->idrel) {
                                                                                }
                                                                                else {
                                                                                    ?>
                                                                                    <option value="<?php echo $enresp['idresposta'];?>" <?php echo $padrao;?> title="<?php echo $enresp['descriresposta'];?>"><?php echo $enresp['descriresposta'];?></option>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </select></td>
                                                                            <td><input type="hidden" style="width:59px; border: 1px solid #333; text-align:center" name="valor_resp" id="valor_resp" value="" /></td>
                                                                            </tr>
                                                                            <tr id="trdescri<?php echo $enperg['idpergunta'];?>" style="display:none">
                                                                                <td><textarea style="width:900px; background-color:#EAB9B9" id="descri<?php echo $enperg['idpergunta'];?>" name="descri<?php echo $enperg['idpergunta'];?>" rows="3"></textarea></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                }
                                                            }
                                                    }
                                                    if($lsub['filtro_vinc'] == 'P') {
                                                            $cperg = "SELECT COUNT(idrel) as result FROM grupo WHERE idrel='".$lsub['idrel']."' AND ativo='S'";
                                                            $ecperg = $_SESSION['fetch_array']($_SESSION['query']($cperg)) or die (mysql_error());
                                                            if($ecperg['result'] == 0) {
                                                            }
                                                            if($ecperg['result'] >= 1) {
                                                                if($lsub['idrel'] == 0) {
                                                                }
                                                                else {
                                                                    $nperg = "SELECT idpergunta, descripergunta, valor_perg, tipo_resp, complepergunta FROM pergunta WHERE idpergunta='".$lsub['idrel']."' AND ativo='S' ORDER BY posicao";
                                                                    $enperg = $_SESSION['fetch_array']($_SESSION['query']($nperg)) or die (mysql_error());
                                                                    ?>
                                                                    <tr>
                                                                        <td bgcolor="#FFCC99" width="920" align="center" title="<?php echo $enperg['complepergunta'];?>"><strong><?php echo $enperg['descripergunta'];?></strong><input name="idperg<?php echo $v['idplan'];?>[]" id="idperg<?php echo $enperg['idpergunta'];?>" type="hidden" value="<?php echo $enperg['idpergunta'];?>" /></td>
                                                                        <td align="center"><input style="width:99px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_perg[]" id="valor_perg" type="text" value="<?php echo $enperg['valor_perg'];?>" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                    <?php
                                                                    if($enperg['tipo_resp'] == "U") {
                                                                        $tipoinput = "";
                                                                    }
                                                                    if($enperg['tipo_resp'] == "M") {
                                                                        $tipoinput = "multiple=\"multiple\" size=\"5\"";
                                                                    }
                                                                    ?>
                                                                        <td align="left">
                                                                        <select style="width:900px; text-align:center" <?php echo $tipoinput;?> name="idresp<?php echo $v['idplan'];?>[]" id="idresp<?php echo $enperg['idpergunta'];?>">
                                                                        <?php
                                                                        $selpadrao = "SELECT count(*) as result FROM pergunta WHERE idpergunta='".$lsub['idrel']."' AND ativo='S' AND ativo_resp='S' AND padrao='S'";
                                                                        $eselpadrao = $_SESSION['fetch_array']($_SESSION['query']($selpadrao)) or die (mysql_error());
                                                                        if($eselpadrao['result'] == 0) {
                                                                            ?>
                                                                            <option value="" selected="selected"></option>
                                                                            <?php
                                                                        }
                                                                        $selresp = "SELECT idresposta,idrel_origem FROM pergunta WHERE idpergunta='".$lsub['idrel']."' AND ativo='S' AND ativo_resp='S' ORDER BY posicao";
                                                                        $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                        while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                            $nresp = "SELECT idresposta, descriresposta, padrao FROM pergunta WHERE idresposta='".$lresp['idresposta']."' AND ativo='S' AND ativo_resp='S' ORDER BY posicao";
                                                                            $enresp = $_SESSION['fetch_array']($_SESSION['query']($nresp)) or die (mysql_error());
                                                                            if($enresp['padrao'] == "S") {
                                                                                $padrao = "selected=\"selected\"";
                                                                            }
                                                                            else {
                                                                                $padrao = "";
                                                                            }
                                                                            if($lresp['idrel_origem'] != "" && $lresp['idrel_origem'] != $this->idrel) {
                                                                            }
                                                                            else {
                                                                                ?>
                                                                                <option value="<?php echo $enresp['idresposta'];?>" <?php echo $padrao;?> title="<?php echo $enresp['descriresposta'];?>"><?php echo $enresp['descriresposta'];?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                        </select></td>
                                                                        <td><input type="hidden" style="width:59px; border: 1px solid #333; text-align:center" name="valor_resp" id="valor_resp" value="" /></td>
                                                                    </tr>
                                                                    <tr id="trdescri<?php echo $enperg['idpergunta'];?>" width="910" style="display:none">
                                                                        <td><textarea style="width:900px;background-color:#EAB9B9" id="descri<?php echo $enperg['idpergunta'];?>" name="descri<?php echo $enperg['idpergunta'];?>" rows="3"></textarea></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                    }
                                                  }
                                                  ?>
                                                  </table>
                                          <?php
                                          }
                                          ?>
                                  </div>
                          <?php
                          }
                          ?>
                          </div>
                    <?php
                    }
                    ?>
               </div>
                <?php
                $selalerta = "SELECT * FROM alertas WHERE ativo='S'";
                $ealerta = $_SESSION['query']($selalerta) or die(mysql_error());
                $calerta = $_SESSION['num_rows']($ealerta);
                if($calerta >= 1) {
                ?>
                <div id="alertas<?php echo $v['idplan'];?>" style="float:left;margin-top:10px;margin-bottom: 10px; margin-left: 1%; width: 97%; background-color: #F90; padding: 4px">
                    <div style="width: 99%; padding: 2px; background-color: #FFF">
                        <table width="100%" border="0">
                            <tr>
                                <td align="center" colspan="<?php echo $calerta;?>" style="background-color:#FFF"><span style="color: red; font-weight: bold">ALERTAS</span></td>
                            </tr>
                        </table>
                        <table id="tabalertas" style="margin: auto">
                            <tr>
                                <?php
                                    while($lalerta = $_SESSION['fetch_array']($ealerta)) {
                                        ?>
                                        <td style=" font-weight: bold; padding: 5px"><input type="checkbox" name="idalerta<?php echo $v['idplan'];?>" id="idalerta<?php echo $v['idplan'];?>" value="<?php echo $lalerta['idalertas'];?>" /> <?php echo $lalerta['nomealerta'];?> </td>
                                        <?php
                                    }
                                ?>
                            </tr>
                        </table>
                    </div>
                </div>
                <div style=" background-color: #EABBBB; padding: 5px; margin: auto; float: left; width: 97%; margin: 7px; font-weight: bold">VERIFIQUE O CORRETO PREENCHIMENTO DO ALERTA</div>
                <?php
                }
                ?>
               <div id="divobs<?php echo $v['idplan'];?>">
                   <table witdh="1024">
                       <tr>
                           <td>
                               <textarea style="width:1000px; height: 70px; background-color: #FFF" id="obsmoni<?php echo $v['idplan'];?>" name="obsmoni<?php echo $v['idplan'];?>"></textarea>
                           </td>
                       </tr>
                   </table>
               </div>
               <?php
               if($tab <= 1) {
                   if($edita == "S") {
                   }
                   else {
                       ?>
                       <script type="text/javascript">
                           $(document).ready(function() {
                               $("[class*='idresptabprox']").hide();
                               $("[class*='idpergtabprox']").hide();
                               $("[id*='idpergtab']").live('change',function() {
                                   var vperg = $(this).attr("id");
                                   var idresp = $(this).val();
                                   var idperg = vperg.substr(9,4);
                                   var tab = vperg.substr(14,2);
                                   var pergabre = $(".idpergtabprox"+idperg+"_"+idresp).html();
                                   if(pergabre != null) {
                                       $('.idpergtabprox'+idperg+'_'+idresp).show();
                                       $('.idresptabprox_'+idperg+'_'+idresp).show();
                                   }
                                   else {
                                       $("[class*='idpergtabprox"+idperg+"']").hide();
                                       $("[class*='idresptabprox_"+idperg+"']").hide();
                                   }
                               })
                           });
                       </script>
                       <?php
                   }
               }
               else {
               }
               foreach($v['idtabulacao'] as $tab) {
                   if($tab == "") {
                   }
                   else {
                       ?>
                       <div id="arvtab<?php echo $v['idplan']."_".$tab;?>">
                           <table width="930" align="center">
                               <?php
                               $newtab = new ArvoreTab();
                               $selperg= "SELECT t.idperguntatab,pt.descriperguntatab,t.idrespostatab,t.idproxpergunta, (SELECT COUNT(*) as result FROM tabulacao WHERE idproxpergunta=t.idperguntatab) as result FROM tabulacao t
                                           INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                                           WHERE t.idtabulacao='$tab' GROUP BY t.idperguntatab ORDER BY t.posicao";
                               $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta da pergunta na tabulação");
                               while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                                   $i = 0;
                                   $idsprox = array();
                                   $selprox = "SELECT t.idproxpergunta, t.idperguntatab, t.idrespostatab FROM tabulacao t
                                                       WHERE t.idtabulacao='$tab' AND t.idperguntatab='".$lselperg['idperguntatab']."' AND idproxpergunta<>'' GROUP BY t.idproxpergunta";
                                   $eselprox = $_SESSION['query']($selprox) or die ("erro na query de consulta da proxima pergunta");
                                   $nprox = $_SESSION['num_rows']($eselprox);
                                   if($nprox >= 1) {
                                       while($lselprox = $_SESSION['fetch_array']($eselprox)) {
                                           $idsprox[$i]['idproxpergunta'] = $lselprox['idproxpergunta'];
                                           $idsprox[$i]['idperguntatab'] = $lselprox['idperguntatab'];
                                           $idsprox[$i]['idrespostatab'] = $lselprox['idrespostatab'];
                                           $i++;
                                       }
                                   }
                                   else {
                                   }
                                   if(in_array($lselperg['idperguntatab'],$newtab->idsproxperg)) {
                                   }
                                   else {
                                       if($lselperg['result'] >= 1) {
                                       }
                                       else {
                                           ?>
                                         <tr class="idpergtab<?php echo $lselperg['idperguntatab'];?>">
                                           <td style="text-align:left; background-color: #6699CC"><strong><?php echo $lselperg['descriperguntatab'];?></strong></td>
                                         </tr>
                                         <tr class="idresptab_<?php echo $lselperg['idperguntatab'];?>">
                                           <td>
                                               <select style="width:930px" name="idpergtab<?php echo $lselperg['idperguntatab']."_".$tab;?>" id="idpergtab<?php echo $lselperg['idperguntatab']."_".$tab;?>">
                                                   <option value=""></option>
                                                   <?php
                                                   $selresp = "SELECT t.idrespostatab, rt.descrirespostatab FROM tabulacao t INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab WHERE t.idperguntatab='".$lselperg['idperguntatab']."' AND idtabulacao='$tab'";
                                                   $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
                                                   while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                                   ?>
                                                       <option value="<?php echo $lselresp['idrespostatab'];?>"><?php echo $lselresp['descrirespostatab'];?></option>
                                                   <?php
                                                   }
                                                   ?>
                                               </select>
                                           </td>
                                         </tr>
                                         <?php
                                       }
                                         if($idsprox[0] != "") {
                                             foreach($idsprox as $prox) {
                                                 $c = 0;
                                                 $newtab->continua = 1;
                                                 $newtab->idproxperg = $prox['idproxpergunta'];
                                                 $newtab->idresp = $prox['idrespostatab'];
                                                 $newtab->idperg = $prox['idperguntatab'];
                                                 while($newtab->continua > $c) {
                                                    $newtab->arvore_moni('','',$tab, $newtab->idperg, $newtab->idproxperg, $newtab->idresp,'','');
                                                 }
                                             }
                                         }
                                   }
                               }
                               ?>
                           </table>
                       </div>
                       <?php
                   }
               }
            }
          }
          ?>
        </div>
        <?php
    }

     function DadosPlanVisualiza() {
         $i = 0;
        $selfiltro = "SELECT nomefiltro_nomes, MIN(nivel) FROM filtro_nomes";
        $eselfiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die ("erro na query de consulta dos filtros");
        $this->filtro = "id_".strtolower($eselfiltro['nomefiltro_nomes']);
        if($this->pagina == "CALIBRAGEM") {
            $tabmoni = "monitoriacalib";
            $tab = "monicalibtabulacao";
        }
        else {
            $this->idmoni = $_GET['idmoni'];
            $tabmoni = "monitoria";
            $tab = "monitabulacao";
        }


        $seldados = "SELECT m.id$tabmoni, m.id".$tabmoni."vinc, m.idsuper_oper, m.idoperador, m.data,
                    m.horaini, m.horafim, m.tmpaudio, m.idplanilha, m.idrel_filtros, fd.nomefiltro_dados,
                    p.tipomoni, m.gravacao, tabfila FROM $tabmoni m
                    INNER JOIN operador o ON o.idoperador = m.idoperador
                    INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                    INNER JOIN conf_rel c ON c.idrel_filtros = m.idrel_filtros
                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.$this->filtro
                    INNER JOIN param_moni p ON p.idparam_moni = c.idparam_moni
                    WHERE m.id$tabmoni='".$this->idmoni."'";
        $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta dos dados da monitoria");
        $this->idrel = $eseldados['idrel_filtros'];
        if($eseldados['tabfila'] == "fila_grava") {
            $alias = "fg";
        }
        if($eseldados['tabfila'] == "fila_oper") {
            $alias = "fo";
        }
        $this->idsmoni[$eseldados['idplanilha']] = $eseldados['id'.$tabmoni];
        if($eseldados['id'.$tabmoni.'vinc'] != "0000000") {
            $vinc = 1;
        }
        for($i = 0; $i < $vinc; $i++) {
            $selidvinc = "SELECT m.id$tabmoni, m.id".$tabmoni."vinc, m.idplanilha FROM $tabmoni m WHERE id$tabmoni='".$eseldados['id'.$tabmoni.'vinc']."'";
            $eselidvinc = $_SESSION['fetch_array']($_SESSION['query']($selidvinc)) or die ("erro na query de consulta dos dados da monitoria vinculada");
            $this->idsmoni[$eselidvinc['idplanilha']] = $eseldados['id'.$tabmoni.'vinc'];
            if($eselidvinc['id'.$tabmoni.'vinc'] != "0000000") {
                $i = 0;
            }
            else {
                $i = 1;
            }
        }

        $count = $this->idsmoni;
        foreach($this->idsmoni as $idplan => $id) {
            $p = 0;
            $ip = 0;
            $selplans = "SELECT DISTINCT(vp.idvinc_plan), vp.planvinc, p.descriplanilha, vp.idrel_filtros
                        FROM vinc_plan vp
                        INNER JOIN planilha p ON p.idplanilha = vp.planvinc WHERE vp.idplanilha='$idplan' AND vp.ativo='S'";
            $eselplans = $_SESSION['query']($selplans) or die ("erro na query de consulta do vinculo da planiha");
            while($lselplans = $_SESSION['fetch_array']($eselplans)) {
                if($lselplans['planvinc'] == "") {

                }
                else {
                    $this->plansdepende[$ip]['idmonitoria'] = $id;
                    $this->plansdepende[$ip]["idplan"] = $lselplans['planvinc'];
                    $this->plansdepende[$ip]["nomeplan"] = $lselplans['descriplanilha'];
                    $this->plansdepende[$ip]["relfiltro"] = $lselplans['idrelfiltro'];
                    $this->plansdepende[$ip]["plancondicional"] = "N";
                    $ip++;
                }
            }

            $vincperg = "SELECT p.idplanilha, ps.idpergunta, ps.idresposta, ps.avaliaplan, ps.idrel_avaliaplan, p.idtabulacao FROM planilha p
                                INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                                LEFT JOIN subgrupo s ON s.idsubgrupo = g.idrel AND g.filtro_vinc='S'
                                LEFT JOIN pergunta ps ON ps.idpergunta = s.idpergunta AND g.filtro_vinc='S' OR ps.idpergunta = g.idrel AND g.filtro_vinc='P'
                                WHERE p.idplanilha='$idplan' AND idaval_plan<>'' AND (idrel_origem IS NULL OR idrel_origem='$this->idrel')
                                GROUP BY ps.idresposta ORDER BY g.idgrupo";
            $evincperg = $_SESSION['query']($vincperg) or die ("erro na query de consulta das planilhas vinculadas por pergunta");
            $nvincperg = $_SESSION['num_rows']($evincperg);
            if($nvincperg >= 1) {
                while($lvincperg = $_SESSION['fetch_array']($evincperg)) {
                    if($ip == 0) {
                        $ipv = 0;
                    }
                    else {
                        $ipv = $ip - 1;
                    }
                    if($this->plansdepende[$ipv]['idplan'] == $lvincperg['avaliaplan'] OR $lvincperg['avaliaplan'] == "") {
                    }
                    else {
                        if($this->pagina == "CALIBRAGEM" && $eseldados['id'.$tabmoni.'vinc'] == "0000000") {
                        }
                        else {
                            $nomeplan = "SELECT descriplanilha, idtabulacao FROM planilha WHERE idplanilha='".$lvincperg['avaliaplan']."' GROUP BY idplanilha";
                            $enomeplan = $_SESSION['fetch_array']($_SESSION['query']($nomeplan)) or die ("erro na query de consulta do nome da planilha");
                            $this->plansdepende[$ip]['idmonitoria'] = $this->idsmoni[$lvincperg['avaliaplan']];
                            $this->plansdepende[$ip]['idplan'] = $lvincperg['avaliaplan'];
                            $this->plansdepende[$ip]['plancondicional'] = 'S';
                            $this->plansdepende[$ip]['nomeplan'] = $enomeplan['descriplanilha'];
                            if($lvincperg['idrel_avaliaplan'] == "") {
                                $verifaud = "SELECT * FROM conf_rel WHERE idrel_filtros='$this->idrel'";
                                $everifaud = $_SESSION['fetch_array']($_SESSION['query']($verifaud)) or die (mysql_error());
                                if($everifaud['idrel_aud'] != "") {
                                    $this->plansdepende[$ip]['relfiltro'] = $everifaud['idrel_aud'];
                                }
                                else {
                                    $this->plansdepende[$ip]['relfiltro'] = $this->idrel;
                                }
                            }
                            else {
                                $this->plansdepende[$ip]['relfiltro'] = $lvincperg['idrel_avaliaplan'];
                            }
                            $this->plansdepende[$ip]['idresposta'] = $lvincperg['idresposta'];
                            $this->plansdepende[$ip]['idpergunta'] = $lvincperg['idpergunta'];
                            $this->plansdepende[$ip]['idplanorigem'] = $lvincperg['idplanilha'];
                            if($enomeplan['idtabulacao'] == "") {
                                $this->plansdepende[$ip]['idtabulacao'] = "";
                            }
                            else {
                                $this->plansdepende[$ip]['idtabulacao'] = explode(",",$enomeplan['idtabulacao']);
                            }
                            $ip++;
                        }
                    }
                }
            }
            else {
            }
        }
        foreach($this->plansdepende as $plans) {
            if(key_exists($plans['idplan'],$this->idsmoni)) {
            }
            else {
                $this->idsmoni[$plans['idplan']] = "";
            }
        }
        ?>
        <head>
        <!--<link href="option.css" rel="stylesheet" type="text/css" />-->
        <!--<link href="../styleadmin.css" rel="stylesheet" type="text/css" />-->
        <script src="/monitoria_supervisao/js/jquery-autocomplete/lib/thickbox-compressed.js" type="text/javascript"></script>
        <script src="/monitoria_supervisao/js/jquery-autocomplete/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
        <script src="/monitoria_supervisao/js/jquery-accordion/jquery.accordion.js" type="text/javascript"></script>
        <script src="/monitoria_supervisao/js/jquery-accordion/lib/jquery.dimensions.js" type="text/javascript"></script>
        <script src="/monitoria_supervisao/js/jquery-accordion/lib/jquery.easing.js" type="text/javascript"></script>
        <script src="/monitoria_supervisao/js/jquery-accordion/lib/chili-1.7.pack.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/monitoria_supervisao/monitor/tabs.css" type="text/css" />
        <link rel="stylesheet" href="/monitoria_supervisao/users/styleexp.css" type="text/css" />
        <script type="text/JavaScript" src="/monitoria_supervisao/audio-player/audio-player.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                var faval = false;
                $("div[id^='grupoaval']").hide();
                $("div[id*='aval_']").click(function() {
                var aval = $(this).attr("id");
                var IDaval = aval.substr(5);
                    if(faval == false) {
                        $('#grupoaval'+IDaval).show();
                        faval = true;
                    }
                    else {
                        $('#grupoaval'+IDaval).hide();
                        faval = false;
                    }
                })

                var fgrup = false;
                $("textarea").live('keyup',function(){
                    var valor = $(this).val().replace('&','E');
                    $(this).val(valor);
                })
                $("div[id^='subperg_']").hide();
                $("div[id*='grupo_']").click(function() {
                    var grup = $(this).attr("id");
                    var IDgrup = grup.substr(6,13);
                    if(fgrup == false) {
                        $('#subperg_'+IDgrup).show();
                        fgrup = true;
                    }
                    else {
                        $('#subperg_'+IDgrup).hide();
                        fgrup = false;
                    }
                })

                $('#exp').click(function() {
                    fgrup = false;
                    faval = false;
                    $("div[id^='subperg_']").show();
                    $("div[id^='grupoaval']").show();
                })

                $('#rec').click(function() {
                    fgrup = true;
                    faval = true;
                    $("div[id^='subperg_']").hide();
                    $("div[id^='grupoaval']").hide();
                })

                var idfila = $('#idfila').val();
                var tipo = $('#tipomoni').val();
                $('#idrelfiltro').change(function() {
                    $('#idoper').html('<option value=\"0\">Carregando...</option>');
                    $.post("reloper.php", {idrel: $(this).val(), idfila: idfila, tipo: tipo}, function(valor) {
                        $('#idoper').html(valor);
                    })
                })

                $('div.divabas').hide();
                $('div.divabas:first').show();
                $("div[id*='dados']").hide();
                $("div[id*='alertas']").hide();
                $("div[id*='arvtab']").hide();
                $("div[id*='divobs']").hide();
                $("div[id*='dados']:first").show();
                $("div[id*='alertas']:first").show();
                $("div[id*='fluxo']").hide();
                $("div[id*='fluxo']:first").show();
                $("div[id*='divobs']:first").show();
                $('#linka a:first').addClass("selected");
                $("a[id*='linka']").live('click', function() {
                    $("[id*='idli']").css("background-color","#333333");
                    $("[id*='linka']").css("color","#FFFFFF");
                    var div = $(this).attr("id");
                    var iddiv = div.substr(5,11);
                    if(iddiv == 4) {
                    }
                    else {
                        iddiv = iddiv.substr(0,4);
                    }
                    var tab = div.substr(10,2);
                    var len = div.length;
                    $('div.divabas').hide();
                    $("div[id*='divobs']").hide();
                    $("div[id*='dados']").hide();
                    $("div[id*='alertas']").hide();
                    $('#abas a').removeClass("selected");
                    $('#linka'+iddiv).addClass("selected");
                    if(len == 12) {
                        $("#arvtab"+iddiv+'_'+tab).show();
                        $('#plan'+iddiv).hide();
                        $("#divobs"+iddiv).hide();
                        $('#idlitab'+tab).css("background-color","#C90");
                        $('#linka'+iddiv+'_'+tab).css("color","#000");
                    }
                    else {
                        $("#arvtab"+iddiv+'_'+tab).hide();
                        $('#plan'+iddiv).show();
                        $("#divobs"+iddiv).show();
                        $('#idli'+iddiv).css("background-color","#C90");
                        $('#linka'+iddiv).css("color","#000");
                    }
                    $('#dados'+iddiv).show();
                    $('#alertas'+iddiv).show();
                    $("div[id*='fluxo']").hide();
                    $('#fluxo'+iddiv).show();
                    return false;
                })

                $("select[id^='idresp']").change(function() {
                    var id = $(this).attr("id");
                    var idperg = id.substr(6,6);
                    $.post("/monitoria_supervisao/af.php", {idresp:$(this).val(), idperg: idperg}, function(valor) {
                        var texto = valor;
                        var separa = texto.split(".");
                        var abre = separa[1];
                        var idpergn = separa[0];
                        if(abre == "F") {
                            $('#trdescri'+idpergn).hide();
                        }
                        if(abre == "A") {
                            $('#trdescri'+idpergn).show();
                        }
                        if(abre == "D") {
                            alert("Você não pode selecionar duas respostas conflitantes!!!");
                        }
                    })
                })

                var abre = false;
                $("div[id*='tabhist']").hide();
                $("div[id*='hist']").click(function() {
                    var hist = $(this).attr("id");
                    var IDhist = hist.substr(4,10);
                    if(abre == false) {
                    $('#tabhist'+IDhist).show();
                        abre = true;
                    }
                    else {
                        $('#tabhist'+IDhist).hide();
                        abre = false;
                    }
                })

                $('#voltapesq').live('click', function() {
                    window.location = 'inicio.php?menu=monitoria&retconsult=1';
                })

            <?php
            foreach($this->plansdepende as $jvinc) {
                if($jvinc['plancondicional'] == "S") {
                    if(array_key_exists($jvinc['idplan'], $this->idsmoni)) {
                        $idmoniv = $this->idsmoni[$jvinc['idplan']];
                        if($idmoniv == "") {
                            echo "$('#idli".$jvinc['idplan']."').remove();\n";
                            echo "$('#idsplan".$jvinc['idplan']."').remove();\n";
                        }
                    }
                    else {
                        echo "$('#idli".$jvinc['idplan']."').remove();\n";
                    }
                }
            }
            foreach($this->plansdepende as $vinc) {
                if($vinc['plancondicional'] == "S") {
                    ?>
                    $('#idresp<?php echo $vinc['idpergunta'];?>').live('change', function() {
                    var idresp = $(this).val();
                    if(idresp == "<?php echo $vinc['idresposta'];?>") {
                        $('#abas').append('<li id=\"idli<?php echo $vinc['idplan'];?>\"><input type=\"hidden\" name=\"idli<?php echo $vinc['idplan'];?>\" value=\"S\" /><a id=\"linka<?php echo $vinc['idplan'];?>\" href=\"#plan<?php echo $vinc['idplan'];?>\"><strong><span><?php echo $vinc['nomeplan'];?></span></strong></a></li>');
                        var inputexist = $('#idsplan<?php echo $vinc['idplan'];?>').find();
                        if(inputexist != true) {
                            $('.idsplan<?php echo $vinc['idplan'];?>').append('<input name=\"idsplan[]\" type=\"hidden\" value=\"<?php echo $vinc['idplan'];?>\" id=\"idsplan<?php echo $vinc['idplan'];?>\" />');
                        }
                        else {
                            $('#tdidsplan<?php echo $vinc['idplan'];?>').append('<input name=\"idsplan[]\" type=\"hidden\" value=\"<?php echo $vinc['idplan'];?>\" id=\"idsplan<?php echo $vinc['idplan'];?>\" /><input name=\"idsmoni[]\" id=\"idsmoni<?php echo $vinc['idplan'];?>\" type=\"hidden\" value=\"<?php echo $vinc['idmonitoria'];?>\" /><input name=\"idmoni<?php echo $vinc['idplan'];?>\" id=\"idmoni<?php echo $vinc['idplan'];?>\" type=\"hidden\" value=\"<?php echo $vinc['idmonitoria'];?>\" />');
                        }
                        <?php
                        foreach($vinc['idtabulacao'] as $tabli) {
                            $ntab = "SELECT nometabulacao FROM tabulacao WHERE idtabulacao='$tabli'";
                            $entab = $_SESSION['fetch_array']($_SESSION['query']($ntab)) or die ("erro na query de consulta do nome da tabulacao");
                            ?>
                            $('#abas').append('<li id=\"idlitab<?php echo $tabli;?>\"><input type=\"hidden\" name=\"idlitab<?php echo $tabli;?>\" /><a id=\"linka<?php echo $vinc['idplan']."_".$tabli;?>\" href=\"#plan<?php echo $vinc['idplan'];?>\"><?php echo $entab['nometabulacao'];?></a></li>');
                            <?php
                        }
                        ?>
                    }
                    else {
                        $('#idli<?php echo $vinc['idplan'];?>').remove();
                        $('#idlitab<?php echo $tabli;?>').remove();
                        $('#idsplan<?php echo $vinc['idplan'];?>').remove();
                        $('#idsmoni<?php echo $vinc['idplan'];?>').remove();
                        $('#idmoni<?php echo $vinc['idplan'];?>').remove();
                        $('name=idli<?php echo $vinc['idplan'];?>').attr('value','N');
                    }
                    })
                    <?php
                }
            }
            ?>

                  $("#link").click(function() {
                      var dt = new Date().toString().split(" ")[4];
                      if($("#iniescuta").val() == '00:00:00') {
                          $("#iniescuta").val(dt);
                      }
                  })

            })
        </script>

        </head>
        <body>
        <div align="center" id="conteudo">
        <form id="tabmoni">
        <?php
        foreach($this->idsmoni as $plan => $idmoni) {
            unset($this->visuweb);
            if($idmoni != "") {
                if($this->pagina != "CALIBRAGEM") {
                    $selfluxo = "SELECT idrel_fluxo, tipo, COUNT(*) as result FROM rel_fluxo rfl WHERE rfl.idrel_fluxo=(
                                    SELECT rf.idrel_fluxo FROM monitoria m
                                    INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                    INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo WHERE m.idmonitoria='$idmoni'
                                    )";
                    $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die ("erro na query de consulta do status do fluxo atual da monitoria");
                    if($eselfluxo['result'] == 0) {
                        $this->tipostatus[$idmoni] = "";
                    }
                    else {
                        if($this->alteramoni == "S") {
                            $this->tipostatus[$idmoni] = "editar";
                        }
                        else {
                            $this->tipostatus[$idmoni] = $eselfluxo['tipo'];
                        }
                    }
                }
                else {
                }
                $selcampos = "SELECT cp.visumoni, co.nomecoluna FROM $tabmoni m
                                INNER JOIN rel_filtros r ON r.idrel_filtros = m.idrel_filtros
                                INNER JOIN conf_rel cr ON cr.idrel_filtros = r.idrel_filtros
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                INNER JOIN camposparam cp ON cp.idparam_moni = pm.idparam_moni
                                INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                                WHERE m.id$tabmoni='".$idmoni."' AND cp.ativo='S' ORDER BY posicao";
                $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta aos campos do relacionamento");
                while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
                      $this->visuweb[] = $lselcampos['nomecoluna'];
                }
            }
            else {
                foreach($this->plansdepende as $posi) {
                    if($posi['idplan'] == $plan) {
                        $relfiltro = $posi['relfiltro'];
                    }
                    else {
                    }
                }
                $selcampos = "SELECT cp.visumoni, co.nomecoluna FROM rel_filtros rf
                                        INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                        INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                        INNER JOIN camposparam cp ON cp.idparam_moni = pm.idparam_moni
                                        INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                                        WHERE rf.idrel_filtros='".$relfiltro."' AND cp.ativo='S' ORDER BY posicao";
                $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta aos campos do relacionamento");
                while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
                      $this->visuweb[] = $lselcampos['nomecoluna'];
                }
            }
            $selparam = "SELECT pm.tiposervidor,pm.endereco,pm.loginftp,pm.senhaftp,pm.camaudio FROM param_moni pm
                                   INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni WHERE cr.idrel_filtros='$this->idrel'";
            $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
            if($idmoni != "") {
                $colapres = array();
                $dadosapres = "SELECT DISTINCT(co.idcoluna_oper), co.incorpora, co.nomecoluna FROM $tabmoni m
                                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                INNER JOIN camposparam cp oN cp.idparam_moni = pm.idparam_moni
                                INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                                WHERE m.id$tabmoni='".$idmoni."'";
                $edapres = $_SESSION['query']($dadosapres) or die ("erro na query de consulta dos dados que serão apresentados");
                while($ldapres = $_SESSION['fetch_array']($edapres)) {
                    if($ldapres['incorpora'] == "OPERADOR") {
                        $colapres[] = "o.".$ldapres['nomecoluna'];
                    }
                    if($ldapres['incorpora'] == "SUPER_OPER") {
                        $colapres[] = "s.".$ldapres['nomecoluna'];
                    }
                    if($ldapres['incorpora'] == "DADOS") {
                        if($ldapres['nomecoluna'] == "auditor") {
                            $colapres[] = "o.operador as auditor";
                        }
                        else {
                            $colapres[] = $alias.".".$ldapres['nomecoluna'];
                        }
                    }
                }
                $colapres = implode(",",$colapres);

                if($this->pagina == "CALIBRAGEM") {
                    $apresdados = "SELECT m.id$tabmoni, m.id".$tabmoni."vinc, m.idsuper_oper, m.idoperador, m.data, m.horaini, m.horafim, m.tmpaudio, m.idplanilha, m.idrel_filtros, m.idfila, $alias.idupload, fd.nomefiltro_dados, p.tipomoni, m.gravacao, $alias.narquivo,$alias.arquivo,$colapres FROM $tabmoni m
                                   INNER JOIN ".$eseldados['tabfila']." $alias ON $alias.id".$eseldados['tabfila']." = m.idfila
                                   INNER JOIN operador o ON o.idoperador = m.idoperador
                                   INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                                   INNER JOIN conf_rel c ON c.idrel_filtros = m.idrel_filtros
                                   INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                                   INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.$this->filtro
                                   INNER JOIN param_moni p ON p.idparam_moni = c.idparam_moni
                                   WHERE m.id$tabmoni='".$idmoni."' ";
                }
                else {
                    $apresdados = "SELECT semdados,m.alertas,m.id$tabmoni, m.id".$tabmoni."vinc, m.idsuper_oper, m.idoperador, m.data, m.horaini, m.horafim, m.inilig, m.tmpaudio, m.idplanilha, m.idrel_filtros, m.idfila, $alias.idupload, fd.nomefiltro_dados, p.tipomoni, m.gravacao, $alias.narquivo,c.idfluxo, nomestatus, mf.idstatus, mf.iddefinicao, $colapres FROM $tabmoni m
                                   INNER JOIN ".$eseldados['tabfila']." $alias ON $alias.id".$eseldados['tabfila']." = m.idfila
                                   INNER JOIN operador o ON o.idoperador = m.idoperador
                                   INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                                   INNER JOIN conf_rel c ON c.idrel_filtros = m.idrel_filtros
                                   INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                                   INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.$this->filtro
                                   INNER JOIN param_moni p ON p.idparam_moni = c.idparam_moni
                                   INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                   INNER JOIN status st ON st.idstatus = mf.idstatus
                                   WHERE m.id$tabmoni='".$idmoni."' ORDER BY mf.idmonitoria_fluxo DESC LIMIT 1";
                }
                $eapres = $_SESSION['fetch_array']($_SESSION['query']($apresdados)) or die ("erro na query para obter os dados que serão montados");


                $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                $this->idsuper = $eapres['idsuper_oper'];
                $this->idoper = $eapres['idoperador'];
                $this->data = $eapres['data'];
                $this->horaini = $eapres['horaini'];
                $this->horafim = $eapres['horafim'];
                $this->tmpaudio = $eapres['tmpaudio'];
                $this->idplan = $eapres['idplanilha'];
                $this->idrel = $eapres['idrel_filtros'];
                $this->idfila = $eapres['idfila'];
                $this->idupload = $eapres['idupload'];
                $this->nomerel = $eapres['nomefiltro_dados'];
                $this->tipomoni = $eapres['tipomoni'];
                $alertas = explode(",",$eapres['alertas']);
                $arquivo = str_replace($rais, "",$eapres['gravacao']);
                if($eselparam['tiposervidor'] == "FTP") {
                    $ftp = ftp_connect($eselparam['endereco']);
                    $user = $eselparam['loginftp'];
                    $senha = base64_decode($eselparam['senhaftp']);
                    $server = $eselparam['endereco'];
                    $arquivo = str_replace($eselparam['camaudio'],"",$arquivo);
                    $this->caminho = "ftp://$user:$senha@$server/".$_SESSION['nomecli']."$arquivo";
                    //$ftp = ftp_connect($server);
                    $ftplogin = ftp_login($ftp, $user, $senha);
                    //$ftpdir = ftp_chdir($ftp, $eseldados['camaudio']);
                    $tmp = ftp_get($ftp, $rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($eseldados['gravacao'])."", $eseldados['gravacao'], FTP_BINARY);
                    $this->camid3 = "$rais/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($eseldados['gravacao']);
                    $this->caminho = "/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($eseldados['gravacao']);
                }
                else {
                    $this->caminho = $arquivo;
                }
                if($this->tipomoni == "G") {
                    $this->sqltab = "fila_grava";
                }
                if($this->tipomoni == "O") {
                    $this->sqltab = "fila_oper";
                }
                $this->dados = $eapres;

                $idstatus = $eapres['idstatus'];
                $iddefini = $eapres['iddefinicao'];
                $idfluxo = $eapres['idfluxo'];
                if($this->pagina == "CALIBRAGEM") {
                }
                else {
                    $selnstatus = "SELECT s.nomestatus, rf.tipo FROM monitoria m
                                  INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                  INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo
                                  INNER JOIN status s ON s.idstatus = rf.idatustatus
                                  WHERE m.idmonitoria='$idmoni'";
                    $eselnstatus = $_SESSION['fetch_array']($_SESSION['query']($selnstatus)) or die ("erro na query de consulta do nome do próximo status");
                }
            }
            else {
                foreach($this->plansdepende as $kp => $kplan) {
                    if($plan == $kplan['idplan']) {
                        $arrayplan = $kp;
                    }
                    else {
                    }
                }
                if($this->plansdepende[$arrayplan]['idplanorigem'] == "") {
                }
                else {

                    $selfila = "SELECT idauditor FROM $tabmoni m INNER JOIN fila_grava fg ON fg.idfila_grava = m.idfila WHERE m.id$tabmoni='".$this->idsmoni[$this->plansdepende[$arrayplan]['idplanorigem']]."'";
                    $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die ("erro na query de consulta da fila");

                    if($eselfila['idauditor'] != "" && $this->plansdepende[$arrayplan]['plancondicional'] == "S") {
                        $dadosapres = "SELECT DISTINCT(co.idcoluna_oper), co.incorpora, co.nomecoluna FROM rel_filtros rf
                                                INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                                INNER JOIN camposparam cp ON cp.idparam_moni = pm.idparam_moni
                                                INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                                                WHERE rf.idrel_filtros='".$this->plansdepende[$arrayplan]['relfiltro']."'";
                    }
                    else {
                        $dadosapres = "SELECT DISTINCT(co.idcoluna_oper), co.incorpora, co.nomecoluna FROM $tabmoni m
                                                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                                INNER JOIN camposparam cp oN cp.idparam_moni = pm.idparam_moni
                                                INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                                                WHERE m.id$tabmoni='".$this->idsmoni[$this->plansdepende[$arrayplan]['idplanorigem']]."'";
                    }
                    $colapres = array();
                    $edapres = $_SESSION['query']($dadosapres) or die ("erro na query de consulta dos dados que serão apresentados");
                    while($ldapres = $_SESSION['fetch_array']($edapres)) {
                        if($ldapres['incorpora'] == "OPERADOR") {
                            $colapres[] = "o.".$ldapres['nomecoluna'];
                        }
                        if($ldapres['incorpora'] == "SUPER_OPER") {
                            $colapres[] = "s.".$ldapres['nomecoluna'];
                        }
                        if($ldapres['incorpora'] == "DADOS") {
                            if($ldapres['nomecoluna'] == "auditor") {
                                $colapres[] = "oa.operador as auditor";
                            }
                            else {
                                $colapres[] = $alias.".".$ldapres['nomecoluna'];
                            }
                        }
                    }
                    $colapres = implode(",",$colapres);

                    if($this->pagina == "CALIBRAGEM") {
                        $apresdados = "SELECT m.id$tabmoni, m.id".$tabmoni."vinc, m.idsuper_oper, m.idoperador, m.data, m.horaini, m.horafim, m.tmpaudio, m.idplanilha, m.idrel_filtros, m.idfila, $alias.idupload, fd.nomefiltro_dados, p.tipomoni, m.gravacao,$alias.narquivo, $colapres FROM $tabmoni m
                                    INNER JOIN ".$eseldados['tabfila']." $alias ON $alias.id".$eseldados['tabfila']." = m.idfila
                                    INNER JOIN operador o ON o.idoperador = m.idoperador
                                    INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                                    INNER JOIN conf_rel c ON c.idrel_filtros = m.idrel_filtros
                                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.$this->filtro
                                    INNER JOIN param_moni p ON p.idparam_moni = c.idparam_moni
                                    WHERE m.id$tabmoni='".$this->idsmoni[$this->plansdepende[$arrayplan]['idplanorigem']]."'";
                    }
                    else {
                        $apresdados = "SELECT semdados,m.alertas, m.idmonitoria, m.idmonitoriavinc, m.idsuper_oper, m.idoperador,$alias.idauditor,m.data, m.horaini, m.horafim, m.inilig, m.tmpaudio, m.idplanilha, m.idrel_filtros, m.idfila, $alias.idupload, fd.nomefiltro_dados, p.tipomoni, m.gravacao, $alias.narquivo,c.idfluxo, nomestatus, mf.idstatus, mf.iddefinicao, $colapres FROM monitoria m
                                    INNER JOIN ".$eseldados['tabfila']." $alias ON $alias.id".$eseldados['tabfila']." = m.idfila
                                    INNER JOIN operador o ON o.idoperador = m.idoperador
                                    LEFT JOIN operador oa ON oa.idoperador = $alias.idauditor
                                    INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                                    INNER JOIN conf_rel c ON c.idrel_filtros = m.idrel_filtros
                                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.$this->filtro
                                    INNER JOIN param_moni p ON p.idparam_moni = c.idparam_moni
                                    INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                    INNER JOIN status st ON st.idstatus = mf.idstatus
                                    WHERE m.idmonitoria='".$this->idsmoni[$this->plansdepende[$arrayplan]['idplanorigem']]."' ORDER BY mf.idmonitoria_fluxo DESC LIMIT 1";
                    }
                    $eapres = $_SESSION['fetch_array']($_SESSION['query']($apresdados)) or die ("erro na query para obter os dados que serão montados");

                    $this->idsuper = $eapres['idsuper_oper'];
                    if($eselfila['idauditor'] != "") {
                        $this->idoper = $eapres['idauditor'];
                    }
                    else {
                        $this->idoper = $eapres['idoperador'];
                    }
                    $this->data = $eapres['data'];
                    $this->horaini = date("H:i:s");
                    $this->horafim = "";
                    $this->tmpaudio = $eapres['tmpaudio'];
                    $this->idplan = $plan;
                    $this->idrel = $this->plansdepende[$arrayplan]['relfiltro'];
                    $this->idfila = $eapres['idfila'];
                    $this->idupload = $eapres['idupload'];
                    $this->nomerel = $eapres['nomefiltro_dados'];
                    $this->tipomoni = $eapres['tipomoni'];
                    $alertas = explode(",",$eapres['alertas']);
                    $arquivo = str_replace($rais, "",$eapres['gravacao']);
                    if($eselparam['tiposervidor'] == "FTP") {
                        $user = $eselparam['loginftp'];
                        $senha = base64_decode($eselparam['senhaftp']);
                        $server = $eselparam['endereco'];
                        $arquivo = str_replace($eselparam['camaudio'],"",$arquivo);
                        $this->caminho = "ftp://$user:$senha@$server/".$_SESSION['nomecli']."$arquivo";
                        //$ftp = ftp_connect($server);
                        $ftplogin = ftp_login($ftp, $user, $senha);
                        //$ftpdir = ftp_chdir($ftp, $eseldados['camaudio']);
                        $tmp = ftp_get($ftp, $rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($eseldados['gravacao'])."", $eseldados['camaudio']."/".$eseldados['arquivo'], FTP_BINARY);
                        $this->camid3 = "$rais/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($eseldados['gravacao']);
                        $this->caminho = "/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".basename($eseldados['gravacao']);
                    }
                    else {
                        $this->caminho = $arquivo;
                    }
                    if($this->tipomoni == "G") {
                        $this->sqltab = "fila_grava";
                    }
                    if($this->tipomoni == "O") {
                        $this->sqltab = "fila_oper";
                    }
                    $this->dados = $eapres;
                }
            }

            ?>
            <div id="dados<?php echo $plan;?>" style="width:1024px; background-color:#CCC; float: left"><br />
                <table align="center" class="tbdados">
                    <?php
                    if($this->pagina == "CALIBRAGEM" OR $this->alteramoni == "S") {
                    }
                    else {
                    ?>
                    <tr>
                        <td colspan="10" align="left"><input type="button" name="voltapesq" id="voltapesq" value="" style="width:35px; height:35px; background-image:url(/monitoria_supervisao/monitor/images/volta.png); float: left" /></td>
                    </tr>
                <?php
                    }
                $cformula = "SELECT COUNT(*) as result FROM formulas WHERE idplanilha='$plan' AND tipo='P'";
                $ecformula = $_SESSION['fetch_array']($_SESSION['query']($cformula)) or die ("erro na query de consulta da formula cadastrada para a planilha");
                if($ecformula['result'] > 2) {
                    $tr = $ecformula['result'] * 2;
                }
                else {
                    $tr =  1;
                }
                if($idmoni == "") {
                }
                else {

                ?>
                <tr>
                    <?php
                    if($this->pagina == "CALIBRAGEM") {
                        ?>
                        <td width="300" style="font-size:14px;" class="corfd_coltexto"><strong>ID MONITORIA CALIBRAGEM</strong></td>
                        <td align="center" style="font-size:14px;" class="corfd_colcampos" colspan="3"><strong><?php echo $idmoni;?></strong></td>
                        <?php
                    }
                    else {
                        ?>
                        <td width="140" style="font-size:14px;" class="corfd_coltexto"><strong>ID MONITORIA</strong></td>
                        <td width="140" align="center" style="font-size:14px;" class="corfd_colcampos"><strong><?php echo $idmoni;?></strong></td>
                        <td width="140" style="font-size:14px;" class="corfd_coltexto"><strong>DATA</strong></td>
                        <td width="140" align="center" style="font-size:14px;" class="corfd_colcampos"><strong><?php echo banco2data($this->data);?></strong></td>
                        <td width="140" style="font-size:14px;" class="corfd_coltexto"><strong>STATUS</strong></td>
                        <td width="334" align="center" style="font-size:14px;" class="corfd_colcampos" colspan="<?php echo $tr;?>"><strong><?php echo $eselnstatus['nomestatus'];?></strong></td>
                        <?php
                    }
                    ?>
                </tr>
                <tr>
                    <?php
                    if($ecformula['result'] >= 1) {
                        if($ecformula['result'] == 1) {
                            $cols = "3";
                        }
                        else {
                            $cols = "";
                        }
                        $sformula = "SELECT * FROM formulas WHERE idplanilha='$plan' AND tipo='P'";
                        $esformula = $_SESSION['query']($sformula) or die ("erro na query de consulta das formulas cadastradas para a planilha");
                        $calc = new Formulas();
                        while($lsformula = $_SESSION['fetch_array']($esformula)) {
                            $calc->formula = $lsformula['formula'];
                            $calc->idmonitoria = $idmoni;
                            if($this->pagina == "CALIBRAGEM") {
                                $calc->tab = "monitoriacalib";
                                $calc->tabavalia = "moniavaliacalib";
                            }
                            else {
                                $calc->tab = "monitoria";
                                $calc->tabavalia = "moniavalia";
                            }
                            $calculo = $calc->Calc_formula();
                            ?>
                            <td class="corfd_coltexto"><strong><?php echo $lsformula['nomeformulas'];?></strong></td>
                            <td bgcolor="#FFFFFF" colspan="<?php echo $cols;?>" align="center" style="font-size:16px"><strong><?php echo number_format($calculo,2);?></strong></td>
                            <?php
                        }
                    }
                    else {
                    }
                    ?>
                </tr>
                <?php
                }
                ?>
            </table><br/>
            <?php
            if($this->tipomoni == "O") {
            ?>
                <table align="center" class="tbdados">
                    <tr>
                        <td width="140" class="corfd_coltexto"><strong>INICIO LIGAÇÃO</strong></td>
                        <td width="140" class="corfd_colcampos" align="center"><input type="hidden" value="<?php echo $plan;?>" name="idplan" /><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="inilig" id="inilig" title="Inicio Ligação" type="text" value="<?php if($idmoni == "") {echo date("H:i:s");} else { echo $eapres['inilig'];}?>" /></td>
                        <td width="140" class="corfd_coltexto"><strong>INICIO MONITORIA</strong></td>
                        <td width="140" class="corfd_colcampos" align="center"><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="inimoni" id="inimoni" title="Inicio Monitoria" type="text" value="<?php echo $this->horaini;?>" /></td>
                        <td width="140" class="corfd_coltexto"><strong>FIM MONITORIA</strong></td>
                        <td width="140" class="corfd_colcampos" align="center"><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="fimmoni" id="fimmoni" title="Fim Monitoria" type="text" value="<?php if($idmoni == "") { } else {echo $this->horafim;}?>" /></td>
                        <td width="290" class="corfd_colcampos">
                        <?php
                             if(eregi(".wav",$this->caminho) OR eregi(".WAV",$this->caminho)) {
                                if(eregi("Windows",$_SERVER['HTTP_USER_AGENT'])) {
                               ?>
                                <EMBED  height="20" src = "<?php echo $this->caminho;?>" VOLUME = "50" loop = "true" controles = "console" AUTOSTART ="FALSE" width="228" />
                                <?php
                                }
                                else {
                                ?>
                                <audio id="audio1" src="<?php echo $this->caminho;?>" controls preload="auto" autobuffer></audio>
                                <?php
                                }
                            }
                            else {
                                echo $this->dados['narquivo'];
                                ?>
                                <!--<object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php //echo $this->caminho;?>">
                                <param name="quality" value="high">
                                <param name="menu" value="false">
                                <param name="wmode" value="transparent"></object>-->
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                </table><br />
            <?php
            }
            if($this->tipomoni == "G") {
                $selimp = "SELECT * FROM fila_grava fg"
                        . " INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros"
                        . " INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni"
                        . " WHERE idfila_grava='".$this->idfila."'";
                $eselimp = $_SESSION['fetch_array']($_SESSION['query']($selimp)) or die (mysql_error());
                ?>
                <table align="center" class="tbdados">
                    <tr>
                        <td width="120" class="corfd_coltexto"><strong>INICIO MONITORIA</strong></td>
                        <td width="120" class="corfd_colcampos" align="center"><input type="hidden" id="iniescuta" name="iniescuta" value="00:00:00" /><input type="hidden" value="<?php echo $plan;?>" name="idplan" /><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="inimoni" title="Inicio Monitoria" type="text" value="<?php echo $this->horaini;?>" /></td>
                        <td width="120" class="corfd_coltexto"><strong>FIM MONITORIA</strong></td>
                        <td width="120" class="corfd_colcampos" align="center"><input style="width:60px; border: 1px solid #FFF; text-align:center" readonly="readonly" name="fimmoni" title="Fim Monitoria" type="text" value="<?php if($idmoni == "") { } else {echo $this->horafim;}?>" /></td>
                        <td width="410" class="corfd_colcampos" align="center">
                            <?php
                            if(eregi("192.168",$_SESSION['ip']) OR eregi("127.0.0.1",$_SESSION['ip']) OR eregi("localhost",$_SESSION['ip']) OR eregi("::1",$_SESSION['ip'])) {
                                $audio = explode("/",$arquivo);
                                $partaudio = $audio[count($audio) - 1];
                                $extaudio = pathinfo($partaudio,PATHINFO_EXTENSION);
                                if($extaudio == "wav" OR $extaudio == "WAV") {
                                    ?>
                                    <div id="divaudio">
                                    <a href="<?php echo $this->caminho;?>" id="link" target="_blank" >AUDIO</a>
                                    <!-- <EMBED SRC="<?php //echo $this->caminho;?>" autostart='false' loop="false" WIDTH="300" HEIGHT="40">
                                    <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                    <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                    <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php //echo $this->caminho;?>">
                                    <param name="quality" value="high">
                                    <param name="menu" value="false">
                                    <param name="wmode" value="transparent"></object>
                                  </div>-->
                                    <?php
                                }
                                else {
                                    ?>
                                    <div id="divaudio">
                                    <a href="<?php echo $this->caminho;?>" id="link" target="_blank">AUDIO</a>
                                    <!-- <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                    <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                    <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php //echo $this->caminho;?>">
                                    <param name="quality" value="high">
                                    <param name="menu" value="false">
                                    <param name="wmode" value="transparent"></object>
                                  </div>-->
                                    <?php
                                }
                            }
                            else {
                                if($eselimp['tipoimp'] == "I") {
                                    ?>
                                    <a href="<?php echo $this->caminho;?>" id="link" target="_blank" >AUDIO</a>
                                    <?php
                                }
                                else {
                                    echo $this->dados['narquivo'];
                                    $audio = explode("/",$arquivo);
                                    $partaudio = $audio[count($audio) - 1];
                                    $extaudio = pathinfo($partaudio,PATHINFO_EXTENSION);
                                    if($extaudio == "wav" OR $extaudio == "WAV") {
                                        ?>
                                        <EMBED SRC="<?php echo $this->caminho;?>" autostart='false' loop="false" WIDTH="300" HEIGHT="40">
                                        <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                        <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                        <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $this->caminho;?>">
                                        <param name="quality" value="high">
                                        <param name="menu" value="false">
                                        <param name="wmode" value="transparent"></object>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                                        <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                                        <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $this->caminho;?>">
                                        <param name="quality" value="high">
                                        <param name="menu" value="false">
                                        <param name="wmode" value="transparent"></object>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </td>
                    </tr>
                </table><br />
            <?php
            }
            ?>

            <font color="#FF0000"><strong><?php echo mysql_real_escape_string($_GET['msg']);?></strong></font>
            <table align="center" class="tbdados">
                <tr>
                    <td bgcolor="#FF9900" colspan="4" align="center"><strong>DADOS MONITORIA</strong></td>
                </tr>
                <?php
                $qtde = count($this->visuweb);
                $linhas = ceil($qtde / 2);
                ?>
                    <tr>
                        <td class="corfd_coltexto" width="150" align="left"><strong>RELACIONAMENTO</strong></td>
                        <td colspan="3" class="corfd_colcampos" align="left"><strong><?php echo nomeapres($this->idrel);?></strong></td>
                    </tr>
                    <tr>
                        <td width="100" class="corfd_coltexto" align="left"><strong><?php echo strtoupper(str_replace("id_","",$this->filtro));?></strong></td>
                        <td colspan="3" width="600" class="corfd_colcampos" align="left"><input type="hidden" name="idrelfiltro<?php echo $plan;?>" id="idrelfiltro" value="<?php echo $this->idrel;?>" /><input style="width:800px; border: 1px solid #FFF; text-align: left" name="nomerel" id="nomerel" readonly="readonly" type="text" value="<?php echo $this->nomerel;?>" />
                        <input type="hidden" name="idfila" id="idfila" value="<?php echo $this->idfila;?>" /> <input type="hidden" name="idupload" id="idupload" value="<?php echo $this->idupload;?>" /><input type="hidden" name="tipomoni" id="tipomoni" value="<?php echo $this->tipomoni;?>" /></td>
                    </tr>
                <?php
                for($i = 0; $i < $linhas; $i++){
                ?>
                    <tr>
                        <?php
                        for($j = 0; $j < 2; $j++) {
                            if(current($this->visuweb) == "") {
                            }
                            else {
                                $valor = current($this->visuweb);
                                $seltipodados = "SELECT tipo FROM coluna_oper WHERE nomecoluna='".current($this->visuweb)."'";
                                $etipodados = $_SESSION['fetch_array']($_SESSION['query']($seltipodados)) or die ("erro na query da consulta do tipo da coluna");
                                if($etipodados['tipo'] == "DATE") {
                                    $style = "width: 80px; border: 1px solid #FFF; background-color:#FFF";
                                    $valcoluna = banco2data($this->dados[current($this->visuweb)]);
                                }
                                if($etipodados['tipo'] == "TEXT") {
                                    $positext[] = key($this->visuweb);
                                }
                                if($etipodados['tipo'] != "TEXT" && $etipodados['tipo'] != "DATE") {
                                    $style = "width:250px; border: 1px solid #FFF; background-color:#FFF";
                                    if(current($this->visuweb) == "auditor") {
                                        $valcoluna = ucfirst($this->dados["auditor"]);
                                    }
                                    else {
                                        $valcoluna = ucfirst($this->dados[current($this->visuweb)]);
                                    }
                                }
                                if($etipodados['tipo'] != "TEXT") {
                                    ?>
                                    <td width="100" class="corfd_coltexto" align="left"><strong><?php echo ucfirst(current($this->visuweb));?></strong></td>
                                    <td width="180" class="corfd_colcampos" align="left">
                                        <?php
                                        if($eapres['idauditor'] != "" && $this->plansdepende[$ip]['plancondicional'] == "S") {
                                            ?>
                                            <input type="hidden" name="idoper<?php echo $plan;?>" value="<?php echo $this->idoper;?>" />
                                            <?php
                                        }
                                        else {
                                            ?>
                                            <input type="hidden" name="idoper<?php echo $plan;?>" value="<?php echo $this->idoper;?>" />
                                            <?php
                                        }
                                        ?>
                                        <input type="hidden" name="idsuper<?php echo $plan;?>" value="<?php echo $this->dados['idsuper_oper'];?>" />
                                    <?php
                                    if(current($this->visuweb) == "operador") {
                                            $id = "id=\"".current($this->visuweb)."\"";
                                            if($this->dados['trocanome'] == "S") {
                                                ?>
                                                <select name="idoper<?php echo $plan;?>" id="idoper" style="<?php echo $style;?>">
                                                <option value="<?php echo $this->dados['idoperador'];?>" selected="selected"><?php echo $valcoluna;?></option>
                                                <?php
                                                if($this->tipomoni == "G") {
                                                    $seloper = "SELECT DISTINCT(fg.idoperador) FROM fila_grava fg
                                                                INNER JOIN operador o ON o.idoperador = fg.idoperador INNER JOIN upload u ON u.idupload = fg.idupload
                                                                WHERE fg.idupload='".$this->idupload."'";
                                                    $eseloper = $_SESSION['query']($seloper) or die ("erro na consulta dos operadores");
                                                    while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                                                        $selnoper = "SELECT operador FROM operador WHERE idoperador='".$lseloper['idoperador']."'";
                                                        $eselnoper = $_SESSION['fetch_array']($_SESSION['query']($selnoper)) or die ("erro na query de consulta do operador");
                                                        if($lseloper['idoperador'] != $this->dados['idoperador']) {
                                                            ?>
                                                            <option value="<?php echo $lseloper['idoperador'];?>"><?php echo $eselnoper['operador'];?></option>
                                                            <?php
                                                        }
                                                        else {
                                                        }
                                                    }
                                                }
                                                if($this->tipomoni == "O") {
                                                    $seloper = "SELECT DISTINCT(fo.idoperador) FROM fila_oper fo
                                                                INNER JOIN operador o ON o.idoperador = fg.idoperador INNER JOIN upload u ON u.idupload = fo.idupload
                                                                WHERE fo.idupload='".$this->idupload."'";
                                                    $eseloper = $_SESSION['query']($seloper) or die ("erro na consulta dos operadores");
                                                    while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                                                        $selnoper = "SELECT operador FROM operador WHERE idoperador='".$lseloper['idoperador']."'";
                                                        $eselnoper = $_SESSION['fetch_array']($_SESSION['query']($selnoper)) or die ("erro na query de consulta do operador");
                                                        ?>
                                                        <option value="<?php echo $lseloper['idoperador'];?>" title="<?php echo $eselnoper['operador'];?>"><?php echo $eselnoper['operador'];?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </select>
                                                <?php
                                            }
                                            else {
                                                ?>
                                                <input style="width:250px; border: 1px solid #FFF" name="<?php echo current($this->visuweb);?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                                <?php
                                            }

                                    }
                                    if(current($this->visuweb) == "super_oper") {
                                        ?>
                                        <input style="<?php echo $style;?>" name="<?php echo current($this->visuweb);?>" readonly="readonly" type="text" value="<?php echo $valcoluna;?>" />
                                        <?php
                                    }
                                    if(current($this->visuweb) != "operador" && current($this->visuweb) != "super_oper") {
                                    if($this->dados['semdados'] == "S" && $valcoluna == "") {
                                        $read = "";
                                    }
                                    else {
                                        $read = "readonly=\"readonly\"";
                                    }
                                        ?>
                                    <input style="<?php echo $style;?>" name="<?php echo current($this->visuweb);?>" <?php echo $read;?> type="text" value="<?php echo $valcoluna;?>" />
                                        <?php
                                    }
                                }
                                else {
                                    $j--;
                                }
                                next($this->visuweb);
                            }
                        }
                        ?>
                            </td>
                    </tr>
                <?php
                }
                foreach($positext as $posi) {
                    ?>
                    <tr><td colspan="4" bgcolor="#999999" align="center"><strong><?php echo ucfirst($this->visuweb[$posi]);?></strong></td></tr>
                    <?php
                    if($this->semdados == "S") {
                        ?>
                        <tr><td colspan="4"><textarea style="width:100%; height: 70px" readonly="readonly" name="<?php echo $this->visuweb[$posi];?>"><?php echo $this->dados[$this->visuweb[$posi]];?></textarea><td></td></tr>
                        <?php
                    }
                    else {
                        ?>
                        <tr><td colspan="4"><textarea style="width:100%; height: 70px" name="<?php echo $this->visuweb[$posi];?>"><?php echo $this->dados[$this->visuweb[$posi]];?></textarea><td></td></tr>
                        <?php
                    }
                }
                ?>
            </table><br />
            </div>
            <?php

            $selalerta = "SELECT * FROM alertas";
            $ealerta = $_SESSION['query']($selalerta) or die(mysql_error());
            $calerta = $_SESSION['num_rows']($ealerta);
            if($calerta >= 1) {
                ?>
                <div id="alertas<?php echo $plan;?>" style="float:left;margin-top:10px;margin-bottom: 10px; margin-left: 1%; width: 97%; background-color: #F90; padding: 4px">
                    <div style="width: 99%;background-color: #FFF">
                    <?php
                    if($this->alteramoni == "S") {
                        ?>
                        <table width="100%" border="0">
                            <tr>
                                <td align="center" colspan="<?php echo $calerta;?>" style="background-color:#FFF"><span style="color: red; font-weight: bold">ALERTAS</span></td>
                            </tr>
                        </table>
                        <table id="tabalertas" style="margin: auto">
                            <tr>
                                <?php
                                    while($lalerta = $_SESSION['fetch_array']($ealerta)) {
                                        if(in_array($lalerta['idalertas'],$alertas)) {
                                            $ckakerta = "checked=\"checked\"";
                                        }
                                        else {
                                            $ckakerta = "";
                                        }
                                        ?>
                                        <td style=" font-weight: bold; padding: 5px"><input <?php echo $ckakerta;?> type="checkbox" name="idalerta<?php echo $plan;?>" id="idalerta<?php echo $plan;?>" value="<?php echo $lalerta['idalertas'];?>" /> <?php echo $lalerta['nomealerta'];?> </td>
                                        <?php
                                    }
                                ?>
                            </tr>
                        </table>
                        <?php
                    }
                    else {
                    ?>
                        <table width="100%" border="0">
                            <tr>
                                <td align="center" colspan="<?php echo $calerta;?>" style="background-color:#FFF"><span style="color: red; font-weight: bold">ALERTAS</span></td>
                            </tr>
                        </table>
                        <table id="tabalertas" align="center" border="0">
                            <tr>
                                <?php
                                    while($lalerta = $_SESSION['fetch_array']($ealerta)) {
                                        if(in_array($lalerta['idalertas'],$alertas)) {
                                            ?>
                                            <td style=" font-weight: bold" width="120px"><input type="text" style="width:40px; height: 20px; border:0px;background-color:<?php echo "#".$lalerta['coralerta'];?>" name="idalerta<?php echo $plan;?>" id="idalerta<?php echo $plan;?>" value=""/> <?php echo $lalerta['nomealerta'];?></td>
                                            <?php
                                        }
                                    }
                                ?>
                            </tr>
                        </table>
                        <?php
                    }
                    ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <?php
        }
    }

    function MontaPlan_corpovisu() {
        if($this->pagina == "CALIBRAGEM") {
            $tabmoni = "monitoriacalib";
            $tab = "monicalibtabulacao";
            $tabavalia = "moniavaliacalib";
        }
        else {
            $this->idmoni = $_GET['idmoni'];
            $tabmoni = "monitoria";
            $tab = "monitabulacao";
            $tabavalia = "moniavalia";
        }
        ?>
        <table style="margin: auto">
            <tr>
                <td style="padding:10px"><input type="button" id="exp" style="border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px;" value="EXPANDIR" /></td>
                <td style="padding:10px"><input type="button" style="border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px;" id="rec" value="RETRAIR" /></td>
            </tr>
        </table><br/>
        <div id="tabnav">
            <div align="center" id="tabs">
                <ul id="abas">
                    <?php
                    if($this->idsmoni != "") {
                        foreach($this->idsmoni as $idplan => $idmoni) {
                            $idstab = array();
                            if($idplan != "") {
                                $selnplan = "SELECT descriplanilha, idtabulacao, COUNT(*) as result FROM planilha WHERE idplanilha='$idplan'";
                                $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selnplan)) or die ("erro na query de consulta do nome da planilha");
                                ?>
                                <li id="idli<?php echo $idplan;?>"><input type="hidden" name="idli<?php echo $idplan;?>" value="S" /><a id="linka<?php echo $idplan;?>" href="#plan<?php echo $idplan;?>"><strong><span><?php echo $eselplan['descriplanilha'];?></span></strong></a></li>
                                <?php
                                if($idmoni != "") {
                                    $seltabs = "SELECT idtabulacao FROM $tab WHERE id$tabmoni='$idmoni' GROUP BY idtabulacao";
                                    $eseltabs = $_SESSION['query']($seltabs) or die ("erro na query de consulta das tabulações respondiadas");
                                    while($lseltabs = $_SESSION['fetch_array']($eseltabs)) {
                                        if(in_array($lseltabs['id'.$tab],$idstab)) {
                                        }
                                        else {
                                            $idstab[] = $lseltabs['idtabulacao'];
                                        }
                                    }
                                    foreach($idstab as $li) {
                                        if($li != "") {
                                        $seltab = "SELECT idtabulacao, nometabulacao FROM tabulacao WHERE idtabulacao='$li'";
                                        $eseltab = $_SESSION['fetch_array']($_SESSION['query']($seltab)) or die ("erro na query de consulta da tabulacao");
                                        ?>
                                        <li id="idlitab<?php echo $li;?>"><input type="hidden" name="idlitab<?php echo $li;?>" /><a id="linka<?php echo $idplan."_".$li;?>" href="#plan<?php echo $idplan;?>"><?php echo $eseltab['nometabulacao'];?></a></li>
                                        <?php
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
            <?php
            foreach($this->idsmoni as $idplan => $idmoni) {
                if($idmoni == "") {
                    $this->MontaPlan_corpomoni(S,$idplan);
                }
                else {
                $selrel = "SELECT idrel_filtros FROM monitoria WHERE idmonitoria='$idmoni'";
                $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel));
                $snplan = "SELECT descriplanilha, idtabulacao FROM planilha WHERE idplanilha='$idplan' GROUP BY idplanilha";
                $enplan = $_SESSION['fetch_array']($_SESSION['query']($snplan)) or die ("erro na query de consulta do nome da planilha");
                ?>
                <div align="center" style="background-color:#EAEAEA;" id="plan<?php echo $idplan;?>" class="divabas">
                      <table width="1024" align="center">
                            <tr>
                                <td align="center" bgcolor="#000000" colspan="3" style="color:#FFF"><strong>PLANILHA</strong></td>
                            </tr>
                            <tr>
                                <td class="corfd_coltexto" align="center" width="820px" id="tdidsplan<?php echo $idplan;?>"><strong><?php echo $enplan['descri'];?></strong><input name="idsplan[]" type="hidden" value="<?php echo $idplan;?>" id="idsplan<?php echo $idplan;?>" /><input name="idsmoni[]" id="idsmoni<?php echo $idplan;?>" type="hidden" value="<?php echo $idmoni;?>" /><input name="idmoni<?php echo $idplan;?>" id="idmoni<?php echo $idplan;?>" type="hidden" value="<?php echo $idmoni;?>" /></td>
                                <td align="center" class="corfd_coltexto" width="100px"><strong>Valor Considerado</strong></td>
                                <td align="center" class="corfd_coltexto" width="100px"><strong>Valor Final</strong></td>
                            </tr>
                      </table>
                    <?php
                      $relaval = "SELECT ma.idaval_plan, av.nomeaval_plan, ra.valor, ma.valor_aval, ma.valor_final_aval, ma.valor_fg  FROM $tabavalia ma
                                 INNER JOIN aval_plan av ON av.idaval_plan = ma.idaval_plan INNER JOIN rel_aval ra ON ra.idaval_plan = av.idaval_plan
                                 WHERE ma.id$tabmoni='$idmoni' AND ma.idplanilha='$idplan' GROUP BY ma.idaval_plan";
                      $erelaval = $_SESSION['query']($relaval) or die (mysql_error());
                      while($laval = $_SESSION['fetch_array']($erelaval)) {
                            ?>
                            <div align="center" id="aval_<?php echo $idplan."_".$laval['idaval_plan'];?>">
                            <table width="1024" align="center">
                                <tr>
                                  <td bgcolor="#669966" align="center" width="820px" colspan="2"><strong><?php echo $laval['nomeaval_plan'];?></strong><input name="idaval<?php echo $idplan;?>[]" type="hidden" value="<?php echo $laval['idaval_plan'];?>"/></td>
                                  <td bgcolor="#669966" align="center" width="100px"><?php echo $laval['valor_aval'];?></td>
                                  <td align="cetner" width="100px"><input style="width:95px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_aval[]" type="text" value="<?php echo $laval['valor_final_aval']." - ".$laval['valor_fg'];?>" /></td>
                                </tr>
                            </table>
                            </div>
                            <div align="center" id="grupoaval<?php echo $idplan."_".$laval['idaval_plan'];?>">
                            <?php
                            $selgrup = "SELECT g.idgrupo, g.descrigrupo,g.complegrupo, m.valor_grupo, m.valor_final_grup, g.idrel FROM $tabavalia m
                                       INNER JOIN planilha p ON p.idplanilha = m.idplanilha AND p.idgrupo = m.idgrupo
                                       INNER JOIN grupo g ON g.idgrupo = m.idgrupo WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idaval_plan='".$laval['idaval_plan']."' GROUP BY g.idgrupo ORDER BY p.posicao"; // faz select dos grupos disponíveis na planilha pelo id da planilha
                            $egrup = $_SESSION['query']($selgrup) or die (mysql_error());
                            while($lgrup = $_SESSION['fetch_array']($egrup)) { // faz um loop dos grupos listados na planilha
                                  ?>
                                  <div align="center" id="grupo_<?php echo $lgrup['idgrupo'];?>">
                                      <table width="1024" align="center">
                                          <tr>
                                              <td bgcolor="#999999" align="center" width="820px" title="<?php echo $lgrup['descrigrupo'];?>"><strong><?php echo $lgrup['descrigrupo'];?></strong><input name="idgrup<?php echo $idplan;?>[]" type="hidden" value="<?php echo $lgrup['idgrupo'];?>"/></td>
                                              <td bgcolor="#999999" align="center" width="100px"><?php echo $lgrup['valor_grupo'];?></td>
                                              <td width="100px"><input style="width:95px; border: 1px solid #333;  text-align:center" readonly="readonly" name="valor_grup[]" id="valor_grup" type="text" value="<?php echo $lgrup['valor_final_grup'];?>" /></td>
                                          </tr>
                                      </table>
                                  </div>
                                  <?php
                                  $selsub = "SELECT if(m.idsubgrupo IS NOT NULL,'S','P') as filtro_vinc, m.idgrupo, m.idpergunta, m.idsubgrupo, m.valor_grupo, m.valor_final_grup FROM $tabavalia m
                                            WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idgrupo='".$lgrup['idgrupo']."' GROUP BY m.idgrupo"; // faz outro select do mesmo grupo para criar um loop das perguntas ou subgrupos relacionados
                                  $esub = $_SESSION['query']($selsub) or die (mysql_error());
                                  ?>
                                  <div align="center" id="subperg_<?php echo $lgrup['idgrupo'];?>" style="width:1024px;">
                                          <?php
                                          while($lsub = $_SESSION['fetch_array']($esub)) {
                                              ?>
                                              <table width="1024" align="center">
                                                      <?php
                                                      if($lsub['filtro_vinc'] == "S") {
                                                            $nsub = "SELECT s.idsubgrupo, s.descrisubgrupo,s.complesubgrupo, m.valor_sub, m.valor_final_sub, s.idpergunta FROM $tabavalia m
                                                                    INNER JOIN grupo g ON g.idgrupo = m.idgrupo INNER JOIN subgrupo s ON s.idsubgrupo = m.idsubgrupo
                                                                    WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND g.idgrupo='".$lsub['idgrupo']."' AND g.filtro_vinc='S' GROUP BY s.idsubgrupo ORDER BY s.posicao"; // faz a seleção do subgrupo baseado no id_rel (id de relacionamento do grupo)
                                                            $ensub = $_SESSION['query']($nsub) or die (mysql_error());
                                                            while($lnsub = $_SESSION['fetch_array']($ensub)) {
                                                                ?>
                                                                <tr>
                                                                    <td bgcolor="#99CCFF" align="center" width="820px" title="<?php echo $lnsub['complesubgrupo'];?>"><strong><?php echo $lnsub['descrisubgrupo'];?></strong><input name="idsub<?php echo $idplan;?>[]" type="hidden" value="<?php echo $lnsub['idsubgrupo'];?>"/></td>
                                                                    <td bgcolor="#99CCFF" align="center" width="100px"><?php echo $lnsub['valor_sub'];?></td>
                                                                    <td align="center" width="100px"><input style="width:95px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_sub[]" id="valor_sub" type="text" value="<?php echo $lnsub['valor_final_sub'];?>" /></td>
                                                                </tr>
                                                                <?php
                                                                $cperg = "SELECT COUNT(m.idpergunta) as result FROM $tabavalia m INNER JOIN subgrupo s ON s.idsubgrupo = m.idsubgrupo WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND s.idsubgrupo='".$lnsub['idsubgrupo']."'"; // faz select para identificar a quantidade de perguntas relacionadas ao subgrupo
                                                                $ecperg = $_SESSION['fetch_array']($_SESSION['query']($cperg)) or die (mysql_error());
                                                                if($ecperg['result'] == 0) { // caso a quantidade seja 0, não insere nada na planilha
                                                                }
                                                                if($ecperg['result'] >= 1) { // caso seja igual ou maior que 1 seja com os parametros
                                                                    $selperg = "SELECT s.idsubgrupo, s.descrisubgrupo, m.idpergunta FROM $tabavalia m
                                                                                INNER JOIN subgrupo s ON s.idsubgrupo = m.idsubgrupo AND s.idpergunta = m.idpergunta
                                                                                WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idsubgrupo='".$lnsub['idsubgrupo']."' GROUP BY idpergunta ORDER BY s.posicao"; // faz novo select do subgrupo para criar loop das perguntas
                                                                    $eperg = $_SESSION['query']($selperg) or die (mysql_error());
                                                                    while($lperg = $_SESSION['fetch_array']($eperg)) {
                                                                            if($lperg['idpergunta'] == '0') { // verifica se o campo id_perg está zerado, pois se estiver não existe pergunta relacionada
                                                                            }
                                                                            else {
                                                                                $nperg = "SELECT p.idpergunta, p.descripergunta, m.valor_perg, m.valor_final_perg, p.idresposta, p.descriresposta, p.tipo_resp, p.avalia, m.obs FROM $tabavalia m
                                                                                         INNER JOIN pergunta p ON p.idresposta = m.idresposta AND p.idresposta = m.idresposta
                                                                                         WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idpergunta='".$lperg['idpergunta']."' GROUP BY m.idpergunta ORDER BY p.posicao";
                                                                                $enperg = $_SESSION['fetch_array']($_SESSION['query']($nperg)) or die (mysql_error());
                                                                                ?>
                                                                                <tr>
                                                                                    <td bgcolor="#FFCC99" align="center" width="820px"><?php if($this->tipostatus[$idmoni] == "inicia") { echo "<input name=\"idpergcontest".$idplan."[]\" id=\"idpergcontest\" type=\"checkbox\" value=\"".$enperg['idpergunta']."\" title=\"".$enperg['descripergunta']."\" />"; } else {}?><strong><?php echo $enperg['descripergunta'];?></strong><input name="idperg<?php echo $idplan;?>[]" id="idperg<?php echo $enperg['idpergunta'];?>" type="hidden" value="<?php echo $enperg['idpergunta'];?>"/></td>
                                                                                    <td bgcolor="#FFCC99" align="center" width="100px"><?php echo $enperg['valor_perg'];?></td>
                                                                                    <td align="center" width="100px"><input style="width:95px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_perg[]" id="valor_perg" type="text" value="<?php echo $enperg['valor_final_perg'];?>" /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                <?php
                                                                                if($enperg['tipo_resp'] == "U") {
                                                                                    $tipoinput = "";
                                                                                }
                                                                                if($enperg['tipo_resp'] == "M") {
                                                                                    $tipoinput = "multiple=\"multiple\" size=\"5\"";
                                                                                }
                                                                                ?>
                                                                                <td align="left">
                                                                                <?php
                                                                                $classe = "style=\"backgroud:#FFF;\"";
                                                                                if($this->tipostatus[$idmoni] == "editar") {
                                                                                    $escrita = "";
                                                                                    ?>
                                                                                    <select style="width:800px; text-align:center; background:#FFF" <?php echo $tipoinput;?> name="idresp<?php echo $idplan;?>[]" id="idresp<?php echo $enperg['idpergunta'];?>">
                                                                                    <?php
                                                                                    $selresp = "SELECT p.idpergunta, p.descripergunta, m.valor_perg, m.valor_final_perg, p.idresposta, p.descriresposta, p.avalia, m.valor_resp, m.obs FROM $tabavalia m
                                                                                               INNER JOIN pergunta p ON p.idresposta = m.idresposta AND p.idresposta = m.idresposta
                                                                                               WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idpergunta='".$lperg['idpergunta']."' ORDER BY p.posicao";
                                                                                    $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                                    while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                                        if($lresp['avalia'] == "FG") {
                                                                                            $classe = "style=\"background:#EA6565;\"";
                                                                                        }
                                                                                        if($lresp['avalia'] == "FGM") {
                                                                                            $classe = "style=\"background:#969;\"";
                                                                                        }
                                                                                        $idresp[] = $lresp['idresposta'];
                                                                                        ?>
                                                                                        <option <?php echo $classe;?> selected="selected" value="<?php echo $lresp['idresposta'];?>" title="<?php echo $lresp['descriresposta'];?>"><?php echo $lresp['descriresposta'];?></option>
                                                                                        <?php
                                                                                    }
                                                                                    $nresp = "SELECT idresposta, descriresposta, idrel_origem FROM pergunta WHERE idpergunta='".$enperg['idpergunta']."' AND ativo_resp='S' ORDER BY posicao";
                                                                                    $enresp = $_SESSION['query']($nresp) or die (mysql_error());
                                                                                    while($lrespop = $_SESSION['fetch_array']($enresp)) {
                                                                                        if(in_array($lrespop['idresposta'],$idresp)) {

                                                                                        }
                                                                                        else {
                                                                                            if($lrespop['idrel_origem'] != "" && $lrespop['idrel_origem'] != $eselrel['idrel_filtros']) {
                                                                                            }
                                                                                            else {
                                                                                                ?>
                                                                                                <option value="<?php echo $lrespop['idresposta'];?>" title="<?php echo $lrespop['descriresposta'];?>"><?php echo $lrespop['descriresposta'];?></option>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    </select>
                                                                                    <?php
                                                                                }
                                                                                else {
                                                                                    $color = "background:#FFF;";
                                                                                    if($enperg['avalia'] == "FG") {
                                                                                        $color = "background:#EA6565;";
                                                                                    }
                                                                                    if($enperg['avalia'] == "FGM") {
                                                                                        $color = "background:#969;";
                                                                                    }
                                                                                    $escrita = "readonly=\"readonly\"";
                                                                                    ?>
                                                                                    <table id="tbresp" width="100%">
                                                                                        <?php
                                                                                        if($enperg['tipo_resp'] == "U") {
                                                                                            ?>
                                                                                            <tr height="20px">
                                                                                                <td style="<?php echo $color;?>">
                                                                                                    <?php
                                                                                                    if($this->tipostatus[$idmoni] == "inicia") {
                                                                                                    ?>
                                                                                                    <input name="idrespcontest<?php echo $idplan;?>[]" id="idrespcontest<?php echo $idplan;?>" type="checkbox" value="<?php echo $enperg['idpergunta']."-".$enperg['idresposta'];?>" style="width:15px; height: 15px"/>
                                                                                                    <?php
                                                                                                    }
                                                                                                    ?>
                                                                                                <?php echo $enperg['descriresposta'];?>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <?php
                                                                                        }
                                                                                        else {
                                                                                            $c = 0;
                                                                                            $selresp = "SELECT p.idpergunta, p.descripergunta, m.valor_perg, m.valor_final_perg, p.idresposta, p.descriresposta, p.avalia, m.valor_resp, m.obs FROM $tabavalia m
                                                                                                                INNER JOIN pergunta p ON p.idresposta = m.idresposta AND p.idresposta = m.idresposta
                                                                                                                WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idpergunta='".$lperg['idpergunta']."' ORDER BY p.posicao";
                                                                                            $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                                            $cresp = $_SESSION['num_rows']($eresp);
                                                                                            while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                                                $c++;
                                                                                                if($lresp['avalia'] == "FG") {
                                                                                                    $classe = "style=\"background:#EA6565; color:#000\"";
                                                                                                }
                                                                                                if($lresp['avalia'] == "FGM") {
                                                                                                    $classe = "style=\"background:#969; color:#000\"";
                                                                                                }
                                                                                                if($lresp['avalia'] != "FG" && $lresp['avalia'] != "FG") {
                                                                                                    $classe = "style=\"color:#000;\"";
                                                                                                }
                                                                                                if($c == 1) {
                                                                                                ?>
                                                                                                <tr height="20px">
                                                                                                    <td <?php echo $classe;?>>
                                                                                                <?php
                                                                                                }
                                                                                                if($this->tipostatus[$idmoni] == "inicia") {
                                                                                                ?>
                                                                                                    <input style="width:15px; height: 15px" name="idrespcontest<?php echo $idplan?>" id="idrespcontest<?php echo $idplan; ?>" type="checkbox" value="<?php echo $lresp['idpergunta']."-".$lresp['idresposta'];?>"/>
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <?php
                                                                                                echo $lresp['descriresposta']."<br/>";
                                                                                                if($cresp == 1) {
                                                                                                    ?>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                }
                                                                                                else {
                                                                                                    if($cresp != 1 && $c == $cresp) {
                                                                                                        ?>
                                                                                                        </td>
                                                                                                        </tr>
                                                                                                        <?php
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </table>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                                </td>
                                                                                <td><input type="hidden" style="width:59px; border: 1px solid #333; text-align:center" name="valor_resp" id="valor_resp" value="" /></td>
                                                                                </tr>
                                                                                <?php
                                                                                if($enperg['obs'] == "") {
                                                                                    $display = "display:none";
                                                                                    $obs = "";
                                                                                }
                                                                                else {
                                                                                    $display = "";
                                                                                    $obs = $enperg['obs'];
                                                                                }
                                                                                ?>
                                                                                <tr id="trdescri<?php echo $enperg['idpergunta'];?>" style="<?php echo $display;?>">
                                                                                    <td><textarea style="width:800px;" <?php echo $escrita;?> id="descri<?php echo $enperg['idpergunta'];?>" name="descri<?php echo $enperg['idpergunta'];?>" rows="5"><?php echo $obs;?></textarea></td>
                                                                                    <td></td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                    }
                                                                }
                                                            }
                                                      }
                                                      if($lsub['filtro_vinc'] == "P") {
                                                            $cperg = "SELECT COUNT(idrel) as result FROM $tabavalia m INNER JOIN grupo g ON g.idgrupo = m.idgrupo WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idgrupo='".$lsub['idgrupo']."'";
                                                            $ecperg = $_SESSION['fetch_array']($_SESSION['query']($cperg)) or die (mysql_error());
                                                            if($ecperg['result'] == 0) {
                                                            }
                                                            if($ecperg['result'] >= 1) {
                                                                $listperg = "SELECT m.idpergunta FROM $tabavalia m
                                                                            INNER JOIN grupo g ON g.idgrupo = m.idgrupo AND g.idrel = m.idpergunta
                                                                            WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idgrupo='".$lsub['idgrupo']."' GROUP BY m.idpergunta";
                                                                $elistperg = $_SESSION['query']($listperg) or die ("erro na query de consulta das perguntas relacionadas ao grupo");
                                                                while($llistperg = $_SESSION['fetch_array']($elistperg)) {
                                                                    if($llistperg['idpergunta'] == 0) {
                                                                    }
                                                                    else {
                                                                        $nperg = "SELECT p.idpergunta, p.descripergunta, m.valor_perg, m.valor_final_perg, p.idresposta, p.descriresposta, p.tipo_resp, p.avalia, p.valor_resp, m.obs
                                                                                  FROM $tabavalia m
                                                                                  INNER JOIN pergunta p ON p.idresposta = m.idresposta
                                                                                  WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND p.idpergunta='".$llistperg['idpergunta']."' ORDER BY p.posicao";
                                                                        $enperg = $_SESSION['fetch_array']($_SESSION['query']($nperg)) or die (mysql_error());
                                                                        ?>
                                                                        <tr>
                                                                            <td bgcolor="#FFCC99" align="center" width="820px"><?php if($this->tipostatus[$idmoni] == "inicia") { echo "<input name=\"idpergcontest".$idplan."[]\" id=\"idpergcontest\" type=\"checkbox\" value=\"".$enperg['idpergunta']."\" />"; } else {}?><strong><?php echo $enperg['descripergunta'];?></strong><input name="idperg<?php echo $idplan;?>[]" id="idperg<?php echo $enperg['idpergunta'];?>" type="hidden" value="<?php echo $enperg['id'];?>"/></td>
                                                                            <td bgcolor="#FFCC99" align="center" width="100px"><?php echo $enperg['valor_perg'];?></td>
                                                                            <td align="center" width="100px"><input style="width:95px; border: 1px solid #333; text-align:center" readonly="readonly" name="valor_perg[]" id="valor_perg" type="text" value="<?php echo $enperg['valor_final_perg'];?>" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                        <?php
                                                                        $color = "background:#FFF;";
                                                                        if($enperg['avalia'] == "FG") {
                                                                            $color = "background:#EA6565;";
                                                                        }
                                                                        if($enperg['avalia'] == "FGM") {
                                                                            $color = "background:#969;";
                                                                        }
                                                                        if($enperg['tipo_resp'] == "U") {
                                                                            $tipoinput = "";
                                                                        }
                                                                        if($enperg['tipo_resp'] == "M") {
                                                                            $tipoinput = "multiple=\"multiple\" size=\"5\"";
                                                                        }
                                                                        ?>
                                                                        <td align="left">
                                                                        <?php
                                                                        $classe = "style=\"background:#FFF;\"";
                                                                        if($this->tipostatus[$idmoni] == "editar") {
                                                                            $escrita = "";
                                                                            ?>
                                                                            <select style="width:800px; text-align:center; background:#FFF" <?php echo $tipoinput;?> name="idresp<?php echo $idplan;?>[]" id="idresp<?php echo $enperg['idpergunta'];?>">
                                                                            <?php
                                                                            $selresp = "SELECT p.idresposta, p.descriresposta, p.avalia, p.valor_resp, m.valor_final_perg FROM $tabavalia m
                                                                                        INNER JOIN pergunta p ON p.idresposta = m.idresposta WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND p.idpergunta='".$llistperg['idpergunta']."' ORDER BY p.posicao";
                                                                            $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                            while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                                if($lresp['avalia'] == "FG") {
                                                                                    $classe = "style=\"background:#EA6565; color:#000;\"";
                                                                                }
                                                                                if($lresp['avalia'] == "FGM") {
                                                                                    $classe = "style=\"background:#969; color:#000;\"";
                                                                                }
                                                                                if($lresp['avalia'] != "FG" && $lresp['avalia'] != "FG") {
                                                                                    $classe = "style=\"color:#000;\"";
                                                                                }
                                                                                $idresp[] = $lresp['idresposta'];
                                                                                ?>
                                                                                <option <?php echo $classe;?> selected="selected" value="<?php echo $lresp['idresposta'];?>" title="<?php echo $lresp['descriresposta'];?>"><?php echo $lresp['descriresposta'];?></option>
                                                                                <?php
                                                                            }
                                                                            $nresp = "SELECT p.idresposta, p.descriresposta,p.idrel_origem FROM pergunta p WHERE p.idpergunta='".$llistperg['idpergunta']."' AND ativo='S' AND ativo_resp='S' ORDER BY p.posicao";
                                                                            $enresp = $_SESSION['query']($nresp) or die (mysql_error());
                                                                            while($lrespop = $_SESSION['fetch_array']($enresp)) {
                                                                                if(in_array($lrespop['idresposta'],$idresp)) {

                                                                                }
                                                                                else {
                                                                                    if($lrespop['idrel_origem'] != "" && $lrespop['idrel_origem'] != $eselrel['idrel_filtros']) {
                                                                                    }
                                                                                    else {
                                                                                        ?>
                                                                                        <option value="<?php echo $lrespop['idresposta'];?>" title="<?php echo $lrespop['descriresposta'];?>"><?php echo $lrespop['descriresposta'];?></option>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>
                                                                            </select>
                                                                            <?php
                                                                        }
                                                                        else {
                                                                                ?>
                                                                                <table id="tbresp" width="100%">
                                                                                    <?php
                                                                                    if($enperg['tipo_resp'] == "U") {
                                                                                        ?>
                                                                                        <tr height="20px">
                                                                                            <td style="text-align: center;<?php echo $color;?>">
                                                                                                <?php
                                                                                                if($this->tipostatus[$idmoni] == "inicia") {
                                                                                                    ?>
                                                                                                    <input style="width:15px; height: 15px" name="idrespcontest<?php echo $idplan;?>[]" id="idrespcontest<?php echo $idplan;?>" type="checkbox" value="<?php echo $enperg['idpergunta']."-".$enperg['idresposta'];?>" />
                                                                                                    <?php
                                                                                                }
                                                                                                ?>
                                                                                                <?php echo $enperg['descriresposta'];?>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                    else {
                                                                                        $c = 0;
                                                                                        $selresp = "SELECT p.idresposta, p.descriresposta, p.avalia, p.valor_resp, m.valor_final_perg FROM $tabavalia m
                                                                                                            INNER JOIN pergunta p ON p.idresposta = m.idresposta WHERE m.id$tabmoni='$idmoni' AND m.idplanilha='$idplan' AND m.idpergunta='".$llistperg['idpergunta']."' ORDER BY p.posicao";
                                                                                        $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                                        $cresp = $_SESSION['num_rows']($eresp);
                                                                                        while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                                            $c++;
                                                                                            if($lresp['avalia'] == "FG") {
                                                                                                $classe = "style=\"background:#EA6565; color:#000; text-align: center;\"";
                                                                                            }
                                                                                            if($lresp['avalia'] == "FGM") {
                                                                                                $classe = "style=\"background:#969; color:#000; text-align: center;\"";
                                                                                            }
                                                                                            if($lresp['avalia'] != "FG" && $lresp['avalia'] != "FGM") {
                                                                                                $classe = "style=\"background:#FFF;color:#000;text-align: center;\"";
                                                                                            }
                                                                                            if($c == 1) {
                                                                                            ?>
                                                                                            <tr height="20px">
                                                                                            <td <?php echo $classe;?>>
                                                                                            <?php
                                                                                            }
                                                                                            if($this->tipostatus[$idmoni] == "inicia") {
                                                                                                ?>
                                                                                                <input style="width:15px; height: 15px" name="idrespcontest<?php echo $idplan;?>[]" id="idrespcontest<?php echo $idplan;?>" type="checkbox" value="<?php echo $llistperg['idpergunta']."-".$lresp['idresposta'];?>"/>
                                                                                                <?php
                                                                                            }
                                                                                            ?>
                                                                                            <?php
                                                                                            echo $lresp['descriresposta']."<br/>";
                                                                                            if($cresp == 1) {
                                                                                                ?>
                                                                                                </td>
                                                                                                </tr>
                                                                                                <?php
                                                                                            }
                                                                                            else {
                                                                                                if($cresp != 1 && $c == $cresp) {
                                                                                                    ?>
                                                                                                    </td>
                                                                                                    </tr>
                                                                                                    <?php
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </table>
                                                                                <?php
                                                                        }
                                                                        ?>
                                                                        </td></tr>
                                                                        <?php
                                                                        if($enperg['obs'] == "") {
                                                                            $display = "display:none";
                                                                            $obs = "";
                                                                        }
                                                                        else {
                                                                            $display = "";
                                                                            $obs = $enperg['obs'];
                                                                        }
                                                                        ?>
                                                                        <tr id="trdescri<?php echo $enperg['idpergunta'];?>" style="<?php echo $display;?>">
                                                                            <td><textarea style="width:800px;" id="descri<?php echo $enperg['idpergunta'];?>"  <?php echo $escrita;?> name="descri<?php echo $enperg['idpergunta'];?>" rows="5"><?php echo $obs;?></textarea></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                           }
                                                      }
                                                  //}
                                                  ?>
                                                  </table>
                                                  <?php
                                          }
                                          ?>
                                  </div>
                                  <?php
                            }
                            ?>
                            </div>
                            <?php
                      }
                      $selobs = "SELECT obs FROM $tabmoni WHERE id$tabmoni='$idmoni'";
                      $eselobs = $_SESSION['fetch_array']($_SESSION['query']($selobs));
                      ?>
                      </div>
                      <div id="divobs<?php echo $idplan;?>">
                        <table witdh="1024">
                            <tr>
                                <td>
                                    <textarea style="width:1000px; height: 150px" id="obsmoni<?php echo $idplan;?>" name="obsmoni<?php echo $idplan;?>"><?php echo $eselobs['obs'];?></textarea>
                                </td>
                            </tr>
                        </table>
                      </div>
                      <?php
                      $idstab = array();
                      $seltabs = "SELECT idtabulacao FROM $tab WHERE id$tabmoni='$idmoni' GROUP BY idtabulacao";
                        $eseltabs = $_SESSION['query']($seltabs) or die ("erro na query de consulta das tabulações respondiadas");
                        while($lseltabs = $_SESSION['fetch_array']($eseltabs)) {
                            if(in_array($lseltabs['idtabulacao'],$idstab)) {
                            }
                            else {
                                $idstab[] = $lseltabs['idtabulacao'];
                            }
                        }
                      foreach($idstab as $tabu) {
                      ?>
                      <script type="text/javascript">
                            $(document).ready(function() {
                                $("[class*='idresptabprox']").hide();
                                $("[class*='idpergtabprox']").hide();
                                <?php
                                $selpergprox = "SELECT t.idproxpergunta, t.idperguntatab, t.idrespostatab FROM tabulacao t
                                                INNER JOIN $tab mt ON mt.idperguntatab = t.idproxpergunta
                                                WHERE mt.id$tabmoni = '$idmoni' AND mt.idtabulacao='$tabu'";
                                $eselpergprox = $_SESSION['query']($selpergprox) or die ("erro na query para consultar as perguntas vinculadas que foram respodidas");
                                while($lselpergprox = $_SESSION['fetch_array']($eselpergprox)) {
                                    $idspergprox[$lselpergprox['idproxpergunta']] = array($lselpergprox['idperguntatab'] => $lselpergprox['idrespostatab']);
                                }
                                foreach($idspergprox as $p) {
                                    ?>
                                    $('.idpergtabprox<?php echo key($p)."_".current($p);?>').show();
                                    $('.idresptabprox_<?php echo key($p)."_".current($p);?>').show();
                                    <?php
                                }
                                ?>
                                $("[id*='idpergtab']").change(function() {
                                    var vperg = $(this).attr("id");
                                    var idresp = $(this).val();
                                    var idperg = vperg.substr(9,4);
                                    var tab = vperg.substr(14,2);
                                    var pergabre = $(".idpergtabprox"+idperg+"_"+idresp).html();
                                    if(pergabre != null) {
                                        $('.idpergtabprox'+idperg+'_'+idresp).show();
                                        $('.idresptabprox_'+idperg+'_'+idresp).show();
                                    }
                                    else {
                                        $("[class*='idpergtabprox"+idperg+"']").hide();
                                        $("[class*='idresptabprox_"+idperg+"']").hide();
                                    }
                                })
                            });
                      </script>
                      <div id="arvtab<?php echo $idplan."_".$tabu;?>">
                        <table width="930" align="center">
                            <?php
                            $newtab = new ArvoreTab();
                            if($this->tipostatus[$idmoni] == "editar") {
                                $selperg = "SELECT t.idperguntatab,pt.descriperguntatab,t.idrespostatab,t.idproxpergunta, (SELECT COUNT(*) as result FROM tabulacao WHERE idproxpergunta=t.idperguntatab) as result FROM tabulacao t
                                            INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                                            WHERE t.idtabulacao='$tabu' GROUP BY t.idperguntatab ORDER BY t.posicao";
                            }
                            else {
                                $selperg = "SELECT m.idperguntatab, pt.descriperguntatab, m.idrespostatab, rt.descrirespostatab, t.idproxpergunta, (SELECT COUNT(*) as result FROM tabulacao WHERE idproxpergunta=t.idperguntatab) as result FROM $tab m
                                            INNER JOIN tabulacao t ON t.idtabulacao = m.idtabulacao AND t.idperguntatab = m.idperguntatab AND t.idrespostatab = m.idrespostatab
                                            INNER JOIN perguntatab pt ON pt.idperguntatab = m.idperguntatab
                                            INNER JOIN respostatab rt ON rt.idrespostatab = m.idrespostatab
                                            WHERE m.id$tabmoni='$idmoni' AND m.idtabulacao='$tabu' GROUP BY m.idperguntatab ORDER BY t.posicao";
                            }
                            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta da resposta da pergunta");
                            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                                $i = 0;
                                $idsprox = array();
                                $selprox = "SELECT t.idproxpergunta, t.idperguntatab, t.idrespostatab FROM tabulacao t
                                            INNER JOIN $tab mt ON mt.idperguntatab = t.idproxpergunta WHERE t.idtabulacao='$tabu' AND t.idperguntatab='".$lselperg['idperguntatab']."' AND idproxpergunta<>'' GROUP BY t.idproxpergunta";
                                $eselprox = $_SESSION['query']($selprox) or die ("erro na query de consulta da proxima pergunta");
                                $nprox = $_SESSION['num_rows']($eselprox);
                                if($nprox >= 1) {
                                    while($lselprox = $_SESSION['fetch_array']($eselprox)) {
                                        $idsprox[$i]['idproxpergunta'] = $lselprox['idproxpergunta'];
                                        $idsprox[$i]['idperguntatab'] = $lselprox['idperguntatab'];
                                        $idsprox[$i]['idrespostatab'] = $lselprox['idrespostatab'];
                                        $i++;
                                    }
                                }
                                else {
                                }
                                if(in_array($lselperg['idperguntatab'],$newtab->idsproxperg)) {
                                }
                                else {
                                    if($lselperg['result'] >= 1) {
                                    }
                                    else {
                                        ?>
                                          <tr class="idpergtab<?php echo $lselperg['idperguntatab'];?>">
                                            <td style="text-align:left; background-color: #6699CC"><strong><?php echo $lselperg['descriperguntatab'];?></strong></td>
                                          </tr>
                                          <tr class="idresptab_<?php echo $lselperg['idperguntatab'];?>">
                                            <td>
                                                <?php
                                                if($this->tipostatus[$idmoni] == "editar") {
                                                    ?>
                                                    <select style="width:930px" name="idpergtab<?php echo $lselperg['idperguntatab']."_".$tabu;?>" id="idpergtab<?php echo $lselperg['idperguntatab']."_".$tabu;?>">
                                                        <option value=""></option>
                                                    <?php
                                                    $selresp = "SELECT t.idrespostatab, rt.descrirespostatab FROM tabulacao t INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab WHERE t.idperguntatab='".$lselperg['idperguntatab']."' AND idtabulacao='$tabu'";
                                                    $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
                                                    while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                                        $verifresp = "SELECT idrespostatab, idperguntatab, COUNT(*) as result FROM $tab WHERE id$tabmoni='$idmoni' AND idtabulacao='$tabu' AND idperguntatab='".$lselperg['idperguntatab']."'";
                                                        $everifresp = $_SESSION['fetch_array']($_SESSION['query']($verifresp)) or die ("erro na query de consulta da resposta cadastrada na monitoria");
                                                        if($lselresp['idrespostatab'] == $everifresp['idrespostatab']) {
                                                        ?>
                                                        <option value="<?php echo $lselresp['idrespostatab'];?>" selected="selected"><?php echo $lselresp['descrirespostatab'];?></option>
                                                        <?php
                                                        }
                                                        else {
                                                        ?>
                                                        <option value="<?php echo $lselresp['idrespostatab'];?>"><?php echo $lselresp['descrirespostatab'];?></option>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                    </select>
                                                <?php
                                                }
                                                else {
                                                    ?>
                                                    <input style="border: 0px; width: 930px;" name="idpergtab<?php echo $everiftab['idperguntatab'];?>" id="idpergtab<?php $lselperg['idperguntatab'];?>" value="<?php echo $lselperg['descrirespostatab'];?>" />
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                          </tr>
                                          <?php
                                    }
                                      if($idsprox[0] != "") {
                                          foreach($idsprox as $prox) {
                                              $c = 0;
                                              $newtab->continua = 1;
                                              $newtab->idproxperg = $prox['idproxpergunta'];
                                              $newtab->idresp = $prox['idrespostatab'];
                                              $newtab->idperg = $prox['idperguntatab'];
                                              while($newtab->continua > $c) {
                                                 $newtab->arvore_moni($idmoni,$this->tipostatus[$idmoni],$tabu, $newtab->idperg, $newtab->idproxperg, $newtab->idresp,$tab,$tabmoni);
                                              }
                                          }
                                      }
                                }
                            }
                            ?>
                        </table>
                      </div>
                      <?php
                      }

                      if($this->pagina == "CALIBRAGEM" OR $this->alteramoni == "S") {
                      }
                      else {
                          $selatu = "SELECT vis_adm, vis_web, resp_adm, resp_web, rf.idatustatus, rf.idfluxo, tipo FROM monitoria_fluxo mf
                                    INNER JOIN monitoria m ON m.idmonitoria_fluxo = mf.idmonitoria_fluxo
                                    INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo
                                    WHERE m.idmonitoria='$idmoni' ORDER BY mf.idmonitoria_fluxo DESC LIMIT 1";
                         $eselatu = $_SESSION['fetch_array']($_SESSION['query']($selatu)) or die ("erro na query de consulta do relacionamento com o fluxo");
                         $usersatu = "SELECT * FROM rel_fluxo WHERE idstatus='".$eselatu['idatustatus']."' AND tipo<>'finaliza'";
                         $eusers = $_SESSION['query']($usersatu) or die ("erro na query para consulta o próximo status");
                         $nstatus = $_SESSION['num_rows']($eusers);
                         if($nstatus >= 1) {
                             while($lusers = $_SESSION['fetch_array']($eusers)) {
                                 if($lusers['resp_adm'] != "") {
                                     $adms = explode(",",$lusers['resp_adm']);
                                     foreach($adms as $adm) {
                                         if(in_array($adm,$respadm)) {
                                         }
                                         else {
                                            $respadm[] = $adm;
                                         }
                                     }
                                 }
                                 if($lusers['resp_web'] != "") {
                                     $webs = explode(",",$lusers['resp_web']);
                                     foreach($webs as $web) {
                                         if(in_array($web,$respweb)) {
                                         }
                                         else {
                                            $respweb[] = $web;
                                         }
                                     }
                                 }
                             }
                         }
                         else {
                            $respadm = "";
                            $respweb = "";
                         }
                         $tipo = $eselatu['tipo'];
                         $atustatus = $eselatu['idatustatus'];
                         $idfluxo = $eselatu['idfluxo'];
                         //$tipostatus = $tipostatus;

                        if($this->tabuser == "user_adm") {
                            if(in_array($this->perfiluser, $respadm)) {
                                $resp = "S";
                            }
                            else {
                                $resp = "N";
                            }
                        }
                        if($this->tabuser == "user_web") {
                            if(in_array($this->perfiluser, $respweb)) {
                                $resp = "S";
                            }
                            else {
                                $resp = "N";
                            }
                        }
                        /* avalia se o perfil do usuário está dentro dos perfis permitidos para visualização de cada registro do histórico */
                        ?>
                        <div id="fluxo<?php echo $idplan;?>">
                            <div id="hist<?php echo $idplan;?>">
                                <table width="1024">
                                    <tr>
                                        <td colspan="4" bgcolor="#FF9900" align="center"><strong>HISTÓRICO DE AVALIAÇÃO</strong></td>
                                    </tr>
                                 </table>
                             </div>
                             <div id="tabhist<?php echo $idplan;?>">
                                 <table width="1024">
                                    <tr>
                                        <td width="82" align="center" bgcolor="#66CCFF"><strong>Data Ação</strong></td>
                                        <td width="187" align="center" bgcolor="#66CCFF"><strong>Definição</strong></td>
                                        <td width="243" align="center" bgcolor="#66CCFF"><strong>Usuário</strong></td>
                                        <td width="492" align="center" bgcolor="#66CCFF"><strong>OBS</strong></td>
                                    </tr>
                            <?php
                            $selhist = "SELECT mf.idmonitoria_fluxo, rf.vis_adm, rf.vis_web, mf.tabuser, mf.iduser, mf.data, mf.hora, d.nomedefinicao as definicao, s.nomestatus as status, mf.obs, rf.tipo FROM monitoria m
                                            INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                                            INNER JOIN rel_fluxo rf ON rf.idfluxo = cr.idfluxo
                                            INNER JOIN monitoria_fluxo mf ON mf.idstatus = rf.idstatus AND mf.iddefinicao = rf.iddefinicao AND mf.idmonitoria = m.idmonitoria
                                            INNER JOIN definicao d ON d.iddefinicao = mf.iddefinicao
                                            INNER JOIN status s ON s.idstatus = mf.idstatus
                                            WHERE m.idmonitoria='".$idmoni."' ORDER BY mf.idmonitoria_fluxo";
                            $eselhist = $_SESSION['query']($selhist) or die ("erro na query de consulta do histórico do fluxo");
                            while($lhist = $_SESSION['fetch_array']($eselhist)) {
                                $tipouser = $lhist['tabuser'];
                                if($tipouser == "user_adm") {
                                    $aliasuser = ",apelido";
                                }
                                else {
                                    $aliasuser = "";
                                }
                                $nuser = "SELECT nome$tipouser$aliasuser FROM $tipouser WHERE id$tipouser='".$lhist['iduser']."'";
                                $enuser = $_SESSION['fetch_array']($_SESSION['query']($nuser)) or die ("erro na query de consulta do nome do usuário da avaliação");
                                if($this->tabuser == "user_adm") {
                                    $vadm = explode(",",$lhist['vis_adm']);
                                    if(in_array($this->perfiluser, $vadm)) {
                                        ?>
                                         <tr>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo banco2data($lhist['data']);?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $lhist['definicao'];?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $nome = $enuser['nome'.$lhist['tabuser']];?></td>
                                            <td bgcolor="#FFFFFF"><textarea style="width:490px" rows="3" readonly="readonly"><?php echo $lhist['obs'];?></textarea></td>
                                         </tr>
                                         <?php
                                         if($lhist['tipo'] == "contesta") {
                                             $selitensconst = "SELECT *, COUNT(*) as result FROM itenscontest WHERE idmonitoria='$idmoni' AND idmonitoria_fluxo='".$lhist['idmonitoria_fluxo']."'";
                                             $eselitens = $_SESSION['fetch_array']($_SESSION['query']($selitensconst)) or die ("erro na query de consulta dos itens contestados");
                                             if($eselitens['result'] >= 1) {
                                             ?>
                                                 <tr>
                                                 <td colspan="4">
                                                    <table width="1020" border="0">
                                                      <tr>
                                                        <td bgcolor="#CCCC99" colspan="3" align="center"><strong>ITENS CONTESTADOS</strong></td>
                                                      </tr>
                                                      <tr>
                                                        <td bgcolor="#FFFFFF" width="200"><strong>GRUPO</strong></td>
                                                        <td bgcolor="#FFFFFF" width="250"><strong>ITEM</strong></td>
                                                        <td bgcolor="#FFFFFF"><strong>RESPOSTA</strong></td>
                                                      </tr>
                                                      <?php
                                                      if($eselitens['idspergunta'] == "") {
                                                      }
                                                      else {
                                                        $itens = explode(",",$eselitens['idspergunta']);
                                                        foreach($itens as $item) {
                                                                $pergresp = explode('-',$item);
                                                                echo "<tr>";
                                                                $gpergcontest = "SELECT g.descrigrupo as ng, pe.descripergunta as npe, COUNT(*) as result FROM grupo g
                                                                                            INNER JOIN planilha p ON p.idgrupo = g.idgrupo
                                                                                            INNER JOIN pergunta pe ON pe.idpergunta = g.idrel AND g.filtro_vinc='P'
                                                                                            WHERE p.idplanilha='$idplan' AND pe.idpergunta='$pergresp[0]' GROUP BY pe.idpergunta";
                                                                $egpergcontest = $_SESSION['query']($gpergcontest) or die ("erro na query para consulta do nome do grupo e item contestado");
                                                                $ncontest = $_SESSION['num_rows']($egpergcontest);
                                                                $lgperg = $_SESSION['fetch_array']($egpergcontest);
                                                                if($ncontest >= 1) {
                                                                    if(count($pergresp) >= 2) {
                                                                        $selresp = "SELECT descriresposta FROM pergunta WHERE idresposta='$pergresp[1]'";
                                                                        $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na localização da resposta contestada");
                                                                    }
                                                                    echo "<td style=\"background:#FFF;color:#000;font-weight: bold; height:20px\">".$lgperg['ng']."</td>";
                                                                    echo "<td style=\"background:#FFF;color:#000;font-weight: bold\">".$lgperg['npe']."</td>";
                                                                    echo "<td style=\"background:#FFF;color:#EA0523\">".$eselresp['descriresposta']."</td>";
                                                                }
                                                                else {
                                                                        $gsubcontest = "SELECT g.descrigrupo as ng, pe.descripergunta as npe, COUNT(*) as result FROM grupo g
                                                                                                INNER JOIN planilha p ON p.idgrupo = g.idgrupo
                                                                                                INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel AND g.filtro_vinc='S'
                                                                                                INNER JOIN pergunta pe ON pe.idpergunta = s.idpergunta
                                                                                                WHERE p.idplanilha='$idplan' AND pe.idpergunta='$pergresp[0]' GROUP BY pe.idpergunta";
                                                                        $egsubcontest = $_SESSION['query']($gsubcontest) or die ("erro na query para consulta do nome do grupo e item contestado");
                                                                        $ncontest = $_SESSION['num_rows']($egsubcontest);
                                                                        $lgsub = $_SESSION['fetch_array']($egsubcontest);
                                                                        if(count($pergresp) >= 2) {
                                                                            $selresp = "SELECT descriresposta FROM pergunta WHERE idresposta='$pergresp[1]'";
                                                                            $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na localização da resposta contestada");
                                                                        }
                                                                        echo "<td style=\"background:#FFF;color:#000;font-weight: bold;height:20px\">".$lgsub['ng']."</td>";
                                                                        echo "<td style=\"background:#FFF;color:#000;font-weight: bold\">".$lgsub['npe']."</td>";
                                                                        echo "<td style=\"background:#FFF;color:#EA0523\">".$eselresp['descriresposta']."</td>";
                                                                }
                                                                echo "</tr>";
                                                            }
                                                      }
                                                        ?>
                                                    </table>
                                                 </td>
                                                 </tr>
                                              <?php
                                                     }
                                             }
                                         }
                                    }
                                if($this->tabuser == "user_web") {
                                     $vweb = explode(",",$lhist['vis_web']);
                                     if(in_array($this->perfiluser, $vweb)) {
                                         if($_SESSION['user_tabela'] == "user_web") {
                                            if($enuser['apelido'] == "" && $tipouser == "user_adm") {
                                                $nome = $enuser['nome'.$lhist['tabuser']];
                                            }
                                            else {
                                                if($enuser['apelido'] != "" && $tipouser == "user_adm") {
                                                    $nome = $enuser['apelido'];
                                                }
                                                else {
                                                    $nome = $enuser['nome'.$lhist['tabuser']];
                                                }
                                            }
                                        }
                                        else {
                                            $nome = $enuser['nome'.$lhist['tabuser']];
                                        }
                                         ?>
                                         <tr>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo banco2data($lhist['data']);?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $lhist['definicao'];?></td>
                                            <td bgcolor="#FFFFFF" align="center"><?php echo $nome;?></td>
                                            <td bgcolor="#FFFFFF"><textarea style="width:490px" rows="3" readonly="readonly"><?php echo $lhist['obs'];?></textarea></td>
                                         </tr>
                                         <?php
                                         if($lhist['tipo'] == "contesta") {
                                             $selitensconst = "SELECT *, COUNT(*) as result FROM itenscontest WHERE idmonitoria='$idmoni' AND idmonitoria_fluxo='".$lhist['idmonitoria_fluxo']."'";
                                             $eselitens = $_SESSION['fetch_array']($_SESSION['query']($selitensconst)) or die ("erro na query de consulta dos itens contestados");
                                             if($eselitens['result'] >= 1) {
                                             ?>
                                                 <tr>
                                                 <td colspan="4">
                                                    <table width="1024" border="0">
                                                      <tr>
                                                        <td bgcolor="#CCCC99" colspan="2" align="center"><strong>ITENS CONTESTADOS</strong></td>
                                                      </tr>
                                                      <tr>
                                                        <td bgcolor="#FFFFFF" width="300"><strong>GRUPO</strong></td>
                                                        <td bgcolor="#FFFFFF"><strong>ITENS</strong></td>
                                                      </tr>
                                                      <?php
                                                      $itens = explode(",",$eselitens['idspergunta']);
                                                      foreach($itens as $item) {
                                                              echo "<tr>";
                                                              $gpergcontest = "SELECT g.descrigrupo as ng, pe.descripergunta as npe, COUNT(*) as result FROM grupo g
                                                                                INNER JOIN planilha p ON p.idgrupo = g.idgrupo
                                                                                INNER JOIN pergunta pe ON pe.idpergunta = g.idrel AND g.filtro_vinc='P'
                                                                                WHERE p.idplanilha='$idplan' AND pe.idpergunta='$item' GROUP BY pe.idpergunta";
                                                              $egpergcontest = $_SESSION['query']($gpergcontest) or die ("erro na query para consulta do nome do grupo e item contestado");
                                                              $ncontest = $_SESSION['num_rows']($egpergcontest);
                                                              $lgperg = $_SESSION['fetch_array']($egpergcontest);
                                                              if($ncontest >= 1) {
                                                                  echo "<td bgcolor=\"#FFFFFF\">".$lgperg['ng']."</td>";
                                                                  echo "<td bgcolor=\"#FFFFFF\">".$lgperg['npe']."</td>";
                                                              }
                                                              else {
                                                                    $gsubcontest = "SELECT g.descrigrupo as ng, pe.descripergunta as npe, COUNT(*) as result FROM grupo g
                                                                                    INNER JOIN planilha p ON p.idgrupo = g.idgrupo
                                                                                    INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel AND g.filtro_vinc='S'
                                                                                    INNER JOIN pergunta pe ON pe.idpergunta = s.idpergunta
                                                                                    WHERE p.idplanilha='$idplan' AND pe.idpergunta='$item' GROUP BY pe.idpergunta";
                                                                    $egsubcontest = $_SESSION['query']($gsubcontest) or die ("erro na query para consulta do nome do grupo e item contestado");
                                                                    $ncontest = $_SESSION['num_rows']($egsubcontest);
                                                                    $lgsub = $_SESSION['fetch_array']($egsubcontest);
                                                                    echo "<td bgcolor=\"#FFFFFF\">".$lgsub['ng']."</td>";
                                                                    echo "<td bgcolor=\"#FFFFFF\">".$lgsub['npe']."</td>";
                                                              }
                                                              echo "</tr>";
                                                        }
                                                        ?>
                                                    </table>
                                                 </td>
                                                 </tr>
                                              <?php
                                                 }
                                             }
                                         }
                                    }
                                }
                            ?>
                            </table>
                        </div>
                        <?php

                        if($resp == "S" && $tipo != "finaliza") {

                            if($this->jsdefin == 0) {
                            ?>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $('#inseredefini').live('click',function() {
                                        <?php
                                        if($this->tipostatus[$idmoni] == "inicia") {
                                            $vdefini = "SELECT r.idfluxo, r.idstatus, r.iddefinicao, d.nomedefinicao, r.resp_adm,r.resp_web,r.tipo FROM rel_fluxo r
                                                    INNER JOIN definicao d ON d.iddefinicao = r.iddefinicao
                                                    WHERE r.idfluxo='$idfluxo' AND r.idstatus='$atustatus' AND tipo<>'inicia' AND tipo<>'finaliza' AND tipodefinicao='normal'";
                                            $evdefini = $_SESSION['query']($vdefini) or die ("erro na query para consulta as definicações possíveis");
                                            while($lvdefini = $_SESSION['fetch_array']($evdefini)) {
                                                if($lvdefini['tipo'] == "contesta") {
                                                    $contesta[] = $lvdefini['iddefinicao'];
                                                    $contesta = implode(",",$contesta);
                                                }
                                            }
                                            if($contesta != "") {
                                                ?>
                                                var defini = '<?php echo $contesta;?>';
                                                var postdefini =$('#definicao').val();
                                                var itens = '';
                                                $('#idpergcontest:checked').each(function() {
                                                    if(itens == '') {
                                                        itens = this.value;
                                                    }
                                                    else {
                                                        itens = itens + ',' + this.value;
                                                    }
                                                })
                                                if(itens != '') {
                                                }
                                                else {
                                                    var procura = postdefini.search(defini);
                                                    if(procura != -1) {
                                                        //alert('Algum item precisa ser sinalizado para que uma contestação seja inserida'+itens);
                                                        //return false;
                                                    }
                                                }
                                                <?php
                                            }
                                        }
                                        ?>
                                         var idplan = $(this).attr('name').substr(12,4);
                                         var idmoni = $("#idmoni"+idplan).val();
                                         var obs = $("#obsdef"+idplan).val();
                                        if(obs == "") {
                                            alert('A observação precisa estar preenchida para que seja permitida a inclusão de uma avaliação');
                                            return false;
                                        }
                                        else {
                                            var tipo = '<?php echo $tipo;?>';
                                            var digsenha = '';
                                            if(tipo == 'senhaoper') {
                                                digsenha = prompt('Favor digitar a senha');
                                            }
                                            <?php
                                            if($tipo == "senhaoper") {
                                                ?>
                                                if(digsenha == "" || digsenha == null) {
                                                    alert('Favor digitar a senha do operador');
                                                }
                                                else {
                                                $.post('/monitoria_supervisao/exemoni.php',{checksenha:'checksenha',idoper:'<?php echo $this->idoper;?>',senha:digsenha},function(ret) {
                                                if(parseInt(ret) == 1) {
                                                <?php
                                            }
                                            ?>
                                            $.blockUI({ message: '<strong>AGUARDE ANALISANDO MONITORIA...</strong>', css: {
                                                border: 'none',
                                                padding: '15px',
                                                backgroundColor: '#000',
                                                '-webkit-border-radius': '10px',
                                                '-moz-border-radius': '10px',
                                                opacity: .5,
                                                color: '#fff'
                                                }
                                            })
                                            var lenidsplan = $("[name*='idsplan']").serialize();
                                            var arrayids = lenidsplan.split("&");
                                            var tamanho = arrayids.length;
                                            var idsplan = new Array();
                                            var i = 0;
                                            for(i = 0; i < tamanho; i++) {
                                                var limpa = arrayids[i].replace("%5B%5D","");
                                                idsplan[i] = limpa;
                                            }
                                            var lenidsmoni = $("[name*='idsmoni']").serialize();
                                            var arrayidsm = lenidsmoni.split("&");
                                            var tamanho = arrayidsm.length;
                                            var idsmoni = new Array();
                                            var i = 0;
                                            for(i = 0; i < tamanho; i++) {
                                                var limpa = arrayidsm[i].replace("%5B%5D","");
                                                idsmoni[i] = limpa;
                                            }
                                            var vars = $("#tabmoni").serialize("id");
                                            var tmpaudio = $('#tmpaudio').val();
                                            vars = decodeURIComponent(vars.replace('/\][()*+-$#&@!?|',""));

                                            $.post("/monitoria_supervisao/exemoni.php", {vars: vars, idsplan: idsplan,idplan:idplan,idsmoni:idsmoni,idmoni:idmoni,inseredefini:'inseredefini',tmpaudio:tmpaudio,tipo:'<?php echo $tipo;?>',idoper:'<?php echo $this->idoper;?>',senha:digsenha,iniescuta:$("#iniescuta").val()}, function(valor) {
                                                var dados = valor.split("*");
                                                var retorno = dados[0];
                                                var msg = dados[1];
                                                if(parseInt(retorno) >= 1) {
                                                    $.unblockUI();
                                                    $.blockUI({ message: '<strong>'+msg+'</strong>', css: {
                                                        border: 'none',
                                                        padding: '15px',
                                                        backgroundColor: '#000',
                                                        '-webkit-border-radius': '10px',
                                                        '-moz-border-radius': '10px',
                                                        color: '#fff'
                                                        }
                                                    })
                                                    setTimeout($.unblockUI,4000);
                                                    return false;
                                                }
                                                else {
                                                    var direciona = dados[2];
                                                    alert(msg);
                                                    window.location = direciona;
                                                }
                                            });
                                            <?php
                                            if($tipo == "senhaoper") {
                                                ?>
                                                }
                                                    else {
                                                        if(parseInt(ret) == 2) {
                                                            alert('Seu senha está incorreta ou seu usuário não possui senha cadastrada, favor conversar com seu Supervisor!!!');
                                                            return false;
                                                        }
                                                        else {
                                                            alert('Sua senha é inicial, e não foi alterada, favor informar uma nova');
                                                            var senha = '';
                                                            senha = prompt('Digite sue nova senha!!!');
                                                            if(senha == '' || senha == null) {
                                                                alert('Sua senha não pode ser vazia');
                                                                return false;
                                                            }
                                                            else {
                                                                if(senha.length < 8) {
                                                                    alert('A senha deverá ter no mínimo 8 caracteres');
                                                                    return false;
                                                                }
                                                                else {
                                                                    var repete = '';
                                                                    repete = prompt('Repita sua nova senha!!!');
                                                                    if(repete == '' || repete == null) {
                                                                        alert('O valor não pode ser vazio');
                                                                        return false;
                                                                    }
                                                                    else {
                                                                        if(senha != repete) {
                                                                            alert('A senha digitada e sua repetição não estão iguais, tenta novamente!!!');
                                                                            return false;
                                                                        }
                                                                        else {
                                                                            $.post('/monitoria_supervisao/exemoni.php',{altsenha:'altsenha',idoper:'<?php echo $this->idoper;?>',novasenha:senha},function(ret) {
                                                                                if(parseInt(ret) == 1) {
                                                                                    alert('Sua senha foi alterada com sucesso, guarde sua senha. Siga com o feed-back da monitoria');
                                                                                    return false;
                                                                                }
                                                                                else {
                                                                                    alert('Ocorreu algum erro na alteração da senha, favor contatar o Administrador!!!');
                                                                                    return false;
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                });
                                                }
                                                <?php
                                            }
                                            ?>
                                        }
                                    })
                                })

                            </script>
                            <?php
                            $this->jsdefin++;
                            }
                            else {
                            }
                            ?>
                             <div id="avalia<?php echo $idplan;?>" align="center" style="border: 2px dotted #666">
                                 <table width="466">
                                    <tr>
                                        <td><input type="hidden" name="tipostatus<?php echo $idplan;?>" value="<?php echo $tipostatus;?>" /><input type="hidden" name="status<?php echo $idplan;?>" value="<?php echo $atustatus;?>" /><input type="hidden" name="idfluxo<?php echo $idplan;?>" value="<?php echo $idfluxo;?>" /><select style="width:600px" name="defini<?php echo $idplan;?>" id="definicao">
                                        <?php
                                        $sdefini = "SELECT r.idfluxo, r.idstatus, r.iddefinicao, d.nomedefinicao, r.resp_adm,r.resp_web FROM rel_fluxo r
                                                    INNER JOIN definicao d ON d.iddefinicao = r.iddefinicao
                                                    WHERE r.idfluxo='$idfluxo' AND r.idstatus='$atustatus' AND tipo<>'inicia' AND tipo<>'finaliza' AND tipodefinicao='normal'";
                                        $edefini = $_SESSION['query']($sdefini) or die ("erro na query para consulta as definicações possíveis");
                                        while($ldefini = $_SESSION['fetch_array']($edefini)) {
                                            if($_SESSION['user_tabela'] == "user_adm") {
                                                $compusers = explode(",",$ldefini['resp_adm']);
                                            }
                                            else {
                                                $compusers = explode(",",$ldefini['resp_web']);
                                            }
                                            if(in_array($this->perfiluser,$compusers)) {
                                            ?>
                                            <option value="<?php echo $ldefini['iddefinicao'];?>"><?php echo $ldefini['nomedefinicao'];?></option>
                                            <?php
                                            }
                                            else {
                                            }
                                        }
                                        ?>
                                        </select></td>
                                    </tr>
                                    <tr>
                                        <td><textarea style="width:600px; height:90px;" name="obsdef<?php echo $idplan;?>" id="obsdef<?php echo $idplan;?>"></textarea></td>
                                    </tr>
                                 </table>
                             </div>
                            <div id="buttondefini">
                                <table width="1024">
                                    <tr>
                                        <td align="center"><input style="border:1px solid #FFF; width:65px; height:17px; background-image:url(images/button.jpg); text-align:center" name="inseredefini<?php echo $idplan;?>" id="inseredefini" type="button" value="Enviar" /></td>
                                    </tr>
                                </table>
                            </div>
                            <?php
                        }
                        if($resp == "N") {
                        }
                        ?>
                        </div>
            <?php
                      }
            }
            }
            ?>
            </div>
        </form>
        </div>
    <?php
    }

    function MontaPlan_encerra($pagina) {
        if($pagina == "MONITOR") {
            ?>
            <table width="248" align="center">
                <tr>
                    <td align="center"><input style="background-image:url(/monitoria_supervisao/monitor/images/ok.png); width:32px; height:32px; border: 0px" name="validar" id="validar" type="button" value="" title="SALVAR MONITORIA" /></td>
                    <td align="center"><input style="background-image:url(/monitoria_supervisao/monitor/images/exit.png); width:32px; height:32px; border: 0px" name="sair" id="sair" type="button" value="" title="IMPRODUTIVA"/></td>
                    <td align="center"><input style="background-image:url(/monitoria_supervisao/monitor/images/volta.png); width:32px; height:32px; border: 0px; text-decoration: none;" type="button" name="voltar" id="voltar" value="" title="SAIR DA MONITORIA" /></td>
                </tr>
            </table>
            </form>
        <?php
        }
        if($pagina == "CALIBRAGEM") {
            ?>
            <table style="margin: auto;width: 300px">
                <tr>
                    <td style="padding:10px;"><input style="background-image:url(/monitoria_supervisao/monitor/images/ok.png); width:32px; height:32px; border: 0px" name="validar" id="validar" type="button" value="" /></td>
                    <td style="padding:10px;"><input style="background-image:url(/monitoria_supervisao/monitor/images/exit.png); width:32px; height:32px; border: 0px" name="sair" id="sair" type="button" value="" /></td>
                </tr>
            </table>
            </form>
            <?php
        }

    }

    function Improdutivo() {
        ?>
        <div id="tabnav">
            <table width="248" align="center">
                <tr>
                    <td class="corfd_coltexto" align="center" colspan="2"><strong>MOTIVO</strong></td>
                </tr>
                <tr height="30px" bgcolor="#FFF">
                    <td align="center" colspan="2"><input type="hidden" name="idupload" value="<?php echo mysql_real_escape_string($_GET['idupload']);?>" /><input type="hidden" name="idfila" value="<?php echo mysql_real_escape_string($_GET['idfila']);?>" />
                    <input type="hidden" name="tipomoni" value="<?php echo mysql_real_escape_string($_GET['tipomoni']);?>" /><input type="hidden" name="idplan" value="<?php echo mysql_real_escape_string($_GET['planmoni']);?>" /><select name="idmotivo" id="idmotivo">
                    <option value="" selected="selected" disabled="disabled">Selecione um motivo para sair da monitoria</option>
                    <?php
                    $selmot = "SELECT m.idmotivo, m.nomemotivo FROM motivo m INNER JOIN tipo_motivo tm ON tm.idtipo_motivo = m.idtipo_motivo WHERE tm.vincula='regmonitoria' and m.ativo='S'";
                    $eselmot = $_SESSION['query']($selmot) or die ("erro na query de consulta dos motivos");
                    while($lselmot = $_SESSION['fetch_array']($eselmot)) {
                        ?>
                        <option value="<?php echo $lselmot['idmotivo'];?>"><?php echo $lselmot['nomemotivo'];?></option>
                        <?php
                    }
                    ?>
                    </select></td>
              </tr>
              <?php
              $selcontest = "SELECT COUNT(*) as r FROM contestreg WHERE idfila='$this->idfila'";
              $eselcontest = $_SESSION['fetch_array']($_SESSION['query']($selcontest)) or die (mysql_error());
              if($eselcontest['r'] >= 1) {
              ?>
              <tr height="20px">
                   <td class="corfd_ntab" align="center" colspan="2"><strong>SUPERVISOR</strong></td>
              </tr>
              <tr>
                   <td class="corfd_coltexto" width="100px"><strong>Login</strong></td>
                   <td class="corfd_colcampos"><input name="user" id="user" type="text" value="" style="font-size:12px;	text-align:center;width:150px;height:20px;border:2px solid #F90;-moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;" /></td>
              </tr>
              <tr>
                   <td class="corfd_coltexto" width="100px"><strong>Senha</strong></td>
                   <td class="corfd_colcampos"><input name="senha" id="senha" type="password" value="" style="font-size:12px;	text-align:center;width:150px;height:20px;border:2px solid #F90;-moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px;" /></td>
              </tr>
              <?php
              }
              ?>
              <tr>
                <td align="center"><input style="background-image:url(/monitoria_supervisao/monitor/images/ok.png); width:32px; height:32px; border: 0px" name="improdutivo" id="improdutivo" type="button" value="" title="SALVAR IMPRODUTIVA" /></td>
                <td align="center"><input style="background-image:url(/monitoria_supervisao/monitor/images/volta.png); width:32px; height:32px; border: 0px" name="voltarmoni" id="voltarmoni" type="button" value="" title="VOLTAR PARA MONITORIA"/></td>
              </tr>
            </table>
        </div>
        </form>
    <?php
    }
}
?>
