<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include_once 'class.monitoria.php';
$class = get_declared_classes();
if(in_array("TabelaSql", $class)) {
}
else {
    include_once 'class.tabelas.php';
}

class Formulas {
    public $colcons;
    public $idmonitoria;
    public $formula;
    public $select;
    public $varwhere;
    public $valwhere;
    public $retorno;
    public $tab;
    public $tabavalia;

    function Calc_formula($tipo,$idplanform, $where) {
        $tabsql = new TabelasSql();
        
        // ALIASES
        $amoni = $tabsql->AliasTab($this->tab);
        $amoniava = $tabsql->AliasTab($this->tabavalia);
        $ainter = $tabsql->AliasTab(interperiodo);
        $aoper = $tabsql->AliasTab(operador);
        $asuper = $tabsql->AliasTab(super_oper);
        $amonitor = $tabsql->AliasTab(monitor);
        $aperiodo = $tabsql->AliasTab(periodo);
        $afilag = $tabsql->AliasTab(fila_grava);
        $afilao = $tabsql->AliasTab(fila_oper);
        $aplan = $tabsql->AliasTab(planilha);
        $arelaval = $tabsql->AliasTab(rel_aval);
        $aavalplan = $tabsql->AliasTab(aval_plan);
        $arelf = $tabsql->AliasTab(rel_filtros);
        $agrupo = $tabsql->AliasTab(grupo);
        $asubgrupo = $tabsql->AliasTab(subgrupo);
        $apergunta = $tabsql->AliasTab(pergunta);
        $aconf = $tabsql->AliasTab(conf_rel);
        $aparam = $tabsql->AliasTab(param_moni);
        $ainter = $tabsql->AliasTab(intervalo_notas);
        $aindice = $tabsql->AliasTab(indice);
        $amonifluxo = $tabsql->AliasTab(monitoria_fluxo);
        $arelfluxo = $tabsql->AliasTab(rel_fluxo);
        $afluxo = $tabsql->AliasTab(fluxo);
        
        $calc = array("MEDIA" => "AVG", "MIN" => "MIN", "MAX" => "MAX", "SOMA" => "SUM");
        $partes = explode(" ", $this->formula);
        $wheresql = $where;
        foreach($partes as $p) {
            if(eregi("MEDIA",$p) OR eregi("MIN",$p) OR eregi("MAX",$p) OR eregi("SOMA",$p) OR eregi("idaval",$p) OR eregi("idplan",$p)) {
                $part = explode("#",$p);
                if((eregi("MEDIA",$p) OR eregi("MIN",$p) OR eregi("MAX",$p) OR eregi("SOMA",$p)) && eregi("idplan",$p)) {
                    if(eregi("idplan-",$p)) {
                        if(strstr($p,"MEDIA")) {
                            $colq = "DISTINCT(".$amoniava.".id$this->tab), ";
                        }
                        else {
                            $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        }
                        $col = $amoniava.".valor_final_aval";
                        $idplan = explode("-",$part[2]);
                        $idplan = $idplan[1];
                    }
                    if(eregi("idplan_FG",$p)) {
                        if(strstr($p,"MEDIA")) {
                            $colq = "DISTINCT(".$amoniava.".id$this->tab), ";
                        }
                        else {
                            $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        }
                        $col = $amoniava.".valor_fg";
                        $idplan = explode("-",$part[2]);
                        $idplan = $idplan[1];
                    }
                    if($wheresql == "") {
                        $w = "WHERE ".$amoniava.".idplanilha='$idplan'";
                    }
                    else {
                        $key = array('idplanilha' => $idplan);
                    }
                }
                if((eregi("MEDIA",$p) OR eregi("MIN",$p) OR eregi("MAX",$p) OR eregi("SOMA",$p)) && eregi("idaval",$p)) {
                    if(eregi("idaval-",$p)) {
                        if(strstr($p,"MEDIA")) {
                            $colq = "DISTINCT(".$amoniava.".id$this->tab), ";
                        }
                        else {
                            $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        }
                        $col = $amoniava.".valor_final_aval";
                        $idpart = explode("-",$part[2]);
                        $idaval = $idpart[1];
                        $idplan = $idpart[2];
                    }
                    if(eregi("idaval_FG",$p)) {
                        if(strstr($p,"MEDIA")) {
                            $colq = "DISTINCT(".$amoniava.".id$this->tab), ";
                        }
                        else {
                            $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        }
                        $col = $amoniava.".valor_fg";
                        $idpart = explode("-",$part[2]);
                        $idaval = $idpart[1];
                        $idplan = $idpart[2];
                    }
                    if($wheresql == "") {
                        $w = "WHERE ".$amoniava.".idplanilha='$idplan' AND ".$amoniava.".idaval_plan='$idaval'";
                    }
                    else {
                        $key = array('idplanilha' => $idplan,'idaval_plan' => $idaval);
                    }
                }
                if((eregi("MEDIA",$p) OR eregi("MIN",$p) OR eregi("MAX",$p) OR eregi("SOMA",$p)) && !eregi("idplan",$p) && !eregi("idaval",$p)) {
                    if($wheresql == "") {
                        if($idplanform != "") {
                            $idplan = $idplanform;
                            $w = "WHERE ".$amoniava.".idplanilha='$idplan'";
                        }
                        else {
                            if(strstr($p,"MEDIA")) {
                                $colq = "DISTINCT(".$amoniava.".id$this->tab), ";
                            }
                            else {
                                $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                            }
                            $col = $amoniava.".valor_fg";
                        }
                    }
                    else {
                        if(strstr($p,"MEDIA")) {
                            $colq = "DISTINCT(".$amoniava.".id$this->tab), ";
                        }
                        else {
                            $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        }
                        $col = $amoniava.".valor_fg";                        
                        $key = array();
                    }
                }
                if((!eregi("MEDIA",$p) && !eregi("MIN",$p) && !eregi("MAX",$p) && !eregi("SOMA",$p)) && eregi("idplan",$p)) {
                    if(eregi("idplan-",$p)) {
                        $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        $col = $amoniava.".valor_final_aval";
                        $idplan = explode("-",$part[0]);
                        $idplan = $idplan[1];
                    }
                    if(eregi("idplan_FG",$p)) {
                        $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        $col = $amoniava.".valor_fg";
                        $idplan = explode("-",$part);
                        $idplan = $idplan[1];
                    }
                    if(eregi("idplan()",$p)) {
                        if($idplanform != "") {
                            $idplan = $idplanform;
                        }
                        $col = "SUM(DISTINCT(".$amoniava.".valor_fg))";
                    }
                    if($wheresql == "") {
                        $w = "WHERE ".$amoniava.".idplanilha='$idplan'";
                    }
                    else {
                        $key = array('idplanilha' => $idplan);
                    }
                }
                if((!eregi("MEDIA",$p) && !eregi("MIN",$p) && !eregi("MAX",$p) && !eregi("SOMA",$p)) && eregi("idaval",$p)) {
                    if(eregi("idaval-",$p)) {
                        $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        $col = $amoniava.".valor_final_aval";
                        $idpart = explode("-",$part[0]);
                        $idaval = $idpart[1];
                        $idplan = $idpart[2];
                    }
                    if(eregi("idaval_FG",$p)) {
                        $colq = "COUNT(DISTINCT(".$amoniava.".id$this->tab)), ";
                        $col = $amoniava.".valor_fg";
                        $idpart = explode("-",$part[0]);
                        $idaval = $idpart[1];
                        $idplan = $idpart[2];
                    }
                    if($wheresql == "") {
                        $w = "WHERE ".$amoniava.".idplanilha='$idplan' AND ".$amoniava.".idaval_plan='$idaval'";
                    }
                    else {
                        $key = array('idplanilha' => $idplan,'idaval_plan' => $idaval);
                    }
                }

                $v = 0;
                if($wheresql != "") {
                    if($key == "") {
                        if($idplan == "") {
                            $w = "WHERE ".implode(" AND ",$wheresql);
                        }
                        else {
                            $w = "WHERE ".$amoniava.".idplanilha='$idplan' AND ".$amoniava.".idaval_plan='$idaval' AND ".implode(" AND ",$wheresql)."";
                        }
                    }
                    else {
                        foreach($key as $kcol => $kval) {
                            if($kval == "") {
                            }
                            else {
                                if(key_exists($kcol,$wheresql)) {
                                }
                                else {
                                    $wheresql[$kcol] = $amoniava.".".$kcol."='".$kval."'";
                                }
                            }
                        }
                        $w = "WHERE ".implode(" AND ",$wheresql);
                    }
                }
                else {
                }

                if($this->idmonitoria == "") {
                    $widmoni = "";
                }
                else {
                    if(isset($this->colcons)) {
                        $widmoni = "AND $this->colcons='$this->idmonitoria'";
                    }
                    else {
                        $widmoni = "AND ".$tabsql->AliasTab(monitoria).".idmonitoria='$this->idmonitoria'";
                    }
                }
                if(eregi("()",$part[0])) {
                    $funcao = str_replace("()","",$part[0]);
                }
                else {
                    $funcao = $part[0];
                }
                if(strstr($p,"MEDIA")) {
                    $dados = $colq.$col." as val";
                    $create = "CREATE TEMPORARY TABLE IF NOT EXISTS tmp_avg (idmonitoria int(7) unsigned zerofill,valor_fg decimal(10,2)) ENGINE=MEMORY";
                    $ecreate = $_SESSION['query']($create) or die (mysql_error());
                    if($this->tab == "monitoria") {
                        $selval = "SELECT $dados FROM $this->tabavalia $amoniava
                                    INNER JOIN $this->tab $amoni ON $amoni.id$this->tab = $amoniava.id$this->tab
                                    INNER JOIN monitoria_fluxo $amonifluxo ON $amonifluxo.idmonitoria = $amoni.idmonitoria
                                    INNER JOIN rel_fluxo $arelfluxo ON $arelfluxo.idrel_fluxo = $amonifluxo.idrel_fluxo
                                    INNER JOIN fluxo $afluxo ON $afluxo.idfluxo = $arelfluxo.idfluxo
                                    INNER JOIN rel_filtros $arelf ON $arelf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $arelf.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    INNER JOIN intervalo_notas $ainter ON $ainter.idindice = $aparam.idindice
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN monitor $amonitor ON $amonitor.idmonitor = $amoni.idmonitor
                                    INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha
                                    LEFT JOIN grupo $agrupo ON $agrupo.idgrupo = $amoniava.idgrupo
                                    LEFT JOIN subgrupo $asubgrupo ON $asubgrupo.idsubgrupo = $amoniava.idsubgrupo
                                    LEFT JOIN pergunta $apergunta ON $apergunta.idpergunta = $amoniava.idpergunta
                                    $w $widmoni";
                    }
                    else {
                        $selval = "SELECT $dados FROM $this->tabavalia $amoniava
                                    INNER JOIN $this->tab $amoni ON $amoni.id$this->tab = $amoniava.id$this->tab
                                    INNER JOIN rel_filtros $arelf ON $arelf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $arelf.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    INNER JOIN intervalo_notas $ainter ON $ainter.idindice = $aparam.idindice
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha
                                    LEFT JOIN grupo $agrupo ON $agrupo.idgrupo = $amoniava.idgrupo
                                    LEFT JOIN subgrupo $asubgrupo ON $asubgrupo.idsubgrupo = $amoniava.idsubgrupo
                                    LEFT JOIN pergunta $apergunta ON $apergunta.idpergunta = $amoniava.idpergunta
                                    $w $widmoni";
                    }
                    $insertval = "INSERT INTO tmp_avg (idmonitoria, valor_fg) $selval";
                    $einsert = $_SESSION['query']($insertval);
                    $seltmp = "SELECT $calc[$funcao](valor_fg) as val FROM tmp_avg";
                    $eselval = $_SESSION['fetch_array']($_SESSION['query']($seltmp)) or die ("erro na query de consulta do valor da avaliação");
                    $valor = $eselval['val'];
                    $set = "SET SQL_SAFE_UPDATES=0;";
                    $eset =$_SESSION['query']($set) or die (mysql_error());
                    $deltmp = "DELETE FROM tmp_avg";
                    $edeltmp = $_SESSION['query']($deltmp);
                }
                else {
                    $dados = $colq.$calc[$funcao]."($col) as val";
                    if($this->tab == "monitoria") {
                        $selval = "SELECT $dados FROM $this->tabavalia $amoniava
                                    INNER JOIN $this->tab $amoni ON $amoni.id$this->tab = $amoniava.id$this->tab
                                    INNER JOIN monitoria_fluxo $amonifluxo ON $amonifluxo.idmonitoria = $amoni.idmonitoria
                                    INNER JOIN rel_fluxo $arelfluxo ON $arelfluxo.idrel_fluxo = $amonifluxo.idrel_fluxo
                                    INNER JOIN fluxo $afluxo ON $afluxo.idfluxo = $arelfluxo.idfluxo
                                    INNER JOIN rel_filtros $arelf ON $arelf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $arelf.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    INNER JOIN intervalo_notas $ainter ON $ainter.idindice = $aparam.idindice
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN monitor $amonitor ON $amonitor.idmonitor = $amoni.idmonitor
                                    INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha
                                    LEFT JOIN grupo $agrupo ON $agrupo.idgrupo = $amoniava.idgrupo
                                    LEFT JOIN subgrupo $asubgrupo ON $asubgrupo.idsubgrupo = $amoniava.idsubgrupo
                                    LEFT JOIN pergunta $apergunta ON $apergunta.idpergunta = $amoniava.idpergunta
                                    $w $widmoni";
                    }
                    else {
                        $selval = "SELECT $dados FROM $this->tabavalia $amoniava
                                    INNER JOIN $this->tab $amoni ON $amoni.id$this->tab = $amoniava.id$this->tab
                                    INNER JOIN rel_filtros $arelf ON $arelf.idrel_filtros = $amoni.idrel_filtros
                                    INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $arelf.idrel_filtros
                                    INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni
                                    INNER JOIN indice $aindice ON $aindice.idindice = $aparam.idindice
                                    INNER JOIN intervalo_notas $ainter ON $ainter.idindice = $aparam.idindice
                                    INNER JOIN operador $aoper ON $aoper.idoperador = $amoni.idoperador
                                    INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $amoni.idsuper_oper
                                    INNER JOIN planilha $aplan ON $aplan.idplanilha = $amoniava.idplanilha
                                    LEFT JOIN grupo $agrupo ON $agrupo.idgrupo = $amoniava.idgrupo
                                    LEFT JOIN subgrupo $asubgrupo ON $asubgrupo.idsubgrupo = $amoniava.idsubgrupo
                                    LEFT JOIN pergunta $apergunta ON $apergunta.idpergunta = $amoniava.idpergunta
                                    $w $widmoni";
                    }
                    $eselval = $_SESSION['fetch_array']($_SESSION['query']($selval)) or die ("erro na query de consulta do valor da avaliação");
                    $valor = $eselval['val'];
                }
                $formula[] = $valor;
                $valor = "";
                $wheresql = $where;
            }
            else {
                if($p == "") {
                }
                else {
                    $formula[] = $p;
                }
            }
        }
        $calculo = trim(implode(" ",$formula));
        eval("\$print = $calculo;");
        return $print;
    }
}

?>
