<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*classe que trabalha para organizar os INNER's criados*/

class Inner {

    public $inners;
    public $newinner;

    function loopsqlinner ($tabloop, $positab, $tabinner, $tabloop2, $inners) {
        //if($l = true) { // continua no while incrementando inners até que a tabverif não esteja presente em outra clasula ON de INNER
            $cinners = count($inners) - 1;
            $alterposi = 0;
            foreach($inners as $k => $n) { // loop pela array $inners para identificar qual string INNER contém o alias da variável $inner para orgnizar o array final
                $tverif = explode("ON", $n);
                $tabv = explode("=", $tverif[1]);
                $tabverifloop = explode(".",$tabv[1]);
                $tverif2 = explode(" ",$tverif[0]);
                $tabverifloop2 = trim($tverif2[3]);
                $tabverifloop = trim($tabverifloop[0]);
                $tabp = explode("=",$tverif[1]);
                $tabp = explode(".",$tabp[1]);
                $tabprox = $tabp[0];
                $positabinner = array_search($n, $this->newinner);
                if($tabloop == $tabverifloop) {
                    if($positab > $positabinner) {
                        if(in_array($n, $this->newinner)) {
                            $ptabloop = array_search($tabinner, $this->newinner);
                            $this->newinner[$positab] = $n;
                            $this->newinner[$positabinner] = $tabinner;
                            $tabloop =  $tabprox;
                            $positab = $positab;
                            $tabinner = $n;
                        }
                        else {
                            //$l = false;
                        }
                    }
                    else {
                        //$l = false;
                    }
                }
                if($tabloop2 == $tabverifloop2) {
                    if($positab < $positabinner) {
                        foreach($this->newinner as $kteste => $teste) {
                            $partv = explode("ON",$teste);
                            $parton = explode("=",$partv[1]);
                            $tabon = explode(".",$parton[1]);
                            $tabverif = trim($tabon[0]);
                            if($tabloop == $tabverif && $positab < $kteste) {
                                $alterposi++;
                            }
                            else {
                            }
                        }
                        if($alterposi >= 1) {
                            reset($this->newinner);
                            $chaveini = key($this->newinner);
                            foreach($this->newinner as $kalt => $inner) {
                                $newposi = $kalt + 1;
                                $this->newinner[$newposi] = $inner;
                            }
                            reset($this->newinner);
                            $prim = key($this->newinner);
                            $this->newinner[$chaveini] = $n;
                        }
                        else {
                            $ptabloop = array_search($tabinner, $this->newinner);
                            $this->newinner[$positab] = $n;
                            $this->newinner[$positabinner] = $tabinner;
                        }
                    }
                    else {
                    }
                }
                if($k == $cinners) {
                    //$l = false;
                }
            }
        //}
    }

    function sqlinner ($vars, $tabpri) {
        //$ntab = count($vars) - 1;
        $this->newinner = array();
        $i = 0;
        foreach($vars as $v) {
            $inners[] = $v; // cria novo array com chave numerada
        }
        foreach($inners as $key => $inner) {
            $pinner = explode("ON", $inner); // cria um array separando o INNER pela clausula ON
            $tinner = explode("=", $pinner[1]); // paga a primeira parte do divisão da estring e classifica como Tabela INNER
            $tinner = explode(".",$tinner[1]); // pega a posição 3 do array para vincular o alias da tabela INNER a variável
            $tinner = trim($tinner[0]);
            if($pinner[1] == "") { // se a parte 1 do array $inners for vazia, coloca a string no inicio do novo array de INNERS
                if(in_array($inner, $this->newinner)) {
                }
                else {
                    $this->newinner[$i] = $inner;
                    $i++;
                }
            }
            if($tinner == $tabpri) {
                if($this->newinner[0] == "") {
                    $this->newinner[$i] = $inner;
                    $i++;
                }
                else {
                    $i++;
                    $newinner = $this->newinner;
                    $this->newinner = array();
                    foreach($newinner as $kinners => $tinners) {
                        if(in_array($tinners,$this->newinner)) {
                        }
                        else {
                            $this->newinner[$kinners + 1] = $tinners;
                            $i--;
                        }
                    }
                    if(in_array($inner,$this->newinner)) {
                        ksort($this->newinner);
                        $end = end($this->newinner);
                        $i = (array_search($end, $this->newinner) + 1);
                    }
                    else {
                        $this->newinner[0] = $inner;
                        ksort($this->newinner);
                        $end = end($this->newinner);
                        $i = (array_search($end, $this->newinner) + 1);
                    }
                }
            }
            if($pinner[1] != "" && $tinner != $tabpri) {
                $notin = 0;
                $pon = explode("=", $pinner[1]); // separa a segunda parte do array $inners separado e separa novamente em outro array pelo sinal de "="
                $ptabon = explode(".", $pon[0]); // pega a segunda parte do array $pon (parte on) e separa pelo "."
                foreach($inners as $k => $n) { // loop pela array $inners para identificar qual string INNER contém o alias da variável $inner para orgnizar o array final
                    $tverif = explode("ON", $n);
                    $tabv = explode(" ", $tverif[0]);
                    $tabverif = trim($tabv[3]);
                    if($tinner == $tabverif) {
                        //$tabloop = $tabloop[0];
                        if(in_array($inner, $this->newinner)) {
                        }
                        else {
                            if(in_array($n, $this->newinner)) {
                                $this->newinner[$i] = $inner;
                                $i++;
                            }
                            else {
                                $this->newinner[$i+1] = $inner;
                            }
                        }
                        if(in_array($n, $this->newinner)) {
                            ksort($this->newinner);
                            $end = end($this->newinner);
                            $i = array_search($end, $this->newinner) + 1;
                        }
                        else {
                            $this->newinner[$i] = $n;
                            ksort($this->newinner);
                            $end = end($this->newinner);
                            $i = array_search($end, $this->newinner) + 1;
                        }
                    }
                    else {
                        $notin++;
                    }
                }
                if($notin == count($inners)) {
                    if(in_array($inner, $this->newinner)) {
                    }
                    else {
                        $this->newinner[$i] = $inner;
                        ksort($this->newinner);
                        $end = end($this->newinner);
                        $i = array_search($end, $this->newinner) + 1;
                    }
                }
            }
        }
        foreach($this->newinner as $kinner => $vinner) {
            $parttab = explode("ON",$vinner);
            $parttabon = explode("=",$parttab[1]);
            $tabloop2 = explode(".",$parttabon[1]);
            $tabloop2 = trim($tabloop2[0]);
            $partinner = explode(" ",$parttab[0]);
            $tabloop1 = trim($partinner[3]);
            $positab = array_search($vinner, $this->newinner);
            $this->loopsqlinner($tabloop1, $positab, $vinner, $tabloop2, $this->newinner);

        }
        ksort($this->newinner);
        $this->newinner = array_unique($this->newinner);
        return $this->newinner;
    }


}
?>
