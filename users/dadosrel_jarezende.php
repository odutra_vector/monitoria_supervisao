<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once($rais.'/monitoria_supervisao/classes/class.relatorios.'.strtolower($_SESSION['nomecli']).'.php');

protegePagina();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/highcharts4.0/js/highcharts.js"></script>
<script src="/monitoria_supervisao/js/highcharts4.0/js/highcharts-3d.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.5/mootools-yui-compressed.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/highcharts4.0/js/modules/exporting.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
</head>
<body>
<div id="relatorio" style="margin: auto; min-width: 1024px;width:1024px;">
<style type="text/css">
.divexport {
    width:150px;
    background-color: #FFBD69;
    border: 1px solid #333;
    font-weight: bold;
    font-size: 12px;
    margin:auto;
    padding: 2px
}

.tabfluxo {
    border-collapse: collapse
}

.tabfluxo tr td {
    border: 1px solid #CCC
}
</style>
<?php

protegepagina();

$rel = new Relatorios();

$rel->Estrutura();

foreach($rel->tabelasrel as $var => $tabelas) {
    $rel->aliasrel[$tabelas] = $rel->AliasTab($tabelas);
}
$rel->aliasrel['status'] = $rel->AliasTab(status);
$rel->aliasrel['definicao'] = $rel->AliasTab(definicao);
$rel->aliasrel['fila_grava'] = $rel->AliasTab(fila_grava);

$selfilt = "SELECT * FROM filtro_nomes WHERE ativo='S'";
$eselfilt = $_SESSION['query']($selfilt) or die ("erro na query de consulta dos filtros");
while($lselfilt = $_SESSION['fetch_array']($eselfilt)) {
    if($_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] == "") {
    }
    else {
        if(strtolower($lselfilt['nomefiltro_nomes']) == "carteira_ccusto" && $_POST['filtro_carteira_ccusto'] != "") {
            $rel->carteira = $_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])];
        }
        $nfiltros[strtolower($lselfilt['nomefiltro_nomes'])] = "rf.id_".strtolower($lselfilt['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])]."'";
        //$rel->vars['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] = $_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])];
    }
}
if($nfiltros == "") {
    $tabuser = str_replace("_", "", $_SESSION['user_tabela']);
    $selperf = "SELECT idrel_filtros FROM ".$tabuser."filtro WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
    $eselperf = $_SESSION['query']($selperf) or die ("erro na query de consulta dos filtros autorizados para o usuário");
    while($lselperf = $_SESSION['fetch_array']($eselperf)) {
        $idsrel[] = $lselperf['idrel_filtros'];
    }
    $rel->vars['idrel_filtros'] = implode(",",$idsrel);
}
else {
    $filtro = implode(" AND ",$nfiltros);
    $tabfiltro = array('user_adm' => 'useradmfiltro','user_web' => 'userwebfiltro');
    $alias = array('user_adm' => 'uaf','user_web' => 'uwf');
    $selrel = "SELECT rf.idrel_filtros,idplanilha_web FROM rel_filtros rf INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
               INNER JOIN ".$tabfiltro[$_SESSION['user_tabela']]." ".$alias[$_SESSION['user_tabela']]." ON ".$alias[$_SESSION['user_tabela']].".idrel_filtros = rf.idrel_filtros
               WHERE $filtro AND ".//cr.ativo='S' AND
               "id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relacionamentos");
    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
        if($_POST['plan'] != "") {
            if(in_array($_POST['plan'],explode(",",$lselrel['idplanilha_web']))) {
                $idsrel[] = $lselrel['idrel_filtros'];
            }
        }
        else {
            $idsrel[] = $lselrel['idrel_filtros'];
        }
    }
    $rel->vars['idrel_filtros'] = implode(",",$idsrel);
}
$nomerel = $_POST['rel'];
foreach($rel->filtros as $kv => $v) {
    if($_POST[$kv] == "") {
    }
    else {
        if($kv == "fg" OR $kv == "fgm") {
            $rel->vars[$v] = "1";
        }
        else {
            if($kv == "dtinictt" OR $kv == "dtfimctt" OR $kv == "dtinimoni" OR $kv == "dtfimmoni") {
                if($kv == "dtinictt" OR $kv == "dtfimctt") {
                    if(key_exists("datactt", $rel->vars)) {
                    }
                    else {
                        $rel->vars['datactt'] = data2banco($_POST['dtinictt']).",".data2banco($_POST['dtfimctt']);
                    }
                }
                if($kv == "dtinimoni" OR $kv == "dtfimmoni") {
                    if(key_exists("data", $rel->vars)) {
                    }
                    else {
                        $rel->vars['data'] = data2banco($_POST['dtinimoni']).",".data2banco($_POST['dtfimmoni']);
                    }
                }
            }
            else {
                $rel->vars[$v] = $_POST[$kv];
            }
        }
    }
}
?>
<br/>
<table width="1024" style="border: 1px solid #CCCCCC;">
    <tr>
        <td style="padding:5px; background-color: #FFBD69" colspan="2"><strong>FILTROS UTILIZADOS</strong></td>
    </tr>
    <?php
    foreach($rel->cabecalho as $fcab => $apres) {
        if($_POST[$fcab]) {
            if($fcab == "plan") {
                $selplan = "SELECT * FROM planilha WHERE idplanilha='$_POST[$fcab]'";
                $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta da planilha");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselplan['descriplanilha']."</td>";
                echo "</tr>";
            }
            if($fcab == "aval") {
                $selaval = "SELECT * FROM aval_plan WHERE idaval_plan='$_POST[$fcab]'";
                $eselaval = $_SESSION['fetch_array']($_SESSION['query']($selaval)) or die (mysql_error());
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselaval['nomeaval_plan']."</td>";
                echo "</tr>";
            }
            if($fcab == "grupo") {
                $selgrupo = "SELECT * FROM grupo WHERE idgrupo='$_POST[$fcab]'";
                $eselgrupo = $_SESSION['fetch_array']($_SESSION['query']($selgrupo)) or die ("erro na query de consulta do nome do grupo");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselgrupo['descrigrupo']."</td>";
               echo "</tr>";
            }
            if($fcab == "sub") {
                $selsub = "SELECT * FROM subgrupo WHERE idsubgrupo='$_POST[$fcab]'";
                $eselsub = $_SESSION['fetch_array']($_SESSION['query']($selsub)) or die ("erro na query de consulta do sub-grupo");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselsub['descrisubgrupo']."</td>";
                echo "</tr>";
            }
            if($fcab == "perg" OR $fcab == "resp") {
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    if($fcab == "perg") {
                        $col = "idpergunta";
                    }
                    if($fcab == "resp") {
                        $col = "idresposta";
                    }
                    $selperg = "SELECT * FROM pergunta WHERE $col='$_POST[$fcab]'";
                    $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da pergunta");
                    if($fcab == "perg") {
                        echo "<td align=\"left\">".$eselperg['descripergunta']."</td>";
                    }
                    if($fcab == "resp") {
                        echo "<td align=\"left\">".$eselperg['descriresposta']."</td>";
                    }
                echo "</tr>";
            }
            if($fcab == "fluxo") {
                $selfluxo = "SELECT * FROM fluxo WHERE idfluxo='$_POST[$fcab]'";
                $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die ("erro na query de consulta do fluxo");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</stronG></td>";
                    echo "<td align=\"left\">".$eselfluxo['nomefluxo']."</td>";
                echo "</tr>";
            }
            if($fcab == "atustatus") {
                $selstatus = "SELECT * FROM status WHERE idstatus='$_POST[$fcab]'";
                $eselstatus = $_SESSION['fetch_array']($_SESSION['query']($selstatus)) or die ("erro na query de consulta do status");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselstatus['nomestatus']."</td>";
                echo "</tr>";
            }
            if($fcab == "indice") {
                $selindice = "SELECT * FROM indice WHERE idindice='$_POST[$fcab]'";
                $eselindice = $_SESSION['fetch_array']($_SESSION['query']($selindice)) or die ("erro na query de consulta do indice");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselindice['nomeindice']."</td>";
                 echo "</tr>";
            }
            if($fcab == "intervalo") {
                $selinter = "SELECT * FROM intervalo_notas WHERE idintervalo_notas='$_POST[$fcab]'";
                $eselinter = $_SESSION['fetch_array']($_SESSION['query']($selinter)) or die ("erro na query de consulta do intervalo da nota consultada");
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">".$eselinter['numini']." a ".$eselinter['numfim']."</td>";
                 echo "</tr>";
            }
            if($fcab == "dtinimoni" OR $fcab == "dtfimmoni") {
                if($_POST['dtinimoni'] != "" && $_POST['dtfimmoni'] != "") {
                    if(in_array("data",$vars)) {
                    }
                    else {
                        $vars[] = "data";
                        echo "<tr>";
                        echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                        echo "<td align=\"left\">".$_POST['dtinimoni']." a ".$_POST['dtfimmoni']."</td>";
                        echo "</tr>";
                    }
                }
                else {
                }
            }
            if($fcab == "dtinictt" OR $fcab == "dtfimctt") {
                if($_POST['dtinictt'] != "" OR $_POST['dtfimctt'] != "") {
                    if(in_array("datactt",$vars)) {
                    }
                    else {
                        $vars[] = "datactt";
                        echo "<tr>";
                        echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                        echo "<td align=\"left\">".$_POST['dtinictt']." a ".$_POST['dtfimctt']."</td>";
                        echo "</tr>";
                    }
                }
                else {
                }
            }
            if($fcab == "idmonitoria" OR $fcab == "super_oper" OR $fcab == "oper" OR $fcab == "nomemonitor" OR $fcab == "fg") {
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">$_POST[$fcab]</td>";
                 echo "</tr>";
            }
            if($fcab == "idalerta") {
                echo "<tr>";
                    echo "<td style=\"padding:5px; width:200px;\"><strong>$apres</strong></td>";
                    echo "<td align=\"left\">";
                    $selnalerta = "SELECT nomealerta FROM alertas WHERE idalertas='".$_POST[$fcab]."'";
                    $enalerta = $_SESSION['fetch_array']($_SESSION['query']($selnalerta)) or die (mysql_error());
                    echo $enalerta['nomealerta']."</td>";
                 echo "</tr>";
            }
        }
        else {
        }
    }
    $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel";
    $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta do filtro");
    while($lselfiltros = $_SESSION['fetch_array']($eselfiltros)) {
        if($_POST['filtro_'.  strtolower($lselfiltros['nomefiltro_nomes'])] != "") {
            $fdados = $_POST['filtro_'.  strtolower($lselfiltros['nomefiltro_nomes'])];
            $seldados = "SELECT * FROM filtro_dados WHERE idfiltro_dados='$fdados'";
            $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta do nome do filtro escolhido");
            echo "<tr>";
                echo "<td style=\"padding:5px; width:200px;\"><strong>".$lselfiltros['nomefiltro_nomes']."</strong></td>";
                echo "<td>".$eseldados['nomefiltro_dados']."</td>";
            echo "</tr>";
        }
    }
    ?>
</table><br/>
<?php
$rel->tipo = $_POST['tipo'];
$selfmax = "SELECT nomefiltro_nomes as nome,idfiltro_nomes as id FROM filtro_nomes ORDER BY nivel DESC LIMIT 1";
$emax = $_SESSION['fetch_array']($_SESSION['query']($selfmax)) or die (mysql_error());
$rel->filtrom = "id_".strtolower($emax['nome'])."='".$_POST['filtro_'.strtolower($emax['nome'])]."'";
if($idsrel == "") {
    ?>
    <table width="100%">
        <tr style="background-color:#CCCCCC">
                <td align="center" style="color:#CC0000"><strong>O CRUZAMENTO DE FILTROS UTILIZADOS NÃO EXISTE OU NÃO ESTÁ DISPONÍVEL AO USUÁRIO</strong></td>
        </tr>
    </table>
    <?php
}
else {
        $rel->Geradados();
        $rel->coreps();
        if($rel->check == 0) {
            ?>
            <table width="100%">
                <tr style="background-color:#CCCCCC">
                    <td align="center" style="color:#CC0000"><strong>NÃO FOI ENCONTRADA NENHUMA INFORMAÇÃO COM OS FILTROS UTILIZADOS</strong></td>
                </tr>
            </table>
            <?php
        }
        else {
            ?>
            <table width="1024px">
                    <tr>
                        <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=<?php echo $rel->tipo?>"><div class="divexport">EXCEL</div></a></td>
                        <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome=<?php echo $rel->tipo?>"><div class="divexport">WORD</div></a></td>
                        <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome=<?php echo $rel->tipo?>"><div class="divexport">PDF</div></a></td>
                    </tr>
            </table><br /><br/>
            <?php
            if($rel->tipo == "operador") {
                foreach($rel->dadosoper as $idoper => $dados) {
                    unset($multicanais);
                ?>
                <div style="page-break-before: always"></div>
                <div style="width: 1020px; border: 1px solid #CCCCCC; margin-bottom: 10px">
                    <div id="dadosrel" style="padding:10px; margin-bottom: 10px">
                        <table width="100%">
                            <tr>
                                <td colspan="6"><div style=" font-weight: bold; float: left;">FORMULÁRIO DE AVALIAÇÃO INDIVIDUAL - NEGOCIADOR</div><div style="float: right"><img src="/monitoria_supervisao/images/img_logo.jpg" height="39" /></div></td>
                            </tr>
                            <tr style="height: 30px"></tr>
                            <tr style="font-weight: bold; font-size: 12px; background-color: #CCC">
                                <td style="padding: 3px" >NEGOCIADOR</td>
                                <td style="padding: 3px" align="center">DATA ADMISSÃO</td>
                                <td style="padding: 3px" align="center">TIPO CONTRATO</td>
                                <td style="padding: 3px" align="center">TURNO</td>
                                <td style="padding: 3px" align="center">MÉDIA ANTERIOR</td>
                                <td style="padding: 3px" align="center">MÉDIA ATUAL</td>
                            </tr>
                            <tr>
                                <?php
                                foreach($dados as $chave => $dado) {
                                    if($chave == "operador") {
                                        $posi = "";
                                    }
                                    else {
                                        $posi = "align=\"center\"";
                                    }
                                    ?>
                                    <td <?php echo $posi;?>><?php echo $dado;?></td>
                                    <?php
                                }
                                ?>
                            </tr>
                        </table>
                    </div>
                    <div id="planilhas" style="padding:10px; margin-bottom: 10px">
                        <?php
                        $dadosaval = array('pa_gravacao' => 'PA GRAVACAO','idmonitoria' => 'ID','valor_fg' => 'NOTA','data' => 'data monitoria','datactt' => 'data contato','narquivo' => 'Nome do Arquivo de Gravação','tmpaudio' => 'Tempo de Ligação','super_oper' => 'Supervisor','nomemonitor' => 'Responsável pela Avaliação','idrel_filtros' => 'Contratante/ Carteira');
                        foreach($rel->idmonitorias[$idoper] as $idplan => $idsmoni) {
                            ?>
                            <table width="100%" style="font-size: 12px">
                            <?php
                            $selplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$idplan'";
                            $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die (mysql_error());
                            ?>
                            <tr>
                                <td colspan="<?php echo 1 + count($idsmoni);?>" align="center" style="font-size: 12px; font-weight: bold; background-color: #CCC; padding: 3px">PLANILHA - <?php echo $eselplan['descriplanilha'];?></td>
                            </tr>
                            <tr bgcolor="#CCC">
                                <td width="40%" style="padding: 3px">DADOS AVALIAÇÃO</td>
                                <?php
                                if($idplan == "0003") {
                                    for($i = 1; $i <= count($idsmoni);$i++) {
                                        ?>
                                        <td align="center" style="padding: 3px">RECADO <?php echo $i;?></td>
                                        <?php
                                    }
                                }
                                else {
                                    for($i = 1; $i <= count($idsmoni);$i++) {
                                        ?>
                                        <td align="center" style="padding: 3px" width="<?php echo 60 / count($idsmoni);?>%">NEG <?php echo $i;?></td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php
                            foreach($idsmoni as $iddados) {
                                $seldados = "SELECT idmonitoria,nomemonitor,super_oper,narquivo,pa_gravacao,m.tmpaudio,m.idrel_filtros,m.data,m.datactt,m.idoperador FROM monitoria m"
                                        . " INNER JOIN fila_grava fg on fg.idfila_grava = m.idfila"
                                        . " INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper"
                                        . " INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor"
                                        . " WHERE idmonitoria='$iddados'";
                                $eseldados = $_SESSION['query']($seldados) or die (mysql_error());
                                while($lseldados = $_SESSION['fetch_array']($eseldados)) {
                                    if($rel->notas_obs[$lseldados['idoperador']][$iddados]['dialogo_multicanais'] != '') {
                                        $multicanais[$iddados] = $rel->notas_obs[$lseldados['idoperador']][$iddados]['dialogo_multicanais'];
                                    }
                                    foreach($dadosaval as $col  => $nome) {
                                        if($col == "idrel_filtros") {
                                            $montadados[$nome][$iddados] = nomevisu($lseldados[$col]);
                                        }
                                        else {
                                            if(strstr($col, "data")) {
                                                $montadados[$nome][$iddados] = banco2data($lseldados[$col]);
                                            }
                                            else {
                                                if($col == "valor_fg") {
                                                    $montadados[$nome][$iddados] = $rel->notas_obs[$lseldados['idoperador']][$iddados]['nota'];
                                                }
                                                else {
                                                    $montadados[$nome][$iddados] = $lseldados[$col];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            foreach($montadados as $ncol => $dadosmoni) {
                                ?>
                                <tr>
                                    <td><?php echo $ncol;?></td>
                                <?php
                                foreach($idsmoni as $idd) {
                                    ?>
                                    <td align="center" style="padding: 3px"><?php echo $dadosmoni[$idd];?></td>
                                    <?php
                                }
                                ?>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr style=" height: 30px"></tr>
                            <?php

                            $selplan = "SELECT count(g.idgrupo) as cgrupo,p.idgrupo, descrigrupo,filtro_vinc,valor_grupo FROM planilha p
                                        INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                                        WHERE idplanilha='$idplan' GROUP BY p.idgrupo ORDER BY p.posicao";
                            $eselplan = $_SESSION['query']($selplan) or die (mysql_error());
                            while($lselplan = $_SESSION['fetch_array']($eselplan)) {
                                $checktab = $_SESSION['fetch_array']($_SESSION['query']("SELECT count(idrel) as cgrupo FROM grupo WHERE tab='S' AND idgrupo=".$lselplan['idgrupo']));
                                if($checktab['cgrupo'] < $lselplan['cgrupo']) {
                                ?>
                                <tr>
                                    <td style="background-color: #C9EAFF; padding: 5px" colspan="<?php echo 1 + count($idsmoni);?>"><?php echo $lselplan['descrigrupo']." peso(".$lselplan['valor_grupo'].")";?></td>
                                </tr>
                                    <?php
                                    if($lselplan['filtro_vinc'] == "P") {
                                        $selperg = "SELECT descripergunta,idrel FROM grupo g
                                                    INNER JOIN pergunta p ON p.idpergunta = g.idrel
                                                    WHERE idgrupo = '".$lselplan['idgrupo']."' AND g.tab='N' GROUP BY idrel ORDER BY g.posicao";
                                        $eselperg = $_SESSION['query']($selperg) or die (mysql_error());
                                        while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                                            ?>
                                            <tr>
                                                <td style=" padding: 3px"><?php echo $lselperg['descripergunta'];?></td>
                                            <?php
                                            foreach($idsmoni as $idresp) {
                                                $selresp = "SELECT descriresposta FROM moniavalia m
                                                            INNER JOIN pergunta p ON p.idresposta = m.idresposta
                                                            WHERE idmonitoria='$idresp' and m.idpergunta='".$lselperg['idrel']."' GROUP BY p.idresposta";
                                                $eselresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                $nresp = $_SESSION['num_rows']($eselresp);
                                                if($nresp >= 1) {
                                                    ?>
                                                    <td align="center">
                                                        <?php
                                                        while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                                            echo $lselresp['descriresposta']."<br/>";
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php
                                                }
                                                else {
                                                    ?>
                                                    <td></td>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                                <tr>
                                    <td style=" background-color: #CCC; font-weight: bold">NOTA</td>
                                    <?php
                                    foreach($idsmoni as $idnota) {
                                        $seloper = "SELECT idoperador FROM monitoria WHERE idmonitoria='$idnota'";
                                        $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die (mysql_error());
                                        ?>
                                        <td align="center" style=" font-weight: bold; background-color: #CCC"><?php echo $rel->notas_obs[$eseloper['idoperador']][$idnota]['nota'];?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>

                            </table></br>
                            <?php
                            if(count($multicanais) > 0) {
                            ?>
                            <table>
                                <tr>
                                    <td style="font-weight: bold">DIALOGO MULTICANAIS:<br/></td>
                                </tr>
                                <?php
                                foreach($multicanais as $idmoni => $text) {
                                ?>
                                <tr>
                                    <td style="margin-bottom: 10px">
                                        <span>ID - <?php echo $idmoni;?></span><br/>
                                        <div style="width: 98%; text-align: justify; border: 1px solid #CCC; padding: 10px">
                                            <?php echo $text;?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </table>
                            <?php
                            }

                        }
                        ?>
                    </div>
                    <div id="obs" style="padding:10px; margin-bottom: 10px">
                        <span style=" font-size: 12px; font-weight: bold">PLANO DE AÇÃO</span><br/>
                        <div style="width: 98%; border: 1px solid #CCC; margin-bottom: 15px; padding: 7px">
                            <textarea style="border:0px; width: 100%; height: 40px" name="planoacao"></textarea>
                        </div>
                    </div>
                    <div id="feedback" style="padding:10px; margin-bottom: 10px">
                        <?php
                        $indice[0] = "PARABÉNS pelo ótimo desempenho. Obrigado por fazer cobrança com seriedade. Lembre-se! Mantenha o seu padrão de RESULTADO & QUALIDADE.";
                        $indice[1] = "CUMPRIU com os procedimentos ou processos necessários para a sua ação de cobrança. Aperfeiçoe seu trabalho para se SUPERAR!";
                        $indice[2] = "Cumpriu de forma PARCIAL com os procedimentos ou processos necessários para sua ação de cobrança. ANALISE o parecer técnico, trace novas técnicas para atingir as expectativas.";
                        $indice[3] = "Não cumpriu com os procedimentos ou processos necessários para sua ação de cobrança. REVEJA o parecer técnico, aproveite o PLANO DE AÇÃO e identifique novas técnicas para seu APERFEIÇOAMENTO.";
                        foreach($rel->indice[$idoper] as $idindice) {
                            $valores = array();
                            ?>
                            <table width="90%" align="center">
                                <tr>
                                <?php
                                $selnotas = "SELECT * FROM intervalo_notas WHERE idindice='$idindice' ORDER BY numini DESC";
                                $eselindice = $_SESSION['query']($selnotas) or die (mysql_error());
                                while($lindice = $_SESSION['fetch_array']($eselindice)) {
                                    $valores[] = $lindice['numini'];
                                    ?>
                                    <td style=" background-color: #999; padding: 10px; font-size: 12px; font-weight: bold; text-align: center;"><?php echo $lindice['numini']." a ".$lindice['numfim'];?></td>
                                    <?php
                                }
                                ?>
                                </tr>
                                <tr>
                                <?php
                                foreach($valores as $kval => $val) {
                                    ?>
                                    <td style=" text-align: center; height: 70px; padding: 10px; background-color: #EAEAEA"><?php echo $indice[$kval];?></td>
                                    <?php
                                }
                                ?>
                                </tr>
                            </table></br>
                            <?php
                        }
                        ?>
                        <table width="70%">
                            <tr>
                                <td style=" font-size: 12px">DATA ____/____/_______</td>
                            </tr>
                            <tr height="40px">
                                <td style=" font-size: 15px; font-weight: bold">Assinaturas</td>
                            </tr>
                            <tr height="50px">
                                <td>_____________________________________________</br>Negociador</td>
                                <td>_____________________________________________</br>Supervidor(a)</td>
                            </tr>
                            <tr height="50px">
                                <td>_____________________________________________</br>Monitor(a)</td>
                                <td>_____________________________________________</br>Supervidor(a)</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php
                }
                ?>
                <div style="page-break-before: always"></div>
                <?php
                foreach($rel->dadosoper as $idoperob => $dadosob) {
                        unset($multicanais);
                        ?>
                        <div style="margin-top: 15px">
                            <table width="100%">
                                <tr>
                                    <td colspan="6"><div style=" font-weight: bold; float: left;">DOSSIÊ - NEGOCIADOR</div><div style="float: right"><img src="/monitoria_supervisao/images/img_logo.jpg" height="39" /></div></td>
                                </tr>
                                <tr style="height: 30px"></tr>
                                <tr style="font-weight: bold; font-size: 12px; background-color: #CCC">
                                    <td style="padding: 3px" >NEGOCIADOR</td>
                                    <td style="padding: 3px" align="center">DATA ADMISSÃO</td>
                                    <td style="padding: 3px" align="center">TIPO CONTRATO</td>
                                    <td style="padding: 3px" align="center">TURNO</td>
                                    <td style="padding: 3px" align="center">MÉDIA ANTERIOR</td>
                                    <td style="padding: 3px" align="center">MÉDIA ATUAL</td>
                                </tr>
                                <tr>
                                    <?php
                                    foreach($dadosob as $chave => $dado) {
                                        if($chave == "operador") {
                                            $posi = "";
                                        }
                                        else {
                                            $posi = "align=\"center\"";
                                        }
                                        ?>
                                        <td <?php echo $posi;?>><?php echo $dado;?></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </table>
                        </div>
                        <?php
                        $dadosaval = array('pa_gravacao' => 'PA GRAVACAO','idmonitoria' => 'ID','valor_fg' => 'NOTA','data' => 'data monitoria','datactt' => 'data contato','narquivo' => 'Nome do Arquivo de Gravação','tmpaudio' => 'Tempo de Ligação','super_oper' => 'Supervisor','nomemonitor' => 'Responsável pela Avaliação','idrel_filtros' => 'Contratante/ Carteira');
                        foreach($rel->idmonitorias[$idoperob] as $kplan => $id) {
                        ?>
                        <div  style="padding:10px; margin-bottom: 10px">
                            <table width="100%" style="font-size: 12px">
                            <?php
                            $selplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$kplan'";
                            $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die (mysql_error());
                            ?>
                            <tr>
                                <td colspan="<?php echo 1 + count($id);?>" align="center" style="font-size: 12px; font-weight: bold; background-color: #CCC; padding: 3px">PLANILHA - <?php echo $eselplan['descriplanilha'];?></td>
                            </tr>
                            <tr bgcolor="#CCC">
                                <td width="40%" style="padding: 3px">DADOS AVALIAÇÃO</td>
                                <?php
                                if($kplan == "0003") {
                                    for($i = 1; $i <= count($id);$i++) {
                                        ?>
                                        <td align="center" style="padding: 3px">RECADO <?php echo $i;?></td>
                                        <?php
                                    }
                                }
                                else {
                                    for($i = 1; $i <= count($id);$i++) {
                                        ?>
                                        <td align="center" style="padding: 3px" width="<?php echo 60 / count($idsmoni);?>%">NEG <?php echo $i;?></td>
                                        <?php
                                    }
                                }
                                ?>
                            </tr>
                            <?php
                            foreach($id as $iddados) {
                                $seldados = "SELECT idmonitoria,nomemonitor,super_oper,narquivo,pa_gravacao,m.tmpaudio,m.idrel_filtros,m.data,m.datactt,m.idoperador FROM monitoria m"
                                        . " INNER JOIN fila_grava fg on fg.idfila_grava = m.idfila"
                                        . " INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper"
                                        . " INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor"
                                        . " WHERE idmonitoria='$iddados'";
                                $eseldados = $_SESSION['query']($seldados) or die (mysql_error());
                                while($lseldados = $_SESSION['fetch_array']($eseldados)) {
                                    foreach($dadosaval as $col  => $nome) {
                                        if($col == "idrel_filtros") {
                                            $montadados[$nome][$iddados] = nomevisu($lseldados[$col]);
                                        }
                                        else {
                                            if(strstr($col, "data")) {
                                                $montadados[$nome][$iddados] = banco2data($lseldados[$col]);
                                            }
                                            else {
                                                if($col == "valor_fg") {
                                                    $montadados[$nome][$iddados] = $rel->notas_obs[$lseldados['idoperador']][$iddados]['nota'];
                                                }
                                                else {
                                                    $montadados[$nome][$iddados] = $lseldados[$col];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            foreach($montadados as $ncol => $dadosmoni) {
                                ?>
                                <tr>
                                    <td><?php echo $ncol;?></td>
                                <?php
                                foreach($id as $idd) {
                                    ?>
                                    <td align="center" style="padding: 3px"><?php echo $dadosmoni[$idd];?></td>
                                    <?php
                                }
                                ?>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr style=" height: 30px"></tr>
                            </table>
                        </div>
                        <?php
                        foreach($id as $idob) {
                            if($rel->notas_obs[$idoperob][$idob]['dialogo_multicanais'] != '') {
                                $multicanais[$idob] = $rel->notas_obs[$idoperob][$idob]['dialogo_multicanais'];
                            }
                        }
                        if(isset($multicanais)) {
                        ?>
                        <table width="100%">
                            <tr><td colspan="<?php echo 1 + count($id);?>" style=" font-weight: bold">DIALOGO MULTICANAIS:</td></tr>
                            <?php
                            foreach($multicanais as $idmoni => $data) {
                            ?>
                            <tr><td><span style="font-weight: bold; font-size: 12px">ID - <?php echo $idmoni; ?></span></td></tr>
                            <tr>
                                <td>
                                    <div style="width: 98%; border: 1px solid #CCC; margin-bottom: 15px; padding: 7px">
                                        <?php echo $data; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                        </table>
                        <?php
                        }
                        ?>
                        <table width="100%">
                            <tr><td colspan="<?php echo 1 + count($id);?>" style=" font-weight: bold">OBSERVAÇÕES - <?php echo $dadosob['operador'];?></td></tr>
                            <tr height="10px"></tr>
                            <tr>
                                <td>
                                <?php
                                $i = 0;
                                foreach($id as $idob) {
                                $i++;
                                ?>
                                <div>
                                    <?php
                                    if($kplan == "0003") {
                                        ?>
                                        <span style="font-weight: bold; font-size: 12px">RECADO <?php echo $idob;?></span>
                                        <?php
                                    }
                                    else {
                                        ?>
                                        <span style="font-weight: bold; font-size: 12px">NEG <?php echo $idob;?></span>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div style="width: 98%; border: 1px solid #CCC; margin-bottom: 15px; padding: 7px">
                                    <?php
                                    if($rel->notas_obs[$idoperob][$idob]['obs'] == "") {
                                    }
                                    else {
                                        echo $rel->notas_obs[$idoperob][$idob]['obs'];
                                    }
                                    ?>
                                </div>
                                <?php
                                }
                                ?>
                                </td>
                            </tr>
                        </table><br/><hr/><br/>
                        <?php
                    }
                    ?>
                    <div style="page-break-before: always"></div>
                    <?php
                }
            }

            if($rel->tipo == "histoperador") {
                $dados .= "<table id=\"taboperador\">";
                    $dados .= "<tr bgcolor=\"#2F54EA\" style=\"color:#FFF; font-weight: bold\">";
                        $dados .= "<td colspan=\"5\" align=\"center\">DADOS</td>";
                        foreach($rel->meses as $mes) {
                            $dados .= "<td align=\"center\">$mes</td>";
                        }
                    $dados .= "</tr>";
                    $dados .= "<tr bgcolor=\"#2F54EA\" style=\"color:#FFF;font-weight: bold\">";
                        $dados .= "<td>Colaborador</td>";
                        $dados .= "<td align=\"center\">Supervisor</td>";
                        $dados .= "<td align=\"center\">Monitor</td>";
                        $dados .= "<td align=\"center\">Carteira</td>";
                        $dados .= "<td align=\"center\">Data de Admissão</td>";
                        foreach($rel->meses as $mes) {
                            $dados .= "<td align=\"center\">Nota</td>";
                        }
                    $dados .= "</tr>";
                    foreach($rel->histoperador as $oper => $dd) {
                        $dados .= "<tr style=\"font-size: 12px\">";
                            $dados .= "<td>".$rel->dadosoper[$oper]['nome']."</td>";
                            $dados .= "<td>".$rel->dadosoper[$oper]['supervisor']."</td>";
                            $dados .= "<td>".$rel->dadosoper[$oper]['monitor']."</td>";
                            $dados .= "<td>".$rel->dadosoper[$oper]['carteira']."</td>";
                            $dados .= "<td>".$rel->dadosoper[$oper]['admissao']."</td>";
                            foreach($rel->meses as $mes) {
                                if($rel->indice[$oper][$mes] == "") {
                                    $cor = "";
                                    $nome = "--";
                                }
                                else {
                                    $selindice = "SELECT i.idindice,numini,numfim, count(*) as r FROM indice i"
                                        . " INNER JOIN intervalo_notas it ON it.idindice = i.idindice"
                                        . " INNER JOIN param_moni pm ON pm.idindice = i.idindice"
                                        . " INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni"
                                        . " WHERE idrel_filtros='".$rel->indice[$oper][$mes]."' AND '$dd[$mes]' between numini AND numfim";
                                    $eselindice = $_SESSION['fetch_array']($_SESSION['query']($selindice)) or die (mysql_error());
                                    if($eselindice['idindice'] == "01") {
                                        if($eselindice['numini'] == 0 && $eselindice['numfim'] == 80.99) {
                                            $cor = "bgcolor=\"EA8F8F\"";
                                            $nome = "ABAIXO DO ESPERADO";
                                        }
                                        if($eselindice['numini'] == 81 && $eselindice['numfim'] == 89.10) {
                                            $cor = "bgcolor=\"A7EA85\"";
                                            $nome = "ESPERADO";
                                        }
                                        if($eselindice['numini'] == 89.11 && $eselindice['numfim'] == 110.00) {
                                            $cor = "bgcolor=\"85C6FF\"";
                                            $nome = "ACIMA DO ESPERADO";
                                        }
                                    }
                                    if($eselindice['idindice'] == "02") {
                                        if((float)$eselindice['numini'] == 0 && (float)$eselindice['numfim'] == 49.99) {
                                            $cor = "bgcolor=\"EA212E\"";
                                            $nome = "4 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 50 && (float)$eselindice['numfim'] == 74.99) {
                                            $cor = "bgcolor=\"EAE04D\"";
                                            $nome = "3 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 75 && (float)$eselindice['numfim'] == 89.99) {
                                            $cor = "bgcolor=\"82BA58\"";
                                            $nome = "2 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 90 && (float)$eselindice['numfim'] == 100) {
                                            $cor = "bgcolor=\"5EA4EB\"";
                                            $nome = "1 Quartil";
                                        }
                                    }
                                    if($eselindice['idindice'] == "03") {
                                        if((float)$eselindice['numini'] == 0 && (float)$eselindice['numfim'] == 25) {
                                            $cor = "bgcolor=\"EA212E\"";
                                            $nome = "4 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 25.10 && (float)$eselindice['numfim'] == 50) {
                                            $cor = "bgcolor=\"EAE04D\"";
                                            $nome = "3 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 50.10 && (float)$eselindice['numfim'] == 75) {
                                            $cor = "bgcolor=\"82BA58\"";
                                            $nome = "2 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 75.10 && (float)$eselindice['numfim'] == 100) {
                                            $cor = "bgcolor=\"5EA4EB\"";
                                            $nome = "1 Quartil";
                                        }
                                    }
                                    if($eselindice['idindice'] == "04") {
                                        if((float)$eselindice['numini'] == 0 && (float)$eselindice['numfim'] == 49.99) {
                                            $cor = "bgcolor=\"EA212E\"";
                                            $nome = "4 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 50 && (float)$eselindice['numfim'] == 74.99) {
                                            $cor = "bgcolor=\"EAE04D\"";
                                            $nome = "3 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 75 && (float)$eselindice['numfim'] == 84.99) {
                                            $cor = "bgcolor=\"82BA58\"";
                                            $nome = "2 Quartil";
                                        }
                                        if((float)$eselindice['numini'] == 85 && (float)$eselindice['numfim'] == 100) {
                                            $cor = "bgcolor=\"5EA4EB\"";
                                            $nome = "1 Quartil";
                                        }
                                    }
                                }
                                $dados .= "<td align=\"center\">".str_replace(".", ",", $dd[$mes])."</td>";
                            }
                        $dados .= "</tr>";
                    }
                $dados .= "</table>";
                $_SESSION['dadosexp'] = $dados;
                echo $dados;

            }

            if($rel->tipo == "mensal" OR $rel->tipo == "semanal") {
            ?>
                <!-- gráfico comparativo resposta grupo específico "NEGOCIAÇÃO, RECADO E CONTATO"-->
                <?php
                if($rel->tipo == "mensal" && $rel->vars['idplanilha'] != "") {
                    ksort($rel->semanas);
                    $nplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='".$rel->vars['idplanilha']."'";
                    $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die (mysql_error());
                    foreach($rel->gruposespec['acompanhamento'] as $id => $dados) {
                    if($id != 1) {
                        $ngrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='$id'";
                        $engrupo = $_SESSION['fetch_array']($_SESSION['query']($ngrupo));
                    }
                    $print = array();
                    $printresp = array();
                    ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'grafcompgrupo<?php echo $id;?>',
                                    defaultSeriesType: 'column',
                                    marginTop: 120,
                                    marginBottom: 130,
                                    width: 1024,
                                    height: 600
                                },
                                title: {
                                    text: 'Acompanhamento dos apontamentos - <?php echo $enplan['descriplanilha']." - ".$engrupo['descrigrupo'];?>'
                                },
                                subtitle: {
                                    text: '(não conformidade)/reincidentes – mês a mês',
                                    x: -20,
                                    style: {
                                        color: '#000000',
                                        fontWeight: 'bold'
                                    }
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $datas = explode(",",$rel->vars['datactt']); echo banco2data($datas[0])."-".  banco2data($datas[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: {
                                    labels: {
                                        style: {
                                            color: '#000000'
                                        },
                                        y: 30
                                    },
                                    categories: [
                                        <?php
                                        echo "'".implode("','",array_values($dados['perguntas']))."'";
                                        ?>
                                    ]
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: 'PERCENTUAL(%)'
                                    }
                                },
                                legend: {
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:50,
                                    floating: true,
                                    shadow: true
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '12px'
                                            }
                                        },
                                        groupPadding: 0.10
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                series: [
                                    <?php
                                    foreach($dados['comparativo'] as $mes => $valor) {
                                        $print[] = "{name:'$mes',data:[".implode(",",  array_values($valor))."]}";
                                    }
                                    echo implode(",",$print);
                                    ?>
                                    ],
                                    exporting: {
                                        enableImages: true,
                                        width: 1280
                                    }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                            });
                    </script>

                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'grafrespostas<?php echo $id;?>',
                                    type: 'column',
                                    marginTop: 120,
                                    marginBottom: 130,
                                    width: 1024,
                                    height: 600
                                },
                                title: {
                                    text: 'Acompanhamento <?php echo $enplan['descriplanilha'];?> - <?php echo $engrupo['descrigrupo'];?> (Conforme VS Não Conforme)'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $datas = explode(",",$rel->vars['datactt']); echo banco2data($datas[0])."-".  banco2data($datas[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: {
                                    labels: {
                                        style: {
                                            color: '#000000'
                                        },
                                        y: 30
                                    },
                                    categories: [
                                        <?php
                                        $perguntas = array_values($dados['perguntas']);
                                        echo "'".implode("','",array_values($dados['perguntas']))."'";
                                        ?>
                                    ]
                                },
                                yAxis: {
                                    min: 0,
                                    title: {
                                        text: 'QUANTIDADE'
                                    }
                                },
                                legend: {
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:50,
                                    floating: true,
                                    shadow: true
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '12px'
                                            }
                                        },
                                        groupPadding: 0.10
                                    },
                                    column: {
                                        stacking: 'normal',
                                    }
                                },
                                tooltip: {
                                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                                    footerFormat: '</table>',
                                    shared: true,
                                    useHTML: true
                                },
                                series: [
                                    <?php
                                    $l = 0;
                                    foreach($rel->respostaperg[$id] as $resp => $qtde) {
                                        if($l == 0) {
                                            ?>
                                            {
                                                name:'<?php echo $resp;?>',
                                                color:'<?php
                                                if($resp == "correta" or $resp == "Correta" or $resp == "CORRETA" OR $resp == "correto" or $resp == "Correto" or $resp == "CORRETO") {
                                                    echo "#2FD929";
                                                }
                                                if($resp == "Incorreta" or $resp == "incorreta" or $resp == "INCORRETA" OR $resp == "Incorreto" or $resp == "incorreto" or $resp == "INCORRETO") {
                                                    echo "#EB6E6E";
                                                }
                                                if($resp == "Parcial" or $resp == "parcial" or $resp == "PARCIAL") {
                                                    echo "#E5EB3B";
                                                }
                                                if(strstr($resp,"NA")) {
                                                    echo "#7644EB";
                                                }
                                                ?>',
                                                data:[<?php echo implode(",",$qtde);?>],
                                                stack:'perguntas'
                                            }
                                            <?php
                                        }
                                        else {
                                            ?>
                                            ,
                                            {
                                                name:'<?php echo $resp;?>',
                                                color:'<?php
                                                if($resp == "correta" or $resp == "Correta" or $resp == "CORRETA" OR $resp == "correto" or $resp == "Correto" or $resp == "CORRETO") {
                                                    echo "#2FD929";
                                                }
                                                if($resp == "Incorreta" or $resp == "incorreta" or $resp == "INCORRETA" OR $resp == "Incorreto" or $resp == "incorreto" or $resp == "INCORRETO") {
                                                    echo "#EB6E6E";
                                                }
                                                if($resp == "Parcial" or $resp == "parcial" or $resp == "PARCIAL") {
                                                    echo "#E5EB3B";
                                                }
                                                if(strstr($resp,"NA")) {
                                                    echo "#7644EB";
                                                }
                                                ?>',
                                                data:[<?php echo implode(",",$qtde);?>],
                                                stack:'perguntas'
                                            }
                                            <?php
                                        }
                                        $l++;
                                    }
                                    ?>
                                    ],
                                    exporting: {
                                        enableImages: true,
                                        width: 1280
                                    }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                        })
                    </script>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                            chart = new Highcharts.Chart({
                               chart: {
                                    renderTo: 'grafitens<?php echo $id;?>',
                                    defaultSeriesType: 'area',
                                    marginTop: 120,
                                    marginBottom: 130,
                                    width: 1024,
                                    height: 600
                                },
                                title: {
                                    text: 'Ofensores <?php echo $engrupo['descrigrupo'];?> – Apontamentos de <?php echo substr(banco2data($datas[1]),3,2)."/".substr(banco2data($datas[1]),6,4);?>'
                                },
                                xAxis: {
                                    categories:[
                                        <?php
                                        echo "'".implode("','",array_values($dados['perguntas']))."'";
                                        ?>
                                    ],
                                    labels: {
                                        formatter: function() {
                                            return this.value; // clean, unformatted number for year
                                        },
                                        style: {
                                            color: '#000000'
                                        },
                                        y: 30
                                    }
                                },
                                yAxis: {
                                    title: {
                                        text: 'QUANTIDADE'
                                    },
                                    labels: {
                                        formatter: function() {
                                            return this.value;
                                        }
                                    }
                                },
                                legend: {
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:50,
                                    floating: true,
                                    shadow: true
                                },
                                tooltip: {
                                    pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
                                },
                                plotOptions: {
                                    area: {
                                        marker: {
                                            enabled: true,
                                            symbol: 'circle',
                                            backgroundColor: '#000',
                                            radius: 4,
                                            states: {
                                                hover: {
                                                    enabled: true
                                                }
                                            }
                                        },
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '14px'
                                            }
                                        }
                                    }
                                },
                                series: [
                                <?php
                                foreach($rel->respostas[$id] as $descri => $valores) {
                                    $printresp[] = "{name:'$descri',data:[".implode(",",  array_values($valores))."]}";
                                }
                                echo implode(",",$printresp);
                                ?>
                                ]
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                        })
                    </script>
                    <?php
                    }
                    if(isset($rel->gruposespec)) {
                    ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'paretogrupo',
                                    marginTop: 100,
                                    marginBottom: 130,
                                    width: 1024,
                                    height: 600,
                                    style: {
                                        fontSize: '9px'
                                    }
                                },
                                title: {
                                    text: 'PARETO PESO - APONTAMENTOS'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: [{
                                    tickWidth: 5,
                                    labels: {
                                        x: 0,
                                        y: 30,
                                        style: {
                                            color: '#000000',
                                            fontSize: '10px'
                                            //fontWeight: 'bold'
                                        }
                                    },
                                    lineColor: '#000000',
                                    categories: ['<?php echo implode("','",array_keys($rel->gruposespec['paretopeso']['perc']));?>']
                                }],
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.series.x +': '+ this.y;
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: 'PERCENTUAL'
                                    }
                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                color: '#000'
                                            }
                                        }
                                },
                                series: [{
                                    name: 'PERCENTUAL PESO',
                                    color: '#2DC432',
                                    type: 'column',
                                    data: [<?php echo implode(",",$rel->gruposespec['paretopeso']['perc']);?>]

                                }, {
                                    name: 'ACUMULADO PESO',
                                    color: '#EA8F49',
                                    type: 'spline',
                                    data: [<?php echo implode(",",$rel->gruposespec['paretopeso']['acum']);?>]
                                }],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                        });
                    </script>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'paretogrupoespec',
                                    marginTop: 100,
                                    marginBottom: 130,
                                    width: 1024,
                                    height: 600,
                                    style: {
                                        fontSize: '9px'
                                    }
                                },
                                title: {
                                    text: 'PARETO - APONTAMENTOS'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: [{
                                    tickWidth: 5,
                                    labels: {
                                        x: 0,
                                        y: 30,
                                        style: {
                                            color: '#000000',
                                            fontSize: '10px'
                                            //fontWeight: 'bold'
                                        }
                                    },
                                    lineColor: '#000000',
                                    categories: ['<?php echo implode("','",array_keys($rel->gruposespec['pareto']['perc']));?>']
                                }],
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.series.x +': '+ this.y;
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: 'PERCENTUAL'
                                    }
                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                color: '#000'
                                            }
                                        }
                                },
                                series: [{
                                    name: 'PERCENTUAL',
                                    color: '#4DA1EA',
                                    type: 'column',
                                    data: [<?php echo implode(",",$rel->gruposespec['pareto']['perc']);?>]

                                }, {
                                    name: 'ACUMULADO',
                                    color: '#EA8F49',
                                    type: 'spline',
                                    data: [<?php echo implode(",",$rel->gruposespec['pareto']['acum']);?>]
                                }],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                        });
                    </script>
                    <?php
                    }
                    foreach($rel->top as $modgraf => $dadostop) {
                        ?>
                        <script type="text/javascript">
                            var chart;
                            $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'graftop<?php echo $modgraf;?>',
                                    marginTop: 100,
                                    marginBottom: 200,
                                    width: 1024,
                                    height: 550
                                },
                                title: {
                                    text: 'TOP 10 APONTAMENTOS - <?php echo $modgraf;?>'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: [{
                                    labels: {
                                        x: 0,
                                        y: 30,
                                        rotation: -45,
                                        style: {
                                            color: '#000000'
                                            //fontWeight: 'bold'
                                        }
                                    },
                                    categories: ['<?php echo implode("','",array_keys($dadostop));?>']
                                }],
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.x +': '+ this.y;
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    max: <?php echo ($rel->qtde[$modgraf]['comfg']['qtde'] + 20);?>,
                                    title: {
                                        text: 'QTDE'
                                    },
                                    plotLines : [{
                                        value : <?php echo $rel->qtde[$modgraf]['comfg']['qtde'];?>,
                                        color : 'red',
                                        dashStyle : 'shortdash',
                                        width : 2,
                                        label : {
                                            text : 'Qtde. Monitorias - <?php echo $rel->qtde[$modgraf]['comfg']['qtde'];?>'
                                        }
                                    }]
                                },
                                legend: {
                                    enabled: false,
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:50,
                                    floating: true,
                                    shadow: true
                                },
                                plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                color: '#000',
                                                style: {
                                                    fontSize: '13px'
                                                }
                                            }
                                        }
                                },
                                series: [{
                                    name: 'QTDE',
                                    type: 'column',
                                    data: [<?php echo implode(",",$dadostop);?>]

                                }],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                            });
                        </script>
                        <?php
                    }
                }
                ?>

                <!-- grafico nota média-->
                <?php
                if($rel->tipo == "mensal" && $rel->vars['idplanilha'] == "") {
                    foreach($rel->fmdados as $fn => $dfn) {
                        ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                    renderTo: 'graf<?php echo $fn;?>',
                                    defaultSeriesType: 'column',
                                    marginTop: 120,
                                    marginBottom: 270,
                                    width: 1024,
                                    height: 700
                                },
                                title: {
                                    text: 'NOTAS MÍNIMAS, MÁXIMAS E MÉDIAS'
                                },
                                subtitle: {
                                    text: 'MÉDIA GLOBAL - <?php echo $rel->notas[$fn]['total'];?>',
                                    x: -20,
                                    style: {
                                        color: '#000000',
                                        fontWeight: 'bold'
                                    }
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $datas = explode(",",$rel->vars['datactt']); echo banco2data($datas[0])."-".  banco2data($datas[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: {
                                    labels: {
                                        style: {
                                            color: '#000000'
                                        },
                                        rotation: 270,
                                        y: 110
                                    },
                                    categories: [
                                        <?php
                                        echo "'".implode("','",array_keys($dfn))."'";
                                        ?>
                                    ]
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: 'NOTAS'
                                    }//,
                                    //plotLines : [{
                                    //     value : <?php echo $rel->notas['total'][0];?>,
                                    //     color : 'black',
                                    //     dashStyle : 'ShortDash',
                                    //     width : 2,
                                    //     label : {
                                    //             rotation: 270,
                                    //             text : 'MÉDIA'
                                    //     }
                                    //}]
                                },
                                legend: {
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:40,
                                    floating: true,
                                    shadow: true
                                },
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '12px'
                                            }
                                        },
                                        groupPadding: 0.10
                                    }
                                },
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.x +': '+ this.y;
                                    }
                                },
                                series: [
                                    <?php
                                    $c = count($rel->notas[$fn]) - 1;
                                    $i = 0;
                                    foreach($rel->notas[$fn] as $nota => $val) {
                                        if($nota == "total") {
                                        }
                                        else {
                                            $i++;
                                            if($i < $c) {
                                                echo "{name: '$nota',";
                                                if($i == 1) {
                                                }
                                                else {
                                                    echo "color: '#FF7A7A',";
                                                }
                                                echo "data: [".implode(",",$val)."]},";
                                            }
                                            if($i == $c) {
                                                echo "{name: '$nota',";
                                                echo "color: '#2DC432',";
                                                echo "data: [".implode(",",$val)."]}";
                                            }
                                        }
                                    }
                                    ?>
                                    ],
                                    exporting: {
                                        enableImages: true,
                                        width: 1280
                                    }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            });
                            });
                        </script>

                        <?php
                    }
                }
                else {
                        $plan = "SELECT descriplanilha FROM planilha WHERE idplanilha='".$_POST['plan']."'";
                        $eselplan = $_SESSION['fetch_array']($_SESSION['query']($plan)) or die (mysql_error());
                        $planilha = $eselplan['descriplanilha'];
                        if($rel->tipo == "semanal" && $_SESSION['selbanco'] == "monitoria_itau" && eregi("auditoria",$planilha)) {
                        }
                        else {
                        ?>
                        <script type="text/javascript">
                            var chart;
                            $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                renderTo: 'grafnotas',
                                defaultSeriesType: 'column',
                                marginTop: 120,
                                marginBottom: 270,
                                width: 1024,
                                height: 700
                            },
                            title: {
                                text: 'NOTAS MÍNIMAS, MÁXIMAS E MÉDIAS'
                            },
                            subtitle: {
                                text: 'MÉDIA GLOBAL - <?php echo $rel->notas['total'][0];?>',
                                x: -20,
                                style: {
                                    color: '#000000',
                                    fontWeight: 'bold'
                                }
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $datas = explode(",",$rel->vars['datactt']); echo banco2data($datas[0])."-".  banco2data($datas[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: {
                                labels: {
                                    style: {
                                        color: '#000000'
                                    },
                                    rotation: 270,
                                    y: 110
                                },
                                categories: [
                                    <?php
                                    echo "'".implode("','",array_keys($rel->cons))."'";
                                    ?>
                                ]
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: 'NOTAS'
                                }//,
                                //plotLines : [{
                                //       value : <?php echo $rel->notas['total'][0];?>,
                                //       color : 'black',
                                //       dashStyle : 'ShortDash',
                                //       width : 2,
                                //       label : {
                                //               rotation: 270,
                                //               text : 'MÉDIA'
                                //       }
                                //}]
                            },
                            legend: {
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '12px'
                                            }
                                        },
                                        groupPadding: 0.10
                                    }
                                },
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y;
                                }
                            },
                            series: [
                                <?php
                                $c = count($rel->notas) - 1;
                                $i = 0;
                                foreach($rel->notas as $nota => $val) {
                                    if($nota == "total") {
                                    }
                                    else {
                                        $i++;
                                        if($i < $c) {
                                            echo "{name: '$nota',";
                                            if($i == 1) {
                                            }
                                            else {
                                                echo "color: '#FF7A7A',";
                                            }
                                            echo "data: [".implode(",",$val)."]},";
                                        }
                                        if($i == $c) {
                                            echo "{name: '$nota',";
                                            echo "color: '#2DC432',";
                                            echo "data: [".implode(",",$val)."]}";
                                        }
                                    }
                                }
                                ?>
                                ],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        });
                        });
                    </script>
                    <?php
                        }
                }
                    ?>
                <!-- termino dados gráfico nota média-->

                <!-- grafico evolutivo de notas-->
                <?php
                    foreach($rel->evolutivo as $graf => $semana) {
                    $plot = 0;
                    ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'grafevolutivo<?php echo $graf;?>',
                                defaultSeriesType: 'line',
                                marginTop: 100,
                                marginBottom: 50,
                                width: 1024,
                                height: 500
                            },
                            title: {
                                text: 'EVOLUTIVO NOTA MÉDIA - <?php echo $graf;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: {
                                labels: {
                                    style: {
                                        color: '#000000'
                                    }
                                },
                                categories: [
                                    <?php
                                    $checkplan1 = array('0002','0003','0004','0008','0010');
                                    $checkplan2 = array('0011','0012');
                                    $cat = array();
                                    foreach($semana as $sem => $valores) {
                                        $plot++;
                                        $dsem = explode(",",$sem);
                                        if($rel->tipo == "mensal") {
                                            if(($rel->meses[substr($dsem[1],5,2)] == "Nov" && substr($dsem[1],0,4) == 2014 && in_array($rel->vars['idplanilha'],$checkplan1)) || ($rel->meses[substr($dsem[1],5,2)] == "Mar" && substr($dsem[1],0,4) == 2015 && in_array($rel->vars['idplanilha'],$checkplan2))) {
                                                $posiplot = $plot;
                                            }
                                            $cat[] = $rel->meses[substr($dsem[1],5,2)]."/".substr($dsem[1],0,4);
                                        }
                                        else {
                                            $cat[] = banco2data($dsem[0])." a ".banco2data($dsem[1]);
                                        }
                                    }
                                    echo "'".implode("','",$cat)."'";
                                    ?>
                                ]
                                <?php

                                if($posiplot >= 1) {
                                ?>,
                                plotLines: [{
                                    color: '#FF0000',
                                    width: 2,
                                    value: <?php echo ($posiplot - 1);?>,
                                    dashStyle: 'shortdot',
                                    label: {
                                        <?php
                                        if(in_array($rel->vars['idplanilha'], $checkplan1)) {
                                            ?>
                                            text: 'A partir do Ciclo Nov/14, foi alterado o Checklist Unificado e os Pesos',
                                            <?php
                                        }
                                        if(in_array($rel->vars['idplanilha'], $checkplan2)) {
                                            ?>
                                            text: 'A partir do Ciclo Mar/15, foi alterado o Checklist',
                                            <?php
                                        }
                                        ?>
                                        verticalAlign: 'top',
                                        <?php
                                        if($posiplot < 9) {
                                            echo "textAlign: 'left',";
                                        }
                                        else {
                                            echo "textAlign: 'right',";
                                        }
                                        ?>
                                        rotation:360
                                    }
                                }]
                                <?php
                                }
                                ?>
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: 'NOTAS'
                                }
                            },
                            legend: {
                                enabled: false,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '13px'
                                            }
                                        }
                                    }
                            },
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y;
                                }
                            },
                            series: [{<?php echo "data: [".implode(",",$semana)."]";?>}],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        });
                        });
                    </script>
                    <!-- termino do gráfico evolutivo-->
                    <?php
                    }

                if($rel->tipo == "mensal") {
                    ?>

                    <!-- grafico duração -->
                    <?php
                    foreach($rel->duracao as $tipodura => $dadosdura) {
                    ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'grafdura<?php echo $tipodura;?>',
                                marginTop: 100,
                                marginBottom: 100,
                                width: 1024,
                                height: 500
                            },
                            title: {
                                text: 'DISTRIBUIÇÃO DAS CHAMADAS - TMA - <?php echo $tipodura;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                labels: {
                                    x: 0,
                                    y: 30,
                                    style: {
                                        color: '#000000'
                                        //fontWeight: 'bold'
                                    }
                                },
                                categories: ['<?php echo implode("','",array_keys($rel->tempos));?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y;
                                }
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: 'PERCENTUAL'
                                }
                            },
                            legend: {
                                enabled: false,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '13px'
                                            }
                                        }
                                    }
                            },
                            series: [{
                                name: 'PERCENTUAL',
                                type: 'column',
                                data: [<?php echo implode(",",$dadosdura);?>]

                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <!-- termino duração -->
                    <?php
                    }

                    foreach($rel->intervalo as $tiponota => $valnotas) {
                    ?>
                    <!-- inicio grafico distribuição notas notas-->
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'grafnotas<?php echo $tiponota;?>',
                                marginTop: 100,
                                marginBottom: 100,
                                width: 1024,
                                height: 500
                            },
                            title: {
                                text: 'DISTRIBUIÇÃO DAS NOTAS MÉDIAS - <?php echo $tiponota;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                labels: {
                                    x: 0,
                                    y: 30,
                                    style: {
                                        color: '#000000'
                                        //fontWeight: 'bold'
                                    }
                                },
                                categories: ['<?php echo implode("','",array_keys($valnotas));?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y;
                                }
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: 'PERCENTUAL'
                                }
                            },
                            legend: {
                                enabled: false,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000',
                                            style: {
                                                fontSize: '13px'
                                            }
                                        }
                                    }
                            },
                            series: [{
                                name: 'PERCENTUAL',
                                type: 'column',
                                data: [<?php echo implode(",",$valnotas);?>]

                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <!-- termino grafico distribuição notas-->

                    <?php
                    }
                }

                if($rel->vars['idplanilha'] != "") {
                    if($rel->tipo == "mensal") {
                    ?>
                    <!-- gráficos incidencia comparativo -->
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                                chart = new Highcharts.Chart({
                                    chart: {
                                        renderTo: 'grafincibloco',
                                        type: 'line',
                                        marginTop: 160,
                                        marginBottom: 100,
                                        width: 1024,
                                        height: 500
                                    },
                                    title: {
                                        text: 'EVOLUÇÃO INCIDÊNCIA POR BLOCO'
                                    },
                                    credits: {
                                        text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                        href: 'Jarezende'
                                    },
                                    xAxis: [{
                                        labels: {
                                            x: 0,
                                            y: 30,
                                            style: {
                                                color: '#000000'
                                                //fontWeight: 'bold'
                                            }
                                        },
                                        categories: ['<?php echo implode("','",$rel->incidenciagrup['categoria']);?>']
                                    }],
                                    tooltip: {
                                        formatter: function() {
                                            return ''+
                                            this.x +': '+ this.y;
                                        }
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: 'PERCENTUAL'
                                        }
                                    },
                                    legend: {
                                        enabled: true,
                                        layout: 'horizontal',
                                        backgroundColor: '#FFFFFF',
                                        align: 'center',
                                        verticalAlign: 'top',
                                        y:80,
                                        floating: true,
                                        shadow: true
                                    },
                                    plotOptions: {
                                            series: {
                                                dataLabels: {
                                                    enabled: true,
                                                    color: '#000',
                                                    style: {
                                                        fontSize: '13px'
                                                    },
                                                    formatter: function() {
                                                        return this.y +'%';
                                                    }
                                                }
                                            }
                                    },
                                    series: [
                                        <?php
                                        $cgrupo = count($rel->incidenciagrup['grupos']);
                                        foreach($rel->incidenciagrup['grupos'] as $grupo => $valores) {
                                            $loopgrupo++;
                                            $selnome = "SELECT descrigrupo FROM grupo WHERE idgrupo='$grupo'";
                                            $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                                            if($loopgrupo == $cgrupo) {
                                                $virg = "";
                                            }
                                            else {
                                                $virg = ",";
                                            }
                                            ?>
                                            {
                                            name: '<?php echo $eselnome['descrigrupo'];?>',
                                            data: [<?php echo implode(",",$valores);?>]
                                            }
                                            <?php
                                            echo $virg;
                                        }
                                        ?>],
                                    exporting: {
                                        enableImages: true,
                                        width: 1280
                                    }
                                },
                                function(chart) {
                                    chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                                }
                            );
                        });
                    </script>
                    <?php
                    foreach($rel->incidenciagrup['gruposperg'] as $idgrupo => $pergs) {
                    $loopgrupo = 0;
                    $selgrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='$idgrupo'";
                    $eselgrupo = $_SESSION['fetch_array']($_SESSION['query']($selgrupo)) or die (mysql_error());
                    ?>
                    <!-- gráficos incidencia comparativo -->
                    <script type="text/javascript">
                        var chart;
                            $(document).ready(function() {
                                chart = new Highcharts.Chart({
                                    chart: {
                                        renderTo: 'grafincibloco<?php echo $idgrupo;?>',
                                        type: 'line',
                                        marginTop: 160,
                                        marginBottom: 100,
                                        width: 1024,
                                        height: 500
                                    },
                                    title: {
                                        text: 'EVOLUÇÃO INCIDÊNCIA POR BLOCO - <?php echo $eselgrupo['descrigrupo'];?>'
                                    },
                                    credits: {
                                        text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                        href: 'Jarezende'
                                    },
                                    xAxis: [{
                                        labels: {
                                            x: 0,
                                            y: 30,
                                            style: {
                                                color: '#000000'
                                                //fontWeight: 'bold'
                                            }
                                        },
                                        categories: ['<?php echo implode("','",$rel->incidenciagrup['categoria']);?>']
                                    }],
                                    tooltip: {
                                        formatter: function() {
                                            return ''+
                                            this.x +': '+ this.y;
                                        }
                                    },
                                    yAxis: {
                                        min: 0,
                                        title: {
                                            text: 'PERCENTUAL'
                                        }
                                    },
                                    legend: {
                                        enabled: true,
                                        layout: 'horizontal',
                                        backgroundColor: '#FFFFFF',
                                        align: 'center',
                                        verticalAlign: 'top',
                                        y:80,
                                        floating: true,
                                        shadow: true
                                    },
                                    plotOptions: {
                                            series: {
                                                dataLabels: {
                                                    enabled: true,
                                                    color: '#000',
                                                    style: {
                                                        fontSize: '13px'
                                                    },
                                                    formatter: function() {
                                                        return this.y +'%';
                                                    }
                                                }
                                            }
                                    },
                                    series: [
                                        <?php
                                        $cgrupo = count($rel->incidenciagrup['gruposperg'][$idgrupo]);
                                        foreach($rel->incidenciagrup['gruposperg'][$idgrupo] as $idperg => $valores) {
                                            $loopgrupo++;
                                            $selnome = "SELECT descripergunta FROM pergunta WHERE idpergunta='$idperg'";
                                            $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                                            if($loopgrupo == $cgrupo) {
                                                $virg = "";
                                            }
                                            else {
                                                $virg = ",";
                                            }
                                            ?>
                                            {
                                            name: '<?php echo $eselnome['descripergunta'];?>',
                                            data: [<?php echo implode(",",$valores);?>]
                                            }
                                            <?php
                                            echo $virg;
                                        }
                                        ?>],
                                    exporting: {
                                        enableImages: true,
                                        width: 1280
                                    }
                                },
                                function(chart) {
                                    chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                                }
                                );
                            });
                        </script>
                        <?php
                    }
                    }

                    if($rel->vars['idplanilha'] == "0015" || $rel->vars['idplanilha'] == "0016") {
                        if(isset($rel->nivel)) {
                        ?>
                        <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                            chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'nivellig',
                                type:'pie'
                            },
                            title: {
                                text: 'NIVEL DE LIGAÇÃO'
                            },
                            tooltip: {
                                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            plotOptions: {
                                pie: {
                                    allowPointSelect: true,
                                    cursor: 'pointer',
                                    depth: 35,
                                    dataLabels: {
                                        enabled: true,
                                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                                        style: {
                                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                        }
                                    }
                                }
                            },
                            series: [{
                                type: 'pie',
                                name: 'Nivel de Ligação',
                                data: [
                                    <?php
                                    $loop = 0;
                                    foreach($rel->nivel as $idresp => $qt) {
                                        $loop++;
                                        $selperg = "SELECT descriresposta as descri FROM pergunta WHERE idresposta = $idresp";
                                        $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg));
                                        if($loop == count($rel->nivel)) {
                                        ?>
                                        ['<?php echo $eselperg['descri'];?>',<?php echo $qt;?>]
                                        <?php
                                        }
                                        else {
                                        ?>
                                        ['<?php echo $eselperg['descri'];?>',<?php echo $qt;?>],
                                        <?php
                                        }

                                    }
                                    ?>
                                ]
                            }]
                            });
                        })
                        </script>
                        <?php
                        }
                    }

                foreach($rel->pareto as $tipo => $val) {
                ?>
                <!-- grafico de pareto-->
                <script type="text/javascript">
                    var chart;
                    $(document).ready(function() {
                    chart = new Highcharts.Chart({
                        chart: {
                            renderTo: 'grafpareto<?php echo $tipo;?>',
                            marginTop: 100,
                            marginBottom: 100,
                            width: 1024,
                            height: 500
                        },
                        title: {
                            text: 'PARETO - <?php echo $tipo;?>'
                        },
                        credits: {
                            text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                            href: 'Jarezende'
                        },
                        xAxis: [{
                            tickWidth: 5,
                            labels: {
                                x: 0,
                                y: 30,
                                style: {
                                    color: '#000000',
                                    fontSize: '11px'
                                    //fontWeight: 'bold'
                                }
                            },
                            tickPixelInterval: 20,
                            lineColor: '#000000',
                            categories: ['<?php echo implode("','",array_keys($val['percacu']));?>']
                        }],
                        tooltip: {
                            formatter: function() {
                                return ''+
                                this.x +': '+ this.y;
                            }
                        },
                        yAxis: {
                            min: 0,
                            max: 100,
                            title: {
                                text: 'PERCENTUAL'
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                                series: {
                                    dataLabels: {
                                        enabled: true,
                                        color: '#000'
                                    }
                                }
                        },
                        series: [{
                            name: 'PERCENTUAL',
                            color: '#4DA1EA',
                            type: 'column',
                            data: [<?php echo implode(",",$val['perc']);?>]

                        }, {
                            name: 'ACUMULADO',
                            color: '#EA8F49',
                            type: 'spline',
                            data: [<?php echo implode(",",$val['percacu']);?>]
                        }],
                        exporting: {
                            enableImages: true,
                            width: 1280
                        }
                    },
                    function(chart) {
                        chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                    }
                    );
                    });
                </script>
                <!-- termino grafico de pareto-->
                <?php
                }

                    foreach($rel->peso as $tipopeso => $valpeso) {
                    ?>
                    <!-- grafico de pareto peso-->
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'grafparetopeso<?php echo $tipopeso;?>',
                                marginTop: 100,
                                marginBottom: 150,
                                width: 1024,
                                height: 700,
                                style: {
                                    fontSize: '9px'
                                }
                            },
                            title: {
                                text: 'PARETO PESO - <?php echo $tipopeso;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                tickWidth: 5,
                                labels: {
                                    x: 0,
                                    y: 30,
                                    style: {
                                        color: '#000000',
                                        fontSize: '10px'
                                        //fontWeight: 'bold'
                                    }
                                },
                                lineColor: '#000000',
                                categories: ['<?php echo implode("','",array_keys($valpeso['percacu']));?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.series.x +': '+ this.y;
                                }
                            },
                            yAxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: 'PERCENTUAL'
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000'
                                        }
                                    }
                            },
                            series: [{
                                name: 'PERCENTUAL PESO',
                                color: '#2DC432',
                                type: 'column',
                                data: [<?php echo implode(",",$valpeso['perc']);?>]

                            }, {
                                name: 'ACUMULADO PESO',
                                color: '#EA8F49',
                                type: 'spline',
                                data: [<?php echo implode(",",$valpeso['percacu']);?>]
                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <!-- termino grafico de pareto peso-->
                    <?php
                    }


                    foreach($rel->paretoncg  as $tipopancg => $dadospancg) {
                        ?>
                        <!-- inicio pareto por NCG-->
                        <script type="text/javascript">
                            var chart;
                            $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'grafparetoncg<?php echo $tipopancg;?>',
                                    marginTop: 100,
                                    marginBottom: 150,
                                    width: 1024,
                                    height: 700
                                },
                                title: {
                                    text: 'PARETO NCG - <?php echo $tipopancg;?>'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: [{
                                    tickWidth: 5,
                                    labels: {
                                        x: 0,
                                        y: 30,
                                        style: {
                                            color: '#000000',
                                            fontSize: '10px'
                                            //fontWeight: 'bold'
                                        }
                                    },
                                    tickPixelInterval: 20,
                                    lineColor: '#000000',
                                    categories: ['<?php echo implode("','",array_keys($dadospancg['percacu']));?>']
                                }],
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.x +': '+ this.y;
                                    }
                                },
                                yAxis: {
                                    min: 0,
                                    max: 100,
                                    title: {
                                        text: 'PERCENTUAL'
                                    }
                                },
                                legend: {
                                    enabled: false
                                },
                                plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                color: '#000'
                                            }
                                        }
                                },
                                series: [{
                                    name: 'PERCENTUAL',
                                    color: '#EA6279',
                                    type: 'column',
                                    data: [<?php echo implode(",",$dadospancg['perc']);?>]

                                }, {
                                    name: 'ACUMULADO',
                                    color: '#EA0514',
                                    type: 'spline',
                                    data: [<?php echo implode(",",$dadospancg['percacu']);?>]
                                }],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            }
                            );
                            });
                        </script>
                        <!-- pareto de NCG-->
                        <?php
                    }
                }

                if($rel->tipo == "mensal" && $rel->vars['idplanilha'] != "") {
                    foreach($rel->gruposespec['acompanhamento'] as $id => $ddgraf) {
                        echo "<div id=\"grafcompgrupo$id\"></div><br/><hr/><br/>";
                        echo "<div id=\"grafrespostas$id\"></div><br/><hr/><br/>";
                        echo "<div id=\"grafitens$id\"></div><br/><hr/><br/>";
                    }
                    echo "<div id=\"graftopGLOBAL\"></div><br/><hr/><br/>";
                }
                foreach($rel->cons as $model => $dadostop) {
                    ?>
                    <div id="graftop<?php echo $model;?>"></div><br/><hr/><br/>
                    <?php
                }

                if($rel->tipo == "mensal") {
                    $dadosop .= "<table id=\"notasoperador\" width=\"100%\" style=\"font-size: 12px\">";
                    $dadosop .= "<tr style=\"background-color: #06F; color: #FFF; font-weight: bold\">";
                        $dadosop .= "<td colspan=\"4\" align=\"center\">DADOS</td>";
                        $mesoper = $rel->mesoper;
                        krsort($mesoper);
                        foreach($mesoper as $m) {
                        $dadosop .= "<td style=\"padding: 5px\">$m</td>";
                        }
                    $dadosop .= "</tr>";
                    $dadosop .= "<tr style=\"background-color: #06F; color: #FFF; font-weight: bold\">";
                        $dadosop .= "<td align=\"center\">Negociador</td>";
                        $dadosop .= "<td align=\"center\">Supervisor</td>";
                        $dadosop .= "<td align=\"center\">Carteira</td>";
                        $dadosop .= "<td align=\"center\">Data De Admissão</td>";
                        $dadosop .= "<td align=\"center\" colspan=\"3\">Nota</td>";
                    $dadosop .= "</tr>";
                        $cores[0] = array('02','03','07','08','09','10');
                        $cores[1] = array('04','05','06','13');
                        arsort($rel->opernota[$rel->mesoper[0]]);
                        $operadores = array_keys($rel->opernota[$rel->mesoper[0]]);
                        foreach($rel->opernota[$rel->mesoper[1]] as $oper => $nota) {
                            if(!in_array($oper, $operadores)) {
                                $operadores[] = $oper;
                            }
                        }
                        foreach($operadores as $op) {
                            $dadosop .= "<tr>";
                            $dadosop .= "<td align=\"center\">".$rel->operadores[$op]['operador']."</td>";
                            $dadosop .= "<td align=\"center\">".$rel->operadores[$op]['super_oper']."</td>";
                            $dadosop .= "<td align=\"center\">".nomevisu($rel->operadores[$op]['idrel_filtros'])."</td>";
                            $dadosop .= "<td align=\"center\">".banco2data($rel->operadores[$op]['data_de_admissao'])."</td>";
                            foreach($mesoper as $km => $mes) {
                                $sparam = "SELECT idparam_moni FROM conf_rel WHERE idrel_filtros=".$rel->operadores[$op]['idrel_filtros']."";
                                $eparam = $_SESSION['fetch_array']($_SESSION['query']($sparam));
                                if($eparam['idparam_moni'] == "03" || $eparam['idparam_moni'] == "04") {
                                    if($rel->opernota[$mes][$op] == "" && $rel->opernota[$mes][$op] != "0") {
                                        $cor = "";
                                    }
                                    else {
                                        if($eparam['idparam_moni'] == "03") {
                                            if($rel->opernota[$mes][$op] == "0" OR $rel->opernota[$mes][$op] < 85) {
                                                $cor = "style=\"background-color:#EA7070\"";
                                            }
                                            if($rel->opernota[$mes][$op] >= 85) {
                                                $cor = "style=\"background-color:#8EEA88\"";
                                            }
                                        }
                                        else {
                                            if($rel->opernota[$mes][$op] == "0" OR $rel->opernota[$mes][$op] < 85) {
                                                $cor = "style=\"background-color:#EA7070\"";
                                            }
                                            if($rel->opernota[$mes][$op] >= 85) {
                                                $cor = "style=\"background-color:#8EEA88\"";
                                            }
                                        }
                                    }
                                }
                                else {
                                    if($eparam['idparam_moni'] == "01" || $eparam['idparam_moni'] == "02") {
                                        if($rel->opernota[$mes][$op] == "" && $rel->opernota[$mes][$op] != "0") {
                                            $cor = "";
                                        }
                                        else {
                                            if($rel->opernota[$mes][$op] == "0" OR $rel->opernota[$mes][$op] < 75) {
                                                $cor = "style=\"background-color:#EA7070\"";
                                            }
                                            if($rel->opernota[$mes][$op] >= 75) {
                                                $cor = "style=\"background-color:#8EEA88\"";
                                            }
                                        }
                                    }
                                    if($eparam['idparam_moni'] == "05") {
                                        if($rel->opernota[$mes][$op] == "" && $rel->opernota[$mes][$op] != "0") {
                                            $cor = "";
                                        }
                                        else {
                                            if($rel->opernota[$mes][$op] == "0" OR $rel->opernota[$mes][$op] < 90) {
                                                $cor = "style=\"background-color:#EA7070\"";
                                            }
                                            if($rel->opernota[$mes][$op] >= 90) {
                                                $cor = "style=\"background-color:#8EEA88\"";
                                            }
                                        }
                                    }
                                }
                                $dadosop .= "<td align=\"center\" $cor>".$rel->opernota[$mes][$op]."</td>";
                            }
                            $dadosop .= "</tr>";
                        }
                    $dadosop .= "</tr>";
                    $dadosop .= "</table><br/><hr/><br/>";
                    echo $dadosop;
                    if($rel->vars['idplanilha'] == "") {
                        $_SESSION['dadosexp'] = $dadosop;
                    }
                }

                ?>
                <div id="nivellig">
                </div><hr/>
                <?php

                if($rel->tipo == "mensal" && $rel->vars['idplanilha'] == "") {
                    foreach($rel->fmdados as $fngraf => $dfngraf) {
                        ?>
                        <div id="graf<?php echo $fngraf;?>">
                        </div><hr/>
                        <?php
                    }
                }
                else {
                    ?>
                    <div id="grafnotas"></div><hr/>
                    <?php
                }

                if($rel->tipo == "mensal") {
                    foreach($rel->duracao as $divtipo => $divdados) {
                        ?>
                        <div id="grafdura<?php echo $divtipo;?>">
                        </div><br/><hr/>
                        <?php
                    }

                    foreach($rel->intervalo as $divinter => $divdinter) {
                        ?>
                        <div id="grafnotas<?php echo $divinter;?>">
                        </div><br/><hr/>
                        <?php
                    }

                    if($rel->vars['idplanilha'] != "") {
                        ?>
                        <div id="grafincibloco">
                        </div><br/><hr/>
                        <?php
                        foreach($rel->incidenciagrup['gruposperg'] as $idgrupo => $pergs) {
                            ?>
                            <div id="grafincibloco<?php echo $idgrupo;?>">
                            </div><br/><hr/>
                            <?php
                        }
                    }
                }


                foreach($rel->evolutivo as $tipoe => $dadose) {
                    ?>
                    <div id="grafevolutivo<?php echo $tipoe;?>">
                    </div><br/><hr/>
                    <?php
                }

                if($rel->vars['idplanilha'] != "") {
                    if(isset($rel->gruposespec)) {
                        echo "<div id=\"paretogrupo\"></div><br/><hr/><br/>";
                        echo "<div id=\"paretogrupoespec\"></div><br/><hr/><br/>";
                    }
                    foreach($rel->peso as $tipope => $dadospe) {
                    ?>
                        <div id="grafparetopeso<?php echo $tipope;?>">
                        </div><br/><hr/>
                    <?php
                    }
                    foreach($rel->pareto as $tipop => $dadosp) {
                    ?>
                        <div id="grafpareto<?php echo $tipop;?>">
                        </div><br/><hr/>
                    <?php
                    }

                    if($_SESSION['nomecli'] != "ITAU") {
                    }
                    else {
                        foreach($rel->paretoncg  as $tipodivncg => $dadosdivncg) {
                            ?>
                            <div id="grafparetoncg<?php echo $tipodivncg;?>">
                            </div><br/><hr/>
                            <?php
                        }
                    }
                    //<!-- tabela de arvore da planilha-->
                    $dadosexp .= "<table width=\"auto\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif;\">";
                    $dadosexp .= "<tr>";
                    $dadosexp .= "<td><strong>LEGENDA</strong></td>";
                    $dadosexp .= "</tr>";
                    $dadosexp .= "<tr>";
                    $dadosexp .= "<td bgcolor=\"#EA6C70\"><strong>itens inativos</stromg></td>";
                    $dadosexp .= "</tr>";
                    $dadosexp .= "<tr>";
                    $dadosexp .= "<td><strong>% BLOCO - total de sinalizações divididos pelo total de sianlizações do grupo</stromg></td>";
                    $dadosexp .= "</tr>";
                    $dadosexp .= "</table>";
                    $dadosexp .= "<br/><br/>";
                    $dadosexp .= "<table width=\"auto\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif;\" border=\"1\" bordercolor=\"#000000\" cellspacing=\"0\">";
                    $dadosexp .= "<tr>";
                        $dadosexp .= "<td colspan=\"3\"></td>";
                        $dadosexp .= "<td bgcolor=\"#999999\" colspan=\"50\"><strong>JAREZENDE</strong></td>";
                    $dadosexp .= "</tr>";
                    $dadosexp .= "<tr>";
                        $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>Grupo</strong></td>";
                        $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>Pergunta</strong></td>";
                        $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>Resposta</strong></td>";
                        foreach($rel->cons as $aneps => $aideps) {
                            $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>$aneps</strong></td>";
                            $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>% BLOCO</strong></td>";
                        }
                        $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>TOTAL</strong></td>";
                        $dadosexp .= "<td bgcolor=\"#999999\" align=\"center\"><strong>% BLOCO</strong></td>";
                    $dadosexp .= "</tr>";
                        foreach($rel->arvore as $idsgrupo => $array) {
                            $ttotal = 0;
                            $ctr = 0;
                            $col = 0;
                            foreach($array as $p) {
                                foreach($p as $idp => $textp) {
                                    $cresp = "SELECT count(*) as r FROM pergunta WHERE idpergunta='$idp'";
                                    $ecresp = $_SESSION['fetch_array']($_SESSION['query']($cresp)) or die (mysql_error());
                                    $col = $col + $ecresp['r'];
                                }
                            }

                            $selgrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='$idsgrupo'";
                            $eselgrupo = $_SESSION['fetch_array']($_SESSION['query']($selgrupo)) or die ("erro na query de consulta do nome do grupo");
                            $dadosexp .= "<tr bgcolor=\"#FFFFFF\">";
                            $dadosexp .= "<td rowspan=\"$col\">".$eselgrupo['descrigrupo']."</td>";
                            foreach($array as $idsperg) {
                                $ttgrupo = array();
                                foreach($idsperg as $idperg => $descri) {
                                    if($ctr == 0) {
                                    }
                                    else {
                                        $dadosexp .= "<tr bgcolor=\"#FFFFFF\">";
                                    }
                                    $totalgrupo = 0;
                                    $cresp = "SELECT ativo, count(idresposta) as r FROM pergunta WHERE idpergunta='$idperg'";
                                    $cresp = $_SESSION['fetch_array']($_SESSION['query']($cresp)) or die (mysql_error());
                                    if($cresp['ativo'] == "N") {
                                        $atvcolor = "#EA6C70";
                                    }
                                    else {
                                        $atvcolor = "#FFFFFF";
                                    }
                                    $dadosexp .= "<td bgcolor=\"$atvcolor\" rowspan=\"".$cresp['r']."\">$descri</td>";
                                    $pr = 0;
                                    foreach($rel->valarvore[$idperg] as $idresp => $cons) {
                                        $ttperg = 0;
                                        $ttdivperg = 0;
                                        $selresp = "SELECT * FROM pergunta WHERE idresposta='$idresp'";
                                        $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
                                        $dadosexp .= "<td bgcolor=\"#FFFFFF\">".$eselresp['descriresposta']."</td>";
                                        foreach ($cons as $nf => $idf) {
                                            //if($pr == 0) {

                                                //$dadosexp .= "</tr>";
                                                //$dadosexp .= "<tr>";
                                            //}
                                            $val = explode("#",$idf);
                                            if($val[1] < "33") {
                                                $color = "#B2E87B";
                                            }
                                            if($val[1] >= "33") {
                                                $color = "#E8E584";
                                            }
                                            if($val[1] >= "67") {
                                                $color = "#E85656";
                                            }
                                            $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">$val[0]</td>";
                                            $dadosexp .= "<td align=\"center\" bgcolor=\"$color\">$val[1]</td>";
                                            $ttperg = $ttperg + $val[0];
                                            $ttgrupo[$nf] = $ttgrupo[$nf] + $val[0];
                                            $totalgrupo = $totalgrupo + $val[2];
                                            $ttdivperg = $ttdivperg + $val[2];
                                            $pr++;
                                        }
                                        $percperg = number_format(($ttperg/$ttdivperg)*100,1);
                                        if($percperg < "33") {
                                            $color = "#B2E87B";
                                        }
                                        if($percperg >= "33") {
                                            $color = "#E8E584";
                                        }
                                        if($percperg >= "67") {
                                            $color = "#E85656";
                                        }
                                        $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">$ttperg</td>";
                                        $dadosexp .= "<td align=\"center\" bgcolor=\"$color\">$percperg</td>";
                                        $dadosexp .= "</tr>";
                                        $ctr++;
                                        $pr++;
                                    }
                                }
                            }
                            $dadosexp .= "<tr bgcolor=\"#CCCCCC\">";
                            $dadosexp .= "<td colspan=\"3\">Total ".$eselgrupo['descrigrupo']."</td>";
                            foreach($rel->cons as $teps => $tneps) {
                                $dadosexp .= "<td align=\"center\">$ttgrupo[$teps]</td>";
                                $dadosexp .= "<td></td>";
                                $ttotal = $ttotal + $ttgrupo[$teps];
                            }
                            $dadosexp .= "<td align=\"center\">$ttotal</td>";
                            $dadosexp .= "<td></td>";
                            $dadosexp .= "</tr>";
                        }
                    $dadosexp .= "</table><br/><hr/>";
                    $dadosexp .= "<table width=\"auto\" id=\"arvoretab\" border=\"1\" bordercolor=\"#000000\" cellspacing=\"0\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif;\">";
                        $dadosexp .= "<tr style=\"background-color: #999999;font-weight: bold\"><td colspan=\"2\" bgcolor=\"#FFFFFF\"></td><td colspan=\"".((count($rel->cons) * 2) + 2)."\">EPS</td></tr>";
                        $dadosexp .= "<tr style=\"background-color: #999999;font-weight: bold\">";
                        $dadosexp .= "<td width=\"200px\">Pergunta</td><td width=\"200px\">Resposta</td>";
                        foreach($rel->cons as $aneps => $aideps) {
                            $dadosexp .= "<td align=\"center\"><strong>$aneps</strong></td>";
                            $dadosexp .= "<td align=\"center\"><strong>% Resposta</strong></td>";
                        }
                        $dadosexp .= "<td align=\"center\"><strong>TOTAL</strong></td>";
                        $dadosexp .= "<td align=\"center\"><strong>% Resposta</strong></td>";
                        $dadosexp .= "</tr>";
                        foreach($rel->arvoretab as $ptab => $ltab) {
                            $sqlperg = "SELECT descripergunta FROM pergunta WHERE idpergunta='$ptab'";
                            $esqltab = $_SESSION['fetch_array']($_SESSION['query']($sqlperg)) or die (mysql_error());
                            $dadosexp .= "<tr><td rowspan=\"".count($ltab)."\">".$esqltab['descripergunta']."</td>";
                            $qtresp = count($ltab);
                            $loop = 0;
                            foreach($ltab as $potab => $rtab) {
                                    $selresp = "SELECT descriresposta FROM pergunta WHERE idresposta='$rtab'";
                                    $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
                                    $ttresptab = 0;
                                    if($loop > 0 && $qtde >= 2) {
                                        $dadosexp .= "<tr>";
                                    }
                                    $dadosexp .= "<td>".$eselresp['descriresposta']."</td>";
                                    foreach($rel->cons as $aeps => $neps) {
                                        $dadostab = explode("#",$rel->valtab[$ptab][$rtab][$aeps]);
                                        $dadosexp .= "<td align=\"center\">$dadostab[0]</td>";
                                        $dadosexp .= "<td align=\"center\">$dadostab[1]</td>";
                                        $ttpergtab = $ttpergtab + $dadostab[0];
                                        $ttresptab = $ttresptab + $dadostab[0];
                                    }
                                    $dadosexp .= "<td align=\"center\">$ttresptab</td>";
                                    $dadosexp .= "<td align=\"center\">".number_format(($ttresptab/$rel->valtab[$ptab]['total']) * 100,1)."</td>";
                                    $dadosexp .= "</tr>";
                                    $loop++;
                            }
                            $dadosexp .= "</tr>";
                            $dadosexp .= "<tr bgcolor=\"#CCCCCC\" height=\"10px\"><td colspan=\"".((count($rel->cons) * 2) + 4)."\"></td></tr>";
                        }
                    $dadosexp .= "</table><br/><hr/>";
                    //<!-- fim arvore planilha-->

                    //apontamentos por pergunta
                    $dadosexp .= "<span style=\"font-weight:bold\">APONTAMENTOS (ERROS)</span><br/><br/>";
                    $dadosexp .= "<table width=\"auto\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif\">";
                    $dadosexp .= "<tr bgcolor=\"#5873CC\"><td align=\"center\"><font color=\"#FFFFFF\">PERGUNTA</font></td><td><font color=\"#FFFFFF\">QTDE</font></td><td><font color=\"#FFFFFF\">% SOBRE TOTAL MONITORADO - (".$rel->peapont['totalmo'].")</font></td></tr>";
                    arsort($rel->peapont['pergunta']);
                    $it = 0;
                    foreach($rel->peapont['pergunta'] as $idpe => $qt) {
                        if($it < 3) {
                            $itensqtde[] = $idpe;
                            $it++;
                        }
                        $descri = "SELECT descripergunta as dc FROM pergunta WHERE idpergunta=$idpe GROUP BY idpergunta";
                        $edescri = $_SESSION['fetch_array']($_SESSION['query']($descri));
                        $dadosexp .= "<tr><td>".$edescri['dc']."</td><td align=\"center\">$qt</td><td align=\"center\">".round(($qt/$rel->peapont['totalmo'])*100,1)."</td></tr>";
                        $ttapont = $ttapont+$qt;
                    }
                    $dadosexp .= "<tr><td><strong>TOTAL</strong></td><td align=\"center\"><strong>$ttapont</strong></td><td align=\"center\"><strong>".round(($ttapont/$rel->peapont['totalmo'])*100,1)."</strong></td></tr>";
                    $dadosexp .= "</table><br/><hr/>";

                    // apontamentos por operador
                    $dadosexp .= "<span style=\"font-weight:bold\">DETALHAMENTO APONTAMENTOS OPERADOR</span><br/><br/>";
                    $dadosexp .= "<table width=\"auto\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif\">";
                    $dadosexp .= "<tr bgcolor=\"#5873CC\"><td align=\"center\"><font color=\"#FFFFFF\">OPERADOR</font></td><td><font color=\"#FFFFFF\">PERGUNTA</font></td><td><font color=\"#FFFFFF\">QUANTIDADE</font></td></tr>";
                    foreach($rel->peapont['operador'] as $nop => $dadosop) {
                        arsort($dadosop);
                        $cl = 0;
                        foreach($dadosop as $idp => $val) {
                            $cl++;
                            $selper = "SELECT descripergunta as dc FROM pergunta WHERE idpergunta=$idp GROUP BY idpergunta";
                            $lselper = $_SESSION['fetch_array']($_SESSION['query']($selper));
                            if($cl == count($dadosop)) {
                                $line = "style=\"border-bottom:1px solid #000\"";
                            }
                            else {
                                $line = "style=\"\"";
                            }
                            $dadosexp .= "<tr><td $line>$nop</td><td align=\"center\" $line>".$lselper['dc']."</td><td align=\"center\" $line>$val</td></tr>";
                        }
                    }
                    $dadosexp .= "</table><br/><hr/>";

                    // quantidade de sinalização por operador dos 3 itens mais pontuados
                    foreach($rel->peapont['operador'] as $nop => $dados) {
                        $opersinal[$nop] = $dados[$itensqtde[0]];
                    }
                    arsort($opersinal);
                    $dadosexp .= "<span style=\"font-weight:bold\">A tabela abaixo apresenta as quantidades de sinalização (sem peso) por operador, dos três itens que mais obtiveram sinalizações</span><br/><br/>";
                    $dadosexp .= "<table width=\"auto\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif\">";
                    $dadosexp .= "<tr bgcolor=\"#5873CC\">";
                    $dadosexp .= "<tr bgcolor=\"#5873CC\"><td><font color=\"#FFFFFF\">OPERADOR</font></td>";
                    foreach($itensqtde as $item) {
                        $descri = "SELECT descripergunta as dc FROM pergunta WHERE idpergunta=$item GROUP BY idpergunta";
                        $edescri = $_SESSION['fetch_array']($_SESSION['query']($descri));
                        $dadosexp .= "<td align=\"center\"><font color=\"#FFFFFF\">".$edescri['dc']."</font></td>";
                    }
                    $dadosexp .= "</tr>";
                    foreach($opersinal as $nop => $qt) {
                        if($qt == null && ($rel->peapont['operador'][$nop][$itensqtde[1]] == true || $rel->peapont['operador'][$nop][$itensqtde[2]] == true)) {
                            $dadosexp .= "<tr><td>$nop</td>";
                            $dadosexp .= "<td align=\"center\">0</td>";
                            if($rel->peapont['operador'][$nop][$itensqtde[1]]) {
                                $dadosexp .= "<td align=\"center\">".$rel->peapont['operador'][$nop][$itensqtde[1]]."</td>";
                            }
                            else {
                                $dadosexp .= "<td align=\"center\">0</td>";
                            }
                            if($rel->peapont['operador'][$nop][$itensqtde[2]]) {
                                $dadosexp .= "<td align=\"center\">".$rel->peapont['operador'][$nop][$itensqtde[2]]."</td></tr>";
                            }
                            else {
                                $dadosexp .= "<td align=\"center\">0</td></tr>";
                            }
                        }
                        else {
                            if($qt != null) {
                                $dadosexp .= "<tr><td>$nop</td>";
                                $dadosexp .= "<td align=\"center\">$qt</td>";
                                if($rel->peapont['operador'][$nop][$itensqtde[1]]) {
                                    $dadosexp .= "<td align=\"center\">".$rel->peapont['operador'][$nop][$itensqtde[1]]."</td>";
                                }
                                else {
                                    $dadosexp .= "<td align=\"center\">0</td>";
                                }
                                if($rel->peapont['operador'][$nop][$itensqtde[2]]) {
                                    $dadosexp .= "<td align=\"center\">".$rel->peapont['operador'][$nop][$itensqtde[2]]."</td></tr>";
                                }
                                else {
                                    $dadosexp .= "<td align=\"center\">0</td></tr>";
                                }
                            }
                        }
                    }
                    $dadosexp .= "</table><br/><hr/>";


                    //<!-- interferencia grupos-->
                        $dadosexp .= "<table width=\"auto\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif\">";
                        $dadosexp .= "<tr colspan=\"8\" align=\"center\"><td>INTERFERÊNCIA GRUPOS</td></tr>";
                        $dadosexp .= "<tr bgcolor=\"#999999\">";
                            $dadosexp .= "<td><strong>JAREZENDE</strong></td>";
                            $dadosexp .= "<td align=\"center\">Qtde.NCG</td>";
                            //$dadosexp .= "<td align=\"center\">NCG</td>";
                            foreach($rel->arvore as $arv => $grup) {
                                $selgrup = "SELECT descrigrupo,fg FROM grupo WHERE idgrupo='$arv'";
                                $eselgrup = $_SESSION['fetch_array']($_SESSION['query']($selgrup)) or die ("erro na query de consulta do nome do grupo");
                                if($eselgrup['fg'] == "S") {
                                }
                                else {
                                    $dadosexp .= "<td align=\"center\">".$eselgrup['descrigrupo']."</td>";
                                }
                            }
                            ?>
                        </tr>
                        <?php
                        $e = 0;
                        foreach($rel->interferencia as $eps => $grupos) {
                            $e++;
                            $soma = 0;
                            $in = 0;
                            $dadosexp .= "<tr>";
                            $dadosexp .= "<td>$eps</td>";
                            $selval = "SELECT COUNT(DISTINCT(".$rel->aliasrel[moniavalia].".idmonitoria)) as qtde, AVG(".$rel->aliasrel[moniavalia].".valor_final_aval) as media FROM moniavalia ".$rel->aliasrel[moniavalia]."
                                    INNER JOIN monitoria ".$rel->aliasrel[monitoria]." ON ".$rel->aliasrel[monitoria].".idmonitoria=".$rel->aliasrel[moniavalia].".idmonitoria
                                    INNER JOIN rel_filtros ".$rel->aliasrel[rel_filtros]." ON ".$rel->aliasrel[rel_filtros].".idrel_filtros=".$rel->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$rel->aliasrel[operador]." ON ".$rel->aliasrel[operador].".idoperador=".$rel->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$rel->aliasrel[super_oper]." ON ".$rel->aliasrel[super_oper].".idsuper_oper=".$rel->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$rel->aliasrel[monitor]." ON ".$rel->aliasrel[monitor].".idmonitor=".$rel->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$rel->aliasrel[conf_rel]." ON ".$rel->aliasrel[conf_rel].".idrel_filtros=".$rel->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$rel->aliasrel[param_moni]." ON ".$rel->aliasrel[param_moni].".idparam_moni=".$rel->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$rel->aliasrel[fluxo]." ON ".$rel->aliasrel[fluxo].".idfluxo=".$rel->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$rel->aliasrel[monitoria_fluxo]." ON ".$rel->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$rel->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$rel->aliasrel[rel_fluxo]." ON ".$rel->aliasrel[rel_fluxo].".idrel_fluxo=".$rel->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$rel->aliasrel[indice]." ON ".$rel->aliasrel[indice].".idindice=".$rel->aliasrel[param_moni].".idindice
                                    INNER JOIN intervalo_notas ".$rel->aliasrel[intervalo_notas]." ON ".$rel->aliasrel[intervalo_notas].".idindice=".$rel->aliasrel[indice].".idindice
                                    WHERE $rel->wheresql $rel->wherectt AND $rel->sqlcons'".$rel->cons[$eps]."' AND ((".$rel->aliasrel[monitoria].".checkfg='NCG OK') OR (".$rel->aliasrel[monitoria].".checkfg='NCG NOK' AND qtdefg='0') OR (".$rel->aliasrel[monitoria].".checkfg IS NULL AND qtdefg=0))";
                            $lselval = $_SESSION['query']($selval) or die (mysql_error());
                            $nval = $_SESSION['num_rows']($eselval);
                            if($nval >= 1) {
                                while($lselval = $_SESSION['fetch_array']($eselval)) {
                                    $in++;
                                    $soma = $soma + $lselval['valor_final_aval'];
                                }
                            }
                            else {
                                $in++;
                                $soma = $soma + 0;
                            }
                            $selncg = "SELECT COUNT(DISTINCT(".$rel->aliasrel[monitoria].".idmonitoria)) as qtde FROM moniavalia ".$rel->aliasrel[moniavalia]."
                                    INNER JOIN monitoria ".$rel->aliasrel[monitoria]." ON ".$rel->aliasrel[monitoria].".idmonitoria=".$rel->aliasrel[moniavalia].".idmonitoria
                                    INNER JOIN rel_filtros ".$rel->aliasrel[rel_filtros]." ON ".$rel->aliasrel[rel_filtros].".idrel_filtros=".$rel->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN operador ".$rel->aliasrel[operador]." ON ".$rel->aliasrel[operador].".idoperador=".$rel->aliasrel[monitoria].".idoperador
                                    INNER JOIN super_oper ".$rel->aliasrel[super_oper]." ON ".$rel->aliasrel[super_oper].".idsuper_oper=".$rel->aliasrel[monitoria].".idsuper_oper
                                    INNER JOIN monitor ".$rel->aliasrel[monitor]." ON ".$rel->aliasrel[monitor].".idmonitor=".$rel->aliasrel[monitoria].".idmonitor
                                    INNER JOIN conf_rel ".$rel->aliasrel[conf_rel]." ON ".$rel->aliasrel[conf_rel].".idrel_filtros=".$rel->aliasrel[monitoria].".idrel_filtros
                                    INNER JOIN param_moni ".$rel->aliasrel[param_moni]." ON ".$rel->aliasrel[param_moni].".idparam_moni=".$rel->aliasrel[conf_rel].".idparam_moni
                                    INNER JOIN fluxo ".$rel->aliasrel[fluxo]." ON ".$rel->aliasrel[fluxo].".idfluxo=".$rel->aliasrel[conf_rel].".idfluxo
                                    INNER JOIN monitoria_fluxo ".$rel->aliasrel[monitoria_fluxo]." ON ".$rel->aliasrel[monitoria_fluxo].".idmonitoria_fluxo=".$rel->aliasrel[monitoria].".idmonitoria_fluxo
                                    INNER JOIN rel_fluxo ".$rel->aliasrel[rel_fluxo]." ON ".$rel->aliasrel[rel_fluxo].".idrel_fluxo=".$rel->aliasrel[monitoria_fluxo].".idrel_fluxo
                                    INNER JOIN indice ".$rel->aliasrel[indice]." ON ".$rel->aliasrel[indice].".idindice=".$rel->aliasrel[param_moni].".idindice
                                    INNER JOIN intervalo_notas ".$rel->aliasrel[intervalo_notas]." ON ".$rel->aliasrel[intervalo_notas].".idindice=".$rel->aliasrel[indice].".idindice
                                    WHERE $rel->wheresql $rel->wherectt AND $rel->sqlcons'".$rel->cons[$eps]."' AND  ".$rel->aliasrel[monitoria].".qtdefg >= 1 AND ".$rel->aliasrel[monitoria].".checkfg='NCG OK'";
                            $eselncg = $_SESSION['fetch_array']($_SESSION['query']($selncg)) or die ("erro na query de consulta da quantidade de ncg");
                            $dadosexp .= "<td align=\"center\">".$eselncg['qtde']."</td>";
                            $totalncg = $totalncg + $eselncg['qtde'];
                            $mediancg = $mediancg + number_format(($lselval['media'] / $lselval['qtde']),2);
                            //$dadosexp .= "<td align=\"center\">".number_format(($lselval['media'] / $lselval['qtde']),2)."</td>";
                            foreach($grupos as $idg => $valor) {
                                $dadosexp .= "<td align=\"center\">$valor</td>";
                                $totais[$idg] = $totais[$idg] + $valor;
                            }
                            $dadosexp .= "</tr>";
                        }
                        $dadosexp .= "<tr bgcolor=\"#CCCCCC\">";
                        $dadosexp .= "<td><strong>TOTAL</strong></td>";
                        $dadosexp .= "<td align=\"center\">$totalncg</td>";
                        $percttncg = number_format(($mediancg / $e),1);
                        //$dadosexp .= "<td align=\"center\">$percttncg</td>";
                        foreach($totais as $tgrupo) {
                            $dadosexp .= "<td align=\"center\">".number_format(($tgrupo / $e),2)."</td>";
                        }
                        $dadosexp .= "</tr>";
                    $dadosexp .= "</table><br/><hr/>";
                    //<!-- fim interferencia-->

                    //<!-- impacto NCG-->
                    $dadosexp .= "<table width=\"600\" style=\"font-size:13px; font-family:Verdana, Geneva, sans-serif\">";
                    $dadosexp .= "<tr><td>IMPACTO NCG</td></tr>";
                    $dadosexp .= "<tr>";
                        $dadosexp .= "<td></td>";
                        $dadosexp .= "<td align=\"center\" bgcolor=\"#999999\">MÉDIA NCG</td>";
                        $dadosexp .= "<td align=\"center\" bgcolor=\"#999999\">MÉDIA S/ NCG</td>";
                    $dadosexp .= "</tr>";
                    foreach($rel->impactoncg as $fncg => $dncg) {
                        $dadosexp .= "<tr>";
                            $dadosexp .= "<td bgcolor=\"#CCCCCC\"><strong>$fncg</strong></td>";
                            $dadosexp .= "<td align=\"center\">".round($dncg['mediancg'],1)."</td>";
                            $dadosexp .= "<td align=\"center\">".round($dncg['media'],1)."</td>";
                        $dadosexp .= "</tr>";
                        }
                    $dadosexp .= "</table><br/><hr/>";

                    //<!-- fim impacto NCG-->

                    $_SESSION['dadosexp'] = $dadosop;
                    $_SESSION['dadosexp'] .= $dadosexp;
                    echo $dadosexp;


                    //<!-- grafico NCG-->
                    ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'grafncg',
                                marginTop: 100,
                                marginBottom: 50,
                                width: 1024,
                                height: 500
                            },
                            title: {
                                text: 'REPRESENTATIVIDADE NCG´S'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                labels: {
                                    style: {
                                        color: '#000000'
                                        //fontWeight: 'bold'
                                    }
                                },
                                categories: ['<?php echo implode("','",array_keys($rel->cons));?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y;
                                }
                            },
                            yAxis: [{ // Primary yAxis
                                min: 0,
                                labels: {
                                    formatter: function() {
                                    return this.value;
                                    },
                                    style: {
                                    color: '#4DA1EA'
                                    }
                                },
                                title: {
                                    text: 'QUANTIDADE',
                                    style: {
                                    color: '#4DA1EA'
                                    }
                                }
                            }, { // Secondary yAxis
                                min: 0,
                                title: {
                                    text: 'PERCENTUAL',
                                    style: {
                                    color: '#EA8F49'
                                    }
                                },
                                labels: {
                                    formatter: function() {
                                    return this.value +' %';
                                    },
                                    style: {
                                    color: '#EA8F49'
                                    }
                                },
                                opposite: true
                            }],
                            legend: {
                                enabled: true,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000'
                                        }
                                    }
                            },
                            series: [{
                                name: 'QUANTIDADE',
                                color: '#4DA1EA',
                                type: 'column',
                                data: [<?php echo implode(",",$rel->ncg['qtde']);?>]

                            }, {
                                name: 'PERCENTUAL',
                                color: '#EA8F49',
                                type: 'line',
                                yAxis: 1,
                                data: [<?php echo implode(",",$rel->ncg['perc']);?>]
                            },
                            {
                                name: 'QTDE.NCG',
                                color: '#BA0202',
                                type: 'scatter',
                                data: [<?php echo implode(",",$rel->ncg['qtdencg']);?>]
                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <div id="grafncg"></div>
                    <?php
                }
                else {
                }
            }

            // relatório de contestação
            if($rel->tipo == "exefluxo" OR $rel->tipo == "contesta" OR $rel->tipo == "contestatmp") {
                $_SESSION['dadosexp'] = $rel->dados;
                echo $rel->dados;
            }

            // inicio relatório especifico por item
            if($rel->tipo != "semanal" && $rel->tipo != "mensal" && $rel->tipo != "exefluxo" && $rel->tipo != "contesta" && $rel->tipo != "contestatmp" && $rel->tipo != "operador" && $rel->tipo != "histoperador") {
                ?>
                <table width="1024">
                    <tr>
                        <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=semanal"><div class="divexport">EXCEL</div></a></td>
                        <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome=semanal"><div class="divexport">WORD</div></a></td>
                        <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome=semanal"><div class="divexport">PDF</div></a></td>
                    </tr>
                </table><br /><br /><hr/>
                <?php
                if($rel->vars['idplanilha'] != "") {
                    ?>
                    <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'relespe',
                                marginTop: 100,
                                marginBottom: 250,
                                width: 1024,
                                height: 600
                            },
                            title: {
                                text: '<?php echo $rel->nomerelatorio;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                labels: {
                                    style: {
                                        color: '#000000'
                                    },
                                    rotation: 270,
                                    y: 110
                                },
                                categories: ['<?php echo implode("','",array_keys($rel->cons));?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y;
                                }
                            },
                            yAxis: [{ // Primary yAxis
                                min: 0,
                                labels: {
                                    formatter: function() {
                                    return this.value;
                                    },
                                    style: {
                                    color: '#4DA1EA'
                                    }
                                },
                                title: {
                                    text: 'QTDE. MONITORIA',
                                    style: {
                                    color: '#4DA1EA'
                                    }
                                }
                            }, { // Secondary yAxis
                                min: 0,
                                title: {
                                    text: 'PERC. NÃO INCENTIVO',
                                    style: {
                                    color: '#EA8F49'
                                    }
                                },
                                labels: {
                                    formatter: function() {
                                    return this.value +' %';
                                    },
                                    style: {
                                    color: '#EA8F49'
                                    }
                                },
                                opposite: true
                            }],
                            legend: {
                                enabled: true,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000'
                                        }
                                    }
                            },
                            series: [{
                                name: 'QTDE. MONITORIA',
                                color: '#4DA1EA',
                                type: 'column',
                                data: [<?php echo implode(",",$rel->relresp['total']);?>]

                            },
                            {
                                name: 'PERC. NÃO INCENTIVO',
                                color: '#BA0202',
                                type: 'scatter',
                                yAxis: 1,
                                data: [<?php echo implode(",",$rel->relresp['perc']);?>]
                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <div id="relespe"></div><br/><hr/>
                    <?php
                    foreach($rel->semanas as $sem) {
                        $datas = explode(",",$sem);
                        $exsem[] = banco2data($datas[0])." a ".banco2data($datas[1]);
                    }
                    foreach($rel->cons as $relsem => $id) {
                        ?>
                        <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'relespe<?php echo $relsem;?>',
                                marginTop: 100,
                                marginBottom: 50,
                                width: 1024,
                                height: 500
                            },
                            title: {
                                text: '<?php echo $rel->nomerelatorio;?> - <?php echo $relsem;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                labels: {
                                    style: {
                                        color: '#000000'
                                    }
                                },
                                categories: ['<?php echo implode("','",$exsem);?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y + '%';
                                }
                            },
                            yAxis: [{ // Primary yAxis
                                min: 0,
                                labels: {
                                    formatter: function() {
                                    return this.value;
                                    },
                                    style: {
                                    color: '#EA8F49'
                                    }
                                },
                                title: {
                                    text: 'PERC. NÃO INCENTIVO',
                                    style: {
                                    color: '#EA8F49'
                                    }
                                }
                            }],
                            legend: {
                                enabled: true,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000'
                                        }
                                    }
                            },
                            series: [{
                                name: 'PERC.NÃO INCENTIVO',
                                color: '#BA0202',
                                type: 'line',
                                data: [<?php echo implode(",",$rel->relrespsem[$relsem]['perc']);?>]
                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <div id="relespe<?php echo $relsem;?>"></div><br/><hr/>
                    <?php
                    }
                }
                else {
                    foreach($rel->fmdados as $idgrupo => $nomes) {
                    ?>
                        <script type="text/javascript">
                            var chart;
                            $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'relespe<?php echo $idgrupo;?>',
                                    marginTop: 100,
                                    marginBottom: 250,
                                    width: 1024,
                                    height: 600
                                },
                                title: {
                                    text: '<?php echo $rel->nomerelatorio;?> - <?php echo $idgrupo;?>'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: [{
                                    labels: {
                                        style: {
                                            color: '#000000'
                                        },
                                        rotation: 270,
                                        y: 110
                                    },
                                    categories: ['<?php echo implode("','",array_keys($nomes));?>']
                                }],
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.x +': '+ this.y;
                                    }
                                },
                                yAxis: [{ // Primary yAxis
                                    min: 0,
                                    labels: {
                                        formatter: function() {
                                        return this.value;
                                        },
                                        style: {
                                        color: '#4DA1EA'
                                        }
                                    },
                                    title: {
                                        text: 'QTDE. MONITORIA',
                                        style: {
                                        color: '#4DA1EA'
                                        }
                                    }
                                }, { // Secondary yAxis
                                    min: 0,
                                    title: {
                                        text: 'PERC. NÃO INCENTIVO',
                                        style: {
                                        color: '#EA8F49'
                                        }
                                    },
                                    labels: {
                                        formatter: function() {
                                        return this.value +' %';
                                        },
                                        style: {
                                        color: '#EA8F49'
                                        }
                                    },
                                    opposite: true
                                }],
                                legend: {
                                    enabled: true,
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:50,
                                    floating: true,
                                    shadow: true
                                },
                                plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                color: '#000'
                                            }
                                        }
                                },
                                series: [{
                                name: 'QTDE. MONITORIA',
                                    color: '#4DA1EA',
                                    type: 'column',
                                    data: [<?php echo implode(",",$rel->relresp[$idgrupo]['total']);?>]

                                },
                                {
                                    name: 'PERC. NÃO INCENTIVO',
                                    color: '#BA0202',
                                    type: 'scatter',
                                    yAxis: 1,
                                    data: [<?php echo implode(",",$rel->relresp[$idgrupo]['perc']);?>]
                                }],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            }
                            );
                            });
                        </script>
                        <div id="relespe<?php echo $idgrupo;?>"></div><br/><hr/>
                        <?php
                    }
                    foreach($rel->semanas as $sem) {
                        $datas = explode(",",$sem);
                        $exsem[] = banco2data($datas[0])." a ".banco2data($datas[1]);
                    }
                    foreach($rel->fmdados as $idgruposem => $nomessem) {
                        foreach($nomessem as $relsem => $idrelsem) {
                            ?>
                            <script type="text/javascript">
                            var chart;
                            $(document).ready(function() {
                            chart = new Highcharts.Chart({
                                chart: {
                                    renderTo: 'relespe<?php echo $relsem;?>',
                                    marginTop: 100,
                                    marginBottom: 50,
                                    width: 1024,
                                    height: 500
                                },
                                title: {
                                    text: '<?php echo $rel->nomerelatorio;?> - <?php echo $relsem;?>'
                                },
                                credits: {
                                    text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                    href: 'Jarezende'
                                },
                                xAxis: [{
                                    labels: {
                                        style: {
                                            color: '#000000'
                                        }
                                    },
                                    categories: ['<?php echo implode("','",$exsem);?>']
                                }],
                                tooltip: {
                                    formatter: function() {
                                        return ''+
                                        this.x +': '+ this.y + '%';
                                    }
                                },
                                yAxis: [{ // Primary yAxis
                                    min: 0,
                                    labels: {
                                        formatter: function() {
                                        return this.value;
                                        },
                                        style: {
                                        color: '#EA8F49'
                                        }
                                    },
                                    title: {
                                        text: 'PERC. NÃO INCENTIVO',
                                        style: {
                                        color: '#EA8F49'
                                        }
                                    }
                                }],
                                legend: {
                                    enabled: true,
                                    layout: 'horizontal',
                                    backgroundColor: '#FFFFFF',
                                    align: 'center',
                                    verticalAlign: 'top',
                                    y:50,
                                    floating: true,
                                    shadow: true
                                },
                                plotOptions: {
                                        series: {
                                            dataLabels: {
                                                enabled: true,
                                                color: '#000'
                                            }
                                        }
                                },
                                series: [{
                                    name: 'PERC. NÃO INCENTIVO',
                                    color: '#BA0202',
                                    type: 'line',
                                    data: [<?php echo implode(",",$rel->relrespsem[$relsem]['perc']);?>]
                                }],
                                exporting: {
                                    enableImages: true,
                                    width: 1280
                                }
                            },
                            function(chart) {
                                chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                            }
                            );
                            });
                        </script>
                        <div id="relespe<?php echo $relsem;?>"></div><br/><hr/>
                        <?php
                        }
                    }
                    $selcolunas = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel LIMIT 3";
                    $eselcolunas = $_SESSION['query']($selcolunas) or die ("erro na query de consulta dos filtros");
                    while($lselcol = $_SESSION['fetch_array']($eselcolunas)) {
                        $colunas["id_".strtolower($lselcol['nomefiltro_nomes'])] = $lselcol['nomefiltro_nomes'];
                    }
                    $colunasrel = array_reverse($colunas,true);
                    foreach($colunasrel as $colinner => $nomeinner) {
                        $inner[] = "INNER JOIN filtro_dados fd".$rel->AliasTab(strtolower($nomeinner))." ON fd".$rel->AliasTab(strtolower($nomeinner)).".idfiltro_dados=rf.$colinner";
                        $colunastab[] = "fd".$rel->AliasTab(strtolower($nomeinner)).".nomefiltro_dados as $nomeinner";
                    }
                    ?>
                    <div style=" font-size: 12px">
                        <?php
                        $dados .= "<table width=\"100%\">";
                            $dados .= "<tr>";
                                $dados .= "<td align=\"center\"><strong>OPERADOR</strong></td>";
                                foreach($colunasrel as $chavefiltros => $crel) {
                                    $dados .= "<td align=\"center\"><strong>$crel</strong></td>";
                                }
                                    $dados .= "<td align=\"center\"><strong>QTDE</strong></td>";
                                    $dados .= "<td align=\"center\"><strong>QTDE FG</strong></td>";
                                    $dados .= "<td align=\"center\"><strong>MEDIA</strong></td>";
                                    $dados .= "<td align=\"center\"><strong>QTDE. NÃO INCENTIVO AO USO</strong></td>";
                                    $dados .= "<td align=\"center\"><strong>% NÃO INCENTIVO</strong></td>";
                            $dados .= "</tr>";
                            foreach($rel->operadores as $idoper => $dadosoper) {
                                $dados .= "<tr>";
                                    foreach($dadosoper as $dado => $valor) {
                                        if($dado == "idrel_filtros") {
                                            $selfiltro = "SELECT ".implode(",",$colunastab)." FROM rel_filtros rf ".implode(" ",$inner)." WHERE idrel_filtros='$valor'";
                                            $eselfiltros = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die ("erro na query de consulta do nome das colunas do relacionamento");
                                            foreach($colunasrel as $nome) {
                                                $dados .= "<td align=\"center\">$eselfiltros[$nome]</td>";
                                            }
                                        }
                                        else {
                                            $dados .= "<td align=\"center\">$valor</td>";
                                        }
                                }
                                $dados .= "</tr>";
                            }
                        $dados .= "</table>";
                        $_SESSION['dadosexp'] = $dados;
                        echo $dados;
                        ?>
                    </div>
                    <?php
                    foreach($this->cons as $relsem => $idrelsem) {
                        ?>
                        <script type="text/javascript">
                        var chart;
                        $(document).ready(function() {
                        chart = new Highcharts.Chart({
                            chart: {
                                renderTo: 'relespe<?php echo $relsem;?>',
                                marginTop: 100,
                                marginBottom: 50,
                                width: 1024,
                                height: 500
                            },
                            title: {
                                text: '<?php echo $rel->nomerelatorio;?> - <?php echo $relsem;?>'
                            },
                            credits: {
                                text: 'extracted: <?php echo date('d/m/Y')."-".date('H:i:s');?> | range: <?php $dt = explode(",",$rel->vars['datactt']); echo banco2data($dt[0])."-".  banco2data($dt[1]);?>',
                                href: 'Jarezende'
                            },
                            xAxis: [{
                                labels: {
                                    style: {
                                        color: '#000000'
                                    }
                                },
                                categories: ['<?php echo implode("','",$exsem);?>']
                            }],
                            tooltip: {
                                formatter: function() {
                                    return ''+
                                    this.x +': '+ this.y + '%';
                                }
                            },
                            yAxis: [{ // Primary yAxis
                                min: 0,
                                labels: {
                                    formatter: function() {
                                    return this.value;
                                    },
                                    style: {
                                    color: '#EA8F49'
                                    }
                                },
                                title: {
                                    text: 'PERC. NÃO INCENTIVO',
                                    style: {
                                    color: '#EA8F49'
                                    }
                                }
                            }],
                            legend: {
                                enabled: true,
                                layout: 'horizontal',
                                backgroundColor: '#FFFFFF',
                                align: 'center',
                                verticalAlign: 'top',
                                y:50,
                                floating: true,
                                shadow: true
                            },
                            plotOptions: {
                                    series: {
                                        dataLabels: {
                                            enabled: true,
                                            color: '#000'
                                        }
                                    }
                            },
                            series: [{
                                name: 'PERC. NÃO INCENTIVO',
                                color: '#BA0202',
                                type: 'line',
                                data: [<?php echo implode(",",$rel->relrespsem[$relsem]['perc']);?>]
                            }],
                            exporting: {
                                enableImages: true,
                                width: 1280
                            }
                        },
                        function(chart) {
                            chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/<?php echo $_SESSION['imagem'];?>', 40, 30, 70, 50).add();
                        }
                        );
                        });
                    </script>
                    <div id="relespe<?php echo $relsem;?>"></div><br/><hr/>

                    <?php
                    }
                }

                    $selcolunas = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel LIMIT 3";
                    $eselcolunas = $_SESSION['query']($selcolunas) or die ("erro na query de consulta dos filtros");
                    while($lselcol = $_SESSION['fetch_array']($eselcolunas)) {
                        $colunas["id_".strtolower($lselcol['nomefiltro_nomes'])] = $lselcol['nomefiltro_nomes'];
                    }
                    $colunasrel = array_reverse($colunas,true);
                    foreach($colunasrel as $colinner => $nomeinner) {
                        $inner[] = "INNER JOIN filtro_dados fd".$rel->AliasTab(strtolower($nomeinner))." ON fd".$rel->AliasTab(strtolower($nomeinner)).".idfiltro_dados=rf.$colinner";
                        $colunastab[] = "fd".$rel->AliasTab(strtolower($nomeinner)).".nomefiltro_dados as $nomeinner";
                    }
                    ?>
                    <div style=" font-size: 12px">
                        <?php
                        $dados .= "<table width=\"100%\">";
                            $dados .= "<tr>";
                                $dados .= "<td align=\"center\"><strong>OPERADOR</strong></td>";
                                foreach($colunasrel as $chavefiltros => $crel) {
                                    $dados .= "<td align=\"center\"><strong>$crel</strong></td>";
                                }
                                $dados .= "<td align=\"center\"><strong>QTDE</strong></td>";
                                $dados .= "<td align=\"center\"><strong>QTDE FG</strong></td>";
                                $dados .= "<td align=\"center\"><strong>MEDIA</strong></td>";
                                $dados .= "<td align=\"center\"><strong>QTDE. NÃO INCENTIVO AO USO</strong></td>";
                                $dados .= "<td align=\"center\"><strong>% NÃO INCENTIVO</strong></td>";
                            $dados .= "</tr>";
                            foreach($rel->operadores as $idoper => $dadosoper) {
                                $dados .= "<tr>";
                                    foreach($dadosoper as $dado => $valor) {
                                        if($dado == "idrel_filtros") {
                                            $selfiltro = "SELECT ".implode(",",$colunastab)." FROM rel_filtros rf ".implode(" ",$inner)." WHERE idrel_filtros='$valor'";
                                            $eselfiltros = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die ("erro na query de consulta do nome das colunas do relacionamento");
                                            foreach($colunasrel as $nome) {
                                                $dados .= "<td align=\"center\">$eselfiltros[$nome]</td>";
                                            }
                                        }
                                        else {
                                            $dados .= "<td align=\"center\">$valor</td>";
                                        }
                                }
                                $dados .= "</tr>";
                            }
                        $dados .= "</table>";
                        $_SESSION['dadosexp'] = $dados;
                        echo $dados;
                        ?>
                    </div>
                    <?php
            }
        }
}
    ?>

    <!-- fim grafico NCG-->

    <script type="text/javascript">
        $(document).ready(function() {
            $.unblockUI();
        })
    </script>
</div>
</body>
</html>
