<?php

/** @scripts_filtros = função para gerar os filtros automáticamente; */

function scripts_filtros() {

?>
<!--<link href="option.css" rel="stylesheet" type="text/css" />-->
<link rel="stylesheet" type="text/css" href="/monitoria_supervisao/js/custom-theme/jquery-ui-1.8.2.custom.css" />
<style type="text/css">
    #tabrel tr.odd td { background: #e6f2ff;}
    #tabrel tr.even td { background: #FFFFFF;}
</style>
<?php
$inners = array("user_adm" => 'INNER JOIN user_adm ua ON ua.idperfil_adm = pa.idperfil_adm',"user_web" => 'INNER JOIN user_web uw ON uw.idperfil_web = pw.idperfil_web');
$tabusers = array('user_adm' => 'perfil_adm,pa','user_web' => 'perfil_web,pw');
$tabperfil = explode(",",$tabusers[$_SESSION['user_tabela']]);
$filtros = "SELECT filtro_arvore FROM $tabperfil[0] $tabperfil[1] ". 
                    $inners[$_SESSION['user_tabela']]."
                    WHERE ".$tabperfil[1].".id$tabperfil[0]='".$_SESSION['usuarioperfil']."' AND id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
$effiltros = $_SESSION['fetch_array']($_SESSION['query']($filtros));
if($effiltros['filtro_arvore'] == "S") {
    ?>
    <script type="text/javascript" src="/monitoria_supervisao/users/combofiltros_<?php echo strtolower($_SESSION['nomecli']);?>.js"></script>
    <?php
}
?>
<script type="text/javascript" src="/monitoria_supervisao/users/comboplan.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(".data").datepicker({
            changeMonth: true,
            changeYear: true,
            //stepMonths: 3,
            buttonImage: '/images/datepicker.gif',
            dateFormat: 'dd/mm/yy',  
            dayNames: [  
            'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'  
            ],  
            dayNamesMin: [  
            'D','S','T','Q','Q','S','S','D'  
            ],  
            dayNamesShort: [  
            'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'  
            ],  
            monthNames: [  
            'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',  
            'Outubro','Novembro','Dezembro'  
            ],  
            monthNamesShort: [  
            'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',  
            'Out','Nov','Dez'  
            ],  
            nextText: 'Próximo',  
            prevText: 'Anterior'

        });
        
        $('#indice').live('change',function() {
            var indice = $(this).val();
            $('#intervalo').load("/monitoria_supervisao/users/comboconfig.php",{tpacao:'indice',origem:'indice', indice:indice});
        });
        
        $('#fluxo').change(function() {
            var fluxo = $(this).val();
            $('#atustatus').load("/monitoria_supervisao/users/comboconfig.php",{tpacao:'fluxo',origem:'fluxo', fluxo:fluxo});
        });
        
        var user = '<?php echo $_SESSION['usuarioID'];?>';
        $('#super_oper').autocomplete("/monitoria_supervisao/users/filtrasuper.php", {
            extraParams: { user: user <?php foreach(filtros () as $nfiltro) { echo ", id_".$nfiltro.": function() { return $('#filtro_".$nfiltro."').val(); }"."\r\n";}?> },
            timeout: 3000,
            alidSelection: false,
            width: 340,
            scrolHeigth: 220,
            selectFirst: true
        });

        var user = '<?php echo $_SESSION['usuarioID'];?>';
        $('#nomemonitor').autocomplete("/monitoria_supervisao/users/filtramonitor.php", {
            extraParams: { user: user <?php foreach(filtros () as $nfiltro) { echo ", id_".$nfiltro.": function() { return $('#filtro_".$nfiltro."').val(); }"."\r\n";}?>},
            timeout: 3000,
            alidSelection: false,
            width: 340,
            scrolHeigth: 220,
            selectFirst: true
        });

        //var tabuser = <?php echo $tabuser;?>;
        var user = <?php echo $_SESSION['usuarioID'];?>;
        $('#oper').autocomplete("/monitoria_supervisao/users/filtraoper.php", {
            extraParams: { user: user <?php foreach(filtros() as $nfiltro) { echo ", id_".$nfiltro.": function() { return $('#filtro_".$nfiltro."').val(); }"."\r\n";}?>},
            timeout: 3000,
            alidSelection: false,
            width: 340,
            scrolHeigth: 220,
            selectFirst: true
        });

        $("#periodoauto").change(function() {
             var datas = $(this).val().split("#");
             if($(this).val() == "") {
                  $("#dtinimoni").attr("value","");
                  $("#dtfimmoni").attr("value","");
                  $("#dtinictt").attr("value","");
                  $("#dtfimctt").attr("value","");
             }
             else {
                  $("#dtinimoni").attr("value",datas[1]);
                  $("#dtfimmoni").attr("value",datas[2]);
                  $("#dtinictt").attr("value",datas[3]);
                  $("#dtfimctt").attr("value",datas[4]);
             }
        });
    })
</script>

<?php
}

function filtros_divs($vals,$menu) {
    
$iduser = $_SESSION['usuarioID'];
$tabuser = $_SESSION['user_tabela'];

?>
<br />
<div style="width:1024px;">
    <input type="button" onclick="javascript:window.location = '<?php echo str_replace("&reset=1","",$_SERVER['REQUEST_URI'])."&reset=1";?>'" name="reset" id="reset" value="RESET FILTROS" style="width:150px;background:#EAAC2F; border:2px solid #FFF; font-size: 13px; font-weight: bolder;border-radius:5px" />
</div><br/>
<div style="width:1024px">
    <?php
    if($_SESSION['user_tabela'] == "user_adm") {
        $tipo = "OK";
    }
    else {
        $selperfil = "SELECT visuext FROM perfil_web WHERE idperfil_web='".$_SESSION['usuarioperfil']."'";
        $eselperfil = $_SESSION['fetch_array']($_SESSION['query']($selperfil)) or die (mysql_error());
        if($eselperfil['visuext'] == "S") {
            $tipo = "OK";
        }
        else {
            $tipo = "NOK";
        }
    }
    if($tipo == "OK") {
        $tmoni = $_POST['tmonitoria'];
        
        if($menu == "monitoria") {
        ?>
        <table width="800px">
        <?php
        }
        else {
        ?>
        <table width="500px">
        <?php
        }
        ?>
        <tr>
            <td class="corfd_ntab" align="center" style=" padding: 3px" width="150px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">TIPO MONITORIA</span></td>
            <td class="corfd_ntab" align="center" width="200" style=" padding: 3px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">PERÍODO</span></td>
            <?php
            if($menu == "monitoria") {
            ?>
            <td class="corfd_ntab" align="center" style=" padding: 3px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">EXPRESSÃO DE PESQUISA</span></td>
            <?php
            }
            ?>
        </tr>
        <tr>
            <td class="corfd_colcampos"><input type="checkbox" class="t" name="tmonitoria[]" id="tmonitoria" value="I" <?php if(strstr($_SESSION['varsconsult']['tmonitoria'],"I")) { echo "checked=\"checked\""; } if(in_array("I",$tmoni)) { echo "checked=\"checked\""; }?> /> JAREZENDE</td>
            <td class="corfd_colcampos" rowspan="2" align="center">
                 <?php
                 $selperiodos = "SELECT * FROM periodo ORDER BY dataini DESC LIMIT 14";
                 $eselperiodos = $_SESSION['query']($selperiodos) or die (mysql_error());
                 $nperiodos = $_SESSION['num_rows']($eselperiodos);
                 ?>
                 <select name="periodoauto" id="periodoauto" style="margin: 5px;width:150px; border: 1px solid #69C;height:30px; padding:5px;background-color:#FFF; border-radius:5px">
                      <option value="">SELECIONE...</option>
                      <?php
                      if($nperiodos >= 1) {
                           while($lselperiodos = $_SESSION['fetch_array']($eselperiodos)) {
                           ?>
                           <option value="<?php echo $lselperiodos['idperiodo']."#".banco2data($lselperiodos['dataini'])."#".banco2data($lselperiodos['datafim'])."#".banco2data($lselperiodos['dtinictt'])."#".banco2data($lselperiodos['dtfimctt']);?>"><?php echo $lselperiodos['nmes']."-".$lselperiodos['ano'];?></option>
                           <?php
                           }
                      }
                      ?>
                 </select> 
            </td>
            <?php
            if($menu == "monitoria") {
            ?>
            <td class="corfd_colcampos" rowspan="2" align="center">
                <input type="text" style=" margin: 4px; width:350px; height:25px;font-size:14px; border: 1px solid #69C; text-align:center;border-radius:5px" value="" name="search" id="search" />
            </td>
            <?php
            }
            ?>
        </tr>
        <tr>
            <td class="corfd_colcampos"><input type="checkbox" class="t" name="tmonitoria[]" id="tmonitoria" value="E" <?php if(strstr($_SESSION['varsconsult']['tmonitoria'],"E")) { echo "checked=\"checked\""; } if(in_array("E",$tmoni)) { echo "checked=\"checked\""; }?>/> EXTERNA <?php echo $_SESSION['nomecli'];?></td>
        </tr>
    </table><br/>
    <?php
    }
    else {
         ?>
    <table width="200px">
        <tr>
            <td class="corfd_ntab" align="center" width="200" style=" padding: 3px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">PERÍODO</span></td>
            <?php
            if($menu == "monitoria") {
            ?>
            <td class="corfd_ntab" align="center" style=" padding: 3px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">EXPRESSÃO DE PESQUISA</span></td>
            <?php
            }
            ?>
        </tr>
        <tr>
            <td class="corfd_colcampos" align="center">
                 <?php
                 $selperiodos = "SELECT * FROM periodo ORDER BY dataini DESC LIMIT 12";
                 $eselperiodos = $_SESSION['query']($selperiodos) or die (mysql_error());
                 $nperiodos = $_SESSION['num_rows']($eselperiodos);
                 ?>
                 <select name="periodoauto" id="periodoauto" style="margin: 5px;width:150px; border: 1px solid #69C;height:30px; padding:5px;background-color:#FFF; border-radius:5px">
                      <option value="">SELECIONE...</option>
                      <?php
                      if($nperiodos >= 1) {
                           while($lselperiodos = $_SESSION['fetch_array']($eselperiodos)) {
                           ?>
                           <option value="<?php echo $lselperiodos['idperiodo']."#".banco2data($lselperiodos['dataini'])."#".banco2data($lselperiodos['datafim'])."#".banco2data($lselperiodos['dtinictt'])."#".banco2data($lselperiodos['dtfimctt']);?>"><?php echo $lselperiodos['nmes']."-".$lselperiodos['ano'];?></option>
                           <?php
                           }
                      }
                      ?>
                 </select> 
            </td>
            <?php
            if($menu == "monitoria") {
            ?>
            <td class="corfd_colcampos" rowspan="2" align="center">
                <input type="text" style=" margin: 4px; width:350px; height:25px;font-size:14px; border: 1px solid #69C; text-align:center;border-radius:5px" value="" name="search" id="search" />
            </td>
            <?php
            }
            ?>
        </tr>
    </table><br/>
    <?php
    }
    ?>
</div>
<div style="width:50%; float:left">
<table width="510">
  <tr>
    <td colspan="5" align="center" class="corfd_ntab" style=" padding: 3px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">FILTROS GERAIS</span></td>
  </tr>
    <tr>
        <td class="corfd_coltexto" style="padding-left: 5px"><strong>ID MONITORIA</strong></td>
        <td colspan="3" class="corfd_colcampos"><input type="text" style="margin: 4px; width: 120px; height:25px;font-size:14px; border: 1px solid #69C; text-align:center;border-radius:5px" maxlength="9" name="ID" id="ID" value="<?php if(isset($_POST['ID'])) { echo mysql_real_escape_string($_POST['ID']);} if($varget['ID'] != "") { echo $varget['ID'];} if($_SESSION['varsconsult']['ID'] != "") { echo $_SESSION['varsconsult']['ID'];} ?>" /></td>
    </tr>
  <tr>
    <td width="121" class="corfd_coltexto" style="padding-left: 5px"><strong>DATA MONITORIA</strong></td>
    <td colspan="3" class="corfd_colcampos"><input type="hidden" name="menu" value="monitoria" />
        <input class="data" name="dtinimoni" id="dtinimoni" type="text" value="<?php if(isset($_POST['dtinimoni'])) { echo mysql_real_escape_string($_POST['dtinimoni']);} if($varget['dtinimoni'] != "") { echo banco2data($varget['dtinimoni']);}if($_SESSION['varsconsult']['dtinimoni'] != "") { echo banco2data($_SESSION['varsconsult']['dtinimoni']);}?>" /> ATÉ
        <input class="data" name="dtfimmoni" id="dtfimmoni" type="text" value="<?php if(isset($_POST['dtfimmoni'])) { echo $_POST['dtfimmoni'];} if($varget['dtfimmoni'] != "") { echo banco2data($varget['dtfimmoni']);}if($_SESSION['varsconsult']['dtfimmoni'] != "") { echo banco2data($_SESSION['varsconsult']['dtfimmoni']);}?>"/>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>DATA CONTATO</strong></td>
    <td colspan="3" class="corfd_colcampos">
        <input class="data" name="dtinictt" id="dtinictt" type="text" value="<?php if(isset($_POST['dtinictt'])) { echo $_POST['dtinictt'];} if($varget['dtinictt'] != "") { echo banco2data($varget['dtinictt']);}if($_SESSION['varsconsult']['dtinictt'] != "") { echo banco2data($_SESSION['varsconsult']['dtinictt']);}?>" /> ATÉ
        <input class="data" name="dtfimctt" id="dtfimctt" type="text" value="<?php if(isset($_POST['dtfimctt'])) { echo $_POST['dtfimctt'];} if($varget['dtfimctt'] != "") { echo banco2data($varget['dtfimctt']);}if($_SESSION['varsconsult']['dtfimctt'] != "") { echo banco2data($_SESSION['varsconsult']['dtfimctt']);}?>" />
    </td>
  </tr>
  <?php
  $periodo = periodo ();

  $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
  $efiltro = $_SESSION['query']($selfiltros) or die ("erro na consulta dos nomes dos filtros");
  while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
          $filtros[] = strtolower($lfiltros['idfiltro_nomes']);
	  $nome = $lfiltros['nomefiltro_nomes'];
	  $idfiltro = "id_".strtolower($lfiltros['nomefiltro_nomes']);
	  if($_SESSION['user_tabela'] == "user_web") {
	  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM userwebfiltro
                         INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = userwebfiltro.idrel_filtros
                         INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                         INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                         INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                         INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                         WHERE iduser_web='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
	  }
	  if($_SESSION['user_tabela'] == "user_adm") {
	  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM useradmfiltro
                         INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = useradmfiltro.idrel_filtros
                         INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                         INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                         INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                         INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                         WHERE iduser_adm='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
	  }
	  $efiltrodados = $_SESSION['query']($filtrodados) or die ("erro na consulta dos filtros realcionados os usuário");
	  echo "<tr>";
            echo "<td class=\"corfd_coltexto\" style=\"padding-left:5px\"><strong>".$nome."</strong></td>";
            echo "<td colspan=\"3\" class=\"corfd_colcampos\"><select style=\"width:98%;margin:4px; border: 1px solid #69C;height:30px; padding:5px;background-color:#FFF; border-radius:5px\" name=\"filtro_".strtolower($nome)."\" id=\"filtro_".strtolower($nome)."\">";
            echo "<option value=\"\" style=\"padding:5px\"";
            if(isset($_POST["filtro_".$nome]) OR $varget["filtro_".$nome] != "") {
            }
            else {
                echo "selected=\"selected\" disabled=\"disabled\"";
            }
            echo ">Selecione....</option>";
            while($lfiltrodados = $_SESSION['fetch_array']($efiltrodados)) {
                if(in_array($lfiltrodados['idindice'],$idsindice)) {
                }
                else {
                    $idsindice[] = $lfiltrodados['idindice'];
                }
                if(in_array($lfiltrodados['idfluxo'],$idsfluxo)) {
                }
                else {
                    $idsfluxo[] = $lfiltrodados['idfluxo'];
                }
                if($lfiltrodados[$idfiltro] == $_POST['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $varget['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $_SESSION['varsconsult']['filtro_'.strtolower($nome)]) {
                    $select = "selected=\"selected\"";
                }
                else {
                    $select = "";
                }
                echo "<option value=\"".$lfiltrodados[$idfiltro]."\" $select style=\"height:15px;padding:5px\">".$lfiltrodados['nomefiltro_dados']."</option>";
            }
            echo "</select><td>";
	 echo "</tr>";
  }
  ?>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>SUPERVISOR</strong></td>
    <td width="315" class="corfd_colcampos"><input style="margin: 4px;width:80%;height:25px; border: 1px solid #69C; text-align:center; font-size:14px; text-align:center;border-radius:5px" name="super_oper" id="super_oper" type="text" value="<?php if(isset($_POST['super'])) {  echo $_POST['super'];} if(!isset($_POST['super']) && $varget['super'] != "") { echo $varget['super'];} if($_SESSION['varsconsult']['super_oper'] != "") { echo $_SESSION['varsconsult']['super_oper'];}  ?>" /></td>
    <td bgcolor="#999999" class="corfd_coltexto"><strong>NCG</strong></td>
    <td width="20" class="corfd_colcampos"><input name="fg" id="fg" type="checkbox" value="fg" <?php if($_POST['fg'] == "fg") { echo "checked=\"checked\""; } else {} if(!isset($_POST['rel']) && $varget['fg'] != "") { echo "checked=\"checked\"";} if($_SESSION['varsconsult']['fg'] != "") { echo "checked=\"checked\"";}?>/></td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>OPERADOR</strong></td>
    <td class="corfd_colcampos" colspan="3"><input style="margin: 4px;width:98%;border-radius:5px; height:25px; border: 1px solid #69C; text-align:center; font-size:14px; text-align:center" name="oper" id="oper" type="text" value="<?php if(isset($_POST['oper'])) { echo $_POST['oper'];} if(!isseT($_POST['oper']) && $varget['oper'] != "") { echo $varget['oper'];} if($_SESSION['varsconsult']['oper'] != "") { echo $_SESSION['varsconsult']['oper'];}?>" /></td>
    <!--<td width="34" bgcolor="#999999" class="corfd_coltexto"><strong>NCGM</strong></td>
    <td width="20" class="corfd_colcampos"><input name="fgm" id="fgm" type="checkbox" value="fgm" <?php if($_POST['fgm'] == "fgm") { echo "checked=\"checked\""; } else {} if(!isseT($_POST['rel']) && $varget['fgm'] != "") {   echo "checked=\"checked\"";} else {}?> /></td>-->
  </tr>
  <?php
  if($_SESSION['user_tabela'] == "user_adm") {
      ?>
      <tr>
        <td class="corfd_coltexto" style="padding-left: 5px"><strong>MONITOR</strong></td>
        <td colspan="3" class="corfd_colcampos"><input style="margin: 4px;width:98%;height:25px; border: 1px solid #69C; text-align:center; font-size:14px; text-align:center;border-radius:5px" name="nomemonitor" id="nomemonitor" type="text" value="<?php if(isset($_POST['nomemonitor'])) { echo $_POST['nomemonitor'];} if(!isset($_POST['nomemonitor']) && $varget['nomemonitor'] != "") { echo $varget['nomemonitor'];} if($_SESSION['varsconsult']['nomemonitor'] != "") { echo $_SESSION['varsconsult']['nomemonitor'];}?>" /></td>
      </tr>
      <?php
  }
  else {
  }
  ?>
  <tr>
      <td class="corfd_coltexto" style="padding-left: 5px"><strong>NOTA</strong></td>
      <td class="corfd_colcampos" colspan="3"><input style="margin: 4px;width:120px;height:25px; border: 1px solid #69C; text-align:center; font-size:14px; text-align:center;border-radius:5px" name="valor_fg" id="valor_fg" type="text" value="<?php if(isset($_POST['valor_fg'])) { echo $_POST['valor_fg'];} if(!isset($_POST['valor_fg']) && $varget['valor_fg'] != "") { echo $varget['valor_fg'];} if($_SESSION['varsconsult']['valor_fg'] != "") { echo $_SESSION['varsconsult']['valor_fg'];}  ?>" /></td>
  </tr>
  <tr>
      <td class="corfd_coltexto" style="padding-left: 5px"><strong>ALERTA</strong></td>
      <td class="corfd_colcampos" colspan="3">
          <select name="idalerta" id="idalerta" style="margin: 4px;width:98%;border: 1px solid #333; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px">
              <option value="" <?php if(isset($_POST['idalerta']) OR $varget['idalerta'] != "") { } else { echo "selected=\"selected\"";}?> disabled="disabled" style="height:15px;padding:5px">Selecione...</option>
              <?php
              $selalerta = "SELECT * FROM alertas";
              $ealerta = $_SESSION['query']($selalerta) or die (mysql_error());
              while ($lalerta = $_SESSION['fetch_array']($ealerta)) {
                  if($lalerta['userfg'] == "S") {
                      $selemail = "SELECT count(*) as result FROM relemailfg WHERE idrel_filtros IN (".implode(",",$idsindice).")";
                      $eselemail = $_SESSION['fetch_array']($_SESSION['query']($selemail)) or die (mysql_error());
                      if($lalerta['idalertas'] == $_POST['idalerta'] OR $lalerta['idalertas'] == $varget['idalerta'] OR $lalerta['idalertas'] == $_SESSION['varconsult']['idalerta']) {
                          $selectal = "selected=\"selected\"";
                      }
                      else {
                          $selectal = "";
                      }
                      ?>
                      <option value="<?php echo $lalerta['idalertas'];?>" <?php echo $selectal;?> style="height:15px;padding:5px"><?php echo $lalerta['nomealerta'];?></option>
                      <?php
                  }
                  else {
                      $useral = explode(",",$lalerta[$_SESSION['user_tabela']]);
                      if(in_array($_SESSION['usuarioID'],$useral)) {
                          if($lalerta['idalertas'] == $_POST['idalerta'] OR $lalerta['idalertas'] == $varget['idalerta'] OR $lalerta['idalertas'] == $_SESSION['varconsult']['idalerta']) {
                              $selectal = "selected=\"selected\"";
                          }
                          else {
                              $selectal = "";
                          }
                          ?>
                          <option value="<?php echo $lalerta['idalertas'];?>" <?php echo $selectal;?> style="height:15px;padding:5px"><?php echo $lalerta['nomealerta'];?></option>
                          <?php
                      }
                  }
              }
              ?>
          </select>
      </td>
  </tr>
</table>
</div>
<div style="width:49%; float:left">
<table width="510">
  <tr>
          <td class="corfd_ntab" colspan="2" align="center" style="padding: 3px"><span style=" color: #FFF; font-size: 14px; font-weight: bold">FILTRO PLANILHA</span></td>
  </tr>
  <tr>
    <td width="128" class="corfd_coltexto" style="padding-left: 5px"><strong>PLANILHA</strong></td>
        <td width="370" class="corfd_colcampos"><select style="margin: 4px;width:98%; border: 1px solid #333; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="plan" id="plan">
    <option value="" <?php if(isset($_POST['plan']) OR $varget['plan'] != "") { } else { echo "selected=\"selected\""; }?> disabled="disabled" style="height:15px;padding:5px">Selecione uma planilha</option>
    <?php
        if($_SESSION['user_tabela'] == "user_web") {
          $selplan = "SELECT * FROM userwebfiltro INNER JOIN conf_rel ON conf_rel.idrel_filtros = userwebfiltro.idrel_filtros WHERE iduser_web='$iduser'";
        }
        if($_SESSION['user_tabela'] == "user_adm") {
          $selplan = "SELECT * FROM useradmfiltro INNER JOIN conf_rel ON conf_rel.idrel_filtros = useradmfiltro.idrel_filtros WHERE iduser_adm='$iduser'";
        }
        $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das planilhas");
        $listplan = array();
        while($lselplan = $_SESSION['fetch_array']($eselplan)) {
            $plans = explode(",",$lselplan['idplanilha_web']);
            foreach($plans as $p) {
                if(!in_array($p, $listplan)) {
                    $listplan[] = $p;
                }
            }
            foreach($plans as $plan) {
                $vincplan = "SELECT planvinc FROM vinc_plan WHERE idplanilha='$plan'";
                $evincplan = $_SESSION['query']($vincplan) or die ("erro na query de consutla das planilha vinculadas");
                while($lvincplan = $_SESSION['fetch_array']($evincplan)) {
                    if($lvincplan['planvinc'] != "") {
                        if(!in_array($lvincplan['planvinc'], $listplan)) {
                            $listplan[] = $lvincplan['planvinc'];
                        }
                    }
                }
            }
        }
        
        foreach($listplan as $p) {
            $nplan = "SELECT DISTINCT(idplanilha), COUNT(idplanilha) as result, descriplanilha FROM planilha WHERE idplanilha='$p' AND ativoweb='S'";
            $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die ("erro na query de consulta do nome da planilha");
            if($enplan['result'] >= 1) {
                $plannomes[$p] = $enplan['descriplanilha'];
            }
        }
        asort($plannomes);

        foreach($plannomes as $kp => $descri) {
            if($kp == $_POST['plan'] OR $kp == $varget['plan'] OR $kp == $_SESSION['varsconsult']['plan']) {
                   $select = "selected=\"selected\"";
                }
                else {
                   $select = "";
                }
            echo "<option value=\"".$kp."\" $select style=\"height:15px;padding:5px\">".$descri."</option>";
            }
        ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>AVALIAÇÃO</strong></td>
    <td class="corfd_colcampos"><select style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="aval" id="aval">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione uma Avaliação</option>
    <?php
      if(!empty($_POST['aval'])) {
            $varplan = $_POST['plan'];
            $selnaval = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$_POST['aval']."'";
            $enaval = $_SESSION['fetch_array']($_SESSION['query']($selnaval)) or die ("erro na query de consulta do nome da avaliação");
            echo "<option value=\"".$_POST['aval']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enaval['nomeaval_plan']."</option>";
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='".$_POST['plan']."'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
              $idavaldb[] = $lselaval['idaval_plan'];
              $nomeavaldb[] = $lselaval['nomeaval_plan'];
            }
            $avaliaplan = array_combine($idavaldb, $nomeavaldb);
            foreach($avaliaplan as $id => $nome) {
              if($id == $_POST['aval']) {
              }
              else {
               echo "<option value=\"".$id."\" style=\"height:15px;padding:5px\">".$nome."</option>";
              }
            }
      }
      if($varget['aval'] != "") {
            $varplan = $varget['plan'];
            $selnaval = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$varget['aval']."'";
            $enaval = $_SESSION['fetch_array']($_SESSION['query']($selnaval)) or die ("erro na query de consulta do nome da avaliação");
            echo "<option value=\"".$varget['aval']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enaval['nomeaval_plan']."</option>";
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='".$varget['plan']."'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
              $idavaldb[] = $lselaval['idaval_plan'];
              $nomeavaldb[] = $lselaval['nomeaval_plan'];
            }
            $avaliaplan = array_combine($idavaldb, $nomeavaldb);
            foreach($avaliaplan as $id => $nome) {
              if($id == $varget['aval']) {
              }
              else {
               echo "<option value=\"".$id."\" style=\"height:15px;padding:5px\">".$nome."</option>";
              }
            }
      }
      if($_SESSION['varsconsult']['aval'] != "") {
            $varplan = $_SESSION['varsconsult']['plan'];
            $selnaval = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$_SESSION['varsconsult']['aval']."'";
            $enaval = $_SESSION['fetch_array']($_SESSION['query']($selnaval)) or die ("erro na query de consulta do nome da avaliação");
            echo "<option value=\"".$_SESSION['varsconsult']['aval']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enaval['nomeaval_plan']."</option>";
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='".$_SESSION['varsconsult']['plan']."'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
              $idavaldb[] = $lselaval['idaval_plan'];
              $nomeavaldb[] = $lselaval['nomeaval_plan'];
            }
            $avaliaplan = array_combine($idavaldb, $nomeavaldb);
            foreach($avaliaplan as $id => $nome) {
              if($id == $_SESSION['varsconsult']['aval']) {
              }
              else {
               echo "<option value=\"".$id."\" style=\"height:15px;padding:5px\">".$nome."</option>";
              }
            }
      }
      if(empty($_POST['aval']) AND $varget['aval'] == "" AND (!empty($_POST['plan']) OR $varget['plan'] != "") AND $_SESSION['varsconsult']['aval'] == "") {
          if(!empty ($_POST['plan'])) {
              $varplan = $_POST['plan'];
          }
          if($varget['plan'] != "") {
              $varplan = $varget['plan'];
          }
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='$varplan'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
                echo "<option value=\"".$lselaval['idaval_plan']."\" style=\"height:15px;padding:5px\">".$lselaval['nomeaval_plan']."</option>";
            }
      }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>GRUPO</strong></td>
    <td class="corfd_colcampos"><select style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="grupo" id="grupo">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione um Grupo</option>
    <?php
      if(!empty($_POST['grupo'])) {
            $varaval = $_POST['aval'];
            $selngrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='".$_POST['grupo']."'";
            $engrupo = $_SESSION['fetch_array']($_SESSION['query']($selngrupo)) or die ("erro na query para consulta do nome do grupo");
            echo "<option value=\"".$_POST['grupo']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$engrupo['descrigrupo']."</option>";
            $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idaval_plan='".$_POST['aval']."'";
            $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
            while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              $idgrupo[] = $lselgrupo['idgrupo'];
              $nomegrupo[] = $lselgrupo['descrigrupo'];
            }
            $grupoidnome = array_combine($idgrupo, $nomegrupo);
            foreach($grupoidnome as $idg => $nomeg) {
              if($idg == $_POST['grupo']) {
              }
              else {
                echo "<option value=\"".$idg."\" style=\"height:15px;padding:5px\">".$nomeg."</option>";
              }
            }
      }
      if($varget['grupo'] != "") {
            $varaval = $_POST['aval'];
            $selngrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='".$varget['grupo']."'";
            $engrupo = $_SESSION['fetch_array']($_SESSION['query']($selngrupo)) or die ("erro na query para consulta do nome do grupo");
            echo "<option value=\"".$varget['grupo']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$engrupo['descrigrupo']."</option>";
            $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idaval_plan='".$varget['aval']."'";
            $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
            while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              $idgrupo[] = $lselgrupo['idgrupo'];
              $nomegrupo[] = $lselgrupo['descrigrupo'];
            }
            $grupoidnome = array_combine($idgrupo, $nomegrupo);
            foreach($grupoidnome as $idg => $nomeg) {
              if($idg == $varget['grupo']) {
              }
              else {
                echo "<option value=\"".$idg."\" style=\"height:15px;padding:5px\">".$nomeg."</option>";
              }
            }
      }
      if($_SESSION['varsconsult']['grupo'] != "") {
            $varaval = $_POST['aval'];
            $selngrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='".$_SESSION['varsconsult']['grupo']."'";
            $engrupo = $_SESSION['fetch_array']($_SESSION['query']($selngrupo)) or die ("erro na query para consulta do nome do grupo");
            echo "<option value=\"".$_SESSION['varsconsult']['grupo']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$engrupo['descrigrupo']."</option>";
            $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$_SESSION['varsconsult']['plan']."' AND planilha.idaval_plan='".$_SESSION['varsconsult']['aval']."'";
            $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
            while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              $idgrupo[] = $lselgrupo['idgrupo'];
              $nomegrupo[] = $lselgrupo['descrigrupo'];
            }
            $grupoidnome = array_combine($idgrupo, $nomegrupo);
            foreach($grupoidnome as $idg => $nomeg) {
              if($idg == $_SESSION['varsconsult']['grupo']) {
              }
              else {
                echo "<option value=\"".$idg."\" style=\"height:15px;padding:5px\">".$nomeg."</option>";
              }
            }
      }
      if(empty($_POST['grupo']) AND $varget['grupo'] == "" AND (!empty ($_POST['aval']) OR $varget['aval'] != "") AND $_SESSION['varsconsult']['grupo'] == "") {
          if(!empty ($_POST['aval'])) {
              $varaval = $_POST['aval'];
          }
          if($varget['aval'] != "") {
              $varaval = $varget['aval'];
          }
          $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$varplan."' AND planilha.idaval_plan='".$varaval."'";
          $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
          while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              echo "<option value=\"".$lselgrupo['idgrupo']."\" style=\"height:15px;padding:5px\">".$lselgrupo['descrigrupo']."</option>";
          }
      }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>SUB-GRUPO</strong></td>
    <td class="corfd_colcampos"><select style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="sub" id="sub">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione um Sub-Grupo</option>
    <?php
      if(!empty($_POST['sub'])) {
            $vargrupo = $_POST['grupo'];
            $selnsub = "SELECT descrisubgrupo FROM subgrupo WHERE idsubgrupo='".$_POST['sub']."'";
            $ensub = $_SESSION['fetch_array']($_SESSION['query']($selnsub)) or die ("erro na query de consulta do sub-grupo");
            echo "<option value=\"".$_POST['sub']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$ensub['descrisubgrupo']."</option>";
            $selsub = "SELECT DISTINCT(subgrupo.idsubgrupo), subgrupo.descrisubgrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                       WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."'";
            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos sub-grupo");
            while($lselsub = $_SESSION['fetch_array']($eselsub)) {
              $idsub[] = $lselsub['idsubgrupo'];
              $nomesel[] = $lselsub['descrisubgrupo'];
            }
            $grupoidsub = array_combine($idsub, $nomesel);
            foreach($grupoidsub as $ids => $nomes) {
              if($ids == $_POST['sub']) {
              }
              else {
                echo "<option value=\"".$ids."\" style=\"height:15px;padding:5px\">".$nomes."</option>";
              }
            }
      }
      if($varget['sub'] != "") {
            $vargrupo = $_POST['grupo'];
            $selnsub = "SELECT descrisubgrupo FROM subgrupo WHERE idsubgrupo='".$varget['sub']."'";
            $ensub = $_SESSION['fetch_array']($_SESSION['query']($selnsub)) or die ("erro na query de consulta do sub-grupo");
            echo "<option value=\"".$varget['sub']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$ensub['descrisubgrupo']."</option>";
            $selsub = "SELECT DISTINCT(subgrupo.idsubgrupo), subgrupo.descrisubgrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                       WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."'";
            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos sub-grupo");
            while($lselsub = $_SESSION['fetch_array']($eselsub)) {
              $idsub[] = $lselsub['idsubgrupo'];
              $nomesel[] = $lselsub['descrisubgrupo'];
            }
            $grupoidsub = array_combine($idsub, $nomesel);
            foreach($grupoidsub as $ids => $nomes) {
              if($ids == $varget['sub']) {
              }
              else {
                echo "<option value=\"".$ids."\" style=\"height:15px;padding:5px\">".$nomes."</option>";
              }
            }
      }
      if(empty ($_POST['sub']) AND $varget['sub'] == "" AND (!empty($_POST['grupo']) OR $varget['grupo'] != "")) {
          if(!empty ($_POST['grupo'])) {
              $vargrupo = $_POST['grupo'];
          }
          if($varget['grupo'] != "") {
              $vargrupo = $varget['grupo'];
          }
          $selsub = "SELECT DISTINCT(idrel), subgrupo.descrisubgrupo, planilha.idgrupo, grupo.filtro_vinc FROM planilha
	      INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	      INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
	      WHERE planilha.idplanilha='$varplan' AND planilha.idaval_plan='$varaval' AND planilha.idgrupo='$vargrupo' AND filtro_vinc='S'";
          $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos sub-grupo");
          while($lselsub = $_SESSION['fetch_array']($eselsub)) {
            echo "<option value=\"".$lselsub['idrel']."\" style=\"height:15px;padding:5px\">".$lselsub['descrisubgrupo']."</option>";
          }
      }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>PERGUNTA</strong></td>
    <td class="corfd_colcampos"><select style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="perg" id="perg">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione uma Pergunta</option>
    <?php
    if(!empty($_POST['perg'])) {
      if(!empty ($_POST['grupo'])) {
          $vargrupo = $_POST['grupo'];
          $varperg = $_POST['perg'];
      }
      if($varget['grupo'] != "") {
          $vargrupo = $varget['grupo'];
          $varperg = $varget['perg'];
      }
      $selnperg = "SELECT descripergunta FROM pergunta WHERE idpergunta='".$_POST['perg']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de consulta do nome da pergunta");
      echo "<option value=\"".$_POST['perg']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enperg['descripergunta']."</option>";
      if(!empty($_POST['sub'])) {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha
                     INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                     INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
                     WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."' AND subgrupo.idsubgrupo='".$_POST['sub']."'";
      }
      else {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                     INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                     WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."'";
      }
      $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
      while($lselperg = $_SESSION['fetch_array']($eselperg)) {
	$idperg[] = $lselperg['idpergunta'];
	$nomeperg[] = $lselperg['descripergunta'];
      }
      $pergid = array_combine($idperg, $nomeperg);
      foreach($pergid as $idp => $nomep) {
          if($idp == $_POST['perg']) {
          }
          else {
            echo "<option value=\"".$idp."\" style=\"height:15px;padding:5px\">".$nomep."</option>";
          }
      }
    }
    if($varget['perg'] != "") {
      if(!empty ($_POST['grupo'])) {
          $vargrupo = $_POST['grupo'];
          $varperg = $_POST['perg'];
      }
      if($varget['grupo'] != "") {
          $vargrupo = $varget['grupo'];
          $varperg = $varget['perg'];
      }
      $selnperg = "SELECT descripergunta FROM pergunta WHERE idpergunta='".$varget['perg']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de consulta do nome da pergunta");
      echo "<option value=\"".$varget['perg']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enperg['descripergunta']."</option>";
      if(!empty($varget['sub'])) {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha
                     INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                     INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
                     WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."' AND subgrupo.idsubgrupo='".$varget['sub']."'";
      }
      else {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                     INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                     WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."'";
      }
      $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
      while($lselperg = $_SESSION['fetch_array']($eselperg)) {
	$idperg[] = $lselperg['idpergunta'];
	$nomeperg[] = $lselperg['descripergunta'];
      }
      $pergid = array_combine($idperg, $nomeperg);
      foreach($pergid as $idp => $nomep) {
          if($idp == $varget['perg']) {
          }
          else {
            echo "<option value=\"".$idp."\" style=\"height:15px;padding:5px\">".$nomep."</option>";
          }
      }
    }
    if(empty ($_POST['sub'])) {
        if(empty($_POST['perg']) AND $varget['perg'] == "" AND (!empty($_POST['grupo']) OR $varget['grupo'] != "")) {
            if(!empty ($_POST['perg'])) {
              $varperg = $_POST['perg'];
            }
            if($varget['perg'] != "") {
              $varperg = $varget['perg'];
            }
            $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                                INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                                WHERE planilha.idplanilha='$varplan' AND planilha.idgrupo='$vargrupo'";
            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                echo "<option value=\"".$lselperg['idpergunta']."\" style=\"height:15px;padding:5px\">".$lselperg['descripergunta']."</option>";
            }
        }
    }
    if(!empty ($_POST['sub'])) {
        if(empty($_POST['perg']) AND $varget['perg'] == "" AND (!empty($_POST['sub']) OR $varget['sub'] != "")) {
            if(!empty ($_POST['perg'])) {
              $varperg = $_POST['perg'];
            }
            if($varget['perg'] != "") {
              $varperg = $varget['perg'];
            }
            $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha
                                 INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                                 INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                                 INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
                                WHERE planilha.idplanilha='$varplan' AND planilha.idgrupo='$vargrupo' AND subgrupo.idsubgrupo='$varsub'";
            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                echo "<option value=\"".$lselperg['idpergunta']."\" style=\"height:15px;padding:5px\">".$lselperg['descripergunta']."</option>";
            }
        }
    }
   ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>RESPOSTA</strong></td>
    <td class="corfd_colcampos"><select style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="resp" id="resp">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione uma Resposta</option>
    <?php
    if(!empty($_POST['resp'])) {
      $selnperg = "SELECT descriresposta FROM pergunta WHERE idresposta='".$_POST['resp']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de para selecionar o nome");
      echo "<option value=\"".$_POST['resp']."\" selected=\"selected\" style=\"height:15px;padding:5px; width:400px\">".$enperg['descriresposta']."</option>";
      $selresp = "SELECT * FROM planilha
                          INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                          INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                          WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."' AND pergunta.idpergunta='".$_POST['perg']."'";
      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
      while($lselresp = $_SESSION['fetch_array']($eselresp)) {
	$idresp[] = $lselresp['idresposta'];
	$nomeresp[] = $lselresp['descriresposta'];
      }
      $respid = array_combine($idresp, $nomeresp);
      foreach($respid as $idr => $nomer) {
          if($idr == $_POST['resp']) {
          }
          else {
            echo "<option value=\"".$idr."\" style=\"height:15px;padding:5px;width:400px\">".$nomer."</option>";
          }
      }
    }
    if($varget['resp'] != "") {
      $selnperg = "SELECT descriresposta FROM pergunta WHERE idresposta='".$varget['resp']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de para selecionar o nome");
      echo "<option value=\"".$varget['resp']."\" selected=\"selected\" style=\"height:15px;padding:5px;width:400px\">".$enperg['descriresposta']."</option>";
      $selresp = "SELECT * FROM planilha
                          INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                          INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                          WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."' AND pergunta.idpergunta='".$varget['perg']."'";
      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
      while($lselresp = $_SESSION['fetch_array']($eselresp)) {
	$idresp[] = $lselresp['idresposta'];
	$nomeresp[] = $lselresp['descriresposta'];
      }
      $respid = array_combine($idresp, $nomeresp);
      foreach($respid as $idr => $nomer) {
          if($idr == $varget['resp']) {
          }
          else {
            echo "<option value=\"".$idr."\" style=\"height:15px;padding:5px;width:400px\">".$nomer."</option>";
          }
      }
    }
    if(empty ($_POST['resp']) AND $varget['resp'] == "" AND (!empty ($_POST['perg']) OR $varget['perg'] != "")) {
        $selresp = "SELECT * FROM planilha
                          INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                          INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                          WHERE planilha.idplanilha='$varplan' AND planilha.idgrupo='$vargrupo' AND pergunta.idpergunta='$varperg'";
      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
      while($lselresp = $_SESSION['fetch_array']($eselresp)) {
          echo "<option value=\"".$lselresp['idresposta']."\" style=\"height:15px;padding:5px\">".$lselresp['descriresposta']."</option>";
      }
    }
  ?>
    </select></td>
  </tr>
</table>
</div>
<div style="width:510px; float:left">
    <table width="508">
    <tr>
        <td colspan="2" class="corfd_ntab" align="center" style=" padding: 3px"><span style="font-size: 13px; font-weight: bold; color: #FFF">FILTRO DE CONFIGURAÇÕES</span></td>
    </tr>
    <tr>
            <td width="126" class="corfd_coltexto" style="padding-left: 5px"><strong>FLUXO</strong></td>
            <td class="corfd_colcampos">
                <select name="fluxo" id="fluxo" style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px">
                    <?php
                    if(isset($_POST['fluxo'])) {
                        ?>
                        <option value="" style="height:15px;padding:5px">Selecione...</option>
                        <?php
                    }
                    else {
                        ?>
                        <option value="" selected="selected" style="height:15px;padding:5px">Selecione...</option>
                        <?php
                    }
                    $selfluxo = "SELECT idfluxo, nomefluxo FROM fluxo WHERE idfluxo IN ('".implode("','",$idsfluxo)."')";
                    $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta dos fluxos");
                    while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                        if($lselfluxo['idfluxo'] == $_POST['fluxo'] OR $lselfluxo['idfluxo'] == $_SESSION['varsconsult']['fluxo']) {
                            echo "<option value=\"".$lselfluxo['idfluxo']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$lselfluxo['nomefluxo']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lselfluxo['idfluxo']."\" style=\"height:15px;padding:5px\">".$lselfluxo['nomefluxo']."</option>";
                        }
                    }                    
                    ?>                    
                </select>
            </td>
        </tr>
      <tr>
        <td width="126" class="corfd_coltexto" style="padding-left: 5px"><strong>STATUS ATUAL</strong></td>
        <td width="370" class="corfd_colcampos">
            <select name="atustatus" id="atustatus" style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px">
            <?php
            if(isset($_POST['atustatus'])) {
                ?>
                <option value="" style="height:15px;padding:5px">Selecione...</option>
                <?php
            }
            else {
                ?>
                <option value="" selected="selected" style="height:15px;padding:5px">Selecione...</option>
                <?php
            }
            if(isset($_POST['fluxo'])) {
                $selstatus = "SELECT idstatus, nomestatus FROM status WHERE idfluxo='".$_POST['fluxo']."'";
                $esels = $_SESSION['query']($selstatus) or die ("erro na query de consulta do status");
                while($lsels = $_SESSION['fetch_array']($esels)) {
                    if($lsels['idstatus'] == $_POST['atustatus'] OR $lsels['idstatus'] == $_SESSION['varsconsult']['atustatus']) {
                        echo "<option value=\"".$lsels['idstatus']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$lsels['nomestatus']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lsels['idstatus']."\" style=\"height:15px;padding:5px\">".$lsels['nomestatus']."</option>";
                    }
                }
            }
            if(isset($_SESSION['varsconsult']['fluxo'])) {
                $selstatus = "SELECT idstatus, nomestatus FROM status WHERE idfluxo='".$_SESSION['varsconsult']['fluxo']."'";
                $esels = $_SESSION['query']($selstatus) or die ("erro na query de consulta do status");
                while($lsels = $_SESSION['fetch_array']($esels)) {
                    if($lsels['idstatus'] == $_SESSION['varsconsult']['atustatus']) {
                        echo "<option value=\"".$lsels['idstatus']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$lsels['nomestatus']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lsels['idstatus']."\" style=\"height:15px;padding:5px\">".$lsels['nomestatus']."</option>";
                    }
                }
            }
            ?>
            </select>
        </td>
      </tr>
      <tr>
          <td class="corfd_coltexto" style="padding-left: 5px"><strong>INDICE NOTAS</strong></td>
          <td class="corfd_colcampos">
              <select name="indice" id="indice" style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px">
                  <?php
                  if(isset($_POST['indice'])) {
                      ?>
                      <option value="" style="height:15px;padding:5px">Selecione...</option>
                      <?php
                  }
                  else {
                      ?>
                      <option value="" selected="selected" style="height:15px;padding:5px">Selecione...</option>
                      <?php
                  }                 
                  $selindice = "SELECT * FROM indice WHERE idindice IN ('".implode("','",$idsindice)."')";
                  $eselindice = $_SESSION['query']($selindice) or die ("erro na query de consulta dos indices");
                  while($lselindice = $_SESSION['fetch_array']($eselindice)) {
                      if($lselindice['idindice'] == $_POST['indice'] OR $lselindice['idindice'] == $_SESSION['varsconsult']['indice']) {
                          echo "<option value=\"".$lselindice['idindice']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$lselindice['nomeindice']."</option>";
                      }
                      else {
                          echo "<option value=\"".$lselindice['idindice']."\" style=\"height:15px;padding:5px\">".$lselindice['nomeindice']."</option>";
                      }
                  }
                  ?>
              </select>
          </td>
      </tr>
      <tr>
        <td class="corfd_coltexto" style="padding-left: 5px"><strong>INTERVALO NOTA</strong></td>
        <td class="corfd_colcampos">
            <select name="intervalo" id="intervalo" style="margin: 4px;width:98%;border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px">
            <?php
            if(isset($_POST['intervalo'])) {
                ?>
                <option value="" style="height:15px;padding:5px">Selecione...</option>
                <?php
            }
            else {
                ?>
                <option value="" selected="selected" style="height:15px;padding:5px">Selecione...</option>
                <?php
            }
            if(isset($_POST['indice'])) {
                $selindice = "SELECT * FROM intervalo_notas WHERE idindice='".$_POST['indice']."'";
                $eselindice = $_SESSION['query']($selindice) or die ("erro na query de consulta do intervalo");
                while($lselindice = $_SESSION['fetch_array']($eselindice)) {
                    if($lselindice['idintervalo_notas'] == $_POST['intervalo']) {
                        echo "<option value=\"".$lselindice['idintervalo_notas']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$lselindice['nomeintervalo_notas']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lselindice['idintervalo_notas']."\" style=\"height:15px;padding:5px\">".$lselindice['nomeintervalo_notas']."</option>";
                    }
                }
            }
            if(isset($_SESSION['varsconsult']['indice'])) {
                $selindice = "SELECT * FROM intervalo_notas WHERE idindice='".$_SESSION['varsconsult']['indice']."'";
                $eselindice = $_SESSION['query']($selindice) or die ("erro na query de consulta do intervalo");
                while($lselindice = $_SESSION['fetch_array']($eselindice)) {
                    if($lselindice['idintervalo_notas'] == $_POST['intervalo'] OR $lselindice['idintervalo_notas'] == $_SESSION['varsconsult']['intervalo']) {
                        echo "<option value=\"".$lselindice['idintervalo_notas']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$lselindice['nomeintervalo_notas']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lselindice['idintervalo_notas']."\" style=\"height:15px;padding:5px\">".$lselindice['nomeintervalo_notas']."</option>";
                    }
                }
            }
            ?>
            </select>
        </td>
      </tr>
    </table>

</div>

<?php
}

function filtros_divs_calib($vals) {
    
$iduser = $_SESSION['usuarioID'];
$tabuser = $_SESSION['user_tabela'];

?>
<br />
<div style="width:1024px;">
    <input type="button" onclick="javascript:window.location = '<?php echo str_replace("&reset=1","",$_SERVER['REQUEST_URI'])."&reset=1";?>'" name="reset" id="reset" value="RESET FILTROS" style="width:150px;background:#EAAC2F; border:2px solid #FFF; font-size: 13px; font-weight: bolder;border-radius:5px" />
</div><br/>
<div style="width:510px; float:left">
<table width="510">
  <tr>
    <td colspan="5" align="center" class="corfd_ntab"><strong>FILTROS GERAIS</strong></td>
  </tr>
  <tr>
    <td width="121" class="corfd_coltexto" style="padding-left: 5px"><strong>DATA CALIBRAGEM</strong></td>
    <td colspan="3" class="corfd_colcampos"><input type="hidden" name="menu" value="monitoria" />
        <input style="margin: 4px;width:100px; height:20px; border: 1px solid #69C; text-align:center; font-size:12px;border-radius:5px" class="data" name="dtinimoni" id="dtinimoni" type="text" value="<?php if(isset($_POST['dtinimoni'])) { echo mysql_real_escape_string($_POST['dtinimoni']);} if($varget['dtinimoni'] != "") { echo banco2data($varget['dtinimoni']);}if($_SESSION['varsconsult']['dtinimoni'] != "") { echo banco2data($_SESSION['varsconsult']['dtinimoni']);}?>" /> ATÉ
        <input style="margin: 4px;width:100px;height:20px; border: 1px solid #69C; text-align:center; font-size:12px;border-radius:5px" class="data" name="dtfimmoni" id="dtfimmoni" type="text" value="<?php if(isset($_POST['dtfimmoni'])) { echo $_POST['dtfimmoni'];} if($varget['dtfimmoni'] != "") { echo banco2data($varget['dtfimmoni']);}if($_SESSION['varsconsult']['dtfimmoni'] != "") { echo banco2data($_SESSION['varsconsult']['dtfimmoni']);}?>"/>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto" style="padding-left: 5px"><strong>DATA CONTATO</strong></td>
    <td colspan="3" class="corfd_colcampos">
        <input style="margin: 4px;width: 100px;height:20px; border: 1px solid #69C; text-align:center;font-size:12px;border-radius:5px" class="data" name="dtinictt" id="dtinictt" type="text" value="<?php if(isset($_POST['dtinictt'])) { echo $_POST['dtinictt'];} if($varget['dtinictt'] != "") { echo banco2data($varget['dtinictt']);}if($_SESSION['varsconsult']['dtinictt'] != "") { echo banco2data($_SESSION['varsconsult']['dtinictt']);}?>" /> ATÉ
        <input style="margin: 4px;width: 100px;height:20px; border: 1px solid #69C; text-align:center; font-size:12px;border-radius:5px" class="data" name="dtfimctt" id="dtfimctt" type="text" value="<?php if(isset($_POST['dtfimctt'])) { echo $_POST['dtfimctt'];} if($varget['dtfimctt'] != "") { echo banco2data($varget['dtfimctt']);}if($_SESSION['varsconsult']['dtfimctt'] != "") { echo banco2data($_SESSION['varsconsult']['dtfimctt']);}?>" />
    </td>
  </tr>
  <?php
  $periodo = periodo ();

  $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
  $efiltro = $_SESSION['query']($selfiltros) or die ("erro na consulta dos nomes dos filtros");
  while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
          $filtros[] = strtolower($lfiltros['idfiltro_nomes']);
	  $nome = $lfiltros['nomefiltro_nomes'];
	  $idfiltro = "id_".strtolower($lfiltros['nomefiltro_nomes']);
	  if($_SESSION['user_tabela'] == "user_web") {
	  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM userwebfiltro
                         INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = userwebfiltro.idrel_filtros
                         INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                         INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                         INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                         INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                         WHERE iduser_web='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
	  }
	  if($_SESSION['user_tabela'] == "user_adm") {
	  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM useradmfiltro
                         INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = useradmfiltro.idrel_filtros
                         INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                         INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                         INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                         INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                         WHERE iduser_adm='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
	  }
	  $efiltrodados = $_SESSION['query']($filtrodados) or die ("erro na consulta dos filtros realcionados os usuário");
	  echo "<tr>";
            echo "<td class=\"corfd_coltexto\"><strong>".$nome."</strong></td>";
            echo "<td colspan=\"3\" class=\"corfd_colcampos\"><select style=\"width:350px; border: 1px solid #69C;height:30px; padding:5px;background-color:#FFF; border-radius:5px\" name=\"filtro_".strtolower($nome)."\" id=\"filtro_".strtolower($nome)."\">";
            echo "<option value=\"\" style=\"padding:5px\"";
            if(isset($_POST["filtro_".$nome]) OR $varget["filtro_".$nome] != "") {
            }
            else {
                echo "selected=\"selected\" disabled=\"disabled\"";
            }
            echo ">Selecione....</option>";
            while($lfiltrodados = $_SESSION['fetch_array']($efiltrodados)) {
                if(in_array($lfiltrodados['idindice'],$idsindice)) {
                }
                else {
                    $idsindice[] = $lfiltrodados['idindice'];
                }
                if(in_array($lfiltrodados['idfluxo'],$idsfluxo)) {
                }
                else {
                    $idsfluxo[] = $lfiltrodados['idfluxo'];
                }
                if($lfiltrodados[$idfiltro] == $_POST['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $varget['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $_SESSION['varsconsult']['filtro_'.strtolower($nome)]) {
                    $select = "selected=\"selected\"";
                }
                else {
                    $select = "";
                }
                echo "<option value=\"".$lfiltrodados[$idfiltro]."\" $select style=\"height:15px;padding:5px\">".$lfiltrodados['nomefiltro_dados']."</option>";
            }
            echo "</select><td>";
	 echo "</tr>";
  }
  ?>
</table>
</div>
<div style="width:510px; float:left">
<table width="510">
  <tr>
    <td class="corfd_ntab" colspan="2" align="center"><strong>FILTRO PLANILHA</strong></td>
  </tr>
  <tr>
    <td width="128" class="corfd_coltexto"><strong>PLANILHA</strong></td>
    <td width="370" class="corfd_colcampos"><select style="width:370px; border: 1px solid #333; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="plan" id="plan">
    <option value="" <?php if(isset($_POST['plan']) OR $varget['plan'] != "") { } else { echo "selected=\"selected\""; }?> disabled="disabled" style="height:15px;padding:5px">Selecione uma planilha</option>
    <?php
        if($_SESSION['user_tabela'] == "user_web") {
          $selplan = "SELECT * FROM userwebfiltro INNER JOIN conf_rel ON conf_rel.idrel_filtros = userwebfiltro.idrel_filtros WHERE iduser_web='$iduser'";
        }
        if($_SESSION['user_tabela'] == "user_adm") {
          $selplan = "SELECT * FROM useradmfiltro INNER JOIN conf_rel ON conf_rel.idrel_filtros = useradmfiltro.idrel_filtros WHERE iduser_adm='$iduser'";
        }
        $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das planilhas");
        $listplan = array();
        while($lselplan = $_SESSION['fetch_array']($eselplan)) {
            $plans = explode(",",$lselplan['idplanilha_web']);
            foreach($plans as $p) {
                $listplan[] = $p;
            }
            foreach($plans as $plan) {
                $vincplan = "SELECT planvinc FROM vinc_plan WHERE idplanilha='$plan'";
                $evincplan = $_SESSION['query']($vincplan) or die ("erro na query de consutla das planilha vinculadas");
                while($lvincplan = $_SESSION['fetch_array']($evincplan)) {
                    $planvinc[] = $evincplan['planvinc'];
                }
                if($planvinc != "") {
                    $listplan = array_merge($listplan, $planvinc);
                }
                else {
                }
                $listplan = array_unique($listplan);
                if(in_array($plan,$listplan)) {
                }
                else {
                    $listplan[] = $plan;
                }
            }
        }
        
        foreach($listplan as $p) {
                $nplan = "SELECT DISTINCT(idplanilha), COUNT(idplanilha) as result, descriplanilha FROM planilha WHERE idplanilha='$p' AND ativoweb='S'";
                $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die ("erro na query de consulta do nome da planilha");
            if($enplan['result'] >= 1) {
                $plannomes[$p] = $enplan['descriplanilha'];
            }
        }
        asort($plannomes);

        foreach($plannomes as $kp => $descri) {
            if($kp == $_POST['plan'] OR $kp == $varget['plan'] OR $kp == $_SESSION['varsconsult']['plan']) {
                       $select = "selected=\"selected\"";
                    }
                    else {
                       $select = "";
                    }
            echo "<option value=\"".$kp."\" $select style=\"height:15px;padding:5px\">".$descri."</option>";
                }
        ?>
    </select></td>
  </tr>
  
</table>
</div>

<?php
}
?>
