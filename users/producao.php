<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.relatorios.php');

$per = $_POST['periodo'];
$consolida = $_POST['consolida'];
$filtro = $_POST['filtro'];
$pernow = periodo();

?>

<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
        $(document).ready(function() {
            <?php
            if($consolida == "") {
                echo "$('.trfiltro').hide();\n";
                echo "$('.trmicro').hide();\n";
                echo "$('.trmonitor').hide();\n";
            }
            else {
                if($consolida == "filtro" OR $consolida == "planning" OR $consolida == "planningctt" OR $consolida == "grafico") {
                    echo "$('.trfiltro').show();\n";
                    echo "$('.trmonitor').hide();\n";
                    if($consolida == "planning" OR $consolida == "planningctt" OR $consolida == "grafico") {
                        echo "$('.trmicro').show();\n";
                    }
                    else {
                        echo "$('.trmicro').hide();\n";
                        echo "$('.trmonitor').hide();\n";
                    }
                }
                else {
                    if($consolida == "monitor") {
                        echo "$('.trmonitor').show();\n";
                        echo "$('.trfiltro').hide();\n";
                        echo "$('.trmicro').hide();\n";
                    }
                    else {
                        echo "$('.trmonitor').hide();\n";
                        echo "$('.trfiltro').hide();\n";
                        echo "$('.trmicro').hide();\n";
                    }
                }
            }
            ?>
            $('#producao').submit(function() {
                var con = $('#consolida').val();
                var filtro = $('#filtro').val();
                var micro = $('#micro').val();
                if(con == "") {
                    alert('Favor selecionar o tipo de consolidação dos dados!!!');
                    return false;
                }
                else {
                    if((con == "planningctt" || con == "planning" || con == "filtro") && (filtro == "" || micro == ""))  {
                        alert("Nos Relatórios PLANNING,PLANNINGCTT E FILTRO, é obrigatório o preenchimento do FILTRO e MICRO FILTRO");
                        return false;
                    }
                    else {
                        $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5,
                        color: '#fff'
                        }})
                    }
                }
            });
            
            $('#consolida').live('change',function() {
                var cons = $(this).val();
                if(cons == "filtro" || cons == "planning" || cons == "planningctt" || cons == "grafico") {
                    $('.trfiltro').show();
                    if(cons == "planning" || cons == "planningctt" || cons == "grafico") {
                        $('.trmicro').show();
                        $('.trmonitor').hide();
                    }
                    else {
                        $('.trmicro').hide();
                        $('.trmonitor').hide();
                    }
                }
                else {
                    if(cons == "monitor") {
                        $('.trmonitor').show();
                        $('.trfiltro').hide();
                        $('.trmicro').hide();
                    }
                    else {
                        $('.trfiltro').hide();
                        $('.trmicro').hide();
                        $('.trmonitor').hide();
                    }
                }
            })
            $('#filtro').change(function() {
                var filtro = $(this).val();
                $('#micro').load('/monitoria_supervisao/admin/carrfiltrom.php',{idfiltro:filtro,pag:'prod'});
            })
            
            <?php
            if(isset($_GET['pesq'])) {
                ?>
                $.unblockUI();
                <?php
            }
            else {
            }
            ?>
	});
</script>
<div>
    <form action="" method="post" id="producao">
    <table width="449">
      <tr>
        <td class="corfd_ntab" align="center" colspan="2"><strong>PESQUISAR PRODUÇÃO</strong></td>
      </tr>
        <tr>
            <td class="corfd_coltexto"><strong>PERÍODO</strong></td>
            <td class="corfd_colcampos">
                <select id="periodo" name="periodo" style="width:300px">
                <?php
                $selper = "SELECT idperiodo,nmes,ano,mes FROM periodo ORDER BY ano DESC,mes DESC";
                $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta do periodo");
                while($lselper = $_SESSION['fetch_array']($eselper)) {
                    if($lselper['idperiodo'] == $_POST['periodo']) {
                        echo "<option value=\"".$lselper['idperiodo']."\" selected=\"selected\">".$lselper['nmes']."/".$lselper['ano']."</option>";
                    }
                    else {
                        if($lselper['idperiodo'] == $pernow && !isset($_POST['pesq'])) {
                            echo "<option value=\"".$lselper['idperiodo']."\" selected=\"selected\">".$lselper['nmes']."/".$lselper['ano']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lselper['idperiodo']."\">".$lselper['nmes']."/".$lselper['ano']."</option>";
                        }
                    }
                }
                ?>
                </select>
            </td>
        </tr>
      <tr>
        <td width="155" class="corfd_coltexto"><strong>FREQUENCIA</strong></td>
        <td width="182" class="corfd_colcashowmpos">
            <select name="frequencia" id="frequencia" style="width:300px">
                <?php
                $arrayp = array('dia' => 'DIA','sem' => 'SEMANA','mes' => 'MES');
                foreach($arrayp as $kp => $p) {
                    if($kp == $_POST['frequencia']) {
                        echo "<option value=\"".$kp."\" selected=\"selected\">".$p."</option>";
                    }
                    else {
                        echo "<option value=\"".$kp."\">".$p."</option>";
                    }
                }
                ?>
            </select>
        </td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>CONSOLIDA POR:</strong></td>
        <td class="corfd_colcampos">
            <select name="consolida" id="consolida" style="width:300px">
                <option value="" selected="selected" disabled="disabled">SELECIONE...</option>
                <?php
                $arraym = array('planning' => 'PLANNING','planningctt' => 'PLANNING - CTT', 'monitor' => "MONITOR",'relacional' => "RELACIONAMENTO",'filtro' => "FILTRO",'grafico' => 'GRÁFICO');
                foreach($arraym as $km => $m) {
                    if($km == $_POST['consolida']) {
                        echo "<option value=\"".$km."\" selected=\"selected\">".$m."</option>";
                    }
                    else {
                        echo "<option value=\"".$km."\">".$m."</option>";
                    }
                }
                ?>
            </select>
        </td>
      </tr>
      <tr class="trfiltro">
          <td class="corfd_coltexto"><strong>FILTROS</strong></td>
          <td class="corfd_colcampos">
            <select name="filtro" id="filtro" style="width:300px">
                <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                <?php
                $filtros = filtros();
                foreach($filtros as $kf => $f) {
                    if($f == $_POST['filtro']) {
                        echo "<option value=\"".$f."\" selected=\"selected\">".strtoupper($f)."</option>";
                    }
                    else {
                        echo "<option value=\"".$f."\">".strtoupper($f)."</option>";
                    }
                }
                ?>
            </select>
          </td>
      </tr>
      <tr class="trmicro">
          <td class="corfd_coltexto"><strong>MICRO</strong></td>
          <td class="corfd_colcampos">
              <select name="micro" id="micro" style="width:300px">
                  <?php
                  if(isset($_POST['pesq'])) {
                      $seldados = "SELECT * FROM filtro_dados fd INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fn.nomefiltro_nomes='".$_POST['filtro']."'";
                      $eseldados = $_SESSION['query']($seldados) or die ("erro na query para listagem dos filtros");
                      while($lseldados = $_SESSION['fetch_array']($eseldados)) {
                          if($lseldados['idfiltro_dados'] == $_POST['micro']) {
                              echo "<option value=\"".$lseldados['idfiltro_dados']."\" selected=\"selected\">".$lseldados['nomefiltro_dados']."</option>";
                          }
                          else {
                              echo "<option value=\"".$lseldados['idfiltro_dados']."\">".$lseldados['nomefiltro_dados']."</option>";
                          }
                      }
                  }
                  else {
                  }
                  ?>
                  
              </select>              
          </td>
      </tr>
      <tr class="trmonitor">
          <td class="corfd_coltexto"><strong>MONITOR</strong></td>
          <td>
              <select name="monitor" id="monitor" style="width:300px">
                  <?php
                  if($_POST['supervisor'] == "") {
                      echo "<option value=\"TODOS\" selected=\"selected\">TODOS</option>";
                  }
                  else {
                      echo "<option value=\"TODOS\">TODOS</option>";
                  }
                  $seluser = "SELECT * FROM monitor WHERE ativo='S' ORDER BY nomemonitor";
                  $eseluser = $_SESSION['query']($seluser) or die (mysql_error());
                  $nuser = $_SESSION['num_rows']($eseluser);
                  if($nuser >= 1) {
                      while($lseluser = $_SESSION['fetch_array']($eseluser)) {
                        if($lseluser['idmonitor'] == $_POST['monitor']) {
                            echo "<option value=\"".$lseluser['idmonitor']."\" selected=\"selected\">".$lseluser['nomemonitor']."</option>";
                        }  
                        else {
                            echo "<option value=\"".$lseluser['idmonitor']."\">".$lseluser['nomemonitor']."</option>";
                        }
                      }
                  }
                  else {
                  }
                  ?>
              </select>
              
          </td>
      </tr>
      <tr>
      	<td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="pesq" id="pesq" type="submit" value="Pesquisar" /></td>
      </tr>
    </table>
    </form><br /><hr />
    <?php
    if(isset($_POST['pesq'])) {
        //levantamento semanas
        $seldatas = "SELECT dataini, datafim, dtinictt,dtfimctt FROM periodo WHERE idperiodo='".$_POST['periodo']."'";
        $eseldatas = $_SESSION['fetch_array']($_SESSION['query']($seldatas)) or die ("erro na query de consutla do período");
        if($eseldatas['dtinictt'] == "" OR $eseldatas['dtfimctt'] == "") {
            $diaini = substr($eseldatas['dataini'], 0,4)."-".substr($eseldatas['dataini'], 5,2)."-01";
            $diafim = ultdiames(substr($dtinictt, 8,2).substr($dtinictt, 5,2).substr($dtinictt, 0,4));
        }
        else {
            $diaini = $eseldatas['dtinictt'];
            $diafim = $eseldatas['dtfimctt'];
        }
        $data = $diaini;
        $s = 0;
        $i = 0;
        $f = 1;
        $dtinis = $diaini;
        for(;$i <= $f;) {
            $s++;
            $sem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($dtinis,5,2),substr($dtinis,8,2),substr($dtinis,0,4)));
            $acres = 6 - $sem;
            $compdt = "SELECT DATE_ADD('$dtinis',INTERVAL $acres DAY) as fimsem";
            $ecomp = $_SESSION['fetch_array']($_SESSION['query']($compdt)) or die ("erro na query de consulta das datas");
            $fimmes = "SELECT '".$ecomp['fimsem']."' > '$diafim' as result";
            $efimmes = $_SESSION['fetch_array']($_SESSION['query']($fimmes)) or die ("verifica o fim do mes");
            if($efimmes['result'] >= 1) {
                $messem[$s] = $dtinis.",".$diafim;
            }
            else {
                $messem[$s] = $dtinis.",".$ecomp['fimsem'];
            }
            $acresdt = "SELECT DATE_ADD('".$ecomp['fimsem']."',INTERVAL 1 DAY) as fimsem";
            $eacres = $_SESSION['fetch_array']($_SESSION['query']($acresdt)) or die ("erro na query de consulta das datas");
            $dtinis = $eacres['fimsem'];
            $datas = "SELECT '$dtinis' <= '$diafim' as result";
            $edatas = $_SESSION['fetch_array']($_SESSION['query']($datas)) or die ("erro na query de comparação das datas");
            if($edatas['result'] >= 1) {
                $i = 0;
            }
            else {
                $i = 2;
            }
        }
        $prod = new RelProducao();
        $prod->periodo['idperiodo'] = $_POST['periodo'];
        $prod->tipo = $_POST['consolida'];
        if($prod->tipo == "planningctt") {
            $prod->periodo['dataini'] = $diaini;
            $prod->periodo['datafim'] = $diafim;
        }
        else {
            $prod->periodo['dataini'] = $eseldatas['dataini'];
            $prod->periodo['datafim'] = $eseldatas['datafim'];
        }
        $prod->periodo['datacttini'] = $diaini;
        $prod->periodo['datacttfim'] = $diafim;
        $prod->cdatas = 0;
        $prod->agrupa = $_POST['filtro'];
        $prod->micro = $_POST['micro'];
        $prod->monitor = $_POST['monitor'];
        if($_POST['frequencia'] == "dia") {
            $prod->tipooper = "d";
            $dtini = $prod->periodo['dataini'];
            $diaini = substr($dtini,8,2);
            $mesini = substr($dtini,5,2);
            $anoini = substr($dtini,0,4);
            $dtfim = $prod->periodo['datafim'];
            $dataini = $dtini;
            $diafim = substr($dtfim,8,2);
            $mesfim = substr($dtfim,5,2);
            $anofim= substr($dtfim,0,4);
            $dt = 1;
            for(;$dt == 1;) {
                $comdt = "SELECT '$dataini' <= '$dtfim' as result";
                $ecomdt = $_SESSION['fetch_array']($_SESSION['query']($comdt)) or die ("erro na quer de comparação das datas");
                if($ecomdt['result'] >= 1) {
                    $prod->datas['d'][] = array($dataini,$dataini);
                    $acredt = "SELECT ADDDATE('$dataini', interval 1 day) as result";
                    $eacredt = $_SESSION['fetch_array']($_SESSION['query']($acredt)) or die ("erro na query para crescentar 1 dia");
                    $dataini = $eacredt['result'];
                    $dt = 1;
                }
                else {
                    $dt = 0;
                }
            }
	}
        if($_POST['frequencia'] == "sem") {
            $prod->tipooper = "s";
            $selinter = "SELECT * FROM interperiodo WHERE idperiodo='".$prod->periodo['idperiodo']."'";
            $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta do intervalo do período");
            while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                $prod->datas['s'][] = array($lselinter['dataini'],$lselinter['datafim']);
                $prod->cdatas++;
            }
        }
        if($_POST['frequencia'] == "mes") {
            $prod->tipooper = "m";
            $selim = "SELECT (count(*) - 12) as lim FROM periodo";
            $elim = $_SESSION['fetch_array']($_SESSION['query']($selim));
            if($elim) {
                $selinter = "SELECT * FROM periodo WHERE idperiodo <= '".$prod->periodo['idperiodo']."' ORDER BY dataini LIMIT ".$elim['lim'].",12";
                $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta do intervalo do período");
                while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                    $prod->datas['m'][$lselinter['nmes']] = array($lselinter['dataini'],$lselinter['datafim']);
                }
                $prod->cdatas++;
            }
        }

        $prod->dadosprod($prod->tipo,$prod->cdatas);
        $prod->montadados();
        $_SESSION['dadosexp'] = $prod->vidados;
        if($prod->tipo == "grafico") {
        }
        else {
        ?>
        <table width="1024">
            <tr>
                <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=PRODUCAO <?php echo strtoupper($_POST['consolida']);?>"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">EXCEL</div></a></td>
                <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome=PRODUCAO <?php echo strtoupper($_POST['consolida']);?>"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">WORD</div></a></td>
                <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome=PRODUCAO <?php echo strtoupper($_POST['consolida']);?>"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">PDF</div></a></td>
            </tr>
        </table><br/>
    	<?php
        echo $prod->visudados();
        }
    }
    ?>
</div>
