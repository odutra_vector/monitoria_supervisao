<?php
$meses = array('01' => 'JANEIRO', '02' => 'FEVEREIRO', '03' => 'MARÇO', '04' => 'ABRIL', '05' => 'MAIO', '06' => 'JUNHO', '07' => 'JULHO', '08' => 'AGOSTO', '09' => 'SETEMBRO', '10' => 'OUTUBRO', '11' => 'NOVEMBRO', '12' => 'DEZEMBRO');
include($rais."/monitoria_supervisao/classes/class.calibpesq.php");
$selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
$efiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta dos filtros cadastrados");
while($lefiltros = $_SESSION['fetch_array']($efiltros)) {
    $vars[$lefiltros['idfiltro_nomes']] = strtolower($lefiltros['nomefiltro_nomes']);
}
if(isset($_GET['usuarios']) && $_GET['usuarios'] != "") {
    $usersop = explode(",",$_GET['usuarios']);
    foreach($usersop as $userop) {
        if(eregi('monitor',$userop)) {
            $uopmonitor[] = str_replace("'", "", $userop);
        }
        if(eregi('user_adm',$userop)) {
            $uopadm[] = str_replace("'", "", $userop);
        }
        if(eregi('user_web',$userop)) {
            $uopweb[] = str_replace("'", "", $userop);
        }
    }
}
else {
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="users/combofiltros_<?php echo strtolower($_SESSION['nomecli']);?>.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#calib').tablesorter();

        $('#pesq').click(function() {
            var mes = $('#mes').val();
            var ano = $('#ano').val();
            if(mes != "" && ano == "") {
                alert('Quando for informado o mês é necessário informar o ano');
                return false
            }
            else {

            }
        })
    })
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<div style="float:left; width:1024px">
<table width="1026">
  <tr>
    <td width="1012" colspan="2" align="center" class="corfd_coltexto"><strong>FILTROS PARA CONSULTA DA AGENDA</strong></td>
  </tr>
</table>
</div>
<form action="" method="post">
<div style="float:left; width:1024px;">
<table width="1023">
    <tr>
        <td width="108" class="corfd_coltexto"><strong>ANO</strong></td>
        <td class="corfd_colcampos">
            <select name="ano" id="ano">
            <?php
            $anoatu = date('Y');
            $anohist = $anoatu;
            for($i = 0; $i <= 10; $i++) {
                if(isset($_POST['pesq'])) {
                    $ano = $_POST['ano'];
                    if($anohist == $ano) {
                        echo "<option value=\"".$anohist."\" selected=\"selected\">".$anohist."</option>";
                    }
                    else {
                        if($anohist == $anoatu) {
                            echo "<option value=\"".$anohist."\" selected=\"selected\">".$anohist."</option>";
                        }
                        else {
                            echo "<option value=\"".$anohist."\">".$anohist."</option>";
                        }
                    }
                    $anohist--;
                }
                else {
                    $ano = $_GET['ano'];
                    if($anohist == $ano) {
                        echo "<option value=\"".$anohist."\" selected=\"selected\">".$anohist."</option>";
                    }
                    else {
                        if($anohist == $anoatu) {
                            echo "<option value=\"".$anohist."\" selected=\"selected\">".$anohist."</option>";
                        }
                        else {
                            echo "<option value=\"".$anohist."\">".$anohist."</option>";
                        }
                    }
                    $anohist--;
                }
            }
            ?>
            </select>
        </td>
    </tr>
  <tr>
    <td width="108" class="corfd_coltexto" rowspan="<?php echo (count($vars) + 2);?>"><strong>MÊS</strong></td>
    <td width="347" class="corfd_colcampos" rowspan="<?php echo (count($vars) + 2);?>"><select name="mes[]" id="mes" multiple="multiple" style="width:200px; height:160px; border: 1px solid #333">
    <?php
        //$meses = array('JANEIRO','FEVEREIRO','MARÇO','ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO');
        if(isset($_POST['pesq'])) {
            $arraymes = $_POST['mes'];
                foreach($meses as $nmes => $mes) {
                    if(in_array($nmes,$arraymes)) {
                        echo "<option value=\"".$nmes."\" selected=\"selected\">".$mes."</option>";
                    }
                    else {
                        echo "<option value=\"".$nmes."\">".$mes."</option>";
                    }
                }
        }
        else {
            $arraymes = explode(",",$_GET['meses']);
            foreach($meses as $nmes => $mes) {
                if(in_array($nmes,$arraymes)) {
                    echo "<option value=\"".$nmes."\" selected=\"selected\">".$mes."</option>";
                }
                else {
                    echo "<option value=\"".$nmes."\">".$mes."</option>";
                }
            }
        }
        
	?>
    </select></td>
    <td width="17" rowspan="<?php echo (count($vars) + 2);?>">
  <?php
  foreach($vars as $vid => $v) {
	  echo "<tr>";
	  	echo "<td bgcolor=\"#999999\"><strong>".strtoupper($v)."</strong></td>";
		echo "<td bgcolor=\"#FFFFFF\"><select name=\"filtro_".$v."\" id=\"filtro_".$v."\" style=\"width:300px; border: 1px solid #333\">";
		echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\">Selecione...</option>";
		$seldados = "SELECT idfiltro_dados, nomefiltro_dados FROM filtro_dados WHERE idfiltro_nomes='".$vid."' AND ativo='S'";
		$edados = $_SESSION['query']($seldados) or die ("erro na query de consulta dos filtro_dados cadastrados");
		while($ldados = $_SESSION['fetch_array']($edados)) {
                    if(isset($_POST['pesq'])) {
                        if($ldados['idfiltro_dados'] == $_POST["filtro_".$v]) {
                            $select = "selected=\"selected\"";
                        }
                        else {
                            $select = "";
                        }
                    }
                    else {
                        $filtros = explode(",",$_GET['filtros']);
                        foreach($filtros as $filtro) {
                            $part = explode("=",$filtro);
                            $fget[$part[0]] = str_replace("'","", $part[1]);
                        }
                        if($ldados['idfiltro_dados'] == $fget["id_".$v]) {
                            $select = "selected\"selected\"";
                        }
                        else {
                            $select = "";
                        }
                    }
			echo "<option $select value=\"".$ldados['idfiltro_dados']."\">".$ldados['nomefiltro_dados']."</option>";
		}
		echo "</select></td>";
      echo "</tr>";
  }
  ?>
    </td>
    <td width="140" class="corfd_coltexto"><strong>FINALIZADO</strong></td>
        <td width="387" class="corfd_colcampos">
            <select name="finalizado">
                <?php
                $ops = array('N','S');
                if(isset($_POST['pesq'])) {
                    if($_POST['finalizado'] == "") {
                        echo "<option value=\"\" selected=\"selected\">Selecione...</option>";
                    }
                    else {
                        echo "<option value=\"\">Selecione...</option>";
                    }
                    foreach($ops as $op) {
                        if($op == $_POST['finalizado']) {
                            echo "<option value=\"".$op."\" selected=\"selected\">".$op."</option>";
                        }
                        else {
                            echo "<option value=\"".$op."\">".$op."</option>";
                        }
                    }
                }
                else {
                    if($_GET['final'] == "") {
                        echo "<option value=\"\" selected=\"selected\">Selecione...</option>";
                    }
                    else {
                        echo "<option value=\"\" >Selecione...</option>";
                    }
                    foreach($ops as $op) {
                        if($op == $_GET['final']) {
                            echo "<option value=\"".$op."\" selected=\"selected\">".$op."</option>";
                        }
                        else {
                            echo "<option value=\"".$op."\">".$op."</option>";
                        }
                    }
                }

                ?>
            </select>
        </td>
  </tr>
</table>
<table width="1024">
<tr>
    <td width="107" class="corfd_coltexto"><strong>USUÁRIOS</strong></td>
    <td width="905" class="corfd_colcampos">
          <select name="usersmonitor[]" id="usermonitor" multiple="multiple" style="width:270px; height:150px">
          <optgroup label="MONITOR">
            <?php
                $selmoni = "SELECT idmonitor, nomemonitor FROM monitor WHERE ativo='S' ORDER BY nomemonitor";
                $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
                while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                    $monitor = $lselmoni['idmonitor']."-monitor";
                    if(isset($_POST['pesq'])) {
                        if(in_array($monitor,$_POST['usersmonitor'])) {
                            echo "<option value=\"".$lselmoni['idmonitor']."-monitor\" selected=\"selected\">".$lselmoni['nomemonitor']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lselmoni['idmonitor']."-monitor\">".$lselmoni['nomemonitor']."</option>";
                        }
                    }
                    else {
                        if(in_array($monitor,$uopmonitor)) {
                            echo "<option value=\"".$lselmoni['idmonitor']."-monitor\" selected=\"selected\">".$lselmoni['nomemonitor']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lselmoni['idmonitor']."-monitor\">".$lselmoni['nomemonitor']."</option>";
                        }
                    }
                }
            //}
            ?>
          </optgroup>
          </select>
          <select name="usersadm[]" id="useradm" multiple="multiple" style="width:270px; height:150px">
          <optgroup label="USUARIOS ADM">
            <?php
                    $seladm = "SELECT iduser_adm, nomeuser_adm FROM user_adm WHERE ativo='S' ORDER BY nomeuser_adm";
                    $eseladm = $_SESSION['query']($seladm) or die ("erro na query de consulta dos monitores");
                    while($lseladm = $_SESSION['fetch_array']($eseladm)) {
                        $useradm = $lseladm['iduser_adm']."-user_adm";
                        if(isset($_POST['pesq'])) {
                            if(in_array($useradm, $_POST['usersadm'])) {
                                echo "<option value=\"".$lseladm['iduser_adm']."-user_adm\" selected=\"selected\">".$lseladm['nomeuser_adm']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lseladm['iduser_adm']."-user_adm\">".$lseladm['nomeuser_adm']."</option>";
                            }
                        }
                        else {
                            if(in_array($useradm, $uopadm)) {
                                echo "<option value=\"".$lseladm['iduser_adm']."-user_adm\" selected=\"selected\">".$lseladm['nomeuser_adm']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lseladm['iduser_adm']."-user_adm\">".$lseladm['nomeuser_adm']."</option>";
                            }
                        }
                    }
                    ?>
          </optgroup>
          </select>
          <select name="usersweb[]" id="userweb" multiple="multiple" style="width:270px; height:150px">
          <optgroup label="USUARIOS WEB">
            <?php
                    $selweb = "SELECT iduser_web, nomeuser_web FROM user_web WHERE ativo='S' ORDER BY nomeuser_web";
                    $eselweb = $_SESSION['query']($selweb) or die ("erro na query de consulta dos monitores");
                    while($lselweb = $_SESSION['fetch_array']($eselweb)) {
                        $usersweb = $lselweb['iduser_web']."-user_web";
                        if(isset($_POST['pesq'])) {
                            if(in_array($usersweb, $_POST['usersweb'])) {
                                echo "<option value=\"".$lselweb['iduser_web']."-user_web\" selected=\selected\">".$lselweb['nomeuser_web']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lselweb['iduser_web']."-user_web\">".$lselweb['nomeuser_web']."</option>";
                            }
                        }
                        else {
                            if(in_array($usersweb,$uopweb)) {
                                echo "<option value=\"".$lselweb['iduser_web']."-user_web\" selected=\selected\">".$lselweb['nomeuser_web']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lselweb['iduser_web']."-user_web\">".$lselweb['nomeuser_web']."</option>";
                            }
                        }
                    }
                    ?>
          </optgroup>
          </select>
        </td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="pesq" id="pesq" type="submit" value="PESQUISAR" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="limpa" onclick="window.location='/monitoria_supervisao/inicio.php?menu=calibragem&opmenu=AGENDA&submenu=OK'" type="button" value="LIMPAR" /></td>
  </tr>
  </table>
  </div><br/><br/></form>
  <div style="width:1024px; float:left; height:500px; overflow:auto">
    <br/></br>
    <table width="1005">
      <tr>
        <td width="1011" colspan="7" align="center" class="corfd_ntab"><strong>CALIBRAGENS</strong></td>
      </tr>
    </table>
    <table width="1022" id="calib" class="calib">
        <thead>
              <tr>
                <th width="27" align="center" class="corfd_coltexto"><strong>ID</strong></th>
                <th width="70" align="center" class="corfd_coltexto"><strong>INICIO</strong></th>
                <th width="70" align="center" class="corfd_coltexto"><strong>FIM</strong></th>
                <th width="56" align="center" class="corfd_coltexto"><strong>QTDE. PART.</strong></th>
                <th width="343" align="center" class="corfd_coltexto"><strong>USUARIO CONTROLE</strong></th>
                <th width="300" align="center" class="corfd_coltexto"><strong>AGENDA</strong></th>
                <th width="227" align="center" class="corfd_coltexto"><strong>FILTRO</strong></th>
                <th width="80" class="corfd_coltexto" align="center"><strong>MONITORIA</strong></th>
                <th width="77" align="center" class="corfd_coltexto"><strong>STATUS</strong></th>
                <th></th>
              </tr>
         </thead>
        <tbody>
      <?php
      $pesq = new Var_Calib_Pesq();
      $pagcalib = "agenda";
      $u = array();
      $user = "";
      if(isset($_POST['pesq'])) {
            $pesq->meses = $_POST['mes'];
            $ano = $_POST['ano'];
            $pesq->Valores_Meses($ano);
            $pesq->Valores_Filtros();
            $participa[] = $_POST['usersadm'];
            $participa[] = $_POST['usersweb'];
            $participa[] = $_POST['usersmonitor'];
            foreach($participa as $pa) {
                foreach($pa as $p) {
                    if($p != "") {
                        $users[] = "'".$p."'";
                    }
                    else {
                    }
                }
            }
            $pesq->users = implode(",",$users);
            if($users != "") {
                $sqlfinal[] = "participantes IN (".implode(",",$users).")";
            }
            else {
            }
            if($pesq->filtros != "") {
                $sqlfinal[] = "idrel_filtros IN (".implode(",",$pesq->filtros).")";
            }
            else {
            }
            if($_POST['finalizado'] != "") {
                $sqlfinal[] = "finalizado='".$_POST['finalizado']."'";
            }
            else {
            }

            if($sqlfinal != "") {
                $sqlfinal = "AND ".implode(" AND ", $sqlfinal);
            }
            else {
            }
            $finalpesq = $_POST['finalizado'];
            foreach($pesq->sqldatas as $datas) {
                //foreach($datas as $data) {
                $selagenda = "SELECT * FROM agcalibragem WHERE dataini BETWEEN '".$datas[0]."' AND '".$datas[1]."' AND datafim BETWEEN '".$datas[0]."' AND '".$datas[1]."' $sqlfinal GROUP BY idagcalibragem ORDER BY dataini DESC,idagcalibragem DESC";
                $eagenda = $_SESSION['query']($selagenda) or die ("erro na query de consulta das calibragens agendadas");
                $linhas = $_SESSION['num_rows']($eagenda);
                if($linhas == 0) {
                }
                else {
                    $pesq->Monta_Tab_Pesq($eagenda, $pagcalib, $finalpesq, $ano);
                }
            }
      }
    
      if(!isset($_POST['pesq']) AND ($_GET['meses'] != "" OR $_GET['usuarios'] != "" OR $_GET['filtros'] != "" OR $_GET['final']) != "") {
          $pesq->meses = explode(",",$_GET['meses']);
          $ano = $_GET['ano'];
          $pesq->Valores_Meses($ano);
          $pesq->Valores_Filtros();
          $pesq->users = $_GET['usuarios'];
          if($_GET['usuarios'] != "") {
              $sqlfinal[] = "participantes IN (".$_GET['usuarios'].")";
          }
          else {
          }
          if($pesq->filtros != "") {
              $sqlfinal[] = "idrel_filtros IN ('".implode(",",$pesq->filtros)."')";
          }
          else {
          }
          if($_GET['final'] != "") {
              $sqlfinal[] = "finalizado='".$_GET['final']."'";
          }
          else {
          }

          if($sqlfinal != "") {
              $sqlfinal = "AND ".implode(" AND ", $sqlfinal);
          }
          else {
          }

          $finalpesq = $_GET['final'];
          foreach($pesq->sqldatas as $datas) {
              //foreach($datas as $data) {
             $selagenda = "SELECT * FROM agcalibragem WHERE dataini BETWEEN '".$datas[0]."' AND '".$datas[1]."' AND datafim BETWEEN '".$datas[0]."' AND '".$datas[1]."' $sqlfinal GROUP BY idagcalibragem ORDER BY dataini DESC,idagcalibragem DESC";
             $eagenda = $_SESSION['query']($selagenda) or die ("erro na query de consulta das calibragens agendadas");
             $linhas = $_SESSION['num_rows']($eagenda);
             if($linhas == 0) {
             }
             else {
                 $pesq->Monta_Tab_Pesq($eagenda, $pagcalib, $finalpesq, $ano);
             }
          }
      }
      
      if(!isset($_POST['pesq']) AND $_GET['meses'] == "" AND $_GET['usuarios'] == "" AND $_GET['filtros'] == "" AND $_GET['final'] == "") {
          $finalpesq = $_GET['final'];
          $selagenda = "SELECT * FROM agcalibragem GROUP BY idagcalibragem ORDER BY dataini DESC, idagcalibragem DESC";
          $eagenda = $_SESSION['query']($selagenda) or die ("erro na query de consulta das calibragens agendadas");
          $linhas = $_SESSION['num_rows']($eagenda);
          if($linhas == 0) {
          }
          else {
              $pesq->Monta_Tab_Pesq($eagenda, $pagcalib, $finalpesq,'');
          }
      }
      ?>
        </tbody>
    </table><br />
    </div>
<?php
if(isset($_POST['pesq'])) {

}
else {
    if(isset($_GET['calib'])) {
    $idcalib = $_GET['calib'];
    $seluserc = "SELECT usercont FROM agcalibragem WHERE idagcalibragem='$idcalib'";
    $eseluserc = $_SESSION['fetch_array']($_SESSION['query']($seluserc)) or die ("erro na query de consulta do usuário controle da calibragem");
    $usercont = $eseluserc['usercont'];
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var usercont = '<?php echo $usercont;?>';
            $("input[id*='delparticipante']").click(function() {
                var idcalib = $(this).attr('name');
                var user = idcalib.split(",");
                if(user[1] == usercont) {
                    var apagar = confirm("Voce deseja excluir este usuario da calibragem, pois irá excluir a calibragem agendada pois o usuário é de controle!!!");
                    if (apagar == true) {
                        window.location = '/monitoria_supervisao/users/delcalib.php?idcalib='+idcalib+'&idusercont='+usercont;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    var apagar = confirm("Voce deseja excluir este usuario da calibragem!!!");
                    if (apagar == true) {
                        window.location = '/monitoria_supervisao/users/delcalib.php?idcalib='+idcalib;
                    }
                    else {
                        return false;
                    }
                }
            })
        })
    </script>
    <div id="detcalib" style="width: 1024px; float: left">
    <table width="995">
      <tr>
            <td class="corfd_ntab" align="center" colspan="8"><strong>DETALHAMENTO DA CALIBRAGEM</strong></td>
      </tr>
    </table>
    <table width="1022">
    <thead>
      <tr>
        <th width="37" align="center" class="corfd_coltexto"><strong>ID</strong></th>
        <th width="92" align="center" class="corfd_coltexto"><strong>DATA INICIAL</strong></th>
        <th width="95" align="center" class="corfd_coltexto"><strong>DATA FINAL</strong></th>
        <th width="242" align="center" class="corfd_coltexto"><strong>USUÁRIO</strong></th>
        <th width="240" align="center" class="corfd_coltexto"><strong>FILTRO</strong></th>
        <th width="94" align="center" class="corfd_coltexto"><strong>STATUS</strong></th>
        <th width="178" align="center" class="corfd_coltexto"><strong>ID MONI. CALIBRAGEM</strong></th>
        <th></th>
      </tr>
    </thead>
    <?php
    echo "<tbody>";
    $m = 1;
    $selcalib = "SELECT a.idagcalibragem, a.dataini, a.datafim, a.participantes, a.idmonitoria as monitoria, 
               a.idrel_filtros, a.finalizado, a.idmonicalib as monicalib, m.idfila,a.usercont,a.useragenda
               FROM agcalibragem a 
               LEFT JOIN monitoria m ON m.idmonitoria = a.idmonitoria
               INNER JOIN rel_filtros rf ON rf.idrel_filtros = a.idrel_filtros
               WHERE a.idagcalibragem='$idcalib' GROUP BY participantes";
    $eselcalib = $_SESSION['query']($selcalib) or die ("erro na query de consulta da calibragem cadastrada");
    while($lcalib = $_SESSION['fetch_array']($eselcalib)) {
      $monicalib = array();
      //$monicalib[] = $lcalib['monicalib'];
      echo "<tr>";
      echo "<td bgcolor=\"#FFFFFF\" align=\"center\"><a name=\"anc".$lcalib['idagcalibragem']."\" style=\"text-decoration:none;\">".$lcalib['idagcalibragem']."</a></td>";
      echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".banco2data($lcalib['dataini'])."</td>";
      echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".banco2data($lcalib['datafim'])."</td>";
      $partuser = explode("-", $lcalib['participantes']);
      $iduser = $partuser[0];
      $tabuser = $partuser[1];
      if($tabuser != "monitor") {
          $selfiltro = "SELECT MIN(nivel),nomefiltro_nomes FROM filtro_nomes";
          $eselfiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die (mysql_error());
          $filtroag = "id_".strtolower($eselfiltro['nomefiltro_nomes']);
          $selrel = "SELECT nomefiltro_dados, $filtroag, count(DISTINCT($filtroag)) as result FROM ".str_replace("_", "", $tabuser)."filtro uf
                    INNER JOIN rel_filtros r ON r.idrel_filtros = uf.idrel_filtros
                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = r.$filtroag
                    WHERE id$tabuser='$iduser'";
          $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
          $snome = "SELECT id$tabuser, nome$tabuser, idperfil_".substr($tabuser, 5)." FROM $tabuser WHERE id$tabuser='$iduser'";
          $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
          if($eselrel['result'] == 1) {
               $nome = $esnome['nome'.$tabuser]." - ".$eselrel['nomefiltro_dados'];
          }
          else {
               $perfil = "SELECT nomeperfil_web FROM perfil_web WHERE idperfil_web='".$esnome['idperfil_'.substr($tabuser, 5)]."'";
               $eperfil = $_SESSION['fetch_array']($_SESSION['query']($perfil)) or die (mysql_error());
               if($tabuser == "user_web") {
                    $nome = $esnome['nome'.$tabuser]." - ".$eperfil['nomeperfil_web'];
               }
               else {
                    $nome = $esnome['nome'.$tabuser]." - CLIENTIS";
               }
          }
          echo "<td bgcolor=\"#FFFFFF\" align=\"center\">$nome</td>";
      }
      else {
           if($_SESSION['user_tabela'] != "user_adm") {
                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">MONITOR$m</td>";
                $m++;
           }
           else {
                $snome = "SELECT id$tabuser, nome$tabuser FROM $tabuser WHERE id$tabuser='$iduser'";
                $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$esnome['nome'.$tabuser]."</td>";
           }
      }
      echo "<td bgcolor=\"#FFFFFF\" align=\"center\">". nomevisu($lcalib['idrel_filtros'])."</td>";
      if($lcalib['finalizado'] == "N") {
          $cstatus = "EM ABERTO";
      }
      if($lcalib['finalizado'] == "S") {
          $cstatus = "FINALIZADO";
      }
      echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$cstatus."</td>";
      $smonicalib = "SELECT idmonitoriacalib FROM monitoriacalib m WHERE iduser='".$lcalib['participantes']."' AND idagcalibragem='".$lcalib['idagcalibragem']."'";
      $emonicalib = $_SESSION['query']($smonicalib) or die ("erro na consulta das monitorias realizadas");
      while($lmonicalib = $_SESSION['fetch_array']($emonicalib)) {
          if(in_array($lmonicalib['idmonitoriacalib'], $monicalib)) {
          }
          else {
              $monicalib[] = "<a href=\"/monitoria_supervisao/users/visumonicalib.php?idmonicalib=".$lmonicalib['idmonitoriacalib']."\" target=\"blank\" style=\"color:#000\">".$lmonicalib['idmonitoriacalib']."</a>";
          }
      }
      echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".implode(",",$monicalib)."</td>";
      if($lcalib['useragenda'] == $_SESSION['participante']) {
        echo "<td><input type=\"button\" style=\"border:0px;width:15px;height:15px;background-image:url(/monitoria_supervisao/images/exit.png)\" name=\"".$lcalib['idagcalibragem'].",".$lcalib['participantes']."\" id=\"delparticipante\"></td>";
      }
      else {
      }
      echo "</tr>";
    }
    echo "</tbody>";
    ?>
    </table>
    </div>
    <?php
        }
        else {
        }
    }
    ?>
</div>
</body>
</html>
