<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<?php
scripts_filtros();
?>

<script type="text/javascript">
    $(document).ready(function() {
        <?php
        $sefiltro = "SELECT nomefiltro_nomes, count(*) as r FROM filtro_nomes WHERE ativo='S'";
        $esefiltro = $_SESSION['query']($sefiltro) or die ("erro na query de consulta dos filtros cadastrados");
        $clinhas = $_SESSION['num_rows']($esefiltro) or die ("erro na contagem de linhas afetadas");
        $i = 0;
        while($lsefiltro = $_SESSION['fetch_array']($esefiltro)) {
            echo "var ".strtolower($lsefiltro['nomefiltro_nomes'])." = $('#filtro_".strtolower($lsefiltro['nomefiltro_nomes'])."').val();\n";
            $fpost[] = strtolower($lsefiltro['nomefiltro_nomes']).": ".$lsefiltro['nomefiltro_nomes'];
        }
        ?>
        $('#user').load("/monitoria_supervisao/users/listusers.php", {
        <?php
        foreach($fpost as $post) {
            $i++;
            echo strtolower($post);
            if($i == $clinhas) {
            }
            else {
                echo ", ";
            }
        }
        ?>
        });
        
        //filtro para pesquisa de monitoria de calibragem
        $("select[id*='filtro_']").live('change',function() {
            <?php
            $sfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
            $esfiltros = $_SESSION['query']($sfiltros) or die ("erro na query de consulta dos filtros cadastrados");
            while($lsfiltros = $_SESSION['fetch_array']($esfiltros)) {
                $filtros[] = strtolower($lsfiltros['nomefiltro_nomes']);
                $filtro = strtolower($lsfiltros['nomefiltro_nomes']);
                echo "var ".$filtro." = $('#filtro_".$filtro."').val();\n";
            }
            echo "$('#calibag').load(".'"/monitoria_supervisao/users/listcalib.php",'."{";
            foreach($filtros as $fs) {
                echo "$fs: $fs,";
            }
            echo "});\n";
            echo "$('#users').load(".'"/monitoria_supervisao/users/listusers.php",'."{";
            foreach($filtros as $f) {
                echo "$f: $f,";
            }
            echo "});\n";
            ?>
    });
        
    //oculta agenda
    $('#agenda').hide();
    var agenda = false;
    $('#tabagcalib').live('click',function() {
        if(agenda == false) {
            $('#agenda').show();
            agenda = true;
        }
        else {
            $('#agenda').hide();
            agenda = false;
        }
    });
        
    //atualiza a lista de usuários adm que vão participar da calibragem
    $('#useradm').change(function() {
        var html = "";
        $('#useradm :selected').each(function() {
            var iduser = $(this).val();
            var nome = $(this).text();
            var add = "<option value="+iduser+">"+nome+" - "+iduser+"</option>";
            html += add;
        })
        $('#userweb :selected').each(function() {
            var iduserweb = $(this).val();
            var nomeweb = $(this).text();
            var add = "<option value="+iduserweb+">"+nomeweb+" - "+iduserweb+"</option>";
            html += add;
        })
        $('#usermonitor :selected').each(function() {
            var idmonitor = $(this).val();
            var nomemonitor = $(this).text();
            var add = "<option value="+idmonitor+">"+nomemonitor+" - "+idmonitor+"</option>";
            html += add;
        })
        $('#usercont').html(html);
    });
        
    //atualiza a lista de usuários web que vão participar da calibragem
    $('#userweb').change(function() {
        var html = "";
        $('#useradm :selected').each(function() {
            var iduser = $(this).val();
            var nome = $(this).text();
            var add = "<option value="+iduser+">"+nome+" - "+iduser+"</option>";
            html += add;
        })
        $('#userweb :selected').each(function() {
            var iduserweb = $(this).val();
            var nomeweb = $(this).text();
            var add = "<option value="+iduserweb+">"+nomeweb+" - "+iduserweb+"</option>";
            html += add;
        })
        $('#usermonitor :selected').each(function() {
            var idmonitor = $(this).val();
            var nomemonitor = $(this).text();
            var add = "<option value="+idmonitor+">"+nomemonitor+" - "+idmonitor+"</option>";
            html += add;
        })
        $('#usercont').html(html);
    });
        
    //atualiza a lista de usuários monitor que vão participar da calibragem
    $('#usermonitor').change(function() {
        var html = "";
        $('#useradm :selected').each(function() {
            var iduser = $(this).val();
            var nome = $(this).text();
            var add = "<option value="+iduser+">"+nome+" - "+iduser+"</option>";
            html += add;
        })
        $('#userweb :selected').each(function() {
            var iduserweb = $(this).val();
            var nomeweb = $(this).text();
            var add = "<option value="+iduserweb+">"+nomeweb+" - "+iduserweb+"</option>";
            html += add;
        })
        $('#usermonitor :selected').each(function() {
            var idmonitor = $(this).val();
            var nomemonitor = $(this).text();
            var add = "<option value="+idmonitor+">"+nomemonitor+" - "+idmonitor+"</option>";
            html += add;
        })
        $('#usercont').html(html);
        });
        
        //vare os id's de monitoria clicados para agendar calibragem
        $("input[id*='idmoni']").change(function() {
            var idmoni = $(this).val();
            if($(this).attr('checked')) {
                var select = $(this).attr('checked');
            }
            else {
                var select = "";
            }
            if(select == "") {
                $('#idsmoni').attr('value', '');
                $("input[id*='idmoni']").each(function() {
                    var idmoni = $(this).val();
                    if($(this).attr('checked')) {
                        var selected = $(this).attr('checked');
                    }
                    else {
                        var selected = "";
                    }
                    if(selected == "") {
                    }
                    else {
                        if($('#idsmoni').val() == "") {
                            var idsmoni = idmoni;
                        }
                        else {
                            var idsmoni = $('#idsmoni').val()+ "," + idmoni;
                        }
                    }
                    $('#idsmoni').attr('value',idsmoni);
                })
            }
            if(select != "") {
                if($('#idsmoni').val() == "") {
                    var idsmoni = idmoni;
                }
                else {
                    var idsmoni = $('#idsmoni').val()+ "," + idmoni;
                }
                $('#idsmoni').attr('value',idsmoni);
            }
        });

        //ação que executa o cadastro a calibragem
        $('#cadcalib').submit(function() {
            var dataini = $('#dtini').val();
            var datafim = $('#dtfim').val();
            var useradm = $('#useradm').val();
            var userweb = $('#userweb').val();
            var usermonitor = $('#usermonitor').val();
            var usercont = $('#usercont').val();
            var idsmoni = $('#idsmoni').val();
            if(dataini == "" || datafim == "" || usercont == null || idsmoni == "") {
                if(useradm == null || userweb == null || usermonitor == null) {
                    alert('Os campos Data, Participantes, Usuário Controle e Monitorias precisam estar preenchidos para cadastro de Calibragem');
                    return false;
                }
                else {
                }
            }
            else {
            }
        });

        $('#monitorias').live(tablesorter());
        $('#agendacalib').live(tablesorter());
    })
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<div id="dadoseagenda" style="width:1024px;">
    <div id="dadoscalib" style="width:1024px; float:left;">
    <form action="/monitoria_supervisao/users/cadcalib.php" method="post" id="cadcalib">
    <table width="946">
      <tr>
          <td class="corfd_coltexto" colspan="2" align="center"><strong>DADOS CALIBRAGEM</strong></td>
      </tr>
      <tr>
        <td width="117" class="corfd_coltexto"><strong>Período Avaliação</strong></td>
        <td class="corfd_colcampos"><input class="data" id="dtini" name="dtini" type="text" style="width:100px; border: 1px solid #333; text-align: center" /> <strong>ATÉ</strong> <input type="text" class="data" id="dtfim" name="dtfim" style="width:100px; border: 1px solid #333; text-align: center" /></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>Participantes</strong></td>
        <td width="817" class="corfd_colcampos" id="users">
          <select name="usersmonitor[]" id="usermonitor" multiple="multiple" style="width:270px; height:150px">
          <optgroup label="MONITOR">
            <?php
                $selmoni = "SELECT idmonitor, nomemonitor FROM monitor WHERE ativo='S' ORDER BY nomemonitor";
                $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
                while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                        echo "<option value=\"".$lselmoni['idmonitor']."-monitor\">".$lselmoni['nomemonitor']."</option>";
                }
            //}
            ?>
          </optgroup>
          </select>
          <select name="usersadm[]" id="useradm" multiple="multiple" style="width:270px; height:150px">
          <optgroup label="USUARIOS ADM">
            <?php
                    $selmoni = "SELECT iduser_adm, nomeuser_adm FROM user_adm WHERE ativo='S' ORDER BY nomeuser_adm";
                    $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
                    while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                            echo "<option value=\"".$lselmoni['iduser_adm']."-user_adm\">".$lselmoni['nomeuser_adm']."</option>";
                    }
                    ?>
          </optgroup>
          </select>
          <select name="usersweb[]" id="userweb" multiple="multiple" style="width:270px; height:150px">
          <optgroup label="USUARIOS WEB">
            <?php
                    $selmoni = "SELECT iduser_web, nomeuser_web FROM user_web WHERE ativo='S' ORDER BY nomeuser_web";
                    $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
                    while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                            echo "<option value=\"".$lselmoni['iduser_web']."-user_web\">".$lselmoni['nomeuser_web']."</option>";
                    }
                    ?>
          </optgroup>
          </select>
        </td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>Usuário Controle</strong></td>
        <td class="corfd_colcampos"><select name="usercont" id="usercont">
        </select>
        </td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>ID´S MONITORIA</strong></td>
        <td class="corfd_colcampos"><span title="Informar o ID's das monitorias separando por virgula"><input style="width:270px; border: 1px solid #333" type="text" readonly="readonly" name="idsmoni" id="idsmoni" /></span></td>
      </tr>
      <tr>
        <td height="22" colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="agendar" type="submit" value="AGENDAR" /></td>
      </tr>
    </table>
    <font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font>
    </form><br /><br />
    </div>
        <div id="tabagcalib" style="width:1024px; float:left">
          <table width="1005">
          <tr>
            <td width="991" colspan="6" align="center" class="corfd_coltexto"><strong>CALIBRAGENS AGENDADAS</strong></td>
          </tr>
          </table>
        </div>
        <div id="agenda" style="width:1024px; height:295px; float:left; overflow: auto">
          <table width="1004" id="agendacalib">
              <thead>
                  <tr>
                    <th width="64" align="center" class="corfd_coltexto"><strong>ID CALIB.</strong></th>
                    <th width="72" align="center" class="corfd_coltexto"><strong>INICIO</strong></th>
                    <th width="74" align="center" class="corfd_coltexto"><strong>FIM</strong></th>
                    <th width="252" align="center" class="corfd_coltexto"><strong>PARTICIPANTES</strong></th>
                    <th width="90" align="center" class="corfd_coltexto"><strong>ID MONITORIA</strong></th>
                    <?php
                    $selminfiltro = "SELECT MIN(nivel), idfiltro_nomes, nomefiltro_nomes FROM filtro_nomes";
                    $emin = $_SESSION['fetch_array']($_SESSION['query']($selminfiltro)) or die ("erro na query de consulta do menor filtro cadastrado");
                    $minfiltro = $emin['nomefiltro_nomes'];
                    ?>
                    <th width="315" align="center" class="corfd_coltexto"><strong><?php echo $minfiltro;?></strong></th>
                    <th width="105" align="center" class="corfd_coltexto"><strong>STATUS</strong></th>
                  </tr>
              </thead>
          <tbody id="calibag">
          <?php
          $periodo = periodo();
          //$datas = "SELECT dataini, datafim FROM periodo WHERE idperiodo='$periodo'";
          //$edatas = $_SESSION['fetch_array']($_SESSION['query']($datas)) or die ("erro na query de consuta das datas do período");
          $data = date('Y-m-d');
          $data = mktime(0,0,0,substr($data,5,6),substr($data,8,9),substr($data,0,3));
          $selag = "SELECT * FROM agcalibragem a WHERE finalizado='N' ORDER BY idagcalibragem, dataini, finalizado";
          $eselag = $_SESSION['query']($selag) or die ("erro na query de consulta das calibragens agendadas");
          while($lselag = $_SESSION['fetch_array']($eselag)) {
              $nomedados = "SELECT fd.nomefiltro_dados FROM rel_filtros rf INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower($minfiltro)." WHERE rf.idrel_filtros='".$lselag['idrel_filtros']."'";
              $edados = $_SESSION['fetch_array']($_SESSION['query']($nomedados)) or die ("erro na query para levantar o nome do dado que deve ser apresentado");
              $dataini = $lselag['dataini'];
              $datafim = $lselag['datafim'];
              $dtini = mktime(0,0,0,substr($dataini,5,6),substr($dataini,8,9),substr($dataini,0,3));
              $dtfim = mktime(0,0,0,substr($datafim,5,6),substr($datafim,8,9),substr($datafim,0,3));
              if($lselag['finalizado'] == "N") {
                  if(($data-86400) < $dtfim OR $data == $dtfim) {
                      $bgcolor = "bgcolor=\"#FFFFCC\"";
                  }
                  if($data > $dtfim) {
                      $bgcolor = "bgcolor=\"#EF9294\"";
                  }
                  if($data < ($dtfim-172800)) {
                      $bgcolor = "bgcolor=\"#FFFFFF\"";
                  }
              }
              if($lselag['finalizado'] == "S") {
                  $bgcolor = "bgcolor=\"#A9DFFA\"";
              }
              echo "<tr>";
                        echo "<td align=\"center\" $bgcolor>".$lselag['idagcalibragem']."</td>";
                echo "<td align=\"center\" $bgcolor>".banco2data($lselag['dataini'])."</td>";
                echo "<td align=\"center\" $bgcolor>".banco2data($lselag['datafim'])."</td>";
                $partnome = explode("-",$lselag['participantes']);
                $iduser = $partnome[0];
                $tabuser = $partnome[1];
                $seluser = "SELECT id$tabuser, nome$tabuser FROM $tabuser WHERE id$tabuser='$iduser'";
                $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do nome do usuário relacionado");
                echo "<td align=\"center\" $bgcolor>".$eseluser['nome'.$tabuser]."</td>";
                echo "<td align=\"center\" $bgcolor>".$lselag['idmonitoria']."</td>";
                echo "<td align=\"center\" $bgcolor>".$edados['nomefiltro_dados']."</td>";
                if($lselag['finalizado'] == "N") {
                    $status = "EM ABERTO";
                }
                else {
                    $status = "FINALIZADO";
                }
                echo "<td align=\"center\" $bgcolor>".$status."</td>";
              echo "</tr>";
          }
          ?>
          </tbody>
        </table>
    </div>
<div style="width:1024px; float:left"><br />
<hr />
</div>
</div>
<div style="width:1024px; float:left;" id="pesqid">
<table width="1024">
    <tr>
    	<td bgcolor="#FFCC00" align="center"><strong>LOCALIZAR MONITORIAS</strong></td>
    </tr>
</table>
</div>
<form action="" method="post">
    <div class="pesqid">
        <?php
          scripts_filtros();
          filtros_divs();
        ?>
        <div style="float:left">
        <table width="1024">
                <tr>
                <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="pesquisa" type="submit" value="PESQUISAR MONITORIAS" /></td>
            </tr>
        </table>
        </div>
    </div>
</form><br /><hr />
        <div style="height:250px; float:left; overflow:auto; width:1024px;" class="pesqid">
        <?php
        $alias = new TabelasSql;
        $selconfag = "SELECT *, count(*) as r FROM conf_calib WHERE ativo='S'";
        $eselconfag = $_SESSION['fetch_array']($_SESSION['query']($selconfag)) or die ("erro na query de consulta das configurações da calibragem");

        $filtros = "SELECT nomefiltro_nomes FROM filtro_nomes ORDER BY nivel";
        $efiltros = $_SESSION['query']($filtros) or die ("erro na query de consulta dos filtros cadastrados");
        while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                $post = "filtro_".strtolower($lfiltros['nomefiltro_nomes']);
                $nomes[$lfiltros['nomefiltro_nomes']] = $post;
                //$filtro.$lfiltros['nome'] = $_POST[$nome];
        }

        if(isset($_POST['pesquisa'])) {
                $tabuserft = array("user_adm" => "useradmfiltro", "user_web" => "userwebfiltro");
                $inner = array();
                $where = array();
                $wherefiltro = array();
                $seluser = "SELECT idrel_filtros FROM ".$tabuserft[$_SESSION['user_tabela']]." WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
                $eseluser = $_SESSION['query']($seluser) or die ("erro na query de consulta dos filtros vinculados ao usuário");
                while($lseluser = $_SESSION['fetch_array']($eseluser)) {
                    $iduserft[] = $lseluser['idrel_filtros'];
                }
                
                foreach($nomes as $filtro => $nome) {
                    if($_POST[$nome] != "") {
                        $wherefiltro[] = "id_".strtolower($filtro)."='".$_POST[$nome]."'";
                    }
                }
                $sqlfiltro = implode(" AND ", $wherefiltro);
                if($sqlfiltro != "") {
                    $selidrel = "SELECT idrel_filtros FROM rel_filtros WHERE $sqlfiltro";
                    $eselidrel = $_SESSION['query']($selidrel) or die ("erro na query de consulta dos relacionamentos");
                    while($lselidrel = $_SESSION['fetch_array']($eselidrel)) {
                        if(in_array($lselidrel['idrel_filtros'], $iduserft)) {
                            $idrel[] = $lselidrel['idrel_filtros'];
                        }
                        else {
                        }
                    }
                    $idrel = implode(",",$idrel);
                    $where['idrel_filtros'] = $alias->AliasTab(monitoria).".idrel_filtros IN (".$idrel.")";
                }
                else {
                    $where['idrel_filtros'] = $alias->AliasTab(monitoria).".idrel_filtros IN (".implode(",",$iduserft).")";
                }

                if($_POST['ID'] != "") {
                    $where['ID'] = $alias->AliasTab(monitoria).".idmonitoria='".$_POST['ID']."'";
                }
                if($_POST['dtinimoni'] != "" AND $_POST['dtfimmoni'] != "") {
                    $where['dtinimoni'] = $alias->AliasTab(monitoria).".data BETWEEN '".data2banco($_POST['dtinimoni'])."' AND '".data2banco($_POST['dtfimmoni'])."'";
                }
                if($_POST['dtinictt'] != "" AND $_POST['dtfimctt'] != "") {
                    $where['dtinimoni'] = $alias->AliasTab(monitoria).".datactt BETWEEN '".data2banco($_POST['dtinictt'])."' AND '".data2banco($_POST['dtfimctt'])."'";
                }
                if(isset($_POST['fg']) OR isset($_POST['fgm'])) {
                    $where['fg'] = $alias->AliasTab(monitoria).".qtdefg >= 1";
                } 
                if($_POST['super'] != "") {
                    $where['super'] = $alias->AliasTab(super_oper).".super_oper LIKE '%".$_POST['super']."%'";
                }
                if($_POST['oper'] != "") {
                    $where['oper'] = $alias->AliasTab(operador).".operador LIKE '%".$_POST['oper']."%'";
                }
                if($_POST['nomemonitor'] != "") {
                    $where['nomemonitor'] = $alias->AliasTab(monitor).".nomemonitor LIKE '%".$_POST['nomemonitor']."%'";
                }
                if($_POST['plan'] != "") {
                    $where['plan'] = $alias->AliasTab(moniavalia).".idplanilha='".$_POST['plan']."'";
                        if(array_key_exists("INNER JOIN moniavalia", $inner)) {
                        }
                        else {
                            $inner['INNER JOIN moniavalia'] = "INNER JOIN moniavalia ".$alias->AliasTab(moniavalia)." ON ".$alias->AliasTab(moniavalia).".idmonitoria = ".$alias->AliasTab(monitoria).".idmonitoria";
                        }
                }
                if($_POST['aval'] != "") {
                    $where['aval'] = $alias->AliasTab(moniavalia).".idaval_plan='".$_POST['aval']."'";
                }
                if($_POST['grupo'] != "") {
                    $where['grupo'] = $alias->AliasTab(moniavalia).".idgrupo='".$_POST['grupo']."'";
                 }
                 if($_POST['sub'] != "") {
                     $where['sub'] = $alias->AliasTab(moniavalia).".idsubgrupo='".$_POST['sub']."'";
                 }
                 if($_POST['perg'] != "") {
                     $where['perg'] = $alias->AliasTab(moniavalia).".idpergunta='".$_POST['perg']."'";
                 }
                 if($_POST['resp'] != "") {
                     $where['resp'] = $alias->AliasTab(moniavalia).".idresposta='".$_POST['resp']."'";
                 }
                 if($_POST['fluxo'] != "") {
                     $where['fluxo'] = $alias->AliasTab(fluxo).".idfluxo='".$_POST['fluxo']."'";
                 }
                 if($_POST['atustatus'] != "") {
                     $where['atustatus'] = $alias->AliasTab(rel_fluxo).".idatustatus='".$_POST['atustatus']."'";
                 }
                 if(count($_POST['tmonitoria']) >= 1) {
                     $tmoni = $_POST['tmonitoria'];
                     $where['tmonitoria'] = $alias->AliasTab(monitoria).".tmonitoria IN ('".implode("','",$tmoni)."')";
                 }
                 if(array_key_exists("ID", $where)) {
                     $wheresql = $alias->AliasTab(monitoria).".idmonitoria='".$_POST['ID']."'";
                 }
                 else {
                    $wheresql = implode(" AND ", $where);
                 }
                 $innersql = implode(" ", $inner);
                 
                 if($idrel == "" && count($wherefiltro) > 0) {
                     echo "<table width=\"100%\" name=\"monitorias\" id=\"monitorias\">";
                     echo "<tr>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\"><font color=\"#FF0000\"><strong>O FILTRO INFORMADO PODE NÃO ESTÁ CADASTRADO OU O USUÁRIO NÃO POSSUI ACESSO!!!</strong></font></td>";
                     echo "</tr>";
                 }
                 else {
                     if($eselconfag['camposag'] == "") {
                         echo "<table width=\"100%\" name=\"monitorias\" id=\"monitorias\">";
                         echo "<thead>";
                            echo "<tr>";
                            echo "<th style=\"color:red;text-align:center;background-color:#FFFFFF\">NÃO HÁ COLUNAS VINCULADAS A CONFIGURAÇÃO DE CALULIBRAGEM PARA MOSTRAR DADOS DA MONITORIA</th>";
                            echo "</tr>";
                         echo "</thead>";
                     }
                     else {
                        echo "<table width=\"150%\" name=\"monitorias\" id=\"monitorias\">";
                        echo "<thead>";
                        echo "<tr>";
                        echo "<th></th>";
                        foreach(explode(",",$eselconfag['camposag']) as $col) {
                            $coluna = explode(".",$col);
                            if($coluna[1] == "idrel_filtros") {
                                echo "<th bgcolor=\"#6699CC\" align=\"center\"><strong>RELACIONAMENTO</strong></th>";
                            }
                            else {
                                echo "<th bgcolor=\"#6699CC\" align=\"center\"><strong>".strtoupper($coluna[1])."</strong></th>";
                            }
                        }
                        echo "</tr>";
                        echo "</thead>";
                        echo "<tbody>";
                        if($sqlfiltro == "" AND $wheresql == "") {
                            $smonitoria = "SELECT ".$eselconfag['camposag']."
                                            FROM monitoria ".$alias->AliasTab(monitoria)."
                                            INNER JOIN operador ".$alias->AliasTab(operador)." ON ".$alias->AliasTab(operador).".idoperador = ".$alias->AliasTab(monitoria).".idoperador
                                            INNER JOIN super_oper ".$alias->AliasTab(super_oper)." ON ".$alias->AliasTab(super_oper).".idsuper_oper = ".$alias->AliasTab(monitoria).".idsuper_oper
                                            INNER JOIN monitor ".$alias->AliasTab(monitor)." ON ".$alias->AliasTab(monitor).".idmonitor = ".$alias->AliasTab(monitoria).".idmonitor
                                            INNER JOIN fila_grava ".$alias->AliasTab(fila_grava)." ON ".$alias->AliasTab(fila_grava).".idfila_grava = ".$alias->AliasTab(monitoria).".idfila
                                            INNER JOIN monitoria_fluxo ".$alias->AliasTab(monitoria_fluxo)." ON ".$alias->AliasTab(monitoria_fluxo).".idmonitoria_fluxo = ".$alias->AliasTab(monitoria).".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$alias->AliasTab(rel_fluxo)." ON ".$alias->AliasTab(rel_fluxo).".idrel_fluxo = ".$alias->AliasTab(monitoria_fluxo).".idrel_fluxo
                                            INNER JOIN fluxo ".$alias->AliasTab(fluxo)." ON ".$alias->AliasTab(fluxo).".idfluxo = ".$alias->AliasTab(rel_fluxo).".idfluxo
                                            GROUP BY ".$alias->AliasTab(monitoria).".idmonitoria ORDER BY ".$alias->AliasTab(monitoria).".idmonitoria DESC LIMIT 1000";
                        }
                        else {
                            $smonitoria = "SELECT ".$eselconfag['camposag']."
                                            FROM monitoria ".$alias->AliasTab(monitoria)."
                                            INNER JOIN operador ".$alias->AliasTab(operador)." ON ".$alias->AliasTab(operador).".idoperador = ".$alias->AliasTab(monitoria).".idoperador
                                            INNER JOIN super_oper ".$alias->AliasTab(super_oper)." ON ".$alias->AliasTab(super_oper).".idsuper_oper = ".$alias->AliasTab(monitoria).".idsuper_oper
                                            INNER JOIN monitor ".$alias->AliasTab(monitor)." ON ".$alias->AliasTab(monitor).".idmonitor = ".$alias->AliasTab(monitoria).".idmonitor
                                            INNER JOIN fila_grava ".$alias->AliasTab(fila_grava)." ON ".$alias->AliasTab(fila_grava).".idfila_grava = ".$alias->AliasTab(monitoria).".idfila
                                            INNER JOIN monitoria_fluxo ".$alias->AliasTab(monitoria_fluxo)." ON ".$alias->AliasTab(monitoria_fluxo).".idmonitoria_fluxo = ".$alias->AliasTab(monitoria).".idmonitoria_fluxo
                                            INNER JOIN rel_fluxo ".$alias->AliasTab(rel_fluxo)." ON ".$alias->AliasTab(rel_fluxo).".idrel_fluxo = ".$alias->AliasTab(monitoria_fluxo).".idrel_fluxo
                                            INNER JOIN fluxo ".$alias->AliasTab(fluxo)." ON ".$alias->AliasTab(fluxo).".idfluxo = ".$alias->AliasTab(rel_fluxo).".idfluxo
                                            $innersql
                                            WHERE $wheresql GROUP BY ".$alias->AliasTab(monitoria).".idmonitoria ORDER BY ".$alias->AliasTab(monitoria).".idmonitoria DESC LIMIT 1000";
                        }
                            $emonitoria = $_SESSION['query']($smonitoria) or die ("erro na query de consulta das monitorias relacionadas");
                            while($lmonitoria = $_SESSION['fetch_array']($emonitoria)) {
                                echo "<tr>";
                                foreach(explode(",",$eselconfag['camposag']) as $aliascol) {
                                    $col = explode(".",$aliascol);
                                    if(eregi("data",$col[1])) {
                                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".banco2data($lmonitoria[$col[1]])."</td>";
                                    }
                                    else {
                                        if($col[1] == "idmonitoria") {
                                            echo "<td><input type=\"checkbox\" name=\"idmoni[]\" id=\"idmoni".$lmonitoria[$col[1]]."\" value=\"".$lmonitoria[$col[1]]."\"></td>";
                                            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lmonitoria[$col[1]]."</td>";                                         
                                        }
                                        else {
                                            if($col[1] == "idrel_filtros") {
                                                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".nomevisu($lmonitoria[$col[1]])."</td>";
                                            }
                                            else {
                                                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lmonitoria[$col[1]]."</td>";
                                            }                                         
                                        }
                                    }
                                }
                                echo "</tr>";
                            }
                        echo "</tbody>";
                     }
                 }
                 echo "</table>";
        }
        else {
        }
        ?>
        </div>
</div>
</body>
</html>
