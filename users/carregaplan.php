<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include($rais.'/monitoria_supervisao/seguranca.php');
include($rais.'/monitoria_supervisao/config/conexao.php');
include($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['plan'])) {
  $idplan = $_POST['plan'];

  $selplan = "SELECT DISTINCT(planilha.idaval_plan), planilha.idplanilha, aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='$idplan'";
  $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das avaliaÃ§Ãµes");
  echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\" style=\"height:15px;padding:5px;width:450px\">Selecione uma Avalialção</option>";
  while($lselplan = $_SESSION['fetch_array']($eselplan)) {
    echo "<option value=\"".$lselplan['idaval_plan']."\" style=\"height:15px;padding:5px;width:450px\" title=\"".$lselplan['nomeaval_plan']."\">".$lselplan['nomeaval_plan']."</option>";
  }
}

if(isset($_POST['aval'])) {
  $idaval = $_POST['aval'];
  $idplan = $_POST['idplan'];

  $selgrupo = "SELECT DISTINCT(planilha.idgrupo), planilha.idplanilha, grupo.descrigrupo,grupo.ativo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	       WHERE planilha.idplanilha='$idplan' AND planilha.idaval_plan='$idaval'";
  $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
  echo "<option value=\"\" style=\"height:15px;padding:5px\">Selecione um Grupo</option>";
  while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
      if($lselgrupo['ativo'] == "N") {
          $aval = "<option value=\"".$lselgrupo['idgrupo']."\" style=\"height:15px;padding:5px; color:red;width:450px\" title=\"".$lselgrupo['descrigrupo']."\">".$lselgrupo['descrigrupo']."</option>";
      }
      else {
        $aval = "<option value=\"".$lselgrupo['idgrupo']."\" style=\"height:15px;padding:5px;width:450px\" title=\"".$lselgrupo['descrigrupo']."\">".$lselgrupo['descrigrupo']."</option>";
      }
    echo $aval;
  }
}

if(isset($_POST['grupo'])) {
  $idplan = $_POST['idplan'];
  $idaval = $_POST['idaval'];
  $idgrupo = $_POST['grupo'];
  $idsub = $_POST['idsub'];

  $selrel = "SELECT filtro_vinc FROM grupo WHERE idgrupo='$idgrupo'";
  $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta da grupo");
  $tiporel = $eselrel['filtro_vinc'];

  if($tiporel == "P") {
    echo "<option value=\"\" style=\"height:15px;padding:5px\"></option>";
  }
  if($tiporel == "S") {
    $selsub = "SELECT DISTINCT(idrel), subgrupo.descrisubgrupo, planilha.idgrupo, grupo.filtro_vinc,subgrupo.ativo FROM planilha
	      INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	      INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
	      WHERE planilha.idplanilha='$idplan' AND planilha.idaval_plan='$idaval' AND planilha.idgrupo='$idgrupo' AND filtro_vinc='S'";
    $esub = $_SESSION['query']($selsub) or die ("erro na query de consutla dos sub-grupos");
    echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\" style=\"height:15px;padding:5px\">Selecione um Sub-Grupo</option>";
    while($lsub = $_SESSION['fetch_array']($esub)) {
        if($lsub['ativo'] == "N") {
            echo "<option value=\"".$lsub['idrel']."\" style=\"height:15px;padding:5px;color:red; width:450px\" title=\"".$lsub['descrisubgrupo']."\">".$lsub['descrisubgrupo']."</option>";
        }
        else {
            echo "<option value=\"".$lsub['idrel']."\" style=\"height:15px;padding:5px;width:450px\" title=\"".$lsub['descrisubgrupo']."\">".$lsub['descrisubgrupo']."</option>";
        }
    }
  }
}


if(isset($_POST['idsub'])) {
  $idplan = $_POST['idplan'];
  $idaval = $_POST['idaval'];
  $idgrupo = $_POST['idgrupo'];
  $idsub = $_POST['idsub'];

  $selperg = "SELECT DISTINCT(subgrupo.idpergunta), pergunta.descripergunta, subgrupo.idsubgrupo as idsub, grupo.idgrupo as idgrupo, planilha.idplanilha as idplan,pergunta.ativo FROM planilha
	      INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	      INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
	      INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
	      WHERE planilha.idplanilha='$idplan' AND planilha.idaval_plan='$idaval' AND planilha.idgrupo='$idgrupo' AND grupo.idrel='$idsub'";
  $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas relacionadas");
  echo "<option value=\"\" style=\"height:15px;padding:5px\">Selecione uma pergunta</option>";
  while($lselperg = $_SESSION['fetch_array']($eselperg)) {
      if($lselperg['ativo'] == "N") {
          echo "<option value=\"".$lselperg['idpergunta']."\" style=\"height:15px;padding:5px;color:red;width:450px\" title=\"".$lselperg['descripergunta']."\">".$lselperg['descripergunta']."</option>";
      }
      else {
        echo "<option value=\"".$lselperg['idpergunta']."\" style=\"height:15px;padding:5px;width:450px\" title=\"".$lselperg['descripergunta']."\">".$lselperg['descripergunta']."</option>";
      }
  }
}

if(isset($_POST['idperg'])) {
  $idplan = $_POST['idplan'];
  $idaval = $_POST['idaval'];
  $idgrupo = $_POST['idgrupo'];
  $idperg = $_POST['idperg'];

  $selperg = "SELECT * FROM pergunta WHERE idpergunta='$idperg'";
  $selperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das respostas");
  echo "<option value=\"\" style=\"height:15px;padding:5px\">Selecione uma resposta</option>";
  while($lselperg = $_SESSION['fetch_array']($selperg)) {
      if($lselperg['ativo_resp'] == "N") {
          echo "<option value=\"".$lselperg['idresposta']."\" style=\"height:15px;padding:5px;color:red;width:450px\" title=\"".$lselperg['descriresposta']."\">".$lselperg['descriresposta']."</option>";
      }
      else {
          echo "<option value=\"".$lselperg['idresposta']."\" style=\"height:15px;padding:5px;width:450px\" title=\"".$lselperg['descriresposta']."\">".$lselperg['descriresposta']."</option>";
      }
  }
}

?>