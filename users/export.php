<?php

session_start();
ob_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/MPDF53/mpdf.php');
include_once($rais.'/monitoria_supervisao/dompdf/dompdf_config.inc.php');

$tipo = $_GET['tipo'];

if($tipo == "PDF") {
    $exp = "pdf";
}
if($tipo == "EXCEL") {
    $exp = "x-msexcel";
    $ext = ".xls";
}
if($tipo == "WORD") {
    $exp = "x-msword";
    $ext = ".doc";
}
if($tipo == "CSV") {
    $exp = "x-msword";
    $ext = ".csv";
}

if($tipo == "PDF") {
    $dados = ob_get_clean();
    $pdf = new mPDF();

    $dados = str_replace("'", "\'", $_SESSION['dadosexp']);
    $mpdf->allow_charset_conversion=true;
    $mpdf->charset_in='UTF-8';
    $pdf->WriteHTML($dados);
    $pdf->Output();
    //$pdf->stream($_GET['nome'].".pdf",array("Attachment" => 0));
    
}
else {
    $arquivo = $_GET['nome'].$ext;
    header ("Last-Modified: ".date('d/m/Y H:i:s')."");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Content-type: application/$exp");
    header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
    header ("Pragma: no-cache");
    header ("Expires: 0");
    $html = utf8_decode($_SESSION['dadosexp']);
    echo $html;
}
//header ("Content-Description: PHP Generated Data" );

//envia conteudo do arquivo para abertua
//unset($_SESSION['dadosexp']);
exit;

?>
