<?php

session_start();

if(isset($_GET['varget']) && !isset($_POST['rel'])) {
    $varsget = explode("*",$_GET['varget']);
    foreach($varsget as $valor) {
        $v = explode("=",$valor);
        $varskey[] = $v[0];
        $varsvalor[] = $v[1];
    }
    $varget = array_combine($varskey,$varsvalor);
}
if(isset($_POST['rel'])) {
    $varget = array();
}

$iduser = $_SESSION['usuarioID'];
$tabuser = $_SESSION['user_tabela'];

$sfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' and obrigatorio='S' ORDER BY nivel DESC";
$efiltros = $_SESSION['query']($sfiltros) or die ("erro na consulta dos filtro ativos");
while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
    $nsfiltros[] = strtolower($lfiltros['nomefiltro_nomes']);
}

if($_GET['opcoes'] != "") {
    $opcoes = explode("#",$_GET['opções']);
}

foreach($opcoes as $opf) {
    $n1 = explode(".",$opf);
    $n2 = explode("=",$n1[1]);
    $opwhere[$n2[0]] = $n2[1];
}


scripts_filtros();
?>

<script type="text/javascript">
$(document).ready(function() {
   //$("#dtinimoni").mask("99/99/9999");
   //$("#dtfimmoni").mask("99/99/9999");
   //$("#dtinictt").mask("99/99/9999");
   //$("#dtfimctt").mask("99/99/9999");

    $("a[id*='relhref']").live('click',function() {
        var url = $(this).attr('name');
        var tmonitoria = '';
        <?php
        $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
        $eselfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
        $vars = array('idmonitoria' => 'ID','idplanilha' => 'plan', 'idaval_plan' => 'aval','idgrupo' => 'grupo','idsubgrupo' => 'sub',
        'idpergunta' => 'perg','idresposta' => 'resp','operador' => 'oper','super_oper' => 'super_oper','nomemonitor' => 'nomemonitor','dtinimoni' => 'dtinimoni',
        'dtfimmoni' => 'dtfimmoni','dtinictt' => 'dtinictt','dtfimctt' => 'dtfimctt','qtdefg' => 'fg','qtdefgm' => 'fgm','idindice' => 'indice','idintervalo_notas' => 'intervalo','idfluxo' => 'fluxo',
        'atustatus' => 'atustatus','tmonitoria' => 'tmonitoria','valor_fg' => 'valor_fg','idalerta' => 'idalerta','search' => 'search');
        foreach($vars as $kva => $va) {
            if($va == "fg" OR $va == "fgm") {
                echo "if($('#$va').attr('checked')) {\n";
                    echo "var $va = '$va';\n";
                echo "}\n";
                echo "else {\n";
                    echo "var $va = '';\n";
                echo "}\n";
            }
            if($va == "tmonitoria") {
                echo "$('.t:checked').each(function() {\n";
                echo "if($va == '') {\n";
                echo $va ." = this.value;\n";
                echo "}\n";
                echo "else {\n";
                echo $va ." = $va + ',' + this.value;\n";
                echo "}\n";
                echo "})\n";
            }
            if($va != "fg" && $va != "fgm" && $va != "tmonitoria") {
                echo "var $va = $('#$va').val();\n";
            }
        }
        while($lfiltros = $_SESSION['fetch_array']($eselfiltros)) {
            echo "var ".strtolower($lfiltros['nomefiltro_nomes'])." = $('#filtro_".strtolower($lfiltros['nomefiltro_nomes'])."').val();\n";
        }
        ?>
        $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5,
        color: '#fff'
        }})
        
        $('#result').load('/monitoria_supervisao/users/dadosrelmoni.php',{variaveis:url,<?php $sfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
        $esfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
        while($lselfiltros = $_SESSION['fetch_array']($esfiltros)) {
            echo "filtro_".strtolower($lselfiltros['nomefiltro_nomes']).": ".strtolower($lselfiltros['nomefiltro_nomes']).",";
        } 
        $i = 0; 
        foreach($vars as $v) {
            $i++; 
            echo "$v: $v"; 
            if($i != count($vars)) { 
                echo ",";    
            } 
            else {
            }
        }
        echo "});\n";
        ?>
    })
    
    $("input[id*='rel']").live('click',function() {
        var tmonitoria = '';
        <?php
        $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
        $eselfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
        foreach($vars as $kva => $va) {
            if($va == "fg" OR $va == "fgm") {
                echo "if($('#$va').attr('checked')) {\n";
                    echo "var $va = '$va';\n";
                echo "}\n";
                echo "else {\n";
                    echo "var $va = '';\n";
                echo "}\n";
            }
            if($va == "tmonitoria") {
                echo "$('.t:checked').each(function() {\n";
                echo "if($va == '') {\n";
                echo $va ." = this.value;\n";
                echo "}\n";
                echo "else {\n";
                echo $va ." = $va + ',' + this.value;\n";
                echo "}\n";
                echo "})\n";
            }
            if($va != "fg" && $va != "fgm" && $va != "tmonitoria") {
                echo "var $va = $('#$va').val();\n";
            }
        }
        while($lfiltros = $_SESSION['fetch_array']($eselfiltros)) {
            echo "var ".strtolower($lfiltros['nomefiltro_nomes'])." = $('#filtro_".strtolower($lfiltros['nomefiltro_nomes'])."').val();\n";
        }
        ?>
        var rel = $(this).val();
        var tabsec = $('#tabsec'+rel).val();
        var tabpri = $('#tabpri'+rel).val();
        var tabuser = '<?php echo $tabuser;?>';
        var mes = dtinictt.substr(3,2);
        var ano = dtinictt.substr(6,4);
        var perfil = '<?php echo $_SESSION['usuarioperfil'];?>';
        //alert(valor_fg);
        <?php
        //inicio do javasscript para preenchimento dos filtros obrigatórios
        foreach($nsfiltros as $filtrosob) {
            $objs[] =  "$filtrosob == ''";
            ?>
            var <?php echo $filtrosob;?> = $('#filtro_<?php echo $filtrosob;?>').val();
            <?php
        }
        if(count($nsfiltros) >= 1) {
            ?>
            if(ID == "" && <?php echo implode(" || ",$objs);?>) {
                alert("Os filtros <?php echo strtoupper(implode(",",$nsfiltros));?> são obrigatórios");
                return false;
            }
            <?php
        }
        ?>
        if(valor_fg != "" && isNaN(valor_fg)) {
            alert("O valor colocado no campo NOTA não é um número");
            return false;
        }
        else {
            if((dtinictt == "" || dtfimctt == "") && ID == "") {
                alert('O Campo DATA CONTATO é de preenchimento obrigatório!!!');
                return false;
            }
            else {
                if(tabsec != 'monitoria' && ID != "") {
                    alert('Quando o relatório não está relacionado a monitorias o campo ID MONITORIA deve ficar vazio, favor fazer RESET dos filtros!!!');
                    return false;
                }
                else {
                    if(tabsec == "regmonitoria" && (ID != "" || atustatus != "" || intervalo != "" || plan != "" || indice != "" || fluxo != "" || fg != "" || fgm != "" || valor_fg != "" || idalerta != "")) {
                        alert('Quando o relatório estiver relacionado a REGISTROS os campos ID MONITORIA, F.G, F.GM, PLANILHA, FLUXO, STATUS ATUAL, INDICE NOTAS, INTERVALO NOTA e ALERTA devem ficar vazios');
                        return false;
                    }
                    else {
                        if(tabsec == "regmonitoria" && tabuser == "user_web" && ((mes < 04 && ano < 2012) ||  (mes >= 04 && ano < 2012) || (mes < 04 && ano == 2012))) { // bloqueio para busca de improdutivas antes de 01/04/2012 solicitado em 06/04/2012
                            alert("A consulta de IMPRODUTIVAS só poderá ser feita com data superior ou igual a 01/04/2012");
                            return false;
                        }                        
                        else {
                            $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5,
                            color: '#fff'
                            }})
                            //add: add,tabadd:tabadd,proxrel:proxrel,valadd: valadd, group:group,
                            $('#result').load('/monitoria_supervisao/users/dadosrelmoni.php',{rel:rel,<?php $sfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
                            $esfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
                            while($lselfiltros = $_SESSION['fetch_array']($esfiltros)) {
                                echo "filtro_".strtolower($lselfiltros['nomefiltro_nomes']).": ".strtolower($lselfiltros['nomefiltro_nomes']).",";
                            } 
                            $i = 0; 
                            foreach($vars as $v) {
                                $i++; 
                                echo "$v: $v"; 
                                if($i != count($vars)) { 
                                    echo ",";    
                                }
                            }
                            echo "});\n";
                            ?>
                            }
                        }
                    }
                }
            }
        })
});
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#chart').hide();
        var abre = "S";
        $('#grafico').click(function() {
            if(abre == "S") {
                $('#chart').show();
                abre = "N";
            }
            else {
                $('#chart').hide();
                abre = "S";
            }
                //$('#chart').dialog('open');
                //return false;
        })
    });
</script>

<div>
    <?php
    if(isset($_GET['reset'])) {
        unset($_SESSION['varsconsult']);
    }
    filtros_divs($opwhere,$_GET['menu']);
    unset($_SESSION['varsconsult']);
    ?>

    <div align="center" style="width:1020px; float:left; border: 2px solid #333">
        <?php
        $i = 0;
        $verifcadrel = "SELECT COUNT(*) as result FROM relmoni";
        $ecadrel = $_SESSION['fetch_array']($_SESSION['query']($verifcadrel)) or die ("erro na queyr de consulta dos relatórios cadastrados");
        if($ecadrel['result'] == 0) {
            ?>
            <table width:1024>
                <tr>
                    <td><strong><font color="#FF0000">NÃO EXISTEM RELATÓRIOS CADASTRADOS, FAVOR CONTATAR O ADMINISTRADOR</font></strong></td>
                </tr>
            </table>
        <?php
        }
        else {
            $selrel = "SELECT * FROM relmoni WHERE ativo='S'";
            $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relatórios cadastrados");
            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                $filtros = explode(",",$lselrel['filtros']);
                ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#rel<?php echo $lselrel['idrelmoni'];?>').click(function() {
                            var rel = $(this).attr("id");
                            
                            <?php
                            foreach($filtros as $filt) {
                                if($filt != "") {
                                    if(in_array($filt,$filtros)) {
                                        ?>
                                        var <?php echo $filt;?> = $('#filtro_<?php echo $filt;?>').val();
                                        <?php
                                    }
                                    else {
                                        ?>
                                        var <?php echo $filt;?> = $('#<?php echo $filt;?>').val();
                                        <?php
                                    }
                                    ?>
                                    if(<?php echo $filt;?> == "") {
                                        alert('O filtro <?php echo $filt;?> não pode estar sem preenchimento!!!');
                                        return false;
                                    }
                                    else {
                                    }
                                <?php
                                }
                                else {
                                }
                            }
                            ?>
                        })
                    })
                </script>
        <?php
                $perfis = array();
                $verifrel = "SELECT COUNT(*) as result FROM relmonicol INNER JOIN relmoni ON relmoni.idrelmoni = relmonicol.idrelmoni
                            WHERE relmonicol.idrelmoni='".$lselrel['idrelmoni']."' AND (tabela<>'' OR tabela<>'DADOS')";
                $everifrel = $_SESSION['fetch_array']($_SESSION['query']($verifrel)) or die ("erro na query de consulta das colunas do relatório");
                if($everifrel['result'] >= 1) {
                    if($_SESSION['user_tabela'] == "user_web") {
                        $perfis = explode(",", $lselrel['idsperfilweb']);
                        if(in_array($_SESSION['usuarioperfil'], $perfis)) {
                          $listrel[] = $lselrel['idrelmoni'];
                          $nomerel[] = $lselrel['nomerelmoni'];
                        }
                    }
                    if($_SESSION['user_tabela'] == "user_adm") {
                        $perfis = explode(",", $lselrel['idsperfiladm']);
                        if(in_array($_SESSION['usuarioperfil'], $perfis)) {
                          $listrel[] = $lselrel['idrelmoni'];
                          $nomerel[] = $lselrel['nomerelmoni'];
                        }
                    }
                }
                else {
                }
            }
            $colunas = 1020 / count($listrel);
            $r = 0;
            ?>
            <table width="1020" bgcolor="#FFFFFF">
            <?php
            if(count($listrel) < 4) {
                $l = 1;
            }
            if(count($listrel) >= 4 AND count($listrel) < 7) {
                $l = 2;
            }
            if(count($listrel) >= 7 AND count($listrel) < 13) {
                $l = 3;
            }
            if(count($listrel) >= 13 AND count($listrel) < 19) {
                $l = 4;
            }
            $qtdecol = ceil(count($listrel) / $l);
            $listrel = array_chunk($listrel, $qtdecol);
              for($i = 0; $i < $l; $i++) {
              ?>
              <tr>
              <?php
                foreach($listrel[$i] as $idrel) {
                    $sqlrel = "SELECT * FROM relmoni WHERE idrelmoni='$idrel'";
                    $esqlrel =  $_SESSION['fetch_array']($_SESSION['query']($sqlrel)) or die ("erro na query de consulta do relatório");
              ?>
                    <td align="center" width="<?php echo $colunas;?>"><input type="hidden" name="tabsec<?php echo $esqlrel['nomerelmoni'];?>" id="tabsec<?php echo $esqlrel['nomerelmoni'];?>" value="<?php echo $esqlrel['tabsec'];?>" /><input type="hidden" name="tabpri<?php echo $esqlrel['nomerelmoni'];?>" id="tabpri<?php echo $esqlrel['nomerelmoni'];?>" value="<?php echo $esqlrel['tabpri'];?>" /><input type="button" class="botaorel" name="rel" id="rel<?php echo $esqlrel['idrelmoni'];?>" tittle="<?php echo $esqlrel['nomerelmoni'];?>" value="<?php echo $esqlrel['nomerelmoni'];?>"></td>
              <?php
                }
              ?>
              </tr>
              <?php
              }
              ?>
            </table>
        <?php
        }
    ?>
    </div>
    <br />
    <div style="height:450px; float:left; overflow:auto; width:1020px;" id="result">
    </div>
</div>
