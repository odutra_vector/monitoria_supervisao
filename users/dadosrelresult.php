<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once($rais.'/monitoria_supervisao/classes/class.relatorios.php');

protegepagina();

$vars = array('idmonitoria' => 'ID','idplanilha' => 'plan', 'idaval_plan' => 'aval','idgrupo' => 'grupo','idsubgrupo' => 'idsubgrupo',
    'idpergunta' => 'idpergunta','idresposta' => 'resp','operador' => 'oper','super_oper' => 'super_oper','nomemonitor' => 'nomemonitor','dtinimoni' => 'dtinimoni',
    'dtfimmoni' => 'dtfimmoni','dtinictt' => 'dtinictt','dtfimctt' => 'dtfimctt','qtdefg' => 'fg','qtdefgm' => 'fgm','idindice' => 'indice','idintervalo_notas' => 'intervalo','idfluxo' => 'fluxo','atustatus' => 'atustatus');
$planapres = array('monitoria' => 'idmonitoria','planilha' => 'idplanilha', 'aval_plan' => 'idaval_plan','grupo' => 'idgrupo','subgrupo' => 'idsubgrupo',
    'pergunta' => 'idpergunta','pergunta' => 'idresposta','operador' => 'operador','super_oper' => 'super_oper','monitor' => 'nomemonitor','data1' => 'dtinimoni',
    'data2' => 'dtfimmoni','datactt1' => 'dtinictt','datactt2' => 'dtfimctt','monitoria' => 'fg','monitoria' => 'fgm','fluxo' => 'idfluxo','atustatus' => 'status','indice' => 'idindice','intervalo_notas' => 'idintervalo_notas');
$descrivars = array('ID MONITORIA' => 'idmonitoria','PLANILHA' => 'idplanilha', 'AVALIAÇÃO' => 'idaval_plan','GRUPO' => 'idgrupo','SUB-GRUPO' => 'idsubgrupo',
    'PERGUNTA' => 'idpergunta','RESPOSTA' => 'idresposta','OPERADOR' => 'operador','SUPERVISOR' => 'super_oper','MONITOR' => 'monitor','DATA MONITORIA' => 'dtinimoni',
    'DATA MONITORIA' => 'dtfimmoni','DATA CONTATO' => 'dtinictt','DATA CONTATO' => 'dtfimctt','FALHA GRAVE' => 'fg','FALHA GRAVISSIMA' => 'fgm','INDICE' => 'indice','INTERVALO' => 'intervalo','FLUXO' => 'idfluxo','STATUS ATUAL' => 'atustatus');
$colvars = array('idmonitoria' => 'idmonitoria','descriplanilha' => 'idplanilha', 'nomeaval_plan' => 'idaval_plan','descrigrupo' => 'idgrupo','descrisubgrupo' => 'idsubgrupo',
    'descripergunta' => 'idpergunta','descriresposta' => 'idresposta','operador' => 'operador','super_oper' => 'super_oper','nomemonitor' => 'nomemonitor','data1' => 'dtinimoni',
    'data2' => 'dtfimmoni','datactt1' => 'dtinictt','datactt2' => 'dtfimctt','fg' => 'fg','fgm' => 'fgm','nomeindice' => 'idindice','nomeintervalo_notas' => 'idintervalo_notas','nomefluxo' => 'idfluxo', 'nomestatus' => 'atustatus');
$rel = new RelGerenciais();
$selfilt = "SELECT * FROM filtro_nomes WHERE ativo='S'";
$eselfilt = $_SESSION['query']($selfilt) or die ("erro na query de consulta dos filtros");
while($lselfilt = $_SESSION['fetch_array']($eselfilt)) {
    if($_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] == "") {
    }
    else {
        $nfiltros[strtolower($lselfilt['nomefiltro_nomes'])] = "rf.id_".strtolower($lselfilt['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])]."'";
        $rel->vars['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] = $_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])];
    }
}
if($nfiltros == "") {
    $tabuser = str_replace("_", "", $_SESSION['user_tabela']);
    $selperf = "SELECT idrel_filtros FROM ".$tabuser."filtro WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
    $eselperf = $_SESSION['query']($selperf) or die ("erro na query de consulta dos filtros autorizados para o usuário");
    while($lselperf = $_SESSION['fetch_array']($eselperf)) {
        $idsrel[] = $lselperf['idrel_filtros'];
    }
    $rel->vars['idrel_filtros'] = implode(",",$idsrel);
}
else {
    $filtro = implode(" AND ",$nfiltros);
    $selrel = "SELECT rf.idrel_filtros FROM rel_filtros rf INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros WHERE $filtro AND cr.ativo='S'";
    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relacionamentos");
    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
        $idsrel[] = $lselrel['idrel_filtros'];
    }
    $rel->vars['idrel_filtros'] = implode(",",$idsrel);
}
$nomerel = $_POST['rel'];
foreach($vars as $kv => $v) {
    if($_POST[$v] == "") {
    }
    else {
        if($kv == "qtdefg" OR $kv == "qtdefgm") {
            $rel->vars[$kv] = "1";
        }
        else {
            $rel->vars[$kv] = $_POST[$v];
        }
    }
}
$selcab = "SELECT rc.caminhologo,rc.posilogo,rc.campos,rc.posicampos,rc.width,rc.height,rc.idrelresult,rc.idrelresultcab, rr.nomerelresult FROM relresultcab rc INNER JOIN relresult rr ON rr.idrelresult=rc.idrelresult WHERE nomerelresult='$nomerel'";
$eselcab = $_SESSION['fetch_array']($_SESSION['query']($selcab)) or die ("erro query para consulta do cabeçalho do relatório");
$camlg = explode(",",$eselcab['caminhologo']);
$posilg = explode(",",$eselcab['posilogo']);
$logos = array_combine($posilg, $camlg);
$filtros[] = explode(",",$eselcab['campos']);
$posic = $eselcab['posicampos'];
$clogo = count($logos);
$selcampos = "SELECT rc.idrelresult,rc.idrelresultcampos,rc.campo,rc.posicao,rc.valor,rc.descricampo FROM relresultcampos rc INNER JOIN relresult rr ON rc.idrelresult=rr.idrelresult wHERE rc.idrelresult='".$eselcab['idrelresult']."' ORDER BY posicao";
$eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos campos");
while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
    $campos[] = array('campo' => $lselcampos['campo'],'descri' => $lselcampos['descricampo'],'valor' => $lselcampos['valor']);
}
ksort($campos);
$rel->idrelresult = $eselcab['idrelresult'];
$frequencia = $eselcab['tipo'];
$rel->idrelcab = $eselcab['idrelresultcab'];
$rel->idsrelfiltros = $rel->vars['idrel_filtros'];

foreach($filtros as $fi) {
    foreach($fi as $f) {
        if($f == "FILTROS") {
            foreach($rel->vars as $knf => $nf) {
                if($nf != "") {
                    if(eregi('dtinimoni',$knf) || eregi('dtfimmoni',$knf) || eregi('dtinictt',$knf) || eregi('dtfimctt',$knf) || eregi('fg',$knf) || eregi('fgm',$knf)) {
                        if(eregi('dtinimoni',$knf) || eregi('dtfimmoni',$knf)) {
                            if($rel->vars['dtinimoni'] != "" && $rel->vars['dtfimmoni']) {
                                if(array_key_exists("data", $apresfiltros)) {
                                }
                                else {
                                    $apresfiltros['data'] = "<strong>DATA MONITORIA: </strong>".$rel->vars['dtinimoni']." À ".$rel->vars['dtfimmoni'];
                                }
                            }
                            else {
                            }
                        }
                        if(eregi('dtinictt',$knf) || eregi('dtfimctt',$knf)) {
                            if($rel->vars['dtinictt'] != "" && $rel->vars['dtfimctt']) {
                                if(array_key_exists('datactt', $apresfiltros)) {
                                }
                                else {
                                    $apresfiltros['datactt'] = "<strong>DATA CONTATO: </strong>".$rel->vars['dtinictt']." À ".$rel->vars['dtfimctt'];
                                }
                            }
                            else {
                            }
                        }
                        if(eregi('fg',$knf) || eregi('fgm',$knf)) {
                            if($nf == "fg") {
                                $apresfiltros['fg'] = "<strong>FALHAS GRAVES: </strong>SIM";
                            }
                            if($nf == "fgm") {
                                $apresfiltros['fgm'] = "<strong>FALHAS GRIVISSIMAS: </strong>SIM";
                            }
                        }
                    }
                    if(eregi('filtro_',$knf)) {
                        $seldados = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$rel->vars[$knf]."'";
                        $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta do nome do filtro");
                        $apresfiltros[$knf] = "<strong>".strtoupper(str_replace("filtro_", "", $knf)).": </strong>".$eseldados['nomefiltro_dados'];
                    }
                    if(!eregi('dtinimoni',$knf) && !eregi('dtfimmoni',$knf) && !eregi('dtinictt',$knf) && !eregi('dtfimctt',$knf) && !eregi('fg',$knf) && !eregi('fgm',$knf) && !eregi('filtro_',$knf)) {
                        if($knf == "idrel_filtros") {
                        }
                        else {
                            $plan = array_search($knf, $planapres);
                            $var = array_search($knf, $colvars);
                            if($knf == "atustatus") {
                                $selvalor = "SELECT $var, COUNT(*) as result FROM status s INNER JOIN rel_fluxo rf ON rf.idatustatus = s.idstatus WHERE id$knf='$nf'";
                            }
                            else {
                                $selvalor = "SELECT $var, COUNT(*) as result FROM $plan WHERE $knf='$nf'";
                            }
                            $eselvalor = $_SESSION['fetch_array']($_SESSION['query']($selvalor)) or die ("erro na query para consultar o valor da variável");
                            if($eselvalor['result'] >= 1) {
                                $apresfiltros[$var] = "<strong>".array_search($knf, $descrivars).": </strong>".$eselvalor[$var];
                            }
                            else {
                                $apresfiltros[$var] = "<strong>".array_search($knf, $descrivars).": </strong>VALOR NÃO ENCONTRADO";
                            }
                        }
                    }
                }
                else {
                }
            }
        }
        if($f == "USUARIO") {
            $apresfiltros[] = "<strong>USUÁRIO: </strong>".$_SESSION['usuarioNome'];
        }
        if($f == "DATA") {
            $apresfiltros[] = "<strong>DATA: </strong>".date('d/m/Y');
        }
        if($f == "DATA CONTATO") {
            if($rel->vars['dtinictt'] != "" && $rel->vars['dtfimctt'] != "") {
                $apresfiltros[] = "<strong>DATA CONTATO: </strong>".$rel->vars['dtinictt']." à ".$rel->vars['dtfimctt'];
            }
            else {
            }
        }
        if($f == "RELACIONAMENTO") {

        }
    }
}
?>

<table width=\"1024\">
    <tr>
        <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome="<?php echo strtoupper($eselcab['nomerelresult']);?>"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">EXCEL</div></a></td>
        <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome="<?php echo strtoupper($eselcab['nomerelresult']);?>"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">WORD</div></a></td>
        <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome="<?php echo strtoupper($eselcab['nomerelresult']);?>"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">PDF</div></a></td>
    </tr>
</table><br />
<?php
if($idsrel == "") {
    ?>
    <table width=\"1024\" style="background-color: #CCC">
        <tr>
            <td style="color:#F00; text-align: center"><strong>NÃO FORAM ENCONTRADOS RELACIONAMENTOS CADASTRADOS PARA OS FILTROS SELECIONADOS</strong></td>
        </tr>
    </table>
    <script type="text/javascript">
        $(document).ready(function() {
            $.unblockUI();
        })
    </script>
    <?php
}
else {
?>
<div id="relatorio" style="width: 1024px; font-size:12px">
    <?php
    $rel->dados = "<table width=\"1024\" style=\"border: 2px solid #CCC\">";
    $rel->dados .= "<tr style=\"border-botton: 1px solid #000\">"; 
      $posicao = array('E' => 'left','C' => 'center', 'D' => 'right');
      $l = 0;
      foreach($logos as $klg => $lg) {
        $l++;
        $host = $_SERVER['HTTP_HOST'];
        $rel->dados .= "<td align=\"".$posicao[$klg]."\"><img src=\"".str_replace($rais,"",$lg)."\" width=\"".$eselcab['width']."\" height=\"".$eselcab['height']."\" alt=\"".$l."\" /></td>";
      }
      $rel->dados .= "</tr>";
      $rel->dados .= "<tr><td colspan=\"".$clogo."\"><br /></td>";
      $rel->dados .= "</tr>";
      $rel->dados .= "<tr>";
      $rel->dados .= "<td colspan=\"".$clogo."\" align=\"".$posicao[$posic]."\">";
        foreach($apresfiltros as $apf) {
            $rel->dados .= "$apf<br />";
        }
        $rel->dados .= "</td>";
        $rel->dados .= "</tr>";
        $rel->dados .= "</table><br />";
        $rel->dados .= "<table width=\"1024\" style=\"border: 2px solid #CCC\">";
        $l = 0;
        $cc = 0;
        $visucampos = $campos;
        $acampos = array('P' => 'PERIODO','QTMO' => 'QTDE. MONITORIAS', 'F' => 'FORMULA','QTOP' => 'QTDE. OPERADOR','QTMOP' => 'QTDE. MONITORIAS/ OPERADOR', 'QTSUPER' => 'QTDE. SUPERVISOR','MO' => 'MÉDIA OPERADOR','QTFG' => 'QTDE. FG/ MONITORIA','QTFGOP' => 'QTDE. FG/ OPERADOR', 'QTFGI' => 'QTDE. FG/ ITENS','QTFGIOP' => 'QTDE. FGI/ OPERADOR', 'QTFGM' => 'QTDE. FGM/ MONITORIA','QTFGMOP' => 'QTDE. FGM/ OPERADOR','QTFGMIOP' => 'QTDE. FGMI/ OPERADOR', 'QTFGMI' => 'QTDE. FGM/ ITENS', 'O' => 'OUTROS/ DIGITE O TEXTO');
        $rel->where_sql();
        foreach($campos as $ca) {
            // inicia a linha
            if($l == 0) {
                $rel->dados .= "<tr>";
            }
            else {
            }

            // verifica qual variável do array vai usar para nome da coluna
            if($ca['descri'] != "") {
                $descri = $ca['descri'];
            }
            else {
                if($ca['campo'] == "F") {
                }
                else {
                    $descri = $acampos[$ca['campo']];
                }
            }

            // verifica qual informação será apresentada e para cada possibilidade faz a adequação do valor da coluna
            if($ca['campo'] == "P") {
                $valor = $rel->valor_campo($ca['campo'],$rel->vars['idmonitoria'],'');
            }
            if($ca['campo'] == "QTMO") {
                $valor = $rel->valor_campo($ca['campo'],'', '');
            }
            if($ca['campo'] == "QTMOP") {
                $valor = $rel->valor_campo($ca['campo'],'', '');
            }
            if($ca['campo'] == "F") {
                $formula = "SELECT nomeformulas, formula FROM formulas f INNER JOIN relresultcampos rrc ON rrc.idformula = f.idformulas WHERE f.idrelresult='$rel->idrelresult'";
                $eformula = $_SESSION['fetch_array']($_SESSION['query']($formula)) or die ("erro na query de consulta do nome da formula");
                $lformula = $eformula['formula'];
                $valor = $rel->valor_campo($ca['campo'],'',$lformula);
                if($ca['descri'] != "") {
                }
                else {
                    $descri = $eformula['nomeformulas'];
                }
            }
            if($ca['campo'] == "QTOP") {
                $valor = $rel->valor_campo($ca['campo'],'', '');
            }
            if($ca['campo'] == "QTSUPER") {
                $valor = $rel->valor_campo($ca['campo'],'', '');
            }
            if($ca['campo'] == "MO") {
                $valor = $rel->valor_campo($ca['campo'],'', '');
            }
            if($ca['campo'] == "QTFG" OR $ca['campo'] == "QTFGM" OR $ca['campo'] == "QTFGI" OR $ca['campo'] == "QTFGMI") {
                $valor = $rel->valor_campo($ca['campo'],'', '');
            }
            if($ca['campo'] == "QTFGOP" OR $ca['campo'] == "QTFGMOP" OR $ca['campo'] == "QTFGIOP" OR $ca['campo'] == "QTFGMIOP") {
                $valor = $rel->valor_campo($ca['campo'], '', '');
            }
            if($ca['campo'] == "O") {
                $valor = $ca['valor'];
            }
            $rel->dados .= "<td width=\"156\" bgcolor=\"#FFD1BB\" style=\"font-size:12px\"><strong>$descri</strong></td>";
            $rel->dados .= "<td width=\"346\" align=\"center\" style=\"font-size:12px\">$valor</td>";

            // verifica quantas colunas foram criadas e caso tenha somente passado 1 vez acrescenta mais uma coluna na linha
            if($l == 0) {
                $l++;
            }
            else {
                if($l == 1) {
                    $rel->dados .= "</tr>";
                    $l++;
                }
                if($l == 2) {
                    $l = 0;
                }
            }
        }
    $rel->dados .= "</tr>";
    $rel->dados .= "</table><br /><hr />";
    $rel->dados .= "<div id=\"corpo\">";
        $selrelcorpo = "SELECT * FROM relresultcorpo WHERE idrelresult='$rel->idrelresult' ORDER BY idrelresultcorpo";
        $ecorpo = $_SESSION['query']($selrelcorpo) or die ("erro na query de consulta do corpo do relatório");
        while($lcorpo = $_SESSION['fetch_array']($ecorpo)) {
            $tipodados = array('H' => 'HISTÓRICO', 'C' => 'COMPARATIVO', 'CON' => 'CONSOLIDADO');
            $nomedados = array('F' => 'CALCULO FORMULA','MO' => 'MÉDIA OPERADOR', 'MS' => 'MÉDIA SUPERVISOR','P' => 'PLANILHA','PI' => 'PARARETO ITENS','PG' => 'PARETO GRUPOS','A' => 'AMOSTRA', 'IT' => 'INTERVALO NOTAS', 'TAB' => 'TABULACAO');
            unset($rel->filtros);
            $rel->filtros;
            $rel->idtabulacao = $lcorpo['tabulacao'];
            $rel->dados_apres($lcorpo['frequenciadados'],$lcorpo['idrelresultcorpo'],$lcorpo['tipodados'],$lcorpo['dados'],$lcorpo['idformula'],$lcorpo['agrupadados'],$lcorpo['apresdados'],$lcorpo['tipografico']);
            if($lcorpo['tipodados'] == "C" AND ($lcorpo['agrupadados'] != "" && $lcorpo['agrupadados'] != "MS" && $lcorpo['agrupadados'] != "MO")) {
                $selfiltros = "SELECT nomefiltro_dados FROM filtro_dados fd
                               INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE nomefiltro_nomes LIKE '%".$lcorpo['agrupadados']."%'";
                $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta do nome dos fitlros");
                while($lselfiltros = $_SESSION['fetch_array']($eselfiltros)) {
                    $nomefiltros[] = $lselfiltros['nomefiltro_dados'];
                }
           }
            $rel->dados .= "<br/>";
            if($lcorpo['apresdados'] == "TABELA") {
            $rel->dados .= "<div style=\"width:1020px; text-align:center; background-color:#999; border:2px solid #CCC\">";
            $rel->dados .= "<strong>".$tipodados[$lcorpo['tipodados']]." - ".$nomedados[$lcorpo['dados']]."</strong><br />";
            $rel->dados .= "</div>";
            $rel->dados .= "<br />";
            $rel->dados .= "<table width=\"1024\">";
              $f = 0;
              $cfreq = count($rel->frequencia);
              for($f = 0; $f < $cfreq; $f++) {
                $rel->dados .= "<tr>";
                $rel->dados .= "<td width=\"241\"></td>";
                if($rel->filtros != "") {
                    $counccol = ((count($rel->apresdados) - 1) * count($rel->filtros));
                }
                else {
                    $counccol = count($rel->apresdados) - 1;
                }
                foreach($rel->frequencia[$f] as $kcol => $ar) {
                    $rel->dados .= "<td align=\"center\" colspan=\"$counccol\" bgcolor=\"#6699CC\"><strong>".$kcol."</strong></td>";
                }
                $rel->dados .= "</tr>";
                if($rel->filtros != "") {
                $rel->dados .= "<tr>";
                   $rel->dados .= "<td></td>";
                   $colspan = count($rel->apresdados) - 1;
                   foreach($rel->frequencia[$f] as $kf) {
                       foreach($nomefiltros as $kfnome) {
                           $rel->dados .= "<td colspan=\"".$colspan."\" style=\"color:#FFF; background-color:#000000; text-align:center\">".$kfnome."</td>";
                       }
                   }  
                $rel->dados .= "</tr>";
                }
                else {
                }
                $rel->dados .= "<tr>";
                    if($lcorpo['dados'] == "F") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>FORMULA</strong></td>";
                    }
                    if($lcorpo['dados'] == "MO" OR $lcorpo['dados'] == "MS") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>NOME</strong></td>";
                    }
                    if($lcorpo['dados'] == "P") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>DESCRIÇÃO</strong></td>";
                    }
                    if($lcorpo['dados'] == "PG" OR $lcorpo['dados'] == "PI") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>DESCRIÇÃO</strong></td>";
                    }
                    if($lcorpo['dados'] == "A") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>RELACIONAMENTO</strong></td>";
                    }
                    if($lcorpo['dados'] == "IT") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>INTERVALO NOTAS</strong></td>";
                    }
                    if($lcorpo['dados'] == "TAB") {
                        $rel->dados .= "<td width=\"241\" bgcolor=\"#CCCCCC\"><strong>PERGUNTAS/ RESPOSTAS</strong></td>";
                    }
                    if($rel->filtros != "") {
                        $cf = (count($rel->frequencia[$f]) * count($rel->filtros));
                    }
                    else {
                        $cf = count($rel->frequencia[$f]);
                    }
                    cortdrel($lcorpo['apresdados'],$lcorpo['dados'],"cor1");
                    $cor1 = "bgcolor=\"#A2D0EB\"";
                    $cor2 = "bgcolor=\"#92B2EB\"";
                    for($i = 0; $i < $cf; $i++) {
                        if(is_float(($i / 2))) {
                            $cor = $cor1;
                        }
                        else {
                            $cor = $cor2;
                        }
                        foreach($rel->apresdados as $col) {
                            if($col == "descri") {
                            }
                            else {
                                if($lcorpo['dados'] == "IT") {
                                    $quebra = explode("_",$col);
                                    $newcol = $quebra[0]."<br/>".$quebra[1]."_".$quebra[2];
                                    $rel->dados .= "<td align=\"center\" width=\"50\" $cor><strong>".$newcol."</strong></td>";
                                }
                                else {
                                    $rel->dados .= "<td align=\"center\" width=\"50\" $cor><strong>".$col."</strong></td>";
                                }
                            }
                        }
                    }
                $rel->dados .= "</tr>";
                $capres = 0;
                    foreach($rel->dadosrel as $kdados => $dados) {
                        $rel->dados .= "<tr>";
                            $i = 0;
                            foreach($rel->frequencia[$f] as $kfreq => $nfreq) {
                                if($lcorpo['tipodados'] == "C") {
                                    foreach($nomefiltros as $nome) {
                                        foreach($rel->apresdados as $kapres => $apres) {
                                            if($kapres == "descri" && $capres >= 1) {
                                            }
                                            else {
                                                if($kapres == "descri") {
                                                    $align = "";
                                                    $cord = "bgcolor=\"#CCCCCC\"";
                                                    $cord = cortdrel($lcorpo['apresdados'], $lcorpo['dados'], "cor1", $kdados, $kapres);
                                                    if($lcorpo['tipodados'] == "C") {
                                                        $rel->dados .= "<td $align $cord>".$dados[$kfreq][$nome][$kapres]."</td>";
                                                    }
                                                    else {
                                                        $rel->dados .= "<td $align $cord>".$dados[$kfreq][$kapres]."</td>";
                                                    }
                                                }
                                                else {
                                                    $cor1 = cortdrel($lcorpo['apresdados'], $lcorpo['dados'], "cor1", $kdados, $kapres);
                                                    $cor2 = cortdrel($lcorpo['apresdados'], $lcorpo['dados'], "cor2", $kdados, $kapres);
                                                    if(is_float(($i / 2))) {
                                                        $cor = $cor1;
                                                    }
                                                    else {
                                                        $cor = $cor2;
                                                    }
                                                    $align = "align=\"center\"";
                                                    if($lcorpo['tipodados'] == "C") {
                                                        $rel->dados .= "<td $align $cor>".$dados[$kfreq][$nome][$kapres]."</td>";
                                                    }
                                                    else {
                                                        $rel->dados .= "<td $align $cor>".$dados[$kfreq][$kapres]."</td>";
                                                    }
                                                }
                                            }
                                            if($kapres == "descri") {
                                                $capres++;
                                            }
                                            else {
                                            }
                                        }
                                    }
                                }
                                else {
                                    foreach($rel->apresdados as $kapres => $apres) {
                                        if($kapres == "descri" && $capres >= 1) {
                                        }
                                        else {
                                            if($kapres == "descri") {
                                                $align = "";
                                                $cord = "bgcolor=\"#CCCCCC\"";
                                                $cord = cortdrel($lcorpo['apresdados'], $lcorpo['dados'], "cor1", $kdados, $kapres);
                                                $rel->dados .= "<td $align $cord>".$dados[$kfreq][$kapres]."</td>";
                                            }
                                            else {
                                                $cor1 = cortdrel($lcorpo['apresdados'], $lcorpo['dados'], "cor1", $kdados, $kapres);
                                                $cor2 = cortdrel($lcorpo['apresdados'], $lcorpo['dados'], "cor2", $kdados, $kapres);
                                                if(is_float(($i / 2))) {
                                                    $cor = $cor1;
                                                }
                                                else {
                                                    $cor = $cor2;
                                                }
                                                $align = "align=\"center\"";
                                                $rel->dados .= "<td $align $cor>".$dados[$kfreq][$kapres]."</td>";
                                            }
                                        }
                                        if($kapres == "descri") {
                                            $capres++;
                                        }
                                        else {
                                        }
                                    }
                                }
                                $i++;
                            }
                            $capres = 0;
                        $rel->dados .= "</tr>";
                    }
                    $rel->dados .= "<tr><td colspan=\"10\"><br /></td></tr>";
                }
            $rel->dados .= "</table>"; 
            }
            if($lcorpo['apresdados'] == "GRAFICO") {
                $f = 0;
                $posileg = array('esquerda' => 'left','centro' => 'center', 'direita' => 'right');
                $categorias = array();
                $cfreq = count($rel->frequencia);
                $graf = new Grafico();
                $graf->render = "visugraf".$lcorpo['idrelresultcorpo'];
                $graf->titulo = $tipodados[$lcorpo['tipodados']]." - ".$nomedados[$lcorpo['dados']];
                $graf->subtitulo = $rel->vars['dtinimoni']." - ".$rel->vars['dtfimmoni'];
                $graf->backgroundColor = $lcorpo['corfdgraf'];
                $graf->pointwidth = $lcorpo['pointwidth'];
                $graf->dataLabels = $lcorpo['valores'];
                $graf->corlabel = $lcorpo['cordados'];
                $graf->labelsXrotation = $lcorpo['textrotacao'];
                $graf->legend = $lcorpo['legenda'];
                $graf->layoutlegend = $lcorpo['orientaleg'];
                $graf->colorlegend = "CCCCCC";
                $graf->alignlegend = $posileg[$lcorpo['posileg']];
                $graf->verticalalign = "top";
                $graf->legendX = $lcorpo['posicaoX'];
                $graf->legendY = $lcorpo['posicaoY'];
                $graf->margin = $lcorpo['margem'];
                $graf->marginbottom = $lcorpo['marginbottom'];
                $graf->width = $lcorpo['widthgraf'];
                $graf->heigth = $lcorpo['heightgraf'];
                $graf->tipograf = $lcorpo['tipografico'];
                $graf->aprestooltip = "SIM";
                $posi = 0;
                foreach($rel->dadosrel as $kdados => $dados) {
                    foreach($rel->frequencia[$f] as $kfreq => $nfreq) {
                        if($lcorpo['dados'] == "MO" OR $lcorpo['dados'] == "MS") {
                            $categorias[] = $dados[$kfreq]['descri'];
                            $apres = array('media','perc_fg');
                            $graf->tituloY = $apres;
                            foreach($apres as $ap) {
                                $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                $graf->seriestype[$ap] = $eselcol['tipograf'];
                                $graf->seriesnome[$ap] = $eselcol['campo'];
                                $graf->seriescor[$ap] = $eselcol['cor'];
                                if($dados[$kfreq][$ap] == "--") {
                                    $seriedados = 0;
                                }
                                else {
                                    $seriedados = $dados[$kfreq][$ap];
                                }
                                $graf->seriesdados[$ap][] = $seriedados;
                            }
                        }
                        if($lcorpo['dados'] == "P") {
                            if(eregi('ggrupo',$kdados)) {
                                $apres = array('possivel','media');
                                $graf->tituloY = $apres;
                                $categorias[] = $dados[$kfreq]['descri'];
                                foreach($apres as $ap) {
                                    $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                    $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                    $graf->seriestype[$ap] = $eselcol['tipograf'];
                                    $graf->seriesnome[$ap] = $eselcol['campo'];
                                    $graf->seriescor[$ap] = $eselcol['cor'];
                                    $graf->seriesdados[$ap][] = $dados[$kfreq][$ap];
                                }
                            }
                            else {
                            }
                        }
                        if($lcorpo['dados'] == "PG" OR $lcorpo['dados'] == "PI") {
                            $apres = array('perc','perc_acumu');
                            $graf->tituloY = $apres;
                            $graf->yaxis = "N";
                            $categorias[] = $dados[$kfreq]['descri'];
                            foreach($apres as $ap) {
                                $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                if(eregi('ggrupo',$kdados)) {
                                    $graf->seriestype[$ap] = $eselcol['tipograf'];
                                    $graf->seriesnome[$ap] = $eselcol['campo'];
                                    $graf->seriescor[$ap] = $eselcol['cor'];
                                    $graf->seriesdados[$ap][] = $dados[$kfreq][$ap];
                                }
                                if(eregi('pergunta',$kdados)) {
                                    $graf->seriestype[$ap] = $eselcol['tipograf'];
                                    $graf->seriesnome[$ap] = $eselcol['campo'];
                                    $graf->seriescor[$ap] = $eselcol['cor'];
                                    $graf->seriesdados[$ap][] = $dados[$kfreq][$ap];
                                }
                                else {
                                }
                            }
                        }
                        if($lcorpo['dados'] == "F") {
                            $categorias[] = $kfreq;
                            $apres = array('qtde_moni','resultado');
                            $graf->tituloY = $apres;
                            if($rel->filtros != "") {
                                $graf->yaxis = "S";
                                $cores = array('5B94EA','EA6569','699962','FF7DFB','FF8C19','B3E3FF','0818FF','FF0000','FFFA69');
                                //$graf->stack = "S";
                                foreach($nomefiltros as $ngraf) {
                                    foreach($apres as $ap) {
                                        $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                        $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                        //$graf->seriestype[$ap] = $eselcol['tipograf'];
                                        $graf->seriestype[$ngraf][$ap] = $eselcol['tipograf'];
                                        $graf->seriesnome[$ngraf][$ap] = $ngraf."_".$ap;
                                        $graf->seriescor[$ngraf][$ap] = current($cores);
                                        if($dados[$kfreq][$ngraf][$ap] == "") {
                                            $dadosgraf = 0;
                                        }
                                        else {
                                            $dadosgraf = $dados[$kfreq][$ngraf][$ap];
                                        }
                                        $graf->seriesdados[$ngraf][$ap][] = $dadosgraf;
                                    }
                                    next($cores);
                                }
                            }
                            else {
                                foreach($apres as $ap) {
                                    $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                    $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                    $graf->seriestype[$ap] = $eselcol['tipograf'];
                                    $graf->seriesnome[$ap] = $eselcol['campo'];
                                    $graf->seriescor[$ap] = $eselcol['cor'];
                                    $graf->seriesdados[$ap][] = $dados[$kfreq][$ap];
                                }
                            }
                        }
                        if($lcorpo['dados'] == "A") {
                            $categorias[] = $dados[$kfreq]['descri'];
                            $apres = array('qtde_real','qtde_imp');
                            $graf->tituloY = $apres;
                            foreach($apres as $ap) {
                                $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                $graf->seriestype[$ap] = $eselcol['tipograf'];
                                $graf->seriesnome[$ap] = $eselcol['campo'];
                                $graf->seriescor[$ap] = $eselcol['cor'];
                                $graf->seriesdados[$ap][] = $dados[$kfreq][$ap];
                            }
                        }
                        if($lcorpo['dados'] == "IT") {
                            $graf->yaxis = "N";
                            if(in_array($kfreq,$categorias)) {
                            }
                            else {
                                if($lcorpo['tipodados'] == "H") {
                                    $categorias[] = $kfreq;
                                }
                                else {
                                    $categorias[] = $dados[$kfreq]['descri'];
                                }
                            }
                            foreach($rel->apresdados as $ap) {
                                if($ap == "descri" OR !eregi('perc',$ap)) {
                                }
                                else {
                                    $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='".strtoupper($ap)."'";
                                    $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                    if($dados[$kfreq][$ap] == "--") {
                                        $dadosgraf = 0;
                                    }
                                    else {
                                        $dadosgraf = $dados[$kfreq][$ap];
                                    }
                                    if($lcorpo['tipodados'] == "H") {
                                        $graf->seriestype[$dados[$kfreq]['descri']."_".$ap] = $eselcol['tipograf'];
                                        $graf->seriesnome[$dados[$kfreq]['descri']."_".$ap] = $dados[$kfreq]['descri']." ".str_replace("perc_", "", $ap);
                                        $graf->seriescor[$dados[$kfreq]['descri']."_".$ap] = $eselcol['cor'];
                                        $graf->seriestack[$dados[$kfreq]['descri']."_".$ap] = $ap;
                                        $graf->stack = "S";
                                        $graf->seriesdados[$dados[$kfreq]['descri']."_".$ap][] = $dadosgraf;
                                    }
                                    else {
                                        $graf->seriestype[$ap] = $eselcol['tipograf'];
                                        $graf->seriesnome[$ap] = $ap;
                                        $graf->seriescor[$ap] = $eselcol['cor'];
                                        $graf->seriesdados[$ap][] = $dadosgraf;
                                    }
                                }
                            }
                        }
                        if($lcorpo['dados'] == "TAB") {
                            $graf->yaxis = "N";
                            $verifperg = "SELECT COUNT(DISTINCT(idperguntatab)) as result FROM tabulacao WHERE idtabulacao='$rel->idtabulacao'";
                            $everif = $_SESSION['fetch_array']($_SESSION['query']($verifperg)) or die ("erro na query de consulta da quantidade de perguntas");
                            $pergs = $everif['result'];
                            $graf->stack = "S";
                            if(eregi("perg",$kdados)) {
                                $categorias[] = $dados[$kfreq]['descri'];
                                $descristack = $dados[$kfreq]['descri'];
                                $posi++;
                            }
                            else {
                                //$apres = array('qtde','perc');
                                //foreach($apres as $ap) {
                                $selcol = "SELECT * FROM graficocorpo WHERE idrelresultcorpo='".$lcorpo['idrelresultcorpo']."' AND campo='QTDE'";
                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do gráfico");
                                $graf->seriestype[$kdados] = $eselcol['tipograf'];
                                $graf->seriesnome[$kdados] = $dados[$kfreq]['descri'];
                                $graf->seriescor[$kdados] = $eselcol['cor'];
                                $graf->seriestack[$kdados] = "";
                                for($i = 1; $i <= $pergs; $i++) {
                                    if($i == $posi) {
                                        if($dados[$kfreq]['qtde'] == "--") {
                                            $graf->seriesdados[$kdados][] = 0;
                                        }
                                        else {
                                            $graf->seriesdados[$kdados][] = $dados[$kfreq]['qtde'];
                                        }
                                    }
                                    else {
                                        $graf->seriesdados[$kdados][] = 0;
                                    }
                                }
                                //}
                            }

                        }
                    }
                }
                $graf->categorias = implode("','",$categorias);
                $_SESSION['dados'] = $lcorpo['dados'];
                $graf->geragrafico($rel->filtros,$rel->apresdados);
                $graf->visudados($rel->filtros,$rel->apresdados);
                $rel->dados .= "<div id=\"".$graf->render."\" style=\"width: 100%\"></div>";
            }
            //$rel->dados .= $graf->visudados($rel->filtros,$rel->apresdados);
            $rel->dados .= "<br /><hr />";
        }
    $rel->dados .= "</div>";
    $options = array("output-xhtml" => true, "clean" => true);
    $_SESSION['dadosexp'] = tidy_parse_string($rel->dados,$options);
    $_SESSION['dadosexp'] = tidy_get_output($_SESSION['dadosexp']);
    echo $rel->dados;
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $.unblockUI();
        })
    </script> 
</div>
<?php
}
?>
