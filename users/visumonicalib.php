<?php

session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.monitoria.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");

protegePagina();

$cor = new CoresSistema();
$idmonicalib = $_GET['idmonicalib'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<?php
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
?>
</head>
<body>
    <div id="tudo">
    	<div id="conteudo" style="background-color: #F0F0F6">
            <?php
            $moni = new Planilha();
            $moni->iduser = $_SESSION['usuarioID'];
            $moni->perfiluser = $_SESSION['usuarioperfil'];
            $moni->tabuser = $_SESSION['user_tabela'];
            $moni->pagina = "CALIBRAGEM";
            $moni->idmoni = $idmonicalib;
            $moni->DadosPlanVisualiza();
            $moni->MontaPlan_corpovisu();
            ?>
            <!--<script type="text/javascript">
                $(window).load("/monitoria_supervisao/delaudiotmp.php?caminho=<?php echo $moni->caminho;?>")
            </script>-->
        </div>
        <div id="rodape">
            <table align="center" width="344" border="0">
                <tr>
                    <td width="224" align="center"><img src="/monitoria_supervisao/images/logo/Logo_Flor_vector_75X35.png" width="75" height="38" /></td>
                </tr>
                <tr>
                    <td height="15" align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
