<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais."/monitoria_supervisao/seguranca.php");
include_once($rais."/monitoria_supervisao/config/conexao.php");
include_once($rais."/monitoria_supervisao/selcli.php");

$tipo = $_POST['tpacao'];
$indice = $_POST['indice'];
$fluxo = $_POST['fluxo'];
$filtro = $_POST['filtro'];

if(isset($_POST['indice']) && $tipo == "indice") {
    echo "<option value=\"\" selected=\"selected\" style=\"height:15px;padding:5px\">Selecione...</option>";
    $selinter = "SELECT idintervalo_notas, nomeintervalo_notas FROM intervalo_notas WHERE idindice='$indice'";
    $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta do intervalor");
    while($lselinter = $_SESSION['fetch_array']($eselinter)) {
        echo "<option value=\"".$lselinter['idintervalo_notas']."\" style=\"height:15px;padding:5px\">".$lselinter['nomeintervalo_notas']."</option>";
    }
}
if(isset($_POST['filtro']) && $tipo == "indice") {
    echo "<option value=\"\" selected=\"selected\" style=\"height:15px;padding:5px\">Selecione...</option>";
    $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
    $eselfitlros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta do nome dos filtros");
    while($lself = $_SESSION['fetch_array']($eselfitlros)) {
        if($_POST[strtolower($lself['nomefiltro_nomes'])] != "") {
            $where[] = "rf.id_".strtolower($lself['nomefiltro_nomes'])."='".$_POST[strtolower($lself['nomefiltro_nomes'])]."'";
        }
        else {
        }
    }
    $selinter = "SELECT i.idindice, i.nomeindice FROM indice i
                INNER JOIN param_moni pm ON pm.idindice = i.idindice
                INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros
                WHERE ".implode(" AND ",$where)." GROUP BY i.idindice ORDER BY i.idindice";
    $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta dos intervalos");
    while($lselinter = $_SESSION['fetch_array']($eselinter)) {
        echo "<option value=\"".$lselinter['idindice']."\" style=\"height:15px;padding:5px\">".$lselinter['nomeindice']."</option>";
    }    
}

if(isset($_POST['fluxo']) && $tipo == "fluxo") {
    echo "<option value=\"\" selected=\"selected\" style=\"height:15px;padding:5px\">Selecione...</option>";
    $selstatus = "SELECT s.idstatus, s.nomestatus FROM rel_fluxo rf
                  INNER JOIN status s ON s.idstatus = rf.idatustatus
                  WHERE rf.idfluxo='$fluxo' GROUP BY s.idstatus";
    $eselstatus = $_SESSION['query']($selstatus) or die ("erro na query de consulta dos status");
    while($lselstatus = $_SESSION['fetch_array']($eselstatus)) {
        echo "<option value=\"".$lselstatus['idstatus']."\" style=\"height:15px;padding:5px\">".$lselstatus['nomestatus']."</option>";
    }    
}
if(isset($_POST['filtro']) && $tipo == "fluxo") {
    echo "<option value=\"\" selected=\"selected\" style=\"height:15px;padding:5px\">Selecione...</option>";
    $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
    $eselfitlros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta do nome dos filtros");
    while($lself = $_SESSION['fetch_array']($eselfitlros)) {
        if($_POST[strtolower($lself['nomefiltro_nomes'])] != "") {
            $where[] = "rf.id_".strtolower($lself['nomefiltro_nomes'])."='".$_POST[strtolower($lself['nomefiltro_nomes'])]."'";
        }
        else {
        }
    }
    $selstatus = "SELECT f.idfluxo, f.nomefluxo FROM fluxo f
                  INNER JOIN conf_rel cr ON cr.idfluxo = f.idfluxo
                  INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros
                  WHERE ".implode(" AND ",$where)." GROUP BY f.idfluxo";
    $eselstatus = $_SESSION['query']($selstatus) or die ("erro na query para consulta dos status");
    while($lsels = $_SESSION['fetch_array']($eselstatus)) {
        echo "<option value=\"".$lsels['idfluxo']."\" style=\"height:15px;padding:5px\">".$lsels['nomefluxo']."</option>";
    }
}

?>
