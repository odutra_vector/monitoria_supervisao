<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include($rais.'/monitoria_supervisao/config/conexao.php');
include($rais.'/monitoria_supervisao/selcli.php');
include($rias.'/monitoria_supervisao/classes/class.monitoria.php');

$selmin = "SELECT MIN(nivel), nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
$eselmin = $_SESSION['fetch_array']($_SESSION['query']($selmin)) or die ("erro na query para consulta do menor filtro cadastrado");
$fmin = $eselmin['nomefiltro_nomes'];
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#relcalib').tablesorter();
    });
</script>
<div style="width:1024px; margin: auto; overflow: auto; height: 200px; text-align: center; background-color: #C5C5C5;">
    <table width="773" align="center">
        <tr>
            <td colspan="6" align="center" class="corfd_coltexto"><strong>CALIBRAGENS AGENDADAS</strong></td>
        </tr>
    </table>
    <table width="773" align="center" id="relcalib">
        <thead>
          <tr>
            <th width="42" align="center" bgcolor="#999999"><strong>ID</strong></th>
            <th width="102" align="center" bgcolor="#999999"><strong>DATA INICIAL</strong></th>
            <th width="102" align="center" bgcolor="#999999"><strong>DATA FIM</strong></th>
            <th width="302" align="center" bgcolor="#999999"><strong>RELACIONAMENTO</strong></th>
            <th width="115" align="center" bgcolor="#999999"><strong>STATUS</strong></th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(isset($_GET['opmenu'])) {
              $opmenu = $_GET['opmenu'];
          }
          else {
              $opmenu = "REAL. CALIBRAGEM";
          }
          if(isset($_GET['submenu'])) {
              $sub = $_GET['submenu'];
          }
          else {
              $sub = "OK";
          }
          $periodo = periodo();
          if($periodo == "") {
              echo "<tr>";
                echo "<td align=\"center\" bgcolor=\"#FFFFFF\" colspan=\"5\">NÃO EXISTEM CALIBRAGENS AGENDADAS PARA ESTE PERÍODO</td>";
              echo "</tr>";
          }
          else {
              $dataatu = date('Y-m-d');
              $seldatas = "SELECT dataini, datafim FROM periodo WHERE idperiodo='$periodo'";
              $eseldatas = $_SESSION['fetch_array']($_SESSION['query']($seldatas)) or die ("erro na query para consulta da faixa de data do perÃ­odo");

              $selcalib = "SELECT a.idagcalibragem, a.dataini, a.datafim, a.participantes, pm.tipomoni, a.idmonitoria, a.idrel_filtros, a.finalizado FROM agcalibragem a
                           INNER JOIN rel_filtros rf ON rf.idrel_filtros = a.idrel_filtros
                           INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                           INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                           WHERE participantes='".$_SESSION['participante']."' AND a.dataini <= '$dataatu' AND a.finalizado='N' ORDER BY a.dataini, a.finalizado";
              $eselcalib = $_SESSION['query']($selcalib) or die ("erro na query da consulta das calibragens agendadas");
              $ncalib = $_SESSION['num_rows']($eselcalib);
              if($ncalib == 0) {
                  echo "<tr>";
                    echo "<td align=\"center\" bgcolor=\"#FFFFFF\" colspan=\"5\"><strong>NÃO EXISTEM CALIBRAGENS AGENDADAS PARA ESTE PERÍODO</strong></td>";
                  echo "</tr>";
              }
              else {
                  //$numcalib = $_SESSION['num_rows']($eselcalib) or die ("erro na consulta da quantidade de calibragens agendadas");
                  while($lselcalib = $_SESSION['fetch_array']($eselcalib)) {
                          echo "<tr>";
                                echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><a style=\"text-decoration:none; color:#000;\" href=\"inicio.php?menu=calibragem&opmenu=".$opmenu."&submenu=".$sub."&idmoni=".$lselcalib['idmonitoria']."&idcalib=".$lselcalib['idagcalibragem']."&tipomoni=".$lselcalib['tipomoni']."\">".$lselcalib['idagcalibragem']."</a></td>";
                                echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><a style=\"text-decoration:none; color:#000;\" href=\"inicio.php?menu=calibragem&opmenu=".$opmenu."&submenu=".$sub."&idmoni=".$lselcalib['idmonitoria']."&idcalib=".$lselcalib['idagcalibragem']."&tipomoni=".$lselcalib['tipomoni']."\">".banco2data($lselcalib['dataini'])."</a></td>";
                                echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><a style=\"text-decoration:none; color:#000;\" href=\"inicio.php?menu=calibragem&opmenu=".$opmenu."&submenu=".$sub."&idmoni=".$lselcalib['idmonitoria']."&idcalib=".$lselcalib['idagcalibragem']."&tipomoni=".$lselcalib['tipomoni']."\">".banco2data($lselcalib['datafim'])."</a></td>";
                                echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><a style=\"text-decoration:none; color:#000;\" href=\"inicio.php?menu=calibragem&opmenu=".$opmenu."&submenu=".$sub."&idmoni=".$lselcalib['idmonitoria']."&idcalib=".$lselcalib['idagcalibragem']."&tipomoni=".$lselcalib['tipomoni']."\">".nomevisu($lselcalib['idrel_filtros'])."</a></td>";
                                if($lselcalib['finalizado'] == "N") {
                                    $status = "EM ABERTO";
                                }
                                if($lselcalib['finalizado'] == "S") {
                                    $status = "ENCERRADO";
                                }
                                echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><a style=\"text-decoration:none; color:#000;\" href=\"inicio.php?menu=calibragem&opmenu=".$opmenu."&submenu=".$sub."&idmoni=".$lselcalib['idmonitoria']."&idcalib=".$lselcalib['idagcalibragem']."&tipomoni=".$lselcalib['tipomoni']."\">".$status."</a></td>";
                          echo "</tr>";
                  }
              }
          }
          ?>
        </tbody>
    </table>
</div>
<?php
if(isset($_GET['idmoni'])) {
    echo "<div style=\"width:1024px; text-align:center; margin:auto; background-color: #F0F0F6\"><br></br>";
    $idmoni = $_GET['idmoni'];
    $moni = new Planilha();
    $selmoni = "SELECT m.idmonitoria, m.idfila, m.idplanilha, m.gravacao, m.idrel_filtros, pm.tipomoni, m.horaini, m.horafim, m.inilig, m.tmpaudio FROM monitoria m
                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                WHERE idmonitoria='$idmoni'";
    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da monitoria de calibraÃ§Ã£o");
    $_SESSION['inimoni'] = $eselmoni['horaini'];
    $_SESSION['codini'] = 1;
    $seltempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('".$eselmoni['horaini']."') + TIME_TO_SEC('".$eselmoni['tmpaudio']."')) as tempo";
    $eseltempo = $_SESSION['fetch_array']($_SESSION['query']($seltempo)) or die ("erro na query de consulta do tempo total");
    $_SESSION['fimlig'] = $eseltempo['tempo'];
    $_SESSION['codfim'] = 1;
    $moni->filtro = "id_".strtolower($fmin);
    $moni->caminho = $eselmoni['gravacao'];
    $moni->idfila = $eselmoni['idfila'];
    $moni->idcalibragem = $_GET['idcalib'];
    if($eselmoni['tabfila'] == "fila_oper") {
        $moni->tipomoni = "O";
    }
    else {
        $moni->tipomoni = "G";
    }
    $moni->idplan = $eselmoni['idplanilha'];
    $pagina = "CALIBRAGEM";
    $eseldados = $moni->DadosPlanMoni($pagina);
    $moni->MontaPlan_dados();
    $moni->MontaCabecalho($pagina);
    if(isset($_GET['improdutivo'])) {
      $moni->Improdutivo();
    }
    else {
        $moni->edita = "S";
        $moni->MontaPlan_corpomoni();
        ?>
        <!--<script type="text/javascript">
            $(window).load("/monitoria_supervisao/delaudiotmp.php?caminho=<?php echo $moni->caminho;?>");
        </script>-->
        <?php
        $moni->MontaPlan_encerra($pagina);
    }
    echo "</div>";
}
else {
}
?>
