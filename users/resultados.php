<?php

$iduser = $_SESSION['usuarioID'];
$tabuser = $_SESSION['user_tabela'];

$sfiltros = "SELECT nomefiltro_nomes,obrigatorio FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
$efiltros = $_SESSION['query']($sfiltros) or die ("erro na consulta dos filtro ativos");
while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
    if($lfiltros['obrigatorio'] == "S") {
        $nsfiltros[] = strtolower($lfiltros['nomefiltro_nomes']);
    }
    $filtros[] = strtolower($lfiltros['nomefiltro_nomes']);
}
asort($nsfiltros);

unset($_SESSION['varsconsult']);
scripts_filtros();
?>
<script type="text/javascript">
    function carregando () {
    
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("input[id*='rel']").click(function() {
            var rel = $(this).attr('id');
            var idrel = rel.substr(3);
            var nome = $(this).val();
            var tmonitoria = '';
            <?php
            $vars = array('idmonitoria' => 'ID','idplanilha' => 'plan', 'idaval_plan' => 'aval','idgrupo' => 'grupo','idsubgrupo' => 'idsubgrupo',
            'idpergunta' => 'idpergunta','idresposta' => 'resp','operador' => 'oper','super_oper' => 'super_oper','nomemonitor' => 'nomemonitor','dtinimoni' => 'dtinimoni',
            'dtfimmoni' => 'dtfimmoni','dtinictt' => 'dtinictt','dtfimctt' => 'dtfimctt','qtdefg' => 'fg','qtdefgm' => 'fgm','idindice' => 'indice','idintervalo_notas' => 'intervalo','idfluxo' => 'fluxo','atustatus' => 'atustatus');
            foreach($vars as $v) {
                if($va == "fg" OR $va == "fgm") {
                    echo "var $v = $('#$v:cheked').val();\n";
                }
                if($va == "tmonitoria") {
                    echo "$('.t:checked').each(function() {\n";
                    echo "if($va == '') {\n";
                    echo $va ." = this.value;\n";
                    echo "}\n";
                    echo "else {\n";
                    echo $va ." = $va + ',' + this.value;\n";
                    echo "}\n";
                    echo "})\n";
                }
                else {
                    echo "var $v = $('#$v').val();\n";               
                }
            }
            foreach($nsfiltros as $filtrosob) {
                $objs[] =  "$filtrosob == ''";
                ?>
                var <?php echo $filtrosob;?> = $('#filtro_<?php echo $filtrosob;?>').val();
                <?php
            }
            if(count($nsfiltros) >= 1) {
                ?>
                if(ID == "" &&  (<?php echo implode(" || ",$objs);?>)) {
                    alert("Os filtros <?php echo strtoupper(implode(",",$nsfiltros));?> são obrigatórios");
                    return false;
                }
                <?php
            }
            ?>
            if((dtinictt == "" || dtfimctt == "") && ID == "") {
                alert('O Campo DATA MONITORIA é de preenchimento obrigatório!!!');
                return false;
            }
            else {
                
                <?php
                //echo "window.open('/monitoria_supervisao/users/visurel.php','resultado');\n";//,{idrel:idrel,"; 
                //echo "this.target = 'resultado';\n";
                //$cvar = count($vars); 
                //$i = 0;
                //foreach($vars as $kvar => $var) {
                    //$i++;
                    //echo $var.":".$var; 
                    //if($i != $cvar) { 
                        //echo ",";
                    //} 
                    //else {
                    //}
                //}
                //echo "});\n\r";
                ?>
            }
        })
        <?php
        $ncampos = array('dataini' => 'DATA INI CONTATO','datafim' => 'DATA FIM CONTATO', 'oper' => 'OPERADOR','super' => 'SUPERVISOR','monitor' => 'MONITOR', 'filtros' => 'FILTROS REL','aval' => 'AVALIAÇÃO', 'plan' => 'PLANILHA','grupo' => 'GRUPO', 'sub' => 'SUBGRUPO', 'perg' => 'PERGUNTA', 'resp' => 'RESPOSTA', 'fg' => 'FAHA GRAVE', 'fgm' => 'FALHA GRAVISSIMA');
        $selreljava = "SELECT * FROM relresult";
        $eselreljava = $_SESSION['query']($selreljava) or die ("erro na query de consulta dos relatórios cadastrados");
        while($lrel = $_SESSION['fetch_array']($eselreljava)) {
            $campos = explode(",",$lrel['filtros']);
            if($campos[0] == "") {
            }
            else {
                foreach($campos as $c) {
                    echo "$('#rel".$lrel['idrelresult']."').click(function() {\n";
                    echo "var ".$c." = $('#".$c."').val();\n\n";
                    echo "if(".$c." == '') {\n";
                    echo "alert('O campo ".$ncampos[$c]." é de preenchimento obrigatório para este relatório!!!');\n";
                    echo "return false;\n";
                    echo "}\n";
                    echo "else {\n";
                    echo "}\n";
                    echo "})\n";
                }
            }
        }
        ?>
    })
</script>
<div>
    <form action="users/visurel.php" method="post" id="result" target="blank">
        <?php
        filtros_divs();
        ?>
        <div align="center" style="width:1020px; float:left; border: 2px solid #333">
        <?php
        $i = 0;
        $verifcadrel = "SELECT COUNT(*) as result FROM relresult WHERE ativo='S'";
        $ecadrel = $_SESSION['fetch_array']($_SESSION['query']($verifcadrel)) or die ("erro na queyr de consulta dos relatórios cadastrados");
        if($ecadrel['result'] == 0) {
            echo "<table width:1024>";
                echo "<tr>";
                    echo "<td align=\"center\" style=\"color:#FF0000\"><strong>NÃO EXISTEM RELATÓRIOS CADASTRADOS, FAVOR CONTATAR O ADMINISTRADOR</strong></td>";
                echo "</tr>";
            echo "</table>";
        }
        else {
            $listrel = array();
            $selrel = "SELECT * FROM relresult WHERE ativo='S'";
            $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relatórios cadastrados");
            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                $v = 1;
                $verif = "(SELECT COUNT(*) as result FROM relresultcab WHERE idrelresult='".$lselrel['idrelresult']."') UNION (SELECT COUNT(*) as result FROM relresultcampos WHERE idrelresult='".$lselrel['idrelresult']."') UNION (SELECT COUNT(*) as result FROM relresultcorpo WHERE idrelresult='".$lselrel['idrelresult']."')";
                $everif = $_SESSION['query']($verif) or die ("erro na query de consulta da estrutura do relatório");
                while($lverif = $_SESSION['fetch_array']($everif)) {
                    if($lverif['result'] == 0) {
                        $v = 0;
                    }
                    else {
                    }
                }
                if($v == 0) {
                }
                else {
                    $perfis = array();
                    if($_SESSION['user_tabela'] == "user_web") {
                        $perfis = explode(",", $lselrel['idsperfweb']);
                        if(in_array($_SESSION['usuarioperfil'], $perfis)) {
                          $listrel[] = $lselrel['idrelresult'];
                          $nomerel[] = $lselrel['nomerelresult'];
                        }
                    }
                    if($_SESSION['user_tabela'] == "user_adm") {
                        $perfis = explode(",", $lselrel['idsperfadm']);
                        if(in_array($_SESSION['usuarioperfil'], $perfis)) {
                          $listrel[] = $lselrel['idrelresult'];
                          $nomerel[] = $lselrel['nomerelresult'];
                        }
                    }
                }
            }
            $colunas = 1020 / count($listrel);
            $r = 0;
            echo "<table width=\"1020\" bgcolor=\"#FFFFFF\">";
            if($listrel[0] == "") {
                echo "<tr>";
                    echo "<td align=\"center\" style=\"color:#FF0000\"><strong>EXISTEM RELATÓRIOS CADASTRADOS, PORÉM NENHUM COM A ESTRUTURA COMPLETA</strong></td>";
                echo "</tr>";
            }
            if($listrel[0] != "" && count($listrel) < 4) {
              $qtdecol = ceil(count($listrel) / 1);
              if($qtdecol < 4) {
                  $qtdecol = 4;
              }
              else {
              }
              $listrel = array_chunk($listrel, $qtdecol);
                      for($i = 0; $i < 1; $i++) {
                      echo "<tr>";
                        foreach($listrel[$i] as $idrel) {
                                $sqlrel = "SELECT * FROM relresult WHERE idrelresult='$idrel'";
                                $esqlrel =  $_SESSION['fetch_array']($_SESSION['query']($sqlrel)) or die ("erro na query de consulta do relatório");
                                echo "<td align=\"center\" width=\"".$colunas."\"><input type=\"submit\" style=\"border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:220px;\" name=\"rel\" id=\"rel".$idrel."\" tittle=\"".$esqlrel['nomerelresult']."\" value=\"".$esqlrel['nomerelresult']."\"></td>";
                        }
                      echo "</tr>";
                      }
            }
            if(count($listrel) >= 4 AND count($listrel) < 11) {
              $qtdecol = ceil(count($listrel) / 2);
              if($qtdecol < 4) {
                  $qtdecol = 4;
              }
              else {
              }
              $listrel = array_chunk($listrel, $qtdecol);
                      for($i = 0; $i < 2; $i++) {
                      echo "<tr>";
                        foreach($listrel[$i] as $idrel) {
                            $sqlrel = "SELECT * FROM relresult WHERE idrelresult='$idrel'";
                            $esqlrel =  $_SESSION['fetch_array']($_SESSION['query']($sqlrel)) or die ("erro na query de consulta do relatório");
                            echo "<td align=\"center\" width=\"".$colunas."\"><input type=\"submit\" style=\"border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:220px;\" name=\"rel\" id=\"rel".$idrel."\" tittle=\"".$esqlrel['nomerelresult']."\" value=\"".$esqlrel['nomerelresult']."\"></td>";
                        }
                      echo "</tr>";
                      }
            }
            if(count($listrel) >= 10 AND count($listrel) < 15) {
              $qtdecol = ceil(count($listrel) / 3);
              if($qtdecol < 4) {
                  $qtdecol = 4;
              }
              else {
              }
              $listrel = array_chunk($listrel, $qtdecol);
                      for($i = 0; $i < 3; $i++) {
                      echo "<tr>";
                        foreach($listrel[$i] as $idrel) {
                                $sqlrel = "SELECT * FROM relresult WHERE idrelresult='$idrel'";
                                $esqlrel =  $_SESSION['fetch_array']($_SESSION['query']($sqlrel)) or die ("erro na query de consulta do relatório");
                                echo "<td align=\"center\" width=\"".$colunas."\"><input type=\"submit\" style=\"border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:220px;\" id=\"rel".$idrel."\" name=\"rel\" tittle=\"".$esqlrel['nomerelresult']."\" value=\"".$esqlrel['nomerelresult']."\"></td>";
                        }
                      echo "</tr>";
                      }
            }
            if(count($listrel) >= 15 AND count($listrel) < 20) {
              $qtdecol = ceil(count($listrel) / 4);
              if($qtdecol < 4) {
                  $qtdecol = 4;
              }
              else {
              }
              $listrel = array_chunk($listrel, $qtdecol);
                      for($i = 0; $i < 4; $i++) {
                      echo "<tr>";
                        foreach($listrel[$i] as $idrel) {
                                $sqlrel = "SELECT * FROM relresult WHERE idrelresult='$idrel'";
                                $esqlrel =  $_SESSION['fetch_array']($_SESSION['query']($sqlrel)) or die ("erro na query de consulta do relatório");
                                echo "<td align=\"center\" width=\"".$colunas."\"><input type=\"submit\" style=\"border: 1px solid #FFF; background-image:url(/monitoria_supervisao/images/button.jpg); width:220px;\" name=\"rel\" id=\"rel".$idrel."\" tittle=\"".$esqlrel['nomerelresult']."\" value=\"".$esqlrel['nomerelresult']."\"></td>";
                        }
                      echo "</tr>";
                      }
            }
            echo "</table>";
        }
    ?>
    </div>
    </form>
</div>
