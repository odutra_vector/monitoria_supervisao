<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
$nome = $_POST['rel'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $nome;?></title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/highcharts/highcharts.js"></script>
<script src="/monitoria_supervisao/js/highcharts/adapters/mootools-adapter.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script type="text/javascript">
    $(document).ready(function carregando () {
        <?php
        $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
        $eselfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
        $vars = array('idmonitoria' => 'ID','idplanilha' => 'plan', 'idaval_plan' => 'aval','idgrupo' => 'grupo','idsubgrupo' => 'sub',
        'idpergunta' => 'perg','idresposta' => 'resp','operador' => 'oper','super_oper' => 'super_oper','nomemonitor' => 'nomemonitor','dtinimoni' => 'dtinimoni',
        'dtfimmoni' => 'dtfimmoni','dtinictt' => 'dtinictt','dtfimctt' => 'dtfimctt','qtdefg' => 'fg','qtdefgm' => 'fgm','idindice' => 'indice','idintervalo_notas' => 'intervalo','idfluxo' => 'fluxo','atustatus' => 'atustatus');
        foreach($vars as $va) {
            if($_POST[$va] == "") {
            }
            else {
                $varload[] = $va;
                echo "var $va='$_POST[$va]';\n";
            }
        }
        while($lfiltros = $_SESSION['fetch_array']($eselfiltros)) {
            if($_POST['filtro_'.strtolower($lfiltros['nomefiltro_nomes'])] == "") {
            }
            else {
                echo "var ".strtolower($lfiltros['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lfiltros['nomefiltro_nomes'])]."';\n";
            }
        }
        ?>
        $.blockUI({ message: '<strong>AGUARDE CARREGANDO DADOS...</strong>', css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5,
            color: '#fff'
            }
        })
        $('#resultado').load('/monitoria_supervisao/users/dadosrelresult.php',{rel:'<?php echo $nome;?>', <?php $nfiltros = $_SESSION['num_rows']($eselfiltros); 
        $sfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S'";
        $esfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
        while($lselfiltros = $_SESSION['fetch_array']($esfiltros)) {
            if($_POST['filtro_'.strtolower($lselfiltros['nomefiltro_nomes'])] == "") {
            }
            else {
                echo "filtro_".strtolower($lselfiltros['nomefiltro_nomes']).": ".strtolower($lselfiltros['nomefiltro_nomes']).",";
            }
        } 
        $i = 0; 
        foreach($varload as $v) {
            if($_POST[$v] == "") {
            }
            else {
                $i++; 
                echo "$v: $v"; 
                if($i != count($varload)) { 
                    echo ",";    
                } 
                else {
                }  
            }
        }
        echo "});\n";
        ?>
        //$.unblockUI();
    })

</script>
</head>
    <body onload="carregando()">
        <div  id="resultado" style="width: 1024px">
        </div>
    </body>
</html>
