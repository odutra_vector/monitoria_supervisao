<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="/monitoria_supervisao/style.css" rel="stylesheet" type="text/css" />
<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#gerar").click(function() {
                    var dataini = $("#dtinimoni").val();
                    var datafim = $("#dtinifim").val();
                    var planilha = $("#plan").val();
                    if(dataini == "" || datafim == "" || planilha == "") {
                        alert("É necessário informar a data de inicio e fim das Calibragens e a Planilha");
                        return false;
                    }
                })
            })
        </script>
        <form action="users/dadosrelcalib.php" method="post" target="_blank">
            <?php
            scripts_filtros();
            filtros_divs_calib($opwhere);
            unset($_SESSION['varsconsult']);
            ?>
            <div style="float:left; width:1010px; padding:5px; margin:auto; text-align:center; border:2px solid #999">
                <?php
                if($_GET['opmenu'] == "RELATORIO") {
                    ?>
                    <li style="text-decoration:none; list-style:none"><input type="submit" class="botaorel" name="gerar" id="gerar" value="GERAR" style="text-align:center" /></li>
                    <?php
                }
                else {
                    ?>
                    <li style="text-decoration:none; list-style:none"><input type="submit" class="botaorel" name="gerar" id="gerar" value="GERAR_ITEM" style="text-align:center" /></li>
                    <?php
                }
                ?>   
            </div>
        </form>
    </div>
</body>
</html>
