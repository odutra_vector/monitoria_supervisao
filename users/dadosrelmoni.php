<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once($rais.'/monitoria_supervisao/classes/class.relatorios.php');

if(isset($_POST['rel']) OR isset($_POST['variaveis'])) {
    if(isset($_POST['rel'])) {
        $_SESSION['wheresql'] = "";
        $_SESSION['opcoes'] = "";
    }
    $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
    include_once ($rais.'/monitoria_supervisao/classes/class.tabelas.php');
    include_once ($rais.'/monitoria_supervisao/classes/class.relatorios.php');
    $variaveis = array();
    $self = "SELECT nomefiltro_nomes FROM filtro_nomes";
    $eself = $_SESSION['query']($self) or die ("erro na query de consulta dos nomes dos filtros");
    while($lself = $_SESSION['fetch_array']($eself)) {
        if($_POST["filtro_".strtolower($lself['nomefiltro_nomes'])] != "undefined" && $_POST["filtro_".strtolower($lself['nomefiltro_nomes'])] != "") {
            $variaveis["filtro_".strtolower($lself['nomefiltro_nomes'])] = $_POST["filtro_".strtolower($lself['nomefiltro_nomes'])];
            $_SESSION['varsconsult']["filtro_".strtolower($lself['nomefiltro_nomes'])] = $_POST["filtro_".strtolower($lself['nomefiltro_nomes'])];
        }
    }
    $listvar = array('ID', 'dtinimoni','dtfimmoni','dtinictt','dtfimctt','fg','fgm','tmonitoria','super_oper','oper','nomemonitor','plan','aval','grupo','sub','perg','resp','atustatus','intervalo','indice','fluxo','valor_fg','idalerta','search');
    foreach($listvar as $var) {
        if($_POST[$var] == "") {
        }
        else {
            if($var == "dtinimoni" OR $var == "dtfimmoni" OR $var == "dtinictt" OR $var == "dtfimctt") {
                $variaveis[$var] = data2banco($_POST[$var]);
                $_SESSION['varsconsult'][$var] = data2banco($_POST[$var]);
            }
            if($var == "fg" OR $var == "fgm") {
                if($_POST[$var] == "") {
                }
                else {
                    $variaveis[$var] = "1";
                    $_SESSION['varsconsult'][$var] = $_POST[$var];
                }
            }
            if(!eregi("filtro_",$var) && $var != "dtinimoni" && $var != "dtfimmoni" && $var != "dtinictt" && $var != "dtfimctt" && $var != "fg" && $var != "fgm") {  
                if($var == "idalerta") {
                    $variaveis['alertas'] = $_POST[$var];
                }
                else {
                    $variaveis[$var] = $_POST[$var];
                    $_SESSION['varsconsult'][$var] = $_POST[$var];
                }
            }
        }
    }
    $result = new RelMonitoria();
    $result->r = 0;
    $result->opcoes = $variaveis;
    if(isset($_POST['rel'])) {
        $_SESSION['opcoes'] = $variaveis;
    }
    if(!isset($_POST['rel']) AND isset($_POST['variaveis'])) {
        $vars = $_POST['variaveis'];
        $part = explode("&",$vars);
        if(eregi("rel=",$part[1])) {
            $partrel = explode("=",$part[1]);
            $partpag = explode("=",$part[2]);
            $partlimit = explode("=",$part[3]);
            $rel = $partrel[1];
            $pag = $partpag[1];
            $limit = $partlimit[1];
        }
        else {
            $partadd = explode("=",$part[1]);
            $partvaladd = explode("=",$part[2]);
            $parttabadd = explode("=",$part[3]);
            $partproxrel = explode("=",$part[4]);
            $partgroup = explode("=",$part[5]);
            $addwhere = $partadd[1];
            $valadd = $partvaladd[1];
            $tabadd = $parttabadd[1];
            $proxrel = $partproxrel[1];
            $groupby = $partgroup[1];
        }
        $result->TabsInner($proxrel, $addwhere, $valadd, $tabadd, $groupby,$rel,$limit);
    }
    else {
        $result->TabsInner();
    }
    $sql = $result->sql;
    //echo $sql;
    $result->MontaTabela();
}
?>
<script type="text/javascript">
    $.unblockUI();
</script>
