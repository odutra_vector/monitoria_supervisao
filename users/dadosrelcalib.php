<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once($rais.'/monitoria_supervisao/classes/class.relatorios.'.strtolower($_SESSION['nomecli']).'.php');

protegePagina();

// levantando variáveis e valores
$selfilt = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
$eselfilt = $_SESSION['query']($selfilt) or die ("erro na query de consulta dos filtros");
while($lselfilt = $_SESSION['fetch_array']($eselfilt)) {
    if($_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] == "") {
        $niveis[strtolower($lselfilt['nomefiltro_nomes'])] =$lselfilt['idfiltro_nomes']; 
    }
    else {
        $niveis[strtolower($lselfilt['nomefiltro_nomes'])] =$lselfilt['idfiltro_nomes'];
        $nfiltros[strtolower($lselfilt['nomefiltro_nomes'])] = "rf.id_".strtolower($lselfilt['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])]."'";
        //$rel->vars['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] = $_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])];
    }
}
if($nfiltros == "") {
    $tabuser = str_replace("_", "", $_SESSION['user_tabela']);
    $selperf = "SELECT idrel_filtros FROM ".$tabuser."filtro WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
    $eselperf = $_SESSION['query']($selperf) or die ("erro na query de consulta dos filtros autorizados para o usuário");
    while($lselperf = $_SESSION['fetch_array']($eselperf)) {
        $idsrel[] = $lselperf['idrel_filtros'];
    }
    $vars['idrel_filtros'] = implode(",",$idsrel);
}
else {
    $filtro = implode(" AND ",$nfiltros);
    $tabfiltro = array('user_adm' => 'useradmfiltro','user_web' => 'userwebfiltro');
    $alias = array('user_adm' => 'uaf','user_web' => 'uwf');
    $selrel = "SELECT rf.idrel_filtros FROM rel_filtros rf INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros 
               INNER JOIN ".$tabfiltro[$_SESSION['user_tabela']]." ".$alias[$_SESSION['user_tabela']]." ON ".$alias[$_SESSION['user_tabela']].".idrel_filtros = rf.idrel_filtros
               WHERE $filtro AND ".//cr.ativo='S' AND 
               "id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relacionamentos");
    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
        $idsrel[] = $lselrel['idrel_filtros'];
    }
    $vars['idrel_filtros'] = implode(",",$idsrel);
}

$tab = "monitoriacalib";
$colunas = new TabelasSql();
$colunas->Tabvar($tab);

foreach($colunas->colvar as $chavecol => $col) {
    if($col == "data" OR $col == "datactt") {
        if(array_key_exists($col, $vars)) {
        }
        else {
            if($col == "data") {
                if($_POST['dtinimoni'] == "" || $_POST['dtfimmoni'] == "") {
                }
                else {
                    $vars[$col] = data2banco($_POST['dtinimoni']).",".data2banco($_POST['dtfimmoni']);
                    $where[$col] = $colunas->AliasTab($colunas->tabvar[$chavecol]).".dataini BETWEEN '".data2banco($_POST['dtinimoni'])."' AND '".data2banco($_POST['dtfimmoni'])."'";
                }
            }
            if($col == "datactt") {
                if($_POST['dtinictt'] == "" || $_POST['dtinictt'] == "") {
                }
                else {
                    $vars[$col] =  data2banco($_POST['dtinictt']).",".data2banco($_POST['dtfimctt']);
                    $where[$col] = $colunas->AliasTab($colunas->tabvar[$chavecol]).".datactt BETWEEN '".data2banco($_POST['dtinictt'])."' AND '".data2banco($_POST['dtfimctt'])."'";
                }
            } 
        }
    }
    else {
        if($_POST[$chavecol] == "") {
        }
        else {
            $vars[$col] = $_POST[$chavecol];
            $where[$col] = $colunas->AliasTab($colunas->tabvar[$chavecol]).".$col='".$_POST[$chavecol]."'";
        }
    }
}

foreach($niveis as $n => $cod) {
    $inners[] = "INNER JOIN filtro_dados ".$colunas->AliasTab(strtolower($n))." ON ".$colunas->AliasTab(strtolower($n)).".idfiltro_dados = ".$colunas->AliasTab(rel_filtros).".id_".strtolower($n)."";
    $orderby[] = $colunas->AliasTab(strtolower($n)).".nomefiltro_dados DESC";
    $colrel[] = $colunas->AliasTab(rel_filtros).".id_".strtolower($n)."";
}

$selcalib = "SELECT usercont, ".$colunas->AliasTab(agcalibragem).".idmonicalib, idagcalibragem,".implode(",",$colrel).", ".$colunas->AliasTab(agcalibragem).".idrel_filtros FROM agcalibragem ".$colunas->AliasTab(agcalibragem)."
                    INNER JOIN monitoria ".$colunas->AliasTab(monitoria)." ON ".$colunas->AliasTab(monitoria).".idmonitoria = ".$colunas->AliasTab(agcalibragem).".idmonitoria
                    INNER JOIN rel_filtros ".$colunas->AliasTab(rel_filtros)." ON ".$colunas->AliasTab(rel_filtros).".idrel_filtros = ".$colunas->AliasTab(agcalibragem).".idrel_filtros
                    ".implode(" ",$inners)."
                    WHERE ".$colunas->AliasTab(monitoria).".idrel_filtros IN (".$vars['idrel_filtros'].") AND idmonicalib IS NOT NULL 
                    AND participantes<>usercont AND ".implode(" AND ",$where)." ORDER BY ".implode(",",$orderby);
$eselcalib = $_SESSION['query']($selcalib) or die (mysql_error());
while($lselcalib = $_SESSION['fetch_array']($eselcalib)) {
    $selesp = "SELECT idmonicalib,COUNT(*) as result FROM agcalibragem WHERE participantes='".$lselcalib['usercont']."' AND idagcalibragem='".$lselcalib['idagcalibragem']."' AND idmonicalib IS NOT NULL";
    $eselesp = $_SESSION['fetch_array']($_SESSION['query']($selesp)) or die ("erro na query de consulta do usuário controle");
    if($eselesp['result'] >= 1) {
        $nivel[$lselcalib['id_divisao']][$lselcalib['id_operacao']][$lselcalib['idrel_filtros']] = $lselcalib['idrel_filtros'];
        $idscalib[$lselcalib['idagcalibragem']][] = $lselcalib['idmonicalib'];
        $usercont[$lselcalib['idagcalibragem']][$lselcalib['usercont']] =$eselesp['idmonicalib']; 
    }
}

foreach($idscalib as $idcalib => $idmonicalib) {
    $tterros = 0;
    $ttpossiveis = 0;
    $selrel = "SELECT idrel_filtros FROM agcalibragem WHERE idagcalibragem='$idcalib'";
    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na consulta do relacionamento da calibragem");
    foreach($usercont[$idcalib] as $ucont => $monicont) {
            $selplan = "SELECT ma.idgrupo FROM moniavaliacalib ma
                         INNER JOIN planilha p ON p.idgrupo = ma.idgrupo
                         WHERE idmonitoriacalib='$monicont' GROUP BY p.idgrupo ORDER BY p.posicao";
            $idmonicont = $monicont;
    }
    $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta da planilha");
    while($lselplan = $_SESSION['fetch_array']($eselplan)) {
        $selgrupo = "SELECT idrel,filtro_vinc FROM moniavaliacalib ma
                    INNER JOIN grupo g ON g.idgrupo = ma.idgrupo AND g.idrel = ma.idpergunta
                    INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta 
                    WHERE ma.idgrupo='".$lselplan['idgrupo']."' AND idmonitoriacalib='$monicont' GROUP BY idrel ORDER BY g.posicao";
        $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query para selecionar o grupo");
        while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
            $idrespcont = array();
            if($lselgrupo['filtro_vinc'] == "S") {
                $selsub = "SELECT * FROM subgrupo WHERE idsubgrupo='".$lselgrupo['idrel']."' GROUP BY idpergunta ORDER BY posicao";
                $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta do subgrupo");
                while($lselsub = $_SESSION['fetch_array']($eselsub)) {
                    $selperg = "SELECT * FROM pergunta WHERE idpergunta='".$lselsub['idpergunta']."'";
                    $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
                    while($lselperg = $_SESSION['fetch_array']($eselperg)) {

                    }
                }
            }
            else {
                if($_POST['gerar'] == "GERAR_ITEM") {
                    $selperg = "SELECT * FROM pergunta WHERE idpergunta='".$lselgrupo['idrel']."' and avalia<>'AVALIACAO_OK' and avalia<>'TABULACAO' ORDER BY posicao";
                    $eselperg = $_SESSION['query']($selperg) or die (mysql_error());
                    while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                        $idsrespostas[$lselperg['idresposta']] = $lselperg['descriresposta'];
                        $selcontrole = "SELECT count(*) as r FROM moniavaliacalib ma
                                        WHERE idmonitoriacalib='$idmonicont' and ma.idpergunta='".$lselgrupo['idrel']."' and ma.idresposta='".$lselperg['idresposta']."'";
                        $econtrole = $_SESSION['fetch_array']($_SESSION['query']($selcontrole)) or die (mysql_error());
                        //$marcacao["item"][$lselperg['idresposta']]["controle"] = $marcacao["item"][$lselperg['idresposta']]["controle"] + $econtrole['r'];
                        foreach($idmonicalib as $idc) {
                            $seltipo = "SELECT * FROM monitoriacalib WHERE idmonitoriacalib='$idc'";
                            $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
                            $tipou = explode("-",$eseltipo['iduser']);
                            if($tipou[1] == "monitor") {
                                 
                            }
                            else {
                                   $selmoni = "SELECT m.idrel_filtros,uw.idperfil_web,iduser,nomeperfil_web,count(*) as r FROM moniavaliacalib mac
                                               INNER JOIN monitoriacalib m ON m.idmonitoriacalib = mac.idmonitoriacalib
                                               INNER JOIN user_web uw ON uw.iduser_web = m.iduser
                                               INNER JOIN perfil_web pw ON pw.idperfil_web = uw.idperfil_web
                                               WHERE mac.idmonitoriacalib='$idc' AND idpergunta='".$lselgrupo['idrel']."' and idresposta='".$lselperg['idresposta']."'";
                                   $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_error());
                                   if(strstr($eselmoni['nomeperfil_web'],"ITAU") OR strstr($eselmoni['nomeperfil_web'],"itau")) {
                                        $perfis[$eselmoni['idperfil_web']] = $eselmoni['nomeperfil_web'];
                                        if($econtrole['r'] != $eselmoni['r']) {
                                             if($eselmoni['r'] == 0 && $econtrole['r'] == 1) {
                                                  //$marcacao["item"][$lselperg['idresposta']]["controle"] = $marcacao["item"][$lselperg['idresposta']]["controle"] + $econtrole['r'];
                                                  $marcacao["perfil"][$lselperg['idresposta']]["controle"] = $marcacao["perfil"][$lselperg['idresposta']]["controle"] + $econtrole['r'];
                                             }
                                             else {
                                                  $marcacao["item"][$lselperg['idresposta']]["outros-itau"] = $marcacao["item"][$lselperg['idresposta']]["outros-itau"] + $eselmoni['r'];
                                                  $marcacao["perfil"][$lselperg['idresposta']]["itau"] = $marcacao["perfil"][$lselperg['idresposta']]["itau"] + $eselmoni['r'];
                                             }
                                            $diverge["item"][$lselperg['idresposta']]["outros-itau"] = $diverge["item"][$lselperg['idresposta']]["outros-itau"] + 1;
                                            $diverge["perfil"][$lselperg['idresposta']]["itau"] = $diverge["perfil"][$lselperg['idresposta']]["itau"] + 1;
                                        }
                                   }
                                   if(strstr($eselmoni['nomeperfil_web'],"EPS") OR strstr($eselmoni['nomeperfil_web'],"eps")) {
                                        if(!in_array($idc,$quanticalib)) {
                                             $quanticalib[] = $idc;
                                        }
                                        if(!in_array($idc,$relfiltros[$eselmoni['idrel_filtros']])) {
                                             $relfiltros[$eselmoni['idrel_filtros']][] = $idc;
                                        }
                                        $selfiltro = "SELECT MIN(nivel),nomefiltro_nomes FROM filtro_nomes";
                                        $eselfiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die (mysql_error());
                                        $filtroag = "id_".strtolower($eselfiltro['nomefiltro_nomes']);
                                        $selrel = "SELECT nomefiltro_dados, $filtroag, count(DISTINCT($filtroag)) as result FROM ".str_replace("_", "", $tipou[1])."filtro uf
                                                 INNER JOIN rel_filtros r ON r.idrel_filtros = uf.idrel_filtros
                                                 INNER JOIN filtro_dados fd ON fd.idfiltro_dados = r.$filtroag
                                                 WHERE id$tipou[1]='$tipou[0]'";
                                        $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                                        $snome = "SELECT id$tipou[1], nome$tipou[1], idperfil_".substr($tipou[1], 5)." FROM $tipou[1] WHERE id$tipou[1]='$tipou[0]'";
                                        $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
                                        if($eselrel['result'] == 1) { // verifica se o usuário só está vinculado a uma EPS, se sim, coloca na variável @perfis o nome da EPS
                                             if(!in_array($idc,$calibperfis[$eselrel['nomefiltro_dados']])) {
                                                  $calibperfis[$eselrel['nomefiltro_dados']][] = $idc;
                                             }
                                             $perfis[$eselrel['nomefiltro_dados']] = $eselrel['nomefiltro_dados'];
                                        }
                                        else {
                                             $perfil = "SELECT idperfil_web,nomeperfil_web FROM perfil_web WHERE idperfil_web='".$esnome['idperfil_'.substr($tipou[1], 5)]."'";
                                             $eperfil = $_SESSION['fetch_array']($_SESSION['query']($perfil)) or die (mysql_error());
                                             $perfis[$eperfil['idperfil_web']] = $eperfil['nomeperfil_web'];
                                             if(!in_array($idc,$calibperfis[$eperfil['idperfil_web']])) {
                                                  $calibperfis[$eperfil['idperfil_web']][] = $idc;
                                             }
                                        }
                                        if($econtrole['r'] != $eselmoni['r']) { // identfica se existe diferença entre as respostas dos usuários das EPS que estão calibrados contra o controle
                                             if($eselmoni['r'] == 0 && $econtrole['r'] == 1) {
                                                  if($eselrel['result'] == 1) {
                                                       $marcacao["perfil"][$lselperg['idresposta']]["controle".$eselrel['nomefiltro_dados']] = $marcacao["perfil"][$lselperg['idresposta']]["controle".$eselrel['nomefiltro_dados']] + $econtrole['r'];
                                                  }
                                                  else {
                                                       $marcacao["perfil"][$lselperg['idresposta']]["controle".$eperfil['idperfil_web']] = $marcacao["perfil"][$lselperg['idresposta']]["controle".$eperfil['idperfil_web']] + $econtrole['r'];
                                                  }
                                                  $marcacao["item"][$lselperg['idresposta']]["controle"] = $marcacao["item"][$lselperg['idresposta']]["controle"] + $econtrole['r'];
                                                  $marcacao["operacao"][$lselperg['idresposta']]["controle".$eselmoni['idrel_filtros']] = $marcacao["operacao"][$lselperg['idresposta']]["controle".$eselmoni['idrel_filtros']] + $econtrole['r'];
                                             }
                                             else {
                                                  if($eselrel['result'] == 1) {
                                                      $marcacao["perfil"][$lselperg['idresposta']][$eselrel['nomefiltro_dados']] = $marcacao["perfil"][$lselperg['idresposta']][$eselrel['nomefiltro_dados']] + $eselmoni['r'];
                                                  }
                                                  else {
                                                      $marcacao["perfil"][$lselperg['idresposta']][$eperfil['idperfil_web']] = $marcacao["perfil"][$lselperg['idresposta']][$eperfil['idperfil_web']] + $eselmoni['r'];
                                                  }
                                                  $marcacao["operacao"][$lselperg['idresposta']]["eps".$eselmoni['idrel_filtros']] = $marcacao["operacao"][$lselperg['idresposta']]["eps".$eselmoni['idrel_filtros']] + $eselmoni['r'];
                                                  $marcacao["item"][$lselperg['idresposta']]["outros-eps"] = $marcacao["item"][$lselperg['idresposta']]["outros-eps"] + $eselmoni['r'];
                                             }
                                            $diverge["operacao"][$lselperg['idresposta']][$eselmoni['idrel_filtros']] = $diverge["operacao"][$lselperg['idresposta']][$eselmoni['idrel_filtros']] + 1;
                                            $diverge["item"][$lselperg['idresposta']]["outros-eps"] = $diverge["item"][$lselperg['idresposta']]["outros-eps"] + 1;
                                            if($eselrel['result'] == 1) {
                                                $diverge["perfil"][$lselperg['idresposta']][$eselrel['nomefiltro_dados']] = $diverge["perfil"][$lselperg['idresposta']][$eselrel['nomefiltro_dados']] + 1;
                                            }
                                            else {
                                                $diverge["perfil"][$lselperg['idresposta']][$eperfil['idperfil_web']] = $diverge["perfil"][$lselperg['idresposta']][$eperfil['idperfil_web']] + 1;
                                            }
                                        }
                                   }
                            }
                        }
                    }
                }
                else {
                    $selresptt = "SELECT COUNT(idresposta) as result FROM pergunta WHERE idpergunta='".$lselgrupo['idrel']."'";
                    $eselresptt = $_SESSION['fetch_array']($_SESSION['query']($selresptt)) or die ("erro na query de consulta do total de resposta");
                    $selrespcont = "SELECT idresposta FROM moniavaliacalib WHERE idmonitoriacalib='$idmonicont' AND idpergunta='".$lselgrupo['idrel']."'";
                    $eselrespcont = $_SESSION['query']($selrespcont) or die ("erro na consulta da resposta controle");
                    while($lrespcont = $_SESSION['fetch_array']($eselrespcont)) {
                        $idrespcont[] = $lrespcont['idresposta'];
                    }
                    foreach($idmonicalib as $idmoni) {
                        $selac = "SELECT iduser, COUNT(*) as result FROM moniavaliacalib ma
                                  INNER JOIN monitoriacalib m ON m.idmonitoriacalib = ma.idmonitoriacalib 
                                  WHERE idresposta IN (".implode(",",$idrespcont).") AND ma.idmonitoriacalib='$idmoni'";
                        $eselac = $_SESSION['fetch_array']($_SESSION['query']($selac)) or die ("erro na consulta dos acertos do usuario");
                        $erros = count($idrespcont) - $eselac['result'];
                        $dadoscalib[$eselrel['idrel_filtros']][$idcalib][$eselac['iduser']][$lselgrupo['idrel']] = round((($eselresptt['result'] - $erros) / $eselresptt['result'] * 100),2);
                        $dadosplanuser[$eselac['iduser']][$lselgrupo['idrel']][] = round((($eselresptt['result'] - $erros) / $eselresptt['result'] * 100),2);
                        $dadosplan[$lselgrupo['idrel']][] = round((($eselresptt['result'] - $erros) / $eselresptt['result'] * 100),2);
                    }
                    $selacertos = "SELECT COUNT(*) as result FROM moniavaliacalib WHERE idresposta IN (".implode(",",$idrespcont).") AND idmonitoriacalib IN (".implode(",",$idmonicalib).")";
                    $eacertostt = $_SESSION['fetch_array']($_SESSION['query']($selacertos)) or die ("erro na query de consulta dos acertos");
                    $tterros = (count($idmonicalib) * count($idrespcont)) - $eacertostt['result'];
                    $ttpossiveis = count($idmonicalib) * $eselresptt['result'];
                    //$dadoscalib[$eselrel['idrel_filtros']][$idcalib]['geral'][$lselgrupo['idrel']] = round((($ttpossiveis - $tterros) / $ttpossiveis * 100),2);
                    //$dadosplan[$lselgrupo['idrel']][] = round((($ttpossiveis - $tterros) / $ttpossiveis * 100),2);
                }
            }
        }
    }
}

?>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.6.2.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<div style="margin:auto;">
    <table width="1024" align="center">
        <tr>
            <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=calibragemporitem"><div style="width:200px; height: 19px; background-color: #FFCC33; border: 2px solid #999; margin:auto; padding: 2px; font-weight: bold">EXCEL</div></a></td>
            <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome=calibragemporitem"><div style="width:200px; height: 19px; background-color: #FFCC33; border: 2px solid #999; margin:auto; padding: 2px; font-weight: bold">WORD</div></a></td>
            <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome=calibragemporitem"><div style="width:200px; height: 19px; background-color: #FFCC33; border: 2px solid #999; margin:auto; padding: 2px; font-weight: bold">PDF</div></a></td>
        </tr>
</table>
</div><br/><br/>
<?php
if($idscalib == "") {
    ?>
    <table style="margin: auto; font-weight: bold">
        <tr>
            <td style="color: #CC0000">NÃO EXISTEM CALIBRAGENS AGENDADAS COM OS FILTROS INFORMADOS</td>
        </tr>
    </table>
    <?php
}
else {
    if($_POST['gerar'] == "GERAR_ITEM") {
         ?>
         <script type="text/javascript">
          $(document).ready(function() {
               $(".tabperfil").hide();
               $('#optab').change(function() {
                    var perfil = $("#optab").val();
                    $(".tabperfil").hide();
                    $('#'+perfil).show();
               });
         });
         </script>
         <?php
         $dados .= "<div id=\"relacinamento\" style=\"margin: auto\">";
               $dados .= "<table width=\"1024\" align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px\">"; //tabela geral
               $dados .= "<tr bgcolor=\"#EA862F\" style=\"background-color:#EA862F; color: #FFF;font-weight:bold\">";
                    $dados .= "<td width=\"600\"><strong>item</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_itens</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_discordancia</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>desvio</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>% total</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes EPS marcou</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes Controle marcou</strong></td>";
               $dados .= "</tr>";
               foreach($diverge['item'] as $idrespdisc => $tipo) {
                    foreach($tipo as $totaltipo) {
                         $totaldisc = $totaldisc + count($totaltipo);
                    }
               }
               foreach($idsrespostas as $idresp => $dresp) {
                    $totaldiverg = 0;
                    foreach($diverge["item"][$idresp] as $tipouser => $qtde) {
                         if($tipouser != "outros-itau") {
                              $totaldiverg = $totaldiverg + $qtde;
                              $total = $total + $qtde;
                         }
                    }
                    foreach($diverge["operacao"][$idresp] as $oper => $qtde) {
                         $totaloper[$oper] = $totaloper[$oper] + $qtde;
                    }
                    $respordem[$idresp] = $totaldiverg;
               }
               
               arsort($respordem);
               arsort($totaloper);
               foreach($respordem as $id => $qtdiv) {
                    $qtdivtt = $qtdivtt + $qtdiv;
                    $desvioeps = 0;
                    $desvioitau = 0;
                    $dados .= "<tr>";
                         $dados .= "<td>$idsrespostas[$id]</td>";
                         $dados .= "<td align=\"center\">".count($quanticalib)."</td>";
                         $dados .= "<td align=\"center\">$qtdiv</td>";
                         $dados .= "<td align=\"center\">".round(($qtdiv/count($quanticalib)) * 100,1)."</td>";
                         $dados .= "<td align=\"center\">".round(($qtdiv/$total) * 100,1)."</td>";
                         foreach($marcacao["item"][$id] as $tipom => $qt) {
                              if($tipom == "controle") {
                                   $desvioitau = $desvioitau + $qt;
                                   $desvttitau = $desvttitau + $qt;
                              }
                              if($tipom == "outros-eps") {
                                   $desvioeps = $desvioeps + $qt;
                                   $desvtteps = $desvtteps + $qt;
                              }
                         }
                         $dados .= "<td align=\"center\">$desvioeps</td>";
                         $dados .= "<td align=\"center\">$desvioitau</td>";
                    $daods .= "</tr>";
               }
                    $dados .= "<tr style=\"font-weight:bold\">";
                         $dados .= "<td>TOTAL</td>";
                         $dados .= "<td align=\"center\">".count($respordem) * count($quanticalib)."</td>";
                         $dados .= "<td align=\"center\">$qtdivtt</td>";
                         $dados .= "<td align=\"center\">".round(($qtdivtt / (count($respordem) * count($quanticalib))) * 100,1)."</td>";
                         $dados .= "<td align=\"center\">--</td>";
                         $dados .= "<td align=\"center\">$desvtteps</td>";
                         $dados .= "<td align=\"center\">$desvttitau</td>";
                    $dados .= "</tr>";
               $dados .= "</table><br/>";
               
               $dados .= "<table width=\"1024\" align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px\">"; //tabela geral
               $dados .= "<tr bgcolor=\"#EA862F\" style=\"background-color:#EA862F; color: #FFF;font-weight:bold\">";
                    $dados .= "<td width=\"600\"><strong>Relacionamento</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_itens</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_discordancia</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>desvio</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>% total</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes EPS marcou</strong></td>";
                    $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes Controle marcou</strong></td>";
               $dados .= "</tr>";
               foreach($totaloper as $idoper => $qtde) {
                    $ttmarcaeps = 0;
                    $ttmarcaitau = 0;
                    $dados .= "<tr>";
                    $dados .= "<td>".nomeapres($idoper)."</td>";
                    $dados .= "<td align=\"center\">".(count($relfiltros[$idoper]) * count($respordem))."</td>";
                    $dados .= "<td align=\"center\">$qtde</td>";
                    $dados .= "<td align=\"center\">".round($qtde / (count($relfiltros[$idoper]) * count($respordem)) * 100,1)."</td>";
                    $dados .= "<td align=\"center\">".round(($qtde / $qtdivtt) * 100,1)."</td>";
                    foreach($marcacao["operacao"] as $idrespoper => $operresp) {
                         foreach($operresp as $idrel => $qtderel) {
                              if($idrel == "eps".$idoper) {
                                   $ttmarcaeps = $ttmarcaeps + $qtderel;
                              }
                              if($idrel == "controle".$idoper) {
                                   $ttmarcaitau = $ttmarcaitau + $qtderel;
                              }
                         }
                    }
                    $dados .= "<td align=\"center\">$ttmarcaeps</td>";
                    $dados .= "<td align=\"center\">$ttmarcaitau</td>";
                    $dados .= "</tr>";
               }
               $dados .= "</table><br/>";
               
               foreach($perfis as $idperfil => $nperfil) {
                    if(!strstr($nperfil,"ITAU")) {
                         foreach($idsrespostas as $idresp => $dresp) {
                              if($diverge["perfil"][$idresp][$idperfil] > 0) {
                                   $divergeitem[$idperfil][$idresp] = $divergeitem[$idperfil][$idresp] + $diverge["perfil"][$idresp][$idperfil];
                                   $ttdiverge[$idperfil] = $ttdiverge[$idperfil] + $diverge["perfil"][$idresp][$idperfil];
                              }
                              else {
                                   $ttdiverge[$idperfil] = $ttdiverge[$idperfil] + 0;
                                   $divergeitem[$idperfil][$idresp] = $divergeitem[$idperfil][$idresp] + 0;
                              }
                              foreach($marcacao["perfil"][$idresp] as $tipopefil => $qtdep) {
                                   if($tipopefil == $idperfil) {
                                        $ttdesveps[$idperfil] = $ttdesveps[$idperfil] + $qtdep;
                                        $desveps[$idperfil][$idresp] = $desveps[$idperfil][$idresp] + $qtdep;
                                   }
                                   else {
                                        if($tipopefil == "controle".$idperfil) {
                                             $ttdesvitau[$idperfil] = $ttdesvitau[$idperfil] + $qtdep; 
                                             $desvitau[$idperfil][$idresp] = $desvitau[$idperfil][$idresp] + $qtdep;
                                        }
                                   }
                              }
                         }
                         arsort($divergeitem);
                    }
               }
               
               // tabela por EPS ou filtros
               arsort($ttdiverge);
               $dados .= "<table width=\"1024\" align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px\">";
                    $dados .= "<tr bgcolor=\"#EA862F\" style=\"background-color:#EA862F; color: #FFF;font-weight:bold\">";
                         $dados .= "<td width=\"600\"><strong>Perfil/ EPS</strong></td>";
                         $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_itens</strong></td>";
                         $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_discordancia</strong></td>";
                         $dados .= "<td width=\"70\" align=\"center\"><strong>desvio</strong></td>";
                         $dados .= "<td width=\"70\" align=\"center\"><strong>% total</strong></td>";
                         $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes EPS marcou</strong></td>";
                         $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes Controle marcou</strong></td>";
                    $dados .= "</tr>";
               
               foreach($ttdiverge as $iddivergeperfil => $qtdiverge) {
                    $percdiverge[$iddivergeperfil] = round(($qtdiverge / (count($calibperfis[$iddivergeperfil]) * count($respordem))) * 100,1);
               }
               arsort($percdiverge);
               foreach($percdiverge as $idperfileps => $perrc) {
                    if(!strstr($perfis[$idperfileps],"ITAU")) {
                         $dados .= "<tr>";
                              $dados .= "<td>$perfis[$idperfileps]</td>";
                              $dados .= "<td align=\"center\">".count($calibperfis[$idperfileps]) * count($respordem)."</td>";
                              $dados .= "<td align=\"center\">$ttdiverge[$idperfileps]</td>";
                              $dados .= "<td align=\"center\">".round(($ttdiverge[$idperfileps] / (count($calibperfis[$idperfileps]) * count($respordem))) * 100,1)."</td>";
                              $dados .= "<td align=\"center\">".round(($ttdiverge[$idperfileps]/$qtdivtt) * 100,1)."</td>";
                              if($ttdesveps[$idperfileps] == "") {
                                   $dados .= "<td align=\"center\">0</td>";
                              }
                              else {
                                   $dados .= "<td align=\"center\">$ttdesveps[$idperfileps]</td>";
                              }
                              if($ttdesvitau[$idperfileps] == "") {
                                   $dados .= "<td align=\"center\">0</td>";
                              }
                              else {
                                   $dados .= "<td align=\"center\">$ttdesvitau[$idperfileps]</td>";
                              }
                         $dados .= "</tr>";
                    }
               }
               $dados .= "</table>";
               
               
               $dados .= "<div style=\"margin:auto\"><li style=\"text-align: center; list-style: none\"><select name=\"optab\" id=\"optab\"><option value=\"\" disabled=\"disabled\" selected=\"selected\">SELECIONE</option>";
               foreach($perfis as $id => $nome) {
                    if(!strstr($nome,"ITAU")) {
                         $dados .= "<option value=\"$id\">$nome</option>";
                    }
               }
               $dados .= "</select></li><br/></div>";
               
               // tabela individual por EPS ou perfil
               foreach($perfis as $idperfiltab => $nperfiltab) {
                    if(!strstr($nperfiltab,"ITAU")) {
                         $tabtt = 0;
                         $desvepstt = 0;
                         $desvitautt = 0;
                         $dados .= "<table width=\"1024\" align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px\" id=\"$idperfiltab\" class=\"tabperfil\">";
                              $dados .= "<tr bgcolor=\"#EA862F\" style=\"background-color:#EA862F; color: #FFF;font-weight:bold\">";
                                   $dados .= "<td width=\"600\"><strong>item - $nperfiltab</strong></td>";
                                   $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_itens</strong></td>";
                                   $dados .= "<td width=\"70\" align=\"center\"><strong>qtde_discordancia</strong></td>";
                                   $dados .= "<td width=\"70\" align=\"center\"><strong>desvio</strong></td>";
                                   $dados .= "<td width=\"70\" align=\"center\"><strong>% total</strong></td>";
                                   $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes EPS/Perfil marcou</strong></td>";
                                   $dados .= "<td width=\"70\" align=\"center\"><strong>Dos discordantes Controle marcou</strong></td>";
                              $dados .= "</tr>";
                              arsort($divergeitem[$idperfiltab]);
                              foreach($divergeitem[$idperfiltab] as $idresptab => $qttab) {
                                   $tabtt = $tabtt + $qttab;
                                   $dados .= "<tr>";
                                   $dados .= "<td>$idsrespostas[$idresptab]</td>";
                                   $dados .= "<td align=\"center\">".count($calibperfis[$idperfiltab])."</td>";
                                   $dados .= "<td align=\"center\">$qttab</td>";
                                   $dados .= "<td align=\"center\">".round(($qttab/count($quanticalib)) * 100,1)."</td>";
                                   $dados .= "<td align=\"center\">".round(($qttab/$ttdiverge[$idperfiltab]) * 100,1)."</td>";
                                   if($desveps[$idperfiltab][$idresptab] > 0) {
                                        $desvepstt = $desvepstt + $desveps[$idperfiltab][$idresptab];
                                        $dados .= "<td align=\"center\">".$desveps[$idperfiltab][$idresptab]."</td>";
                                   }
                                   else {
                                        $desvepstt = $desvepstt + 0;
                                        $dados .= "<td align=\"center\">0</td>";
                                   }
                                   if($desvitau[$idperfiltab][$idresptab] > 0) {
                                        $desvitautt = $desvitautt + $desvitau[$idperfiltab][$idresptab];
                                        $dados .= "<td align=\"center\">".$desvitau[$idperfiltab][$idresptab]."</td>";
                                   }
                                   else {
                                        $desvitautt = $desvitautt + 0;
                                        $dados .= "<td align=\"center\">0</td>";
                                   }
                                   $dados .= "</tr>";
                              }
                              $dados .= "<tr style=\"font-weight:bold\">";
                              $dados .= "<td>TOTAL</td>";
                              $dados .= "<td align=\"center\">".count($respordem) * count($calibperfis[$idperfiltab])."</td>";
                              $dados .= "<td align=\"center\">$tabtt</td>";
                              $dados .= "<td align=\"center\">".round(($tabtt / (count($respordem) * count($calibperfis[$idperfiltab]))) * 100,1)."</td>";
                              $dados .= "<td align=\"center\">--</td>";
                              $dados .= "<td align=\"center\">$desvepstt</td>";
                              $dados .= "<td align=\"center\">$desvitautt</td>";
                              $dados .= "</tr>";
                         $dados .= "</table>";
                    }
               }
               $dados .= "</div>";
    }
    else {
     $dados .= "<div id=\"relacinamento\" style=\"margin: auto\">";
     $dados .= "<table width=\"1024\" align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px\">";
         $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel DESC LIMIT 4";
         $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
         while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
             $filtros[$lfiltros['idfiltro_nomes']] = $lfiltros['nomefiltro_nomes'];
             $inner[] = "INNER JOIN filtro_dados ".strtolower($colunas->AliasTab($lfiltros['nomefiltro_nomes']))." ON ".strtolower($colunas->AliasTab($lfiltros['nomefiltro_nomes'])).".idfiltro_dados=".$colunas->AliasTab(rel_filtros).".id_".strtolower($lfiltros['nomefiltro_nomes'])."";
             $cols[] = strtolower($colunas->AliasTab($lfiltros['nomefiltro_nomes'])).".nomefiltro_dados as ".$lfiltros['nomefiltro_nomes'];
             $colf++;
         }
          $dados .= "<tr style=\"background-color:#EA862F; color: #FFF; font-weight: bold\">";
             foreach($filtros as $fid => $fnome) {
             $dados .= "<td align=\"center\">$fnome</td>";
             }
             $dados .= "<td align=\"center\">OPERAÇÃO</td>";
             $dados .= "<td align=\"center\">Precisão (%)</td>";
             $dados .= "<td align=\"center\">Precisão - ".$_SESSION['nomecli']." (%)</td>";
             $dados .= "<td align=\"center\">Precisão - Clientis (%)</td>";
             $dados .= "<td align=\"center\">QTDE</td>";
         $dados .= "</tr>";
         foreach($dadoscalib as $idrel => $calib) {
             $countca = 0;
             $precisaott = 0;
             $media = 0;
             $mediatipo  = array();
             $ctipoext = 0;
             $ctipoint = 0;
             foreach($calib as $user) {
                 foreach($user as $coduser => $vals) {
                     foreach($vals as $val) {
                         $valttrel = $valttrel + $val;
                         $cvals++;
                     }
                     $tipouser = explode("-",$coduser);
                     if($tipouser['1'] == "user_web") {
                         $mediatipo['externo'] = $mediatipo['externo'] + round(($valttrel/$cvals),2);
                         $ctipoext++;
                         $mtipott['externo'] = $mtipott['externo'] + round(($valttrel/$cvals),2);
                         $ctotalext++;
                     }
                     else {
                         $mediatipo['interno'] = $mediatipo['interno'] + round(($valttrel/$cvals),2);
                         $ctipoint++;
                         $mtipott['interno'] = $mtipott['interno'] + round(($valttrel/$cvals),2);
                         $ctotalint++;
                     }
                     $media = $media + round(($valttrel/$cvals),2);
                     $precisao = $precisao + round(($valttrel/$cvals),2);
                     $countpre++;
                     $countca++;
                     $countcatt++;
                     $cvals = 0;
                     $valttrel = 0;
                 }
             }
             //echo $idrel."=$media-$countca<br/>";
             $precisaott = round(($media / $countca),2);
             $dadosnivel[$idrel] = $precisaott;
                 $dados .= "<tr>";
                     $eselrel = "SELECT ".implode(",",$cols)." FROM rel_filtros ".$colunas->AliasTab(rel_filtros)." ".implode(" ",$inner)." WHERE idrel_filtros='$idrel'";
                     $lselrel = $_SESSION['fetch_array']($_SESSION['query']($eselrel));
                     foreach($filtros as $chavef => $valorf) {
                     $dados .= "<td align=\"center\">$lselrel[$valorf]</td>";
                     }
                     $dados .= "<td align=\"center\">".nomevisu($idrel)."</td>";
                     $dados .= "<td align=\"center\">$precisaott</td>";
                     $dados .= "<td align=\"center\">".round(($mediatipo['externo']/$ctipoext),2)."</td>";
                     $dados .= "<td align=\"center\">".round(($mediatipo['interno']/$ctipoint),2)."</td>";
                     $dados .= "<td align=\"center\">$countca</td>";
                 $dados .= "</tr>";
             }
             $dados .= "<tr style=\"font-weight: bold\">";
                 $dados .= "<td colspan=\"".($colf + 1)."\">TOTAL</td>";
                 $dados .= "<td align=\"center\">".round(($precisao / $countpre),2)."</td>";
                 $dados .= "<td align=\"center\">".round(($mtipott['externo'] / $ctotalext),2)."</td>";
                 $dados .= "<td align=\"center\">".round(($mtipott['interno'] / $ctotalint),2)."</td>";
                 $dados .= "<td align=\"center\">$countcatt</td>";
             $dados .= "</tr>";
             $dados .= "</table>";
     $dados .= "</div><table><tr><td><br/><br/></td></tr></table>";
     $dados .= "<div id=\"nivel\">";
             foreach($nivel as $coltd => $reltd) {
                 $dados .= "<table align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px; width: 1024px\">";
                 $dados .= "<tr style=\"background-color:#A64D00; color: #FFF; font-weight: bold\">";
                     $selnome = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='$coltd'";
                     $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                     $dados .= "<td>".$eselnome['nomefiltro_dados']."</td>";
                     $dados .= "<td align=\"center\"><strong>QTDE</strong></td>";
                     $dados .= "<td align=\"center\" width=\"20%\"><strong>PRECISÃO (%)</strong></td>";
                     $dados .= "<td align=\"center\" width=\"20%\" style=\"background-color: #FF8FA2; color:#000\"><strong>PRECISÃO (%) - ".$_SESSION['nomecli']."</strong></td>";
                     $dados .= "<td align=\"center\" width=\"20%\" style=\" background-color: #FF8FA2;color:#000\"><strong>PRECISÃO (%) - CLIENTIS</strong></td>";
                 $dados .= "</tr>";
                 foreach($reltd as $opertd => $idrelnivel) {
                     $selnome = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='$opertd'";
                     $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                     $colsplan[$opertd] = $eselnome['nomefiltro_dados'];
                     $dados .= "<tr>";
                     $dados .= "<td><strong>".$eselnome['nomefiltro_dados']."</strong></td>";
                     foreach($idrelnivel as $idreldados) {
                         foreach($dadoscalib[$idreldados] as $idcalibdados) {
                             foreach($idcalibdados as $iduser => $dadosval) {
                                 $ccalib++;
                                 $qtdett++;
                                 if($iduser == "geral") {
                                 }
                                 else {
                                     $exuser = explode("-",$iduser);
                                     if($exuser[1] == "user_web") {
                                         foreach($dadosval as $val) {
                                             $ttreluser = $ttreluser + $val;
                                             $creluser++;
                                         }
                                         $niveluser['externo'][] = round(($ttreluser / $creluser),2);
                                         $nivelusertt['externo'][] = round(($ttreluser / $creluser),2);
                                     }
                                     else {
                                         foreach($dadosval as $val) {
                                             $ttreluser = $ttreluser + $val;
                                             $creluser++;
                                         }
                                         $niveluser['clientis'][] = round(($ttreluser / $creluser),2);
                                         $nivelusertt['clientis'][] = round(($ttreluser / $creluser),2);
                                     }
                                     $dadosuser[$iduser][$idreldados][] = round(($ttreluser / $creluser),2);
                                     $moniuser = $moniuser + round(($ttreluser / $creluser),2);
                                     $precniveltt = $precniveltt + round(($ttreluser / $creluser),2);
                                     $cmoniuser++;
                                     $ttreluser = 0;
                                     $creluser = 0;
                                 }
                             }
                         }
                     }
                     foreach($niveluser as $tipo => $valtipo) {
                         foreach($valtipo as $v) {
                             if($tipo == "externo") {
                                 $ttext = $ttext + $v;
                                 $cttext++;
                             }
                             else {
                                 $ttinter = $ttinter + $v;
                                 $cttinter++;
                             }
                             $tt = $tt + $v;
                             $c++;
                         }
                     }
                     //echo $tt."-$c=".round(($tt / $c),2);
                     $tt = 0;
                     $c = 0;
                     $dados .= "<td align=\"center\">$ccalib</td>";
                     $dados .= "<td align=\"center\">".round(($moniuser / $cmoniuser),2)."</td>";
                     $dados .= "<td align=\"center\">".round(($ttext /  $cttext),2)."</td>";
                     $dados .= "<td align=\"center\">".round(($ttinter /  $cttinter),2)."</td>";
                     $dados .= "</tr>";
                     $moniuser = 0;
                     $cmoniuser = 0;
                     $precni = 0;
                     $niveluser = array();
                     $precnivel = 0;
                     $ttext = 0;
                     $ttinter = 0;
                     $cttext = 0;
                     $cttinter = 0;
                     $cnivel++;
                     $ccalib = 0;
                 }
                 foreach($nivelusertt as $tipon => $valtipon) {
                     foreach($valtipon as $vn) {
                         if($tipon == "externo") {
                             $ttextn = $ttextn + $vn;
                             $cttextn++;
                         }
                         else {
                             $ttintern = $ttintern + $vn;
                             $cttintern++;
                         }
                     }
                 }
                 $dados .= "<tr style=\"background-color: #EA862F;color:#FFF; font-weight: bold\">";
                     $dados .= "<td>TOTAL - ".$eselnome['nomefiltro_dados']."</td>";
                     $dados .= "<td align=\"center\">$qtdett</td>";
                     $dados .= "<td align=\"center\">".round(($precniveltt / $qtdett),2)."</td>";
                     $dados .= "<td align=\"center\">".round(($ttextn /  $cttextn),2)."</td>";
                     $dados .= "<td align=\"center\">".round(($ttintern /  $cttintern),2)."</td>";
                 $dados .= "</tr>";
                 $dados .= "</table><table><tr><td><br/></td></tr></table>";
                 $nivelusertt = array();
                 $precniveltt = 0;
                 $cnivel = 0;
                 $ttextn = 0;
                 $cttextn = 0;
                 $ttintern = 0;
                 $cttintern = 0;
                 $qtdett = 0;
             }
     $dados .= "</div><table><tr><td><br/><br/></td></tr></table>";
     $dados .= "<div id=\"planilha\">";
         $dados .= "<table align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px;\">";
             $dados .= "<tr style=\"background-color:#EA862F; color: #FFF; font-weight: bold\">";
                 $dados .= "<td width=\"50%\">ITENS</td>";
                 $dados .= "<td align=\"center\" width=\"10%\">PRECISÃO GERAL</td>";
                 foreach($colsplan as $colplan) {
                     $dados .= "<td align=\"center\">$colplan</td>";
                 }
                 foreach($dadosplanuser as $kuser => $duser) {
                      $tipou = explode("-",$kuser);
                     if($tipou[1] != "monitor") {
                         $selfiltro = "SELECT MIN(nivel),nomefiltro_nomes FROM filtro_nomes";
                         $eselfiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die (mysql_error());
                         $filtroag = "id_".strtolower($eselfiltro['nomefiltro_nomes']);
                         $selrel = "SELECT nomefiltro_dados, $filtroag, count(DISTINCT($filtroag)) as result FROM ".str_replace("_", "", $tipou[1])."filtro uf
                                   INNER JOIN rel_filtros r ON r.idrel_filtros = uf.idrel_filtros
                                   INNER JOIN filtro_dados fd ON fd.idfiltro_dados = r.$filtroag
                                   WHERE id$tipou[1]='$tipou[0]'";
                         $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                         $snome = "SELECT id$tipou[1], nome$tipou[1], idperfil_".substr($tipou[1], 5)." FROM $tipou[1] WHERE id$tipou[1]='$tipou[0]'";
                         $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
                         if($eselrel['result'] == 1) {
                              $nomeuser = $esnome['nome'.$tipou[1]]." - ".$eselrel['nomefiltro_dados'];
                         }
                         else {
                              $perfil = "SELECT nomeperfil_web FROM perfil_web WHERE idperfil_web='".$esnome['idperfil_'.substr($tipou[1], 5)]."'";
                              $eperfil = $_SESSION['fetch_array']($_SESSION['query']($perfil)) or die (mysql_error());
                              if($tipou[1] == "user_web") {
                                   $nomeuser = $esnome['nome'.$tipou[1]]." - ".$eperfil['nomeperfil_web'];
                              }
                              else {
                                   $nomeuser = $esnome['nome'.$tipou[1]]." - CLIENTIS";
                              }
                         }
                     }
                     else {
                          if($_SESSION['user_tabela'] != "user_adm") {
                               $nomeuser = "MONITOR".$m;
                               $m++;
                          }
                          else {
                               $snome = "SELECT id$tipou[1], nome$tipou[1] FROM $tipou[1] WHERE id$tipou[1]='$tipou[0]'";
                               $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
                               $nomeuser = $esnome['nome'.$tipou[1]];
                          }
                     }
                     $dados .= "<td align=\"center\">$nomeuser</td>";
                 }
             $dados .= "</tr>";
             $selplan = "SELECT p.idgrupo, g.descrigrupo,idrel FROM planilha p
                         INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                         WHERE idplanilha='".$vars['idplanilha']."' ORDER BY p.posicao, g.posicao";
             $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta da planilha");
             while($lplan = $_SESSION['fetch_array']($eselplan)) {
                 if($lplan['filtro_vinc'] == "S") {
                 }
                 else {
                     $valgeral = 0;
                     $cgeral = 0;
                     $selperg = "SELECT * FROM pergunta WHERE idpergunta='".$lplan['idrel']."'";
                     $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da pergunta");
                     $dados .= "<tr>";
                         $dados .= "<td><strong>".$eselperg['descripergunta']."</strong></td>";
                         foreach($dadosplan[$eselperg['idpergunta']] as $vsgeral) {
                             $valgeral = $valgeral + $vsgeral;
                             $cgeral++;
                         }
                         $dados .= "<td align=\"center\">".round(($valgeral / $cgeral),2)."</td>";
                         foreach($nivel as $prinivel => $segnivel) {
                             foreach($segnivel as $idoperperg => $idsrelperg) {
                                 foreach($idsrelperg as $idrelperg) {
                                     foreach($dadoscalib[$idrelperg] as $calibperg) {
                                         foreach($calibperg as $userperg => $vperg) {
                                             if($userperg == "geral") {
                                             }
                                             else {
                                                 $valperg = $valperg + $vperg[$eselperg['idpergunta']];
                                                 $relperg++;
                                             }
                                         }
                                     }
                                 }
                                 $dados .= "<td align=\"center\">".round(($valperg /  $relperg),2)."</td>";
                                 $valperg = 0;
                                 $relperg = 0;
                             }
                         }
                         foreach($dadosplanuser as $puser => $dadosu) {
                             $mperg = 0;
                             $p = 0;
                             foreach($dadosu[$eselperg['idpergunta']] as $valorp) {
                                 $mperg = $mperg + $valorp;
                                 $p++;
                             }
                             $dados .= "<td align=\"center\">".round(($mperg / $p),2)."</td>";
                         }
                     $dados .= "</tr>";
                 }
             }
         $dados .= "</table>";
     $dados .= "</div><table><tr><td><br/><br/></td></tr></table>";
     $dados .= "<div id=\"monitor\">";
         $dados .= "<table align=\"center\" style=\"font-family:Verdana, Geneva, sans-serif; font-size: 13px;\">";
             $dados .= "<tr style=\"background-color:#EA862F; color: #FFF; font-weight: bold\">";
                 $dados .= "<td align=\"center\">MONITOR</td>";
                 $dados .= "<td align=\"center\">QTDE. CALIBRAGENS</td>";
                 $dados .= "<td align=\"center\">PRECISÃO GERAL</td>";
                 foreach($nivel as $div => $opera) {
                   foreach($opera as $chaveoper => $idsrel) {
                       foreach($idsrel as $rel) {
                         $dados .= "<td align=\"center\">".nomevisu($rel)."</td>";
                       }
                   }  
                 }
             $dados .= "</tr>";
                 $m = 1;
                 foreach($dadosuser as $chavedados => $partic) {
                     $tipou = explode("-",$chavedados);
                     $seluser = "SELECT * FROM $tipou[1] WHERE id$tipou[1]='$tipou[0]'";
                     $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
                     ?>
                     <tr>
                     <?php
                     $ttuser = 0;
                     $cttuser = 0;
                     foreach($partic as $reluser => $dadospartic) {
                         foreach($dadospartic as $valpartic) {
                             $ttuser = $ttuser + $valpartic;
                             $cttuser++;
                             $ttgeral = $ttgeral + $valpartic;
                             $cttgeral++;
                         }
                     }
                     if($tipou[1] != "monitor") {
                         $selfiltro = "SELECT MIN(nivel),nomefiltro_nomes FROM filtro_nomes";
                         $eselfiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die (mysql_error());
                         $filtroag = "id_".strtolower($eselfiltro['nomefiltro_nomes']);
                         $selrel = "SELECT nomefiltro_dados, $filtroag, count(DISTINCT($filtroag)) as result FROM ".str_replace("_", "", $tipou[1])."filtro uf
                                   INNER JOIN rel_filtros r ON r.idrel_filtros = uf.idrel_filtros
                                   INNER JOIN filtro_dados fd ON fd.idfiltro_dados = r.$filtroag
                                   WHERE id$tipou[1]='$tipou[0]'";
                         $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                         $snome = "SELECT id$tipou[1], nome$tipou[1], idperfil_".substr($tipou[1], 5)." FROM $tipou[1] WHERE id$tipou[1]='$tipou[0]'";
                         $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
                         if($eselrel['result'] == 1) {
                              $nomeuser = $esnome['nome'.$tipou[1]]." - ".$eselrel['nomefiltro_dados'];
                         }
                         else {
                              $perfil = "SELECT nomeperfil_web FROM perfil_web WHERE idperfil_web='".$esnome['idperfil_'.substr($tipou[1], 5)]."'";
                              $eperfil = $_SESSION['fetch_array']($_SESSION['query']($perfil)) or die (mysql_error());
                              if($tipou[1] == "user_web") {
                                   $nomeuser = $esnome['nome'.$tipou[1]]." - ".$eperfil['nomeperfil_web'];
                              }
                              else {
                                   $nomeuser = $esnome['nome'.$tipou[1]]." - CLIENTIS";
                              }
                         }
                     }
                     else {
                          if($_SESSION['user_tabela'] != "user_adm") {
                               $nomeuser = "MONITOR".$m;
                               $m++;
                          }
                          else {
                               $snome = "SELECT id$tipou[1], nome$tipou[1] FROM $tipou[1] WHERE id$tipou[1]='$tipou[0]'";
                               $esnome = $_SESSION['fetch_array']($_SESSION['query']($snome)) or die ("erro na query de consulta do nome do usuário");
                               $nomeuser = $esnome['nome'.$tipou[1]];
                          }
                     }
                     $dados .= "<td style=\"font-weight: bold\">".$nomeuser."</td>";
                     $dados .= "<td align=\"center\">$cttuser</td>";
                     $dados .= "<td align=\"center\"";
                     if(round(($ttuser/$cttuser),2) < 95) { 
                         $dados .= "style=\"color:#EA0920\"";
                     }
                     $dados .= ">".round(($ttuser/$cttuser),2)."</td>";
                     foreach($nivel as $divuser => $operausers) {
                         $mediauser = 0;
                         $cmediauser = 0;
                         foreach($operausers as $choperusers => $idsrelusers) {
                             foreach($idsrelusers as $idreluser) {
                                 if($dadosuser[$chavedados][$idreluser] == "") {
                                     $dados .= "<td align=\"center\"></td>";
                                 }
                                 else {
                                     foreach($dadosuser[$chavedados][$idreluser] as $valreluser) {
                                         $dadostt[$idreluser][] =  $valreluser;
                                         $mediauser = $mediauser + $valreluser;
                                         $cmediauser++;
                                         if($tipou[1] == "user_web") {
                                             $dadostipo[$idreluser]["externo"][] = round(($ttuser/$cttuser),2);
                                         }
                                         else {
                                             $dadostipo[$idreluser]["interno"][] = round(($ttuser/$cttuser),2);
                                         }
                                     }
                                     $dados .= "<td align=\"center\">".round(($mediauser/$cmediauser),2)."</td>";
                                 }
                             }
                         }
                     }
                     $dados .= "</tr>";
                 }
                 $dados .= "<tr style=\"font-weight: bold\">";
                     $dados .= "<td>QTDE. GERAL</td>";
                     $dados .= "<td align=\"center\">$cttgeral</td>";
                     $dados .= "<td></td>";
                     foreach($nivel as $divqtde => $operaqtde) {
                         foreach($operaqtde as $choperqtde => $idsrelqtde) {
                             foreach($idsrelqtde as $idrelqtde) {
                                 $dados .= "<td align=\"center\">".count($dadostt[$idrelqtde])."</td>";
                             }
                         }
                     }
                 $dados .= "</tr>";
                 $dados .= "<tr style=\"font-weight: bold\">";
                     $dados .= "<td>MÉDIA GERAL</td>";
                     $dados .= "<td align=\"center\"></td>";
                     $dados .= "<td align=\"center\"";
                     if(round(($ttgeral/$cttgeral),2) < 95) { 
                         $dados .= "style=font-weight:bold\"";
                     }
                     $dados .= ">".round(($ttgeral/$cttgeral),2)."</td>";
                     foreach($nivel as $divmedia => $operamedia) {
                         foreach($operamedia as $chopermedia => $idsrelmedia) {
                             foreach($idsrelmedia as $idrelmedia) {
                                 $ttmedia = 0;
                                 $cttmedia = 0;
                                 foreach($dadostt[$idrelmedia] as $valmedia) {
                                     $ttmedia = $ttmedia + $valmedia;
                                     $cttmedia++;
                                 }
                                 $dados .= "<td align=\"center\">".round(($ttmedia/$cttmedia),2)."</td>";
                             }
                         }
                     }
                 $dados .= "</tr>";
                 $dados .= "<tr>";
                     $dados .= "<td>ITAU - QTDE</td>";
                     $dados .= "<td></td>";
                     $dados .= "<td></td>";
                     foreach($nivel as $divext => $operaext) {
                         foreach($operaext as $choperext => $idsrelext) {
                             foreach($idsrelext as $idrelext) {
                                 $dados .= "<td align=\"center\">".count($dadostipo[$idrelext]['externo'])."</td>";
                             }
                         }
                     }
                 $dados .= "</tr>";
                 $dados .= "<tr>";
                     $dados .= "<td>CLIENTIS - QTDE</td>";
                     $dados .= "<td></td>";
                     $dados .= "<td></td>";
                     foreach($nivel as $divint => $operaint) {
                         foreach($operaint as $choperint => $idsrelint ) {
                             foreach($idsrelint as $idrelint) {
                                 $dados .= "<td align=\"center\">".count($dadostipo[$idrelint]['interno'])."</td>";
                             }
                         }
                     }
                 $dados .= "</tr>";
         $dados .= "</table>";
     $dados .= "</div>";
    }
    $_SESSION['dadosexp'] = $dados;
    echo $dados;
}

?>