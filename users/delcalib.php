<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
//include_once($rais.'/monitoria_supervisao/config/criadbmysql.php');
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

$separa = explode(",",$_GET['idcalib']);
$user = $separa[1];
$idcalib = $separa[0];
$idusercont = $_GET['idusercont'];

if($user == "" OR $idusercont != "") {
    $selmoni = "SELECT idmonitoriacalib FROM monitoriacalib WHERE idagcalibragem='$idcalib'";
    $eselmoni = $_SESSION['query']($selmoni) or die ('erro na query de consulta das monitorias');
    while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
        $delavalia = "DELETE FROM moniavaliacalib
                               WHERE idmonitoriacalib='".$lselmoni['idmonitoriacalib']."'";
        $edelavalia = $_SESSION['query']($delavalia) or die ("erro na query para apagar a calibragem");
    }

    $delmoni = "DELETE FROM monitoriacalib WHERE idagcalibragem='$idcalib'";
    $edelmoni = $_SESSION['query']($delmoni) or die ('erro na query para apagar as monitorias da calibragem');

    $alter = "ALTER TABLE monitoriacalib AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query para reset do incremental");

    $delcalib = "DELETE FROM agcalibragem WHERE idagcalibragem='$idcalib'";
    $edelcalib = $_SESSION['query']($delcalib) or die ('erro na query para apagar a calibragem');

    $alter = "ALTER TABLE agcalibragem AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query para reset do incremental");

    $msg = "Calibragem apagada com sucesso";
    ?>
    <script type="text/javascript">
        alert("Calibragem excluida com sucesso");
        window.location = "/monitoria_supervisao/inicio.php?menu=calibragem&opmenu=AGENDA&submenu=OK";
    </script>
    <?php
    //header("location:/monitoria_supervisao/inicio.php?menu=calibragem&opmenu=AGENDA&submenu=OK&msg=".$msg);
}
else {
    $selmoni = "SELECT idmonitoriacalib FROM monitoriacalib WHERE idagcalibragem='$idcalib' AND iduser='$user'";
    $eselmoni = $_SESSION['query']($selmoni) or die ('erro na query de consulta das monitorias');
    while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
        $delavalia = "DELETE FROM moniavaliacalib
                               WHERE idmonitoriacalib='".$lselmoni['idmonitoriacalib']."'";
        $edelavalia = $_SESSION['query']($delavalia) or die ("erro na query para apagar a calibragem");
    }

    $delmoni = "DELETE FROM monitoriacalib WHERE idagcalibragem='$idcalib' AND iduser='$user'";
    $edelmoni = $_SESSION['query']($delmoni) or die ('erro na query para apagar as monitorias da calibragem');

    $alter = "ALTER TABLE monitoriacalib AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query para reset do incremental");

    $delcalib = "DELETE FROM agcalibragem WHERE idagcalibragem='$idcalib' AND participantes='$user'";
    $edelcalib = $_SESSION['query']($delcalib) or die ('erro na query para apagar a calibragem');

    $alter = "ALTER TABLE agcalibragem AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query para reset do incremental");

    $msg = "Usuário excluído da calibragem com sucesso";
    ?>
    <script type="text/javascript">
        alert("Usuario excluido da calibragem com sucesso");
        window.location="/monitoria_supervisao/inicio.php?menu=calibragem&opmenu=AGENDA&submenu=OK&calib=<?php echo $idcalib;?>";
    </script>
    <?php
    //header("location:/monitoria_supervisao/inicio.php?menu=calibragem&opmenu=AGENDA&submenu=OK&calib=$idcalib&msg=".$msg);
}

?>
