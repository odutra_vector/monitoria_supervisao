<?php

include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');

$classtab = new TabelasSql(); 
$idreg = $_GET['idreg'];

$selreg = "SELECT tipomoni,cr.idparam_moni,idfila, rg.idrel_filtros FROM regmonitoria rg
          INNER JOIN conf_rel cr ON cr.idrel_filtros = rg.idrel_filtros
          INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
          WHERE idregmonitoria='$idreg'";
$eselrel = $_SESSION['fetch_array']($_SESSION['query']($selreg)) or die (mysql_error());

$selcontest = "SELECT *,count(*) as r FROM contestreg WHERE idfila='".$eselrel['idfila']."'";
$eselcontest = $_SESSION['fetch_array']($_SESSION['query']($selcontest)) or die (mysql_errno());

if($eselrel['tipomoni'] == "G") {
	$tabfila = "fila_grava";
	
}
if($eselrel['tipomoni'] == "O") {
	$tabfila = "fila_oper";
}

$selcol = "SELECT nomecoluna,incorpora,tipo FROM camposparam ".$classtab->AliasTab("camposparam")."
          INNER JOIN coluna_oper ".$classtab->AliasTab("coluna_oper")." ON ".$classtab->AliasTab("coluna_oper").".idcoluna_oper = ".$classtab->AliasTab("camposparam").".idcoluna_oper
          INNER JOIN param_moni ".$classtab->AliasTab("param_moni")." ON ".$classtab->AliasTab("param_moni").".idparam_moni = ".$classtab->AliasTab("camposparam").".idparam_moni
          WHERE ".$classtab->AliasTab("param_moni").".idparam_moni='".$eselrel['idparam_moni']."' ORDER BY posicao";
$eselcol = $_SESSION['query']($selcol) or die (mysql_error());
while($lselcol = $_SESSION['fetch_array']($eselcol)) {
	if($lselcol['incorpora'] == "SUPER_OPER") {
          $s++;
          $colsql[] = $classtab->AliasTab("super_oper").".".$lselcol['nomecoluna'];
	}
        if($lselcol['incorpora'] == "OPERADOR") {
          $colsql[] = $classtab->AliasTab("operador").".".$lselcol['nomecoluna'];   
        }
        if($lselcol['incorpora'] == "DADOS") {
             $colsql[] = $classtab->AliasTab($tabfila).".".$lselcol['nomecoluna'];
        }
	$colunas[$lselcol['nomecoluna']] = $lselcol['tipo']."-".$lselcol['incorpora'];
}
if(!isset($s)) {
	$dados = "SELECT narquivo,gravacao,data,datactt,nomemotivo,".implode(",",$colsql)." FROM regmonitoria ".$classtab->AliasTab("regmonitoria")."
			  INNER JOIN fila_grava ".$classtab->AliasTab($tabfila)." ON ".$classtab->AliasTab($tabfila).".idfila_grava = ".$classtab->AliasTab("regmonitoria").".idfila
			  INNER JOIN operador ".$classtab->AliasTab("operador")." ON ".$classtab->AliasTab("operador").".idoperador = ".$classtab->AliasTab($tabfila).".idoperador 
			  INNER JOIN motivo ".$classtab->AliasTab("motivo")." ON ".$classtab->AliasTab("motivo").".idmotivo = ".$classtab->AliasTab("regmonitoria").".idmotivo
			  WHERE idregmonitoria='$idreg'";
}
else {
	$dados = "SELECT narquivo,gravacao,data,datactt,nomemotivo,".implode(",",$colsql)." FROM regmonitoria ".$classtab->AliasTab("regmonitoria")."
			  INNER JOIN fila_grava ".$classtab->AliasTab($tabfila)." ON ".$classtab->AliasTab($tabfila).".idfila_grava = ".$classtab->AliasTab("regmonitoria").".idfila
			  INNER JOIN operador ".$classtab->AliasTab("operador")." ON ".$classtab->AliasTab("operador").".idoperador = ".$classtab->AliasTab($tabfila).".idoperador
			  LEFT JOIN super_oper ".$classtab->AliasTab("super_oper")." ON ".$classtab->AliasTab("super_oper").".idsuper_oper = ".$classtab->AliasTab("operador").".idoperador
			  INNER JOIN motivo ".$classtab->AliasTab("motivo")." ON ".$classtab->AliasTab("motivo").".idmotivo = ".$classtab->AliasTab("regmonitoria").".idmotivo
			  WHERE idregmonitoria='$idreg'";
}
$edados = $_SESSION['fetch_array']($_SESSION['query']($dados)) or die (mysql_error());
?>

<link href="/monitoria_supervisao/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
     #contestreg {
          background-color: #EAAC2F; 
          color: #000; 
          border: 0px; 
          font-weight: bold; 
          padding: 3px; 
          margin: 5px; 
          padding-left: 10px; 
          padding-right: 10px; 
          -moz-border-radius: 5px; 
          -webkit-border-radius: 5px; 
          border-radius: 5px;
     }
     
     #contestreg:hover {
          background-color: #FF8533;
     }
</style>
<script type="text/JavaScript" src="/monitoria_supervisao/audio-player/audio-player.js"></script>
<script type="text/javascript">
     $(document).ready(function() {
         $('#voltapesq').live('click', function() {
             window.location = 'inicio.php?menu=monitoria&retconsult=1';
         });
         
         $("#contestreg").click(function() {
              var obs = $("#obsreg").val();
              if(obs === "") {
                   alert("Favor preencher a Observação");
                   return false;
              }
              else {
                   $.blockUI({ message: '<strong>AGUARDE REGISTRANDO CONTESTAÇÃO...</strong>', css: { 
                         border: 'none', 
                         padding: '15px', 
                         backgroundColor: '#000', 
                         '-webkit-border-radius': '10px', 
                         '-moz-border-radius': '10px', 
                         opacity: .5,
                         color: '#fff'
                         }
                   });
                   $.post("/monitoria_supervisao/exemoni.php",{contestreg:"contestreg",idreg:$("#idreg").val(),obs:$("#obsreg").val()},function(ret) {
                        if(ret == 1) {
                             $.unblockUI();
                             alert("Contestação enviada com sucesso!!!");
                             window.location = 'inicio.php?menu=monitoria&retconsult=1';
                        }
                         if(ret == 0) {
                             $.unblockUI();
                             alert("Ocorreu um erro no processo de inserção da contestação, favor contatar os responsáveis");
                         }
                         if(ret == 2) {
                             $.unblockUI();
                             alert("Registro já apagado da base de dados");
                             window.location = 'inicio.php?menu=monitoria&retconsult=1';
                        }
                   });
              }
         });
     });
 </script>
<div id="histregistro">
     <br/>
     <table id="tabpri" style="width:100%">
        <tr style="height: 50px">
            <td colspan="10" align="left"><input type="button" name="voltapesq" id="voltapesq" value="" style="width:35px; height:35px; background-image:url(/monitoria_supervisao/monitor/images/volta.png); float: left" /></td>
        </tr>
    	<tr>
            <td class="corfd_coltexto"><strong>GRAVAÇÃO</strong></td>
            <td class="corfd_colcampos" colspan="4" width="800px"><?php echo $edados['narquivo'];?><input id="idreg" type="hidden" value="<?php echo $idreg;?>" /></td>
            <td class="corfd_colcampos">
               <?php
               $audio = explode("/",$edados['gravacao']);
               $partaudio = $audio[count($audio) - 1];
               $extaudio = pathinfo($partaudio,PATHINFO_EXTENSION);
               if($extaudio == "wav" OR $extaudio == "WAV") {
                   ?>
                   <EMBED SRC="<?php echo $edados['gravacao'];?>" autostart='false' loop="false" WIDTH="300" HEIGHT="40">
                   <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                   <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                   <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $edados['gravacao'];?>">
                   <param name="quality" value="high">
                   <param name="menu" value="false">
                   <param name="wmode" value="transparent"></object>
                   <?php
               }
               else {
                   ?>
                   <object type="application/x-shockwave-flash" data="/monitoria_supervisao/audio-player/player.swf" id="audioplayer1" height="24" width="290">
                   <param name="movie" value="/monitoria_supervisao/audio-player/player.swf">
                   <param name="FlashVars" value="playerID=audioplayer2&soundFile=<?php echo $edados['gravacao'];?>">
                   <param name="quality" value="high">
                   <param name="menu" value="false">
                   <param name="wmode" value="transparent"></object>
                   <?php
               }
               ?>  
            </td>
        </tr>
        <tr>
            <td class="corfd_coltexto"><strong>DTCTT</strong></td>
            <td class="corfd_colcampos"><?php echo banco2data($edados['datactt']);?></td>
            <td class="corfd_coltexto"><strong>DATA</strong></td>
            <td class="corfd_colcampos"><?php echo banco2data($edados['data']);?></td>
            <td class="corfd_coltexto"><strong>MOTIVO</strong></td>
            <td class="corfd_colcampos"><?php echo $edados['nomemotivo'];?></td>
        </tr>
    </table><br />
    <table id="tabdados" style="width:100%">
    	<tr>
          <td align="center" class="corfd_colcampos" colspan="4" style=" background-color: #EAA43B"><strong>DADOS REGISTRO</strong></td>
        </tr>
        <tr>
             <td class="corfd_coltexto"><strong>RELACIONAMENTO</strong></td>
             <td class="corfd_colcampos" colspan="4"><?php echo nomeapres($eselrel['idrel_filtros']);?></td>
        </tr>
    	<?php
          $countcol = count($colunas);
          foreach($colunas as $nome => $tipo) {
              $quebratipo = explode("-",$tipo);
              $c++;
              $ctt++;
              if($c == 1) {
                    if($ctt == $countcol) {
                    ?>
                    <tr>
                         <td class="corfd_coltexto"><strong><?php echo ucfirst($nome);?></strong></td>
                         <td class="corfd_colcampos" colspan="3"><?php if($quebratipo[0] == "DATE") { echo banco2data($edados[$nome]); } else { echo strtoupper($edados[$nome]); }?> </td>
                    </tr>
                    <?php	
                    }
                    else {
                         ?>
                         <tr>
                         <td class="corfd_coltexto"><strong><?php echo ucfirst($nome);?></strong></td>
                         <td class="corfd_colcampos"><?php if($quebratipo[0] == "DATE") { echo banco2data($edados[$nome]); } else { echo strtoupper($edados[$nome]); }?> </td>
                    <?php	
                    }
              }
              else {
              ?>
                   <td class="corfd_coltexto"><strong><?php echo ucfirst($nome);?></strong></td>
                   <td class="corfd_colcampos"><?php if($quebratipo[0] == "DATE") { echo banco2data($edados[$nome]); } else { echo strtoupper($edados[$nome]); }?></td>
              </tr>
              <?php
                   $c = 0;
              }
          }
          
          ?>
          <tr>
               <td colspan="4"><textarea id="obsreg" style="width: 1020px; height: 100px" <?php if($eselcontest['r'] >= 1) { echo "readonly=\"readonly\"";}?>><?php if($eselcontest['r'] >= 1) { echo $eselcontest['obscontest'];}?></textarea></td>
          </tr>
          <?php
          if($eselcontest['r'] == 0) {
               ?>
               <tr width="50px">
                    <td align="center" colspan="4"><input type="button" id="contestreg" value="CONTESTAR"  /></td>
               </tr>
               <?php
          }
          ?>
    </table>
</div>