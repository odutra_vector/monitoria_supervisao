<?php

session_start();

$iduser = $_SESSION['usuarioID'];
$tabuser = $_SESSION['user_tabela'];

$sfiltros = "SELECT nomefiltro_nomes,obrigatorio FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
$efiltros = $_SESSION['query']($sfiltros) or die ("erro na consulta dos filtro ativos");
while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
    if($lfiltros['obrigatorio'] == "S") {
        $nsfiltros[] = strtolower($lfiltros['nomefiltro_nomes']);
    }
    $filtros[] = strtolower($lfiltros['nomefiltro_nomes']);
}
asort($nsfiltros);

scripts_filtros();
unset($_SESSION['varsconsult']);

$selrelesp = "SELECT * FROM rel_resp_especifica rr
              INNER JOIN resp_especificas re ON re.idrel_resp_especifica = rr.idrel_resp_especifica GROUP BY rr.idrel_resp_especifica";
$eselresp = $_SESSION['query']($selrelesp) or die ("erro na consulta do relatório especifico");
while($lrespjava = $_SESSION['fetch_array']($eselresp)) {
    $relresp[$lrespjava['idrel_resp_especifica']] = $lrespjava['nome'];
}
$keys = array_keys($relresp);
foreach($keys as $key) {
    $relrespcheck[] = "tipo == '$key'";
}
$relresptipos = implode(" || ",$relrespcheck);
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#gerarel').click(function() {
            var ID = $("#ID").val();
            var periodo = $("#periodoauto").val();
            var tipo_rel = $('#tipo').val();
            var plan = $('#plan').val();
            var nomeplan = $("#plan").find(":selected").text();
            var operador = $("#oper").val();
            var dtini = $("#dtinimoni").val();
            var dtfim = $("#dtfimmoni").val();
            var dtinictt = $('#dtinictt').val();
            var dtfimctt = $('#dtfimctt').val();
            <?php
            foreach($nsfiltros as $filtrosob) {
                $objs[] =  "$filtrosob == ''";
                ?>
                var <?php echo $filtrosob;?> = $('#filtro_<?php echo $filtrosob;?>').val();
                <?php
            }
            if(count($nsfiltros) >= 1) {
                ?>
                if(ID == "" &&  (<?php echo implode(" || ",$objs);?>)) {
                    alert("Os filtros <?php echo strtoupper(implode(",",$nsfiltros));?> são obrigatórios");
                    return false;
                }
                <?php
            }
            ?>
            if(tipo_rel == "") {
                alert('Favor selecionar o tipo do relatório');
                return false;
            }
            else {
                if(tipo_rel == "semanal") {
                    if(plan == "" || dtinictt == "" || dtfimctt == "") {
                        alert('Os campos Data Contato e Planilha são de preenchimento obrigatório!!!');
                        return false;
                    }
                    else {
                        <?php
                        if($_SESSION['selbanco'] == "monitoria_itau") {
                            ?>
                            if(tipo_rel == "mensal" && (nomeplan.match("AUDITORIA") || nomeplan.match("auditoria"))) {
                                alert("A planilha de AUDITORIA não possui relatório mensal disponível");
                                return false;
                            }
                            <?php
                        }
                        ?>
                    }
                }
                else {
                    if(dtinictt == "" || dtfimctt == "" || dtini == "" || dtfim == "") {
                        alert('Os campos Data Contato e Data Monitoria precisam estar preenchidos!!!');
                        return false;
                    }
                    else {
                        if((tipo_rel == "operador" || tipo_rel == "histoperador") && periodo == "") {
                            alert('É necessário preencher o filtro de PERIODO para o RELATÓRIO "OPERADOR" OU "QUARTIL/QUADRANTE"!!!');
                            return false;
                        }
                    }
                }
            }
        })
    });
</script>
<div>
    <form action="users/dadosrel_<?php echo strtolower($_SESSION['nomecli']);?>.php" method="post" id="result" target="_blank">
        <br/>
        <table width="243">
          <tr>
            <td width="117" class="corfd_coltexto"><strong>CONSOLIDAÇÃO</strong></td>
            <td width="114" class="corfd_colcampos">
            	<select name="tipo" id="tipo">
                    <option value="" selected="selected" disabled="disabled">SELECIONE...</option>
                    <option value="operador">OPERADOR</option>
                    <option value="histoperador">QUARTIL / QUADRANTE</option>
                    <option value="semanal">SEMANAL</option>
                    <?php
                    foreach($relresp as $idrelresp  => $nomerel) {
                        echo "<option value=\"$idrelresp\">$nomerel</option>";
                    }
                    if($_SESSION['user_tabela'] == "user_adm") {
                        ?>
                        <option value="mensal">MENSAL</option>
                        <option value="exefluxo">EXECUTIVO FLUXO</option>
                        <option value="contesta">ANALÍTICO CONTESTAÇÕES</option>
                        <option value="contestatmp">ANALÍTICO TEMPOS CONTESTACOES</option>
                        <?php
                    }
                    else {
                        ?>
                        <option value="mensal">MENSAL</option>
                        <?php
                    }
                    ?>
                </select>
            </td>
          </tr>
        </table>
        <?php
        filtros_divs();
        ?>
        <table width="1024" bgcolor="#FFFFFF">
            <tr>
            <?php
            if(count($filtros) == 0) {
                ?>
                <td align="center" style="color:#F00"><strong>NÃO EXISTEM FILTROS CADASTRADOS NO SISTEMA, FAVOR VERIFICAR</strong></td>
                <?php
            }
            else {
                ?>
                <td align="center"><input type="submit" class="botaorel" name="gerarel" id="gerarel" tittle="Relatório Semanal" value="GERAR RELATÓRIO"></td>
                <?php
            }
            ?>
          </tr>
        </table>
    </form>
</div>
