<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SISTEMA DE MONITORIA SIGMA-Q</title>
<link href="styleinicio.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.6.2.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
        $("#spanlogin").hide();
        $("#spansenha").hide();
        $("#spannovasenha").hide();
        $("#spannovasenharep").hide();
        $("spanmsg").hide();
		$("#altera").click(function() {
            $("#spanlogin").hide();
            $("#spansenha").hide();
            $("#spannovasenha").hide();
            $("#spannovasenharep").hide();
            var login = $("#user").val();
            var senha = $("#senha").val();
            var novasenha = $("#novasenha").val();
            var novasenharep = $("#confirsenha").val();
            if(login == "" || senha == "" || novasenha == "" || novasenharep == "") {
                if(login == "") {
                    $("#spanlogin").show();
                }
                if(senha == "") {
                    $("#spansenha").show();
                }
                if(novasenha == "") {
                    $("#spannovasenha").show();
                }
                if(novasenharep == "") {
                    $("#spannovasenharep").show();
                }
                return false;
            }
            else {
                if(novasenha != novasenharep) {
                    $("#spanmsg").text("A REPETIÇÃO DA SENHA NÃO CONFERE!!!");
                    $("#spanmsg").show();
                    return false;
                }
                else {
                    if(novasenha.length >= 8) {
                    }
                    else {
                        $("#spanmsg").text("A SENHA NÃO CONTEM 8 CARACTERES!!!");
                        $("#spanmsg").show();
                        return false;
                    }
                }
            }
        })
    });
</script>
</head>
<body>
	<div id="topo">
	    <span style="float:left; font-weight:bold; color:#ccc; font-size:11px">Resolução aconselhável 1024 X 768</span>
    	<span style="float: right; font-weight:bold; color:#ccc; font-size:11px">Powered by Vector DSI TEC</span>
        <div style="margin:auto;">
            <table align="center">
                <tr>
                    <td><span style="color:#FFFFFF; font-size:20px; font-weight:bold">SISTEMA DE MONITORIA </span><br /><span style="color:#FFFFFF; font-size:11px">&copy 2010 Todos os direitos reservados</span></td>
                    <td><img src="/monitoria_supervisao/images/img_logo.jpg" height="39" /></td>
                 </tr>
            </table>
        </div>
    </div>
	<div id="login">
    	<form action="alterasenha.php" method="post" name="login" >
        	<table width="317" align="center">
            	<tr height="50px">
                	<td width="117" class="textos">LOGIN</td>
                    <td width="188"><input type="text" name="user" id="user" class="input"  /><br />
                    <span id="spanlogin" class="span">* preencha o login</span></td>
                </tr>
                <tr height="50px">
                	<td width="117" class="textos">SENHA</td>
                    <td width="188"><input type="password" name="senha" id="senha" class="input"  /><br />
                    <span id="spansenha" class="span">* preencha a senha</span></td>
                </tr>
                <tr height="50px">
                	<td width="117" class="textos">NOVA SENHA</td>
                    <td width="188"><input type="password" name="novasenha" id="novasenha" class="input"  /><br />
                    <span id="spannovasenha" class="span">* preencha a nova senha</span></td>
                </tr>
                <tr height="50px">
                	<td width="117" class="textos">CONFIRMAÇÃO</td>
                    <td width="188"><input type="password" name="confirsenha" id="confirsenha" class="input"  /><br />
                    <span id="spannovasenharep" class="span">* confirme a senha</span></td>
                </tr>
                <tr>
                	<td colspan="2"><input type="submit" name="altera" id="altera" value="ALTERAR" class="button" /><input style="margin-left:10px" type="submit" name="volta" value="VOLTAR" class="button" /></td>
                </tr>            
            </table>
        </form>
        <div style="margin:auto; text-align:center">
            <br /><span class="span" id="spanmsg"><?php echo $_GET['msg']; ?></span>
        </div>
        <br />
    </div>
</body>
</html>
