<?php
session_start();
$ip = $_SERVER['REMOTE_ADDR'];
$rais = $_SERVER['DOCUMENT_ROOT'];
header('Content-type: text/html; charset=utf-8');

if(!file_exists($rais."/monitoria_supervisao/config/conexao.php")) {
    header("location:/monitoria_supervisao/config/index.php");
}
else {
    if(isset($_SESSION['usuarioID']) OR isset($_SESSION['usuarioNome'])) {
        header("location:/monitoria_supervisao/inicio.php");
    }
    else {
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1" />
<meta http-equiv="Content-Type" content="text/html, charset=utf-8"/>
<title>SISTEMA DE MONITORIA SIGMA-Q</title>
<link href="styleinicio.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.6.2.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/thickbox/thickbox.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#esquecisenha").hide();
        $("#spanlogin").hide();
        $("#spansenha").hide();
        $("#spanimg").hide();
        $("#logar").click(function() {
            $("#spanmsg").hide();
            $("#spanlogin").hide();
            $("#spansenha").hide();
            $("#spanmsg").hide();
            var login = $("#user").val();
            var senha = $("#senha").val();
            var img = $("#imgdig").val();
            if(login == "" || senha == "") {
                if(login == "") {
                    $("#spanlogin").show();
                }
                if(senha == "") {
                    $("#spansenha").show();
                }
                return false;
            }
            else {
                <?php
                if(eregi("192.168",$ip) OR eregi("localhost",$ip) OR eregi("127.0.0.1",$ip) OR eregi("::1",$ip) OR in_array($ip, $ipitau)) {
                ?>
                $("#spanmsg").show();
                <?php
                }
                else {
                ?>
                if(img == "") {
                    $("#spanimg").show();
                    return false;
                }
                else {
                $("#spanmsg").show();
                }
                <?php
                }
                ?>
            }
        });
		
        $("#asenha").click(function() {
            $("#login").hide();
            $("#esquecisenha").show();
        })

        $("#envia").click(function() {
            var email = $("#email").val();
            if(email == "") {
                $("#spanemail").html("FAVOR INFORMAR O E-MAIL");
                return false;
            }				
        })
    });

</script>
</head>
<body>
    <div id="topo">
    <span style="float:left; font-weight:bold; color:#FFF; font-size:11px">Resolução aconselhável 1024 X 768</span>
    <span style="float: right; font-weight:bold; font-size:11px" id="asite"><a href="http://www.vectordsitec.com.br" target="_blank" style="text-decoration: none; color: #FFF">Powered by Vector DSI TEC</a></span>
    <div style="margin:auto;">
            <table align="center">
                <tr>
                <td><span style="color:#FFFFFF; font-size:20px; font-weight:bold">SISTEMA DE MONITORIA </span><br /><span style="color:#FFFFFF; font-size:11px">&copy 2010 Todos os direitos reservados</span></td>
                <td><img src="/monitoria_supervisao/images/img_logo.jpg" height="39" /></td>
             </tr>
        </table>
    </div>
    </div>
	<div id="login">
    	<form action="autentica.php" method="post">
            <table width="300" align="center">
                <tr height="50px">
                    <td width="89" class="textos">LOGIN</td>
                    <td width="190"><input class="input" name="user" id="user" value="" type="text" /><br /><span class="span" id="spanlogin">* preencher login</span></td>
                </tr>
                <tr height="50px">
                    <td width="89" class="textos">SENHA</td>
                    <td width="172"><input class="input" name="senha" id="senha" value="" type="password" /><br /><span class="span" id="spansenha">* preencher senha</span></td>
                </tr>
                <tr height="90px">
                    <td colspan="2"><input type="submit" name="logar" id="logar" value="OK" class="button" /><input style="margin-left:10px" type="submit" name="altsenha" value="ALTERAR SENHA" class="button" /><br /><br /><span style="font-size:12px;color:#09F;font-weight:bold;"><a href="#" id="asenha" style="color: #09F; font-size: 13px; font-family: sans-serif; font-weight: bold">Esqueci minha senha!!!</a></span></td>
                </tr>
            </table>
        </form>
        <div style="text-align: center; margin-top:10px">
            <span class="span" id="spanmsg"><?php echo strtoupper($_GET['msg']);?></span>
        </div>
        <br />
	</div>
    <div id="esquecisenha">
    	<form action="envsenha.php" method="post">
            <ul style="list-style:none">
            	<li class="textos" style="height:35px">E-MAIL <input name="email" id="email" type="text" class="input" style="margin-left:10px; width:200px"/></li>
                <li><input type="submit" name="envia" id="envia" value="ENVIAR" class="button"  /></li>
                <li style="text-align:center"><span class="span" id="spanemail"></span></li>
            </ul>
        </form>
    </div>
</body>
</html>
<?php
    }
}
?>
