$(document).ready(function() {
    var hoje = new Date();
    $("#dtinictt").datepicker({
        minDate: new Date(hoje.getFullYear(),hoje.getUTCMonth() -4,1), // bloqueio de calendário 14/12 solicitação clientis
        dateFormat: 'dd/mm/yy',
        //stepMonths: 3,
        changeMonth: true,
        changeYear: true,
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],  
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],  
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],  
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],  
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],  
        nextText: 'Próximo',  
        prevText: 'Anterior'
    });
});