<?php

session_id();
session_start();

$sessionid = session_id();
unset($_SESSION['userbancos']);
include_once('config/conexao.php');
include_once('admin/functionsadm.php');

if(isset($_POST['selcli'])) {
    $cli = $_POST['cliente'];
}
else {
    $_SESSION['usuario'] = $_POST['user'];
    $_SESSION['senha'] = md5($_POST['senha']);
    $_SESSION['novasenha'] = addslashes(trim($_POST['novasenha']));
    $_SESSION['confirma'] = addslashes(trim($_POST['confirsenha']));
}

if(isset($_POST['altera']) OR isset($_POST['selcli'])) {
    if($_SESSION['usuario'] == "" || $_SESSION['senha'] == "" || $_SESSION['novasenha'] == "" || $_SESSION['confirma'] == "") {
        $msg = "Para alteração da senha todos campos precisam estar preenchidos, favor tentar novamente!";
        header("location:altsenha.php?msg=".$msg);
        break;
    }
    if($_SESSION['novasenha'] != $_SESSION['confirma']) {
        $msg = "A confirmação da nova senha não confere com a senha digitada!";
        header("location:altsenha.php?msg=".$msg);
        break;
    }
    if(strlen($_SESSION['novasenha']) < 8) {
        $msg = "A nova senha deve ter no mínimo 8 caracteres !";
        header("location:altsenha.php?msg=".$msg);
        break;
    }
    if(!eregi('[0-9]',$_SESSION['novasenha']) OR !eregi('[a-z]',$_SESSION['novasenha'])) {
        $msg = "A senha deve conter letras e números!";
        header("location:altsenha.php?msg=".$msg);
        break;
    }
    else {
    // Usa a fun��o addslashes para escapar as aspas
      $nusuario = addslashes($_SESSION['usuario']);
      $senha = addslashes($_SESSION['senha']);

      if(isset($_POST['selcli'])) {
          $selbanco[] = $cli;
          $_SESSION['userbancos'][] = $cli;
          $tabuser = array('monitor','user_adm','user_web');
          $sqlbanco = $_SESSION['seldb'](implode(",",$selbanco));
          foreach($tabuser as $tab) {
              $seluser = "SELECT COUNT(*) as result FROM $tab WHERE usuario='".$_SESSION['usuario']."' AND senha='".$_SESSION['senha']."'";
              $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
              if($eseluser['result'] >= 1) {
                  $tabela[] = $tab;
              }
              else {
              }
          }
      }
      else {
          $listdb = "SHOW DATABASES LIKE 'monitoria_%'";
          $elistdb = $_SESSION['query']($listdb);
          $tabelas = array('user_adm' => 'iduser_adm', 'user_web' => 'iduser_web', 'monitor' => 'idmonitor');
          $rows = $_SESSION['num_rows']($elistdb["Database (monitoria_%)"]);
          //$selbanco = array(); // instancia a variável do banco de dados selecionado vazia
            while($nomes = $_SESSION['fetch_assoc']($elistdb)) { // lista os bancos de dados existentes
                if(!strstr($nomes["Database (monitoria_%)"],'automatica') && !strstr($nomes["Database (monitoria_%)"],'telefonica')) {
                    $selbd = $_SESSION['seldb']($nomes["Database (monitoria_%)"]);
                    foreach($tabelas as $tab => $idtab) {
                        $login = "SELECT *, COUNT(*) as result FROM $tab WHERE usuario='$nusuario' AND senha='$senha' LIMIT 1";
                        $elogin = $_SESSION['fetch_array']($_SESSION['query']($login)) or die (mysql_error());
                        if($elogin['result'] >= 1) {
                            $selbanco['banco'] = $nomes["Database (monitoria_%)"]; // acrescenta valor a variável que foi instanciada como vazia como a condição seja atendida
                            $senhaini[$nomes["Database (monitoria_%)"]] = $lresult['senha_ini'];
                            $tabela[] = $tab; // identifica a tabela quando o usuário é localizado no banco consultado
                            $_SESSION['userbancos'][] = $nomes["Database (monitoria_%)"];
                        }
                    }
                }
            }
        }
      $list = array(); // cria um array para armazenar em quais bancos de dados o usuario esta presente com o mesmo login e senha

      if($selbanco == "") { // faz novamente a verificação se a variável $selbanco está vazia, redundância de segurança
        $msg = "''Usuário'' ou ''Senha'' inválidos!";
        header("location:altsenha.php?msg=".$msg);
      }
      else { // caso o select não apresente nenhuma das situações acima retorna "true" logando usuário ao sistema
            if(count($_SESSION['userbancos']) == 1) {
              $selbd = $_SESSION['seldb']($_SESSION['userbancos'][0]);
              $checkatv = "SELECT ativo FROM $tabela[0] WHERE usuario='$nusuario' AND senha='$senha' LIMIT 1";
              $echeck = $_SESSION['fetch_array']($_SESSION['query']($checkatv)) or die ("erro na query de verificação se o usuário está ativo");
              if ($echeck['ativo'] == "N") {  // verifica se usuário está ativo
                  $msg = "''Usuário'' ou ''Senha'' inválidos!";
                  header("location:altsenha.php?msg=".$msg);
              }
              else {
                  $novasenha = md5($_SESSION['novasenha']);
                  $sqlbanco = $_SESSION['seldb'](implode(",",$selbanco));
                  $tabup = implode(",",array_unique($tabela));
                  $sql = "UPDATE $tabup SET senha='$novasenha', senha_ini='N' WHERE usuario='$nusuario'";
                  $exe = $_SESSION['query']($sql) or die (mysql_error());
                  $msg = "Senha alterada com sucesso!!!";
                  header("location:index.php?&msg=".$msg);
              }
            }
            if(count($_SESSION['userbancos']) > 1) {
                $_SESSION['tabela'][] = $tabela;
                $msg = "Por favor, informe em qual cliente deseja efetuar alteração de senha ou em ambos!";
                header("location:selclisenha.php?".$sessionid."&msg=".$msg);
            }
      }
    }
}
if(isset($_POST['volta'])) {
    unset($_SESSION['usuario']);
    unset($_SESSION['senha']);
    unset($_SESSION['novasenha']);
    unset($_SESSION['confirma']);
    header("location:index.php");
}
?>
