<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

if(isset($_POST['validar'])) {
  $iduser = $_SESSION['usuarioID'];
  $nomeuser = $_SESSION['usuarioNome'];
  $data = date('Y-m-d');
  $inicio = date('H:i:s');
  $motivo = $_POST['motivo'];
  if($motivo == "") {
    $msgp = "FAVOR SELECIONAR UM DETALHE DO MOTIVO PARA ATUALIZAR O STATUS";
    header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
    break;
  }
  else {
    $selnmot = "SELECT m.nomemotivo, m.idmotivo, tp.nometipo_motivo as nometipo, tp.idtipo_motivo as idtipo, tp.vincula FROM motivo m
                INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo WHERE m.idmotivo='$motivo'";
    $eselnmot = $_SESSION['fetch_array']($_SESSION['query']($selnmot)) or die ("erro na query de consulta do nome do motivo");
    $nmotivo = $eselnmot['nomemotivo'];
    $tipo = $eselnmot['nometipo'];
    if($tipo == "PAUSA") {
      $status = "SELECT status,(time_to_sec('".date('H:i:s')."') - time_to_sec(hstatus)) as decorrido FROM monitor WHERE idmonitor='$iduser'";
      $estatus = $_SESSION['fetch_array']($_SESSION['query']($status)) or die ("erro na query de consulta do status");
      if($estatus['status'] == "PAUSA") {
	$msgp = "SEU STATUS JÁ ESTÁ DEFINIDO COMO ''PAUSA'', FINALIZE O STATUS ANTES DE COLOCAR UMA NOVA PAUSA";
	header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
	break;
      }
      if($estatus['status'] == "AGUARDANDO") {
        if($_SESSION['idfila'] != "") {
            lock($_SESSION['tabfila'], "WRITE");
            $upfila = "UPDATE ".$_SESSION['tabfila']." SET monitorando='0', idmonitor=NULL WHERE id".$_SESSION['tabfila']."='".$_SESSION['idfila']."'";
            $eupfila = $_SESSION['query']($upfila) or die (mysql_error());
            unlock();
            unset($_SESSION['idfila']);
            unset($_SESSION['tabfila']);
        }
	$selpausas = "SELECT COUNT(*) as result FROM moni_pausa WHERE idmonitor='$iduser' AND horafim='00:00:00'";
	$eselpausas = $_SESSION['fetch_array']($_SESSION['query']($selpausas)) or die ("erro na query de consutla das pausas do monitor!!!");
	if($eselpausas['result'] >= 1) {
	  $msgp = "VOCÊ POSSUI UMA INSCONSISTENCIA EM SUAS PAUSAS, FAVOR CONTATAR SEU SUPERVISOR!!!";
	  header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
	  break;
	}
	else {
          $selauto = "SELECT idmotivo,tempo,count(*) as result FROM motivo m
                      INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo WHERE vincula='automatico' AND m.ativo='S'";
          $eselauto = $_SESSION['fetch_array']($_SESSION['query']($selauto)) or die (mysql_error());
          if($eselauto['result'] == 1) {
              $tempoauto = $eselauto['tempo'];
              if($estatus['decorrido'] > $tempoauto) {
                  $s = 1;
              }
              else {
                  $s = 0;
              }
          }
          else {
              $s = 0;
          }
          if($s == 0) {
            $atualiza = "INSERT INTO moni_pausa (idmonitor, idmotivo, data, horaini,horafim,tempo) VALUES ('$iduser', '$motivo', '$data', '$inicio','00:00:00','00:00:00')";
            $eatu = $_SESSION['query']($atualiza) or die ("erro na query de atualizaÃ§Ã£o do status");
            $atumoni = "UPDATE monitor SET status='PAUSA', hstatus='$inicio' WHERE idmonitor='$iduser'";
            $eatumoni = $_SESSION['query']($atumoni) or die ("erro na query de atualizaÃ§Ã£o do status do monitor");
            if($eatu && $eatumoni) {
              $msgp = "STATUS ATUALIZADO";
              header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
              break;
            }
            else {
              $msgp = "Erro no processo de atualizaÃ§Ã£o do status, favor chamar o administrador!!!";
              header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
              break;
            }
          }
          else {
              $hora = date('H:i:s');
              $pausa = "INSERT INTO moni_pausa (idmonitor,idmotivo,data,horaini,horafim,tempo,lib_super,iduser_adm,obs) VALUES ('".$_SESSION['usuarioID']."','".$eselauto['idmotivo']."','".date('Y-m-d')."','$hora','00:00:00','00:00:00','N',NULL,NULL)";
              $epause = $_SESSION['query']($pausa) or die ("erro na query de inclusão de pausa");
              $atumoni = "UPDATE monitor SET status='PAUSA',hstatus='$hora' WHERE idmonitor='".$_SESSION['usuarioID']."'";
              $eatumoni = $_SESSION['query']($atumoni) or die ("erro na query para atualizar o status do monitor na pausa automatica");
              unset($_SESSION['horaini']);
              unset($_SESSION['inimoni']);
              unset($_SESSION['fimlig']);
              unset($_SESSION['codini']);
              unset($_SESSION['codfim']);
              unset($_SESSION['idfila']);
              unset($_SESSION['tabfila']);
              $msgp = "PAUSE INSERIDA AUTOMÁTICAMENTE!!!";
              header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
              break;
          }
	}
      }
    }
    if($tipo == "MONITORAR") {
      $status = "SELECT status FROM monitor WHERE idmonitor='$iduser'";
      $estatus = $_SESSION['fetch_array']($_SESSION['query']($status)) or die ("erro na query de consulta do status");
      if($estatus['status'] == "AGUARDANDO") {
	$msgp = "SEU STATUS JÃ� ESTÃ� DEFINIDO COMO ''AGUARDANDO'', NÃƒO PODENDO SER ATUALIZADO COM ESTE MOTIVO!!!";
	header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
	break;
      }
      if($estatus['status'] == "PAUSA") {
	$fim = date('H:i:s');
	$selpausa = "SELECT mp.idmoni_pausa, mp.horaini, mp.horafim, m.tempo  FROM moni_pausa mp 
                     INNER JOIN motivo m ON m.idmotivo = mp.idmotivo 
                     WHERE mp.idmonitor='$iduser' AND mp.horafim='00:00:00' ORDER BY mp.data DESC, mp.horaini DESC LIMIT 1";
	$epausa = $_SESSION['fetch_array']($_SESSION['query']($selpausa)) or die ("erro na query para selecionar a ultima pausa");
	$idpausa = $epausa['idmoni_pausa'];
	$hini = $epausa['horaini'];
	$tempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('$fim') - TIME_TO_SEC('$hini')) as tempo, (TIME_TO_SEC('$fim') - TIME_TO_SEC('$hini')) > '".$epausa['tempo']."' as dif";
        $etempo = $_SESSION['fetch_array']($_SESSION['query']($tempo)) or die ("erro na query de calculo do tempo da pausa");
	if($etempo['dif'] >= 1) {
            $msgp = "A PAUSA ESTÃ� ESTOURADA, FAVOR CONTATAR SEU SUPERVISOR!!!";
            header("location:/monitoria_supervisao/inicio.php?menu=sistema&msgp=".$msgp);
            break;
        }
        else {
            $atupausa = "UPDATE moni_pausa SET horafim='$fim', tempo='".$etempo['tempo']."' WHERE idmoni_pausa='$idpausa'";
            $eatupausa = $_SESSION['query']($atupausa) or die ("erro na query de atualizaÃ§Ã£o da pausa");
            $atumoni = "UPDATE monitor SET status='AGUARDANDO', hstatus='$inicio' WHERE idmonitor='$iduser'";
            $eatumoni = $_SESSION['query']($atumoni) or die ("erro na query de atualizaÃ§Ã£o do status do monitor");
            if($eatupausa && $eatumoni) {
              $msgp = "PAUSA ATUALIZADA COM SUCESSO E STATUS ATUALIZADO!!!";
              header("location:/monitoria_supervisao/inicio.php?meni=sistema&msgp=".$msgp);
              break;
            }
            else {
              $msgp = "Erro no processo de atualizaÃ§Ã£o do status, favor chamar o administrador!!!";
              header("location:/monitoria_supervisao/inicio.php?meni=sistema&msgp=".$msgp);
              break;
            }
        }
      }

      if($estatus['status'] == "OFFLINE") {
          header("location:/monitoria_supervisao/index.php");
          break;
      }
    }
  }
}
if(isset($_POST['auto'])) {
    $selauto = "SELECT idmotivo,count(*) as result FROM motivo m
                INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo WHERE vincula='automatico'";
    $eselauto = $_SESSION['fetch_array']($_SESSION['query']($selauto)) or die (mysql_error());
    if($eselauto['result'] == 1) {
        $hora = date('H:i:s');
        $pausa = "INSERT INTO moni_pausa (idmonitor,idmotivo,data,horaini,horafim,tempo,lib_super,iduser_adm,obs) VALUES ('".$_SESSION['usuarioID']."','".$eselauto['idmotivo']."','".date('Y-m-d')."','$hora','00:00:00','00:00:00','N',NULL,NULL)";
        $epause = $_SESSION['query']($pausa) or die ("erro na query de inclusão de pausa");
        $atumoni = "UPDATE monitor SET status='PAUSA',hstatus='$hora' WHERE idmonitor='".$_SESSION['usuarioID']."'";
        $eatumoni = $_SESSION['query']($atumoni) or die ("erro na query para atualizar o status do monitor na pausa automatica");
        echo 1;
    }
    else {
        echo 0;
    }
}

?>