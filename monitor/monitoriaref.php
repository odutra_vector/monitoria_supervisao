<?php

session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'], $_SESSION['idcli'], $tipo);
$idfila = mysql_real_escape_string($_GET['idfila']);

protegePagina();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MONITORIA REFERÊNCIA</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.6.2.js" type="text/javascript"></script>
</head>
<body>
    <div style="font-family:Verdana, Geneva, sans-serif; margin: auto">
        <?php
        $selfila = "SELECT fg.idrel_filtros,idmonitoriavenda,ccampos FROM fila_grava fg 
                    INNER JOIN conf_rel cr ON cr.idrel_filtros = fg.idrel_filtros
                    INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                    WHERE idfila_grava='$idfila'";
        $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die (mysql_error());
        $idmonitoria = $eselfila['idmonitoriavenda'];
        $selmonitoria = "SELECT * FROM monitoria WHERE idmonitoria='$idmonitoria'";
        $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmonitoria)) or die (mysql_error());
        $idplan = $eselmoni['idplanilha'];
        $obs = $eselmoni['obs'];
        $selfg = "SELECT ma.idgrupo,descriplanilha,descrigrupo,g.posicao as posivinc,descriresposta,descripergunta,p.posicao as posiperg,descrisubgrupo,s.posicao as posisub,filtro_vinc,obs FROM moniavalia ma
                  INNER JOIN planilha pa ON pa.idplanilha = ma.idplanilha AND pa.idgrupo = ma.idgrupo
                  INNER JOIN grupo g ON g.idgrupo = pa.idgrupo
                  LEFT JOIN subgrupo s ON s.idsubgrupo = ma.idsubgrupo
                  INNER JOIN pergunta p ON p.idpergunta = ma.idpergunta AND p.idresposta = ma.idresposta
                  WHERE idmonitoria='$idmonitoria' AND p.avalia='FG'
                  ORDER BY pa.posicao";
        $efg = $_SESSION['query']($selfg) or die (mysql_error());
        while($lfg = $_SESSION['fetch_array']($efg)) {
            $descriplan = $lfg['descriplanilha'];
            if(!in_array($lfg['idgrupo'], $grupofg)) {
                $grupofg[$lfg['idgrupo']] = $lfg['descrigrupo'];
            }
            if($lfg['filtro_vinc'] == "P") {
                $grupoperg[$lfg['idgrupo']][$lfg['posivinc']] = $lfg['descripergunta'];
                $pergresp[$lfg['descripergunta']][$lfg['posiperg']] = $lfg['descriresposta'];
                $obsperg[$lfg['descripergunta']] = $lfg['obs'];
            }
            else {
                $gruposub[$lfg['idgrupo']][$lfg['posivinc']] = $lfg['descrisubgrupo'];
                $subperg[$lfg['descrisubgrupo']][$lfg['posisub']] = $lfg['descripergunta'];
                $pergresp[$lfg['descripergunta']][$lfg['posiperg']] = $lfg['descriresposta'];
                $obsperg[$lfg['descripergunta']] = $lfg['obs'];
            }
        }
        ?>
        <table border="0" width="700px" style="font-size: 12px">
            <thead>
                <tr>
                    <th class="corfd_ntab" style="font-weight: bold">SINALIZAÇÕES GRAVES</th>
                </tr>
            </thead>
            <tbody>
                <tr bgcolor="#669966" align="center"><td>PLANILHA - <?php echo $descriplan;?></td></tr>
                <?php
                foreach($grupofg as $idgrupo => $descri) {
                    ?>
                    <tr align="center" bgcolor="#999999"><td><?php echo $descri;?></td></tr>
                    <?php
                    if($gruposub[$idgrupo]) {
                        ksort($gruposub[$idgrupo]);
                        foreach($gruposub[$idgrupo] as $posisub => $descrisub) {
                            ?>
                            <tr align="center" bgcolor="#99CCFF"><td><?php echo $descrisub;?></td></tr>
                            <?php
                            ksort($subperg[$descrisub]);
                            foreach($subperg[$descrisub] as $posiperg => $pergs) {
                                ?>
                                <tr align="center" bgcolor="#FFCC99"><td><?php echo $pergs;?></td></tr>
                                <tr align="center"><td>
                                <?php
                                ksort($pergresp[$pergs]);
                                foreach($pergresp[$pergs] as $resp) {
                                    ?>
                                    <ul>
                                    <li style="list-style: none"><?php echo $resp;?></li>
                                    </ul>
                                    <?php
                                }
                                ?>
                                </td></tr>
                                <tr>
                                    <td align="center" style="border: 1px solid #999; width: 700px; height: 100px; overflow: auto; background-color:#EAB9B9"><?php echo $obsperg[$pergs];?></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    if($grupoperg[$idgrupo]) {
                        ksort($grupoperg[$idgrupo]);
                        foreach($grupoperg[$idgrupo] as $posip => $descrip) {
                            ?>
                            <tr align="center" bgcolor="#FFCC99"><td><?php echo $descrip;?></td></tr>
                            <tr><td align="center" style="border:1px solid #999">
                            <?php
                            ksort($pergresp[$descrip]);
                            foreach($pergresp[$descrip] as $descrir) {
                                ?>
                                <ul>
                                <li style="list-style: none"><?php echo $descrir;?></li>
                                </ul>
                                <?php
                            }
                            ?>
                            </td></tr>
                            <tr>
                                <td align="center" style="border: 1px solid #999; width: 700px; height: 100px; overflow: auto; background-color:#EAB9B9"><?php echo $obsperg[$descrir];?></td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
                <tr>
                    <td style=" height: 20px"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; background-color: #FF9900; text-align: center">OBS MONITORIA</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #999; height: 200px"><?php echo $obs;?></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>

