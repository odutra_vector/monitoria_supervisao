<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>

<?php

session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/midi.class.php');
include_once($rais.'/monitoria_supervisao/classes/class.monitoria.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");

$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);

$selauto = "SELECT idmotivo,tempo,count(*) as result FROM motivo m
            INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo WHERE vincula='automatico' AND m.ativo='S'";
$eselauto = $_SESSION['fetch_array']($_SESSION['query']($selauto)) or die (mysql_error());
if($eselauto['result'] == 1) {
    $checkmoni = "SELECT * FROM moni_pausa mp
                inner join monitor m on m.idmonitor = mp.idmonitor
                WHERE mp.idmonitor='".$_SESSION['usuarioID']."' AND data='".date('Y-m-d')."' ORDER BY idmoni_pausa DESC LIMIT 1";
    $echeck = $_SESSION['query']($checkmoni) or die ("erro na query de consulta da pausa automatica");
    $ncheck = $_SESSION['num_rows']($echeck);
    if($ncheck >= 1) {
        $lcheck = $_SESSION['fetch_array']($echeck);
        if($lcheck['status'] == "PAUSA" && $lcheck['idmotivo'] == $eselauto['idmotivo']) {
            $cont = 0;
        }
        else {
            $checkatu = "SELECT status,(time_to_sec('".date('H:i:s')."') - time_to_sec(hstatus)) as tempo FROM monitor WHERE idmonitor='".$_SESSION['usuarioID']."'";
            $echeckatu = $_SESSION['fetch_array']($_SESSION['query']($checkatu)) or die (mysql_errno());
            if($echeckatu['status'] == "AGUARDANDO" && $echeckatu['tempo'] > $eselauto['tempo']) {
                lock(array("monitor","moni_pausa","fila_grava"));
                $hora = date('H:i:s');
                $upfila = "UPDATE ".$_SESSION['tabfila']." SET monitorando='0', idmonitor=NULL WHERE id".$_SESSION['tabfila']."='".$_SESSION['idfila']."' AND idmonitor='".$_SESSION['usuarioID']."'";
                $eupfila = $_SESSION['query']($upfila) or die (mysql_error());
                $pausa = "INSERT INTO moni_pausa (idmonitor,idmotivo,data,horaini,horafim,tempo,lib_super,iduser_adm,obs) VALUES ('".$_SESSION['usuarioID']."','".$eselauto['idmotivo']."','".date('Y-m-d')."','$hora','00:00:00','00:00:00','N',NULL,NULL)";
                $epause = $_SESSION['query']($pausa) or die ("erro na query de inclusão de pausa");
                $atumoni = "UPDATE monitor SET status='PAUSA',hstatus='$hora' WHERE idmonitor='".$_SESSION['usuarioID']."'";
                $eatumoni = $_SESSION['query']($atumoni) or die ("erro na query para atualizar o status do monitor na pausa automatica");
                $cont = 0;
                unlock();
            }
            else {
                $cont = 1;
                unlock();
            }
        }
    }
    else {
        $checkatu = "SELECT status,(time_to_sec('".date('H:i:s')."') - time_to_sec(hstatus)) as tempo FROM monitor WHERE idmonitor='".$_SESSION['usuarioID']."'";
        $echeckatu = $_SESSION['fetch_array']($_SESSION['query']($checkatu)) or die (mysql_errno());
        if($echeckatu['status'] == "AGUARDANDO" && $echeckatu['tempo'] > $eselauto['tempo']) {
            lock(array("monitor","moni_pausa","fila_grava"));
            $hora = date('H:i:s');
            $upfila = "UPDATE ".$_SESSION['tabfila']." SET monitorando='0', idmonitor=NULL WHERE id".$_SESSION['tabfila']."='".$_SESSION['idfila']."' AND idmonitor='".$_SESSION['usuarioID']."'";
            $eupfila = $_SESSION['query']($upfila) or die (mysql_error());
            $pausa = "INSERT INTO moni_pausa (idmonitor,idmotivo,data,horaini,horafim,tempo,lib_super,iduser_adm,obs) VALUES ('".$_SESSION['usuarioID']."','".$eselauto['idmotivo']."','".date('Y-m-d')."','$hora','00:00:00','00:00:00','N',NULL,NULL)";
            $epause = $_SESSION['query']($pausa) or die ("erro na query de inclusão de pausa");
            $atumoni = "UPDATE monitor SET status='PAUSA',hstatus='$hora' WHERE idmonitor='".$_SESSION['usuarioID']."'";
            $eatumoni = $_SESSION['query']($atumoni) or die ("erro na query para atualizar o status do monitor na pausa automatica");
            $cont = 0;
            unlock();
        }
        else {
            $cont = 1;
            unlock();
        }
    }
}
else {
    $cont = 1;
}

if($cont == 1) {
    ?>

    <body>
        <div align="center" id="tudo">
            <?php
            $selfiltro = "SELECT nomefiltro_nomes, MIN(nivel) FROM filtro_nomes";
            $eselfiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die ("erro na query de consulta dos filtros");

            $moni = new Planilha();
            $moni->filtro = "id_".strtolower($eselfiltro['nomefiltro_nomes']);
            $participa = $_SESSION['usuarioID']."-".$_SESSION['user_tabela'];
            $selcalib = "SELECT *, COUNT(*) as result FROM agcalibragem WHERE finalizado='N' AND participantes='$participa' AND '".date('Y-m-d')."' BETWEEN dataini AND datafim ORDER BY dataini";
            $eselcalib = $_SESSION['fetch_array']($_SESSION['query']($selcalib)) or die (mysql_error());
            if($eselcalib['result'] >= 1) {
                $upfila = "UPDATE fila_grava SET monitorando='0',monitorado='0',idmonitor=NULL WHERE idfila_grava='".$_SESSION['idfila']."'";
                $eupfila = $_SESSION['query']($upfila) or die (mysql_error());
                unset($_SESSION['horaini']);
                unset($_SESSION['inimoni']);
                unset($_SESSION['fimlig']);
                unset($_SESSION['codini']);
                unset($_SESSION['codfim']);
                unset($_SESSION['idfila']);
                unset($_SESSION['tabfila']);
                $idmoni = $eselcalib['idmonitoria'];
                $selmoni = "SELECT m.idmonitoria, m.idfila, m.idplanilha, m.gravacao, m.idrel_filtros, pm.tipomoni, m.horaini, m.horafim, m.inilig, m.tmpaudio FROM monitoria m
                            INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                            INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                            WHERE idmonitoria='$idmoni'";
                $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da monitoria de calibragem");
                $_SESSION['inimoni'] = $eselmoni['horaini'];
                $_SESSION['codini'] = 1;
                $seltempo = "SELECT SEC_TO_TIME(TIME_TO_SEC('".$eselmoni['horaini']."') + TIME_TO_SEC('".$eselmoni['tmpaudio']."')) as tempo";
                $eseltempo = $_SESSION['fetch_array']($_SESSION['query']($seltempo)) or die ("erro na query de consulta do tempo total");
                $_SESSION['fimlig'] = $eseltempo['tempo'];
                $_SESSION['codfim'] = 1;
                $moni->caminho = $eselmoni['gravacao'];
                $moni->idfila = $eselmoni['idfila'];
                $moni->idcalibragem = $eselcalib['idagcalibragem'];
                if($eselmoni['tabfila'] == "fila_oper") {
                    $moni->tipomoni = "O";
                }
                else {
                    $moni->tipomoni = "G";
                }
                $moni->idplan = $eselmoni['idplanilha'];
                $pagina = "CALIBRAGEM";
            }
            else {
                $pagina = "MONITOR";
            }
            lock('monitor');
            $atumoni = "UPDATE monitor SET status='MONITORANDO', hstatus='".date('H:i:s')."' WHERE idmonitor='".$_SESSION['usuarioID']."'";
            $eatu = mysql_query($atumoni) or die (mysql_error());
            unlock();
            $eseldados = $moni->DadosPlanMoni($pagina);
            $moni->MontaPlan_dados($pagina);
            $moni->MontaCabecalho($pagina);
            if(isset($_GET['improdutivo'])) {
              $moni->Improdutivo();
            }
            else {
                //$moni->edita = "S";
                $moni->MontaPlan_corpomoni();
                $moni->MontaPlan_encerra($pagina);
            }
            ?>
            <div align="center" id="rodape">
                <table align="center" width="1024">
                    <tr>
                        <td align="center" style="color:#FFF"><strong>VECTOR DSI TEC</strong></td>
                    </tr>
                    <tr>
                        <td height="15" align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
    </html>
    <?php
    }
else {
    if($_SESSION['idfila'] != "") {
        lock($_SESSION['tabfila'],"");
        $upfila = "UPDATE ".$_SESSION['tabfila']." SET monitorando='0', idmonitor=NULL WHERE id".$_SESSION['tabfila']."='".$_SESSION['idfila']."' AND idmonitor='".$_SESSION['usuarioID']."'";
        $eupfila = $_SESSION['query']($upfila) or die (mysql_error());
    }
    unset($_SESSION['horaini']);
    unset($_SESSION['inimoni']);
    unset($_SESSION['fimlig']);
    unset($_SESSION['codini']);
    unset($_SESSION['codfim']);
    unset($_SESSION['idfila']);
    unset($_SESSION['tabfila']);
    unlock();
    header("location:/monitoria_supervisao/inicio.php");
}
?>