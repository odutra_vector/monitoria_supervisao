<?php

$tabelas = new TabelasSql();
$amoni = $tabelas->AliasTab(monitoria);
$areg = $tabelas->AliasTab(regmonitoria);
$asuper = $tabelas->AliasTab(super_oper);
$aoper = $tabelas->AliasTab(operador);
$aupload = $tabelas->AliasTab(upload);
$afilaoper = $tabelas->AliasTab(fila_oper);
$afilagrava = $tabelas->AliasTab(fila_grava);
$aup = $tabelas->AliasTab(upload);
$aconf = $tabelas->Aliastab(conf_rel);
$aparam = $tabelas->Aliastab(param_moni);
$arel = $tabelas->AliasTab(rel_filtros);
$adimenmoni = $tabelas->AliasTab(dimen_moni);
$adimenmod = $tabelas->AliasTab(dimen);
$acampos = $tabelas->AliasTab(camposparam);
$acol = $tabelas->AliasTab(coluna_oper);


$data = date('Y-m-d');
$iduser = $_SESSION['usuarioID'];
$sstatus = "SELECT status,TIME_TO_SEC(hstatus) as hstatus,(TIME_TO_SEC('".date('H:i:s')."') - TIME_TO_SEC(hstatus)) as tempo FROM monitor WHERE idmonitor='$iduser'";
$esstatus = $_SESSION['fetch_array']($_SESSION['query']($sstatus)) or die ("erro na query de consulta do status atual do monitor");
$atustatus = $esstatus['tempo'];
$status = $esstatus['status'];

?>
<script type="text/javascript">
    history.forward();
   $(document).ready(function() {
        
        <?php
        $selauto = "SELECT idmotivo,tempo,count(*) as result FROM motivo m
                    INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo WHERE vincula='automatico' AND m.ativo='S'";
        $eselauto = $_SESSION['fetch_array']($_SESSION['query']($selauto)) or die (mysql_error());
        if($eselauto['result'] == 1) {
            $checkmoni = "SELECT * FROM moni_pausa mp
                        inner join monitor m on m.idmonitor = mp.idmonitor
                        WHERE mp.idmonitor='".$_SESSION['usuarioID']."' ORDER BY idmoni_pausa DESC LIMIT 1";
            $echeck = $_SESSION['query']($checkmoni) or die ("erro na query de consulta da pausa automatica");
            $ncheck = $_SESSION['num_rows']($echeck);
            if($echeck >= 1) {
                $lcheck = $_SESSION['fetch_array']($echeck);
                if($lcheck['status'] == "PAUSA" && $lcheck['idmotivo'] == $eselauto['idmotivo']) {
                    echo "$('#tracao').hide();\n";
                    echo "$('#trbotao').hide();\n";
                }
                else {
                    echo "$('#tracao').show();\n";
                    echo "$('#trbotao').show();\n";
                }
            }
        }
        ?>
        $('#decorrido').countdown({since: -<?php echo $atustatus;?>, compact:true, format:'HMS'});
   })
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#tipomotivo').change(function() {
        $('tr#motivo').show();
    });
    
    $("#inicia").mouseenter(function() {
        $(this).css('background-color','#EAA43B');
        $(this).css('color','#FFF');
    }).mouseleave(function() {
        $(this).css('background-color','#FFFFFF');
        $(this).css('color','#000');
    });
    
    $("#validar").mouseenter(function() {
        $(this).css('background-color','#0099CC');
        $(this).css('color','#FFF');
    }).mouseleave(function() {
        $(this).css('background-color','#FFFFFF');
        $(this).css('color','#000');
    });
})
</script>
<script type="text/javascript">
$(document).ready(function() {
    $("select[name=tipomotivo]").change(function() {
        $("select[name=motivo]").html('<option value="0">Carregando...</option>');

        $.post("/monitoria_supervisao/monitor/carregamotivo.php",{tipomotivo:$(this).val()},function(valor) {
            $("select[name=motivo]").html(valor);
        })
    });
})
</script>
<body>
<div style="width:510px; float:left;">
<table width="510">
<form action="/monitoria_supervisao/monitor/atustatus.php" method="post">
  <tr>
    <td width="139" class="corfd_coltexto"><strong>STATUS</strong></td>
    <td width="359" align="center" class="corfd_colcampos"><font size="+1">
    <?php
    if($status == "PAUSA") {
        $selpausa = "SELECT motivo.idmotivo, motivo.nomemotivo FROM moni_pausa 
                    INNER JOIN motivo ON motivo.idmotivo = moni_pausa.idmotivo 
                    WHERE idmonitor='$iduser' AND horafim='00:00:00' ORDER BY data DESC, horaini DESC LIMIT 1";
        $eselpausa = $_SESSION['fetch_array']($_SESSION['query']($selpausa)) or die ("erro na query de consulta do nome da pausa");
        echo $estatus['status']." - ".$eselpausa['nomemotivo'];
    }
    else {
        $selpausa = "SELECT COUNT(*) as result FROM moni_pausa 
                    INNER JOIN motivo ON motivo.idmotivo = moni_pausa.idmotivo 
                    WHERE idmonitor='$iduser' AND horafim='00:00:00' ORDER BY horaini DESC LIMIT 1";
        $eselpausa = $_SESSION['fetch_array']($_SESSION['query']($selpausa)) or die ("erro na query de consulta das pausas");
        if($eselpausa['result'] >= 1) {
            $status = "PAUSA";
            $msgp = "VOCÊ POSSUI UMA INSCONSISTENCIA EM SUAS PAUSAS, FAVOR CONTATAR SEU SUPERVISOR!!!";
        }
        else {
        }
        echo $estatus['status'];
    }
    ?>
    </font>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>TEMPO DECOR.</strong></td>
    <td class="corfd_colcampos" align="center">
        <div id="decorrido" style="font-size:15px"></div>
    </td>
  </tr>
  <tr id="tracao">
    <td class="corfd_coltexto"><strong>AÇÃO</strong></td>
    <td align="center" class="corfd_colcampos"><select style="width:248px; text-align:center" name="tipomotivo" id="tipomotivo">
    <option disabled="disabled" selected="selected" value="">Selecione um motivo</option>
    <?php
    $selmot = "SELECT DISTINCT(tipo_motivo.nometipo_motivo), tipo_motivo.idtipo_motivo FROM motivo 
               INNER JOIN tipo_motivo ON tipo_motivo.idtipo_motivo = motivo.idtipo_motivo 
               WHERE vincula='moni_pausa'";
    $eselmot = $_SESSION['query']($selmot) or die ("erro na query de consulta dos motivos");
    while($lselmot = $_SESSION['fetch_array']($eselmot)) {
      echo "<option value=\"".$lselmot['idtipo_motivo']."\">".$lselmot['nometipo_motivo']."</option>";
    }
    ?>
    </select>
    </td>
  </tr>
  <tr id="motivo" style="display:none">
    <td class="corfd_coltexto"><strong>DETALHE</strong></td>
    <td align="center" class="corfd_colcampos"><select style="width:248px; text-align:center" name="motivo"></select></td>
  </tr>
  <tr id="trbotao">
    <td align="center" colspan="2" bgcolor="#FFFFFF"><input style="width:250px; height:30px; border: 3px solid #0099CC; font-weight: bold; background-color: #FFF; margin: 10px" name="validar" id="validar" type="submit" value="ATUALIZAR STATUS" /></td>
  </tr>
  <tr>
    <td align="center" colspan="2" bgcolor="#FFFFFF"><font color="#FF0000"><strong><?php if(isset($_GET['msgp'])) {echo $_GET['msgp'];} else {echo $msgp;}?></strong></font></td>
  </tr>
  </form>
</table>
</div>
<?php
$periodo = periodo('datas');
if($status == "PAUSA") {
}
else {
    ?>
    <div style="width:510px; float:left; background-color:#FFF;">
    <form action="/monitoria_supervisao/monitor/planmoni.php" method="post">
    <?php

    $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
    if($periodo == 0) {
    }
    else {
      if($_SESSION['idfila'] != "") {
          $tabfila = array('monitoria','regmonitoria');
          foreach($tabfila as $tab) {
            $checkfila = "SELECT count(*) as r FROM $tab WHERE idfila='".$_SESSION['idfila']."'";
            $echeckfila = $_SESSION['fetch_array']($_SESSION['query']($checkfila)) or die (mysql_error());
            if($echeckfila['r'] >= 1) {
                $fila++;
            }
          }
          if($fila >= 1) {
              unset($_SESSION['idfila']);
          }
          else {
              lock("fila_grava", "");
              $selfila = "SELECT count(*) as r FROM fila_grava WHERE idmonitor<>'".$_SESSION['usuarioID']."' AND idfila_grava='".$_SESSION['idfila']."'";
              $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die (mysql_error());
              if($eselfila['r'] >= 1) {
                  unset($_SESSION['idfila']);
              }
              else {
                  $updafila = "UPDATE fila_grava SET monitorando='1',idmonitor='".$_SESSION['usuarioID']."' WHERE idfila_grava='".$_SESSION['idfila']."'";
                  $eupfila = $_SESSION['query']($updafila) or die (mysql_error());
              }
              unlock();
          }
      }
      $data = date('Y-m-d');
      $datadia = substr($data, 7, 2);
      $datames = substr($data, 5, 2);
      $dataano = substr($data,0 , 4);
      $ultdia = ultdiames($datadia.$datames.$dataano);

      $dataini = $periodo['dataini'];
      $datafim = $periodo['datafim'];

        if($periodo['dtinictt'] == "" OR $periodo['dtfimctt'] == "") {
            $dtinictt = substr($dataini, 0,4)."-".substr($dataini, 5,2)."-01";
            $dtfimctt = ultdiames(substr($dtinictt, 8,2).substr($dtinictt, 5,2).substr($dtinictt, 0,4));
        }
        else {
            $dtinictt = $periodo['dtinictt'];
            $dtfimctt = $periodo['dtfimctt'];
        }
      
      if($_SESSION['idfila'] != "") {
          $selupload = "SELECT * FROM ".$_SESSION['tabfila']." WHERE id".$_SESSION['tabfila']."='".$_SESSION['idfila']."'";
          $eupload = $_SESSION['fetch_array']($_SESSION['query']($selupload)) or die (mysql_error());
          $idsfila[] = $eupload['idupload'];
          $idsrel[$eupload['idupload']] = $eupload['idrel_filtros'];
      }
      else {
          $feriados = getFeriados(date('Y'));
          $where[] = "AND $afilagrava.datactt BETWEEN '$dtinictt' and '$dtfimctt'";
          
          // bloqueio de tabelas para obter a ordem da fila
          $tab = array("operador $aoper","super_oper $asuper","upload $aup","periodo p",
              "interperiodo it","conf_rel $aconf","monitoria $amoni","rel_filtros $arel","camposparam $acampos","coluna_oper $acol",
              "dimen_moni $adimenmoni","param_moni $aparam","dimen_mod $adimenmod","fila_grava $afilagrava","fila_oper $afilaoper",'monitoria',"regmonitoria","regmonitoria $areg");
          lock($tab,"WRITE"); 
          
          //executa query para identificar o dimensionamento do monitor no periodo
          $dimenmoni = "select diasctt,$aup.tabfila,$afilagrava.idupload,$adimenmoni.idrel_filtros,qtdeext,qtdeoper from dimen_moni $adimenmoni
                        inner join dimen_mod $adimenmod ON $adimenmod.iddimen_mod = $adimenmoni.iddimen_mod
                        inner join upload $aup on $aup.idrel_filtros = $adimenmod.idrel_filtros
                        inner join fila_grava $afilagrava on $afilagrava.idupload = $aup.idupload
                        inner join conf_rel $aconf on $aconf.idrel_filtros = $afilagrava.idrel_filtros
                        where $adimenmoni.idmonitor='$iduser' and $afilagrava.datactt BETWEEN '$dtinictt' and '$dtfimctt' 
                        and monitorando='0' and monitorado='0' and $afilagrava.idmonitor is null and $adimenmoni.idperiodo='".$periodo['idperiodo']."' and $adimenmoni.ativo='S' and $aup.imp='S' group by $aup.idupload";
          $edimenmoni = $_SESSION['query']($dimenmoni) or die (mysql_error());
          while($ldimenmoni = $_SESSION['fetch_array']($edimenmoni)) {
              $dtini = date('Y-m-d');
              $reduz = 0;
              $idt = 0;
              
              // verifica quantos dias devem ser subtraidos da data atual para criar o limite da datactt
              for(;$idt < $ldimenmoni['diasctt'];) {
                  $menosdt = "SELECT ADDDATE('$dtini',-1) as data";
                  $emenosdt = $_SESSION['fetch_array']($_SESSION['query']($menosdt)) or die ("erro na query de verificação das datas");
                  $dtini =$emenosdt['data']; 
                  $diasem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($emenosdt['data'],5,2), substr($emenosdt['data'],8,2), substr($emenosdt['data'],0,4)));
                  if($diasem == "6" OR $diasem == "0") { // verifica se é sabado ou domingo, pois não pode contar e incfrementa 1 dia na subtração
                      $reduz++;
                  }
                  else {
                      if(in_array(substr($emenosdt['data'],5,2)."-".substr($emenosdt['data'],8,2),$feriados)) { // se a data for feriado, também incrementa 1 dia
                          $reduz++;
                      }
                      else {
                          $idt++;
                      }
                  }
              }
              $dreduz = $ldimenmoni['diasctt'] + $reduz;
              $datalim = "SELECT ADDDATE('".date('Y-m-d')."',-$dreduz) as reduz";
              $edatalim = $_SESSION['fetch_array']($_SESSION['query']($datalim)) or die ("erro na query de consulta da data limite para audios");
              $dtlim = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($edatalim['reduz'],5,2), substr($edatalim['reduz'],8,2), substr($edatalim['reduz'],0,4)));
              if($dtlim == "6") { // se a data final para subtração da data atual for um sabado, reduz mais 1 dia
                  $dreduz++;
                  $datalim = "SELECT ADDDATE('".date('Y-m-d')."',-$dreduz) as reduz";
                  $edatalim = $_SESSION['fetch_array']($_SESSION['query']($datalim)) or die ("erro na query de consulta da data limite para audios");
              }
              else {
              }
              
              // acrescenta na query de busca da fila, para identificar se existe upload disponível com os parematros necessários de data
              $limitactt[$ldimenmoni['idrel_filtros']] = "AND ".$tabelas->Aliastab($ldimenmoni['tabfila']).".datactt >= '".$edatalim['reduz']."'";
              $seldisp = "SELECT COUNT(monitorando) as monitorando, COUNT(monitorado) as monitorado FROM ".$ldimenmoni['tabfila']." ".$tabelas->Aliastab($ldimenmoni['tabfila'])." 
                          WHERE monitorando='0' and monitorado='0' and ".$tabelas->Aliastab($ldimenmoni['tabfila']).".idmonitor is null and idupload='".$ldimenmoni['idupload']."' ".$limitactt[$ldimenmoni['idrel_filtros']]." $where[0]";
              $eseldisp = $_SESSION['fetch_array']($_SESSION['query']($seldisp)) or die ("erro na query de consulta da quantidade de registros disponíveis");
              if($eseldisp['monitorando'] == 0 && $eseldisp['monitorado'] == 0) {
              }
              else {
                  if($_SESSION['tmonitor'] == "I") {
                      if($ldimenmoni['qtdeoper'] == 0) {
                            $fila[$ldimenmoni['idupload']] = $ldimenmoni['idrel_filtros'];
                      }
                      else {
                        $selqtde = "SELECT count(*) as q FROM fila_grava ".$tabelas->Aliastab($ldimenmoni['tabfila'])."
                                    LEFT JOIN monitoria $amoni ON $amoni.idoperador = ".$tabelas->Aliastab($ldimenmoni['tabfila']).".idoperador
                                    WHERE monitorando='0' and monitorado='0' and ".$tabelas->Aliastab($ldimenmoni['tabfila']).".idmonitor is null and ".$tabelas->Aliastab($ldimenmoni['tabfila']).".idupload='".$ldimenmoni['idupload']."' GROUP BY ".$tabelas->Aliastab($ldimenmoni['tabfila']).".idoperador HAVING count(distinct(idmonitoria)) < ".$ldimenmoni['qtdeoper']." LIMIT 1";
                        $eselqtde = $_SESSION['query']($selqtde) or die (mysql_error());
                        $nselqtde = $_SESSION['num_rows']($eselqtde);
                        if($nselqtde >= 1) {
                                $uploadqtde[$ldimenmoni['idupload']] = $ldimenmoni['qtdeoper'];
                                $fila[$ldimenmoni['idupload']] = $ldimenmoni['idrel_filtros'];
                        }
                      }
                  }
                  else {
                      if($ldimenmoni['qtdeext'] == 0) {  
                      }
                      else {
                            $fila[$ldimenmoni['idupload']] = $ldimenmoni['idrel_filtros'];
                      }
                  }
              }
          }
          //$fila = array_combine($iduploads, $idrelfiltros);
            foreach($fila as $idupload => $idrelfiltro) {
                //$seldatas = "SELECT * FROM upload u
                //            INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
                //            WHERE u.idupload='$idupload'";
                //$eseldatas = $_SESSION['fetch_array']($_SESSION['query']($seldatas)) or die ("erro na query de consulta das datas inicial e final do upload");
                //$iniup = $eseldatas['dataini'];
                //$fimup = $eseldatas['datafim'];
                //$verifdata = "SELECT if('$iniup' <= '$data' AND '$fimup' >= '$data',1,0) as datafora";
                //$everif = $_SESSION['fetch_array']($_SESSION['query']($verifdata)) or die ("erro na query de comparação das datas");
                //if($everif['datafora'] == 1) {
                $idsfila[] = $idupload;
                $idsrel[$idupload] = $idrelfiltro;
                //}
            }
            $idsfila = implode(",",$idsfila);
      }
      
      if($idsfila == "") {
          unlock();
      }
      else {
          for(;$ic == 0;) {
          $ordem = array_unique($idsrel);
          if($retidrel != "") {
              unset($ordem[$retidrel]);
          }
          else {
          }
          if($_SESSION['tmonitor'] == "I") {
              $tmoni = "estimativa";
          }
          else {
              $tmoni = "qtdeext";
          }
          foreach($ordem as $idrel) {
              $diaspass = array();
              if($_SESSION['tmonitor'] == "I") {
                      $datamoni = $amoni.".data BETWEEN '".$periodo['dataini']."' AND '".$periodo['datafim']."' AND $amoni.datactt BETWEEN '".$periodo['dtinictt']."' AND '".$periodo['dtfimctt']."' AND tmonitoria='I'";
              }
              else {
                  $datamoni = $amoni.".data='".date('Y-m-d')."' AND tmonitoria='E'";
              }
              
              // verfica se tem limite de meta para monitoria externa
              $sellimit = "SELECT limita_meta,$tmoni FROM conf_rel $aconf
                            INNER JOIN dimen_mod $adimenmod ON $adimenmod.idrel_filtros = $aconf.idrel_filtros AND $adimenmod.idperiodo='".$periodo['idperiodo']."'
                            WHERE $aconf.idrel_filtros='$idrel'";
              $esellimit = $_SESSION['fetch_array']($_SESSION['query']($sellimit)) or die ("erro na query de consulta do limite de meta");
              $limita = explode(",",$esellimit['limita_meta']);
              $dtloop = $dataini;
              $dt = 0;
              
              // levanta quantos dias se passaram do inicio do periodo, para calculo de meta atingida no dia
              for(;$dt == 0;) {
                  $comdt = "SELECT '$dtloop' < '$datafim' as menor, '$dtloop' = '$datafim' as igual";
                  $ecomdt = $_SESSION['fetch_array']($_SESSION['query']($comdt)) or die ("erro na query de comparação das datas");
                    if($ecomdt['menor'] >= 1 OR $ecomdt['igual'] >= 1) {
                      $verifdt = jddayofweek ( cal_to_jd(CAL_GREGORIAN, substr($dtloop,5,2),substr($dtloop,8,2), substr($dtloop,0,4)), 0);
                      if($verifdt == 0 OR $verifdt == 6) {
                      }
                      else {
                          $compatu = "SELECT '$dtloop' > '".date('Y-m-d')."' as dtatu";
                          $ecompatu = $_SESSION['fetch_array']($_SESSION['query']($compatu)) or die ("erro na query de comparação da data com a data atual");
                          if($ecompatu['dtatu'] >= 1) {
                          }
                          else {
                            $diaspass[] = $dtloop;
                          }
                      }
                      if($ecomdt['menor'] == 1) {
                          $dt = 0;
                          $adddt = "SELECT ADDDATE('$dtloop',interval 1 day) as data";
                          $eadddt = $_SESSION['fetch_array']($_SESSION['query']($adddt)) or die ("erro na query para acrescentar 1 dia na data");
                          $dtloop = $eadddt['data'];
                      }
                      if($ecomdt['igual'] == 1) {
                          $dt = 1;
                      }
                  }
                  else {
                  }
              }
              $seldias = "SELECT DATEDIFF('".date('Y-m-d')."','$dtinictt') as dif";
              $eseldias = $_SESSION['fetch_array']($_SESSION['query']($seldias)) or die ("erro na query de consulta da quatidades de dias decorridos");
              $diasdif = ($eseldias['dif'] + 1);
              $ano = date('Y');
              $diasfer = getFeriados($ano);
              $diasmes = $periodo['diasprod'];
              foreach($diasfer as $d) {
                  $comp = "SELECT '$ano-$d' BETWEEN '$dtinictt' AND '".date('Y-m-d')."' as result";
                  $ecomp = $_SESSION['fetch_array']($_SESSION['query']($comp)) or die ("erro na query de comparação da datas");
                  if($ecomp['result'] >= 1) {
                      $diasdif--;
                  }
                  else {
                  }
              }
              if($_SESSION['tmonitor'] == "I") {// se monitor interno, verificar se existe limite de meta alcançada no dia e identfica se o relacionado deve continuar para verificação da fila
                  $selrelperc = "SELECT COUNT(DISTINCT(idmonitoria)) as qtde  FROM monitoria $amoni
                                        WHERE $amoni.idrel_filtros='$idrel' AND $datamoni";
                  $eselperc = $_SESSION['fetch_array']($_SESSION['query']($selrelperc)) or die ("erro na query de calculo do percentual de meta atingida");
                  $meta = (($esellimit[$tmoni] / $diasmes) * count($diaspass));
                  $perc = round((($eselperc['qtde'] / $meta) * 100),5);
                  if($limita[0] == "S") {
                      if($perc > $limita[1]) {
                      }
                      else {
                            $ordenafila[$idrel] = $perc;
                      }
                  }
                  else {
                        $ordenafila[$idrel] = $perc;
                  }
              }
              else {
                    $selrelperc = "SELECT COUNT(DISTINCT(idmonitoria)) as qtde  FROM monitoria $amoni
                                   WHERE $amoni.idrel_filtros='$idrel' AND $datamoni";
                    $eselperc = $_SESSION['fetch_array']($_SESSION['query']($selrelperc)) or die ("erro na query de calculo do percentual de meta atingida");
                    $meta = ($esellimit[$tmoni] / $diasmes);
                    $perc = round(($eselperc['qtde'] / $meta) * 100);
                    if($perc >= "100") {
                    }
                    else {
                        $ordenafila[$idrel] = $perc;
                    }
              }
          }
          asort($ordenafila);
          
          if(empty($ordenafila)) {
              $ic = 1;
              $nverif = 0;
              break;
          }
          else {
              foreach($ordenafila as $o => $oidrel) {
                    $verifordem = "SELECT COUNT(*) as result FROM fila_grava ".$tabelas->AliasTab(fila_grava)." where monitorando='0' and monitorado='0' and idmonitor is null and idupload IN ('".implode("','",array_keys($idsrel,$o))."')";
                    $everifordem = $_SESSION['fetch_array']($_SESSION['query']($verifordem)) or die (mysql_error());
                    if($everifordem['result'] >= 1) {
                        $idordem = $o;
                        $idupload = implode("','",array_keys($idsrel,$o));
                        break;
                    }
              }

              if($_SESSION['idfila'] != "") {
                  $idordem = current(array_unique($idsrel));
              }
              else {
              }
              $paramfila = "SELECT * FROM param_moni $aparam
                            INNER JOIN conf_rel $aconf ON $aconf.idparam_moni = $aparam.idparam_moni
                            INNER JOIN rel_filtros $arel ON $arel.idrel_filtros = $aconf.idrel_filtros
                            WHERE $arel.idrel_filtros='$idordem'";
              $eparam  = $_SESSION['fetch_array']($_SESSION['query']($paramfila)) or die ("erro na query de consulta dos parametros do relacionamento");
              $filtrosfila['dataimp'] = explode(",",$eparam['filtro_dataimp']);
              $filtrosfila['ultmoni'] = explode(",",$eparam['filtro_ultmoni']);
              $filtrosfila['datafimimp'] = explode(",",$eparam['filtro_datafimimp']);
              $filtrosfila['diasimp'] = explode(",",$eparam['filtro_dultimp']);
              $filtrosfila['limmulti'] = explode(",",$eparam['filtro_limmulti']);
              $filtrosfila['ordemctt'] = explode(",",$eparam['ordemctt']);
              $semdados = $eparam['semdados'];
              if($filtrosfila['dataimp'][0] == "S") {
                  $orderfg[$filtrosfila['dataimp'][1]] = "$aup.dataimp DESC";
                  $orderfo[$filtrosfila['dataimp'][1]] = "$aup.dataimp DESC";
              }
              if($filtrosfila['ultmoni'][0] == "S") {
                  $orderfg[$filtrosfila['ultmoni'][1]] = "$amoni.data";
                  $orderfo[$filtrosfila['ultmoni'][1]] = "$amoni.data";
              }
              if($filtrosfila['datafimimp'][0] == "S") {
                  if($eparam['tipomoni'] == "O") {
                  }
                  else {
                    $dias = $filtrosfila['diasimp'][0];
                    $consdult = "SELECT dataimp FROM upload $aup WHERE idrel_filtros='$idordem' ORDER BY idupload DESC LIMIT 1";
                    $econsdsult = $_SESSION['query']($consdult) or die ("erro na query para consulta o ultimo ulpoad");
                    $nconsult = $_SESSION['num_rows']($econsdsult);
                    if($nconsult >= 1) {
                        $lconsult = $_SESSION['fetch_array']($econsdsult);
                        if($lconsult['dataimp'] == "0000-00-00") {

                        }
                        else {
                            $dult = mktime(0,0,0,substr($lconsult['dataimp'],5,2),substr($lconsult['dataimp'],8,2),substr($lconsult['dataimp'],0,4));
                            for($i = 0; $i < $dias; $i++) {
                                $dult -= 86400;
                            }
                            $dult = date('Y-m-d',$dult);
                            $whereimp[$filtrosfila['datafimimp'][0]] = "$afilagrava.datactt >= '$dult'";
                        }
                    }
                    else {
                    }
                  }
              }
              if($whereimp != "") {
                  $wheresql = "AND ".implode(",",$whereimp);
              }
              if($eparam['tipomoni'] == "O") {
                $tabela = "fila_oper";
                $aliastab = $afilaoper;
                if($orderfo == "") {
                }
                else {
                    $orderby = "ORDER BY ".implode(",",$orderfo);
                }
              }
              if($eparam['tipomoni'] == "G") {
                $tabela = "fila_grava";
                $aliastab = $afilagrava;
                if($filtrosfila['ordemctt'][0] == "S") {
                    $ordem = array('C' => 'ASC','D' => 'DESC');
                    if($orderfg == "") {
                        // alterado a ordenação de ctt mais antigo para ctt mais novo 13/12/2011
                        $orderby = "ORDER BY $aliastab.datactt ".$ordem[$filtrosfila['ordemctt'][1]]."";
                    }
                    else {
                        // alterado a ordenação de ctt mais antigo para ctt mais novo 13/12/2011
                        $orderby = "ORDER BY $aliastab.datactt ".$ordem[$filtrosfila['ordemctt'][1]].",".implode(",",$orderfg);
                    }
                }
                else {
                    if($orderfg == "") {
                    }
                    else {
                        $orderby = "ORDER BY".implode(",",$orderfg);
                    }
                }
              }
            $selcampos = "SELECT * FROM camposparam $acampos INNER JOIN coluna_oper $acol ON $acol.idcoluna_oper = $acampos.idcoluna_oper WHERE $acampos.idparam_moni='".$eparam['idparam_moni']."' ORDER BY posicao";
            $eselcampos = $_SESSION['query']($selcampos) or die ("erro na consulta dos campos parametrizados");
            while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
              if($lselcampos['visumoni'] == "S") {
                if($lselcampos['incorpora'] == "OPERADOR") {
                    if($lselcampos['classifica'] == "S") {
                        $columclass[] = "$aoper.".$lselcampos['nomecoluna'];
                    }
                    $visucol[] = $lselcampos['nomecoluna'];
                    $coluna[] = "$aoper.".$lselcampos['nomecoluna'];

                }
                if($lselcampos['incorpora'] == "SUPER_OPER") {
                    if($lselcampos['classifica'] == "S") {
                        $columclass[] = "$asuper.".$lselcampos['nomecoluna'];
                    }
                    $visucol[] = $lselcampos['nomecoluna'];
                    $coluna[] = "$asuper.".$lselcampos['nomecoluna'];
                    $coluna[] = $aliastab.".idsuper_oper";
                }
                if($lselcampos['incorpora'] == "DADOS") {
                    if($lselcampos['classifica'] == "S") {
                        $columclass[] = $aliastab.".".$lselcampos['nomecoluna'];
                    }
                    $visucol[] = $lselcampos['nomecoluna'];
                    $coluna[] = $aliastab.".".$lselcampos['nomecoluna'];
                }
              }
              else {
                    if($lselcampos['classifica'] == "S") {
                        $columclass[] = "$aoper.".$lselcampos['nomecoluna'];
                    }
              }
            }
            $coluna[] = "$aconf.idplanilha_moni";
            if($eparam['tipomoni'] == "O") {
                $coluna[] = "$afilaoper.idfila_oper";
                $coluna[] = "$afilaoper.idrel_filtros";
                $coluna[] = "$afilaoper.idoperador";
                $coluna[] = "p.dataini";
                $coluna[] = "p.datafim";
                $coluna[] = "$afilaoper.qtdmoni";
                $coluna[] = "$aconf.idfluxo";
            }
            if($eparam['tipomoni'] == "G") {
                $coluna[] = "$afilagrava.idfila_grava";
                $coluna[] = "$afilagrava.idrel_filtros";
                $coluna[] = "$afilagrava.idoperador";
                $coluna[] = "p.dataini";
                $coluna[] = "p.datafim";
                $coluna[] = "$aconf.idfluxo";
            }
            $colsql = implode(",",$coluna);
            $class = implode(",",$columclass);

            if($_SESSION['idfila'] != "") {
                if($semdados == "S") {
                    $verifila = "SELECT * FROM $tabela $aliastab
                                INNER JOIN upload $aup ON $aup.idupload = $aliastab.idupload
                                INNER JOIN periodo p ON p.idperiodo = $aup.idperiodo
                                INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $aliastab.idrel_filtros
                                WHERE $aliastab.id$tabela='".$_SESSION['idfila']."'";
                }
                else {
                     $verifila = "SELECT $colsql,$aliastab.arquivo FROM operador $aoper
                                INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $aoper.idsuper_oper
                                LEFT JOIN $tabela $aliastab ON $aliastab.idoperador = $aoper.idoperador
                                INNER JOIN upload $aup ON $aup.idupload = $aliastab.idupload
                                INNER JOIN periodo p ON p.idperiodo = $aup.idperiodo
                                INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $aliastab.idrel_filtros
                                WHERE $aliastab.id$tabela='".$_SESSION['idfila']."'";
                }
            }
            else {
                if($uploadqtde[$idupload] >= 1) {
                    $group = "GROUP BY $aliastab.idoperador HAVING count(distinct(idmonitoria)) < $uploadqtde[$idupload]";
                }
                else {
                    $group = "";
                }
                if($semdados == "S") {
                    $verifila = "SELECT * FROM $tabela $aliastab
                                INNER JOIN upload $aup ON $aup.idupload = $aliastab.idupload
                                INNER JOIN periodo p ON p.idperiodo = $aup.idperiodo
                                INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $aliastab.idrel_filtros
                                WHERE monitorando='0' AND monitorado='0' AND $aliastab.idmonitor is null AND $aliastab.idupload IN ('$idupload') $where[0] $limitactt[$idordem] $wheresql $orderby LIMIT 1";
                }
                else {
                    $verifila = "SELECT $colsql,$aliastab.arquivo FROM $tabela $aliastab
                                INNER JOIN operador $aoper ON $aoper.idoperador = $aliastab.idoperador
                                INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $aliastab.idsuper_oper
                                INNER JOIN upload $aup ON $aup.idupload = $aliastab.idupload
                                INNER JOIN periodo p ON p.idperiodo = $aup.idperiodo
                                INNER JOIN conf_rel $aconf ON $aconf.idrel_filtros = $aliastab.idrel_filtros
                                LEFT JOIN monitoria $amoni ON $amoni.idoperador = $aliastab.idoperador
                                WHERE monitorando='0' AND monitorado='0' AND $aliastab.idmonitor is null AND $aliastab.idupload IN('$idupload') $where[0] $limitactt[$idordem] $wheresql $group $orderby LIMIT 1";
                }
            }
            $everiffila = $_SESSION['query']($verifila) or die (mysql_error());
            $nverif = $_SESSION['num_rows']($everiffila);
            if($nverif >= 1) {
                unset($retidrel);
                $lveriffila = $_SESSION['fetch_array']($everiffila);
                $checkmoni = "SELECT COUNT(*) as result FROM $tabela $aliastab WHERE id$tabela='".$lveriffila['id'.$tabela]."' and (monitorando='1' or monitorado='1') AND idmonitor<>'".$_SESSION['usuarioID']."'";
                $echeckmoni = $_SESSION['fetch_array']($_SESSION['query']($checkmoni)) or die (mysql_error());
                if($echeckmoni['result'] >= 1) {
                    $ic = 0;
                    $_SESSION['idfila'] = "";
                    unlock() ;
                }
                else {
                    $ic = 1;
                    if($_SESSION['idfila'] == "") {
                        //lock("$tabela $aliastab");
                        $updatefila = "UPDATE $tabela $aliastab SET monitorando='1', idmonitor='".$_SESSION['usuarioID']."' WHERE id$tabela='".$lveriffila['id'.$tabela]."'";
                        $upfila = $_SESSION['query']($updatefila) or die (mysql_error());
                        $limpa = "UPDATE $tabela $aliastab SET monitorando='0', idmonitor=NULL WHERE id$tabela<>'".$lveriffila['id'.$tabela]."' AND monitorando='1' and monitorado='0' AND idmonitor='".$_SESSION['usuarioID']."'";
                        $elimpa = $_SESSION['query']($limpa) or die (mysql_error());
                        unlock();
                    }
                    else {
                        //lock("$tabela $aliastab");
                        $updatefila = "UPDATE $tabela $aliastab SET monitorando='1', idmonitor='".$_SESSION['usuarioID']."' WHERE id$tabela='".$_SESSION['idfila']."'";
                        $upfila = $_SESSION['query']($updatefila) or die (mysql_error());
                        $limpa = "UPDATE $tabela $aliastab SET monitorando='0', idmonitor=NULL WHERE id$tabela<>'".$_SESSION['idfila']."' AND monitorando='1' and monitorado='0' AND idmonitor='".$_SESSION['usuarioID']."'";
                        $elimpa = $_SESSION['query']($limpa) or die (mysql_error());
                        unlock();
                    }
                    $_SESSION['idfila'] = $lveriffila['id'.$tabela];
                    $_SESSION['tabfila'] = $tabela;
                    $conffluxo = "SELECT COUNT(*) as result FROM rel_fluxo WHERE idfluxo='".$lveriffila['idfluxo']."' AND tipo='inicia'";
                    $econf = $_SESSION['fetch_array']($_SESSION['query']($conffluxo)) or die ("erro na query de consulta da configuração do fluxo");
                    $pmoni = explode(",",$lveriffila['idplanilha_moni']);
                    foreach($pmoni as $pla) {
                        $verifplan = "SELECT DISTINCT(idplanilha), COUNT(*) as result FROM planilha WHERE idplanilha='$pla' AND ativomoni='S'";
                        $everifplan = $_SESSION['fetch_array']($_SESSION['query']($verifplan));
                        if($everifplan['result'] >= 1) {
                            $planmoni[] = $everifplan['idplanilha'];
                        }
                        else {
                        }
                    }
                    foreach($visucol as $visu) {
                      $seltipodado = "SELECT * FROM coluna_oper WHERE nomecoluna='$visu'";
                      $etipodado = $_SESSION['fetch_array']($_SESSION['query']($seltipodado)) or die ("erro na query de consulta do tipo do dado");
                      if($etipodado['tipo'] == "DATE") {
                        $visudados[] = banco2data($lveriffila[$visu]);
                      }
                      else {
                        $visudados[] = $lveriffila[$visu];
                      }
                    }
                    $visucoldados = array_combine($visucol, $visudados);
                }
            }
            else {
                $retidrel = $idordem;
                unlock();
            }
          }
      }
      }
      unlock();
    }
}
?>
<table width="510">
    <tr>
           <td align="center" colspan="2" class="corfd_ntab"><strong>PRÓXIMA MONITORIA</strong>
          <input name="tipomoni" type="hidden" value="<?php echo $eparam['tipomoni'];?>"/><input name="semdados" type="hidden" value="<?php echo $semdados;?>"/><input name="idfila" type="hidden" value="<?php echo $lveriffila['id'.$tabela];?>"/></td>
    </tr>
<?php
if($periodo == 0) {
  ?>
    <tr>
        <td width="120" class="corfd_colcampos" colspan="2" align="center"><strong><font color="#FF0000">NÃO HÁ PERÍODO CADASTRADO PARA A DATA ATUAL, FAVOR CONTATAR O ADMINISTRADOR!!!</font></strong></td>
    </tr>
   <?php
}
else {
    if($status == "PAUSA") {
        ?>
        <tr>
          <td width="120" class="corfd_colcampos" colspan="2" align="center" style="color:#F00"><strong>MONITOR EM PAUSA</strong></td>
        </tr>
    <?php
    }
    else {
        if($idsfila == "" OR $nverif == 0) {
            ?>
            <tr>
              <td width="120" class="corfd_colcampos" colspan="2" align="center"><strong><font color="#FF0000">NÃO EXISTEM MONITORIAS À REALIZAR COM BASE NOS PAREMETROS DAS OPERAÇÕES DIMENSIONADAS AO MONITOR, POR FAVOR CONTATE SEU SUPERVISOR!!!</font></strong></td>
            </tr>
            <?php
        }
        else {
            if($econf['result'] == 0) {
                ?>
                <tr>
                  <td width="120" class="corfd_colcampos" colspan="2" align="center"><strong><font color="#FF0000">A MONITORIA À SER REALIZADA NÃO POSSUI FLUXO DE MONITORIA VINCULADO, FAVOR CONTATAR O ADMINISTRADOR!!!</font></strong></td>
                </tr>
            <?php
            }
            else {
                if($semdados == "S") {
                    ?>
                    <tr>
                        <td width="120" class="corfd_coltexto"><strong><?php echo 'datactt';?></strong></td>
                        <td width="230" class="corfd_colcampos"><?php echo $visucoldados['datactt'];?></td>
                    </tr>
                    <?php
                }
                else {
                    foreach($visucoldados as $coluna => $dados) {
                        ?>
                        <tr>
                            <td width="120" class="corfd_coltexto"><strong><?php echo $coluna;?></strong></td>
                            <td width="230" class="corfd_colcampos"><?php echo $dados;?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr>
                        <td colspan="2"><a target="_blank" href="<?php echo $lveriffila['arquivo'];?>">AUDIO</a></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td width="350" colspan="2" class="corfd_coltexto" align="center"><strong>PLANILHAS</strong></td>
                </tr>
                <?php
                if($planmoni == "") {
                    ?>
                    <tr>
                      <td width="350" colspan="2" class="corfd_colcampos" align="center" style="color:#F00"><strong>NÃO EXISTEM PLANILHAS ATIVAS VINCULADAS AO RELACIONAMENTO, FAVOR CONTATAR O SUPERVISOR</strong></td>
                    </tr>
                    <?php
                }
                if(count($planmoni) == 1) {
                    foreach($planmoni as $plan) {
                      $nplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$plan'";
                      $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die ("erro na query de consulta da planilha");
                      ?>
                      <tr>
                        <td width="350" colspan="2" class="corfd_colcampos" align="center"><?php echo $enplan['descriplanilha'];?></td>
                      </tr>
                      <?php
                    }
                }
                if(count($planmoni) > 1) {
                  foreach($planmoni as $plan) {
                  ?>
                    <tr>
                    <?php
                    $nplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$plan'";
                    $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die ("erro na query de consulta da planilha");
                    if(isset($_GET['planmoni'])) {
                      if($plan == $_GET['planmoni']) {
                          $color ="bgcolor=\"#99CCFF\"";
                      }
                      else {
                          $color = "bgcolor=\"#FFFFFF\"";
                      }
                    }
                    else {
                        $color = "bgcolor=\"#FFFFFF\"";
                    }
                    ?>
                    <td width="350" colspan="2" <?php echo $color;?> align="center">
                    <?php
                    if(isset($_GET['planmoni'])) {
                      if($plan == $_GET['planmoni']) {
                          echo $enplan['descriplanilha']."</td>";
                      }
                      else {
                           echo "<strong><a style=\"text-decoration:none; color:#000\" href=\"inicio.php?menu=sistema&planmoni=$plan\">".$enplan['descriplanilha']."</a></strong></td>";
                      }
                    }
                    else {
                        echo "<strong><a style=\"text-decoration:none; color:#000\" href=\"inicio.php?menu=sistema&planmoni=$plan\">".$enplan['descriplanilha']."</a></strong></td>";
                    }
                    ?>
                    </tr>
                    <?php
                  }
                }
            }
        }
    }
}
?>
<tr>
<?php
  if($idsfila != "" && $nverif != 0 && $econf['result'] != 0 && $planmoni != "") {
      ?>
	<td colspan="2" align="center">
        <?php
	if(count($planmoni) > 1) {
            ?>
            <input type="hidden" name="idplan" value="<?php echo $_GET['planmoni'];?>">
            <?php
	}
	if(count($planmoni) == 1) {
            ?>
            <input type="hidden" name="idplan" value="<?php echo $lveriffila['idplanilha_moni'];?>" />
            <?php
	}
	if($eselproximo['idplanilha_moni'] == "") {
	}
        ?>
	<input style="width:250px; height: 30px; background-color: #FFFFFF; font-weight: bold; border: 3px solid #EAA43B; margin: 5px" name="inicia" id="inicia" type="submit" value="INICIAR MONITORIA"/></td>
        <?php
    }
    else {
    }
    ?>
</tr>
<tr>
  <td align="center" colspan="2"><font color="#FF0000"><strong><?php echo $_GET['msg'];?></strong></font></td>
</tr>
</table>
<?php
if($status == "PAUSA") {

}
else {
    ?>
    </form></div>
    <?php
}
?>
</body>
