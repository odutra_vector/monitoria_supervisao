<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

$idfila = $_POST['idfila'];
$idrel = $_POST['idrel'];
$tipo = $_POST['tipo'];

if($tipo == "G") {
    $selper = "SELECT * FROM fila_grava fg WHERE idfila_grava='$idfila'";
    $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta das datas da fila");
    $dataini = $eselper['dataini'];
    $datafim = $eselper['datafim'];
    $idrelfila = $eselper['idrel_filtros'];
    $filtros = "SELECT COUNT(*) as result, nomefiltro_nomes FROM filtro_nomes WHERE trocafiltro='S'";
    $efiltros = $_SESSION['query']($filtros) or die ("erro na query de consulta dos filtros");
    while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
      $nfiltros[] = "id_".strtolower($lfiltros['nomefiltro_nomes']);
      $f++;
    }
    $selfiltros = "SELECT * FROM rel_filtros WHERE idrel_filtros='$idrelfila'";
    $eselfiltros = $_SESSION['fetch_array']($_SESSION['query']($selfiltros)) or die ("erro na query de consulta dos filtros");

    $idfiltros = array();
    foreach($nfiltros as $nfiltro) {
        $col = $nfiltro;
        $idfiltros[] = $eselfiltros[$col];
    }

    $sql = array_combine($nfiltros, $idfiltros);
    $sqlquery = array();
    if(count($sql) == 1) {
      foreach($sql as $key => $value) {
        $sqlquery[] = $key."='".$value."'";
      }
    }
    if(count($sql) > 1) {
      foreach($sql as $key => $value) {
        $sqlquery[] = $key."='".$value."' AND";
      }
    }

    $sqlquery = implode(",",$sqlquery);

    $ultfilt = "SELECT *, MIN(nivel) as nivel FROM filtro_nomes";
    $eult = $_SESSION['fetch_array']($_SESSION['query']($ultfilt)) or die ("erro na query para obter o menor filtro");
    $ult = "id_".strtolower($eult['nomefiltro_nomes']);
    $selrel = "SELECT idrel_filtros FROM rel_filtros WHERE $sqlquery";
    $eselrel = $_SESSION['query']($selrel) or die ("erro na query dos filtros");
    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
        $idsrel[] = $lselrel['idrel_filtros'];
    }
    $seloper = "SELECT fg.idoperador, o.operador FROM fila_grava fg INNER JOIN operador o ON o.idoperador = fg.idoperador WHERE fg.idrel_filtros IN ('".implode("','",$idsrel)."') AND fg.dataini BETWEEN '$dataini' AND '$datafim' AND fg.datafim BETWEEN '$dataini' AND '$datafim'";
    $selatu = "SELECT o.idoperador, o.operador FROM fila_grava fg INNER JOIN operador o ON o.idoperador = fg.idoperador WHERE fg.idfila_grava='$idfila'";
    $eselatu = $_SESSION['fetch_array']($_SESSION['query']($selatu)) or die ("erro na query para consulta do nome do operador da fila");
    echo "<option value=\"".$eselatu['idoperador']."\" selected=\"selected\">".$eselatu['operador']."</option>";
}
if($tipo == "O") {
    $selper = "SELECT * FROM fila_oper fg WHERE idfila_oper='$idfila'";
    $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta das datas da fila");
    $dataini = $eselper['dataini'];
    $datafim = $eselper['datafim'];
    $idrelfila = $eselper['idrel_filtros'];
    $seloper = "SELECT fo.idoperador, o.operador FROM fila_oper fo INNER JOIN operador o ON o.idoperador = fo.idoperador WHERE fo.idrel_filtros='$idrel' AND fo.dataini BETWEEN '$dataini' AND '$datafim' AND fo.datafim BETWEEN '$dataini' AND '$datafim'";
    $selatu = "SELECT o.idoperador, o.operador FROM fila_oper fo INNER JOIN operador o ON o.idoperador = fo.idoperador WHERE fo.idfila_oper='$idfila'";
    $eselatu = $_SESSION['fetch_array']($_SESSION['query']($selatu)) or die ("erro na query para consulta do nome do operador da fila");
    echo "<option value=\"".$eselatu['idoperador']."\" selected=\"selected\">".$eselatu['operador']."</option>";
}
$eseloper = $_SESSION['query']($seloper) or die ("erro na query para consultar os operadores importados neste filtros");
while($lseloper = $_SESSION['fetch_array']($eseloper)) {
    if($lseloper['idoperador'] == $eselatu['idoperador']) {
    }
    else {
        echo "<option value=\"".$lseloper['idoperador']."\">".$lseloper['operador']."</option>";
    }
}

?>
