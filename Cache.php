<script type="text/javascript">
    var chart;
    $(document).ready(function() {
       chart = new Highcharts.Chart({
          chart: {
             renderTo: 'grafconfianca',
             marginTop: 120,
             marginBottom: 250,
             width: 1024,
             height: 700
          },
          title: {
             text: 'INTERVALO DE CONFIANÇA'
          },
          credits: {
              text: 'extracted: 23/10/2011-13:49:55 | range: 01/10/2011-15/10/2011',
              href: 'http://www.clientis.com.br'
          },
          xAxis: {
             labels: {
                 style: {
                     color: '#000000'
                 },
                 rotation: 270,
                 y: 100
              },
             categories: [
                'ATENDEBEM','TIVIT_SC','TMKT'                         ]
          },
          yAxis: {
             labels: {
                enabled: false
             },
             min: 70,
             max: 105
          },
          legend: {
             layout: 'horizontal',
             backgroundColor: '#FFFFFF',
             align: 'center',
             verticalAlign: 'top',
             y:40,
             floating: true,
             shadow: true
          },
          plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        color: '#000',
                        style: {
                            fontSize: '13px'
                        }
                    },
                    stacking: 'normal'
                }
            },
          tooltip: {
             formatter: function() {
                return ''+
                   this.x +': '+ this.y;
             }
          },
           series: [
               {name:'intervalo máximo',type:'scatter',data:[95.7,94.2,97.5],dataLabels: {fontWeight: 'bold'},color: '#FF0000',marker: {radius: 5,symbol: 'triangle'}},{name:'media',type:'column',data:[95.2,93.5,96.7],dataLabels: { enabled: false },marker: {radius: 5,symbol: 'triangle'}},{name:'intervalo mínimo',type:'scatter',data:[94.7,92.7,96],dataLabels: {fontWeight: 'bold',y: 17,color: '#FFFFFF'},color: '#FF0000',marker: {radius: 5,symbol: 'triangle-down'}}                         ],
             exporting: {
                enableImages: true
             }
       },
       function(chart) {
           chart.renderer.image('http://localhost/monitoria_supervisao/images/logo/logo_itau.jpg', 40, 30, 50, 50).add();
       });
    });
</script>