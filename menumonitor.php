<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');
$seluser = "SELECT * FROM monitor WHERE idmonitor='".$_SESSION['usuarioID']."'";
$eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
$selresp = "SELECT * FROM user_adm WHERE iduser_adm='".$eseluser['iduser_resp']."'";
$eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
$iduser = $_SESSION['usuarioID'];
$selstatus = "SELECT status FROM monitor WHERE idmonitor='$iduser'";
$estatus = $_SESSION['fetch_array']($_SESSION['query']($selstatus));

$datalog = date('Y-m-d');
$hlogin = "SELECT (TIME_TO_SEC('".date('H:i:s')."') - TIME_TO_SEC(MAX(login))) + (TIME_TO_SEC(MAX(login)) - TIME_TO_SEC(MIN(login))) as tempo FROM moni_login m WHERE idmonitor='$iduser' AND data='$datalog' ORDER BY idmoni_login DESC";
$ehlogin = $_SESSION['fetch_array']($_SESSION['query']($hlogin)) or die ("erro na consulta do tempo logado");
$horalogin = $ehlogin['tempo'];

$cor = new CoresSistema();
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'], 'SISTEMA');

?>

<link href="style.css" rel="stylesheet" type="text/css" />
<link href="stylemenuini.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<link type="text/css" href="/monitoria_supervisao/js/jquery.countdown.css" rel="stylesheet"/>
<script src="/monitoria_supervisao/js/jquery.countdown.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/thickbox/thickbox.js"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/thickbox/thickbox.css" type="text/css" />

<script type="text/javascript">
    $(document).ready(function() {        
        setInterval(function() {
            location.reload();
        },2000000);
        
        $("body").click(function(event) {
            var id = event.target.id;
            if(id != "auser") {
                $("#dduser").hide();
            }
        });
        
        $("#auser").click(function() {
            if($("#dduser").is(":visible")) {
                $("#dduser").animate({opacity: "hide"}, "fast");
                
            }
            else {
                $("#dduser").animate({opacity: "show"}, "fast");
            }
        });
        
        $('#logado').countdown({since: -<?php echo $horalogin;?>, compact:true, format:'HMS'});
        
        $('li a').click(function() {
            var val = $(this).attr('id');
            var a = val.substr(1);
            $('li.monitor').css('background-color','#264BDE');
            $('#'+a).css('background-color','#264BDE');
        });
        
        $("#logout").click(function() {
           $.post("/monitoria_supervisao/logout.php",function(ret) {
                window.location = '/monitoria_supervisao/index.php?msg='+ret;
           }); 
        });
    })
</script>
<div id="menuleft">
    <div id="divcabecalho" style="background-image: url(images/logo/clientis_<?php echo strtolower($_SESSION['nomecli']).".png)";?>">
        <ul id="cabecalho">
            <li style="text-align: left; font-size: 16px;margin: 5px;">
                <img name="user" src="/monitoria_supervisao/images/user.png" width="35px" height="35px" id="auser"/>
                <div id="dduser" class="divrodape">
                    <span style="color: #F90; font-size: 13px; font-weight: bold">Informações do Usuário</span><br/><br/>
                    <span style="color: #F90; font-weight: bold">Nome:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $_SESSION['usuarioNome'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Entrada:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $eseluser['entrada'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Saida:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $eseluser['saida'];?></span><br/>
                    <span style="color:#F90; font-weight: bold">Resp:</span> <span style="color: #FFF;margin-left: 10px"><?php echo $eselresp['nomeuser_adm'];?></span><br/>
                    <div><div style="float:left"><span style="color:#F90; font-weight: bold">Logado: </span></div> <div id="logado" style="font-size:13px; color: #FFF; background-color: #000; width: 100px; float: left; margin-left: 10px"></div></div>
                </div>
            </li>
            <li style="text-align: center; font-size: 16px; width:48%; margin: 0px; margin:auto; text-align: center"><img src="/monitoria_supervisao/images/img_logo.jpg" width="70px" height="35px" /></li>
            <li style="text-align: right;"><img name="logout" id="logout" src="/monitoria_supervisao/images/sair.png" width="35px" height="35px" style=" border: 0px; padding: 0px" /></li>
        </ul>
    </div>
    <div style="float:right; width: 1024px; text-align: center">
        <?php
        if($_SESSION['tipotopo'] == "") {
        }
        if($_SESSION['tipotopo'] == "BANNER") {
        ?>
    	<img src="images/logo/<?php echo $_SESSION['imgbanner'];?>" alt="logo" />
        <?php
        }
        if($_SESSION['tipotopo'] == "ORIENTACAO") {
        }
        if($_SESSION['tipotopo'] == "META") {
        }
        ?>
    </div>
</div>
<div style="width:1024px; height:20px; float: left;">
    
</div>
<div style="width:1024px; height:25px; padding-bottom: 10px">
    <ul id="monitor">
        <!--<a id="achat" style="color:#FFF; text-decoration: none" href="#" target="_blank"><li id="chat" class="monitor" ><strong>CHAT</strong></li></a>-->
        <a id="aebook" style="color:#FFF; text-decoration: none" href="monitor/ebook.php" target="_blank"><li id="ebook" class="monitor" style="border-top-left-radius: 10px;border-bottom-right-radius: 10px;" ><strong>E-BOOK</strong></li></a>
    </ul>
</div>
<?php
if($_GET['menu'] == "ebook") {
    echo "<div style=\"padding:10; height:140px\" align=\"center\">";
    echo "<table bgcolor=\"#CCCCCC\" align=\"center\" width=\"248\">";
    echo "<tr>";
    echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><strong>ULTIMAS ORIENTAÇÕES</strong></td>";
    echo "</tr>";
    echo "</table>";
    echo "<marquee bgcolor=\"#CCCCCC\" behavior=\"scroll\" direction=\"up\" height=\"90\" width=\"248px\" align=\"middle\" onmousemove=\"this.stop()\" onmouseout=\"this.start()\" scrollamount=\"3\">";
    $selparam = "SELECT * FROM param_book";
    $eparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
    $data = date('Y-m-d');
    $dtinidb_ano = substr($data,0,4);
    $dtinidb_mes = substr($data,5,2);
    $dtinidb_dia = substr($data,8,2);
    $dinidb = mktime(0,0,0,$dtinidb_mes, $dtinidb_dia, $dtinidb_ano);
    $dias = $eparam['dias'];
    for($i = 0; $i < $dias; $i++) {
            $dinidb -= 86400;
    }
    $dt = date("Y-m-d", $dinidb);
    $selorienta = "SELECT * FROM ebook WHERE dataalt BETWEEN '$dt' AND '$data'";
    $eorienta = $_SESSION['query']($selorienta) or die (mysql_error());
    while($lorienta = $_SESSION['fetch_array']($eorienta)) {
      echo "<table align=\"center\" width=\"221\">";
      echo "<tr>";
      if($lorienta['tporienta'] == "ARQUIVO") {
              echo "<td align=\"center\"><strong><a href=\"".str_replace($rais,"",$lorienta['arquivo'])."\">".$lorienta['titulo']."</a></strong></td>";
      }
      if($lorienta['tporienta'] == "TEXTO") {
            echo "<td align=\"center\"><strong><a href=\"inicio.php?menu=ebook&texto=".$_GET['texto']."&orientacao=".$lorienta['idebook']."\">".$lorienta['titulo']."</a></strong></td>";
      }
      echo "</tr>";
      echo "</table>";
    }
    echo "</marquee>";
    echo "</div>";
}
else {
}
?>
<div id="continicio">
  <?php
    $include_once = $_GET['menu'];
    if(isset($_GET['menu'])) {
      switch($include_once) {
              case "chat";
              include_once "monitor/chat.php";
              break;
              case "sistema";
              include_once "monitor/sistema.php";
              break;
      }
    }
    if(!isset($_GET['menu'])) {
      include_once ("monitor/sistema.php");
    }
    ?>
</div>