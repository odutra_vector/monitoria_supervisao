<?php
$idimprodutiva = $_POST['idimprodutiva'];
$dtini = $_POST['dtinimoni'];
$dtfim = $_POST['dtfimmoni'];
$iduser = $_SESSION['usuarioID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="/monitoria_supervisao/js/custom-theme/jquery-ui-1.8.2.custom.css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dataini.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/datafim.js"></script>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
        $("#spanacao").hide();
        <?php
        if($_GET['delreg'] == 1) {
            ?>
            $("#spanacao").show();
            $("#spanacao").html("Registros alterados/excluídos com sucesso");  
            <?php
        }
        ?>
        $("#pesquisar").live('click',function() {
            var dtini = $("#dtinimoni").val();
            var dtfim = $("#dtfimmoni").val();
            var id = $("#idimprodutiva").val();
            if(id != "") {
            }
            else {
                if(dtini == "" || dtfim == "") {
                    alert("Favor preencher a faixa de data da monitoria!!!");
                    return false;
                }
                else {
                }
            }
        });
        
        $("#marcar").live('click',function() {
            var bmarcar = $("#bmarcar").val();
            if(bmarcar == 0) {
                $("#marcar").attr("value","Desmacar");
                $("#bmarcar").attr("value","1");
                $("input[name='idimprod']").each(function() {
                    $(this).attr("checked","checked");
                })
            }
            else {
                $("#marcar").attr("value","Marcar Todas");
                $("#bmarcar").attr("value","0");
                $("input[name='idimprod']").each(function() {
                    $(this).removeAttr("checked");
                })
            }
        })
        
        $("[id*='excluir']").live('click',function() {
            $("#spanacao").hide();
            var pegaid = $(this).attr("id");
            var id = pegaid.substr(7);
            var op = $("#acao").val();
            var motivo = $("#idmotivo"+id).val();
            if(op == "") {
                $("#spanacao").show();
                $("#spanacao").html("Para executar o processo referente ao registro selecionado, é necessário selecionar a AÇÃO");
            }
            else {
                $("#spanacao").hide();
                var confirma = confirm("Você deseja realmente tomar esta ação sobre este registro?");
                if(confirma) {
                    if(op == "excluirfila" || op == "excluirreg") {
                        $.post('/monitoria_supervisao/admin/delreg.php',{delreg:'exluir',idregmonitoria:id,acao:op},function(retorno) {
                            if(retorno == 1) {
                                window.location="/monitoria_supervisao/admin/admin.php?menu=improdutivas&delreg=1";                            
                            }
                            else {
                                $("#spanacao").show();
                                $("#spanacao").html("Ocorreu um erro para executar a ação sobre o registro, favor contatar o administrador");
                            }
                        });
                    }
                    else {
                        $.post('/monitoria_supervisao/admin/delreg.php',{delreg:'excluir',idregmonitoria:id,acao:op,idmotivo:motivo},function(retorno) {
                            if(retorno == 1) {
                                window.location="/monitoria_supervisao/admin/admin.php?menu=improdutivas&delreg=1";                            
                            }
                            if(retorno == 0) {
                                $("#spanacao").show();
                                $("#spanacao").html("Ocorreu um erro para executar a ação sobre o registro, favor contatar o administrador");
                            }
                            if(retorno == 2) {
                                $("#spanacao").show();
                                $("#spanacao").html("O registro não foi alterado, pois o motivo não foi motidificado");
                            }
                        });
                    }
                }
                else {
                    return false;
                }
            }
        });
        
        $("#varios").live('click',function() {
            var op = $("#acao").val();
            if(op == "") {
                $("#spanacao").show();
                $("#spanacao").html("Para executar o processo, é necessário selecionar a AÇÃO");
            }
            else {
                var ids = "";
                $("input[id*='idimprod']").each(function() {
                    if($(this).attr('checked')) {
                        ids = ids+","+$(this).val();
                    }
                })
                if(ids == "") {
                    $("#spanacao").show();
                    $("#spanacao").html("Favor selecionar pelo menos 1 registro");
                }
                else {
                    var ids = ids.substring(1);
                    var confirma = confirm("Você deseja realmente tomar esta ação sobre este registro?");
                    if(confirma) {
                        if(op == "excluirfila" || op == "excluirreg") {
                            $.post('/monitoria_supervisao/admin/delreg.php',{delreg:'exluir',idregmonitoria:ids,acao:op},function(retorno) {
                                if(retorno == 1) {
                                    window.location="/monitoria_supervisao/admin/admin.php?menu=improdutivas&delreg=1";
                                }
                                else {
                                    $("#spanacao").show();
                                    $("#spanacao").html("Ocorreu um erro para executar a ação sobre o registro, favor contatar o administrador");
                                }
                            });
                        }
                        else {
                            $("#spanacao").show();
                            $("#spanacao").html("A alteração do motivo do registro só pode ser efetuado com 1 registro por vez!!!");
                        }
                    }
                    else {
                        return false;
                    }
                }
            }
        })
    })
</script>
</head>
<body>
    <div id="conteudo">
        <div id="opcoes">
            <form action="" method="post">
            <table>
              <tr>
                <td width="100" class="corfd_coltexto"><strong>ID IMPRODUTIVA</strong></td>
                <td width="321" class="corfd_colcampos"><input name="idimprodutiva" id="idimprodutiva" style="width:100px; border: 1px solid #9CF; text-align:center" value="<?php echo $idimprodutiva;?>" /></td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>DATA MONITORIA</strong></td>
                <td class="corfd_colcampos">
                    <input name="dtinimoni" id="dtinimoni" value="<?php echo $dtini;?>" type="text" style="width:100px; border: 1px solid #9CF; text-align:center"/> A <input name="dtfimmoni" id="dtfimmoni" value="<?php echo $dtfim;?>" type="text" style="width:100px; border: 1px solid #9CF; text-align:center"/>
                </td>
              </tr>
              <tr>
                  <td class="corfd_coltexto"><strong>MOTIVOS</strong></td>
                  <td class="corfd_colcampos">
                      <select name="idmotivo" id="idmotivo">
                      <?php
                      if($_POST['idmotivo'] != "") {
                          ?>
                          <option value="">SELECIONE...</option>
                          <?php
                      }
                      else {
                          ?>
                          <option selected="selected" value="">SELECIONE...</option>
                          <?php
                      }
                      $selmot = "SELECT idmotivo,nomemotivo FROM motivo m
                                 INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo
                                 WHERE vincula='regmonitoria' ORDER BY nomemotivo";
                      $eselmot = $_SESSION['query']($selmot) or die (mysql_error());
                      while($lselmot = $_SESSION['fetch_array']($eselmot)) {
                        $motivos[$lselmot['idmotivo']] = $lselmot['nomemotivo'];
                        if($_POST['idmotivo'] == $lselmot['idmotivo']) {
                            ?>
                            <option selected="selected" value="<?php echo $lselmot['idmotivo'];?>"><?php echo $lselmot['nomemotivo'];?></option>
                            <?php
                        }  
                        else {
                            ?>
                            <option value="<?php echo $lselmot['idmotivo'];?>"><?php echo $lselmot['nomemotivo'];?></option>
                            <?php
                        }
                      }
                      ?>
                      </select>
                  </td>
              </tr>
              <?php
              $periodo = periodo();
            
              $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
              $efiltro = $_SESSION['query']($selfiltros) or die ("erro na consulta dos nomes dos filtros");
              while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
                  $nomes[] = strtolower($lfiltros['nomefiltro_nomes']);
                  $filtros[] = strtolower($lfiltros['idfiltro_nomes']);
                  $nome = $lfiltros['nomefiltro_nomes'];
                  $idfiltro = "id_".strtolower($lfiltros['nomefiltro_nomes']);
                  if($_SESSION['user_tabela'] == "user_web") {
                  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM userwebfiltro
                                 INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = userwebfiltro.idrel_filtros
                                 INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                                 INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                                 INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                                 INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                                 WHERE iduser_web='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
                  }
                  if($_SESSION['user_tabela'] == "user_adm") {
                  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM useradmfiltro
                                 INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = useradmfiltro.idrel_filtros
                                 INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                                 INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                                 INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                                 INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                                 WHERE iduser_adm='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
                  }
                  $efiltrodados = $_SESSION['query']($filtrodados) or die ("erro na consulta dos filtros realcionados os usuário");
                  echo "<tr>";
                    echo "<td class=\"corfd_coltexto\"><strong>".$nome."</strong></td>";
                    echo "<td class=\"corfd_colcampos\"><select style=\"width:350px; border: 1px solid #333;\" name=\"filtro_".strtolower($nome)."\" id=\"filtro_".strtolower($nome)."\">";
                    if(isset($_POST["filtro_".$nome]) OR $varget["filtro_".$nome] != "") {
                    }
                    else {
                        //echo "selected=\"selected\" disabled=\"disabled\"";
                    }
                    echo "<option value=\"\">Selecione....</option>";
                    while($lfiltrodados = $_SESSION['fetch_array']($efiltrodados)) {
                        if(in_array($lfiltrodados['idindice'],$idsindice)) {
                        }
                        else {
                            $idsindice[] = $lfiltrodados['idindice'];
                        }
                        if(in_array($lfiltrodados['idfluxo'],$idsfluxo)) {
                        }
                        else {
                            $idsfluxo[] = $lfiltrodados['idfluxo'];
                        }
                        if($lfiltrodados[$idfiltro] == $_POST['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $varget['filtro_'.strtolower($nome)]) {
                            $select = "selected=\"selected\"";
                        }
                        else {
                            $select = "";
                        }
                        echo "<option value=\"".$lfiltrodados[$idfiltro]."\" $select >".$lfiltrodados['nomefiltro_dados']."</option>";
                    }
                    echo "</select><td>";
                 echo "</tr>";
              }
              ?>
              <tr>
              	<td colspan="2">
                    <input type="submit" name="pesquisar" id="pesquisar" value="PESQUISAR" style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)"/> 
                    <input type="button" name="marcar" id="marcar" value="Marcar Todas" style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)"/>
                    <input type="hidden" name="bmarcar" id="bmarcar" value="0" />
                </td>
              </tr>
            </table>
            </form>
      </div><br /><hr/>
      <div>
        <span id="spanacao" style="color:red; font-weight: bold; font-size: 12px"></span>
        <table id="tabacao">
            <tr>
                <td class="corfd_coltexto"><strong>AÇÃO</strong></td>
                <td class="corfd_colcampos">
                    <select name="acao" id="acao">
                        <option value="">SELECIONE...</option>
                        <option value="excluirfila">EXCLUIR DA FILA</option>
                        <option value="excluirreg">EXCLUIR IMPRODUTIVA</option>
                        <option value="alterarreg">ALTERAR IMPRODUTIVA</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="corfd_colcampos"><input type="button" name="varios" id="varios" value="AÇÃO SELECIONADOS" style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" /></td>
            </tr>
        </table>
      </div><br/>
      <div id="listagem" style="height: 400px; overflow: auto">
        <table id="tabimp" style="width: 1400px">
        <tr>
        <td width="70px"></td>
        <?php
            $alias = new TabelasSql();
            $colunas["idregmonitoria"] = "ID";
            $colunas[$alias->AliasTab("regmonitoria").".idrel_filtros"] = "RELACIONAMENTO";
            $nomecolunas = array("idregmonitoria" => "ID","r.idrel_filtros" => "RELACIONAMENTO","datactt" => "DATA CTT","horactt" => "HORA CTT",
                       "o.operador" => "OPERADOR","nome_cliente" => "CLIENTE","cnpj_cliente" => "CNPJ","nomemotivo" => "MOTIVO","nomemonitor" => "MONITOR","data" => "DATA","horaini" => "HORA","narquivo" => "NOME ARQUIVO");
            $selcol = "SHOW COLUMNS FROM fila_grava";
            $eselcol = $_SESSION['query']($selcol) or die (mysql_error());
            while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                $colfila[] = $lselcol['Field'];
            }
            foreach($nomecolunas as $col => $aprescol) {
                if(in_array($col,$colfila)) {
                    $colunas[$col] = $aprescol;
                }
            }
            $colunas['data'] = "DATA";
            $colunas['horaini'] = "HORA";
            $colunas["nomemotivo"] = "MOTIVO";
            $colunas["nomemonitor"] = "MONITOR";
            $colunas[$alias->AliasTab("operador").".operador"] = "OPERADOR";
            
            foreach($colunas as $col => $ncol) {
                ?>
                <td class="corfd_ntab" align="center"><strong><?php echo $ncol;?></strong></td>
                <?php
            }
            ?>
            </tr>
            <?php
            if($_POST['pesquisar']) {
                $dados['idregmonitoria'] = "idimprodutiva";
                $dados['dataini'] = "dtinimoni";
                $dados['datafim'] = "dtfimmoni";
                $dados[$alias->AliasTab("motivo").".idmotivo"] = "idmotivo";
                foreach($nomes as $n) {
                    $dados['id_'.$n] = $n;	
                }
                foreach($dados as $chdado => $dado) {
                    if(array_key_exists("idregmonitoria", $where)) {
                        break;
                    }
                    else {
                        if(strstr($chdado,"id_")) {
                            if($_POST['filtro_'.$dado] != "") {
                                if(array_key_exists($chdado, $where)) {
                                }
                                else {
                                    $where[$chdado] = "$chdado='".$_POST['filtro_'.$dado]."'";
                                }
                            }
                        }
                        else {
                            if($_POST[$dado] != "") {
                                if(in_array($chdado, $where)) {
                                }
                                else {
                                    if($dado == "dtinimoni" OR $dado == "dtfimmoni") {
                                    if(array_key_exists("data", $where)) {
                                    }
                                    else {
                                        $where['data'] = "data BETWEEN '".data2banco($_POST['dtinimoni'])."' AND '".data2banco($_POST['dtfimmoni'])."'";
                                    }
                                    }
                                    else {
                                        $where[$chdado] = $chdado."='".$_POST[$dado]."'";
                                    }
                                }
                            }
                        }
                    }
                }
                if($where != "") {
                    $w = "WHERE ".implode(" AND ",$where);
                }
                $selreg = "SELECT ".implode(",",array_keys($colunas)).",".$alias->AliasTab("regmonitoria").".idmotivo FROM regmonitoria ".$alias->AliasTab("regmonitoria")."
                                 INNER JOIN rel_filtros ".$alias->AliasTab("rel_filtros")." ON ".$alias->AliasTab("rel_filtros").".idrel_filtros = ".$alias->AliasTab("regmonitoria").".idrel_filtros
                                 LEFT  JOIN fila_grava ".$alias->AliasTab("fila_grava")." ON ".$alias->AliasTab("fila_grava").".idfila_grava = ".$alias->AliasTab("regmonitoria").".idfila
                                 INNER JOIN monitor ".$alias->AliasTab("monitoria")." ON ".$alias->AliasTab("monitoria").".idmonitor = ".$alias->AliasTab("regmonitoria").".idmonitor
                                 INNER JOIN motivo ".$alias->AliasTab("motivo")." ON ".$alias->AliasTab("motivo").".idmotivo = ".$alias->AliasTab("regmonitoria").".idmotivo
                                 INNER JOIN operador ".$alias->AliasTab("operador")." ON ".$alias->AliasTab("operador").".idoperador = ".$alias->AliasTab("regmonitoria").".idoperador $w ORDER BY idregmonitoria DESC LIMIT 500";
                $eselreg = $_SESSION['query']($selreg) or die (mysql_error());
                $linhas = $_SESSION['num_rows']($eselreg);
                if($linhas == 0) {
                    ?>
                    <tr>
                        <td class="corfd_colcampos" style="font-weight: bold;color:red; text-align: center" colspan="<?php echo count($colunas);?>">NÃO FORAM ENCONTRADOS REGISTROS COM OS FILTROS ESCOLHIDOS</td>
                    </tr>
                    <?php
                }
                while($lselreg = $_SESSION['fetch_array']($eselreg)) {
                    ?>
                    <tr>
                    <?php
                    foreach($colunas as $colret => $colapres) {
                        if($colret == "datactt" OR $colret == "data") {
                            $tam = "";
                            $valor = banco2data($lselreg[$colret]);
                        }
                        else {
                            if(strstr($colret,"idrel_filtros")) {
                                $tam = "width=\"300px\"";
                                $valor = nomevisu($lselreg["idrel_filtros"]);
                            }
                            else {
                                if(strstr($colret,"operador")) {
                                    $tam = "width=\"300px\"";
                                    $valor = $lselreg["operador"];
                                }
                                else {
                                    if($colret == "nomemonitor") {
                                        $tam = "width=\"300px\"";
                                    }
                                    else {
                                        $tam = "";
                                    }
                                    $valor = $lselreg[$colret];
                                }
                            }
                        }
                        if($colret == "idregmonitoria") {
                            ?>
                            <td class="corfd_colcampos">
                                <div style="width:50px; text-align: center"><input type="checkbox" name="idimprod" id="idimprod<?php echo $valor;?>" value="<?php echo $valor;?>" /><a href="#excluir<?php echo $valor;?>" name="excluir<?php echo $valor;?>" id="excluir<?php echo $valor;?>"><img src="/monitoria_supervisao/images/option.png"></img></a></div>
                            </td>
                            <td class="corfd_colcampos" align="center"><?php echo $valor;?></td>
                            <?php
                        }
                        else {
                            if($colret == "nomemotivo") {
                                ?>
                                <td class="corfd_colcampos" align="center" <?php echo $tam;?>>
                                    <select name="idmotivo<?php echo $lselreg['idregmonitoria'];?>" id="idmotivo<?php echo $lselreg['idregmonitoria'];?>">
                                        <?php
                                        foreach($motivos as $idm => $nmotivo) {
                                            if($idm == $lselreg['idmotivo']) {
                                                ?>
                                                <option value="<?php echo $idm;?>" selected="selected"><?php echo $nmotivo;?></option>
                                                <?php
                                            }
                                            else {
                                                ?>
                                                <option value="<?php echo $idm;?>"><?php echo $nmotivo;?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </td>
                                <?php                                
                            }
                            else {
                                ?>
                                <td class="corfd_colcampos" align="center" <?php echo $tam;?>><?php echo $valor;?></td>
                                <?php
                            }
                        }
                    }
                    ?>
                    </tr>
                    <?php
                }
            }
            ?>
          </table>
      </div>
    </div>
</body>
</html>
