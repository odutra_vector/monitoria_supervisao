<?php

$dados = array('F' => 'FORMULA','MO' => 'MÉDIA OPERADOR','MS' => 'MÉDIA SUPERVISOR','P' => 'PLANIHA','PI' => 'PARETO ITENS','PG' => 'PARETO GRUPOS','A' => 'AMOSTRA');
$tipodados = array('CON' => 'CONSOLIDADO','H' => 'HISTORICO','C' => 'COMPARATIVO');

if($_GET['idrel'] != "") {
    $idrel = $_GET['idrel'];
    $parterel = $_GET['parte'];
    $tipoestru = $_GET['tipoestru'];
    $idestru = $_GET['idestru'];
    $sitestru = $_GET['sitestru'];
    if($tipoestru == "cab") {
        $tabestru = "relresultcab";
    }
    if($tipoestru == "corpo") {
        $tabestru = "relresultcorpo";
    }

    $selrel = "SELECT * FROM relresult WHERE idrelresult='$idrel'";
    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relatório cadastrado");

    $relcorpo = "SELECT * FROM relresultcorpo WHERE idrelresult='$idrel'";
    $ecorpo = $_SESSION['query']($relcorpo) or die ("erro na query de consulta da estrutura do relatório");
    $linhascorpo = $_SESSION['num_rows']($ecorpo);

    if($idestru != "") {
        $consestru = "SELECT * FROM $tabestru WHERE idrelresult='$idrel' AND id$tabestru='$idestru'";
        $econsestru = $_SESSION['fetch_array']($_SESSION['query']($consestru)) or die ("erro na query de consulta da estrutrura");
    }
    else {
    }

    $relcab = "SELECT * FROM relresultcab WHERE idrelresult='$idrel'";
    $ecab = $_SESSION['query']($relcab) or die ("erro na query de consulta da estrutura do relatório");
    $linhascab = $_SESSION['num_rows']($ecab);

    $selc = "SELECT COUNT(*) as result FROM relresultcampos WHERE idrelresult='$idrel'";
    $eselc = $_SESSION['fetch_array']($_SESSION['query']($selc)) or die ("erro na query de consulta dos campos");
    $lcampos = $eselc['result'];
}
else {
    
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jscolor/jscolor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("tr[class*='tr_']").hide();
        <?php
        $tabs[] = "<option value=\"\" disabled=\"disabled\" selected=\"selected\">SELECIONE...</option>";
        $seltab = "SELECT * FROM tabulacao WHERE ativo='S' GROUP BY idtabulacao";
        $eseltab = $_SESSION['query']($seltab) or die ("erro na query de consulta da tabulacao");
        while($lseltab = $_SESSION['fetch_array']($eseltab)) {
            $tabs[$lseltab['idtabulacao']] = "<option value=\"".$lseltab['idtabulacao']."\">".$lseltab['nometabulacao']."</option>";
            $tabsdb[$lseltab['idtabulacao']] = $lseltab['nometabulacao'];
        }
        if($econsestru['apresdados'] == "GRAFICO") {
            $bddados = $econsestru['dados'];
            echo "$('#paramgraf').show();\n";
            echo "$('#confcolgraf').show();\n";
            echo "$('.tr_".$bddados."').show();\n";
        }
        else {
            ?>
            var tabs = '<?php echo implode("",$tabs);?>';
            $('#paramgraf').hide();
            $('#confcolgraf').hide();
            <?php
        }
        ?>
        <?php
        if($econsestru['dados'] == "F") {
            echo "$('#tdagd').show();\n";
            echo "$('#tdformula').show();\n";
            echo "$('#tdtab').hide();\n";
        }
        else {
            if($econsestru['tipodados'] == "C") {
                echo "$('#tdformula').hide();\n";
                echo "$('#tdtab').hide();\n";
                echo "$('#tdtab').hide();\n";
            }
            else {
                if($econsestru['dados'] == "TAB") {
                    echo "$('#tdtab').show();\n";
                    echo "$('#tdagd').hide();\n";
                    echo "$('#tdformula').hide();\n";
                }
                else {
                    echo "$('#tdtab').hide();\n";
                    echo "$('#tdagd').hide();\n";
                    echo "$('#tdformula').hide();\n";
                }
            }
        }

        if(($idestru != "" && $tipoestru == "cab") OR ($idestru == "" && $tipoestru == "cab")) {
            echo "$('#tabcab').show();\n";
            echo "$('#tabbutton').show();\n";
            echo "$('#tabcorpo').hide();\n";
        }
        if(($idestru != "" && $tipoestru == "corpo") OR ($idestru == "" && $tipoestru == "corpo")) {
            echo "$('#tabcab').hide();\n";
            echo "$('#tabbutton').hide();\n";
            echo "$('#tabcorpo').show();\n";
        }
        if($idestru == "" && $tipoestru == "") {
            echo "$('#tabcab').hide();\n";
            echo "$('#tabbutton').hide();\n";
            echo "$('#tabcorpo').hide();\n";
        }
        $agrupa = array('NULL' => 'NENHUM', 'MO' => 'MÉDIA OPERADOR','MS' => 'MÉDIA SUPERVISOR');
        foreach($agrupa as $kag => $ag) {
            $agd[$kag] = $ag;
        }
        foreach(filtros() as $f) {
            $agdfiltros[] = "<option value=\"".strtoupper($f)."\">".strtoupper($f)."</option>";
            $agd[strtoupper($f)] = strtoupper($f);
        }
        foreach($agd as $kagdf => $agdf) {
            if($kagdf == $econsestru['agrupadados']) {
                $agdfi[] = "<option value=\"".$kagdf."\" selected=\"selected\">".$agdf."</option>";
            }
            else {
                $agdfi[] = "<option value=\"".$kagdf."\">".$agdf."</option>";
            }
        }
        $agdfiltros[] = "<option value=\"NULL\" selected=\"selected\">NENHUM</option>";
        $agdcompara = implode(",",$agdfiltros);
        $agd = implode("",$agdfi);
        ?>
        $('#layout').change(function() {
            var layout = $(this).val();

            if(layout == "CABECALHO") {
                $('#tabcab').show();
                $('#tabbutton').show();
                $('#tabcorpo').hide();
            }
            if(layout == "CORPO") {
                $('#tabcab').hide();
                $('#tabbutton').hide();
                $('#tabcorpo').show();
            }
        })
        
        $('#qtdelogo').change(function() {
            var count = $('tr.trlogos').size();
            var qtde = ($(this).val() - count);
            //$('tr.trlogos').remove();
            if(qtde < 0) {
                for(i = 1; i <= -qtde; i++) {
                    var n = i;
                    $('tr.trlogos:last').remove();

                }
            }
            else {
                for(i = 1; i <= qtde; i++) {
                    var n = count + 1;
                    $('#tabcab tbody:last').append('<tr class=\"trlogos\"><td width=\"141\" class=\"corfd_coltexto\"><strong>LOGO</strong></td><td class=\"corfd_colcampos\"></td><td width=\"259\" class=\"corfd_colcampos\"><label><input type=\"radio\" name=\"posilogo'+n+'\" value=\"E\" />ESQUEUDA</label><label><input type=\"radio\" name=\"posilogo'+n+'\" value=\"C\" />CENTRO</label><label><input type=\"radio\" name=\"posilogo'+n+'\" value=\"D\" />DIREITA</label></td><td width=\"263\" class=\"corfd_colcampos\"><input name=\"camlogo'+n+'\" id=\"camlogo'+n+'\" type=\"file\" value=\"\" /></td></tr>');
                    n++;
                }
            }
        })

        $('#dados').live('change',function() {
            var dados = $(this).val();
            var tipodados = $('#tipodados').val();

            if(tipodados == "H") {
                if(dados != "F") {
                    if(dados == "TAB") {
                        $('#tdtab').show();
                        $('#tabulacao').html(tabs);
                        $('#tdagd').hide();
                        $('#tdformula').hide();
                    }
                    else {
                        $('#tdagd').hide();
                        $('#tdformula').hide();
                    }
                    var apres = new Object();
                    if(dados == "IT" || dados == "TAB") {
                        apres['TABELA'] = 'TABELA';
                        apres['GRAFICO'] = 'GRAFICO';
                    }
                    else {
                        apres['TABELA'] = 'TABELA';
                    }
                }
                else {
                    $('#tdtab').hide();
                    $('#tdagd').show();
                    $('#tdformula').show();
                    var apres = new Object();
                    apres['TABELA'] = 'TABELA';
                    apres['GRAFICO'] = 'GRAFICO';
                }

                var htmla = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SECIONE...</option>';
                for(var a in apres) {
                    htmla = htmla + '<option value=\"'+a+'\">'+apres[a]+'</option>';
                }
                $('#apresdados').html(htmla);
            }
            else {
                if(dados == "F") {
                    $('#tdagd').show();
                    $('#tdformula').show();
                    $('#tdtab').hide();
                }
                else {
                    if(tipodados == "C") {
                        $('#tdformula').hide();
                        $('#tdtab').hide();
                    }
                    else {
                        if(tipodados == "CON") {
                            if(dados == "TAB") {
                                $('#tdtab').show();
                                $('#tabulacao').html(tabs);
                                $('#tdagd').hide();
                                $('#tdformula').hide();
                            }
                            else {
                                $('#tdtab').hide();
                                $('#tdagd').hide();
                                $('#tdformula').hide();
                            }
                        }
                        else {
                            $('#tdtab').hide();
                            $('#tdagd').hide();
                            $('#tdformula').hide();
                        }
                    }
                }
                var apres = new Object();
                apres['TABELA'] = 'TABELA';
                apres['GRAFICO'] = 'GRAFICO';
                var htmla = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SECIONE...</option>';
                for(var a in apres) {
                    htmla = htmla + '<option value=\"'+a+'\">'+apres[a]+'</option>';
                }
                $('#apresdados').html(htmla);
            }
        })

        $('#tipodados').change(function() {
            var tp = $(this).val();
            if(tp == "H") {
                $('#tdagd').hide();
                $('#agd').html('<?php echo $agd;?>');
                var seldados = $('#dados').val();

                var dados = new Object();
                dados['F'] = 'FORMULA';
                dados['MO'] = 'MÉDIA OPERADOR';
                dados['MS'] = 'MÉDIA SUPERVISOR';
                dados['P'] = 'PLANILHA';
                dados['A'] = 'AMOSTRA';
                dados['IT'] = 'INTERVALO NOTAS';
                dados['TAB'] = 'TABULACAO';

                var opfreq = new Object();
                opfreq['D'] = 'DIARIO';
                opfreq['S'] = 'SEMAMAL';
                opfreq['M'] = 'MENSAL';
                opfreq['A'] = 'ANUAL';

                var graficos = new Object();
                graficos['bar'] = 'BARRAS';
                graficos['column'] = 'COLUNAS';
                graficos['line'] = 'LINHAS';
                graficos['spline'] = 'CURVAS';
                graficos['bar,spline'] = 'BARRAS, CURVAS';
                graficos['bar,line'] = 'BARRAS, LINHAS';
                graficos['column,line'] = 'COLUNAS, LINHAS';
                graficos['column,spline'] = 'COLUNAS, CURVAS';

                var htmlf = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SECIONE...</option>';
                for(var j in opfreq) {
                    htmlf = htmlf + '<option value=\"'+j+'\">'+opfreq[j]+'</option>';
                }
                $('#freq').html(htmlf);

                var html = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SELECIONE...</option>';

                for(var i in dados) {
                    html = html + '<option value=\"'+i+'\">'+dados[i]+'</option>';   
                }
                $('#dados').html(html);

                var htmlg = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SECIONE...</option>';
                htmlg = htmlg + '<option value=\"NULL\">NENHUM</option>';
                for(var g in graficos) {
                    htmlg = htmlg + '<option value=\"'+g+'\">'+graficos[g]+'</option>';
                }
                $('#tipograf').html(htmlg);

            }
            if(tp == "CON") {
                $('#tdagd').hide();
                $('#agd').html('<?php echo $agd;?>');
                var dados = new Object();
                dados['F'] = 'FORMULA';
                dados['MO'] = 'MÉDIA OPERADOR';
                dados['MS'] = 'MÉDIA SUPERVISOR';
                dados['P'] = 'PLANILHA';
                dados['PI'] = 'PARETO ITENS';
                dados['PG'] = 'PARETO GRUPOS';
                dados['A'] = 'AMOSTRA';
                dados['IT'] = 'INTERVALO NOTAS';
                dados['TAB'] = 'TABULACAO';

                var graficos = new Object();
                graficos['bar'] = 'BARRAS';
                graficos['column'] = 'COLUNAS';
                graficos['line'] = 'LINHAS';
                graficos['spline'] = 'CURVAS';
                //graficos['pie'] = 'PIZZA';
                graficos['bar,spline'] = 'BARRAS, CURVAS';
                graficos['bar,line'] = 'BARRAS, LINHAS';
                graficos['column,line'] = 'COLUNAS, LINHAS';
                graficos['column,spline'] = 'COLUNAS, CURVAS';

                var freq = '<option value=\"G\" selected=\"selected\">GERAL</option>';
                var html = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SELECIONE...</option>';
                var htmlg = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SELECIONE...</option>';
                htmlg = htmlg + '<option value=\"NULL\">NENHUM</option>';

                for(var i in dados) {
                    html = html + '<option value=\"'+i+'\">'+dados[i]+'</option>';
                }
                $('#dados').html(html);
                
                $('#freq').html(freq);

                for(var g in graficos) {
                    htmlg = htmlg + '<option value=\"'+g+'\">'+graficos[g]+'</option>';
                }
                $('#tipograf').html(htmlg);

                $('#tdext1').remove();
                $('#tdext2').remove();
            }
            if(tp == "C") {
                $('#tdtab').hide();
                $('#tdagd').show();
                $('#agd').html('<?php echo $agdcompara;?>');
                var dados = new Object();
                dados['F'] = 'FORMULA';
                dados['MO'] = 'PLANILHA';
                dados['A'] = 'AMOSTRA';
                dados['IT'] = 'INTERVALO NOTAS';
                var html ='<option value=\"\" selected=\"selected\" disabled=\"disabled\">SELECIONE...</option>';

                var opfreq = new Object();
                opfreq['D'] = 'DIARIO';
                opfreq['S'] = 'SEMAMAL';
                opfreq['M'] = 'MENSAL';
                opfreq['A'] = 'ANUAL';
                var htmlf = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SECIONE...</option>';

                var graficos = new Object();
                graficos['bar'] = 'BARRAS';
                graficos['column'] = 'COLUNAS';
                graficos['line'] = 'LINHAS';
                graficos['spline'] = 'CURVAS';
                //graficos['pie'] = 'PIZZA';
                graficos['bar,spline'] = 'BARRAS, CURVAS';
                graficos['bar, line'] = 'BARRAS, LINHAS';
                graficos['column, line'] = 'COLUNAS, LINHAS';
                graficos['column, spline'] = 'COLUNAS, CURVAS';
                var htmlg = '<option value=\"\" selected=\"selected\" disabled=\"disabled\">SELECIONE...</option>';
                htmlg = htmlg + '<option value=\"NULL\">NENHUM</option>';
                
                for(var j in opfreq) {
                    htmlf = htmlf + '<option value=\"'+j+'\">'+opfreq[j]+'</option>';
                }
                $('#freq').html(htmlf);

                for(var i in dados) {
                    html = html + '<option value=\"'+i+'\">'+dados[i]+'</option>';
                }
                $('#dados').html(html);

                for(var g in graficos) {
                    htmlg = htmlg + '<option value=\"'+g+'\">'+graficos[g]+'</option>';
                }
                $('#tipograf').html(htmlg);

                $('#tdext1').remove();
                $('#tdext2').remove();
            }
        })
        
        $('#apresdados').change(function() {
            var dados = $('#dados').val();
            var apres = $('#apresdados').val()
            var tipograf = $(this).val();
            $("tr[class*='tr_']").hide();
            if(dados != "" && apres == "GRAFICO") {
                var dados = $('#dados').val();
            
                $('#paramgraf').show();
                $('#confcolgraf').show();
                $(".tr_"+dados).show();
                if(dados == "IT") {
                    $('#tipograf').html('<option value=\"NULL\" selected=\"selected\">NENHUM</option><option value=\"bar\">BARRAS</option><option value=\"column\">COLUNAS</option>');
                }
                else {

                }
            }
            else {
                $('#paramgraf').hide();
                $("tr[class*='tr_']").hide();
                $('#confcolgraf').hide();
            }
        })

        $('#tipograf').change(function() {
            var dados = $('#dados').val();
            var apres = $('#apresdados').val()
            var tipograf = $(this).val();
            if(dados != "" && apres == "GRAFICO") {
                if(tipograf == "NULL") {
                    $(".tp").html('<option value=\"NULL\" selected=\"selected\">NENHUM</option>');
                }
                if(tipograf == "bar") {
                    $(".tp").html('<option value=\"bar\" selected=\"selected\">BARRAS</option>');
                }
                if(tipograf == "column") {
                    $(".tp").html('<option value=\"column\" selected=\"selected\">COLUNAS</option>');
                }
                //if(tipograf == "pie") {
                    //$(".tp").html('<option value=\"pie\" selected=\"selected\">PIZZA</option>');
                //}
                if(tipograf == "line") {
                    $(".tp").html('<option value=\"line\" selected=\"selected\">LINHAS</option>');
                }
                if(tipograf == "spline") {
                    $(".tp").html('<option value=\"spline\" selected=\"selected\">CURVAS</option>');
                }
                if(tipograf == "bar,line") {
                    $(".tp").html('<option value=\"bar\" selected=\"selected\">BARRAS</option><option value=\"line\">LINHAS</option>');
                }
                if(tipograf == "bar,spline") {
                    $(".tp").html('<option value=\"bar\" selected=\"selected\">BARRAS</option><option value=\"spline\">CURVAS</option>');
                }
                if(tipograf == "column,spline") {
                    $(".tp").html('<option value=\"column\" selected=\"selected\">COLUNAS</option><option value=\"spline\">CURVAS</option>');
                }
                if(tipograf == "column,line") {
                    $(".tp").html('<option value=\"column\" selected=\"selected\">COLUNAS</option><option value=\"line\">LINHAS</option>');
                }
            }
            else {
            }
        })

        $("input[id*='delrel']").click(function() {
            var conf = confirm('Deseja realmente apagar o relatório?');

            if(conf == true) {
            }
            else {
                return false;
            }
        })

        $('#cadcorpo').click(function() {
            var tp = $('#tipodados').val();
            var dados = $('#dados').val();
            var apres = $('#apresdados').val();
            var tipograf = $('#tipograf').val();
            var exthist = $('#exthist').val();
            var agd = $('#agd').val();
            var formula = $('#idformula').val();

            if(tp == "" || dados == "" || apres == "" || tipograf == "") {
                alert('Os campos baixo precisam estar preenchidos para seguir com o cadastramento!!!\n\
                       TIPO DADOS\n\
                       DADOS\n\
                       APRESENTAÇÃO DOS DADOS\n\
                       TIPO DO GRÁFICO');
                return false;
            }
            else {
                if(dados == "F" && formula == "") {
                    alert('Quando a tipo de dados for FORMULA é necessário informar a qual formula irá se referir os resultados!!!');
                    return false;
                }
                else {
                }
            }
        })

    })
</script>
<script type="text/javascript">
    function abreimg(url) {
        window.open(url,'Imagem','toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=400');
    }

</script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#addmais').click(function() {
			var c = $('tbody.campos tr').size();
                        var l = (c + 1);
                        if(l == 20) {
                        }
                        else {
                            $('#latu').attr('value',l);
                            var add = '<tr id=\"tdcampos'+l+'\"><td class=\"corfd_colcampos\"><select name=\"campos'+l+'\" id=\"campos'+l+'\">\n\
                                     <option value=\"\" disabled=\"disabled\" selected=\"selected\">SELECIONE...</option>'
                                    <?php
                                    $campos = array('P' => 'PERIODO','QTMO' => 'QTDE. MONITORIAS', 'F' => 'FORMULA','QTOP' => 'QTDE. OPERADOR',
                                        'QTMOP' => 'QTDE. MONITORIAS/ OPERADOR', 'QTSUPER' => 'QTDE. SUPERVISOR','MO' => 'MÉDIA OPERADOR',
                                        'QTFG' => 'QTDE. FG/ MONITORIA','QTFGOP' => 'QTDE. FG/ OPERADOR', 'QTFGI' => 'QTDE. FG/ ITENS','QTFGIOP' => 'QTDE. FGI/ OPERADOR',
                                        'QTFGM' => 'QTDE. FGM/ MONITORIA','QTFGMOP' => 'QTDE. FGM/ OPERADOR','QTFGMIOP' => 'QTDE. FGMI/ OPERADOR', 'QTFGMI' => 'QTDE. FGM/ ITENS',
                                        'O' => 'OUTROS/ DIGITE O TEXTO');
                                    foreach($campos as $kca => $ca) {
                                    ?>
                                    +'<option value=\"<?php echo $kca;?>\"><?php echo $ca;?></option>'
                                    <?php
                                    }
                                    ?>
                                    +'</select>\n\
                                    </td>\n\
                                    <td class=\"corfd_colcampos\"><select name=\"idformula'+l+'\" id=\"idformula'+l+'\">\n\
                                    <option value=\"\">SELECINE...</option>'
                                    <?php
                                    $selform = "SELECT idformulas, nomeformulas FROM formulas WHERE idrelresult='$idrel'";
                                    $eselform = $_SESSION['query']($selform) or die ("erro na query de consulta das formulas relacionadas ao relatório");
                                    $lselform = $_SESSION['num_rows']($eselform);
                                    if($lselform >= 1) {
                                        while($lform = $_SESSION['fetch_array']($eselform)) {
                                            ?>
                                            +'<option value=\"<?php echo $lform['idformulas'];?>\"><?php echo $lform['nomeformulas'];?></option>'
                                        <?php
                                        }
                                    }
                                    else {
                                    }
                                    ?>
                                    +'</select></td>\n\
                                    <td class=\"corfd_colcampos\"><input name=\"descri'+l+'\" id=\"descri'+l+'\" style=\"width:200px; border: 1 solid #96C\" /></td>\n\
                                    <td class=\"corfd_colcampos\"><input name=\"valor'+l+'\" id=\"valor'+l+'" style=\"width:90px; border: 1 solid #96C\" /></td>\n\
                                    <td align=\"center\" class=\"corfd_colcampos\"><select name=\"posic'+l+'\" id=\"posic'+l+'\" style=\"width:50px\">'
                                    <?php
                                    for($i = 1; $i <= 20; $i++) {
                                    ?>
                                    +'<option value=\"<?php echo $i;?>\"><?php echo $i;?></option>'
                                    <?php
                                    }
                                    ?>
                                    '</select>\n\
                                    </td></tr>';
                            $('#tabcampos tbody:last').append(add);
                            }
		})

                $('#addmenos').click(function() {
                    var l = $('#latu').val();
                    $('#tdcampos'+l).remove();
                    var nl = (l - 1);
                    $('#latu').attr('value',nl);
                })

                $('#tabrel').submit(function() {
                    var nome = $('#nome').val();
                    var tipo = $('#tipo').val();
                    var perfadm = $('#idsperfadm').val();
                    var perfweb = $('#idsperfweb').val();
                    var c = $('tbody.campos tr').size();

                    for(i = 1;i <= c; i++) {
                        var campo = $('#campos'+i).val();
                        var form = $('#idformula'+i).val();
                        if(campo == "F" && form == "") {
                            alert('Quando o "Campo" for uma Formula é necessário informar a "FORMULA" que será utilizada: linha ('+i+')');
                        }
                        if(campo != "F" && form != "") {
                            alert('Quando o "Campo" for diferente de uma Formula a coluna "FORMULA" deverá ficar vazia: linha ('+i+')');
                        }
                    }

                    if(nome == ""|| tipo == "") {
                        alert('OS campos NOME e TIPO devem estar preenchidos para cadastro do relatório!!!'+perfadm);
                        return false;
                    }
                    else {
                    }
                    if(perfadm == null && perfweb == null) {
                        alert('É necessário preencher pelo menos 1 perfil para acesso ao relatório!!!');
                        return false;
                    }
                    else {
                    }
                })
	})
</script>
</head>
<body>
    <div id="conteudo">
    <?php
    if($idrel == "" OR ($idrel != "" && $parterel == "relatorio")) {
    ?>
      <form action="cadrelresult.php" method="post" id="tabrel">
    	<table width="702">
          <tr>
            <td align="center" class="corfd_ntab" colspan="4"><strong>CADASTRO DE RELATÓRIO</strong></td>
          </tr>
          <tr>
            <td width="96" class="corfd_coltexto"><strong>NOME</strong></td>
            <td width="252" class="corfd_colcampos"><input name="idrel" type="hidden" value="<?php echo $idrel;?>" /><input name="nome" id="nome" style="width:250px; border: 1px solid #9CF" type="text" value="<?php if($idrel != "") { echo $eselrel['nomerelresult'];} else {}?>" /></td>
            <td class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td class="corfd_colcampos" colspan="3">
                    <select name="ativo" id="ativo">
                        <?php
                        $ativo = array('S','N');
                        foreach($ativo as $atv) {
                            if($atv == $eselrel['ativo']) {
                                echo "<option value=\"".$atv."\" selected=\"selected\">".$atv."</option>";
                            }
                            else {
                                echo "<option value=\"".$atv."\">".$atv."</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
          </tr>
          <tr>
          <td class="corfd_coltexto"><strong>FITROS OBRIGATÓRIOS</strong></td>
            <td class="corfd_colcampos">
                <select name="filtros[]" id="filtros" multiple="multiple" style="width:200px; height: 120px; border: 1px solid #9CF">
                    <?php
                    $filtrosbd = explode(",",$eselrel['filtros']);
                    $filtros = array('dataini' => 'DATA INI CONTATO','datafim' => 'DATA FIM CONTATO', 'oper' => 'OPERADOR','super' => 'SUPERVISOR','monitor' => 'MONITOR', 'filtros' => 'FILTROS REL','aval' => 'AVALIAÇÃO', 'plan' => 'PLANILHA','grupo' => 'GRUPO', 'sub' => 'SUBGRUPO', 'perg' => 'PERGUNTA', 'resp' => 'RESPOSTA', 'fg' => 'FAHA GRAVE', 'fgm' => 'FALHA GRAVISSIMA');
                    foreach($filtros as $kf => $f) {
                        if(in_array($kf,$filtrosbd)) {
                            echo "<option value=\"".$kf."\" selected=\"selected\">".$f."</option>";
                        }
                        else {
                            echo "<option value=\"".$kf."\">".$f."</option>";
                        }
                    }
                    ?>
                </select>
             </td>
              <td class="corfd_coltexto"><strong>DESCRIÇÃO RELATÓRIO</strong></td>
              <td class="corfd_colcampos"><textarea name="descri" id="descri" style="width:250px; height:100px; border: 1px solid #9CF"><?php echo $eselrel['descricao'];?></textarea></td>
          </tr>
          <tr>
          	<td class="corfd_coltexto"><strong>Perfil ADM</strong></td>
            <td class="corfd_colcampos">
            	<select name="idsperfadm[]" id="idsperfadm" multiple="multiple" style="width:200px; height: 120px; border: 1px solid #9CF">
                	<?php
                        $perfisadm = explode(",",$eselrel['idsperfadm']);
                        $seladm = "SELECT * FROM perfil_adm WHERE ativo='S'";
                        $eseladm = $_SESSION['query']($seladm) or die ("erro na query de consulta dos perfis cadastrados");
                        while($lseladm = $_SESSION['fetch_array']($eseladm)) {
                            if(in_array($lseladm['idperfil_adm'],$perfisadm)) {
                                echo "<option value=\"".$lseladm['idperfil_adm']."\" selected=\"selected\">".$lseladm['nomeperfil_adm']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lseladm['idperfil_adm']."\">".$lseladm['nomeperfil_adm']."</option>";
                            }
                        }
                        ?>
                </select>
            </td>
            <td class="corfd_coltexto"><strong>Perfil WEB</strong></td>
            <td class="corfd_colcampos">
            	<select name="idsperfweb[]" id="idsperfweb" multiple="multiple" style="width:200px; height: 120px; border: 1px solid #9CF">
                <?php
                $perfisweb = explode(",",$eselrel['idsperfweb']);
                $selweb = "SELECT * FROM perfil_web WHERE ativo='S'";
                $eselweb = $_SESSION['query']($selweb) or die ("erro na query de consulta dos perfis cadastrados");
                while($lselweb = $_SESSION['fetch_array']($eselweb)) {
                    if(in_array($lselweb['idperfil_web'],$perfisweb)) {
                        echo "<option value=\"".$lselweb['idperfil_web']."\" selected=\"selected\">".$lselweb['nomeperfil_web']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lselweb['idperfil_web']."\">".$lselweb['nomeperfil_web']."</option>";
                    }
                }
                ?>
                </select>
            </td>
          </tr>
        </table><br />
              <?php
              if($idrel != "") {
              ?>
              <table width="596" id="tabcampos">
                  <tr>
                    <td class="corfd_ntab" align="center" colspan="5"><strong>DADOS INICIAIS APRESENTAÇÃO</strong></td>
                  </tr>
                  <tr>
                    <td width="214" align="center" class="corfd_coltexto"><strong>CAMPO</strong></td>
                    <td width="150" align="center" class="corfd_coltexto"><strong>FORMULA</strong></td>
                    <td width="200" align="center" class="corfd_coltexto"><strong>DESCRI CAMPO</strong></td>
                    <td width="90" align="center" class="corfd_coltexto"><strong>VALOR</strong></td>
                    <td width="72" align="center" class="corfd_coltexto"><strong>POSIÇÃO</strong></td>
                  </tr>
                  <tr>
                         <td colspan="4"><input name="latu" id="latu" type="hidden" value="<?php if($idrel != "" && $eselc['result'] >= 1) { echo $eselc['result'];} else { echo "1";}?>" /><input name="addmais" id="addmais" style="width:30px" type="button" value="+"  /> <input name="addmenos" id="addmenos" style="width:30px" type="button" value="-"  /></td>
                  </tr>
                  <tbody class="campos">
              <?php
                $i = 1;
                $selcampos = "SELECT * FROM relresultcampos WHERE idrelresult='$idrel'";
                $ecampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos campos cadastrados");
                while($lcampos = $_SESSION['fetch_array']($ecampos)) {
              ?>
              <tr id="tdcampos<?php echo $i;?>">
                  <td class="corfd_colcampos"><input name="idcampos<?php echo $i;?>" id="idcampos<?php echo $i;?>" value="<?php echo $lcampos['idrelresultcampos'];?>" type="hidden"/>
                    <select name="campos<?php echo $i;?>" id="campos<?php echo $i;?>">
                        <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                        <?php
                        $campos = array('P' => 'PERIODO','QTMO' => 'QTDE. MONITORIAS', 'F' => 'FORMULA','QTOP' => 'QTDE. OPERADOR',
                                        'QTMOP' => 'QTDE. MONITORIAS/ OPERADOR', 'QTSUPER' => 'QTDE. SUPERVISOR','MO' => 'MÉDIA OPERADOR',
                                        'QTFG' => 'QTDE. FG/ MONITORIA','QTFGOP' => 'QTDE. FG/ OPERADOR', 'QTFGI' => 'QTDE. FG/ ITENS','QTFGIOP' => 'QTDE. FGI/ OPERADOR',
                                        'QTFGM' => 'QTDE. FGM/ MONITORIA','QTFGMOP' => 'QTDE. FGM/ OPERADOR','QTFGMIOP' => 'QTDE. FGMI/ OPERADOR', 'QTFGMI' => 'QTDE. FGM/ ITENS',
                                        'O' => 'OUTROS/ DIGITE O TEXTO');
                        foreach($campos as $kc => $c) {
                            if($kc == $lcampos['campo']) {
                                echo "<option value=\"".$kc."\" selected=\"selected\">".$c."</option>";
                            }
                            else {
                                echo "<option value=\"".$kc."\">".$c."</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
                <td class="corfd_colcampos">
                    <select name="idformula<?php echo $i;?>" id="idformula<?php echo $i;?>">
                        <?php
                        $selformbd = "SELECT idformulas, nomeformulas FROM formulas WHERE idrelresult='$idrel'";
                        $eselformbd = $_SESSION['query']($selformbd) or die ("erro na query de consulta das formulas relacionadas ao relatório");
                        $lselformbd = $_SESSION['num_rows']($eselformbd);
                        if($lselformbd == 0) {
                            echo "<option value=\"\">SELECIONE...</option>";
                        }
                        else {
                        while($lformbd = $_SESSION['fetch_array']($eselformbd)) {
                                if($lcampos['idformula'] == "") {
                                    echo "<option value=\"\" selected=\selected\">SELECIONE...</option>";
                                    echo "<option value=\"".$lformbd['idformulas']."\">".$lformbd['nomeformulas']."</option>";
                                }
                                else {
                                    if($lformbd['idformulas'] == $lcampos['idformula']) {
                                        echo "<option value=\"".$lformbd['idformulas']."\" selected=\selected\">".$lformbd['nomeformulas']."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$lformbd['idformulas']."\">".$lformbd['nomeformulas']."</option>";
                                    }

                                }
                            }
                        }
                        ?>
                    </select>
                </td>
                <td class="corfd_colcampos"><input name="descri<?php echo $i;?>" maxlength="100" id="descri<?php echo $i;?>" style="width:200px; border: 1 solid #96C" value="<?php echo $lcampos['descricampo'];?>" /></td>
                <td class="corfd_colcampos"><input name="valor<?php echo $i;?>" maxlength="7" id="valor<?php echo $i;?>" style="width:90px; border: 1 solid #96C" value="<?php echo $lcampos['valor'];?>" /></td>
                <td align="center" class="corfd_colcampos">
                    <select name="posic<?php echo $i;?>" id="posic<?php echo $i;?>" style="width:50px">
                    <?php
                    for($po = 0; $po <= 20; $po++) {
                        if($po == $lcampos['posicao']) {
                            echo "<option value=\"".$po."\" selected=\"selected\">".$po."</option>";
                        }
                        else {
                            echo "<option value=\"".$po."\">".$po."</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
              </tr>
              <?php
                $i++;
                }
              ?>
                </tbody>
              </table>
              <?php
              }
              else {
              }
              ?>
       <table width="596">
          <tr>
            <td colspan="2">
            <?php
            if($idrel == "") {
            ?>
                <input type="submit" name="cadrel" id="cadrel" value="Cadastrar" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #fff" />
            <?php
            }
            else {
            ?>
                <input type="submit" name="altrel" id="altrel" value="Alterar" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #fff" /> <input type="submit" name="novorel" id="altrel" value="NOVO" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #fff" />
            <?php
            }
            ?>
            </td>
          </tr>
        </table>
        <font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font>
      </form>
    <?php
    }
    if($parterel == "estrutura") {
    ?>
    <form action="cadrelresult.php" method="post" enctype="multipart/form-data">
        <table width="246">
            <tr>
                <td class="corfd_ntab" align="center"><strong>CADASTROS DE RELATÓRIOS</strong></td>
            </tr>
            <tr>
                <td class="corfd_colcampos" align="center"><input type="submit" name="novorel" id="novorel" value="NOVO" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #fff" /></td>
            </tr>
        </table><br></br>
    	<table width="246">
          <tr>
            <td width="114" class="corfd_coltexto"><strong>LAYOUT</strong></td>
            <td width="120" class="corfd_colcampos">
            	<select name="layout" id="layout">
                    <option value="" disabled="disabled" readonly="readonly" selected="selected">SELECIONE...</option>
                    <?php
                    $tipos = array('cab' => 'CABECALHO','corpo' => 'CORPO');
                    foreach($tipos as $kt => $t) {
                        if($kt == "cab" && $linhascab >= 1) {
                        }
                        else {
                            if($tipoestru == $kt) {
                                echo "<option value=\"".$t."\" selected=\"selected\">".$t."</option>";
                            }
                            else {
                                echo "<option value=\"".$t."\">".$t."</option>";
                            }
                        }
                    }
                    ?>
                </select>
            </td>
          </tr>
        </table> <br />
        <table width="800" id="tabcab">
        <tbody>
          <tr>
            <td class="corfd_ntab" align="center" colspan="6"><strong>CADASTRO ESTRUTURA CABEÇALHO</strong></td>
          </tr>
          <tr>
            <td width="141" class="corfd_coltexto"><strong>CAMPOS</strong></td>
            <td colspan="4" class="corfd_colcampos"><input type="hidden" name="idrelresult" value="<?php echo $_GET['idrel'];?>" /><input type="hidden" name="idestru" value="<?php echo $_GET['idestru'];?>" />
            	<select name="campos[]" id="campos" multiple="multiple" style="width:150px; border: 1px solid #9CF; height:70px">
                <?php
                $camposbd = explode(",", $econsestru['campos']);
                $campos = array('FILTROS','USUARIO','DATA','DATA CONTATO','RELACIONAMENTO');
                foreach($campos as $campo) {
                    if(in_array($campo,$camposbd)) {
                        echo "<option value=\"".$campo."\" selected=\"selected\">".$campo."</option>";
                    }
                    else {
                        echo "<option value=\"".$campo."\">".$campo."</option>";
                    }
                }
                ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>POSIÇÃO CAMPOS</strong></td>
            <td colspan="4" class="corfd_colcampos">
                <?php
                if($idestru != "" && $tipoestru == "cab") {
                    $posicoes = array('C' => 'CENTRO','E' => 'ESQUERDA', 'D' => 'DIREITA');
                    foreach($posicoes as $kp => $posi) {
                        if($kp == $econsestru['posicampos']) {
                            echo "<label><input type=\"radio\" name=\"posicampo\" value=\"".$kp."\" checked=\"checked\" />".$posi."</label>";
                        }
                        else {
                            echo "<label><input type=\"radio\" name=\"posicampo\" value=\"".$kp."\" />".$posi."</label>";
                        }
                    }
                }
                else {
                ?>
                <label><input type="radio" name="posicampo" value="C" id="poscentro" />CENTRO</label>
                <label><input type="radio" name="posicampo" value="E" id="posesq" />ESQUERDA</label>
                <label><input type="radio" name="posicampo" value="D" id="posdir" />DIREITA</label>
                <?php
                }
                ?>
            </td>
          </tr>
          <tr>
          	<td class="corfd_coltexto"><strong>TAM. LOGOS</strong></td>
            <td class="corfd_colcampos" colspan="4"><input style="width:70px; border: 1px solid #9CF; text-align:center" maxlength="4" name="width" id="width" value="<?php echo $econsestru['width'];?>"/> <input style="width:70px; border: 1px solid #9CF; text-align:center" maxlength="4" name="height" id="height" value="<?php echo $econsestru['height'];?>" /></td>
          </tr>
          <tr>
            <td width="141" class="corfd_coltexto"><strong>QTDE. LOGOS</strong></td>
            <td colspan="4" class="corfd_colcampos"><input type="text" name="qtdelogo" id="qtdelogo" maxlength="1" style="width:40px; border: 1px solid #9CF; text-align:center" value="<?php if($tipoestru == "cab") { echo $econsestru['qtdelogos'];} else {}?>"/></td>
          </tr>
              <?php
              $posilogos = array('E' => 'ESQUERDA','D' => 'DIREITA','C' => 'CENTRO');
              if($idestru != "" && $tipoestru == "cab") {
                  $l = 1;
                  $posilogo = explode(",",$econsestru['posilogo']);
                  $logos = explode(",",$econsestru['caminhologo']);
                  for(;list(,$lo) = each($logos), list(,$posil) = each($posilogo);) {
                    $check = "";
                    $imagem = explode("/",$lo);
                    echo "<tr class=\"trlogos\"><td width=\"141\" class=\"corfd_coltexto\"><strong>LOGO</strong></td>";
                    echo "<td class=\"corfd_colcampos\"><a onclick=\"javascript:abreimg('".str_replace($rais, "",$lo)."')\"><input type=\"hidden\" name=\"arquivo".$l."\" value=\"".$lo."\"/>".end($imagem)."</a></td>";
                    echo "<td width=\"259\" class=\"corfd_colcampos\">";
                    foreach($posilogos as $kpl => $pl) {
                        if($kpl == $posil) {
                            echo "<label><input type=\"radio\" name=\"posilogo".$l."\" value=\"".$kpl."\" checked=\"checked\" />".$pl."</label>";
                        }
                        else {
                            echo "<label><input type=\"radio\" name=\"posilogo".$l."\" value=\"".$kpl."\" />".$pl."</label>";
                        }
                        
                    }
                    echo "</td>";
                    echo "<td width=\"263\" class=\"corfd_colcampos\"><input name=\"camlogo".$l."\" id=\"camlogo".$l."\" type=\"file\" value=\"".$lo."\" /></td>
                        </tr>";
                    $l++;
                  }
              }
              else {
                  //echo "<tr class=\"trlogos\"></tr>";
              }
              ?>
        </tbody>
        </table>
        <table id="tabbutton">
             <tr>
                <td colspan="3">
                <?php
                if(($idestru == "" && $tipoestru == "cab" && $sitestru == "cad") OR ($idestru == "" && $tipoestru == "" && $sitestru == "") OR ($idestru != "" && $tipoestru == "corpo")) {
                ?>
                    <input type="submit" name="cadcab" id="cadcab" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #fff" value="Cadastrar" />
                <?php
                }
                if($idestru != "" && $tipoestru == "cab" && $sitestru == "alt") {
                ?>
                    <input type="submit" name="altcab" id="altcab" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #fff" value="Alterar" />
                <?php
                }
                ?>
                </td>
              </tr>
        </table>
        <font color="#FF0000"><strong><?php echo $_GET['msgcab']; ?></strong></font>
        <table width="541" id="tabcorpo">
          <tr>
            <td class="corfd_ntab" align="center" colspan="4"><strong>CADASTRO ESTRUTURA CORPO</strong></td>
          </tr>
          <tr id="trext">
            <td width="226" class="corfd_coltexto"><strong>TIPO DADOS</strong></td>
            <td colspan="3" width="303" class="corfd_colcampos"><input type="hidden" name="idrelresult" value="<?php echo $_GET['idrel'];?>" /><input type="hidden" name="idestru" value="<?php echo $_GET['idestru'];?>" />
            	<select name="tipodados" id="tipodados" style="width:230px">
                    <?php
                    $tipodados = array('CON' => 'CONSOLIDADO','H' => 'HISTORICO','C' => 'COMPARATIVO');
                    if($idestru != "" && $tipoestru == "corpo") {
                        echo "<option value=\"\" disabled=\"disabled\" readonly=\"readonly\">SELECIONE...</option>";
                        foreach($tipodados as $ktd => $td) {
                            if($ktd == $econsestru['tipodados']) {
                                echo "<option value=\"".$ktd."\" selected=\"selected\">".$td."</option>";
                            }
                            else {
                                echo "<option value=\"".$ktd."\">".$td."</option>";
                            }
                        }
                    }
                    else {
                    ?>
                    <option value="" disabled="disabled" readonly="readonly" selected="selected">SELECIONE...</option>
                    <?php
                    foreach($tipodados as $ktd => $td) {
                        echo "<option value=\"".$ktd."\">".$td."</option>";
                    }
                    }
                    ?>
                </select>                
            </td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>FREQUENCIA</strong></td>
            <td colspan="3" class="corfd_colcampos"><select name="freq" id="freq" style="width:230px">
                    <?php
                    $freq = array('G' => 'GERAL', 'D' => 'DIARIO','S' => 'SEMANAL','M' => 'MENSAL','A' => 'ANUAL');
                    if($linhascorpo >= 1 && $idestru != "") {
                        if($econsestru['tipodados'] == "CON") {
                            echo "<option value=\"G\">GERAL</option>";
                        }
                        else {
                            foreach($freq as $kf => $f) {
                                if($kf == $econsestru['frequenciadados']) {
                                    echo "<option value=\"".$kf."\" selected=\"selected\">".$f."</option>";
                                }
                                else {
                                    echo "<option value=\"".$kf."\">".$f."</option>";
                                }
                            }
                        }
                    }
                    else {

                    ?>
                    <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                    <?php
                    foreach($freq as $kfreq => $fr) {
                        echo "<option value=\"".$kfreq."\">".$fr."</option>";
                    }
                    }
                    ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>DADOS</strong></td>
            <td class="corfd_colcampos" colspan="3">
            	<select name="dados" id="dados" style="width:230px">
                    <?php
                    if($econsestru['tipodados'] == "C") {
                        $dados = array('F' => 'FORMULA','P' => 'PLANILHA','A' => 'AMOSTRA');
                    }
                    else {
                        $dados = array('F' => 'FORMULA','MO' => 'MÉDIA OPERADOR','MS' => 'MÉDIA SUPERVISOR','P' => 'PLANIHA','PI' => 'PARETO ITENS', 'PG' => 'PARETO GRUPOS','A' => 'AMOSTRA', 'IT' => 'INTERVALO NOTAS','TAB' => 'TABULACAO');
                    }               
                    if($linhascorpo >= 1 && $idestru != "") {
                        foreach($dados as $kda => $da) {
                            if($kda == $econsestru['dados']) {
                                echo "<option value=\"".$kda."\" selected=\"selected\">".$da."</option>";
                            }
                            else {
                                echo "<option value=\"".$kda."\">".$da."</option>";
                            }
                        }
                    }
                    else {
                    ?>
                    <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                    <?php
                    foreach($dados as $kd => $d) {
                        echo "<option value=\"".$kd."\">".$d."</option>";
                    }
                    }
                    ?>
                </select>
            </td>
          </tr>
            <tr id="tdagd">
                <td class="corfd_coltexto"><strong>AGRUPA DADOS</strong></td>
                <td class="corfd_colcampos" colspan="3">
                    <select name="agd" id="agd" style="width:230px">
                        <?php
                            echo $agd;
                        ?>
                    </select>
                </td>
            </tr>
            <tr id="tdformula">
                <td class="corfd_coltexto"><strong>FORMULA</strong></td>
                <td colspan="3" class="corfd_colcampos">
                    <select name="idformula" id="idformula" style="width:230px">
                    <?php
                    if($econsestru['idformula'] == "") {
                        echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                    }
                    else {
                        echo "<option value=\"\">SELECIONE...</option>";
                    }
                    $sformula = "SELECT * FROM formulas WHERE idrelresult='$idrel'";
                    $eformula = $_SESSION['query']($sformula) or die ("erro na query de consulta das formulas relacionadas ao relatório");
                    while($lformula = $_SESSION['fetch_array']($eformula)) {
                        if($lformula['idformulas'] == $econsestru['idformula']) {
                            echo "<option value=\"".$lformula['idformulas']."\" selected=\"selected\">".$lformula['nomeformulas']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lformula['idformulas']."\">".$lformula['nomeformulas']."</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
            </tr>
            <tr id="tdtab">
                <td class="corfd_coltexto"><strong>TABULAÇÃO</strong></td>
                <td class="corfd_colcampos">
                    <select name="tabulacao" id="tabulacao" style="width:230px">
                        <?php
                        if($econsestru['tabulacao'] == "") {
                            echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                        }
                        else {
                            echo "<option value=\"\">SELECIONE...</option>";
                        }
                        foreach($tabsdb as $ktab => $tab) {
                            if($econsestru['tabulacao'] == $ktab) {
                                echo "<option value=\"$ktab\" selected=\"selected\">".$tab."</option>";
                            } 
                            else {
                                echo "<option value=\"$ktab\">".$tab."</option>";
                            }
                        }
                        ?>                        
                    </select>
                </td>
            </tr>
          <tr>
            <td class="corfd_coltexto"><strong>APRESENTAÇÃO DADOS</strong></td>
            <td class="corfd_colcampos" colspan="3">
            	<select name="apresdados" id="apresdados" style="width:230px">
                    <?php
                    if($idestru != "" && $tipoestru == "corpo") {
                        $apresdados = array('TABELA','GRAFICO');
                        foreach($apresdados as $ad) {
                            if($ad == $econsestru['apresdados']) {
                                echo "<option value=\"".$ad."\" selected=\"selected\">".$ad."</option>";
                            }
                            else {
                                echo "<option value=\"".$ad."\">".$ad."</option>";
                            }
                        }
                    }
                    else {
                    ?>
                    <option value="" disabled="disabled" readonly="readonly" selected="selected">SELECIONE...</option>
                    <option value="TABELA">TABELA</option>
                    <option value="GRAFICO">GRAFICO</option>
                    <?php
                    }
                    ?>
                </select>
            
            </td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>TIPO GRÁFICO</strong></td>
            <td class="corfd_colcampos" colspan="3">
            	<select name="tipograf" id="tipograf" style="width:230px">
                    <?php
                    if($econsestru['dados'] == "IT") {
                        $tipograf = array('NULL' => 'NENHUM', 'bar' => 'BARRAS','column' => 'COLUNAS');
                    }
                    else {
                        $tipograf = array('NULL' => 'NENHUM', 'bar' => 'BARRAS','column' => 'COLUNAS', 'line' => 'LINHAS', 'spline' => 'CURVAS', 'bar,spline' => 'BARRAS, CURVAS', 'bar,line' => 'BARRAS, LINHAS', 'column,line' => 'COLUNAS, LINHAS', 'column,spline' => 'COLUNAS, CURVAS');
                    }
                    if($idestru != "" && $tipoestru == "corpo") {
                        echo "<option value=\"\" disabled=\"disabled\" readonly=\"readonly\">SELECIONE...</option>";
                        foreach($tipograf as $kgraf => $graf) {
                            if($kgraf == $econsestru['tipografico']) {
                                echo "<option value=\"".$kgraf."\" selected=\"selected\">".$graf."</option>";
                            }
                            else {
                                echo "<option value=\"".$kgraf."\">".$graf."</option>";
                            }
                        }
                    }
                    else {
                    ?>
                    <option value="" disabled="disabled" readonly="readonly" selected="selected">SELECIONE...</option>
                    <?php
                    $tipograf = array('NULL' => 'NENHUM', 'bar' => 'BARRAS','column' => 'COLUNAS', 'line' => 'LINHAS', 'spline' => 'CURVAS', 'bar,spline' => 'BARRAS, CURVAS', 'bar,line' => 'BARRAS, LINHAS', 'column,line' => 'COLUNAS, LINHAS', 'column,spline' => 'COLUNAS, CURVAS');
                    foreach($tipograf as $kgraf => $graf) {
                        echo "<option value=\"".$kgraf."\">".$graf."</option>";
                    }
                    }
                    ?>
                </select>            
            </td>
          </tr>
          <tr id="paramgraf">
              <td colspan="4">
        <table width="534">
          <tr>
            <td align="center" colspan="4" class="corfd_ntab"><strong>CONFIGURAÇÃO GRÁFICO</strong></td>
          </tr>
          <tr>
            <td class="corfd_coltexto" width="129"><strong>COR FUNDO</strong></td>
            <td class="corfd_colcampos"><input id="corfdgraf" name="corfdgraf" type="text" maxlength="6" style="width:100px; border: 1px solid #9CF; text-align: center" class="color" value="<?php echo $econsestru['corfdgraf'];?>" /></td>
            <td width="125" class="corfd_coltexto"><strong>COR DADOS</strong></td>
            <td class="corfd_colcampos"><input type="text" name="cordados" id="cordados" maxlength="6" style="width:100px; border: 1px solid #9CF; text-align: center" class="color" value="<?php if($idestru != "") { echo $econsestru['cordados']; } else { echo "000000";}?>" /></td>
          </tr>
          <tr>
              <td width="125" class="corfd_coltexto"><strong>MARGEM TOPO</strong></td>
              <td width="105" class="corfd_colcampos"><input name="margem" id="margem" maxlength="3" style="width:100px; text-align:center; border: 1px solid #9CF" value="<?php if($idestru != "") { echo $econsestru['margem'];} else { echo "110"; };?>" /></td>
              <td width="125" class="corfd_coltexto"><strong>MARGEM BASE</strong></td>
              <td width="105" class="corfd_colcampos"><input name="marginbottom" id="marginbottom" maxlength="3" style="width:100px; text-align:center; border: 1px solid #9CF" value="<?php if($idestru != "") { echo $econsestru['marginbottom'];} else { echo "100"; };?>" /></td>
          </tr>
          <tr>
              <td class="corfd_coltexto"><strong>TAM. PONTO X</strong></td>
              <td class="corfd_colcampos" colspan="3"><input type="text" style="text-align:center; border: 1px solid #9CF; width:100px" maxlength="3" name="pointwidth" id="pointwidth" value="<?php if($idestru != "") { echo $econsestru['pointwidth'];} else { echo "20"; }?>" /></td>
          </tr>
          <tr>
              <td class="corfd_coltexto"><strong>VALORES</strong></td>
              <td class="corfd_colcampos">
                  <select name="valores" id="valores" style="width:100px; text-align:center">
                      <?php 
                      $valores = array('S','N');
                      foreach($valores as $v) {
                          if($v == $econsestru['valores']) {
                              echo "<option value=\"$v\" selected=\"selected\">$v</option>";
                          }
                          else {
                              echo "<option value=\"$v\">$v</option>";
                          }
                      }
                      ?>                      
                  </select>
              </td>
              <td class="corfd_coltexto"><strong>ROTAÇÃO</strong></td>
              <td class="corfd_colcampos"><input name="rotacaoval" id="rotacaoval" style="width:100px; border: 1px solid #9CF; text-align:center" value="<?php if($idestru != "") { echo $econsestru['rotacaoval']; } else { echo "0"; }?>" /></td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>TAM. HORIZONTAL</strong></td>
            <td class="corfd_colcampos"><input name="widthgraf" id="widthgraf" maxlength="4" style="width:100px; border: 1px solid #9CF; text-align:center" value="<?php if($idestru != "") { echo $econsestru['widthgraf']; } else { echo "1024"; }?>"  /></td>
            <td class="corfd_coltexto"><strong>TAM. VERTICAL</strong></td>
            <td class="corfd_colcampos"><input name="heightgraf" id="heightgraf" maxlength="4" style="width:100px; border: 1px solid #9CF; text-align:center" value="<?php if($idestru != "") { echo $econsestru['heightgraf'];} else { echo "600"; } ?>"  /></td>
          </tr>
          <tr>
          <td width="129" class="corfd_coltexto"><strong>TEXTO ROTAÇÃO</strong></td>
            <td width="155" class="corfd_colcampos">
                <select name="textrotacao" id="textrotacao" style="width:100px; text-align: center">
                <?php
                $labelsX = array('0','-45','-90','45','90');
                foreach($labelsX as $label) {
                    if($label == $econsestru['textrotacao']) {
                            echo "<option value=\"".$label."\" selected=\"selected\">".$label."</option>";
                    }
                    else {
                            echo "<option value=\"".$label."\">".$label."</option>";
                    }
                }
                ?>
                </select>
            </td>
            <td class="corfd_coltexto"><strong>LEGENDA</strong></td>
            <td class="corfd_colcampos">
                <select name="legenda" id="legenda" style="width:100px; text-align: center">
                <?php
                $legenda = array('true','false');
                foreach($legenda as $lg) {
                    if($lg == $econsestru['legenda']) {
                            echo "<option value=\"".$lg."\" selected=\"selected\">".$lg."</option>";
                    }
                    else {
                            echo "<option value=\"".$lg."\">".$lg."</option>";
                    }
                }
                ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>POSIÇÃO HOR. LEG.</strong></td>
            <td class="corfd_colcampos">
                <select name="posileg" id="posileg" style="width:100px; text-align: center">
                <?php
                $posihor = array('esquerda','centro','direita');
                foreach($posihor as $ph) {
                    if($ph == $econsestru['posileg']) {
                            echo "<option value=\"".$ph."\" selected=\"selected\">".$ph."</option>";
                    }
                    else {
                            echo "<option value=\"".$ph."\">".$ph."</option>";
                    }
                }
                ?>
                </select>
            </td>
			<td class="corfd_coltexto"><strong>ORIENTAÇÃO LEG.</strong></td>
            <td class="corfd_colcampos">
                <select name="orientaleg" id="orientaleg" style="width:100px; text-align: center">
                <?php
                $orileg = array('vertical','horizontal');
                foreach($orileg as $ol) {
                    if($ol == $econsestru['orientaleg']) {
                            echo "<option value=\"".$ol."\" selected=\"selected\">".$ol."</option>";
                    }
                    else {
                            echo "<option value=\"".$ol."\" >".$ol."</option>";
                    }
                }
                ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>POSIÇÃO X</strong></td>
            <td class="corfd_colcampos"><input name="posicaoX" id="posicaoX" maxlength="3" style="width:100px; text-align:center; border: 1px solid #9CF" value="<?php if($idestru != "") { echo $econsestru['posicaoX']; } else {echo "-10";}?>" /></td>
            <td class="corfd_coltexto"><strong>POSIÇÃO Y</strong></td>
            <td class="corfd_colcampos"><input name="posicaoY" id="posicaoY" maxlength="3" style="width:100px; text-align:center; border: 1px solid #9CF" value="<?php if($idestru != "") { echo $econsestru['posicaoY']; } else {echo "60";}?>" /></td>
          </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td colspan="4">
            	<table width="534" id="confcolgraf">
                  <tr>
                    <td width="163" height="15" align="center" class="corfd_coltexto"><strong>COR</strong></td>
                    <td width="190" align="center" class="corfd_coltexto"><strong>CAMPO</strong></td>
                    <td width="165" align="center" class="corfd_coltexto"><strong>TIPO GRÁFICO</strong></td>
                  </tr>
                    <?php
                    $selform = "SELECT * FROM formulas WHERE idplanilha<>''";
                    $eselform = $_SESSION['query']($selform) or die ("erro na query de consulta das formulas vinculas a planilhas");
                    while($lselform = $_SESSION['fetch_array']($eselform)) {
                        //$formulas[] = strtoupper(trim(str_replace(" ", "_", $lselform['nomeformulas'])));
                        $formulas[] = strtoupper("perc_".trim(str_replace(" ", "_", $lselform['nomeformulas'])));
                    }
                    $colsgraf = array('F' => 'QTDE_MONI,RESULTADO','MO' => 'MEDIA,PERC_FG','MS' => 'MEDIA,PERC_FG','P' => 'POSSIVEL,MEDIA','PG' => 'PERC,PERC_ACUMU','PI' => 'PERC,PERC_ACUMU','A' => 'QTDE_IMP,QTDE_REAL','IT' => implode(",",$formulas), 'TAB' => 'QTDE,PERC');
                        foreach($colsgraf as $dados => $colgraf) {
                            $cols = explode(",",$colgraf);
                            foreach($cols as $col) {
                                if($dados == $econsestru['dados']) {
                                    $selconfcol = "SELECT *, COUNT(*) as result FROM graficocorpo WHERE idrelresultcorpo='".$econsestru['idrelresultcorpo']."' AND campo='$col'";
                                    $eselconf = $_SESSION['fetch_array']($_SESSION['query']($selconfcol)) or die ("erro na query de consulta das configurações das colunas do gráfico");
                                }
                                else {
                                }

                            ?>
                          <tr class="tr_<?php echo $dados;?>">
                            <td height="16" align="center" class="corfd_colcampos"><input id="cor<?php echo $col;?>" name="cor<?php echo $dados.$col;?>" type="text" class="color" style="text-align:center" value="<?php echo $eselconf['cor'];?>" /></td>
                            <td align="center" class="corfd_colcampos"><input name="<?php echo $col;?>" id="<?php echo $col;?>" type="hidden" value="<?php echo $col;?>" style="border: 1px solid #9CF"  /><?php echo $col;?></td>
                            <td align="center" class="corfd_colcampos">
                            	<select name="tp<?php echo $dados.$col;?>" class="tp">
                                    <?php
                                    if($dados == $econsestru['dados']) {
                                        $grafico = explode(",",$tipograf[$econsestru['tipografico']]);
                                        foreach($grafico as $g) {
                                            $gf = array_search(trim($g), $tipograf);
                                            if($gf == $eselconf['tipograf']) {
                                                echo "<option value=\"".$gf."\" selected=\"selected\">".$g."</option>";
                                            }
                                            else {
                                                echo "<option value=\"".$gf."\">".$g."</option>";
                                            }
                                            $gf = "";
                                        }
                                    }
                                    else {
                                    }
                                    ?>
                                </select>
                            </td>
                          </tr>
                      <?php
                            }
                        }
                        ?>
                </table>
            </td>
          </tr>
          <tr>
            <td colspan="4">
            <?php
            if(($idestru == "" && $tipoestru == "corpo" && $sitestru == "cad") OR ($linhas == 0 && $tipoestru == "" && $sitestru == "") OR ($idestru != "" && $tipoestru == "cab" && $tipoestru != "corpo")) {
            ?>
                <input type="submit" name="cadcorpo" id="cadcorpo" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Cadastrar" />
            <?php
            }
            if($idestru != "" && $tipoestru == "corpo" && $sitestru == "alt") {
            ?>
                <input type="submit" name="altcorpo" id="altcorpo" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Alterar" /> <input type="submit" name="novocorpo" id="novcorpo" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Novo" />
            <?php
            }
            ?>
            </td>
          </tr>
        </table>
        <font color="#FF0000"><strong><?php echo $_GET['msgcorpo']; ?></strong></font>
        </form><br /><br />
      <div id="estrutura">
      	<div id="cab" style="width:1018px; border: 2px solid #999">
          <table width="769">
          	<tr>
            	<td align="center" colspan="7" class="corfd_ntab"><strong>CABEÇALHO CADASTRADO</strong></td>
                <td></td>
            </tr>
              <tr>
                <td width="30" align="center" class="corfd_coltexto"><strong>ID CAB.</strong></td>
                <td width="36" align="center" class="corfd_coltexto"><strong>ID REL.</strong></td>
                <td width="51" align="center" class="corfd_coltexto"><strong>QTDE. LOGOS</strong></td>
                <td width="184" align="center" class="corfd_coltexto"><strong>LOGO</strong></td>
                <td width="60" align="center" class="corfd_coltexto"><strong>P. LOGO</strong></td>
                <td width="131" align="center" class="corfd_coltexto"><strong>CAMPOS</strong></td>
                <td width="70" align="center" class="corfd_coltexto"><strong>P. CAMPOS</strong></td>
                <td width="171" align="center"><strong></strong></td>
              </tr>
                  <?php
                  $posicao = array('C' => 'CENTRO','D' => 'DIREITA', 'E' => 'ESQUERDA');
                  while($lcab = $_SESSION['fetch_array']($ecab)) {
                    $caminhos = explode(",",$lcab['caminhologo']);
                    $posilogos = explode(",",$lcab['posilogo']);
                    $campos = explode(",",$lcab['campos']);
                    $posicampos = explode(",",$lcab['posicampos']);

                  ?>
              <form action="cadrelresult.php" method="post">
              <tr>
                <td class="corfd_colcampos" align="center"><input name="idrelresult" type="hidden" value="<?php echo $idrel;?>" /><input name="idestru" type="hidden" value="<?php echo $lcab['idrelresultcab'];?>" /><input name="arquivo" type="hidden" value="<?php echo $lcab['caminhologo'];?>" /><?php echo $lcab['idrelresultcab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lcab['idrelresult'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lcab['qtdelogos'];?></td>
                <td class="corfd_colcampos" align="center">
                    <?php
                    foreach($caminhos as $c) {
                        $arq = explode("/",$c);
                        echo end($arq)."<br />";
                    }
                    ?>
                </td>
                <td class="corfd_colcampos" align="center">
                    <?php
                    foreach($posilogos as $pl) {
                        echo $posicao[$pl]."<br />";
                    }
                    ?>
                </td>
                <td class="corfd_colcampos" align="center">
                    <?php
                    foreach($campos as $ca) {
                        echo $ca."<br />";
                    }
                    ?>
                </td>
                <td class="corfd_colcampos" align="center">
                    <?php
                    foreach($posicampos as $pc) {
                        echo $posicao[$pc]."<br />";
                    }
                    ?>
                </td>
                <td>
                    <input type="submit" name="carcab" id="carcab" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Carregar" />
                    <input type="submit" name="delcab" id="delcab" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Apagar" />
                </td>
              </tr>
              </form>
              <?php
              }
              ?>
            </table><font color="#FF0000"><strong><?php echo $_GET['msgcadcab']; ?></strong></font>
            </div> <br /><br />
            <div id="corpo" style="width:1018px; height:200px; overflow:auto; border: 2px solid #999">
            <table width="950">
          	<tr>
            	<td align="center" colspan="8" class="corfd_ntab"><strong>CORPO CADASTRADO</strong></td>
                <td width="170"></td>
            </tr>
              <tr>
                <td width="47" align="center" class="corfd_coltexto"><strong>ID CORPO.</strong></td>
                <td width="36" align="center" class="corfd_coltexto"><strong>ID REL.</strong></td>
                <td width="85" align="center" class="corfd_coltexto"><strong>TIPO DADOS</strong></td>
                <td width="85" align="center" class="corfd_coltexto"><strong>FREQUENCIA</strong></td>
                <td width="150" align="center" class="corfd_coltexto"><strong>DADOS</strong></td>
                <td width="130" align="center" class="corfd_coltexto"><strong>FORMULA</strong></td>
                <td width="113" align="center" class="corfd_coltexto"><strong>VISUALIZA</strong></td>
                <td width="136" align="center" class="corfd_coltexto"><strong>TIPO GRAF.</strong></td>
                <td></td>
              </tr>
                <?php
                $tipograf = array('NULL' => 'NENHUM', 'bar' => 'BARRAS','column' => 'COLUNAS', 'line' => 'LINHAS', 'spline' => 'CURVAS', 'bar,spline' => 'BARRAS, CURVAS', 'bar,line' => 'BARRAS, LINHAS', 'column,line' => 'COLUNAS, LINHAS', 'column,spline' => 'COLUNAS, CURVAS');
                $dados = array('F' => 'FORMULA','MO' => 'MÉDIA OPERADOR','MS' => 'MÉDIA SUPERVISOR','P' => 'PLANIHA','PI' => 'PARETO ITENS','PG' => 'PARETO GRUPOS','A' => 'AMOSTRA','IT' => "INTERVALO NOTAS", 'TAB' => 'TABULAÇÃO');
                $frequen = array('G' => 'GERAL','S' => 'SEMANAL','D' => 'DIARIO','M' => 'MENSAL');
                while($lcorpo = $_SESSION['fetch_array']($ecorpo)) {
                    $selnform = "SELECT nomeformulas, COUNT(*) as result FROM formulas WHERE idformulas='".$lcorpo['idformula']."'";
                    $eselform = $_SESSION['fetch_array']($_SESSION['query']($selnform)) or die ("erro na query de consulta do nome da formula");

                ?>
                <form action="cadrelresult.php" method="post">
                  <tr>
                    <td class="corfd_colcampos" align="center"><input name="idestru" type="hidden" value="<?php echo $lcorpo['idrelresultcorpo'];?>" /><?php echo $lcorpo['idrelresultcorpo'];?></td>
                    <td class="corfd_colcampos" align="center"><input name="idrel" type="hidden" value="<?php echo $lcorpo['idrelresult'];?>" /><?php echo $lcorpo['idrelresult'];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $tipodados[$lcorpo['tipodados']];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $frequen[$lcorpo['frequenciadados']];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $dados[$lcorpo['dados']];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $eselform['nomeformulas'];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $lcorpo['apresdados'];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $tipograf[$lcorpo['tipografico']];?></td>
                    <td>
                        <input type="submit" name="carcorpo" id="carcorpo" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Carregar" />
                    <input type="submit" name="delcorpo" id="delcorpo" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Apagar" />
                    </td>
                  </tr>
                </form>
                <?php
                }
                ?>
            </table> <br /><br />
            </div>
      </div>
      <?php
      }
      ?>
      <hr />
      <div id="relcad">
            <table width="1024">
              <tr>
                <td align="center" colspan="6" class="corfd_ntab"><strong>RELATÓRIOS CADASTRADOS</strong></td>
              </tr>
              <tr>
                <td width="34" align="center" class="corfd_coltexto"><strong>ID</strong></td>
                <td width="226" align="center" class="corfd_coltexto"><strong>NOME</strong></td>
                <td width="90" align="center" class="corfd_coltexto"><strong>PERFIL. ADM</strong></td>
                <td width="90" align="center" class="corfd_coltexto"><strong>PERFIL. WEB</strong></td>
                <td width="150" align="center" class="corfd_coltexto"><strong>FILTROS</strong></td>
                <td width="186" align="center" class="corfd_coltexto"><strong>DESCRIÇÃO</strong></td>
                <td width="217" align="center"></td>
              </tr>
              <?php
              $relcad = "SELECT * FROM relresult";
              $erelcad = $_SESSION['query']($relcad) or die ("erro na query de consulta dos relatórios cadastrados");
              while($lrelcad = $_SESSION['fetch_array']($erelcad)) {
              ?>
                <form action="cadrelresult.php" method="post">
                <tr>
                  <td align="center" class="corfd_colcampos"><input name="idrel" id="idrel" type="hidden" value="<?php echo $lrelcad['idrelresult'];?>" /><?php echo $lrelcad['idrelresult'];?></td>
                  <td align="center" class="corfd_colcampos"><?php echo $lrelcad['nomerelresult'];?></td>
                  <td align="center" class="corfd_colcampos"><?php echo $lrelcad['idsperfadm'];?></td>
                  <td align="center" class="corfd_colcampos"><?php echo $lrelcad['idsperfweb'];?></td>
                  <td align="center" class="corfd_colcampos">
                  <?php
                  $filtros = array('dtini' => 'DATA INICIAL', 'dtfim' => 'DATA DATA FINAL','dataini' => 'DATA INI CONTATO','datafim' => 'DATA FIM CONTATO', 'oper' => 'OPERADOR','super' => 'SUPERVISOR','monitor' => 'MONITOR', 'filtros' => 'FILTROS REL','aval' => 'AVALIAÇÃO', 'plan' => 'PLANILHA','grupo' => 'GRUPO', 'sub' => 'SUBGRUPO', 'perg' => 'PERGUNTA', 'resp' => 'RESPOSTA', 'fg' => 'FAHA GRAVE', 'fgm' => 'FALHA GRAVISSIMA');
                  $filt = explode(",",$lrelcad['filtros']);
                  foreach($filt as $f) {
                      echo $filtros[$f]."<br />";
                  }
                  ?></td>
                  <td class="corfd_colcampos"><?php echo $lrelcad['descricao'];?></td>
                  <td><input type="submit" name="carrel" id="carrel" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Carregar" /> <input type="submit" name="estrurel" id="estrurel" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Estrutura" /> <input type="submit" name="delrel" id="delrel<?php echo $lrelcad['idrelresult'];?>" style="background-image:url(../images/button.jpg); height:18px; border: 1px solid #FFF" value="Apagar" /></td>
                </tr>
                </form>
              <?php
              }
              ?>
            </table>
      </div>  
</div>
</body>
</html>
