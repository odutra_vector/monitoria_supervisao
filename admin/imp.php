<?php

include_once($rais.'/monitoria_supervisao/admin/visufiltros.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');

$atab = new TabelasSql();

$iduser = $_SESSION['usuarioID'];
$ultimo = "SELECT COUNT(*) as result FROM filtro_nomes WHERE ativo='S' ORDER BY nivel LIMIT 1";
$eultimo = $_SESSION['fetch_array']($_SESSION['query']($ultimo)) or die (mysql_error());
if($eultimo['result'] >= 1) {
    $ultimo = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel LIMIT 1";
    $eultimo = $_SESSION['fetch_array']($_SESSION['query']($ultimo)) or die (mysql_error());
    $ftunico = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel";
    $eftunico = $_SESSION['query']($ftunico) or die (mysql_error());
    while($lunico = $_SESSION['fetch_array']($eftunico)) {
        $filtros[$lunico['nomefiltro_nomes']] = "id_".strtolower($lunico['nomefiltro_nomes']);
        $schunico = "SELECT idfiltro_dados,nomefiltro_dados,count(*) as r FROM filtro_dados WHERE idfiltro_nomes='".$lunico['idfiltro_nomes']."' AND nomefiltro_dados='IMPUNICO'";
        $eschunico = $_SESSION['fetch_array']($_SESSION['query']($schunico)) or die (mysql_error());
        $rel[$lunico['nomefiltro_nomes']] = "id_".strtolower($lunico['nomefiltro_nomes'])."='".$eschunico['idfiltro_dados']."'";
    }
    if($rel) {
        $selrel = "SELECT idrel_filtros,count(*) as r FROM rel_filtros WHERE ".implode(" AND ",$rel);
        $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
        $idrelunico = $eselrel['idrel_filtros']; 
    }
}
//$ultimo = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel LIMIT 1";
//$eultimo = $_SESSION['fetch_array']($_SESSION['query']($ultimo)) or die (mysql_error());
if($_SESSION['user_tabela'] == "user_adm") {
    $tabuser = "user_adm";
    $tabreluser = "useradmfiltro";
    $atabuser = "a";
    $atabreluser = "af";
}
if($_SESSION['user_tabela'] == "user_web") {
    $tabuser = "user_web";
    $tabreluser = "userwebfiltro";
    $atabuser = "w";
    $atabreluser = "wf";
}

$part = explode("_",$tabuser);
$part = $part[1];
$cruzarel = "SELECT $atabreluser.idrel_filtros FROM $tabuser $atabuser
            INNER JOIN $tabreluser $atabreluser ON $atabreluser.iduser_$part = $atabuser.id".$_SESSION['user_tabela']."
            INNER JOIN rel_filtros rf ON rf.idrel_filtros = $atabreluser.idrel_filtros
            WHERE $atabuser.id".$_SESSION['user_tabela']."='$iduser' AND $atabuser.import='S'";
$ecruzarel = $_SESSION['query']($cruzarel) or die ("erro na query de consulta dos relacionamentos vinculados ao usuário");
$ncruzarel = $_SESSION['num_rows']($ecruzarel);
if($ncruzarel >= 1) {
    while($lcruzarel = $_SESSION['fetch_array']($ecruzarel)) {
        $idsrelperf[] = $lcruzarel['idrel_filtros'];
    }
}
else {
}

$periodo = periodo('datas','datactt');
$selper = "SELECT nmes, ano, COUNT(*) as result FROM periodo WHERE idperiodo='".$periodo['idperiodo']."'";
$eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta do nome do periodo");
$nomeperiodo = $eselper['ano']."-".$eselper['nmes'];

$semana = 1;
$dataini = date('Y-m-d');
$datafim = date('Y-m-d');

?>

<script type="text/javascript" src="users/combofiltros_<?php echo strtolower($_SESSION['nomecli']);?>.js"></script>
<link rel="stylesheet" type="text/css" href="/monitoria_supervisao/js/custom-theme/jquery-ui-1.8.2.custom.css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dataini.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/datafim.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtini.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtfim.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtfimimp.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtiniimp.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtinifilt.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtfimfilt.js"></script>
<style type="text/css">
    a.linque:hover {
    display:block;
    color:#fff;
    background-color:#69C;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
        var idsrel = "<?php echo implode(",",$idsrelperf);?>";
        $('#idrelimp').live("change",function() {
            var dataini = $('#dtinifilt').val();
            var datafim = $('#dtfimfilt').val();
            var idrel = $('#idrelimp').val();
            $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5,
            color: '#fff'
            }})
           $('#histimp').load('/monitoria_supervisao/admin/loadimport.php', {idrel: idrel, dataini: dataini, datafim: datafim, idsrelperf: idsrel});
        });
        //$('#tabhist').tablesorter();
        $("input[id*='dataini']").mask('99/99/9999');
        $("input[id*='datafim']").mask('99/99/9999');
        $("input[id*='dtiniimp']").mask('99/99/9999');
        $("input[id*='dtfimimp']").mask('99/99/9999');
        $("input[id*='datactt']").mask('99/99/9999');
        $("div[id*='divrel']").hide();

        <?php
        if(isset($_GET['idrel']) && $_GET['idrel'] != "") {
            echo "$('#divrel".$_GET['idrel']."').show();\n";
            echo "$('#".$_GET['idrel']."').css('background-color','#FFCC99');\n";
        ?>
        $('tr.idrel').click(function() {
            $("tr[class*='idrel']").css('background-color','#FFFFFF');
            $("div[id*='divrel']").hide();
            var idrel = $(this).attr('id');
            $(this).css('background-color','#FFCC99');
            $('#divrel'+idrel).show();
        });
        <?php
        }
        else {
        ?>
        $('tr.idrel').click(function() {
            $("tr[class*='idrel']").css('background-color','#FFFFFF');
            $("div[id*='divrel']").hide();
            var idrel = $(this).attr('id');
            $(this).css('background-color','#FFCC99');
            $('#divrel'+idrel).show();
        });
        <?php
        }
        ?>

        //$("form[id*='formupload'").submit(function() {
        $("input[id^='enviar']").live('click',function() {
                var opimp = $("#opcaoimp").val();
                var upidrel = $(this).attr('id');
                var idrel = upidrel.substr(6);
                //var file = new File('#audioup'+idrel).val();
                var idperiodo = $('#periodo'+idrel).val();
                var semana = $('#semana'+idrel).val();
                var arqup = $('#arquivoup'+idrel).val();
                var audioup = $('#audioup'+idrel).val();
                var tipoimp = $('#tipoimp'+idrel).val();
                //alert(idrel+','+idperiodo+','+semana+','+arqup);
                //$('#arquivoup'+idrel).attr('value','1');

                if(opimp == "U") {
                    if(tipoimp == "LA") {
                        if(audioup == "" && arqup == "") {
                            alert("Por favor, informe um arquivo para envio ao servidor");
                            return false;
                        }
                    }
                    if(tipoimp == "I" || tipoimp == "O" || tipoimp == "L") {
                        if(arqup == "") {
                            alert("Favor informa um arquivo para envio");
                            return false;
                        }
                    }
                    if(tipoimp == "A" || tipoimp == "S") {
                        if(audioup == "") {
                            alert("Favor informa um arquivo para envio");
                            return false;
                        }
                    }
                    
                    if($("#idparam").val() == "") {
                        alert("Por favor, informe o parametro que será utilizado na importação");
                        return false;
                    }
                    else {
                        $.blockUI({ message: '<strong>AGUARDE EXECUTANDO O UPLOAD DOS ARQUIVOS...</strong>', css: { 
                        border: 'none', 
                        padding: '15px', 
                        backgroundColor: '#000', 
                        '-webkit-border-radius': '10px', 
                        '-moz-border-radius': '10px', 
                        opacity: .5,
                        color: '#fff',
                        timeout: 0,
                        }});
                    }
                }
                else {
                    if(tipoimp == "LA") {
                        if(arqup == "" || audioup == "") {
                            alert("Favor informa os arquivos para envio");
                            return false;
                        }
                        else {
                            $.blockUI({ message: '<strong>AGUARDE EXECUTANDO O UPLOAD DOS ARQUIVOS...</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5,
                            color: '#fff',
                            timeout: 0,
                            }});
                        }
                    }
                    if(tipoimp == "I" || tipoimp == "O" || tipoimp == "L") {
                        if(arqup == "") {
                            alert("Favor informa um arquivo para envio");
                            return false;
                        }
                        else {
                            $.blockUI({ message: '<strong>AGUARDE EXECUTANDO O UPLOAD DOS ARQUIVOS...</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5,
                            color: '#fff',
                            timeout: 0,
                            }});
                        }
                    }
                    if(tipoimp == "A" || tipoimp == "S") {
                        if(audioup == "") {
                            alert("Favor informa um arquivo para envio");
                            return false;
                        }
                        else {
                            var parte = audioup.split("\\");
                            if(parte.length > 1) {
                                var nomearq = parte[(parte.length-1)];
                                nomearq = nomearq.split(".");
                            }
                            else {
                                var nomearq = audioup.split(".");
                            }                      
                            if(nomearq[0] != idrel) {
                                alert('O arquivo com os audios não possui o mesmo código que o relacionamento escolhido, favor verificar!!!');
                                return false;
                            }
                            else {
                                $.blockUI({ message: '<strong>AGUARDE EXECUTANDO O UPLOAD DOS ARQUIVOS...</strong>', css: { 
                                border: 'none', 
                                padding: '15px', 
                                backgroundColor: '#000', 
                                '-webkit-border-radius': '10px', 
                                '-moz-border-radius': '10px', 
                                opacity: .5,
                                color: '#fff',
                                timeout: 0,
                                }})
                            }
                        }
                    }
                }
            })
            
            $("#idparam").change(function() {
                var id = $(this).val();
                var idrel = '<?php echo $idrelunico;?>';
                $("#divinputarq").empty();
                $("#divinputarq").load("/monitoria_supervisao/admin/importa.php",{id:id,idrel:idrel,param:'param'});
            })
            
            $("input[id*='importa']").click(function() {
                var opcao = $("input[name='acaoimport']:checked").val();
                if(opcao != "i" && opcao != "v") {
                    alert("Favor selecionar uma opção entre ''VERIFICAÇÃO'' ou ''IMPORTAÇÃO''");
                    return false;
                }
                else {
                    var idsup = "";
                    $("input[id*='idupload']").each(function() {
                        if($(this).attr("checked")) {
                            if(idsup == "") {
                                idsup = $(this).val();
                            }
                            else {
                                idsup = idsup+","+$(this).val();
                            }
                        }
                    })
                     var idsarray = idsup.split(",");
                    var cidsup = idsarray.length;
                    if(idsup == "") {
                        alert("Favor Selecionar 1 upload para apagar os dados");
                        return false;
                    }
                    else {
                        if(cidsup > 1) {
                            alert("Para executar a importação somente 1 UPLOAD pode estar marcado");
                            return false;
                        }
                        else {
                            $.blockUI({ message: '<strong>AGUARDE EXECUTANDO A IMPORTAÇÃO...</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px', 
                            opacity: .5,
                            color: '#fff',
                            timeout: 0,
                            }})
                        }
                     }
                }
            })
            <?php
            if($_GET['retorno'] == 1) {
                ?>
                $.unblockUI();
                <?php
            }
            else {
            }
            ?>

     $('#pesquisa').click(function() {
         <?php
         $sfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
         $esfiltros = $_SESSION['query']($sfiltros) or die ("erro na query de consulta dos nomes dos filtros");
         while($lsfiltros = $_SESSION['fetch_array']($esfiltros)) {
            $vars[] = "filtro_".strtolower($lsfiltros['nomefiltro_nomes']);
            echo "var filtro_".strtolower($lsfiltros['nomefiltro_nomes'])." = $('#filtro_".strtolower($lsfiltros['nomefiltro_nomes'])."').val();\n";
         }
         foreach ($vars as $var) {
             $montaif[] = $var." == ".'"'.'"';
         }
         $montaif = implode(" && ",$montaif);
         echo "if(".$montaif.") {\n";
         echo "alert(".'"'."É necessário que pelo menos 1 filtro seja selecionado para fazer a pesquisa".'"'.");\n";
         echo "return false;";
         echo "}\n";
         echo "else {\n";
         echo "}\n";
         ?>
     });

    var open = true;
    var openhist = true;
    $('#tabupload').live("click",function() {
         if(open == true) {
            $('#uploadnimp').hide();
            open = false;
         }
         else {
            $('#uploadnimp').show();
            open = true;
         }
    })
    $('#tabhistimp').live("click",function() {
         if(openhist == true) {
            $('#histimp').hide();
            openhist = false;
         }
         else {
            $('#histimp').show();
            openhist = true;
         }
    });
    
    //div's importação, unico e relacionamento
    $("#msgup").hide();
    <?php /** @impunico restringe a opção de importação unica somente à usuários administradores e se o relacionamento de importação unica estiver criado*/
    if(isset($_GET['msgu'])) {
        echo '$("#msgup").show();'."\n";
    }
    
    //verificar se existe o relacionamento para importação unica
    if($idrelunico != null && $_SESSION['user_tabela'] == "user_adm") {
    ?>
    $("#filtros").hide();
    $("#unico").hide();
    <?php
    if($_GET['opimp'] == "R" OR $_GET['tipoimp'] == "R") {
        echo '$("#filtros").show();'."\n";
    }
    if($_GET['tipoimp'] == "U") {
        echo '$("#unico").show()'."\n";
    }
    ?>
    
    $("#opcoes").click(function() {
       var op = $(this).val();
       if(op == "R") {
           $("#filtros").show();
           $("#unico").hide();
           $("#formupload").prepend('<input type=\"hidden\" name=\"opcaoimp\" id=\"opcaoimp\" value=\"R\" />');
       }
       if(op == "U") {
           $("#filtros").hide();
           $("#tabrel").remove();
           $("div[[id*='divrel']").remove();
           $("#unico").show();
           $("#formupload").prepend('<input type=\"hidden\" name=\"opcaoimp\" id=\"opcaoimp\" value=\"U\" />');
       }
    });
    <?php
    }
    ?>

    $("#alteraup").click(function() {
        var idsup = "";
        var esquerda = (screen.width - 1024)/2;
        var topo = (screen.height - 400)/2;
        $("input[id*='idupload']").each(function(i) {
            if($(this).attr("checked")) {
                if(idsup == "") {
                    idsup = $(this).val();
                }
                else {
                    idsup = idsup+","+$(this).val();
                }
            }
        });
        var idsarray = idsup.split(",");
        cidsup = idsarray.length;
        if(idsup == "") {
            alert("Favor Selecionar 1 upload para alteração de dados");
            return false;
        }
        else {
            if(cidsup > 1) {
                alert("A alteração no upload só pode ser feita com 1 upload por vez");
                return false;
            }
            else {
                window.open("/monitoria_supervisao/admin/alteraupload.php?idup="+idsup,'_blank','height=400,width=1024, top='+topo+', left='+esquerda+',scrollbars=YES,resizable=YES,Menubar=0,Status=0,Toolbar=0');
            }
        }
    });

    $("#apaga").click(function() {
        var idsup = "";
        $("input[id*='idupload']").each(function(i) {
            if($(this).attr("checked")) {
                if(idsup == "") {
                    idsup = $(this).val();
                }
                else {
                    idsup = idsup+","+$(this).val();
                }
            }
        });
        var idsarray = idsup.split(",");
        cidsup = idsarray.length;
        if(idsup == "") {
            alert("Favor Selecionar 1 upload para apagar os dados");
            return false;
        }
        else {
            if(cidsup > 1) {
                alert("Para apagar UPLOAD o sistema aceita somente uma solicitação por vez");
                return false;
            }
            else {
                var confirma = confirm("Você tem certeza que deseja APAGAR o UPLOAD!!!");
                if(confirma) {
                }
                else {
                    return false;
                }
            }
        }
    })

    $("[id*='periodo']").change(function() {
        var idperiodo = $(this).val();
        var valor = $(this).attr("id");
        var idrel = $('#idrel').val();
        alert(idperiodo);
        $('#semana'+idrel).load('/monitoria_supervisao/admin/carregaper.php',{idrel:idrel,idperiodo:idperiodo, con:'periodo'});
        $.post("/monitoria_supervisao/admin/carregaper.php",{idrel:idrel,semana:'<?php echo $semana;?>',idperiodo:idperiodo,con: 'semana'},function(valor) {
           var dados = valor.split(",");
           $('#dataini'+idrel).attr('value',dados[0]);
           $('#datafim'+idrel).attr('value',dados[1]);
        });
    })        
    $("[id*='semana']").change(function() {
        var semana = $(this).val();
        var valor = $(this).attr("id");
        var idrel = valor.substr(6, 6);
        var idperiodo = $('#periodo'+idrel).val();
        if(idperiodo == "") {
            alert('Favor preencher o período antes de informar a semana');
            return false;
        }
        else {
            $.post("/monitoria_supervisao/admin/carregaper.php",{idrel:idrel,semana:semana,idperiodo:idperiodo,con: 'semana'},function(valor) {
               var dados = valor.split(",");
               $('#dataini'+idrel).attr('value',dados[0]);
               $('#datafim'+idrel).attr('value',dados[1]);
            });
        }
    })
    $("[id*='semanaup']").change(function() {
        var semana = $(this).val();
        var valor = $(this).attr("id");
        var idup = valor.substr(8, 6);
        var idperiodo = $('#periodoup'+idup).val();
        $.post("/monitoria_supervisao/admin/carregaper.php",{idup:idup,semana:semana,idperiodo:idperiodo,con: 'semana'},function(valor) {
           var dados = valor.split(",");
           $('#dataini'+idup).attr('value',dados[0]);
           $('#datafim'+idup).attr('value',dados[1]);
        });
    });
    
    $("input[id^='arquivo']").bind("change",function() {
        if(this.files[0].size > 31457280) {
            alert('O tamanho do arquivo excede o tamanho permitido, 30MB');
            $(this).attr('value','');
        }
    });
    
    $("input[id^='audioup']").bind("change",function() {
        if(this.files[0].size > 104857600) {
            alert('O tamanho do arquivo excede o tamanho permitido, 100MB');
            $(this).attr('value','');
        }
    })
});
</script>
<body>
<div style="margin-top: 10px">
    <?php
    if(isset($_SESSION['import'])) {
        if($_SESSION['import'] == "S") {
            ?>
            <div id="msgup" style="display: none;width: 95%;padding: 10px;margin: auto;margin-bottom: 10px;text-align: center; background-color:#EABBBB; color:#000">
                <span style="font-size: 16px; font-weight: bold; color: #F00"><?php echo strtoupper($_GET['msgu']);?></span>
            </div>
            <?php
            if($idrelunico != null && $_SESSION['user_tabela'] == "user_adm") {
            ?>
            <div id="opcao" style="margin-top:10px; margin: auto">
                <span style=" font-weight: bold; font-size: 16px;">SELECIONE O TIPO DE IMPORTAÇÃO</span><br/>
                <select name="opcoes" id="opcoes" style=" margin-top: 10px; margin-bottom: 10px " class="select">
                    <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                    <option value="U">ÚNICO</option>
                    <option value="R">FILTRO RELACIONAMENTO</option>
                </select>
            </div>
            <div id="unico">
                <form action="admin/importa.php" method="post" enctype="multipart/form-data" name="formupload" id="formupload">
                    <div name="arquivos" id="arquivos" style="border: 2px solid #999; width:450px; padding: 5px;">
                        <input type="hidden" name="idrel" id="idrel" value="<?php echo $idrelunico;?>" />
                        <span style="font-size: 12px; font-weight: bold;">SELECIONE O ARQUIVO DE AUDIO E/OU ARQUIVO DE DADOS, "RAR OU ZIP"</span>
                        <span style="font-weight: bold">PARAMETRO: </span>
                        <select name="idparam" id="idparam" style=" margin: 5px">
                            <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                            <?php
                            $selparam = "SELECT idparam_moni FROM param_moni";
                            $eselparam = $_SESSION['query']($selparam) or die (mysql_error());
                            $nparam = $_SESSION['num_rows']($eselparam);
                            if($nparam >= 1) {
                                while($lparam = $_SESSION['fetch_array']($eselparam)) {
                                    ?>
                                    <option value="<?php echo $lparam['idparam_moni'];?>"><?php echo $lparam['idparam_moni'];?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select><br/>
                        <div id="divinputarq">
                        </div>
                        <input type="submit" name="enviar<?php echo $idrelunico;?>" id="enviar<?php echo $idrelunico;?>" value="UPLOAD" class="botao" />
                    </div>
                </form>
            </div>
            <?php
            }
            if($idrelunico == null || $_SESSION['user_tabela'] == "user_web") {
                ?>
                <div id="filtros">
                <?php
            }
            else {
                ?>
                <div id="filtros" style="display:none">
                <?php
            }
            ?>
                <form action="" method="get">
                <table style="border: 1px solid #999">
                  <tr>
                    <td class="corfd_ntab" align="center" colspan="2"><strong>FILTROS UPLOAD</strong></td>
                  </tr>
                  <tr>
                  <td width="176"><input name="menu" type="hidden" value="importa" /><input name="divpesq" type="hidden" value="pesq" />
                    <input name="iduser" type="hidden" value="<?php echo $iduser;?>" /><input name="pag" type="hidden" value="<?php echo $_GET['pag'];?>" /><input name="opimp" id="opimp" value="R" type="hidden"/></td>
                    <td colspan="3"></td>
                  </tr>
                  <?php
                  $filtnome = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                  $efilt = $_SESSION['query']($filtnome) or die (mysql_error());
                  $idfiltros = array();
                  $nomefiltros = array();
                  $url = array();
                  $urlid = array();
                  while($lfilt = $_SESSION['fetch_array']($efilt)) {
                          echo "<tr $style>";
                            echo "<td class=\"corfd_coltexto\"><strong>".$lfilt['nomefiltro_nomes']."</strong></td>";
                                echo "<td class=\"corfd_colcampos\"><select style=\"width:252px\" class=\"select\" name=\"filtro_".strtolower($lfilt['nomefiltro_nomes'])."\" id=\"filtro_".strtolower($lfilt['nomefiltro_nomes'])."\">";
                                        if($_GET['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == "") {
                                                $selected=($_GET['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == 0 ? "SELECTED" : "");
                                                echo "<option $selected value=\"\" >Selecione um filtro</option>";
                                        }
                                        else {
                                            echo "<option $selected value=\"\" >Selecione um filtro</option>";
                                        }
                                        $seldados = "SELECT fd.idfiltro_dados, fd.nomefiltro_dados FROM $tabuser $atabuser INNER JOIN $tabreluser $atabreluser ON $atabreluser.iduser_$part = $atabuser.id$tabuser
                                                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = $atabreluser.idrel_filtros
                                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower($lfilt['nomefiltro_nomes'])."
                                                    WHERE $atabuser.id$tabuser='$iduser' AND $atabuser.import='S' GROUP BY rf.id_".strtolower($lfilt['nomefiltro_nomes'])." ORDER BY nomefiltro_dados";
                                        $edados = $_SESSION['query']($seldados) or die (mysql_error());
                                        while($ldados = $_SESSION['fetch_array']($edados)) {
                                          if($_GET['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == $ldados['idfiltro_dados']) {
                                              $idfiltros[] = $ldados['idfiltro_dados'];
                                              $nomefiltros[] = 'id_'.strtolower($lfilt['nomefiltro_nomes']);
                                          }
                                          else {
                                          }
                                          $selected=($_GET['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == $ldados['idfiltro_dados'] ? "SELECTED" : "");
                                          echo "<option $selected value=\"".$ldados['idfiltro_dados']."\">".$ldados['nomefiltro_dados']."</option>";
                                        }
                                array_push($url, '&'.strtolower($lfilt['nomefiltro_nomes']).'='.$_GET['filtro_'.strtolower($lfilt['nomefiltro_nomes'])]);
                                echo "</select></td>";
                            echo "</tr>";
                  }
                  ?>
                  <tr>
                    <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; width:90px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="pesquisa" id="pesquisa" type="submit" value="Pesquisar" /></td>
                  </tr>
                  </table>
                  </form>
                  <font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font><br/>
            </div>
              <?php
                    if(isset($_GET['pesquisa'])) {
                        array_push($url, '&pesquisa=Pesquisar');
                        array_unshift($url, 'inicio.php?menu=importa&divpesq=pesq&iduser='.$iduser);
                        $url1 = implode('', $url);
                        for($p = 0; $p < $counturl; $p++) {
                        (list($keys, $vals) = each($url1));
                            $url2[] = "$keys$vals";
                        }
                        $uni = array_combine($nomefiltros, $idfiltros);
                        $count = count($uni);
                        $ultfiltro = array();
                        echo "<table width=\"700\" border=\"0\" id=\"tabrel\">";
                        foreach($uni as $nrel => $id) {
                            if(count($uni) > 1) {
                                $ultfiltro[] = "$nrel='$id'";
                            }
                            if(count($uni) ==  1) {
                                $ultfiltro[] = "$nrel=$id";
                            }
                        }
                        $sqlwhere = implode(" AND ", $ultfiltro);
                        //echo $sqlwhere;
                        $selrel = "SELECT rf.idrel_filtros as idrel, $atabreluser.id$tabreluser as iddados FROM rel_filtros rf 
                                   INNER JOIN $tabreluser $atabreluser ON $atabreluser.idrel_filtros = rf.idrel_filtros 
                                   WHERE $sqlwhere AND $atabreluser.iduser_$part='$iduser'";
                        $eselrel = $_SESSION['query']($selrel) or die (mysql_error());
                        echo "<tr>";
                        echo "<td class=\"corfd_coltexto\" width=\"700\" align=\"center\"><strong>RELACIONAMENTOS</strong></td>";
                        echo "</tr>";
                        while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                                $selconfigrel = "SELECT COUNT(*) as result, ativo FROM conf_rel WHERE idrel_filtros='".$lselrel['idrel']."' AND ativo='S'";
                                $econfigrel = $_SESSION['fetch_array']($_SESSION['query']($selconfigrel)) or die (mysql_error());
                                if($econfigrel['result'] >= 1 & $econfigrel['ativo'] == "S") {
                                    $idsrel[] = $lselrel['idrel'];
                                }
                        }
                        $idsreluser = arvoreescalar("",$_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
                        foreach($idsreluser as $idrel) {
                            if(in_array($idrel,$idsrel)) {
                                echo "<tr class=\"idrel\" id=\"".$idrel."\">";
                                echo "<td id=\"linha\" style=\"border: 1px solid #000;padding:3px;padding-left:15px\" ><strong>".nomeapres($idrel)." ($idrel)</strong><input type=\"hidden\" name=\"url\" value=\"".$url1."&idrel=".$idrel."\"><input type=\"hidden\" name=\"".strtolower($eultimo['nomefiltro_nomes'])."\" value=\"".$idrel."\"></td>";
                                echo "</tr>";
                            }
                        }
                        echo "</table><br/>";
                    }
                    foreach($idsrel as $idr) {
                        uploadrel($idr,$periodo,$semana,$nomeperiodo,$dataini,$datafim);
                    }
              ?>
            <br />
            <div style="width:99%; margin: 5px; background-color: #EABBBB">
                <span style="width: 100%;color: #F00; font-size: 15px; font-weight: bold"><?php echo str_replace(",", ", ", $_GET['msgimp']); ?></span>
            </div>
            <?php
            $selcampo = "SELECT * FROM filtro_nomes ORDER BY nivel LIMIT 1";
            $eselcampo = $_SESSION['query']($selcampo) or die (mysql_error());
            $ncampo = $_SESSION['num_rows']($eselcampo);
            if($ncampo >= 1) {
                $lselcampo = $_SESSION['fetch_array']($eselcampo);
            }
            ?>
            <div style="border: 2px solid #999; padding: 5px">
            <form action="admin/importa.php" method="post">
                <div id="uploadnimp" style="overflow:auto;">
                    <table width="auto" border="0">
                        <tr>
                            <td></td>
                            <td colspan="7" align="center" class="corfd_ntab"><span style=" font-size: 14px; text-align: center; font-weight: bold">UPLOADS NÃO IMPORTADOS</span></td>
                        </tr>
                      <tr>
                        <td></td>
                        <td width="51" class="corfd_coltexto" align="center"><strong>ID UP.</strong></td>
                        <td width="800" class="corfd_coltexto" align="center"><strong>RELACIONAMENTO</strong></td>
                        <td width="50" class="corfd_coltexto" align="center"><strong>Sem.</strong></td>
                        <td width="84" class="corfd_coltexto" align="center"><strong>Data Inicial</strong></td>
                        <td width="74" class="corfd_coltexto" align="center"><strong>Data Final</strong></td>
                        <td width="129" class="corfd_coltexto" align="center"><strong>Data/Hora Up.</strong></td>
                        <td width="150" class="corfd_coltexto" align="center"><strong>Usuário Resp.</strong></td>
                      </tr>
                    <?php
                    $perfiluser = "SELECT * FROM $tabreluser WHERE id$tabuser='".$iduser."'";
                    $eperfiluser = $_SESSION['query']($perfiluser) or die (mysql_error());
                    $arrayperfil = array();
                    while($lperfil = $_SESSION['fetch_array']($eperfiluser)) {
                      $selup = "SELECT u.tabuser,u.iduser,u.idrel_filtros,u.camupload,u.idupload,pm.tipomoni,p.idperiodo,u.dataup,u.horaup,u.dataini,u.datafim,
                                        pm.tiposervidor, pm.semdados,pm.tiposervidor,pm.endereco,pm.loginftp,pm.senhaftp FROM upload u
                                        INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                                        INNER JOIN conf_rel cr ON cr.idrel_filtros = u.idrel_filtros 
                                        INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni 
                                        WHERE u.idrel_filtros='".$lperfil['idrel_filtros']."' AND u.imp='N' ORDER BY u.dataup, u.horaup";
                      $eselup = $_SESSION['query']($selup) or die (mysql_error());
                      while($lselup = $_SESSION['fetch_array']($eselup)) {
                          if($lselup['idrel_filtros'] != "") {
                            if($lselup['tiposervidor'] == "FTP") {
                                $ftp = ftp_connect($lselup['endereco']);
                                $ftplogin = ftp_login($ftp, $lselup['loginftp'], base64_decode($lselup['senhaftp']));
                                $camaudio = $lselup['camupload'];
                                $dir = ftp_nlist($ftp, $camaudio);
                                foreach($dir as $d) {
                                    if(basename($d) == "." || basename($d) == ".." || !eregi(".txt",basename($d)) || !eregi("logverif",basename($d))) {
                                    }
                                    else {
                                        $arq = basename($d);
                                    }
                                }
                                $cam = "/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/log/".str_replace($lselup['camaudio'],"",$caminho)."$arq";
                            }
                            else {
                                $camaudio = $lselup['camupload'];
                                $dir = scandir($camaudio);
                                $arq = "";
                                foreach($dir as $d) {
                                    if($d == "." || $d == ".." || strstr($d,".txt")) {
                                    }
                                    else {
                                        if(strstr($d,"logverif")) {
                                            $arq = $d;
                                        }
                                    }
                                }
                                $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
                                $cam = str_replace($rais, "", $camaudio)."/$arq";
                            }
                            $idrelapres = $lselup['idrel_filtros'];
                            $tipouser = $lselup['tabuser'];
                            $urlimp = str_replace('/monitoria_supervisao/admin/', '', mysql_real_escape_string($_SERVER['REQUEST_URI']));
                            echo "<tr>";
                                echo "<td><input name=\"idupload[]\" id=\"idupload\" type=\"checkbox\" value=\"".$lselup['idupload']."\" /></td>";
                                echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:50px; border: 1px solid #FFF; text-align:center\" name=\"idup\" type=\"hidden\" value=\"".$lselup['idupload']."\" /><a href=\"$cam\" target=\"blank\">".$lselup['idupload']."</a></td>";
                                //echo "<td align=\"center\"><input style=\"width:50px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" name=\"idrel\" type=\"text\" value=\"".$lselup['idrel_filtros']."\" /><</td>";
                                //echo "<td align=\"center\" class=\"corfd_colcampos\"><input name=\"url\" type=\"hidden\" value=\"".$urlimp."\" /></td>";
                                echo "<td align=\"center\" class=\"corfd_colcampos\">".nomeapres($idrelapres)."</td>";
                                echo "<td align=\"center\" class=\"corfd_colcampos\"><input name=\"periodoup".$lselup['idupload']."\" id=\"periodoup".$lselup['idupload']."\" type=\"hidden\" value=\"".$lselup['idperiodo']."\" />";
                                    echo "<select name=\"semanaup".$lselup['idupload']."\" id=\"semanaup".$lselup['idupload']."\">";
                                    $selinter = "SELECT * FROM interperiodo WHERE idperiodo='".$lselup['idperiodo']."'";
                                    $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta dos períodos cadastrados");
                                    while($lselinter = $_SESSION['fetch_array']($eselinter)) {
                                        if($lselinter['semana'] == $lselup['semana']) {
                                            echo "<option value=\"".$lselinter['semana']."\" selected=\"selected\">".$lselinter['semana']."</option>";
                                        }
                                        else {
                                            echo "<option value=\"".$lselinter['semana']."\">".$lselinter['semana']."</option>";
                                        }
                                    }
                                    echo  "</select>";
                                if($lselup['semdados'] == "S") {
                                    echo "<td align=\"center\" class=\"corfd_colcampos\" colspan=\"2\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center;\"  name=\"datactt".$lselup['idupload']."\" id=\"datactt".$lselup['idupload']."\" type=\"text\" value=\"\" /></td>";
                                }
                                else {
                                    echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center;font-size:12px\"  name=\"dataini".$lselup['idupload']."\" id=\"dataini".$lselup['idupload']."\" type=\"text\" value=\"".banco2data($lselup['dataini'])."\" readonly=\"readonly\" /></td>";
                                    echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center;font-size:12px\" name=\"datafim".$lselup['idupload']."\" id=\"datafim".$lselup['idupload']."\" type=\"text\" value=\"".banco2data($lselup['datafim'])."\" readonly=\"readonly\" /></td>";
                                }
                                echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:140px; border: 1px solid #FFF; text-align:center;font-size:12px\" readonly=\"readonly\" name=\"dthoraup\" type=\"text\" value=\"".banco2data($lselup['dataup'])." - ".$lselup['horaup']."\" /></td>";
                                $seluser = "SELECT usuario FROM $tipouser WHERE id$tipouser='".$lselup['iduser']."'";
                                $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na consulta da nome do usuário responsável");
                                echo "<td align=\"center\" class=\"corfd_colcampos\"><input name=\"usuario\" type=\"hidden\" value=\"".$eseluser['usuario']."\" />".$eseluser['usuario']."</td>";
                            echo "</tr>";
                          }
                      }
                    }
                    ?>
                    </table>
                    <?php
                    $idrelup = $_GET['idrelup'];
                    if($idrelup == "") {
                    }
                    else {
                        $idrel = $_GET['idrel'];
                        $include_once = $_GET['carrega'];
                        switch($include_once) {
                            case "carrega";
                            include_once "visuup.php";
                            break;
                        }
                    }
                }
                else {
                    echo "<table align=\"center\">";
                        echo "<tr>";
                            echo "<td class=\"corfd_colcampos\" style=\"color:#F00\" align=\"center\"><strong>SEU USUÁRIO NÃO ESTÁ AUTORIZADO A VISUALIZAR A ÁREA DE IMPORTAÇÃO, FAVOR CONTATAR O ADMINISTRADOR!!!</strong></td>";
                        echo "</tr>";
                    echo "</table>";
                }
            }
            else {
                echo "<table align=\"center\">";
                        echo "<tr>";
                            echo "<td class=\"corfd_colcampos\" style=\"color:#F00\" align=\"center\"><strong>SEU USUÁRIO NÃO ESTÁ AUTORIZADO A VISUALIZAR A ÁREA DE IMPORTAÇÃO, FAVOR CONTATAR O ADMINISTRADOR!!!</strong></td>";
                        echo "</tr>";
                echo "</table>";
            }
      ?>
        </div>
          <div style="padding-top:10px">
            <table width="956" border="0">
              <tr height="20px">
                  <td>
                      <div style="height: 25px">
                        <label><input type="radio" name="acaoimport" value="v" /><span style=" font-weight: bold; margin-right: 15px"> VERIFICAR</strong></label>
                        <label><input type="radio" name="acaoimport" value="i" /><strong> IMPORTAR</strong></label>
                      </div>
                  </td>
              </tr>
              <tr>
                  <td colspan="7"><input style=" margin-right: 5px;border: 1px solid #FFF; height: 18px;width:100px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="carrega" type="submit" value="Visualizar" />
                  <input style="margin-right: 5px;border: 1px solid #FFF; height: 18px; width:100px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="importa" id="importa" type="submit" value="Importar" /> <input style="margin-right: 5px;border: 1px solid #FFF; height: 18px; width:100px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="apaga" id="apaga" type="submit" value="Apagar" /> <input style="border: 1px solid #FFF; height: 18px; width:100px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="alteraup" id="alteraup" type="button" value="Alterar Dados" /></td>
              </tr>
            </table>
          </div>
          </form><br/>
          </div><br/>
          <div style="border: 2px solid #999; padding: 10px">
          <?php
          if($_SESSION['import'] == "S") {
          ?>
             <form action="admin/importa.php" method="post">
             <table width="100%">
                  <tr>
                      <td class="corfd_ntab" align="center" colspan="2"><span style="font-weight: bold; font-size: 14px">IMPORTAÇÕES REALIZADAS - CONSULTAS</strong></td>
                  </tr>
                  <tr>
                      <td bgcolor="#999999" class="corfd_coltexto"><strong>DATAS</strong></td>
                      <td class="corfd_colcampos"><input type="text" style="border: 1px solid #333; width:70px; text-align: center" name="dtiniimp" id="dtinifilt" value="<?php echo banco2data($_GET['dataini']);?>" /> <strong>À</strong> <input type="text" style="border: 1px solid #333; width:70px; text-align: center" name="dtfimimp" id="dtfimfilt" value="<?php echo banco2data($_GET['datafim']);?>"/></td>
                  </tr>
                  <tr>
                      <td width="110" class="corfd_coltexto"><strong>RELACIONAMENTO</strong></td>
                      <td width="364" class="corfd_colcampos">
                            <select name="idrelimp" id="idrelimp" style="width: 100%">
                                <option value="">TODOS</option>
                                <?php
                                $idsrel = arvoreescalar();
                                foreach($idsrel as $rel) {
                                    $selrelimp = "SELECT COUNT(*) as result FROM rel_filtros rf 
                                                  INNER JOIN $tabreluser ua ON ua.idrel_filtros = rf.idrel_filtros 
                                                  INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                                  WHERE id$tabuser='".$_SESSION['usuarioID']."' AND cr.ativo='S' AND ua.idrel_filtros='$rel'";
                                    $eselrelimp = $_SESSION['fetch_array']($_SESSION['query']($selrelimp)) or die ("erro na query de consulta dos relacionamentos cadastrados");
                                    if($eselrelimp['result'] >= 1) {
                                        if($rel == $_GET['idrel']) {
                                            $selectimp = "selected=\"selected\"";
                                        }
                                        else {
                                            $selectimp = "";
                                        }
                                        echo "<option $selectimp value=\"".$rel."\">".nomeapres($rel)."</option>";
                                    }
                                    else {
                                    }
                                }
                                ?>
                            </select>
                      </td>
                  </tr>
             </table><br></br>
             <font color="#FF0000"><strong><?php echo $_GET['imprel'] ?><a href="#" name="imp"></a></strong></font>
                  <div id="histimp" style="overflow: auto; height: 400px">
                      <!-- AREA DESABILITADA
                      CARREGA AS DUAS ULTIMAS IMPORTAÇÕES REALIZADAS DE CADA RELACIONAMENTO NO CARREGAMENTO DA PÁGINA
                      <table width="1100" border="0" id="tabhist">
                            <thead>
                              <th width="51" class="corfd_coltexto" align="center"><strong>ID UP.</strong></th>
                              <th width="51" class="corfd_coltexto" align="center"><strong>ID REL.</strong></th>
                              <th width="500" class="corfd_coltexto" align="center"><strong><?php echo $lselcampo['nomefiltro_nomes']; ?></strong></th>
                              <th width="50" class="corfd_coltexto" align="center"><strong>Sem.</strong></th>
                              <th width="160" class="corfd_coltexto" align="center"><strong>Datas Ctts</strong></th>
                              <th width="44" class="corfd_coltexto" align="center"><strong>Reg. Disp</strong></th>
                              <th width="44" class="corfd_coltexto" align="center"><strong>Reg. Exe</strong></th>
                              <th width="149" class="corfd_coltexto" align="center"><strong>Data/Hora IMP.</strong></th>
                              <th width="200" class="corfd_coltexto" align="center"><strong>Usurio Resp.</strong></th>
                              <th></th>
                            </thead>
                          <?php
                              /*$periodo = periodo(datas);
                              if($_GET['dataini'] != "" AND $_GET['datafim'] != "") {
                                  $datas = "AND u.dataimp BETWEEN '".$_GET['dataini']."' AND '".$_GET['datafim']."'";
                              }
                              else {
                                  $datas = "AND u.dataimp BETWEEN '".$periodo['dataini']."' AND '".$periodo['datafim']."'";
                              }
                              $perfilimp = "SELECT * FROM $tabreluser WHERE id$tabuser='".$iduser."' ORDER BY idrel_filtros";
                              $eperfilimp = $_SESSION['query']($perfilimp) or die (mysql_error());
                              $l = 0;
                              while($lfiltimp = $_SESSION['fetch_array']($eperfilimp)) {
                                      $selimp = "SELECT u.idupload, u.idperiodo, u.idrel_filtros, u.iduser, u.tabuser, it.dataini, it.datafim, u.dataimp, u.semana, u.horaimp, pm.tipomoni, u.tabfila FROM upload u
                                                         inner join periodo p ON p.idperiodo = u.idperiodo
                                                         inner join interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
                                                         inner join conf_rel cr ON cr.idrel_filtros = u.idrel_filtros
                                                         inner join param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                                         WHERE u.idrel_filtros='".$lfiltimp['idrel_filtros']."' $datas AND u.imp='S' ORDER BY u.dataimp DESC, u.horaimp DESC LIMIT 1";
                                      $eselimp = $_SESSION['query']($selimp) or die (mysql_error());
                                      while($lselimp = $_SESSION['fetch_array']($eselimp)) {
                                              $l++;
                                              if(is_int($l / 2)) {
                                                  $bgcolor = "bgcolor=\"#C5D4EA\"";
                                              }
                                              else {
                                                  $bgcolor = "bgcolor=\"#FFFFFF\"";
                                              }
                                              $tabuser = $lselimp['tabuser'];
                                              $fila = $lselimp['tabfila'];
                                              if($lselimp['tabfila'] == "fila_grava") {
                                                  $alias = "fg";
                                              }
                                              $fila = $lselimp['tabfila'];
                                              if($lselimp['tabfila'] == "fila_oper") {
                                                  $alias = "fo";
                                              }
                                              $cupload = "select SUM(if(monitorado = '1',1,0)) as prod, SUM(if(monitorado = '0',1,0)) as disp from upload u
                                                                      inner join $fila $alias ON $alias.idupload = u.idupload where u.idupload='".$lselimp['idupload']."';";
                                              $ecupload = $_SESSION['fetch_array']($_SESSION['query']($cupload)) or die ("erro na query de contagem das monitorias efetuadas no upload");
                                              $cprod = $ecupload['prod'];
                                              $cdisp = $ecupload['disp'];
                                              $dataimp = banco2data($lselimp['dataimp'])." - ".$lselimp['horaimp'];
                                              $nomerel = nomeapres($lselimp['idrel_filtros']);
                                              $nuser = "SELECT usuario FROM $tabuser WHERE id$tabuser='".$lselimp['iduser']."'";
                                              $enuser = $_SESSION['fetch_array']($_SESSION['query']($nuser)) or die ("erro na query de consulta do nome do usuário que importou os registros");
                                              $nomeuser = $enuser['usuario'];
                                      ?>
                                      <tbody>
                                          <tr <?php echo $bgcolor;?>>
                                              <td align="center"><?php echo $lselimp['idupload'];?></td>
                                              <td align="center"><?php echo $lselimp['idrel_filtros'];?></td>
                                              <td align="center">
                                                  <?php
                                                  if($_SESSION['usuarioperfil'] != "01") {
                                                      echo "<input name=\"relimp\" id=\"relimp\" value=\"".$lselimp['idrel_fitlros']."\" type=\"hidden\" /><span title=\"$nomerel\">$nomerel</span>";
                                                  }
                                                  else {
                                                  ?>
                                                  <select type="text" name="relimp<?php echo $lselimp['idupload'];?>"  id="relimp<?php echo $lselimp['idupload'];?>" style="width:500px">
                                                      <option value="<?php echo $lselimp['idrel_filtros'];?>" title="<?php echo $nomerel;?>" selected="selected"><?php echo $nomerel;?></option>
                                                      <?php
                                                      $relfiltros = "SELECT rf.idrel_filtros FROM rel_filtros rf
                                                                     INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                                                     INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni WHERE pm.tipomoni='".$lselimp['tipomoni']."'";
                                                      $erelfiltros = $_SESSION['query']($relfiltros) or die ("erro na query de consulta dos relacionamentos");
                                                      while($lrelfiltros = $_SESSION['fetch_array']($erelfiltros)) {
                                                          if($lrelfiltros['idrel_filtros'] == $lselimp['idrel_filtros']) {
                                                          }
                                                          else {
                                                              $nomeapres = nomeapres($lrelfiltros['idrel_filtros']);
                                                              echo "<option value=\"".$lrelfiltros['idrel_filtros']."\" title=\"$nomeapres\">$nomeapres</option>";
                                                          }
                                                      }
                                                      ?>
                                                  </select></td>
                                                  <?php
                                                  }
                                                  ?>
                                              <td align="center"><input name="periodoup<?php echo $lselimp['idupload'];?>" id="periodoup<?php echo $lselimp['idupload'];?>" value="<?php echo $lselimp['idperiodo'];?>" type="hidden" />
                                                  <?php
                                                  if($_SESSION['usuarioperfil'] != "01") {
                                                      echo $lselimp['semana'];
                                                  }
                                                  else {
                                                      ?>
                                                      <select name="semanaup<?php echo $lselimp['idupload'];?>" id="semanup<?php echo $lselimp['idupload'];?>">
                                                          <?php
                                                          $selsem = "SELECT * FROM interperiodo WHERE idperiodo='".$lselimp['idperiodo']."'";
                                                          $eselsem = $_SESSION['query']($selsem) or die ("erro na query de consulta das semanas");
                                                          while($lselinter = $_SESSION['fetch_array']($eselsem)) {
                                                              if($lselinter['semana'] == $lselimp['semana']) {
                                                                  echo "<option value=\"".$lselinter['semana']."\" selected=\"selected\">".$lselinter['semana']."</option>";
                                                              }
                                                              else {
                                                                  echo "<option value=\"".$lselinter['semana']."\">".$lselinter['semana']."</option>";
                                                              }
                                                          }
                                                          ?>
                                                      </select>
                                                  <?php
                                                  }
                                                  ?>
                                              </td>
                                              <td align="center">
                                                  <?php
                                                  $seldatas = "SELECT datactt, COUNT(*) as result FROM fila_grava WHERE idupload='".$lselimp['idupload']."' GROUP BY datactt";
                                                  $eseldatas = $_SESSION['query']($seldatas) or die ("erro na query para consultas as datas de contato");
                                                  while($lseldatas = $_SESSION['fetch_array']($eseldatas)) {
                                                      echo banco2data($lseldatas['datactt'])."-".$lseldatas['result']."<br/>";
                                                  }
                                                  ?>                                                    
                                              </td>
                                              <!-- <td bgcolor="#FFFFFF" align="center"><input readyonly="readonly" style="width: 70px; border: 1px solid #FFF; text-align: center" type="text" name="dtiniimp<?php //echo $lselimp['idupload'];?>" id="dtiniimp<?php //echo $lselimp['idupload'];?>" value="<?php //echo banco2data($lselimp['dataini']);?>" /></td>
                                              <td bgcolor="#FFFFFF" align="center"><input readyonly="readonly" style="width: 70px; border: 1px solid #FFF; text-align: center" type="text" name="dtfimimp<?php //echo $lselimp['idupload'];?>" id="dtfimimp<?php //echo $lselimp['idupload'];?>" value="<?php //echo banco2data($lselimp['datafim']);?>" /></td>-->
                                              <td align="center"><?php echo $cdisp;?></td>
                                              <td align="center"><?php echo $cprod;?></td>
                                              <td align="center"><?php echo $dataimp;?></td>
                                              <td align="center"><?php echo $nomeuser;?></td>
                                              <?php
                                              if($_SESSION['usuarioperfil'] != "01") {
                                              }
                                              else {
                                              ?>
                                              <td><input type="checkbox" name="idupimp[]" value="<?php echo $lselimp['idupload'];?>" /></td>
                                              <?php
                                              }
                                              ?>
                                          </tr>
                                      </tbody>
                                      <?php
                                      }
                              }*/
                              ?>
                              <!--</table>-->
                          <?php
                          }
                          else {
                          ?>
                          <table width="956" border="1">
                            <tr>
                              <td bgcolor="#FFFFFF" align="center" style="color:#F00"><strong>SEU USUÁRIO NÃO ESTÁ AUTORIZADO A VISUALIZAR A ÁREA DE IMPORTAÇÃO, FAVOR CONTATAR O ADMINISTRADOR!!! </strong></td>
                            </tr>
                          </table>
                          <?php
                          }
                          ?>
                  </div>
                  <?php
                  if($_SESSION['usuarioperfil'] != '01') {
                  }
                  else {
                  ?>
                      <table width="956" border="0">
                          <td><input style="border: 1px solid #FFF; height: 18px; width:60px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="alteraimp" id="alteraimp" type="submit" value="Alterar" />
                          <input style="border: 1px solid #FFF; height: 18px; width:60px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="exclui" id="exclui" type="submit" value="Excluir" /></td>
                      </table>
                  <?php
                  }
                  ?>
              </form>
          </div>
</div>
</body>
