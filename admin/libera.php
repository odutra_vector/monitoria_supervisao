<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.corsistema.php');

$cor = new CoresSistema();
$cor->Cores();

$idmonitor = $_GET['idmonitor'];
$idpausa = $_GET['idpausa'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<title>Documento sem título</title>
</head>
<body style="background-color: #EAEAEA">
    <div style="width:800px; font-family: Verdana, Geneva, sans-serif; font-size: 10px;">
        <table width="800">
            <thead>
              <tr>
                <th width="66" class="corfd_coltexto" align="center"><strong>DATA</strong></th>
                <th width="193" class="corfd_coltexto" align="center"><strong>MOTIVO</strong></th>
                <th width="85" class="corfd_coltexto" align="center"><strong>INICIO</strong></th>
                <th width="84" class="corfd_coltexto" align="center"><strong>FIM</strong></th>
                <th width="244" class="corfd_coltexto" align="center"><strong>OBS</strong></th>
                <th width="100"></th>
              </tr>
            </thead>
            <tbody>
              <?php
              if(isset($_GET['libpausa'])) {
                  $selmotivos = "SELECT * FROM moni_pausa mp
                                INNER JOIN motivo m ON m.idmotivo = mp.idmotivo
                                WHERE idmonitor='$idmonitor' AND idmoni_pausa='$idpausa'";
              }
              else {
                  $selmotivos = "SELECT * FROM moni_pausa mp
                                INNER JOIN motivo m ON m.idmotivo = mp.idmotivo
                                WHERE idmonitor='$idmonitor' AND horafim='00:00:00'";
              }
              $eselmot = $_SESSION['query']($selmotivos) or die ("erro na query de consulta das pausas inconsistentes");
              while($lselmot = $_SESSION['fetch_array']($eselmot)) {
              ?>
                  <tr>
                    <form action="atustatus.php" method="post">
                    <td class="corfd_colcampos" align="center"><?php if(isset($_GET['libpausa'])) { echo "<input name=\"libpausa\" type=\"hidden\" value=\"1\" />"; } else {}?><input name="idmonitor" type="hidden" value="<?php echo $idmonitor;?>" /><input name="idpausa" type="hidden" value="<?php echo $lselmot['idmoni_pausa'];?>" /><?php echo banco2data($lselmot['data']);?></td>
                    <td class="corfd_colcampos" align="center"><input name="idmotivo" type="hidden" value="<?php echo $lselmot['idmotivo'];?>" /><?php echo $lselmot['nomemotivo'];?></td>
                    <td class="corfd_colcampos" align="center"><input name="horaini" type="hidden" value="<?php echo $lselmot['horaini'];?>" /><?php echo $lselmot['horaini'];?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $lselmot['horafim'];?></td>
                    <td class="corfd_colcampos" align="center"><textarea name="obs" size="3" style="width:250px"></textarea></td>
                    <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altpausa" type="submit" value="Alterar" /></td>
                    </form>
                  </tr>
              <?php
              }
              ?>
            </tbody>
        </table>
    </div>
</body>
</html>
