<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PAUSAS</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
        $('#pausas').tablesorter();
    })
</script>
</head>
<body>
    <div style="width:500px">
        <table width="490" style="border: 2px solid #CCC" align="center" id="pausas">
          <thead>
            <th width="182" class="corfd_coltexto"><strong>MONITOR</strong></th>
            <th width="182" class="corfd_coltexto" align="center"><strong>DATA</strong></th>
            <th width="116" class="corfd_coltexto" align="center"><strong>PAUSA</strong></th>
            <th width="81" class="corfd_coltexto" align="center"><strong>TEMPO</strong></th>
          </thead>
        <?php
        $periodo = periodo(datas);
        $idsuper = $_GET['idsuper'];
        $idmonitor = $_GET['idmonitor'];
        $dia = $_GET['dia'];
        $inner = "INNER JOIN motivo mt ON mt.idmotivo = m.idmotivo INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor INNER JOIN user_adm ua ON ua.iduser_adm = mo.iduser_resp";

        if($idmonitor != "") {
            $where[] = "m.idmonitor='".$idmonitor."'";
        }
        else {
        }
        if($idsuper != "") {
            $where[] = "mo.iduser_resp='".$idsuper."'";
        }
        else {
        }
        if($dia != "") {
            $where[] = "m.data='".$dia."'";
        }
        else {
            $where[] = "m.data BETWEEN '".$periodo['dataini']."' AND '".$periodo['datafim']."'";
        }

        if($where != "" OR $where[0] != "") {
            $where = "WHERE ".implode(" AND ",$where);
        }
        else {

        }

        $selpausa = "SELECT m.tempo, m.data, nomemotivo, nomemonitor FROM moni_pausa m $inner $where";
        $epausa = $_SESSION['query']($selpausa) or die ("erro na query para obter as pausas");
        while($lepausa = $_SESSION['fetch_array']($epausa)) {
        ?>
                <tbody>
                  <tr>
                    <td><?php echo $lepausa['nomemonitor'];?></td>
                    <td align="center"><?php echo banco2data($lepausa['data']);?></td>
                    <td align="center"><?php echo $lepausa['nomemotivo'];?></td>
                    <td align="center"><?php echo $lepausa['tempo'];?></td>
                  </tr>
                </tbody>
        <?php
        }
        ?>
        </table>
    </div>
</body>
</html>
