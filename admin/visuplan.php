<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
$idplan = $_GET['planmoni'];
    
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>VISUALIZAÇÃO DA PLANILHA</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-autocomplete/lib/thickbox-compressed.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.css" type="text/css"></link>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/lib/thickbox.css" type="text/css"></link>
<script src="/monitoria_supervisao/js/jquery-autocomplete/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-accordion/jquery.accordion.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-accordion/lib/jquery.dimensions.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-accordion/lib/jquery.easing.js" type="text/javascript"></script>
<script src="/monitoria_supervisao/js/jquery-accordion/lib/chili-1.7.pack.js" type="text/javascript"></script>
<link rel="stylesheet" href="tabs.css" type="text/css"></link>
<script language="JavaScript" type="text/javascript" src="../audio-player/audio-player.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("select[id^='idresp']").change(function() {
	var id = $(this).attr("id");
        var idperg = id.substr(6,6);
        $.post("/monitoria_supervisao/af.php", {idresp:$(this).val(), idperg: idperg}, function(valor) {
            var texto = valor;
            var separa = texto.split(".");
            var abre = separa[1];
            var idpergn = separa[0];
            if(abre == "F") {
                $('#descri'+idpergn).hide();
            }
            if(abre == "A") {
                $('#descri'+idpergn).show();
            }
            if(abre == "D") {
                alert("Você não pode selecionar duas respostas conflitantes!!!");
            }
        })
    })
})
</script>
<?php
$selplans = "SELECT vp.idvinc_plan, vp.planvinc, p.descriplanilha, COUNT(*) as result FROM vinc_plan vp 
                    INNER JOIN planilha p ON p.idplanilha = vp.idplanilha WHERE vp.idplanilha='$idplan' AND vp.ativo='S'";
$eselplans = $_SESSION['query']($selplans) or die (mysql_error());
while($lselplans = $_SESSION['fetch_array']($eselplans)) {
    $planilhas[] = $lselplans['planvinc'];
}
$i = 1;
foreach($planilhas as $p) {
    $planilhas[$i] = $p;
    $i++;
}
$planilhas[0] = $idplan;
foreach($planilhas as $planilha) {
	if($planilha == "") {
	}
	else {
    	$selnplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$planilha'";
    	$eselplan = $_SESSION['fetch_array']($_SESSION['query']($selnplan)) or die ("erro na query de consulta do nome da planilha");
    	$plansdepende[$eselplan['descriplanilha']] = $planilha;
	}
}
foreach($plansdepende as $plandep) {
    echo "<script type=\"text/javascript\">";
    echo '$(function() {';
    echo "$("."'#plan".$plandep."'".").accordion({";
    echo "header: "."'div.titulo'".',';
    echo 'active: false,';
    echo 'alwaysOpen: false,';
    echo 'animated: false,';
    echo 'autoheight: false';
    echo '});';
    echo 'var wizard = $("#wizard").accordion({';
    echo 'header: "'."'.titulo'".'",';
    echo 'event: false';
    echo '});';
    echo '';
    echo '';
    echo 'var wizardButtons = $([]);';
    echo '$("div.titulo", wizard).each(function(index) {';
    echo 'wizardButtons = wizardButtons.add($(this)';
    echo '.next()';
    echo '.children(":button")';
    echo '.filter(".next, .previous")';
    echo '.click(function() {';
    echo 'wizard.accordion("activate", index + ($(this).is(".next") ? 1 : -1))';
    echo '}));';
    echo '});';
    echo '});';
    echo '</script>';
}
echo "</script>";
?>
<script type="text/javascript">
    <?php
    echo "$(document).ready(function() {\r\n";
    echo '  $('."'".'div.divabas'."'".').hide();'."\r\n";
    echo '  $('."'".'div.divabas:first'."'".').show();'."\r\n";
    echo '  $('."'".'#plan'.$idplan.' a:first'."'".').addClass("selected");'."\r\n";
    echo '  $('."'".'#abas a'."'".').click(function() {'."\r\n";
    echo '  $('."'".'div.divabas'."'".').hide();'."\r\n";
    echo '  $('."'".'#abas a'."'".').removeClass("selected");'."\r\n";
    echo '  $(this).addClass("selected");'."\r\n";
    echo '  $($(this).attr("href")).show();'."\r\n";
    echo '  return false;'."\r\n";
    echo '  })'."\r\n";
    echo '});'."\r\n";
    ?>
</script>
</head>
    <body>
        <div align="center" id="conteudo">
            <div id="tabnav">
                <div align="center" id="tabs">
                    <ul id="abas">
                        <?php
                        if($plansdepende == "") {
                        }
                        else {
                            foreach($plansdepende as $nomeplan => $idplanilha) {
                                if($idplan == "") {

                                }
                                else {
                                    echo "<li><a href=\"#plan".$idplanilha."\"><strong><span>".$nomeplan."</span></strong></a></li>";
                                }
                            }
                        }
                        ?>
                   </ul>
                </div>
                <?php
                foreach($plansdepende as $nplan => $idplan) {
                    echo "<div align=\"center\" style=\"background-color:#EAEAEA;\" id=\"plan".$idplan."\" class=\"divabas\">";
                      echo "<table width=\"825\" align=\"center\">";
                        echo "<tr>";
                                echo "<td align=\"center\" bgcolor=\"#000000\" colspan=\"2\" style=\"color:#FFF\"><strong>PLANILHA</strong></td>";
                        echo "<tr>";
                                echo "<td bgcolor=\"#6699CC\" align=\"center\" width=\"725\"><strong>".$nplan."</strong><input name=\"idsplan[]\" type=\"hidden\" value=\"".$idplan."\" /></td>";
                                echo "<td align=\"center\" bgcolor=\"#6699CC\" width=\"100\"><strong>Valor</strong></td>";
                        echo "</tr>";
                      echo "</table>";
                      $relaval = "SELECT * FROM rel_aval WHERE idplanilha='$idplan'";
                      $erelaval = $_SESSION['query']($relaval) or die (mysql_error());
                      while($laval = $_SESSION['fetch_array']($erelaval)) {
                        $idaval = $laval['idaval_plan'];
                        $selaval = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$laval['idaval_plan']."'";
                        $eaval = $_SESSION['fetch_array']($_SESSION['query']($selaval)) or die (mysql_error());
                        echo "<div align=\"center\">";
                        echo "<div align=\"center\" class=\"titulo\">";
                        echo "<table width=\"825\" align=\"center\">";
                        echo "<tr>";
                          echo "<td bgcolor=\"#669966\" align=\"center\" width=\"725\"><strong>".$eaval['nomeaval_plan']."</strong><input name=\"idaval[]\" type=\"hidden\" value=\"".$laval['idaval_plan']."\"/></td>";
                          echo "<td align=\"cetner\" width=\"100\"><input style=\"width:99px; border: 1px solid #333; text-align:center\" readonly=\"readonly\" name=\"valor_aval[]\" type=\"text\" value=\"".$laval['valor']."\" /></td>";
                        echo "<tr/>";
                        echo "</table></div>";
                        echo "<div align=\"center\">";
                         $selgrup = "SELECT * FROM planilha WHERE idplanilha='$idplan' AND idaval_plan='".$laval['idaval_plan']."' AND ativo='S' ORDER BY posicao"; // faz select dos grupos disponÃƒÂ­veis na planilha pelo id da planilha
                          $egrup = $_SESSION['query']($selgrup) or die (mysql_error());
                              while($lgrup = $_SESSION['fetch_array']($egrup)) { // faz um loop dos grupos listados na planilha
                                      $ngrup = "SELECT * FROM grupo WHERE idgrupo='".$lgrup['idgrupo']."' AND ativo='S'"; // faz select das informaÃƒÂ§ÃƒÂµes do grupo na passagem do loop
                                      $engrup = $_SESSION['fetch_array']($_SESSION['query']($ngrup)) or die (mysql_error());
                                      echo "<div>";
                                      echo "<div id=\"grupo".$engrup['idgrupo']."\">";
                                      echo "<table width=\"825\" align=\"center\">";
                                      echo "<tr>";
                                      echo "<td bgcolor=\"#999999\" align=\"center\" width=\"725\"><strong>".$engrup['descrigrupo']."</strong><input name=\"idgrup[]\" type=\"hidden\" value=\"".$engrup['idgrupo']."\"/></td>";
                                      echo "<td align=\"center\"><input style=\"width:99px; border: 1px solid #333;  text-align:center\" readonly=\"readonly\" name=\"valor_grup[]\" id=\"valor_grup\" type=\"text\" value=\"".$engrup['valor_grupo']."\" /></td>";
                                      echo "</tr>";
                                      echo "</table></div>";
                                      echo "<div>";
                                      echo "<table width=\"825\" align=\"center\">";
                                      $selsub = "SELECT * FROM grupo WHERE idgrupo='".$lgrup['idgrupo']."' AND ativo='S' ORDER BY posicao"; // faz outro select do mesmo grupo para criar um loop das perguntas ou subgrupos relacionados
                                      $esub = $_SESSION['query']($selsub) or die (mysql_error());
                                              while($lsub = $_SESSION['fetch_array']($esub)) {
                                                      if($engrup['idrel'] == 0) {
                                                      }
                                                      else {
                                                        if($lsub['filtro_vinc'] == 'S') { // apÃƒÂ³s o loop verifica se o grupo selecionado tem o filro de subgrupo
                                                                $nsub = "SELECT * FROM subgrupo WHERE idsubgrupo='".$lsub['idrel']."' AND ativo='S'"; // faz a seleÃƒÂ§ÃƒÂ£o do subgrupo baseado no id_rel (id de relacionamento do grupo)
                                                                $ensub = $_SESSION['fetch_array']($_SESSION['query']($nsub)) or die (mysql_error());
                                                                echo "<tr>";
                                                                echo "<td bgcolor=\"#99CCFF\" align=\"center\" width=\"725\"><strong>".$ensub['descrisubgrupo']."</strong><input name=\"idsub[]\" type=\"hidden\" value=\"".$ensub['idsubgrupo']."\"/></td>";
                                                                echo "<td align=\"center\"><input style=\"width:99px; border: 1px solid #333; text-align:center\" readonly=\"readonly\" name=\"valor_sub[]\" id=\"valor_sub\" type=\"text\" value=\"".$ensub['valor_sub']."\" /></td>";
                                                                echo "</tr>";
                                                                $cperg = "SELECT COUNT(idpergunta) as result FROM subgrupo WHERE idsubgrupo='".$lsub['idrel']."' AND ativo='S'"; // faz select para identificar a quantidade de perguntas relacionadas ao grupo
                                                                $ecperg = $_SESSION['fetch_array']($_SESSION['query']($cperg)) or die (mysql_error());
                                                                if($ecperg['result'] == 0) { // caso a quantidade seja 0, nÃƒÂ£o insere nada na planilha
                                                                }
                                                                if($ecperg['result'] >= 1) { // caso seja igual ou maior que 1 seja com os parametros
                                                                    $selperg = "SELECT * FROM subgrupo WHERE idsubgrupo='".$lsub['idrel']."' AND ativo='S' GROUP BY idpergunta"; // faz novo select do subgrupo para criar loop das perguntas
                                                                    $eperg = $_SESSION['query']($selperg) or die (mysql_error());
                                                                    while($lperg = $_SESSION['fetch_array']($eperg)) {
                                                                        if($lperg['idpergunta'] == '0') { // verifica se o campo id_perg estÃƒÂ¡ zerado, pois se estiver nÃƒÂ£o existe pergunta relacionada
                                                                        }
                                                                        else {
                                                                            $nperg = "SELECT * FROM pergunta WHERE idpergunta='".$lperg['idpergunta']."' AND ativo='S'"; // faz select da pergunta do loop para exibir dados
                                                                            $enperg = $_SESSION['fetch_array']($_SESSION['query']($nperg)) or die (mysql_error());
                                                                            echo "<tr>";
                                                                            echo "<td bgcolor=\"#FFCC99\" align=\"center\" width=\"725\"><strong>".$enperg['descripergunta']."</strong><input name=\"idperg[]\" id=\"idperg".$enperg['idpergunta']."\" type=\"hidden\" value=\"".$enperg['idpergunta']."\"/></td>";
                                                                            echo "<td align=\"center\"><input style=\"width:99px; border: 1px solid #333; text-align:center\" readonly=\"readonly\" name=\"valor_perg[]\" id=\"valor_perg\"  type=\"text\" value=\"".$enperg['valor_perg']."\" /></td>";
                                                                            echo "</tr>";
                                                                            echo "<tr>";
                                                                            if($enperg['tipo_resp'] == "U") {
                                                                              echo "<td colspan=\"2\" align=\"left\"><select style=\"width:718px; text-align:center\" name=\"idresp[]\" id=\"idresp".$enperg['idpergunta']."\">";
                                                                            }
                                                                            if($enperg['tipo_resp'] == "M") {
                                                                              echo "<td colspan=\"2\" align=\"left\"><select style=\"width:718px; text-align:center\" multiple=\"multiple\" size=\"3\" name=\"idresp[]\" id=\"idresp".$enperg['idpergunta']."\">";
                                                                            }
                                                                            $selresp = "SELECT * FROM pergunta WHERE idpergunta='".$lperg['idpergunta']."' AND ativo_resp='S'"; // faz select das perguntas para criar loop das respostas
                                                                            $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                            while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                                $nresp = "SELECT * FROM pergunta WHERE idresposta='".$lresp['idresposta']."'"; // faz select das respostas da pergunta atual do loop
                                                                                $enresp = $_SESSION['fetch_array']($_SESSION['query']($nresp)) or die (mysql_error());
                                                                                echo "<option value=\"".$enresp['idresposta']."\" />".$enresp['descriresposta']."</option>";
                                                                            }
                                                                            echo "</select></td>";
                                                                            echo "<td><input type=\"hidden\" style=\"width:59px; border: 1px solid #333; text-align:center\" name=\"valor_resp\" id=\"valor_resp\" value=\"\"></td>";
                                                                            echo "</tr>";
                                                                        }
                                                                        echo "</tr>";
                                                                        echo "<tr id=\"descri".$enperg['idpergunta']."\" style=\"display:none\">";
                                                                        echo "<td><textarea style=\"width:718px;\" name=\"descri".$enperg['idpergunta']."\" rows=\"3\"></textarea></td>";
                                                                        echo "</tr>";
                                                                    }
                                                                }
                                                        }
                                                        if($lsub['filtro_vinc'] == 'P') {
                                                                $cperg = "SELECT COUNT(idrel) as result FROM grupo WHERE idgrupo='".$lsub['idgrupo']."' AND ativo='S'";
                                                                $ecperg = $_SESSION['fetch_array']($_SESSION['query']($cperg)) or die (mysql_error());
                                                                if($ecperg['result'] == 0) {
                                                                }
                                                                if($ecperg['result'] >= 1) {
                                                                    if($lsub['idrel'] == '0') {
                                                                    }
                                                                    else {
                                                                        $nperg = "SELECT * FROM pergunta WHERE idpergunta='".$lsub['idrel']."' AND ativo='S'";
                                                                        $enperg = $_SESSION['fetch_array']($_SESSION['query']($nperg)) or die (mysql_error());
                                                                        echo "<tr>";
                                                                        echo "<td bgcolor=\"#FFCC99\" align=\"center\" width=\"725\"><strong>".$enperg['descripergunta']."</strong><input name=\"idperg[]\" id=\"idperg".$enperg['idpergunta']."\" type=\"hidden\" value=\"".$enperg['idpergunta']."\"/></td>";
                                                                        echo "<td align=\"center\"><input style=\"width:99px; border: 1px solid #333; text-align:center\" readonly=\"readonly\" name=\"valor_perg[]\" id=\"valor_perg\" type=\"text\" value=\"".$enperg['valor_perg']."\" /></td>";
                                                                        echo "</tr>";
                                                                        echo "<tr>";
                                                                        if($enperg['tipo_resp'] == "U") {
                                                                          echo "<td colspan=\"2\" align=\"left\"><select style=\"width:718px; text-align:center\" name=\"idresp[]\" id=\"idresp".$enperg['idpergunta']."\">";
                                                                        }
                                                                        if($enperg['tipo_resp'] == "M") {
                                                                          echo "<td colspan=\"2\" align=\"left\"><select style=\"width:718px; text-align:center\" multiple=\"multiple\" size=\"3\" name=\"idresp[]\" id=\"idresp".$enperg['idpergunta']."\">";
                                                                        }
                                                                        $selresp = "SELECT * FROM pergunta WHERE idpergunta='".$lsub['idrel']."' AND ativo_resp='S'";
                                                                        $eresp = $_SESSION['query']($selresp) or die (mysql_error());
                                                                        while($lresp = $_SESSION['fetch_array']($eresp)) {
                                                                            $nresp = "SELECT * FROM pergunta WHERE idresposta='".$lresp['idresposta']."' AND ativo='S'";
                                                                            $enresp = $_SESSION['fetch_array']($_SESSION['query']($nresp)) or die (mysql_error());
                                                                            echo "<option value=\"".$enresp['idresposta']."\" />".$enresp['descriresposta']."</option>";
                                                                        }
                                                                        echo "</select></td>";
                                                                        echo "<td><input type=\"hidden\" style=\"width:59px; border: 1px solid #333; text-align:center\" name=\"valor_resp\" id=\"valor_resp\" value=\"\"></td>";
                                                                        echo "</tr>";
                                                                        echo "<tr id=\"descri".$lsub['idrel']."\" style=\"display:none\">";
                                                                        echo "<td><textarea style=\"width:718px;\" name=\"descri".$lsub['idrel']."\" rows=\"3\"></textarea></td>";
                                                                        echo "</tr>";
                                                                    }
                                                                }
                                                        }
                                                      }
                                              }
                                  echo "</table></div></div>";
                                  
                              }
                              echo "<tr>";
                                echo "<td colspan=\"2\"><br><hr /></td>";
                              echo "</tr>";
                              echo "</div>";
                        }
                      ?>
                      <table width="176" border="0" align="center">
                        <tr>
                            <td width="55" class="corfd_coltexto" align="center"><a style="color:#000; text-decoration:none" href="admin.php?menu=planilha&id=<?php echo $idplan;?>"><strong>VOLTAR</strong></a></td>
                        </tr>
                      </table>
                    <?php
                    echo "</div>";
                    }
                ?>
            </div>
            <div align="center" id="rodape">
              <table align="center" width="300" border="0">
		    	<tr>
		        	<td align="center"><strong><font color="#FFFFFF">Design by: Vector DSI</font></strong></td>
		        </tr>
		        <tr>
		        	<td align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
		        </tr>
		     </table>
            </div>
            </div>
    </body>
</html>
