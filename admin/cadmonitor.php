<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/validacao.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.smtp.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.phpmailer.php');
include_once($rais.'/monitoria_supervisao/classes/class.envemail.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadmin.php');

$nome = mysql_real_escape_string(strtoupper(trim($_POST['nome'])));
$cpf = str_pad(ereg_replace('[^0-9]', '', $_POST['cpf']), 11, '0', STR_PAD_LEFT);
$resp = $_POST['resp'];
$hentra = $_POST['hentra'];
$hsaida = $_POST['hsaida'];
$user = mysql_real_escape_string($_POST['user']);
$tmonitor = $_POST['tmonitor'];
$atv = $_POST['ativo'];
$senha_ini = $_POST['senha_ini'];
$gerasenha = geraSenha(10, true, true, true);
$senhacrypt = md5($gerasenha);
$caracter = "[]$[><}{)(:;!?*%&#@]";
$data = date('Y-m-d');
$usercheck = 0;

if(isset($_POST['cadastra'])) {
    $tabcheck = array("monitor","user_adm","user_web");
    foreach($tabcheck as $tab) {
        $cuser = "SELECT COUNT(*) as result FROM $tab WHERE usuario='$user'";
        $luser = $_SESSION['fetch_array']($_SESSION['query']($cuser)) or die (mysql_error());
        if($luser['result'] >= 1) {
            $usercheck++;
        }
    }
    $cnome = "SELECT COUNT(*) as result FROM monitor WHERE nomemonitor='$nome'";
    $enome = $_SESSION['query']($cnome) or die (mysql_error());
    $lnome = $_SESSION['fetch_array']($enome) or die (mysql_error());
    $ccpf = "SELECT COUNT(*) as result FROM monitor WHERE CPF='$cpf'";
    $ecpf = $_SESSION['query']($ccpf) or die (mysql_error());
    $lcpf = $_SESSION['fetch_array']($ecpf) or die (mysql_error());
    if($nome == "" || $cpf == "" || $hentra == "" || $hsaida == "" || $user == "") {
	$msg = "Todos os campos precisam estar preenchidos para cadastro do monitor!!!";
	header("location:admin.php?menu=monitor&msg=".$msg);
    }
    else {
        if(eregi($caracter, $nome) OR (eregi($caracter, $user))) {
            $msg = "O campo ''nome'' não pode conter caracter inválido $caracter!!!";
            header("location:admin.php?menu=monitor&msg=".$msg);
        }
        else {
            if(checkCPF($cpf) == false) {
              $msg = "CPF inválido, por favor, informe um CPF válido!!!";
              header("location:admin.php?menu=monitor&msg=".$msg);
            }
            else {
                if($usercheck >= 1){
                    $msg = "Este ''usuÃ¡rio'' já está cadastrado na base, favor escolher outro!!!";
                    header("location:admin.php?menu=monitor&msg=".$msg);
                }
                else {
                    if($lnome['result'] >= 1){
                        $msg = "Este ''Nome de usuário'' já está cadastrado na base, favor escolher outro!!!";
                        header("location:admin.php?menu=monitor&msg=".$msg);
                    }
                    else {
                        if($lcpf['result'] >= 1){
                            $msg = "Este ''CPF'' já está cadastrado na base, favor escolher outro!!!";
                            header("location:admin.php?menu=monitor&msg=".$msg);
                        }
                        else {
                            $cad = "INSERT INTO monitor (nomemonitor, CPF, iduser_resp, usuario,tmonitor, senha, status, entrada, saida, datacad, ativo, senha_ini) VALUE ('$nome', '$cpf', '$resp', '$user', '$tmonitor','$senhacrypt', 'OFFLINE', '$hentra', '$hsaida', '$data', 'S', 'S')";
                            $ecad = $_SESSION['query']($cad) or die (mysql_error());
                            $id = mysql_insert_id();
                            if($ecad) {
                                echo ("<script>alert(\"Monitor cadastrado com sucesso!!!\");
                                </script>");
                                $confemail = "SELECT idconfemailenv FROM confemailenv WHERE tipo='MONITOR'";
                                $nconf = $_SESSION['num_rows']($_SESSION['query']($confemail));
                                if($nconf >= 1) {
                                    $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail));
                                    $mail = new ConfigMail();
                                    $mail->IsSMTP();
                                    $mail->IsHTML(true);
                                    $mail->SMTPAuth = true;
                                    $mail->idconf = $econfemail['idconfemailenv'];
                                    $mail->monitor = $nome;
                                    $mail->idmonitor = $id;
                                    $mail->resp = $resp;
                                    $mail->senha = $gerasenha;
                                    $mail->config();
                                    $selmail = "SELECT nomeuser_adm, email FROM user_adm WHERE iduser_adm='".$mail->resp."'";
                                    $exmail = $_SESSION['fetch_array']($_SESSION['query']($selmail)) or die (mysql_error());
                                    $email = $exmail['email'];
                                    $nome = $exmail['nomeuser_adm'];
                                    $mail->SMTPDebug  = 1; 

                                    $mail->AddAddress("$email", "$nome");
                                    if (!$mail->Send()) {
                                        echo "Mailer Error: " . $mail->ErrorInfo;
                                        echo ("<script>alert(\"E-mail não enviado. Senha temporária ''$gerasenha''!!!\");
                                        window.location = 'admin.php?menu=monitor';
                                        </script>");
                                    }
                                    else {
                                        echo ("<script>alert(\"E-mail enviado com sucesso. Senha temporária ''$gerasenha''!!!\");
                                        window.location = 'admin.php?menu=monitor';
                                        </script>");
                                    }
                                }
                                else {
                                    echo ("<script>alert(\"E-mail não enviado pois não existe configuraçã cadastrada. Senha temporária ''$gerasenha''!!!\");
                                    window.location = 'admin.php?menu=monitor';
                                    </script>");
                                }
                            }
                            else {
                                $msg = "Erro no processo do cadastro, contate o administrador!!!";
                                header("location:admin.php?menu=monitor&msg=".$msg);
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $id = $_POST['id'];
    header("location:admin.php?menu=edmonitor&id=".$id."&altera=altera");
}

if(isset($_POST['canced'])) {
    $id = $_POST['id'];
    header("location:admin.php?menu=monitor&id=".$id);
}

if(isset($_POST['volta'])) {
    $id = $_POST['id'];
    header("location:admin.php?menu=monitor&id=".$id);
}

if(isset($_POST['edita'])) {
    $id = $_POST['id'];
    $seluser = "SELECT * FROM monitor WHERE idmonitor='$id'";
    $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
    $hoentra = explode(':', $eseluser['entrada']);
    $hosaida = explode(':', $eseluser['saida']);
    $entrada = $hoentra['0'].":".$hoentra['1'];
    $saida = $hosaida['0'].":".$hosaida['1'];
    $tabcheck = array("monitor","user_adm","user_web");
    foreach($tabcheck as $tab) {
        if($tab == "monitor") {
            $cuser = "SELECT COUNT(*) as result FROM $tab WHERE usuario='$user' AND id$tab<>'$id'";
        }
        else {
            $cuser = "SELECT COUNT(*) as result FROM $tab WHERE usuario='$user'";
        }
        $luser = $_SESSION['fetch_array']($_SESSION['query']($cuser)) or die (mysql_error());
        if($luser['result'] >= 1) {
            $usercheck++;
        }
    }
    $cnome = "SELECT COUNT(*) as result FROM monitor WHERE nomemonitor='$nome' AND idmonitor<>'$id'";
    $enome = $_SESSION['query']($cnome) or die (mysql_error());
    $lnome = $_SESSION['fetch_array']($enome) or die (mysql_error());
    $ccpf = "SELECT COUNT(*) as result FROM monitor WHERE CPF='$cpf' AND idmonitor<>'$id'";
    $ecpf = $_SESSION['query']($ccpf) or die (mysql_error());
    $lcpf = $_SESSION['fetch_array']($ecpf) or die (mysql_error());
    if($nome == "" || $cpf == "" || $hentra == "" || $hsaida == "" || $user == "") {
	$msg = "Todos os campos precisam estar preenchidos para cadastro do monitor!!!";
	header("location:admin.php?menu=edmonitor&id=".$id."&msg=".$msg);
    }
    else {
        if(checkCPF($cpf) == false) {
          $msg = "CPF inválido, por favor, informe um CPF válido!!!";
          header("location:admin.php?menu=edmonitor&id=".$id."&msg=".$msg);
        }
        else {
            if($nome == $eseluser['nomemonitor'] && $cpf == $eseluser['CPF'] && $resp == $eseluser['iduser_resp'] && $hentra == $entrada && $hsaida == $saida && 
                $user == $eseluser['usuario'] && $atv == $eseluser['ativo'] && $senha_ini == $eseluser['senha_ini'] && $tmonitor == $eseluser['tmonitor']) {
                $msg = "Nenhum campo foi alterado, operaçã não realizada!!!";
                header("location:admin.php?menu=edmonitor&altera=altera&id=".$id."&msg=".$msg);
            }
            else {
                if($usercheck >= 1){
                    $msg = "Este ''usuÃ¡rio'' já está cadastrado na base, favor escolher outro!!!";
                    header("location:admin.php?menu=edmonitor&altera=altera&id=".$id."&msg=".$msg);
                }
                if($lnome['result'] >= 1){
                    $msg = "Este ''Nome de usuÃ¡rio'' já está cadastrado na base, favor escolher outro!!!";
                    header("location:admin.php?menu=edmonitor&altera=altera&id=".$id."&msg=".$msg);
                    break;
                }
                else {
                    if($lcpf['result'] >= 1){
                        $msg = "Este ''CPF'' já está cadastrado na base, favor escolher outro!!!";
                        header("location:admin.php?menu=edmonitor&altera=altera&id=".$id."&msg=".$msg);
                    }
                    else {
                        $alter = "UPDATE monitor SET nomemonitor='$nome', cpf='$cpf', iduser_resp='$resp', entrada='$hentra', saida='$hsaida', usuario='$user',tmonitor='$tmonitor', ativo='$atv', senha_ini='$senha_ini' WHERE idmonitor='$id'";
                        $ealter = $_SESSION['query']($alter) or die (mysql_error());
                        if($ealter) {
                            $msgi = "Cadastro alterado com sucesso!!!";
                            header("location:admin.php?menu=monitor&id=".$id."&msgi=".$msgi);
                        }
                        else {
                            $msg = "Erro no processo de alteração, favor contatar o administrador!!!";
                            header("location:admin.php?menu=edmonitor&altera=altera&id=".$id."&msg=".$msg);
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $id = $_POST['id'];
    $monitoria = "SELECT COUNT(*) as result FROM monitoria WHERE idmonitor='$id'";
    $emoni = $_SESSION['fetch_array']($_SESSION['query']($monitoria)) or die (mysql_error());

    if($emoni['result'] >= 1) {
	$msgi = "Este monitor não pode ser apagado da base pois existem monitorias vinculadas a ele!!!";
	header("location:admin.php?menu=monitor&id=".$id."&msgi=".$msgi);
    }
    else {
        $del = "DELETE FROM monitor WHERE idmonitor='$id'";
        $edel = $_SESSION['query']($del) or die (mysql_error());
        $alter = "ALTER TABLE monitor AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die (mysql_error());
        if($ealter) {
            $msgi = "Monitor apagado da base!!!";
            header("location:admin.php?menu=monitor&id=".$id."&msgi=".$msgi);
        }
        else {
            $msgi = "Ocorreu um erro no processo para apagar o monitor, favor contatar o administrador!!!";
            header("location:admin.php?menu=monitor&id=".$id."&msgi=".$msgi);
        }
    }
}

if(isset($_POST['reset'])) {
    $id = $_POST['id'];
    $mail = "SELECT email FROM user_adm WHERE iduser_adm='$resp'";
    $exmail = $_SESSION['fetch_array']($_SESSION['query']($mail)) or die (mysql_error());
    $email = $exmail['email'];
    $reset = "UPDATE monitor SET senha='$senhacrypt', senha_ini='S', status='OFFLINE' WHERE idmonitor='$id'";
    $ereset = $_SESSION['query']($reset) or die (mysql_error());
    if($ereset) {
        echo ("<script>alert(\"Reset efetuado com sucesso!!!\");
	</script>");
	$confmail = "SELECT *, COUNT(*) as result FROM confemailenv WHERE tipo='MONITOR'";
	$econfmail = $_SESSION['fetch_array']($_SESSION['query']($confmail));
        if($econfmail['result'] == 0) {
            echo ("<script>alert(\"E-mail não enviado, pois nã existe configuraçã relacionada. Senha temporária ''$gerasenha''!!!\");
            window.location = 'admin.php?menu=monitor';
            </script>");
        }
        else {
            $mail = new ConfigMail();
            $mail->IsSMTP();
            $mail->IsHTML(true);
            $mail->SMTPAuth = true;
            $mail->idconf = $econfmail['idconfemailenv'];
            $mail->monitor = $nome;
            $mail->idmonitor = $id;
            $mail->resp = $resp;
            $mail->senha = $gerasenha;
            $mail->config();
            $selmail = "SELECT nomeuser_adm, email FROM user_adm WHERE iduser_adm='".$mail->resp."'";
            $exmail = $_SESSION['fetch_array']($_SESSION['query']($selmail)) or die (mysql_error());
            $email = $exmail['email'];
            $nome = $exmail['nomeuser_adm'];
            $mail->SMTPDebug  = 1; 

            $mail->AddAddress("$email", "$nome");
            if (!$mail->Send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
                echo ("<script>alert(\"E-mail não enviado. Senha temporária ''$gerasenha''!!!\");
                window.location = 'admin.php?menu=monitor';
                </script>");
            }
            else {
                echo ("<script>alert(\"E-mail enviado com sucesso. Senha temporária ''$gerasenha''!!!\");
                window.location = 'admin.php?menu=monitor';
                </script>");
            }
        }
    }
    else {
        $msg = "Erro no processo do cadastro, contate o administrador.!!!";
        header("location:admin.php?menu=monitor&msg=".$msg);
    }
}

if(isset($_POST['dimen'])) {
    $id = $_POST['id'];
    header("location:admin.php?menu=edmonitor&id=".$id."&dimen=dimen");
}

if(isset($_POST['logout'])) {
    $id = $_POST['id'];
    $tabs = array('moni_pausa','monitor','fila_grava fg','fila_oper fo','moni_login ml');
    lock($tabs);
    $selmoni = "SELECT * FROM monitor WHERE idmonitor='$id'";
    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_erro());
    $update = "UPDATE monitor SET status='OFFLINE' WHERE idmonitor='$id'";
    $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o do status do monitor");
    $selpausas = "SELECT * FROM moni_pausa WHERE idmonitor='$id' AND horafim='00:00:00'";
    $eselpausas = $_SESSION['query']($selpausas) or die (mysql_error());
    while($lselpausas = $_SESSION['fetch_array']($eselpausas)) {
        if($lselpausas['data'] != date('Y-m-d')) {
            $check = "SELECT TIMEDIFF('".$eselmoni['saida']."','".$lselpausas['horaini']."') as ret";
        }
        else {
            $verifhora = "SELECT '".date('H:i:s')."' > '".$eselmoni['saida']."' as ret";
            $everifhora = $_SESSION['fetch_array']($_SESSION['query']($verifhora)) or die (mysql_error());
            if($everifhora['ret'] >= 1) {
                $check = "SELECT TIMEDIFF('".$eselmoni['saida']."','".$lselpausas['horaini']."') as ret";
            }
            else {
                $check = "SELECT TIMEDIFF('".date('H:i:s')."','".$lselpausas['horaini']."') as ret";
            }
        }
        $echeck = $_SESSION['fetch_array']($_SESSION['query']($check)) or die (mysql_error());
        if($lselpausas['data'] != date('Y-m-d')) {
            $hora = $eselmoni['saida'];
        }
        else {
            if($everifhora['ret'] >= 1) {
                $hora = $eselmoni['saida'];
            }
            else {
                $hora = date('H:i:s');
            }
        }
        $uppausa = "UPDATE moni_pausa SET horafim='$hora',lib_super='S',tempo='".$echeck['ret']."',iduser_adm='".$_SESSION['usuarioID']."',obs='AUTOMATICO' WHERE idmoni_pausa='".$lselpausas['idmoni_pausa']."'";
        $euppausa = $_SESSION['query']($uppausa) or die (mysql_error());
    }
    $filas = array('fila_oper' => 'fo','fila_grava' => 'fg');
    foreach($filas as $kf => $f) {
        $veriffila = "SELECT DISTINCT(id$kf) FROM $kf $f
                      INNER JOIN moni_login ml ON ml.idmonitor = $f.idmonitor
                      WHERE $f.monitorando='1' AND $f.monitorado<>1 AND $f.idmonitor IS NOT NULL AND ml.logout<>'00:00:00'";
        $everif = $_SESSION['query']($veriffila) or die (mysql_error());
        $nverif = $_SESSION['num_rows']($everif);
        if($nverif >= 1) {
            while($lverif = $_SESSION['fetch_array']($everif)) {
                $updatefila = "UPDATE $kf $f SET monitorando='0' WHERE id$kf='".$lverif['id'.$kf]."'";
                $eupdatefila = $_SESSION['query']($updatefila) or die ("erro na query de atualizaÃ§Ã£o da $kf");
            }
        }
        else {
        }
    }
    unlock();
    if($eupdate) {
        $msg = "Status do Monitor ataulizado com sucesso!!!";
        header("location:admin.php?menu=monitor&msg=".$msg);
    }
    else {
        $msg = "Erro no processo de atualizaÃ§Ã£o do status do monitor, contate o administrador!!!";
        header("location:admin.php?menu=monitor&msg=".$msg);
    }
}
?>