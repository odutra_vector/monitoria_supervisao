<?php
include_once($rais.'/monitoria_supervisao/admin/visufiltros.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');

if($_SESSION['user_tabela'] == "user_adm") {
    $tabuser = "user_adm";
    $part = explode("_",$tabuser);
    $part = $part[1];
    $tabreluser = "useradmfiltro";
    $atabuser = "a";
    $atabreluser = "af";
}
if($_SESSION['user_tabela'] == "user_web") {
    $tabuser = "user_web";
    $part = explode("_",$tabuser);
    $part = $part[1];
    $tabreluser = "userwebfiltro";
    $atabuser = "w";
    $atabreluser = "wf";
}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $("#gerar").click(function() {
            var idperiodo = $("#periodo").val();
            if(idperiodo == "") {
                alert("POR FAVOR, INFORME O PERIODO!!!");
                return false;
            }
            else {
                $.blockUI({ message: '<strong>AGUARDE GERANDO BASE...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
            }
        });
        
        $("a[id*='exclui']").click(function() {
            var arquivo = $(this).attr("name");
            var confirma = confirm("Você confirma a exclusão do arquivo?");
            if(confirma == true) {
                window.location = "admin.php?menu=exportacaoimp&del="+arquivo;
            }
            else {
                return false;
            }
        })
    })
</script>
<script type="text/javascript" src="/monitoria_supervisao/users/combofiltros_<?php echo strtolower($_SESSION['nomecli']);?>.js"></script>
<div id="conteudo">
    <div id="opcoesgera" style="padding-top: 20px">
        <form action="" method="post">
            <table>
                <tr>
                    <td class="corfd_coltexto" style="font-weight: bold">PERÍODO</td>
                    <td class="corfd_colcampos">
                        <select name="periodo" id="periodo">
                            <option value="">Selecione o Período</option>
                            <?php
                            $selper = "SELECT * FROM periodo ORDER BY ano DESC, mes DESC";
                            $eselper = $_SESSION['query']($selper) or die (mysql_error());
                            while($lselper = $_SESSION['fetch_array']($eselper))  {
                                if($_POST['periodo'] == $lselper['idperiodo']) {
                                    $selected = "selected=\"selected\"";
                                }
                                else {
                                    $selected = "";
                                }
                                ?>
                                <option <?php echo $selected;?> value="<?php echo $lselper['idperiodo'];?>"><?php echo $lselper['nmes']."-".$lselper['ano'];?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <?php
                  $filtnome = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                  $efilt = $_SESSION['query']($filtnome) or die (mysql_error());
                  $idfiltros = array();
                  $nomefiltros = array();
                  $url = array();
                  $urlid = array();
                  while($lfilt = $_SESSION['fetch_array']($efilt)) {
                    $campos[strtolower($lfilt['nomefiltro_nomes'])] = "filtro_".strtolower($lfilt['nomefiltro_nomes']);
                    if($lfilt['idfiltro_nomes'] == $eultimo['idfiltro_nomes']) {
                        $style = "style=\"display:none\"";
                    }
                    else {
                        $style = "";
                    }
                    echo "<tr $style>";
                      echo "<td class=\"corfd_coltexto\"><strong>".$lfilt['nomefiltro_nomes']."</strong></td>";
                          echo "<td class=\"corfd_colcampos\"><select style=\"width:252px\" name=\"filtro_".strtolower($lfilt['nomefiltro_nomes'])."\" id=\"filtro_".strtolower($lfilt['nomefiltro_nomes'])."\">";
                          if($_POST['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == "") {
                              $selected = "selected=\"selected\"";
                              echo "<option $selected value=\"\" >Selecione um filtro</option>";
                          }
                          else {
                              $selected = "";
                              echo "<option $selected value=\"\" >Selecione um filtro</option>";
                          }
                          $seldados = "SELECT fd.idfiltro_dados, fd.nomefiltro_dados FROM $tabuser $atabuser INNER JOIN $tabreluser $atabreluser ON $atabreluser.iduser_$part = $atabuser.id$tabuser
                                      INNER JOIN rel_filtros rf ON rf.idrel_filtros = $atabreluser.idrel_filtros
                                      INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower($lfilt['nomefiltro_nomes'])."
                                      WHERE $atabuser.id$tabuser='".$_SESSION['usuarioID']."' GROUP BY rf.id_".strtolower($lfilt['nomefiltro_nomes'])." ORDER BY nomefiltro_dados";
                          $edados = $_SESSION['query']($seldados) or die (mysql_error());
                          while($ldados = $_SESSION['fetch_array']($edados)) {
                            if($_POST['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == $ldados['idfiltro_dados'] || $getfiltros['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == $ldados['idfiltro_dados']) {
                                $selected = "selected=\"selected\"";
                                $idfiltros[] = $ldados['idfiltro_dados'];
                                $nomefiltros[] = 'id_'.strtolower($lfilt['nomefiltro_nomes']);
                            }
                            else {
                                $selected = "";
                            }
                            echo "<option $selected value=\"".$ldados['idfiltro_dados']."\">".$ldados['nomefiltro_dados']."</option>";
                          }
                          echo "</select></td>";
                      echo "</tr>";
                  }
                ?>
                <tr>
                    <td><input type="submit" name="gerar" id="gerar" value="EXPORTAR" style="border:1px solid #333; height: 18px; background-image:url(../images/button.jpg); text-align:center" /></td>
                </tr>
            </table>
        </form>
    </div>
    <div id="basesimp" style="margin-top: 20px">
        <fieldset>
            <legend style="margin-left:20px;padding:5px; border:2px solid #333; background-color:#FFF;font-weight:bold ">
                DADOS IMPORTAÇÕES
            </legend>
            <div style="margin-top:10px">
                <ul style="list-style: none; margin:0;padding-left: 20px;padding-bottom: 10px">
                    <?php
                    if(isset($_POST['gerar'])) {
                        foreach($campos as $kpost => $cpost) {
                            if($_POST[$cpost] != "") {
                                $whererels[] = "id_$kpost='$_POST[$cpost]'";
                                $selnome = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='$_POST[$cpost]'";
                                $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                                $paramfiltros[] = $eselnome['nomefiltro_dados'];
                            }
                        }
                        if(isset($whererels)) {
                            $where = "WHERE ".implode(" AND ",$whererels);
                            $paramarq = implode("_",$paramfiltros)."_";
                        }
                        else {
                            $where = "";
                            $paramarq = "";
                        }
                        $selrels = "SELECT rf.idrel_filtros FROM rel_filtros rf
                                    INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                    $where";
                        $eselrels = $_SESSION['query']($selrels) or die (mysql_error());
                        $nrels = $_SESSION['num_rows']($eselrels);
                        if($nrels >= 1) {
                            while (($lrels = $_SESSION['fetch_array']($eselrels))) {
                                $idsrel[] = $lrels['idrel_filtros'];
                            }
                            $periodo = "SELECT * FROM periodo WHERE idperiodo='".$_POST['periodo']."'";
                            $eperiodo = $_SESSION['fetch_array']($_SESSION['query']($periodo)) or die (mysql_error());
                            $pasta = $_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/exportacaoimp/".$_SESSION['nomecli'];
                            $criapasta = mkdir($pasta,0777);
                            $arquivo = $pasta."/$paramarq".$eperiodo['nmes']."_".$eperiodo['ano']."_".date('dmY')."_".date("His").".csv";
                            $cria = fopen($arquivo, "w+");
                            $colunas = "ID. Upload;Data IMP;ID. Rel;Nome Relacionamento;Nome Arquivo;datactt;horactt;OBS";
                            fwrite($cria,$colunas."\r\n");
                            if($eperiodo['dtinictt'] != "" && $eperiodo['dtfimctt'] != "") {
                                $dtini = $eperiodo['dtinictt'];
                                $dtfim = $eperiodo['dtfimctt'];
                            }
                            else {
                                $dtiniper = $eperiodo['dataini'];
                                $dtini = substr($dtiniper,0,4)."-".substr($dtiniper,5,2)."-01";
                                $dtfim = ultdiames(substr($dtini,8,2).substr($dtini,5,2).substr($dtini,0,4));
                            }
                            foreach($idsrel as $idrel) {
                                $selupload = "SELECT u.idupload,camupload,dataimp FROM upload u
                                              INNER JOIN fila_grava fg ON fg.idupload = u.idupload
                                              WHERE datactt BETWEEN '$dtini' and '$dtfim' AND u.idrel_filtros='$idrel' AND imp='S' GROUP BY u.idupload";
                                $eselupload = $_SESSION['query']($selupload) or die (mysql_error());
                                $nupload = $_SESSION['num_rows']($eselupload);
                                if($nupload >= 1) {
                                    while($lupload = $_SESSION['fetch_array']($eselupload)) {
                                        foreach(scandir($lupload['camupload']) as $arqpasta) {
                                            if(strstr($arqpasta,"logimp")) {
                                                $dados = file($lupload['camupload']."/".$arqpasta, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                                            }
                                        }
                                        foreach($dados as $kdata => $dado) {
                                            if($kdata == 0) {
                                                $colunas = explode(";",$dado);
                                            }
                                            else {
                                                $l++;
                                                $dlinha = explode(";",$dado);
                                                $addlinha = $lupload['idupload'].";".banco2data($lupload['dataimp']).";".$dlinha[array_search("ID. Rel", $colunas)].";".$dlinha[array_search("Nome Relacionamento", $colunas)].";".$dlinha[array_search("Nome Arquivo", $colunas)].";".$dlinha[array_search("datactt", $colunas)].";".$dlinha[array_search("horactt", $colunas)].";".$dlinha[array_search("OBS", $colunas)];
                                                fwrite($cria, $addlinha."\r\n");
                                            }
                                        }
                                    }
                                }
                            }
                            fclose($cria);
                        }
                        ?>
                        <script type="text/javascript">
                            <?php
                            if($l >= 1) {
                            ?>
                            alert("EXPORTAÇÃO GERADA COM SUCESSO!!!");
                            window.location = "admin.php?menu=exportacaoimp";
                            <?php
                            }
                            else {
                            ?>
                            alert("EXPORTAÇÃO SEM DADOS!!!");
                            window.location = "admin.php?menu=exportacaoimp";
                            <?php    
                            }
                            ?>
                        </script>
                        <?php
                    }
                    if(isset($_GET['del'])) {
                        $arquivo = $_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/exportacaoimp/".$_SESSION['nomecli']."/".$_GET['del'];
                        if(unlink($arquivo)) {
                            ?>
                            <script type="text/javascript">
                                alert("Arquivo apagado com sucesso!!!");
                            </script>
                            <?php
                        }
                    }
                    $arquivos = scandir($_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/exportacaoimp/".$_SESSION['nomecli']);
                    foreach($arquivos as $file) {
                        if($file != "." && $file != "..") {
                            $e++;
                            ?>
                            <li style="background-color: #FFF; margin-top: 5px;padding: 5px; width: 60%">
                                <a id="exclui<?php echo $e;?>" href="#" name="<?php echo $file;?>" style="text-decoration:none; outline: none; border: 0px;"><img src="/monitoria_supervisao/images/exit.png"/></a> <a href="<?php echo "/monitoria_supervisao/exportacaoimp/".$_SESSION['nomecli']."/$file";?>" target="_blank" style="text-decoration: none; color:#000;"><?php echo $file;?></a>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </fieldset><br/>
    </div>
</div>
