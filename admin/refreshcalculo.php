<?php
session_start();
header('Content-type: text/html; charset=utf-8');
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/comandossql.php');
comandossql();
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/classes/class.salvamonitoria.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.phpmailer.php');
if(!file_exists($rais."/monitoria_supervisao/logs/logrefreshcalculo.csv")) {
    $cria = fopen($rais."/monitoria_supervisao/logs/logrefreshcalculo.csv",'a+');
    chmod($rais."/monitoria_supervisao/logs/logrefreshcalculo.csv", 0777);
    fwrite($cria, "data;hora;retorno;qtdemonitorias"."\r\n");
}
else {
    $cria = fopen($rais."/monitoria_supervisao/logs/logrefreshcalculo.csv",'a+');
    chmod($rais."/monitoria_supervisao/logs/logrefreshcalculo.csv", 0777);
}

try {
    $email = new PHPMailer(true);
    $email->IsSMTP();
    $confemail = 0;
    $q = 0;
    $msg = "<html>";
    $msg .= "<header>";
    $msg .= "</header>";
    $msg .= "<body>";
    $msg .= "Segue os ID's das monitorias que tiveram suas notas recalculadas para corrigir instabilidade do banco de dados.<br/><br/>";

    $moni = new Salva_Moni();
    $selbanco = "SHOW DATABASES";
    $eselbanco = mysql_query($selbanco) or die (mysql_error());
    while($lselbanco = mysql_fetch_array($eselbanco)) {
        if(strstr($lselbanco['Database'],"monitoria_")) {
            $seldb = mysql_select_db($lselbanco['Database']);
            if($confemail == 0) {
                $confmail = "SELECT * FROM conf_mail";
                $econfmail = mysql_fetch_array(mysql_query($confmail)) or die ("erro na consulta da configuraÃ§Ã£o para envio de e-mail");
                if($econfmail['tls'] == "SIM") {
                    $email->SMTPSecure = "tls";
                }
                $email->IsHTML(true);
                $email->CharSet = "UTF-8";
                $email->Timeout = 30;
                $email->Host = $econfmail['host'];
                $email->Port = $econfmail['porta'];
                if($econfmail['autentica'] == 1) {
                    $email->SMTPAuth = true;
                    $email->Username = $econfmail['email'];
                    $email->Password = $econfmail['senha'];
                }
                else {
                    $email->SMTPAuth = false;
                }
                $email->SetFrom($econfmail['email'],"CLIENTIS");
                $email->Subject = "EMAIL DE MONITORIAS RE-CALCULADAS AUTOMATICAMENTE";
                $confemail = 1;
            }
            $idscalc = array();
            if($seldb) {
                $selmoni = "SELECT * FROM monitoria WHERE qtdefg is null";
                $eselmoni = mysql_query($selmoni) or die (mysql_error());
                while($lselmoni = mysql_fetch_array($eselmoni)) {
                    $q++;
                    $moni->idmoni = $lselmoni['idmonitoria'];
                    $moni->tabavalia = "moniavalia";
                    $moni->tabmoni = "monitoria";
                    $moni->plan = $lselmoni['idplanilha'];
                    $moni->CalcValorSemFG();
                    $moni->CalcValorFG();
                    $check = "SELECT qtdefg FROM monitoria WHERE idmonitoria='".$moni->idmoni."'";
                    $echeck = mysql_fetch_array(mysql_query($check)) or die (mysql_error());
                    if($echeck['qtdefg'] != "") {
                        if($lselbanco['Database'] != "monitoria_escob_acc") {
                            $upmoni = "UPDATE monitoria SET checkfg='NCG OK' WHERE idmonitoria='".$moni->idmoni."'";
                            $eupmoni = mysql_query($upmoni);
                        }
                        $idscalc[] = $moni->idmoni." - ".banco2data($lselmoni['data']);
                    }
                }
            }
            $msg .= strtoupper($lselbanco['Database'])."<br/>-------------------------<br/>";
            $msg .= implode("<br/>",$idscalc)."<br/><br/>";
        }
    }
    $msg .= "</body>";
    $msg .= "</html>";
    $email->Body = $msg;
    $email->AddAddress("odutra@vectordsitec.com.br","Otto");
    if($q >= 1) {
        if($email->Send()) {
            fwrite($cria, date('d/m/Y').";".date('H:i:s').";Mensagem enviada;$q"."\r\n");
        }
        else {
            fwrite($cria, date('d/m/Y').";".date('H:i:s').";Mailer Error: " . $email->ErrorInfo.";"."\r\n");
        }
    }
    else {
        fwrite($cria, date('d/m/Y').";".date('H:i:s').";nada feito;$q"."\r\n");
    }
}
catch (phpmailerException $e) {
    fwrite($cria, date('d/m/Y').";".date('H:i:s').";".$e->errorMessage().";"."\r\n"); //Pretty error messages from PHPMailer
} 
catch (Exception $e) {
    fwrite($cria, date('d/m/Y').";".date('H:i:s').";".$e->getMessage().";"."\r\n"); //Boring error messages from anything else!
}
