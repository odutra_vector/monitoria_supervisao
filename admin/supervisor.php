<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once("functionsadm.php");

$classtab = new TabelasSql();
$periodo = periodo(datas);

$asuper = $classtab->AliasTab(super_oper);
$aconf = $classtab->AliasTab(conf_rel);
$aparam = $classtab->AliasTab(param_moni);
$tabs = array('O' => 'fila_oper','G' => 'fila_grava');
$tipocols = "SHOW COLUMNS FROM super_oper";
$etipo = $_SESSION['query']($tipocols) or die ("erro na query de consulta dos campos do supervisor");
while($ltipo = $_SESSION['fetch_array']($etipo)) {
	$cols[$ltipo['Field']] = $ltipo['Type'];
	$colssql[] = $asuper.".".$ltipo['Field'];
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem tÃ­tulo</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#cpfpesq').mask('999.999.999-99');
        $('.DATE').mask("99/99/9999");
        $('.TIME').mask("99:99:99");
        $('#tabsuper').tablesorter();
        $('#hist').tablesorter();
        
        $("#delsuper").click(function() {
            var id = prompt("INFORME O CÓDIGO DO SUPERVISOR QUE ASSUIRÁ AS MONITORIAS E REGISTROS DESTE SUPERVIDOR QUE SERÁ DELETADO");
            var iddel = $("#idsuper").val();
            if(id == "") {
                alert("FAVOR INFORMAR UM ID DE SUPERVIDOR PARA REPASSAR OS DADOS!!!");
                return false;
            }
            else {
                $.post("/monitoria_supervisao/admin/cadsuper.php",{delete:'delete',iddel:iddel,id:id},function(ret) {
                    var int = parseInt(ret);
                    if(int >= 1) {
                        alert("SUPERVISOR DELETADO COM SUCESSO E DADOS TRANSFERIDOS COM SUCESSO");
                        window.location = '/monitoria_supervisao/admin/admin.php?menu=supervisor';
                    }
                    else {
                        if(int == -1) {
                            alert("O VALOR INFORMADO, NÃO É UM NÚMERO OU O ID ESTÁ INCORRETO");
                        }
                        else {
                            alert("SUPERVISOR DELETADO COM SUCESSO, SEM TRANSFERENCIA DE DADOS");
                            window.location = '/monitoria_supervisao/admin/admin.php?menu=supervisor';
                        }
                    }
                });
            }
        });
    })
</script>
</head>
<body>
    <div id="conteudo" class="corfd_pag">
        <form action="" method="post">
            <table width="200" style="border: 1px solid #FFF">
              <tr>
                <td class="corfd_ntab" colspan="2" align="center"><strong>PESQUISA SUPERVISOR</strong></td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>CPF</strong></td>
                <td class="corfd_colcampos"><input name="cpfpesq" id="cpfpesq" type="text" style="width:150px; border: 1px solid #729fcf; text-align:center" value="<?php if(isset($_POST['cpfpesq'])) { echo $_POST['cpfpesq']; } else { }?>" /></td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>NOME</strong></td>
                <td class="corfd_colcampos"><input name="superpesq" id="superpesq" type="text" style="width:350px; border: 1px solid #729fcf" value="<?php if(isset($_POST['superpesq'])) { echo $_POST['superpesq']; } else { }?>" /></td>
              </tr>
              <tr>
                <?php
                    $selult = "SELECT MIN(nivel), nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
                    $eselult = $_SESSION['fetch_array']($_SESSION['query']($selult)) or die ("erro na query de consulta do filtro de menor nÃ­vel");
                    echo "<td class=\"corfd_coltexto\"><strong>".$eselult['nomefiltro_nomes']."</strong></td>";
                    echo "<td class=\"corfd_colcampos\"><select name=\"rel\" style=\"width:100%\">";
                    if(isset($_POST['rel'])) {
                        if($_POST['rel'] == "") {
                            echo "<option value=\"\" selected=\"selected\">TODOS...</option>";
                        }
                        else {
                            echo "<option value=\"\">TODOS...</option>";
                        }
                    }
                    else {
                        echo "<option value=\"\" selected=\"selected\">TODOS...</option>";
                    }
                    $selrel = "SELECT * FROM rel_filtros";
                    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consutla dos relacionamentos ativos");
                    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                        $verifconf = "SELECT COUNT(*) as result FROM conf_rel WHERE idrel_filtros='".$lselrel['idrel_filtros']."'";
                        $econf = $_SESSION['fetch_array']($_SESSION['query']($verifconf)) or die ("erro na query de consulta da configuraÃ§Ã£o do relacionamento");
                        if($econf['result'] >= 1) {
                            if($lselrel['idrel_filtros'] == $_POST['rel']) {
                                $select = "selected=\"selected\"";
                            }
                            else {
                                $select = "";
                            }
                            echo "<option value=\"".$lselrel['idrel_filtros']."\" $select>".nomeapres($lselrel['idrel_filtros'])."</option>";
                        }
                        else {
                        }
                    }
                    echo "</select></td>";
              	?>
              </tr>
              <tr>
                <td colspan="2"><input style="border: 1px solid #FFF; height: 18px;width:80px; background-image:url(../images/button.jpg)" name="pesq" type="submit" value="Pesquisar" /></td>
              </tr>
            </table>  
        </form><br/><hr/>
        <form action="cadsuper.php" method="post">
        <div id="dadossuper" style="width:1024px; border: 1px solid #FFF; float:left; margin-bottom: 30px">
            <?php
                if(isset($_GET['idsuper']) && !isset($_POST['pesq'])) {
                    ?>
                    <table width="500">
                        <tr>
                            <td class="corfd_ntab" colspan="2" align="center"><strong>DADOS SUPERVISOR</strong><input type="hidden" id="idsuper" value="<?php echo $_GET['idsuper'];?>" /></td>
                        </tr>
                        <?php
                        $idsuper = $_GET['idsuper'];
                        $block = array('idsuper_oper','dtbase','horabase');
                        $seldados = "SELECT *,count(*) as r FROM super_oper WHERE idsuper_oper='$idsuper'";
                        $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query para seleiconar os dados do suepervisor");
                        if($eseldados['r'] >= 1) {
                                foreach($cols as $klist => $tlist) {
                                    if(in_array($klist,$block)) {
                                        if($klist == "idsuper_oper") {
                                            $inputid = "<input type=\"hidden\" name=\"$klist\" value=\"$eseldados[$klist]\" />";
                                            $dados = $eseldados[$klist];
                                        }
                                        if($tlist == "date") {
                                            $dados = banco2data($eseldados[$klist]);
                                        }
                                        if($klist != "idsuper_oper" && $tlist != "date") {
                                            $dados = $eseldados[$klist];
                                        }
                                        $td = "<td class=\"corfd_colcampos\">$inputid$dados</td>";
                                    }
                                    else {
                                        if($klist == "super_oper") {
                                            $dados = strtoupper($eseldados[$klist]);
                                        }
                                        else {
                                            $dados = $eseldados[$klist];
                                        }
                                        $td = "<td class=\"corfd_colcampos\"><input name=\"$klist\" style=\"width:320px; border: 1px solid #729fcf\" type=\"text\" value=\"$dados\" /></td>";
                                    }
                                    ?>
                                    <tr>
                                        <td class="corfd_coltexto"><strong><?php echo strtoupper($klist);?></strong></td>
                                        <?php
                                         echo $td;
                                         ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table><font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font><br></br>
                            <table>
                            <tr>
                              <td><input style="border: 1px solid #FFF; height: 18px; width:80px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="altera" id="altera" type="submit" value="Alterar" /><input type="button" name="delsuper" id="delsuper" value="DELETA SUPER" style=" margin-left: 10px; background-color: #E65E5E; color:#000; border: 0px; padding: 5px; font-weight: bold" /></td>
                            </tr>
                            </table>
                            <table width="1024" id="hist">
                                <thead>
                                <th class="corfd_coltexto" align="center"><strong>ID REL.</strong></th>
                                <th class="corfd_coltexto" align="center"><strong>ID UP.</strong></th>
                                <th class="corfd_coltexto" align="center"><strong>TIPO FILA</strong></th>
                                <th class="corfd_coltexto" align="center"><strong><?php echo $eselult['nomefiltro_nomes'];?></strong></th>
                                <th class="corfd_coltexto" align="center"><strong>MES</strong></th>
                                <th class="corfd_coltexto" align="center"><strong>DATA INI</strong></th>
                                <th class="corfd_coltexto" align="center"><strong>DATA FIM</strong></th>
                                </thead>
                              <tbody>
                                  <?php
                                  foreach($tabs as $alias => $tabfila) {
                                      $a = strtolower($alias);
                                      $selsuper = "SELECT $a.idrel_filtros,$a.idupload,u.tabfila,p.nmes,ip.dataini, ip.datafim FROM $tabfila $a
                                                  INNER JOIN upload u ON u.idupload = $a.idupload
                                                  INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                                                  INNER JOIN interperiodo ip ON ip.idperiodo = u.idperiodo AND ip.semana = u.semana
                                                  WHERE $a.idsuper_oper='$idsuper' GROUP BY $a.idupload ORDER BY dataini DESC";
                                      $eselsuper = $_SESSION['query']($selsuper) or die ("erro na query de consulta da fila");
                                      while($lselsuper = $_SESSION['fetch_array']($eselsuper)) {
                                          ?>
                                          <tr>
                                                <td class="corfd_colcampos" align="center"><?php echo $lselsuper['idrel_filtros'];?></td>
                                                <td class="corfd_colcampos" align="center"><?php echo $lselsuper['idupload'];?></td>
                                                <td class="corfd_colcampos" align="center"><?php echo strtoupper($lselsuper['tabfila']);?></td>
                                                <td class="corfd_colcampos" align="center"><?php echo nomeapres($lselsuper['idrel_filtros']);?></td>
                                                <td class="corfd_colcampos" align="center"><?php echo $lselsuper['nmes'];?></td>
                                                <td class="corfd_colcampos" align="center"><?php echo banco2data($lselsuper['dataini']);?></td>
                                                <td class="corfd_colcampos" align="center"><?php echo banco2data($lselsuper['datafim']);?></td>
                                          </tr>
                                          <?php
                                      }
                                  }
                                  ?>
                              </tbody>
                            </table>
                            <?php
                        }
                        else {
                            ?>
                            <tr align="center"><td style="background-color: #FFF; text-align: center; font-size: 14px; font-weight: bold" colspan="2"><span>O ID informado é inválido</span></td></tr>
                            </table>
                            <?php
                        }
                }
            ?>
        </div><p></p>
        </form>
        <div id="listasuper" style="width:1024px; overflow:auto; height:300px;">
        <table width="1000" id="tabsuper">
            <thead>
                <?php
                foreach($cols as $kcols => $tcols) {
                    if($kcols == "idsuper_oper") {
                        ?>
                        <th class="corfd_coltexto" align="center"><strong><?php echo "ID";;?></strong></th>
                        <?php
                    }
                    else {
                        ?>
                        <th class="corfd_coltexto" align="center"><strong><?php echo strtoupper($kcols);?></strong></th>
                        <?php
                    }
                }
                ?>
            </thead>
            <tbody>
                <?php
                $cpf = str_replace(".","",$_POST['cpfpesq']);
                $cpf = str_replace("-","",$cpf);
                if(isset($_POST['pesq'])) {
                    $opcoes = array('cpf' => $cpf, 'supervisor' => $_POST['superpesq'], 'rel' => $_POST['rel']);
                    foreach($opcoes as $op) {
                        if($op == "") {
                        }
                        else {
                            $countp++;
                        }
                    }
                    if($countp >= 1) {
                        foreach($opcoes as $kop => $opcao) {
                            if($kop == "cpf") {
                                if($_POST['cpfpesq'] == "") {
                                }
                                else {
                                    $where[] = $asuper.".cpfsuper='$opcao'";
                                }
                            }
                            if($kop == "supervisor") {
                                if($_POST['superpesq'] == "") {
                                }
                                else {
                                    $where[] = $asuper.".super_oper LIKE '%$opcao%'";
                                }
                            }
                            if($kop == "rel") {
                                if($_POST['rel'] == "") {
                                }
                                else {
                                    $idrel = $opcao;
                                    $selparam = "SELECT tipomoni FROM conf_rel $aconf 
                                                INNER JOIN param_moni $aparam ON $aparam.idparam_moni = $aconf.idparam_moni WHERE $aconf.idrel_filtros='$idrel'";
                                    $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die ("erro na query para consultar o parametro do relacionamento selecionado");
                                    $inner[] = "INNER JOIN ".$tabs[$eselparam['tipomoni']]." ".$classtab->AliasTab($tabs[$eselparam['tipomoni']])." ON ".$classtab->AliasTab($tabs[$eselparam['tipomoni']]).".idsuper_oper = $asuper.idsuper_oper"; 
                                    $where[] = $classtab->AliasTab($tabs[$eselparam['tipomoni']]).".idrel_filtros='$opcao'";                            
                                }
                            }
                        }
                        $selsuper = "SELECT ".implode(",",$colssql)." FROM super_oper $asuper ".implode(",",$inner)." WHERE ".implode(" AND ",$where)." GROUP BY $asuper.idsuper_oper ORDER BY $asuper.super_oper";
        
                    }
                    else {
                        $selsuper = "SELECT ".implode(",",$colssql)." FROM super_oper $asuper ORDER BY super_oper";	
                    }
                }
                else {
                    $selsuper = "SELECT ".implode(",",$colssql)." FROM super_oper $asuper ORDER BY super_oper";
                }
                $eselsuper = $_SESSION['query']($selsuper) or die ("erro na query de consulta dos supervisores");
                while($lselsuper = $_SESSION['fetch_array']($eselsuper)) {
                    echo "<tr>";
                    foreach($cols as $coluna => $tipocol) {
                        if($coluna == "idsuper_oper" OR $coluna == "super_oper") {
                            if($coluna == "idsuper_oper") {
                                echo "<td class=\"corfd_colcampos\" align=\"center\"><a href=\"admin.php?menu=supervisor&visu=det&idsuper=".$lselsuper['idsuper_oper']."\" style=\"text-decoration:none; color:#000\">".$lselsuper['idsuper_oper']."</a></td>"; 
                            }
                            if($coluna == "super_oper") {
                                echo "<td class=\"corfd_colcampos\" align=\"center\"><a href=\"admin.php?menu=supervisor&visu=det&idsuper=".$lselsuper['idsuper_oper']."\" style=\"text-decoration:none; color:#000\">".$lselsuper['super_oper']."</a></td>";
                            }							
                        }
                        if($tipocol == "date") {
                            echo "<td align=\"center\" class=\"corfd_colcampos\">".banco2data($lselsuper[$coluna])."</td>";
                        }
                        if($coluna != "idsuper_oper" && $coluna != "super_oper" && $tipocol != "date") {
                            echo "<td align=\"center\" class=\"corfd_colcampos\">".$lselsuper[$coluna]."</td>";
                        }
                    }
                    echo "</tr>";
                }
                ?>
                </tbody>
           </table>         
        </div>    
    </div>
</body>
</html>
