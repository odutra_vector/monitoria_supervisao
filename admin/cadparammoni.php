
<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

if(isset($_POST['cadastra'])) {
    $ip = explode(":",$_POST['endereco']);
    $login = $_POST['loginftp'];
    $senha = $_POST['senhaftp'];
    $ftp = ftp_connect($_POST['endereco']);
    $ftplogin = ftp_login($ftp, $login, $senha);
    $indice = $_POST['indice'];
    $dataimp = $_POST['dataimp'];
    $pridataimp = $_POST['pridataimp'];
    if(in_array($pridataimp, $arraypri) && $pridataimp != 0) {
        $existe++;
    }
    $arraypri[] = $pridataimp;
    $fdataimp = trim($dataimp.",".$pridataimp);
    $dataultmoni = $_POST['ultmoni'];
    $pridataulmoni = $_POST['priultmoni'];
    if(in_array($pridataulmoni, $arraypri) && $pridataulmoni != 0) {
        $existe++;
    }
    $arraypri[] = $pridataulmoni;
    $fdataultmoni = trim($dataultmoni.",".$pridataulmoni);
    $datafim = $_POST['datafim'];
    $pridatafim = $_POST['pridatafim'];
    if(in_array($pridatafim, $arraypri) && $pridatafim != 0) {
        $existe++;
    }
    $arraypri[] = $pridatafim;
    $fdatafim = trim($datafim.",".$pridatafim);
    if($_POST['dultimp'] == "") {
        $dultimp = 0;
    }
    else {
        $dultimp = $_POST['dultimp'];
    }
    $pridultimp = $_POST['pridultimp'];
    if(in_array($pridultimp, $arraypri) && $pridultimp != 0) {
        $existe++;
    }
    $arraypri[] = $pridultimp;
    $fdultimp = trim($dultimp.",".$pridultimp);
    $datalimmulti = $_POST['limmulti'];
    $prilimmulti = $_POST['prilimmulti'];
    if(in_array($prilimmulti, $arraypri) && $prilimmulti != 0) {
        $existe++;
    }
    $arraypri[] = $prilimmulti;
    $flimmulti = trim($datalimmulti.",".$prilimmulti);
    $tpordem = $_POST['tpordem'];
    $atvordem = $_POST['atvordem'];
    $ordem = trim($atvordem.",".$tpordem);
    $tipomoni = $_POST['tipomoni'];
    $checkfg = $_POST['checkfg'];
    $tipoimp = $_POST['tipoimp'];
    $semdados = $_POST['semdados'];
    $tipoaudio = implode(",",$_POST['tipoaudio']);
    $tpcompress = implode(",",$_POST['tpcompress']);
    $tparquivo = implode(",",$_POST['tparquivo']);
    $tiposervidor = val_vazio($_POST['tiposervidor']);
    if($_POST['tiposervidor'] == "LOCAL") {
        $endereco = "NULL";
        $loginftp = "NULL";
        $senhaftp = "NULL";
    }
    else {
        $endereco = val_vazio($_POST['endereco']);
        $loginftp = val_vazio($_POST['loginftp']);
        $senhaftp = val_vazio(base64_encode($_POST['senhaftp']));
    }
    $camaudio = trim($_POST['camaudio']);
    $ccampos = trim($_POST['ccampos']);
    $cpartes = trim($_POST['cpartes']);
    $conf = $_POST['conf'];
    $hpa = $_POST['hpa'];
    $hmonitor = $_POST['hmonitor'];
    $caractertime = "[)(*&%$#@!?/\.,;-|^~]";
    $caractercamaudio = "[]$[><}{;,!?*%&#@]";
    $caractersepara = "^[a-z,A-Z,0-9]";
    $campos = $_POST['campos'];

    if($indice == "" || $tipomoni == "" || $tipoimp == "" || $checkfg == "" || $camaudio == "" || $hpa == "" || $hmonitor == "") {
        $msg = "O preenchimento dos campos com asterisco são obrigatórios";
        header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
    }
    else {
        if(!$ftp && $tiposervidor == "FTP") {
            $msg = "Erro na conexão ao IP do FTP";
            header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
        }
        else {
            if(!$ftplogin && $tiposervidor == "FTP") {
                $msg = "Erro no login com o FTP";
                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
            }
            else {
                if($existe != 0) {
                    $msgi = "Existem dois filtros do parametro com a mesma prioridade, favor verificar";
                    header("location:admin.php?menu=parammoni&msg=".$msgi);
                }
                else {
                    if($tipomoni == "G") {
                        if($tipoimp == "A") {
                            if($ccampos == "" || $cpartes == "" || $tipoaudio == "" || $tpcompress == "") {
                                $msg = "Os campos ''Separa Campos'', ''Separa Partes'' e Tipo Audio são obrigatórios!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                if(eregi($caractersepara, $ccampos) || (eregi($caractersepara, $cpartes))) {
                                    $msg = "Os campos ''Separa Campos ou Partes'' não podem conter os caracteres ''$caractersepara''";
                                    header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                                }
                                else {
                                    $pass = "OK";
                                }
                            }
                        }
                        if($tipoimp == "LA") {
                            if($tipoaudio == "" || $tparquivo == "" || $tpcompress == "") {
                                $msg = "Os campos ''Tipo Audio'', ''Tipo Arquivo'', ''Tipo Compressçao'' são obrigatórios!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                $pass = "OK";
                            }
                        }
                        if($tipoimp == "I") {
                            if($tipoaudio == "" || $tparquivo == "") {
                                $msg = "O campo Tipo Audio é obrigatório!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                $pass = "OK";
                            }
                        }
                    }
                    if($tipomoni == "O") {
                        if($tipoimp == "L") {
                            if($tipoaudio == "" || $tparquivo == "") {
                                $msg = "O campo Tipo Audio é obrigatório!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                $pass = "OK";
                            }
                        }
                        if($tipoimp == "SL") {
                            if($tipoaudio == "") {
                                $msg = "O campo Tipo Audio é obrigatório!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                $pass = "OK";
                            }
                        }
                    }
                    if($pass == "OK") {
                        if($_POST['tiposervidor'] != "FTP" && !is_dir($camaudio)) {
                                $msg = "O caminho para salvar os audios não é um diretório!!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                        }
                        else {
                            if(eregi($caractercamaudio, $camaudio)) {
                                $msg = "O campo ''Caminho'' não pode conter os caracteres ''$caractercamaudio''";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                if(eregi($caractertime, $hpa) || eregi($caractertime, $hmonitor)) {
                                    $msg = "O/s campo/s ''Horas PA ou Horas Monitor'' não pode conter os caracteres ''$caractercamaudio''";
                                    header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                                }
                                else {
                                    if($_POST['tiposervidor'] == "FTP") {
                                    }
                                    else {
                                        if(file_exists($camaudio)) {
                                            if(is_writable($camaudio)) {
                                            }
                                            else {
                                              chmod("$camaudio", 0777);
                                            }
                                        }
                                        else {
                                            mkdir($camaudio, 0777);
                                        }
                                    }
                                    $insert = "INSERT INTO param_moni (idindice, filtro_dataimp, filtro_ultmoni, filtro_datafimimp, filtro_dultimp, filtro_limmulti, ordemctt,tipomoni,
                                    checkfg,tipoimp, semdados, tpaudio, tpcompress, tparquivo, tiposervidor,endereco,loginftp,senhaftp, camaudio, ccampos, cpartes, conf, hpa, hmonitor)
                                    VAlUES ('$indice', '$fdataimp', '$fdataultmoni', '$fdatafim', '$fdultimp', '$flimmulti', '$ordem', '$tipomoni','$checkfg', '$tipoimp', '$semdados', '$tipoaudio', '$tpcompress', 
                                    '$tparquivo',$tiposervidor,$endereco, $loginftp,$senhaftp, '$camaudio','$ccampos', '$cpartes', '$conf', '$hpa', '$hmonitor')";
                                    $einsert = $_SESSION['query']($insert) or die (mysql_error());
                                    $idparam = mysql_insert_id();
                                    if($einsert) {
                                        $msg = "Parametros cadastrado com sucesso!!!";
                                        header("location:admin.php?menu=camposparam&idparam=".$idparam."&msg=".$msg."#msg");
                                    }
                                    else {
                                        $msg = "Ocorreu um erro no processo de cadastramento, favor contatar o administrador";
                                        header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
  $idparam = $_POST['idparam'];
  header("location:admin.php?menu=parammoni&idparam=".$idparam."#alt");
}

if(isset($_POST['novo'])) {
  $idparam = $_POST['idparam'];
  header("location:admin.php?menu=parammoni");
}

if(isset($_POST['alteraparam'])) {
    $arraypri = array();
    $existe = 0;
    $ip = $_POST['endereco'];
    $porta = explode(":",$_POST['endereco']);
    $login = $_POST['loginftp'];
    $senha = $_POST['senhaftp'];
    //$ftp = ftp_connect($_POST['endereco']);
    //$ftplogin = ftp_login($ftp, $login, $senha);
    $indice = $_POST['indice'];
    $idparam = $_POST['idparam'];
    $dataimp = $_POST['dataimp'];
    $pridataimp = $_POST['pridataimp'];
    if(in_array($pridataimp, $arraypri) && $pridataimp != 0) {
        $existe++;
    }
    $arraypri[] = $pridataimp;
    $fdataimp = trim($dataimp.",".$pridataimp);
    $dataultmoni = $_POST['ultmoni'];
    $pridataulmoni = $_POST['priultmoni'];
    if(in_array($pridataulmoni, $arraypri) && $pridataulmoni != 0) {
        $existe++;
    }
    $arraypri[] = $pridataulmoni;
    $fdataultmoni = trim($dataultmoni.",".$pridataulmoni);
    $datafim = $_POST['datafim'];
    $pridatafim = $_POST['pridatafim'];
    if(in_array($pridatafim, $arraypri) && $pridatafim != 0) {
        $existe++;
    }
    $arraypri[] = $pridatafim;
    $fdatafim = trim($datafim.",".$pridatafim);
    if($_POST['dultimp'] == "") {
        $dultimp = 0;
    }
    else {
        $dultimp = $_POST['dultimp'];
    }
    $pridultimp = $_POST['pridultimp'];
    if(in_array($pridultimp, $arraypri) && $pridultimp != 0) {
        $existe++;
    }
    $arraypri[] = $pridultimp;
    $fdultimp = trim($dultimp.",".$pridultimp);
    $datalimmulti = $_POST['limmulti'];
    $prilimmulti = $_POST['prilimmulti'];
    if(in_array($prilimmulti, $arraypri) && $prilimmulti != 0) {
        $existe++;
    }
    $arraypri[] = $prilimmulti;
    $flimmulti = trim($datalimmulti.",".$prilimmulti);
    $tpordem = $_POST['tpordem'];
    $atvordem = $_POST['atvordem'];
    $ordem = trim($atvordem.",".$tpordem);
    $tipomoni = $_POST['tipomoni'];
    $checkfg = $_POST['checkfg'];
    $tipoimp = $_POST['tipoimp'];
    $tipoaudio = implode(",",$_POST['tipoaudio']);
    $semdados = $_POST['semdados'];
    $tparquivo = implode(",",$_POST['tparquivo']);
    $tpcompress = implode(",",$_POST['tpcompress']);
    $tiposervidor = val_vazio($_POST['tiposervidor']);
    if($_POST['tiposervidor'] == "LOCAL") {
        $endereco = "NULL";
        $loginftp = "NULL";
        $senhaftp = "NULL";
    }
    else {
        $endereco = val_vazio($_POST['endereco']);
        $loginftp = val_vazio($_POST['loginftp']);
        $senhaftp = val_vazio(base64_encode($_POST['senhaftp']));
    }
    $camaudio = $_POST['camaudio'];
    $ccampos = $_POST['ccampos'];
    $cpartes = $_POST['cpartes'];
    $conf = $_POST['conf'];
    $hpa = $_POST['hpa'];
    $hmonitor = $_POST['hmonitor'];
    $caractertime = "[)(*&%$#@!?/\.,;-|^~]";
    $campos = implode(",", $_POST['campos']);
    $caractercamaudio = "[]$[><}:;,!?*%&#@]";
    $caractersepara = "^[a-z,A-Z,0-9]";
    $selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
    $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam))  or die (mysql_error());
    if($tipomoni == "" || $tipoimp == "" || $camaudio == "") {
        $msgi = "O preenchimento dos campos com asterisco são obrigatórios";
        header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
    }
    else {
        if(!$ftp && $_POST['tiposervidor'] == "FTP") {
            $msg = "Erro na conexão ao IP do FTP";
            header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msg."#alt");
        }
        else {
            if(!$ftplogin && $_POST['tiposervidor'] == "FTP") {
                $msg = "Erro no login com o FTP";
                header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msg."#alt");
            }
            else {
                if($existe != 0) {
                    $msgi = "Existem dois filtros do parametro com a mesma prioridade, favor verificar";
                    header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                }
                else {
                    if($tipomoni == "G") {
                        if($tipoimp == "A") {
                            if($ccampos == "" || $cpartes == "" || $tipoaudio == "" || $tpcompress == "") {
                              $msgi = "Os campos ''Separa Campos'' e ''Separa Partes'' é obrigatório!!";
                              header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                            }
                            else {
                                if(eregi($caractersepara, $ccampos) || (eregi($caractersepara, $cpartes))) {
                                  $msgi = "Os campos ''Separa Campos ou Partes'' não podem conter os caracteres ''$caractersepara''";
                                  header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                }
                                else {
                                    if($indice == $eselparam['idindice'] && $tipomoni == $eselparam['tipomoni'] && $checkfg == $eselparam['checkfg'] && $tipoimp == $eselparam['tipoimp'] &&
                                        $fdataimp == $eselparam['filtro_dataimp'] && $fdatafim == $eselparam['filtro_datafimimp'] && $fdataultmoni == $eselparam['filtro_ultmoni'] &&
                                        $fdultimp == $eselparam['filtro_dultimp'] && $flimmulti == $eselparam['filtro_limmulti'] && $ordem == $eselparam['ordemctt'] &&
                                        $tipoaudio == $eselparam['tpaudio'] && $semdados == $eselparam['semdados'] && $tpcompress == $eselparam['tpcompress'] && $camaudio == $eselparam['camaudio'] &&
                                        $ccampos == $eselparam['ccampos'] && $cpartes == $eselparam['cpartes'] && $conf == $eselparam['conf'] && $hpa == $eselparam['hpa'] &&
                                        $hmonitor == $eselparam['hmonitor'] && $_POST['tiposervidor'] == $eselparam['tiposervidor'] && $_POST['endereco'] == $eselparam['endereco'] && $_POST['loginftp'] == $eselparam['loginftp'] && $_POST['senhaftp'] == $eselparam['senhaftp']) {
                                        $msgi = "Nenhum campo foi alterado, favor verificar";
                                        header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                    }
                                    else {
                                        $pass = "OK";
                                    }
                                }
                            }
                        }
                        if($tipoimp == "LA") {
                            if($camaudio == "" || $tparquivo == "" || $tpcompress == "" || $tipoaudio == "") {
                              $msgi = "O campo ''Caminho, Tipo Arquivo e Tipo Compressão'' não podem estar vazios";
                              header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                            }
                            else {
                                if(eregi($caractercamaudio, $camaudio)) {
                                  $msgi = "O campo ''Caminho'' não pode conter os caracteres ''$caractercamaudio''";
                                  header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                }
                                else {
                                    if($indice == $eselparam['idindice'] && $tipomoni == $eselparam['tipomoni'] && $checkfg == $eselparam['checkfg'] && $tipoimp == $eselparam['tipoimp'] &&
                                            $fdataimp == $eselparam['filtro_dataimp'] && $fdatafim == $eselparam['filtro_datafimimp'] && $fdataultmoni == $eselparam['filtro_ultmoni'] &&
                                        $fdultimp == $eselparam['filtro_dultimp'] && $semdados == $eselparam['semdados'] && $flimmulti == $eselparam['filtro_limmulti'] && $ordem == $eselparam['ordemctt'] && $tipoaudio == $eselparam['tpaudio'] &&
                                            $tparquivo == $eselparam['tparquivo'] && $tpcompress == $eselparam['tpcompress'] && $camaudio == $eselparam['camaudio'] &&
                                            $conf == $eselparam['conf'] && $hpa == $eselparam['hpa'] && $hmonitor == $eselparam['hmonitor'] && $_POST['tiposervidor'] == $eselparam['tiposervidor']) {
                                      $msgi = "Nenhum campo foi alterado, favor verificar";
                                      header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                    }
                                    else {
                                        $pass = "OK";
                                    }
                                }
                            }
                        }
                        if($tipoimp == "I") {
                            if($tipoaudio == "" || $camaudio == "") {
                                $msg = "O campo Tipo Audio é obrigatório!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                if(eregi($caractertime, $hpa) || eregi($caractertime, $hmonitor)) {
                                    $msg = "O/s campo/s ''Horas PA ou Horas Monitor'' não pode conter os caracteres ''$caractercamaudio''";
                                    header("location:admin.php?menu=parammoni&msgi=".$msg."#alt");
                                }
                                else {
                                    if($indice == $eselparam['idindice'] && $tipomoni == $eselparam['tipomoni'] && $checkfg == $eselparam['checkfg'] && $tipoimp == $eselparam['tipoimp'] &&
                                            $fdataimp == $eselparam['filtro_dataimp'] && $fdatafim == $eselparam['filtro_datafimimp'] && $fdataultmoni == $eselparam['filtro_ultmoni'] &&
                                          $fdultimp == $eselparam['filtro_dultimp'] && $semdados == $eselparam['semdados'] && $flimmulti == $eselparam['filtro_limmulti'] && $ordem == $eselparam['ordemctt'] && $tipoaudio == $eselparam['tpaudio'] &&
                                            $camaudio == $eselparam['camaudio'] && $conf == $eselparam['conf'] && $hpa == $eselparam['hpa'] &&
                                            $tparquivo == $eselparam['tparquivo'] && $hmonitor == $eselparam['hmonitor'] && $_POST['tiposervidor'] == $eselparam['tiposervidor']) {
                                      $msgi = "Nenhum campo foi alterado, favor verificar";
                                      header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                    }
                                    else {
                                        $pass = "OK";
                                    }
                                }
                            }
                        }
                    }
                    if($tipomoni == "O") {
                        if($tipoimp == "L") {
                            if($tipoaudio == "" || $tparquivo == "" || $camaudio == "") {
                                $msg = "O campo Tipo Audio é obrigatório!!";
                                header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                if($indice ==  $eselparam['idindice'] && $tipomoni == $eselparam['tipomoni'] && $checkfg == $eselparam['checkfg'] && $tipoimp == $eselparam['tipoimp'] && 
                                        $fdataimp == $eselparam['filtro_dataimp'] && $fdatafim == $eselparam['filtro_datafimimp'] && $fdataultmoni == $eselparam['filtro_ultmoni'] &&
                                      $fdultimp == $eselparam['filtro_dultimp'] && $semdados == $eselparam['semdados'] && $flimmulti == $eselparam['filtro_limmulti'] && $ordem == $eselparam['ordemctt'] && $tipoaudio == $eselparam['tpaudio'] &&
                                       $tparquivo == $eselparam['tparquivo'] && $camaudio == $eselparam['camaudio'] && $conf == $eselparam['conf'] && $hpa == $eselparam['hpa'] && 
                                        $hmonitor == $eselparam['hmonitor'] && $_POST['tiposervidor'] == $eselparam['tiposervidor']) {
                                    $msgi = "Nenhum campo foi alterado, favor verificar";
                                    header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                }
                                else {
                                    $pass = "OK";
                                }
                            }
                        }
                        if($tipoimp == "SL") {
                            if($tipoaudio == "" || $camaudio == "") {
                                  $msg = "O campo Tipo Audio é obrigatório!!";
                                  header("location:admin.php?menu=parammoni&msg=".$msg."#msg");
                            }
                            else {
                                if($indice == $eselparam['idindice'] && $tipomoni == $eselparam['tipomoni'] && $checkfg == $eselparam['checkfg'] && $tipoimp == $eselparam['tipoimp'] && 
                                    $fdataimp == $eselparam['filtro_dataimp'] && $fdatafim == $eselparam['filtro_datafimimp'] && $fdataultmoni == $eselparam['filtro_ultmoni'] &&
                                    $fdultimp == $eselparam['filtro_dultimp'] && $semdados == $eselparam['semdados'] && $flimmulti == $eselparam['filtro_limmulti'] && $ordem == $eselparam['ordemctt'] && $tipoaudio == $eselparam['tpaudio'] &&
                                    $camaudio == $eselparam['camaudio'] && $conf == $eselparam['conf'] && $hpa == $eselparam['hpa'] && $hmonitor == $eselparam['hmonitor'] && $_POST['tiposervidor'] == $eselparam['tiposervidor']) {
                                  $msgi = "Nenhum campo foi alterado, favor verificar";
                                  header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                }
                                else {
                                    $pass = "OK";
                                }
                            }
                        }
                    }
                    if($pass == "OK") {
                        if(eregi($caractercamaudio, $camaudio)) {
                          $msgi = "O campo ''Caminho'' não pode conter os caracteres ''$caractercamaudio''";
                          header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                        }
                        else {
                            if(eregi($caractertime, $hpa) || eregi($caractertime, $hmonitor)) {
                              $msg = "O/s campo/s ''Horas PA ou Horas Monitor'' não pode conter os caracteres ''$caractercamaudio''";
                              header("location:admin.php?menu=parammoni&msg=".$msg."#msg"."#alt");
                            }
                            else {
                                if($camaudio == "") {
                                  $msgi = "O campo ''Caminho e Tipo Compressão'' não podem estar vazios";
                                  header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                }
                                else {
                                    if(!is_dir($camaudio) && $_POST['tiposervidor'] == "LOCAL") {
                                      $msgi = "O caminho para salvar os audios não é um diretório!!!";
                                      header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                    }    
                                    else {
                                        $altera = "UPDATE param_moni SET idindice='$indice', tipomoni='$tipomoni',checkfg='$checkfg', tipoimp='$tipoimp', semdados='$semdados',
                                                  tpaudio='$tipoaudio', tpcompress='$tpcompress',filtro_dataimp='$fdataimp', filtro_ultmoni='$fdataultmoni', 
                                                  filtro_datafimimp='$fdatafim', filtro_dultimp='$fdultimp', filtro_limmulti='$flimmulti',ordemctt='$ordem', tparquivo='$tparquivo', 
                                                  tiposervidor=$tiposervidor,endereco=$endereco, loginftp=$loginftp,senhaftp=$senhaftp, camaudio='$camaudio',
                                                  ccampos='$ccampos', cpartes='$cpartes', conf='$conf', hpa='$hpa', hmonitor='$hmonitor' 
                                                  WHERE idparam_moni='$idparam'";
                                        $ealtera = $_SESSION['query']($altera) or die (mysql_error());
                                        if($ealtera) {
                                            $msgi = "Parametro alterado com sucesso!!!";
                                            header("location:admin.php?menu=camposparam&idparam=".$idparam."&msgi=".$msgi);
                                        }
                                        else {
                                            $msgi = "Ocorreu um erro no processo de alteração, favor contatar o administrador";
                                            header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#alt");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['volta'])) {
    header("location:admin.php?menu=parammoni");
}

if(isset($_POST['apaga'])) {
    $idparam = $_POST['idparam'];
    $selconfig = "SELECT COUNT(*) as result FROM conf_rel WHERE idparam_moni='$idparam'";
    $econfig = $_SESSION['fetch_array']($_SESSION['query']($selconfig)) or die (mysql_error());
    $selcampos = "SELECT COUNT(*) as result FROM camposparam WHERE idparam_moni='$idparam'";
    $eselcampos = $_SESSION['fetch_array']($_SESSION['query']($selcampos)) or die (mysql_error());
    if($econfig['result'] >= 1 OR $eselcampos['result'] >= 1) {
      $msgi = "Este parametro jÃ¡ estÃ¡ relacionado com uma configuraÃ§Ã£o do relacionamento de filtros";
      header("location:admin.php?menu=parammoni&idparam=".$idparam."&msgi=".$msgi."#msgi");
    }
    else {
      $selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
      $eparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die ("erro na consulta do parametro");
      $arquivos = scandir($eparam['camaudio']);
      if(count($arquivos) > 2) {
      }
      if(count($arquvios) <= 2) {
	rmdir($eparam['camaudio']);
      }	
      $del = "DELETE FROM param_moni WHERE idparam_moni='$idparam'";
      $edel = $_SESSION['query']($del) or die (mysql_error());
      $alt = "ALTER TABLE param_moni AUTO_INCREMENT=1";
      $ealt = $_SESSION['query']($alt) or die (mysql_error());
      if($ealt) {
        $msgi = "Parametro apagado com sucesso!!!";
        header("location:admin.php?menu=parammoni&msgi=".$msgi."#msgi");
      }
      else {
        $msgi = "Ocorreu um erro no processo, favor contatar o Administrador!!!";
        header("location:admin.php?menu=parammoni&msgi=".$msgi."#msgi");
      }
    }
}

if(isset($_POST['altrelcamp'])) {
  $idparam = $_POST['idparam'];
  header("location:admin.php?menu=camposparam&idparam=".$idparam);
}

if(isset($_POST['cadcampos'])) {
    $incorp = $_POST['incorp'];
    $caracter = "[]$[><}{)(:;,!?*%&#@]";
    $coluna = str_replace(' ','_',strtolower(trim($_POST['coluna'])));
    $type = $_POST['type'];
    $tamanho = $_POST['tamanho'];
    $unico = $_POST['unico'];
    $export = $_POST['export'];
    $grava = $_POST['ngravacao'];
    $cadcaracter = $_POST['caracter'];
    $restricao = $_POST['restricao'];
    $selcolunas = "SELECT COUNT(*) as result FROM coluna_oper WHERE incorpora='$incorp' AND nomecoluna='$coluna'";
    $ecolunas = $_SESSION['fetch_array']($_SESSION['query']($selcolunas));
    if($ecolunas['reuslt'] >= 1) {
        $msgo = "Esta coluna já existe na tabela do ''OPERADOR'', favor verificar!!!";
        header("location:admin.php?menu=parammoni&msgo=".$msgo);
    }
    else {
        if($coluna == "" || $type == "") {
            $msgo = "Os campos ''Dado de Entrada'' e ''Type'' são obrigatórios!!!";
            header("location:admin.php?menu=parammoni&msgo=".$msgo);
        }
        else {
            if(eregi($caracter, $coluna) || (eregi($caracter, $tamanho))) {
                $msgo = "No campo ''coluna'' e ''tamanho'' não é aceito nenhum caracter especial ''$caracter''!!!";
                header("location:admin.php?menu=parammoni&msgo=".$msgo);
            }
            else {
                if($type == "VARCHAR" OR $type == "INT") {
                    if($tamanho == "") {
                        $msgo = "Para ''TIPO DO CAMPO'', ''TEXTO ou NUMERO'' é obrigatório o preenchimento do ''tamanho'' do campo";
                        header("location:admin.php?menu=parammoni&msgo=".$msgo);
                    }
                    else {
                        if(!is_numeric($tamanho)) {
                            $msgo = "O campo ''tamanho'' só pode conter números";
                            header("location:admin.php?menu=parammoni&msgo=".$msgo);
                        }
                        else {
                            if($type == "INT") {
                                $default = "DEFAULT NULL";
                            }
                            if($type == "VARCHAR") {
                                $default = "DEFAULT ''";
                            }
                            $insert = "INSERT INTO coluna_oper (incorpora, nomecoluna, unico, tipo, tamanho, caracter,restricao,export,ngravacao) VALUE ('$incorp', '$coluna', '$unico', '$type', '$tamanho', '$cadcaracter','$restricao','$export','$grava');";
                            $einsert = $_SESSION['query']($insert) or die (mysql_error());
                            if($incorp == "SUPER_OPER") {
                                $alter = "ALTER TABLE super_oper ADD $coluna $type( $tamanho ) $default";
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                            }
                            if($incorp == "OPERADOR") {
                                $alter = "ALTER TABLE operador ADD $coluna $type( $tamanho ) $default";
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                            }
                            if($incorp == "DADOS") {
                                $alter = "ALTER TABLE fila_oper ADD $coluna $type( $tamanho ) $default";
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                $alter = "ALTER TABLE fila_grava ADD $coluna $type( $tamanho ) $default";
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                            }
                            /*if($type == 'INT') {
                                $alter = "ALTER TABLE selecao ADD $coluna $type( $tamanho ) $default AFTER `idauditor`";
                            }
                            else {
                                $alter = "ALTER TABLE selecao ADD $coluna $type( $tamanho ) NOT NULL $default AFTER `idauditor`";
                            }
                            $ealter = $_SESSION['query']($alter) or die (mysql_error());*/
                        }
                    }
                }
                if($type == "TIME" OR $type == "DATE" OR $type == "TEXT") {
                    if($type == "DATE") {
                        $default = "DEFAULT '0000-00-00'";
                    }
                    if($type == "TIME") {
                        $default = "DEFAULT '00:00:00'";
                    }
                    if($type == "TEXT") {
                        $default = "DEFAULT NULL";
                    }
                    $insert = "INSERT INTO coluna_oper (incorpora, nomecoluna, unico, tipo, tamanho, caracter,restricao,export,ngravacao) VALUE ('$incorp', '$coluna', '$unico', '$type', '$tamanho', '$cadcaracter','$restricao','$export','$grava');";
                    $einsert = $_SESSION['query']($insert) or die (mysql_error());
                    if($incorp == "SUPER_OPER") {
                        $alter = "ALTER TABLE super_oper ADD $coluna $type $default";
                        $ealter = $_SESSION['query']($alter) or die (mysql_error());
                    }
                    if($incorp == "OPERADOR") {
                        $alter = "ALTER TABLE operador ADD $coluna $type $default";
                        $ealter = $_SESSION['query']($alter) or die (mysql_error());
                    }
                    if($incorp == "DADOS") {
                        $alter = "ALTER TABLE fila_oper ADD $coluna $type $default";
                        $ealter = $_SESSION['query']($alter) or die (mysql_error());
                        $alter = "ALTER TABLE fila_grava ADD $coluna $type $default";
                        $ealter = $_SESSION['query']($alter) or die (mysql_error());
                    }
                    //$alter = "ALTER TABLE selecao ADD $coluna $type $default AFTER `idauditor`";
                    //$ealter = $_SESSION['query']($alter) or die (mysql_error());
                    $log = new log();
                    if($log->conectabancolog()) {
                        $log->exelog("insert", "coluna_oper", "incorpora, nomecoluna, unico, tipo, tamanho, caracter,restricao,export,ngravacao","$incorp#$coluna#$unico#$type#$tamanho#$cadcaracter#$restricao#$export#$grava", $_SESSION['selbanco'], $_SESSION['usuarioID'], $_SESSION['user_tabela']);
                    }
                    else {
                        $log->server();
                        mysql_select_db($_SESSION['selbanco']);
                    }
                }
                if($ealter) {
                    $msgo = "Parametro cadastrado com sucesso!!!";
                    header("location:admin.php?menu=parammoni&msgo=".$msgo);
                }
                else {
                    $msgo = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                    header("location:admin.php?menu=parammoni&msgo=".$msgo);
                }
            }
        }
    }
}

if(isset($_POST['alteracampo'])) {
    $id = $_POST['id'];
    $caracter = "[]$[><}{)(:;,!?*%&#@]";
    $nome = str_replace(' ','_',strtolower(trim($_POST['coluna'])));
    $tipo = $_POST['tipo'];
    $tamanho = $_POST['tamanho'];
    $cadcaracter = $_POST['caracter'];
    $restricao = $_POST['restricao'];
    $export = $_POST['export'];
    $grava = $_POST['ngravacao'];
    $selid = "SELECT * FROM coluna_oper WHERE idcoluna_oper='$id'";
    $eid = $_SESSION['fetch_array']($_SESSION['query']($selid)) or die (mysql_error());
    $incorp = $eid['incorpora'];
    $selcolunas = "SELECT COUNT(*) as result FROM coluna_oper WHERE nomecoluna='$nome' AND idcoluna_oper <> '$id'";
    $ecolunas = $_SESSION['fetch_array']($_SESSION['query']($selcolunas)) or die (mysql_error());
    if($nome == "") {
      $msgoalt = "O campos ''Coluna'' não pode estar vazio";
      header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
    }
    else {
        if(eregi($caracter, $nome)) {
          $msgoalt = "O campo ''Coluna'' não pode conter os caracteres ''$caracter''";
          header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
        }
        else {
            if($ecolunas['result'] >= 1) {
              $msgoalt = "O nome informado jÃ¡ estÃ¡ cadastrado na tabela!!!";
              header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
            }
            else {
                if($nome == $eid['nomecoluna'] && $tamanho == $eid['tamanho'] && $cadcaracter == $eid['caracter'] && $restricao == $eid['restricao'] && $grava == $eid['ngravacao'] && $export == $eid['export']) {
                  $msgoalt = "Nenhum campo foi alterado, processo nÃ£o executado!!!";
                  header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
                }
                else {
                    if($eid['tipo'] == "VARCHAR" AND $tamanho == "") {
                        $msgoalt = "Quando o tipo do campo for ''VARCHAR'' o campo ''tamanho'' nÃ£o pode estar vazio!!!";
                        header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
                    }
                    else {
                        if($eid['tipo'] == "INT" AND $tamanho == "") {
                            $msgoalt = "Quando o tipo do campo for ''INT'' o campo ''tamanho'' não pode estar vazio!!!";
                            header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
                        }
                        else {
                            if($eid['tipo'] == "TIME" OR $eid['tipo'] == "DATE" OR $eid['tipo'] == "TEXT") {
                                $sqlup = "nomecoluna='$nome', caracter='$cadcaracter', restricao='$restricao',export='$export',ngravacao='$grava'";
                                if($type == "DATE") {
                                    $default = "DEFAULT '0000-00-00'";
                                }
                                if($type == "TIME") {
                                    $default = "DEFAULT '00:00:00'";
                                }
                                if($type == "TEXT") {
                                    $default = "DEFAULT NULL";
                                }
                            }
                            else {
                                $sqlup = "nomecoluna='$nome', tamanho='$tamanho', caracter='$cadcaracter', restricao='$restricao',export='$export',ngravacao='$grava'";
                                if($eid['tipo'] == "INT") {
                                    $default = "DEFAULT NULL";
                                }
                                else {
                                    $default = "DEFAULT ''";
                                }
                            }
                            $update= "UPDATE coluna_oper SET $sqlup WHERE idcoluna_oper='$id'";
                            $eupdate = $_SESSION['query']($update) or die (mysql_error());
                            if($incorp == "SUPER_OPER" && ($nome != $eid['nomecoluna'] OR $tamanho != $eid['tamanho'])) {
                                if($eid['tipo'] == "TIME" OR $eid['tipo'] == "DATE") {
                                      $alter = "ALTER TABLE `super_oper` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']." $default";
                                }
                                else {
                                      $alter = "ALTER TABLE `super_oper` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']."( $tamanho ) DEFAULT NULL";
                                }
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                            }
                            if($incorp == "OPERADOR" && ($nome != $eid['nomecoluna'] OR $tamanho != $eid['tamanho'])) {
                                if($eid['tipo'] == "TIME" OR $eid['tipo'] == "DATE") {
                                      $alter = "ALTER TABLE `operador` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']." $default";
                                }
                                else {
                                      $alter = "ALTER TABLE `operador` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']."( $tamanho ) DEFAULT NULL";
                                }
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                            }
                            if($incorp == "DADOS" && ($nome != $eid['nomecoluna'] OR $tamanho != $eid['tamanho'])) {
                                if($eid['tipo'] == "TIME" OR $eid['tipo'] == "DATE" OR $eid['tipo'] == "TEXT") {
                                    $alter = "ALTER TABLE `fila_oper` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']." $default";
                                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                    $alter = "ALTER TABLE `fila_grava` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']." $default";
                                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                    $alter = "ALTER TABLE `selecao` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']." $default";
                                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                }
                                else {
                                    $alter = "ALTER TABLE `fila_oper` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']."( $tamanho ) $default";
                                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                    $alter = "ALTER TABLE `fila_grava` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']."( $tamanho ) $default";
                                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                    $alter = "ALTER TABLE `selecao` CHANGE `".$eid['nomecoluna']."` `$nome` ".$eid['tipo']."( $tamanho ) $default";
                                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                }
                            }
                            else {
                            }
                            if($eupdate) {
                                $msgoalt = "Alteração realizada com sucesso!!!";
                                header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
                            }
                            else {
                                $msgoalt = "Ocorreu um erro no processo de alteraçã, favor contatar o adminstrador!!!";
                                header("location:admin.php?menu=parammoni&msgoalt=".$msgoalt);
                            }
                        }
                    }
                }
            }
        }
    }
}
?>