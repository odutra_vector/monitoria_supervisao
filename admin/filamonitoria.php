<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
            $('#trfiltro').hide();
            $('#trmonitor').hide();
            $('#trvisualiza').hide();
            $('#selfila').hide();
            $('#idfiltro').change(function() {
                var idperiodo = $('#idperiodo').val();
                var idfiltro = $(this).val();
                if(idperiodo == "") {
                    alert('Favor selecionar o PERÍODO antes do FILTRO');
                }
                else {
                    var nome = $('#idfiltro').find('option').filter(':selected').text();
                    $('#trfiltro').show();
                    $('#tdfiltro').html('<strong>'+nome+'</strong>');
                    $('#relfiltro').load('/monitoria_supervisao/admin/carregafiltrofila.php',{idperiodo: idperiodo, idfiltro: idfiltro,nome:nome,tipo:'carregafiltro'});
                }
            })
            $('#tipo').change(function() {
                var tipo = $(this).val();
                if(tipo == "CONSOLIDADO - RELACIONAMENTO") {
                    $('#selfila').hide();
                    $('#trvisualiza').hide();
                    $("#trmonitor").hide();
                    $('#idperiodo').val('value','');
                }
                if(tipo == "CONSOLIDADO - OPERADOR") {
                    $('#trvisualiza').hide();
                    $("#trmonitor").hide();
                    $('#selfila').show();
                    $('#idperiodo').val('value','');
                }
                if(tipo == "VISUALIZAR FILA TEMPO REAL") {
                    $('#visualiza').val('value','');
                    $('#trvisualiza').show();
                    $('#selfila').hide();
                    $('#idperiodo').val('value','');
                }
            })
            $("#visualiza").change(function() {
                var visualiza = $(this).val();
                if(visualiza == "monitor") {
                    $("#trmonitor").show();
                    $('#selfila').hide();
                }
                if(visualiza == "relacionamento") {
                    $("#trmonitor").hide();
                    $('#selfila').show();
                }
            })
            $('#relfiltro').live('change',function() {
                var idperiodo = $('#idperiodo').val();
                var iddados = $(this).val();
                var nome = $('#idfiltro').find('option').filter(':selected').text();
                $('#idrelfiltro').load('/monitoria_supervisao/admin/carregafiltrofila.php',{idperiodo:idperiodo,nome:nome,iddados:iddados,tipo:'carregarelfiltro'});
            })
            $('#idrelfiltro').live('change',function() {
                var tipo = $('#tipo').val();
                var periodo = $('#idperiodo').val();
                var visualiza = $('#visualiza').val();
                var idrelfiltro = $(this).val();
                $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
                if(tipo == "CONSOLIDADO - OPERADOR") {
                    $('#fila').load('/monitoria_supervisao/admin/carregafiltrofila.php',{idperiodo:periodo,idrelfiltro:idrelfiltro,tipo:'filaoper'});
                }
                if(tipo == "VISUALIZAR FILA TEMPO REAL") {
                    $('#fila').load('/monitoria_supervisao/admin/carregafiltrofila.php',{visualiza:visualiza,idperiodo:periodo,idrelfiltro:idrelfiltro,tipo:'filatempo'});
                }
            })
            $('#idperiodo').live('change',function() {
                var periodo = $(this).val();
                var tipo = $('#tipo').val();
                if(tipo == "CONSOLIDADO - RELACIONAMENTO") {
                    $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }})
                    $('#fila').load('/monitoria_supervisao/admin/carregafiltrofila.php',{idperiodo:periodo,tipo:'filarel'});
                }
            })
            $('#monitor').change(function() {
                var tipo = $('#tipo').val();
                var periodo = $('#idperiodo').val();
                var visualiza = $('#visualiza').val();
                var monitor = $(this).val();
                $.blockUI({ message: '<strong>AGUARDE CARREGANDO...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
                $('#fila').load('/monitoria_supervisao/admin/carregafiltrofila.php',{visualiza:visualiza,idmonitor:monitor,idperiodo:periodo,tipo:'filatempo'});
            })
	})
</script>
</head>
<body>
	<div id="conteudo">
    	<div id="divtipo" style="width:1024px">
            <table width="420">
              <tr>
                <td class="corfd_ntab" align="center" colspan="2"><strong>CONSULTA FILA DE MONITORIAS</strong></td>
              </tr>
              <tr>
                <td width="111" class="corfd_coltexto"><strong>TIPO CONSULTA</strong></td>
                <td width="250" class="corfd_colcampos">
                    <select name="tipo" id="tipo">
                    <option value="" selected="selected" disabled="disabled">SELECIONE...</option>
                    <?php
                    $tipos = array('CONSOLIDADO - RELACIONAMENTO','CONSOLIDADO - OPERADOR','VISUALIZAR FILA TEMPO REAL');
                    foreach($tipos as $tipo) {
                        echo "<option value=\"".$tipo."\">$tipo</option>";	
                    }
                    ?>
                </select>
                </td>
              </tr>
              <tr>
              	<td class="corfd_coltexto"><strong>PERÍODO</strong></td>
                <td class="corfd_colcampos">
                    <select name="idperiodo" id="idperiodo">
                        <option value="" selected="selected">SELECIONE...</option>
                        <?php
                        $selper = "SELECT * FROM periodo ORDER BY ano DESc, mes DESC";
                        $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta do período");
                        while($lselper = $_SESSION['fetch_array']($eselper)) {
                                echo "<option value=\"".$lselper['idperiodo']."\">".$lselper['nmes']." - ".$lselper['ano']."</option>";
                        }
                        ?>
                    </select>
                </td>
              </tr>
              <tr id="trvisualiza">
                  <td class="corfd_coltexto"><strong>VISUALIZA</strong></td>
                  <td class="corfd_colcampos">
                      <select name="visualiza" id="visualiza">
                          <option value="">SELECIONE...</option>
                          <option value="relacionamento">RELACIONAMENTO</option>
                          <option value="monitor">MONITOR</option>
                      </select>
                  </td>
              </tr> 
              <tr id="trmonitor">
                  <td class="corfd_coltexto"><strong>MONITOR</strong></td>
                  <td class="corfd_colcampos">
                      <select name="monitor" id="monitor">
                      <option value="">SELECIONE....</option>
                      <?php
                      $monitor = "SELECT * FROM monitor WHERE ativo='S'";
                      $selmoni = $_SESSION['query']($monitor) or die (mysql_error());
                      while($lmoni = $_SESSION['fetch_array']($selmoni)) {
                          echo "<option value=\"".$lmoni['idmonitor']."\">".$lmoni['nomemonitor']."</option>";
                      }
                      ?>
                      </select>
                  </td>
              </tr>
            </table><br/>
      </div>
	  <div id="selfila" style="width:1024px;">
            <table style="width:auto">
              <tr>
                <td class="corfd_ntab" align="center" colspan="2"><strong>SELECIONE O RELACIONAMENTO</strong></td>
              </tr>
              <tr>
              	<td class="corfd_coltexto"><strong>FILTRO</strong></td>
                <td class="corfd_colcampos">
                    <select name="idfiltro" id="idfiltro">
                        <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                	<?php
                        $filtros = filtros();
                        foreach($filtros as $idfiltro => $nomefiltro) {
                            echo "<option value=\"".$idfiltro."\">".strtoupper($nomefiltro)."</option>";
                        }
                        ?>
                    </select>
                </td>
              </tr>
              <tr id="trfiltro">
                <td class="corfd_coltexto" id="tdfiltro"><strong></strong></td>
                <td class="corfd_colcampos" colspan="2">
                    <select name="relfiltro" id="relfiltro" style="width:200px">
                    </select>           
                </td>
              </tr>
                <tr id="tridrelfiltro">
                    <td colspan="2" class="corfd_colcampos">
                        <select name="idrelfiltro" id="idrelfiltro">
                        </select>
                    </td>
                </tr>
            </table>
      </div><br /><hr />
      <div id="fila" style="width:1024px; height: 550px; overflow:auto">
      </div>
    </div>
</body>
</html>
