<?php
session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");

$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
 
$alias = new TabelasSql();
$tabfiltros = array("user_adm" => "useradmfiltro","user_wev" => "userwebfiltro");
$idsrel = arvoreescalar("", $_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
$selrel = "SELECT ".$alias->AliasTab(rel_filtros).".idrel_filtros,idrelemailfg,idrelemailimp FROM rel_filtros ".$alias->AliasTab(rel_filtros)."
            INNER JOIN ".$tabfiltros[$_SESSION['user_tabela']]." ".$alias->AliasTab($tabfiltros[$_SESSION['user_tabela']])." ON ".$alias->AliasTab($tabfiltros[$_SESSION['user_tabela']]).".idrel_filtros = ".$alias->AliasTab(rel_filtros).".idrel_filtros
            LEFT JOIN relemailfg ".$alias->AliasTab(relemailfg)." ON ".$alias->AliasTab(relemailfg).".idrel_filtros = ".$alias->AliasTab($tabfiltros[$_SESSION['user_tabela']]).".idrel_filtros
            LEFT JOIN relemailimp ".$alias->AliasTab(relemailimp)." ON ".$alias->AliasTab(relemailimp).".idrel_filtros = ".$alias->AliasTab($tabfiltros[$_SESSION['user_tabela']]).".idrel_filtros
            WHERE id".$_SESSION['user_tabela']."='".$_SESSION['usuarioID']."'";
$eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relacionamentos vinculados ao usuário");
while($lselrel = $_SESSION['fetch_array']($eselrel)) {
    $selemailfluxo = "SELECT count(*) as result FROM relemailfluxo WHERE idrel_filtros='".$lselrel['idrel_filtros']."'";
    $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selemailfluxo)) or die(mysql_error());
    $selemailfg = "SELECT count(*) as result FROM relemailfg WHERE idrel_filtros='".$lselrel['idrel_filtros']."'";
    $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selemailfg)) or die(mysql_error());
    if($eselfluxo['result'] >= 1 || $eselfg['result'] >= 1) {
        $idsuser[] = $lselrel['idrel_filtros'];
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>RELPLICA RELACIONAMENTO E-MAIL</title>
<link href="/monitoria_supervisao/style.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.6.2.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#replicaemail").click(function() {
            var ref = $("#referencia").val();
            var cad = $("#referenciado").val();
            if(ref == "" || cad == "") {
                $("#spanmsg").html("Favor preencher os campos REFERÊNCIA E CADASTRO");
                return false;
            }
            else {
                 $.blockUI({ message: '<strong>AGUARDE EXECUTANDO A REPLICA...</strong>', css: { 
                 border: 'none', 
                 padding: '15px', 
                 backgroundColor: '#000', 
                 '-webkit-border-radius': '10px', 
                 '-moz-border-radius': '10px', 
                 opacity: .5,
                 color: '#fff'
                 }})
            }
        })
    })
</script>
</head>
<body>
        <div style="width:900px; margin:auto; background-color:<?php echo $cor->corfd_pag;?>; text-align: center">
        <form action="cadrelenvmail.php" method="post">
            <table width="900" align="center" style=" font-size: 12px; font-family: sans-serif">
              <tr>
                <th scope="col" align="center" class="corfd_ntab" colspan="2"><strong>REPLICAR CONFIGURAÇÃO DE E-MAIL</strong></th>
              </tr>
              <tr>
                <td width="200" align="center" class="corfd_coltexto"><strong>RELACIONAMENTO REFERÊNCIA</strong></td>
                <td width="900" align="center" class="corfd_coltexto"><strong>RELACIONAMENTO CADASTRO</strong></td>
              </tr>
              <tr>
                <td class="corfd_colcampos" align="center">
                    <select name="referencia" id="referencia" style="width: 200px">
                    <option selected="selected" disabled="disabled" value="">Selecione...</option>
                    <?php
                    foreach($idsrel as $idr) {
                        if(in_array($idr, $idsuser)) {
                            echo "<option value=\"".$idr."\" title=\"".nomeapres($idr)."\">".nomeapres($idr)."</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
                <td class="corfd_colcampos" align="center">
                    <select name="referenciado[]" id="referenciado" style="width: 700px; height: 200px" multiple="multiple">
                    <option disabled="disabled" selected="selected" value="">Selecione...</option>
                    <?php
                        foreach($idsrel as $idrel) {
                            ?>
                            <option value="<?php echo $idrel;?>" title="<?php echo nomeapres($idrel);?>"><?php echo nomeapres($idrel);?></option>
                            <?php
                        }
                    ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)"  name="replicaemail" id="replicaemail" type="submit" value="CADASTRAR/ ALTERAR" /></td>
              </tr>
            </table></br>
            <span id="spanmsg" style="color: #EA071E; font-weight: bold; font-family: sans-serif; font-size: 12px"><?php echo $_GET['msg']; ?></span>
            </form>
        </div>	
</body>
</html>
