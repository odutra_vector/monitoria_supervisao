<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

if($_SESSION['user_tabela'] == "user_adm") {
    $tabuser = "user_adm";
    $tabreluser = "useradmfiltro";
    $atabuser = "a";
    $atabreluser = "af";
}
if($_SESSION['user_tabela'] == "user_web") {
    $tabuser = "user_web";
    $tabreluser = "userwebfiltro";
    $atabuser = "w";
    $atabreluser = "wf";
}

$periodo = periodo(datas);
if($_POST['idrel'] == "") {
    $idsrel = explode(",",$_POST['idsrelperf']);
    $orderlimit = "ORDER BY u.idrel_filtros, u.dataimp DESC, u.horaimp DESC LIMIT 2";
}
else {
    $idsrel = explode(",",$_POST['idrel']);
    $orderlimit = "ORDER BY u.idrel_filtros, u.dataimp DESC, u.horaimp DESC";
}
if($_POST['dataini'] != "" && $_POST['datafim'] != "") {
    $dataini = data2banco($_POST['dataini']);
    $datafim = data2banco($_POST['datafim']);
}
else {
    $dataini = $periodo['dataini'];
    $datafim = $periodo['datafim'];
}

$selcampo = "SELECT MIN(nivel), nomefiltro_nomes FROM filtro_nomes";
$eselcampo = $_SESSION['fetch_array']($_SESSION['query']($selcampo)) or die ("erro na query de consultado do nome do campo");

?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#exclui').live('click',function() {
            var idup = "";
            $('input[id*="idupimp"]').each(function() {
                if($(this).attr("checked")) {
                    if(idup == "") {
                        idup = $(this).val();
                    }
                    else {
                        idup = idup+","+$(this).val();
                    }
                }
            })
            var iduparray = idup.split(",");
            var countidup = iduparray.length;
            if(idup == "") {
                alert("Favor selecinar uma importação para executar a ação");
                return false;
            }
            else {
                if(countidup > 1) {
                    alert("Favor selecinar somente uma importação para executar a ação");
                    return false;
                }
                else {
                }
            }
        });
        
        $('#alteraimp').live('click',function() {
            var idup = "";
            $('input[id*="idupimp"]').each(function() {
                if($(this).attr("checked")) {
                    if(idup == "") {
                        idup = $(this).val();
                    }
                    else {
                        idup = idup+","+$(this).val();
                    }
                }
            })
            var iduparray = idup.split(",");
            var countidup = iduparray.length;
            if(idup == "") {
                alert("Favor selecinar uma importação para executar a ação");
                return false;
            }
            else {
                if(countidup > 1) {
                    alert("Favor selecinar somente uma importação para executar a ação");
                    return false;
                }
                else {
                }
            }
        })
    })
</script>
<table width="1124" border="0" id="tabhist">
  <thead>
    <?php
    if($_SESSION['user_tabela'] != "user_web") {
    ?>
    <th></th>
    <?php
    }
    ?>
    <th width="51" class="corfd_coltexto" align="center"><strong>ID UP.</strong></th>
    <th width="51" class="corfd_coltexto" align="center"><strong>ID REL.</strong></th>
    <th width="560" class="corfd_coltexto" align="center"><strong><?php echo $eselcampo['nomefiltro_nomes']; ?></strong></th>
    <th width="84" class="corfd_coltexto" align="center"><strong>Datas Ctts/Qtde</strong></th>
    <th width="50" class="corfd_coltexto" align="center"><strong>Qtde</strong></th>
    <!--<th width="84" class="corfd_coltexto" align="center"><strong>Data Inicial</strong></th>
    <th width="74" class="corfd_coltexto" align="center"><strong>Data Final</strong></th>-->
    <th width="44" class="corfd_coltexto" align="center"><strong>Reg. Disp</strong></th>
    <th width="44" class="corfd_coltexto" align="center"><strong>Reg. Exe</strong></th>
    <th width="149" class="corfd_coltexto" align="center"><strong>Data/Hora IMP.</strong></th>
    <th width="180" class="corfd_coltexto" align="center"><strong>Usurio Resp.</strong></th>
  </thead>
  <tbody>
<?php
$l = 0;
foreach($idsrel as $idrel) {
    $selimp = "SELECT u.idupload, u.idperiodo, u.idrel_filtros, u.iduser,u.camupload, u.semana, u.tabuser, it.dataini, it.datafim, u.dataimp, u.horaimp, pm.tipomoni, u.tabfila FROM upload u
                       inner join periodo p ON p.idperiodo = u.idperiodo
                       inner join interperiodo it ON it.idperiodo = u.idperiodo
                       inner join conf_rel cr ON cr.idrel_filtros = u.idrel_filtros
                       inner join param_moni pm ON pm.idparam_moni = cr.idparam_moni
                       WHERE u.idrel_filtros='".$idrel."' AND u.dataimp BETWEEN '".$dataini."' AND '".$datafim."' AND u.imp='S' GROUP BY u.idupload $orderlimit";
    $eselimp = $_SESSION['query']($selimp) or die (mysql_error());
    while($lselimp = $_SESSION['fetch_array']($eselimp)) {
        $tt = 0;
        $log = "";
        $scanlog = scandir($lselimp['camupload']);
        foreach($scanlog as $scan) {
            if(strstr($scan, "logimp")) {
                $log = str_replace($_SERVER['DOCUMENT_ROOT'], "", $lselimp['camupload'])."/".$scan;
            }
        }
        $l++;
        if(is_int($l / 2)) {
            $bgcolor = "bgcolor=\"#C5D4EA\"";
        }
        else {
            $bgcolor = "bgcolor=\"#FFFFFF\"";
        }
        $tabuserimp = $lselimp['tabuser'];
        $fila = $lselimp['tabfila'];
        if($lselimp['tabfila'] == "fila_grava") {
            $alias = "fg";
        }
        $fila = $lselimp['tabfila'];
        if($lselimp['tabfila'] == "fila_oper") {
            $alias = "fo";
        }
        $cupload = "select SUM(if(monitorado = '1',1,0)) as prod, SUM(if(monitorado = '0',1,0)) as disp from upload u
                    inner join $fila $alias ON $alias.idupload = u.idupload where u.idupload='".$lselimp['idupload']."'";
        $ecupload = $_SESSION['fetch_array']($_SESSION['query']($cupload)) or die ("erro na query de contagem das monitorias efetuadas no upload");
        if($_SESSION['user_tabela'] == "user_adm") {
            $cprod = $ecupload['prod'];
            $cdisp = $ecupload['disp'];
        }
        $dataimp = banco2data($lselimp['dataimp'])." - ".$lselimp['horaimp'];
        $nomerel = nomeapres($lselimp['idrel_filtros']);
        $nuser = "SELECT nome$tabuserimp FROM $tabuserimp WHERE id$tabuserimp='".$lselimp['iduser']."'";
        $enuser = $_SESSION['fetch_array']($_SESSION['query']($nuser)) or die ("erro na query de consulta do nome do usuÃ¡rio que importou os registros");
        $nomeuser = $enuser['nome'.$tabuserimp];
        ?>

        <tr <?php echo $bgcolor;?>>
            <?php
            if($_SESSION['user_tabela'] != "user_web") {
            ?>
            <td><input type="checkbox" name="idupimp[]" id="idupimp<?php echo $lselimp['idupload'];?>" value="<?php echo $lselimp['idupload'];?>" /></td>
            <?php
            }
            ?>
            <td align="center"><a href="<?php echo $log;?>" target="_blanck" style="text-decoration: none; outline: none; color: #000"><?php echo $lselimp['idupload'];?></a></td>
            <td align="center"><?php echo $lselimp['idrel_filtros'];?></td>
            <td align="center">
                <?php
                if($_SESSION['user_tabela'] == "user_web" OR ($_SESSION['usuarioperfil'] != "01" && $_SESSION['user_tabela'] == "user_adm")) {
                    echo "<input name=\"relimp\" id=\"relimp\" value=\"".$lselimp['idrel_fitlros']."\" type=\"hidden\" /><span title=\"$nomerel\">$nomerel</span>";
                }
                else {
                ?>
                <select type="text" name="relimp<?php echo $lselimp['idupload'];?>"  id="relimp<?php echo $lselimp['idupload'];?>" style="width:560px">
                    <option value="<?php echo $lselimp['idrel_filtros'];?>" selected="selected"><?php echo $nomerel;?></option>
                    <?php
                    $idsrel = arvoreescalar($lselimp['tipomoni']);
                    foreach($idsrel as $idrel) {
                        $selrelimp = "SELECT COUNT(*) as result FROM rel_filtros rf 
                                      INNER JOIN $tabreluser ua ON ua.idrel_filtros = rf.idrel_filtros 
                                      INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                      WHERE id$tabuser='".$_SESSION['usuarioID']."' AND cr.ativo='S' AND ua.idrel_filtros='$idrel'";
                        $eselrelimp = $_SESSION['fetch_array']($_SESSION['query']($selrelimp)) or die ("erro na query de consulta dos relacionamentos cadastrados");
                        if($idrel == $lselimp['idrel_filtros'] OR $eselrelimp['result'] == 0) {
                        }
                        else {
                            echo "<option value=\"".$idrel."\">".nomeapres($idrel)."</option>";
                        }
                    }
                    ?>
                </select>
                <?php
                }
                ?>
            </td>
            <td align="center">
                <?php
                $seldatas = "SELECT datactt, COUNT(*) as result FROM fila_grava WHERE idupload='".$lselimp['idupload']."' GROUP BY datactt";
                $eseldatas = $_SESSION['query']($seldatas) or die ("erro na query para consultas as datas de contato");
                while($lseldatas = $_SESSION['fetch_array']($eseldatas)) {
                    echo banco2data($lseldatas['datactt'])."-".$lseldatas['result']."<br/>";
                    $tt = $tt + $lseldatas['result'];
                }
                ?>                                                    
            </td>
            <td align="center"><?php echo $tt;?></td>
            <!--<td bgcolor="#FFFFFF" align="center"><input style="width: 70px; border: 1px solid #FFF; text-align: center" type="text" name="dtiniimp<?php //echo $lselimp['idupload'];?>" id="dtiniimp<?php //echo $lselimp['idupload'];?>" value="<?php //echo banco2data($lselimp['dataini']);?>" /></td>
            <td bgcolor="#FFFFFF" align="center"><input style="width: 70px; border: 1px solid #FFF; text-align: center" type="text" name="dtfimimp<?php //echo $lselimp['idupload'];?>" id="dtfimimp<?php //echo $lselimp['idupload'];?>" value="<?php //echo banco2data($lselimp['datafim']);?>" /></td>-->
            <td align="center"><?php echo $cdisp;?></td>
            <td align="center"><?php echo $cprod;?></td>
            <td align="center"><?php echo $dataimp;?></td>
            <td align="center"><?php echo $nomeuser;?></td>
        </tr>
    <?php
    }
}
    ?>
    </tbody>
</table>
<script type="text/javascript">
    $.unblockUI();
</script>
