<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.envmail.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$idmoni = $_POST['idmoni'];
$idrel = $_POST['idrel'];
$idoper = $_POST['idoper'];
$idsuper = $_POST['idsuper'];
$checkfg = val_vazio($_POST['checkfg']);

if(isset($_POST['altmoni'])) {
    if($idrel == "") {
        $msg = "O Relacionamento não pode ser nulo!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
    else {
    $selmoni = "SELECT m.idrel_filtros,m.idoperador,m.idsuper_oper, m.checkfg, pm.checkfg as checkfgpm FROM monitoria m
                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros 
                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                WHERE idmonitoria='$idmoni'";
    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da monitoria");
    if($idrel == $eselmoni['idrel_filtros'] && $idoper == $eselmoni['idoperador'] && $idsuper == $eselmoni['idsuper_oper'] && $_POST['checkfg'] == $eselmoni['checkfg']) {
        $msg = "Nenhum dado foi alterado, processo nÃ£o executado!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
    else {
        $upmoni = "UPDATE monitoria SET idoperador='$idoper', idrel_filtros='$idrel',idsuper_oper='$idsuper',checkfg=$checkfg WHERE idmonitoria='$idmoni'";
        $eupmoni = $_SESSION['query']($upmoni) or die ("erro na query de atualizaÃ§Ã£o da montioria");
        if($_POST['checkfg'] == "NCG OK" && $eselmoni['checkfgpm'] == "S") {
            $mail = new ConfigMail();
            $sconfemail = "SELECT idconfemailenv, user_adm, user_web FROM relemailfg rm 
                               WHERE idrel_filtros='".$eselmoni['idrel_filtros']."'";
            $lcon = $_SESSION['query']($sconfemail) or die ("erro na query de consulta dos dados para envio de e-mail");
            $nconf = $_SESSION['num_rows']($lcon);
            if($nconf >= 1) {
                $econfmail = $_SESSION['fetch_array']($lcon);
                $conf = $econfmail['idconfemailenv'];
                $mail->IsSMTP();
                $mail->IsHTML(true);
                $mail->SMTPAuth = true;
                $mail->idconf = $conf;
                $mail->tipouser = "user_adm";
                $mail->idmonitoria = $idmoni;
                $mail->config();
                if($econfmail['user_web'] != "") {
                    $usersweb = explode(",",$econfmail['user_web']);
                }
                else {
                }
                foreach($usersweb as $uw) {
                    $users['user_web'] = $uw;
                }

                foreach($users as $tab => $idusermail) {
                    $seluser = "SELECT nome$tab, email FROM $tab WHERE id$tab='$idusermail'";
                    $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do usuaÅ•io");
                    $email = $eseluser['email'];
                    $nome = $eseluser['nome'.$tab];

                    $mail->AddAddress("$email", "$nome");

                    if ($mail->Send()) {
                        $count++;
                    }
                    else {
                    }
                    $mail->ClearAllRecipients();
                }
            }
            else {
            }
        }
        else {
        }
        if($eupmoni) {
            $msg = "Monitoria Atualizada com sucesso!!!";
            header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
        }
        else {
            $msg = "Erro na query para atualizaÃ§Ã£o da monitoria, favor contatar o administrador!!!";
            header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
        }
    }
}
}

if(isset($_POST['altfluxo'])) {
    $obs = $_POST['obs'];
    $idmonifluxo = $_POST['idmonifluxo'];
    $selfluxo = "SELECT * FROM monitoria_fluxo WHERE idmonitoria_fluxo='$idmonifluxo'";
    $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die ("erro na query de consulta do fluxo da monitoria");
    if($obs == $eselfluxo['obs']) {
        $msg = "A observaÃ§Ã£o nÃ£o foi alterada, processo nÃ£o executado para alteraÃ§Ã£o do fluxo ''$idmonifluxo''!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
    else {
        $upobs = "UPDATE monitoria_fluxo SET obs='$obs' WHERE idmonitoria_fluxo='$idmonifluxo'";
        $eupobs = $_SESSION['query']($upobs) or die ("erro na query de atualizaÃ§Ã£o da observaÃ§Ã£o da avaliaÃ§Ã£o");
        if($eupobs) {
            $msg = "ObservaÃ§Ã£o do fluxo ''$idmonifluxo'' alterada com sucesso!!!";
            header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
        }
        else {
            $msg = "Erro no processo de alteraÃ§Ã£o da observaÃ§Ã£o, favor contatar o administrador!!!";
            header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
        }
    }
}

if(isset($_POST['apagafluxo'])) {
    $idmonifluxo = $_POST['idmonifluxo'];
    $tabslock = array('monitoria_fluxo','monitoria');
    $delfluxo = "DELETE FROM monitoria_fluxo WHERE idmonitoria_fluxo='$idmonifluxo'";
    $edelfluxo = $_SESSION['query']($delfluxo) or die (mysql_error());
    $atuincre = "ALTER TABLE monitoria_fluxo AUTO_INCREMENT=1";
    $eatuincre = $_SESSION['query']($atuincre) or die ("erro na query para atualizar auto_incremento");
    $selfluxo = "SELECT * FROM monitoria_fluxo WHERE idmonitoria='$idmoni' ORDER BY idmonitoria_fluxo DESC LIMIT 1";
    $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die ("erro na query para consulta o fluxo da monitoria");
    $idfluxoatu = $eselfluxo['idmonitoria_fluxo'];
    $atustatus = "UPDATE monitoria SET idmonitoria_fluxo='$idfluxoatu' WHERE idmonitoria='$idmoni'";
    $eatustatus = $_SESSION['query']($atustatus) or die ("erro na query de atualizaÃ§Ã£o da monitoria");
    if($atustatus) {
        $msg = "AvaliaÃ§Ã£o apagada com sucesso e fluxo da monitoria atualizado!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
    else {
        $msg = "Erro no processo para apagar a avaliaÃ§Ã£o, favor contatar o administrador!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
}

if(isset($_POST['reenvia'])) {
    $selmoni = "SELECT *, COUNT(*) as result FROM monitoria WHERE idmonitoria='$idmoni'";
    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da monitoria");
    $mail = new ConfigMail();
    $sconfemail = "SELECT idconfemailenv, user_adm, user_web FROM relemailfg rm 
                       WHERE idrel_filtros='".$eselmoni['idrel_filtros']."'";
    $lcon = $_SESSION['query']($sconfemail) or die ("erro na query de consulta dos dados para envio de e-mail");
    $nconf = $_SESSION['num_rows']($lcon);
    if($nconf >= 1) {
        $econfmail = $_SESSION['fetch_array']($lcon);
        $conf = $econfmail['idconfemailenv'];
        $mail->IsSMTP();
        $mail->IsHTML(true);
        $mail->SMTPAuth = true;
        $mail->idconf = $conf;
        $mail->tipouser = "user_adm";
        $mail->idmonitoria = $idmoni;
        $mail->config();
        if($econfmail['user_web'] != "") {
            $usersweb = explode(",",$econfmail['user_web']);
        }
        if($econfmail['user_adm'] != "") {
            $usersadm = explode(",",$econfmail['user_adm']);
        }
        foreach($usersweb as $uw) {
            $users['user_web'][] = $uw;
        }
        foreach($usersadm as $ua) {
            $users['user_adm'][] = $ua;
        }

        foreach($users as $tab => $idusermail) {
            foreach($idusermail as $id) {
                $seluser = "SELECT nome$tab, email FROM $tab WHERE id$tab='$id'";
                $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do usuaÅ•io");
                $email = $eseluser['email'];
                $nome = $eseluser['nome'.$tab];
                $mail->From = "sist.monitoria@clientis.com.br";
                $mail->FromName = $_SESSION['nomecli'];

                $mail->AddAddress("$email", "$nome");
            }
        }

        if ($mail->Send()) {
            $count++;
        }

        $mail->ClearAddresses();
        $mail->ClearAllRecipients();
    }
    if($count >= 1) {
        $msg = "E-mail enviado com sucesso!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
    else {
        $msg = "Nenhum e-mail enviado!!!";
        header("location:admin.php?menu=monitorias&idmoni=".$idmoni."&msg=".$msg."#".$idmoni);
    }
}

if(isset($_POST['apagamoni']) OR isset($_POST['apagafila'])) {

    $selfila = "SELECT * FROM monitoria WHERE idmonitoria='$idmoni'";
    $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die ("erro na query de consulta do ID da fila");
    $tabfila = $eselfila['tabfila'];
    $idfila = $eselfila['idfila'];
    $tabs = array('pergunta p','grupo g','planilha','planilha pa','moniavalia','monitoria_fluxo','monitoria','monitoria m','monitoria mv',$tabfila);

    $selvinc = "SELECT mv.idmonitoria,mv.idplanilha,COUNT(*) as result FROM monitoria m
                INNER JOIN monitoria mv ON mv.idmonitoriavinc = m.idmonitoria
                WHERE m.idmonitoria='$idmoni'";
    $eselvinc = $_SESSION['fetch_array']($_SESSION['query']($selvinc)) or die (mysql_error());

    lock($tabs);

    if($eselvinc['result'] >= 1) {
        $selplan = "SELECT g.idgrupo,filtro_vinc FROM planilha pa
                    INNER JOIN grupo g ON g.idgrupo = pa.idgrupo
                    WHERE idplanilha='".$eselvinc['idplanilha']."' GROUP BY g.idgrupo";
        $eselplan = $_SESSION['query']($selplan) or die (mysql_error());
        while($lselplan = $_SESSION['fetch_array']($eselplan)) {
            if($lselplan['filtro_vinc'] == "S") {
                $selgrupo = "SELECT idsubgrupo FROM grupo g
                            INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                            WHERE idgrupo='".$lselplan['idgrupo']."' AND avaliaplan='".$eselfila['idplanilha']."' GROUP BY idsubgrupo";
                $eselgrupo = $_SESSION['query']($selgrupo) or die (mysql_error());
                while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
                    $selsub = "SELECT p.idpergunta FROM subgrupo s
                                INNER JOIN pergunta p ON p.idpergunta = s.idpergunta
                                WHERE idsubgrupo='".$lselgrupo['idsubgrupo']."' AND avaliaplan='".$eselfila['idplanilha']."' GROUP BY idpergunta";
                    $eselsub = $_SESSION['query']($selsub) or die (mysql_error());
                    while($lselsub = $_SESSION['fetch_array']($selsub)) {
                        $checkperg = "SELECT COUNT(*) as result FROM moniavalia WHERE idpergunta='".$lselsub['idpergunta']."' AND idmonitoria='".$eselvinc['idmonitoria']."'";
                        $echeck = $_SESSION['fetch_array']($_SESSION['query']($checkperg)) or die (mysql_error());
                        if($echeck['result'] >= 1) {
                            $selresp = "SELECT idresposta FROM pergunta p WHERE idpergunta='".$lselsub['idpergunta']."' AND avaliaplan IS NULL";
                            $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
                            $upprincipal = "UPDATE moniavalia SET idresposta='".$eselresp['idresposta']."' WHERE idmonitoria='".$eselvinc['idmonitoria']."' AND idpergunta='".$lselgrupo['idpergunta']."'";
                            $euppri = $_SESSION['query']($upprincipal) or die (mysql_error());
                        }
                    }
                }
            }
            else {
                $selgrupo = "SELECT idpergunta FROM grupo g
                            INNER JOIN pergunta p ON p.idpergunta = g.idrel
                            WHERE idgrupo='".$lselplan['idgrupo']."' AND avaliaplan='".$eselfila['idplanilha']."' GROUP BY idpergunta";
                $eselgrupo = $_SESSION['query']($selgrupo) or die (mysql_error());
                while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
                    $checkperg = "SELECT COUNT(*) as result FROM moniavalia WHERE idpergunta='".$lselgrupo['idpergunta']."' AND idmonitoria='".$eselvinc['idmonitoria']."'";
                    $echeck = $_SESSION['fetch_array']($_SESSION['query']($checkperg)) or die (mysql_error());
                    if($echeck['result'] >= 1) {
                        $selresp = "SELECT idresposta FROM pergunta p WHERE idpergunta='".$lselgrupo['idpergunta']."' AND avaliaplan IS NULL";
                        $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
                        $upprincipal = "UPDATE moniavalia SET idresposta='".$eselresp['idresposta']."' WHERE idmonitoria='".$eselvinc['idmonitoria']."' AND idpergunta='".$lselgrupo['idpergunta']."'";
                        $euppri = $_SESSION['query']($upprincipal) or die (mysql_error());
                    }
                }
            }
        }
        if($euppri) {
            $upmoni = "UPDATE monitoria SET idmonitoriavinc='0000000' WHERE idmonitoria='".$eselvinc['idmonitoria']."'";
            $eupmoni = $_SESSION['query']($upmoni) or die (mysql_error());
        }
        $delmoni = "DELETE FROM monitoria WHERE idmonitoria='$idmoni'";
        $edelmoni = $_SESSION['query']($delmoni) or die ("erro na query para apagar a monitoria");
    }
    else {
        if(isset($_POST['apagamoni'])) {
            $delmoni = "DELETE FROM monitoria WHERE idfila='$idfila'";
            $edelmoni = $_SESSION['query']($delmoni) or die ("erro na query para apagar a monitoria");
        }
        if(isset($_POST['apagafila'])) {
            $delmoni = "DELETE FROM monitoria WHERE idfila='$idfila'";
            $edelmoni = $_SESSION['query']($delmoni) or die ("erro na query para apagar a monitoria");
            $delfila = "DELETE FROM fila_grava WHERE idfila_grava='$idfila'";
            $edelfila = $_SESSION['query']($delfila) or die (mysql_error());
        }
    }
    //$atuincre = "ALTER TABLE monitoria AUTO_INCREMENT=1";
    //$eincre = $_SESSION['query']($atuincre) or die ("erro na query de atualizaÃ§Ã£o do auto incremento");

    $checkfila = "SELECT count(*) as result FROM monitoria WHERE idfila='$idfila' AND idmonitoria<>$idmoni";
    $echeckfila = $_SESSION['fetch_array']($_SESSION['query']($checkfila)) or die (mysql_error());

    if($euppri && $echeckfila['result'] >= 1) {
    }
    else {
        if(isset($_POST['apagamoni'])) {
            $atufila = "UPDATE $tabfila SET monitorado='0', monitorando='0',idmonitor=NULL WHERE id$tabfila='$idfila'";
            $eatu = $_SESSION['query']($atufila) or die ("erro na query para atualizaÃ§Ã£o da fila");
        }
    }

    $verifmoni = "SELECT COUNT(*) as result FROM monitoria WHERE idmonitoria='$idmoni'";
    $everifm = $_SESSION['fetch_array']($_SESSION['query']($verifmoni)) or die ("erro na query para verificar se a monitoria foi excluida");
    $veriffluxo = "SELECT COUNT(*) as result FROM monitoria_fluxo WHERE idmonitoria='$idmoni'";
    $everiff = $_SESSION['fetch_array']($_SESSION['query']($veriffluxo)) or die ("erro na query de consulta do fluxo da monitoria excluída");
    $verifavalia = "SELECT COUNT(*) as result FROM moniavalia WHERE idmonitoria='$idmoni'";
    $everifa = $_SESSION['fetch_array']($_SESSION['query']($verifavalia)) or die ("erro na query de consulta da avaliaÃ§Ã£o da monitoria");

    unlock();

    if($euppri && $echeckfila['result'] >= 1) {
        $msg = "Monitoria vinculada excluída com sucesso!!!";
        header("location:admin.php?menu=monitorias&msg=".$msg);
    }
    else {
        if($edelmoni && $everifa['result'] == 0 && $everiff['result'] == 0 && $everifm['result'] == 0) {
            if(isset($_POST['apagamoni'])) {
                $msg = "Monitoria excluída com sucesso e registro fila número $idfila disponível para monitoria!!!";
                header("location:admin.php?menu=monitorias&msg=".$msg);
            }
            if(isset($_POST['apagafila'])) {
                $msg = "Monitoria excluída com sucesso e REGISTRO excluído da fila de MONITORIA!!!";
                header("location:admin.php?menu=monitorias&msg=".$msg);
            }
        }
        else {
            $msg = "Erro no processo para excluir a monitoria, favor contatar o administrador!!!";
            header("location:admin.php?menu=monitorias&msg=".$msg);
        }
    }
}
?>
