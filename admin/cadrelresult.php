<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include($rais.'/monitoria_supervisao/seguranca.php');
include($rais.'/monitoria_supervisao/config/conexao.php');
include($rais.'/monitoria_supervisao/selcli.php');
include($rais.'/monitoria_supervisao/admin/functionsadm.php');

if(isset($_POST['cadrel'])) {
    $nome = strtoupper(trim($_POST['nome']));
    $filtros = implode(",",$_POST['filtros']);
    if($_POST['descri'] == "") {
        $descri = "NULL";
    }
    else {
        $descri = "'".$_POST['descri']."'";
    }
    if($filtros[0] == "") {
        $filtros = "NULL";
    }
    else {
        $filtros = "'".$filtros."'";
    }
    $idsperfadm = val_vazio(implode(",",$_POST['idsperfadm']));
    $idsperfweb = val_vazio(implode(",",$_POST['idsperfweb']));
    $ativo = $_POST['ativo'];
    $linhas = $_POST['latu'];
    for($i = 1; $i <= $linhas; $i++) {
        if($_POST['campos'.$i] != "" OR $_POST['posic'.$i] != "") {
            $campos[$i] = array('campo' => $_POST['campos'.$i],'descri' => $_POST['descri'.$i],'idformula' => $_POST['idformula'.$i], 'valor' => $_POST['valor'.$i],'posic' => $_POST['posic'.$i]);
        }
        else {
        }
    }
    $vcampos = $campos;
    $incons = 0;
    foreach($campos as $kvc => $vc) {
        $vposi = $vc['posic'];
        $vkey = $kvc;
        foreach($vcampos as $kcomp => $comp) {
            if($kcomp == $vkey) {
            }
            else {
                if($vposi == $comp['posic']) {
                    $incons++;
                }
                else {
                }
            }
        }
        reset($vcampos);
    }

    if($_POST['nome'] == "") {
        $msg = "Para cadastramento de novo relatÃ³rio Ã© necessÃ¡rio preencher o campo ''NOME''";
        header("location:admin.php?menu=relresult&msg=".$msg);
        break;
    }
    if($idsperfadm[0] == "" && $idsperfweb[0] == "") {
        $msg = "Ã‰ nencessÃ¡rio informar pelo menos 1 perfil para visualizar o relatÃ³rio";
        header("location:admin.php?menu=relresult&msg=".$msg);
        break;
    }
    if($incons >= 1) {
        $msg = "Existe inconsistencia no preenchimento da posiÃ§Ã£o dos campos";
        header("location:admin.php?menu=relresult&msg=".$msg);
        break;
    }
    else {
        lock(relresult);
        $cad = "INSERT INTO relresult (nomerelresult, filtros, descricao, idsperfadm,idsperfweb,ativo) VALUES ('$nome',$filtros,$descri,$idsperfadm,$idsperfweb,'$ativo');";
        $ecad = $_SESSION['query']($cad) or die ("erro na query de inserÃ§Ã£o do relatÃ³rio");
        $idrel = $_SESSION['insert_id']();
        unlock();
        foreach($campos as $cl) {
            lock(relresultcampos);
            $idformula = val_vazio($cl['idformula']);
            $cadcampos = "INSERT INTO relresultcampos (idrelresult,campo,idformula,descricampo,valor,posicao) VALUES ($idrel,'".$cl['campo']."',$idformula,'".$cl['descri']."','".$cl['valor']."','".$cl['posic']."')";
            $ecadcampos = $_SESSION['query']($cadcampos) or die ("erro na query de cadastramento dos campos");
            unlock();
        }
        if($ecad) {
            $msg = "Cadastro efetuado com sucesso!!!";
            header("location:admin.php?menu=relresult&msg=".$msg);
            break;
        }
        else {
            $msg = "Erro no cadastramento do relatÃ³rio, favor contatar o administrador!!!";
            header("location:admin.php?menu=relresult&msg=".$msg);
            break;
        }
    }
}

if(isset($_POST['carrel'])) {
    $idrel = $_POST['idrel'];
    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru=");
}

if(isset($_POST['estrurel'])) {
    $idrel = $_POST['idrel'];
    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=&idestru=&sitestru=");
    break;
}

if(isset($_POST['delrel'])) {
    $idrel = $_POST['idrel'];
    $logo= "SELECT * FROM relresultcab WHERE idrelresult='$idrel'";
    $elogo = $_SESSION['query']($logo) or die ("erro na query de consutla do cabeÃ§alho do relatÃ³rio");
    $clogo = $_SESSION['num_rows']($elogo);
    if($clogo >= 1) {
        $llogo = $_SESSION['fetch_array']($clogo);
        $cam = explode(",",$llogo['caminhologo']);
        foreach($cam as $c) {
            unlink($c);
        }
    }
    else {
    }
    $del = "DELETE FROM relresult WHERE idrelresult='$idrel'";
    $edel = $_SESSION['query']($del) or die ("erro na query para apagar o relatÃ³rio");
    $incre = "ALTER TABLE relresult AUTO_INCREMENT=1";
    $eincre = $_SESSION['query']($incre) or die ("erro na query de atualizaÃ§Ã£o do AUTO_INCREMENTO");
    $auto = array('relresult','relresultcab','relresultcorpo','relresulcampos');
    foreach($auto as $a) {
        $alter = "ALTER TABLE $a AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die ("erro na query para atualizar o auto incremento");
    }
    if($edel) {
        $msg = "RelatÃ³rio apagado com sucesso!!!";
        header("location:admin.php?menu=relresult&msgr=".$msg);
        break;
    }
    else {
        $msg = "Erro no processo para apagar o relatÃ³rio, favor contatar o administrador!!!";
        header("location:admin.php?menu=relresult&msgr=".$msg);
        break;
    }

}

if(isset ($_POST['novorel'])) {
    header("location:admin.php?menu=relresult&idrel=&parte=relatorio&tipoestru=&idestru=&sitestru=");
    break;
}

if(isset ($_POST['altrel'])) {
    $idrel = $_POST['idrel'];
    $nome = strtoupper(trim($_POST['nome']));
    $f = implode(",",$_POST['filtros']);
    $descri = val_vazio($_POST['descri']);
    $filtros = implode(",",$_POST['filtros']);
    if($filtros[0] == "") {
        $filtros = "NULL";
    }
    else {
        $filtros = "'".$filtros."'";
    }
    if($_POST['idsperfadm'] == "") {
        $idsperfadm = "";
    }
    else {
        $idsperfadm = implode(",",$_POST['idsperfadm']);
    }
    if($_POST['idsperfweb'] == "") {
        $idsperfweb = "";
    }
    else {
        $idsperfweb = implode(",",$_POST['idsperfweb']);
    }
    $ativo = $_POST['ativo'];
    $linhas = $_POST['latu'];
    for($i = 1; $i <= $linhas; $i++) {
        if($_POST['idcampos'.$i] != "") {
            if($_POST['campos'.$i] != "" OR $_POST['posic'.$i] != "") {
                $idcampos[$_POST['idcampos'.$i]] = array('idcampos' => $_POST['idcampos'.$i], 'campo' => $_POST['campos'.$i],'idformula' => $_POST['idformula'.$i], 'descri' => $_POST['descri'.$i],'valor' => $_POST['valor'.$i],'posic' => $_POST['posic'.$i]);
                $posiv[] = $_POST['posic'.$i];
            }
            else {
            }
        }
        else {
            if($_POST['campos'.$i] != "" OR $_POST['posic'.$i] != "") {
                $campos[$i] = array('campo' => $_POST['campos'.$i],'idformula' => $_POST['idformula'.$i], 'descri' => $_POST['descri'.$i],'valor' => $_POST['valor'.$i],'posic' => $_POST['posic'.$i]);
                $posiv[] = $_POST['posic'.$i];
            }
            else {
            }
        }
    }
    $posiverif = $posiv;
    $insert = 0;
    foreach($posiv as $kpv => $pv) {
        $posi = $pv;
        $posik = $kpv;
        foreach($posiverif as $kposi => $posivf) {
            if($posik == $kposi) {
            }
            else {
                if($pv == $posivf) {
                    $incons++;
                }
                else {
                }
            }
        }
        reset($posiverif);
    }
    $selcorpo = "SELECT COUNT(*) as result FROM relresultcorpo WHERE idrelresult='$idrel' AND dados IN ('P','PI','PG')";
    $eselcorpo = $_SESSION['fetch_array']($_SESSION['query']($selcorpo)) or die ("erro na query de consulta da estrutura do relatÃ³rio");


    if($nome == "") {
        $msg = "NÃ£o Ã© possÃ­vel alterar o relatÃ³rio se o ''NOME'' nÃ£o estiver preenchido";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru&msg=".$msg);
        break;
    }
    if($incons >= 1) {
        $msg = "Existem posiÃ§Ãµes dos campos em conflito, favor verificar para alteraÃ§Ã£o";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru&msg=".$msg);
        break;
    }
    if($eselcorpo['result'] >= 1 AND !in_array("plan",$_POST['filtros'])) {
        $msg = "JÃ¡ existe estrutura do relatÃ³rio relacionado a ''PLANILHA'', este filtro precisa estar presente";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru&msg=".$msg);
        break;
    }
    else {
        $alt = 0;
        foreach($idcampos as $vid) {
            $verif = "SELECT * FROM relresultcampos WHERE idrelresultcampos='".$vid['idcampos']."'";
            $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ("erro na query de consulta do campo");
            if($vid['campo'] == $everif['campo'] && $vid['idformula'] == $everif['idformula'] && $vid['descri'] == $everif['descricampo'] && $vid['valor'] == $everif['valor'] && $vid['posic'] == $everif['posicao']) {
            }
            else {
                $alt++;
            }
        }
        $selcampos = "SELECT * FROM relresultcampos WHERE idrelresult='$idrel'";
        $ecampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos campos cadastrados");
        while($lcampos = $_SESSION['fetch_array']($ecampos)) {
            $idcamposbd[$lcampos['idrelresultcampos']] = $lcampos['idrelresultcampos'];
        }
        $delc = array_diff_key($idcamposbd,$idcampos);
        foreach($delc as $d) {
            $delcampos[] = $d;
        }

        $selrel = "SELECT * FROM relresult WHERE idrelresult='$idrel'";
        $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relatÃ³rio");
        if($eselrel['descricao'] == "") {
            $describd = "NULL";
        }
        else {
            $describd = $eselrel['descricao'];
        }
        if($eselrel['nomerelresult'] == $nome && $f == $eselrel['filtros'] && $_POST['descri'] == $eselrel['descricao'] && 
           $alt == 0 && $campos == "" && $delcampos[0] == "" && $ativo == $eselrel['ativo'] && $idsperfadm == $eselrel['idsperfadm'] &&
           $idsperfweb == $eselrel['idsperfweb']) {
            $msg = "Nenhum campo foi alterado, processo nÃ£o executado";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru&msg=".$msg);
            break;
        }
        else {
            lock(relresult);
            $alter = "UPDATE relresult SET nomerelresult='$nome', filtros=$filtros, descricao=$descri, idsperfadm='$idsperfadm', idsperfweb='$idsperfweb', ativo='$ativo' WHERE idrelresult='$idrel'";
            $ealter = $_SESSION['query']($alter) or die ("erro na query para atualizaÃ§Ã£o do cadastro");
            unlock();
            lock(relresultcampos);
            foreach($delcampos as $delc) {
                $del = "DELETE FROM relresultcampos WHERE idrelresultcampos='$delc'";
                $edel = $_SESSION['query']($del) or die ("erro na query para apagar campos");
            }
            $autoincre = "ALTER TABLE relresultcampos AUTO_INCREMENT=1";
            $eincre = $_SESSION['query']($autoincre) or die ("erro na query de alteraÃ§Ã£o da tabela");
            foreach($idcampos as $ca) {
                if($alt == 0) {
                }
                else {
                    $cadcampos = "UPDATE relresultcampos SET idrelresult='$idrel',campo='".$ca['campo']."',idformula=".val_vazio($ca['idformula']).", descricampo='".$ca['descri']."',valor='".$ca['valor']."',posicao='".$ca['posic']."' WHERE idrelresultcampos='".$ca['idcampos']."'";
                    $ecadcampos = $_SESSION['query']($cadcampos) or die ("erro na query de cadastramento dos campos");
                }
            }
            $insert = 0;
            foreach($campos as $cl) {
                $idformula = val_vazio($cl['idformula']);
                $cadcampos = "INSERT INTO relresultcampos (idrelresult,campo,idformula,descricampo,valor,posicao) VALUES ($idrel,'".$cl['campo']."',$idformula,'".$cl['descri']."','".$cl['valor']."','".$cl['posic']."')";
                $ecadcampos = $_SESSION['query']($cadcampos) or die ("erro na query de cadastramento dos campos");
                $insert++;
            }
            unlock();

            if($ealter OR ($campos[0] != "" OR $alt >= 1 OR $edel OR $insert != 0)) {
                $msg = "Cadastro alterado com sucesso!!!";
                header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru&msg=".$msg);
                break;
            }
            else {
                $msg = "Erro no processo para alteraÃ§Ã£o do cadastro, favor contatar o administrador";
                header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=relatorio&tipoestru=&idestru=&sitestru&msg=".$msg);
                break;
            }
        }
    }
}

if(isset ($_POST['cadcab'])) {
    $idrel = $_POST['idrelresult'];
    $idestru = $_POST['idestru'];
    $campos = implode(",",$_POST['campos']);
    $caminho = $rais."/monitoria_supervisao/logos";
    $posicampos = $_POST['posicampo'];
    $widthlogo = val_vazio($_POST['width']);
    $heightlogo = val_vazio($_POST['height']);
    $qtdes = $_POST['qtdelogo'];
    $nok = 0;
    $errocam = 0;
    for($i = 1; $i <= $qtdes; $i++) {
        $posilogo[$i] = $_POST['posilogo'.$i];
        if(is_file($_FILES['camlogo'.$i]['tmp_name'])) {
            $camlogo[$i] = $caminho."/".$_FILES['camlogo'.$i]['name'];
            $camup[$i] = $_FILES['camlogo'.$i]['tmp_name'];
        }
        else {
            $camlogo[$i] = "erro";
        }
    }
    for($i = 1; $i <= $qtdes; $i++) {
        if($posilogo[$i] == "") {
            $nok++;
        }
        else {
        }
        if($camlogo[$i] == "") {
            $nok++;
        }
        if($camlogo[$i] == "erro") {
            $errocam++;
        }
        if($camlogo[$i] != "" && $camlogo[$i] != "erro") {
        }
    }
    if($campos == "" OR $posicampos == "" OR $qtdes == "") {
        $msg = "Todos os campos precisam estar preenchidos para cadastro do cabeÃ§alho";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=cad&msgcab=".$msg);
        break;
    }
    else {
        if($nok >= 1) {
            $msg = "Existem inconsistencias no preenchimento dos arquivos com os logos, favor verificar";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=cad&msgcab=".$msg);
            break;
        }
        else {
            if($errocam >= 1) {
                $msg = "Os arquivos informados para os logos do relatÃ³rio nÃ£o sÃ£o arquivos vÃ¡lidos";
                header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=cad&msgcab=".$msg);
                break;
            }
            else {
                foreach($camup as $kup => $up) {
                    move_uploaded_file($up, $caminho."/".$_FILES['camlogo'.$kup]['name']);
                }
                $cl = val_vazio(implode(",",$camlogo));
                $pl = val_vazio(implode(",",$posilogo));
                $cadcab = "INSERT INTO relresultcab (idrelresult,qtdelogos,caminhologo,posilogo,width,height,campos,posicampos) VALUES ('$idrel','$qtdes',$cl,$pl,$widthlogo,$heightlogo, '$campos','$posicampos')";
                $ecadcab = $_SESSION['query']($cadcab) or die ("erro na query de inserÃ§Ã£o do cabeÃ§alho do relatÃ³rio");
                if($ecadcab) {
                    $msg = "Cadastro efetuado com sucesso!!";
                    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=cad&msgcab=".$msg);
                    break;
                }
                else {
                    $msg = "Erro no processo para execuÃ§Ã£o do cadastro, favor contatar o administrador";
                    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=cad&msgcab=".$msg);
                    break;
                }
            }
        }
    }
}

if(isset ($_POST['carcab'])) {
    $idrel = $_POST['idrelresult'];
    $idestru = $_POST['idestru'];
    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt");
    break;
}

if(isset ($_POST['delcab'])) {
    $idrel = $_POST['idrelresult'];
    $idestru = $_POST['idestru'];
    $imagens = explode(",",$_POST['arquivo']);
    foreach($imagens as $i) {
        unlink($imagem);
    }
    $delcab = "DELETE FROM relresultcab WHERE idrelresultcab='$idestru' AND idrelresult='$idrel'";
    $edelcab = $_SESSION['query']($delcab) or die ("erro na query para apagar o cabeÃ§alho");
    $alter = "ALTER TABLE relresultcab AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query para alteraÃ§Ã£o da tabela");
    if($edelcab) {
        $msg = "CabeÃ§alho apagado com sucesso!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=&idestru=&sitestru=&msgcadcab=".$msg);
        break;
    }
    else {
        $msg = "Erro na query para apagar o cabeÃ§alho, favor contatar o administrador";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=&idestru=&sitestru=&msgcadcab=".$msg);
        break;
    }
}

if(isset ($_POST['altcab'])) {
    $idrel = $_POST['idrelresult'];
    $idestru = $_POST['idestru'];
    $campos = implode(",",$_POST['campos']);
    $caminho = $rais."/monitoria_supervisao/logos";
    $posicampos = $_POST['posicampo'];
    $widthlogo = val_vazio($_POST['width']);
    $heightlogo = val_vazio($_POST['height']);
    $qtdes = $_POST['qtdelogo'];
    $nok = 0;
    $errocam = 0;
    for($i = 1; $i <= $qtdes; $i++) {
        $posilogo[$i] = trim($_POST['posilogo'.$i]);
        $arquivo = $caminho."/".$_FILES['camlogo'.$i]['name'];
        if(is_file($_FILES['camlogo'.$i]['tmp_name'])) {
            $camlogo[$i] = $arquivo;
            if($_POST['arquivo'.$i] == $arquivo) {
            }
            else {
                $camup[$i] = $_FILES['camlogo'.$i]['tmp_name'];
            }
            $camdel[$i] = $_POST['arquivo'.$i];
        }
        else {
            $camlogo[$i] = $_POST['arquivo'.$i];
        }
    }
    for($i = 1; $i <= $qtdes; $i++) {
        if($posilogo[$i] == "") {
            $nok++;
        }
        else {
        }
        if($camlogo[$i] == "") {
            $nok++;
        }
        if($camlogo[$i] == "erro") {
            $errocam++;
        }
        if($camlogo[$i] != "" && $camlogo[$i] != "erro") {
        }
    }
    if($campos == "" OR $posicampos == "" OR $qtdes == "") {
        $msg = "Todos os campos precisam estar preenchidos para cadastro do cabeÃ§alho";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt&msgcab=".$msg);
        break;
    }
    else {
        if($nok >= 1) {
            $msg = "Existem inconsistencias no preenchimento dos arquivos com os logos, favor verificar";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt&msgcab=".$msg);
            break;
        }
        else {
            if($errocam >= 1) {
                $msg = "Os arquivos informados para os logos do relatÃ³rio nÃ£o sÃ£o arquivos vÃ¡lidos";
                header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt&msgcab=".$msg);
                break;
            }
            else {
                $cl = implode(",",$camlogo);
                $pl = implode(",",$posilogo);
                $selcab = "SELECT * FROM relresultcab WHERE idrelresultcab='$idestru'";
                $eselcab = $_SESSION['fetch_array']($_SESSION['query']($selcab)) or die ("erro na query de consulta do cabeÃ§alho cadastrado");
                if($campos == $eselcab['campos'] && $posicampos == $eselcab['posicampos'] && $pl == $eselcab['posilogo'] && $cl == $eselcab['caminhologo'] && $_POST['width'] == $eselcab['width'] && $_POST['height'] == $eselcab['height']) {
                    $msg = "Nenhum dado foi alterado, processo nÃ£o executado";
                    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt&msgcab=".$msg);
                    break;
                }
                else {
                    foreach($camup as $kcamup => $cup) {
                        move_uploaded_file($cup, $caminho."/".$_FILES['camlogo'.$kcamup]['name']);
                    }
                    foreach($camdel as $cdel) {
                        unlink($cdel);
                    }
                    $lock = "LOCK TABLES relresultcab WRITE";
                    $elock = $_SESSION['query']($lock) or die ("erro na query de bloqueio da tabela");
                    $update = "UPDATE relresultcab SET campos='$campos', posicampos='$posicampos', posilogo='$pl',width=$widthlogo,height=$heightlogo, caminhologo='$cl' WHERE idrelresultcab='$idestru'";
                    $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o do cadastro");
                    $unlock = "UNLOCK TABLES";
                    $eunlock = $_SESSION['query']($unlock) or die ("erro na query para desbloquear a tabela");
                    if($eupdate) {
                        $msg = "AlteraÃ§Ã£o executada com sucesso!!!";
                        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt&msgcab=".$msg);
                        break;
                    }
                    else {
                        $msg = "Erro no processo de alteraÃ§Ã£o, favor contatar o administrador";
                        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=cab&idestru=".$idestru."&sitestru=alt&msgcab=".$msg);
                        break;

                    }
                }
                
            }
        }
    }
}

if(isset ($_POST['cadcorpo'])) {
    $idrel = $_POST['idrelresult'];
    $tipodados = $_POST['tipodados'];
    $dados = $_POST['dados'];
    $freq = $_POST['freq'];
    $apresdados = $_POST['apresdados'];
    $selform = "SELECT * FROM formulas WHERE idplanilha<>''";
    $eselform = $_SESSION['query']($selform) or die ("erro na query de consulta das formulas vinculas a planilhas");
    while($lselform = $_SESSION['fetch_array']($eselform)) {
        $formulas[] = strtoupper(trim(str_replace(" ", "_", $lselform['nomeformulas'])));
        $formulas[] = strtoupper("perc_".trim(str_replace(" ", "_", $lselform['nomeformulas'])));
    }
    $colsgraf = array('F' => 'QTDE_MONI,RESULTADO','MO' => 'MEDIA,PERC_FG','MS' => 'MEDIA,PERC_FG','P' => 'POSSIVEL,MEDIA','PG' => 'PERC,PERC_ACUMU','PI' => 'PERC,PERC_ACUMU','A' => 'QTDE_IMP,QTDE_REAL','IT' => implode(",",$formulas));
    $camposfora = array('idrelresultcorpo','idrelresult','tipodados','frequenciadados','dados','tabulacao','idformula','agrupadados','apresdados','tipografico');
    $selcampos = "SHOW COLUMNS FROM relresultcorpo";
    $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos campos do grafico");
    while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
        if(in_array($lselcampos['Field'],$camposfora)) {
        }
        else {
            $dadosparam[] = $lselcampos['Field'];
        }
    }
    if($apresdados == "GRAFICO") {
        foreach($dadosparam as $dp) {
            if($_POST[$dp] == "") {
            }
            else {
                $paramgraf[] = val_vazio($_POST[$dp]);
            }
        }
        $cols = explode(",",$colsgraf[$dados]);
        foreach($cols as $col) {
            $confcol[$col]['cor'] = $_POST['cor'.$dados.$col];
            $confcol[$col]['tipograf'] = $_POST['tp'.$dados.$col];
        }
    }
    else {
        foreach($dadosparam as $dp) {
            $paramgraf[] = "NULL";
        }
        $confcol = "";
    }


    if($_POST['dados'] == "F") {
        $agd = val_vazio($_POST['agd']);
    }
    else {
        $agd  = "NULL";
    }
    if($_POST['tipograf'] == "NULL") {
        $tg = "NULL";
        $tipograf = $_POST['tipograf'];
    }
    else {
        $tg = $_POST['tipograf'];
        $tipograf = "'".$_POST['tipograf']."'";
    }
    $formula = val_vazio($_POST['idformula']);
    $tabula = val_vazio($_POST['tabulacao']);

    if($tipodados == "" OR $dados == "" OR $apresdados == "" OR $freq == "") {
        $msg = "OS campos ''Tipos Dados'',''Frequencia'', ''Dados'' e ''ApresentaÃ§Ã£o Dados'' nÃ£o podem estar vazios";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
        break;
    }
    if($dados == "F" && $formula == "NULL") {
        $msg = "Quando os dados do relatÃ³rio for ''FORMULA'' Ã© necessÃ¡rio selecionar 1 formula para cadastro!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
        break;
    }
    if($dados == "TAB" && $_POST['tabulacao'] == "") {
        $msg = "Quando os dados do relatÃ³rio for ''TABULAÇÃO'' Ã© necessÃ¡rio selecionar uma tabulaÃ§Ã£o para cadastro!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
        break;
    }
    else {
        if($apresdados == "TABELA" && $tg != "NULL") {
            $msg = "Se o tipo de apresentaÃ§Ã£o dos dados for ''TABELA'', o tipo de grÃ¡fico tem que ser ''NENHUM''";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
            break;
        }
        if($apresdados == "GRAFICO" && ($tg == "NULL" OR count($paramgraf) < 13)) {
            $msg = "Se o tipo de apresentaÃ§Ã£o dos dados for ''GRAFICO'', o tipo de grÃ¡fico tem que ser diferente ''NENHUM'' e os parametros do grÃ¡fico precisam estar preenchidos!!!";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
            break;
        }
        if(($apresdados == "TABELA" && $tg == "NULL") OR ($apresdados == "GRAFICO" && $tg != "NULL")) {
            $verif = "SELECT *, COUNT(*) as result FROM relresultcorpo WHERE tipodados='$tipodados' AND dados='$dados' AND apresdados='$apresdados' AND tipografico=$tipograf AND idrelresult='$idrel' AND agrupadados=$agd AND idformula=$formula AND tabulacao=$tabula";
            $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ("erro na query de consulta da estrutura cadastrada");
            if($everif['result'] >= 1) {
                $msg = "JÃ¡ existe estrutura do mesmo relatÃ³rio cadastrado com os mesmos parametros";
                header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
                break;
            }
            else {
                if($dados == "P" OR $dados == "PI" OR $dados == "PG") {
                    $selrelf = "SELECT filtros, COUNT(*) as result FROM relresult WHERE idrelresult='$idrel'";
                    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrelf)) or die ("erro na query de consulta do relatÃ³rio");
                    if($eselrel['filtros'] == "") {
                        $filtros = array();
                    }
                    else {
                        $filtros = explode(",",$eselrel['filtros']);
                    }
                    if(!in_array("plan", $filtros)) {
                        $filtros[] = "plan";
                        $f = implode(",",$filtros);
                        lock(relresult);
                        $insertf = "UPDATE relresult SET filtros='$f' WHERE idrelresult='$idrel'";
                        $einsertf = $_SESSION['query']($insertf) or die ("erro na query de insert do filtro PLANILHA");
                        unlock(relresult);
                    }
                    else {
                    }
                }
                lock(relresultcorpo);
                $cadestru = "INSERT INTO relresultcorpo (idrelresult, tipodados, frequenciadados, dados, tabulacao, idformula, agrupadados, apresdados, tipografico,".implode(",",$dadosparam).") VALUES ('$idrel', '$tipodados','$freq', '$dados',$tabula,$formula, $agd,'$apresdados',$tipograf,".implode(",",$paramgraf).")";
                $ecadestru = $_SESSION['query']($cadestru) or die ("erro na query de cadastramento da estrutura");
                $idrelresult = $_SESSION['insert_id']();
                unlock(relresultcorpo);
                if($confcol == "") {
                }
                else {
                    foreach($confcol as $kconf => $conf) {
                        $cadconf = "INSERT INTO graficocorpo (idrelresultcorpo,cor,campo,tipograf) VALUES ($idrelresult,'".$conf['cor']."','".$kconf."','".$conf['tipograf']."')";
                        $ecadconf = $_SESSION['query']($cadconf) or die ("erro na query de inserÃ§Ã£o das configuraÃ§Ãµes das colunas do grÃ¡fico");
                    }
                }
                if($ecadestru) {
                    $msg = "Cadastro efetuado com sucesso!!!";
                    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
                    break;
                }
                else {
                    $msg = "Erro no processo de cadatramento da estrutura, favor contatar o administrador";
                    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
                    break;
                }
            }
        }
    }
}

if(isset ($_POST['carcorpo'])) {
    $idrel = $_POST['idrel'];
    $idestru =  $_POST['idestru'];
    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt");
    break;
}

if(isset ($_POST['novocorpo'])) {
    $idrel = $_POST['idrelresult'];
    $idestru = $_POST['idestru'];
    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad");
    break;
}

if(isset ($_POST['altcorpo'])) {
    $idrel = $_POST['idrelresult'];
    $idestru = $_POST['idestru'];
    $idrel = $_POST['idrelresult'];
    $tipodados = $_POST['tipodados'];
    $dados = $_POST['dados'];
    $freq = $_POST['freq'];
    $apresdados = $_POST['apresdados'];
    $selform = "SELECT * FROM formulas WHERE idplanilha<>''";
    $eselform = $_SESSION['query']($selform) or die ("erro na query de consulta das formulas vinculas a planilhas");
    while($lselform = $_SESSION['fetch_array']($eselform)) {
        $formulas[] = strtoupper(trim(str_replace(" ", "_", $lselform['nomeformulas'])));
        $formulas[] = strtoupper("perc_".trim(str_replace(" ", "_", $lselform['nomeformulas'])));
    }
    $colsgraf = array('F' => 'QTDE_MONI,RESULTADO','MO' => 'MEDIA,PERC_FG','MS' => 'MEDIA,PERC_FG','P' => 'POSSIVEL,MEDIA','PG' => 'PERC,PERC_ACUMU','PI' => 'PERC,PERC_ACUMU','A' => 'QTDE_IMP,QTDE_REAL','IT' => implode(",",$formulas),'TAB' => 'QTDE,PERC');
    $camposfora = array('idrelresultcorpo','idrelresult','tipodados','frequenciadados','dados','tabulacao','idformula','agrupadados','apresdados','tipografico');
    $selcampos = "SHOW COLUMNS FROM relresultcorpo";
    $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos campos do grafico");
    while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
        if(in_array($lselcampos['Field'],$camposfora)) {
        }
        else {
            $dadosparam[] = $lselcampos['Field'];
        }
    }
    if($apresdados == "GRAFICO") {
        foreach($dadosparam as $dp) {
            $paramgraf[$dp] = val_vazio($_POST[$dp]);
        }
        $cols = explode(",",$colsgraf[$dados]);
        foreach($cols as $col) {
            $confcol[$col]['cor'] = $_POST["cor$dados$col"];
            $confcol[$col]['tipograf'] = $_POST["tp$dados$col"];
        }
    }
    else {
        foreach($dadosparam as $dp) {
            $paramgraf[$dp] = "NULL";
        }
        $confcol = "";
    }
    $c = 0;
    foreach($confcol as $vkconf => $vconfcol) {
        $selcol = "SELECT COUNT(*) as result FROM graficocorpo WHERE campo='$vkconf' AND cor='".$vconfcol['cor']."' AND tipograf='".$vconfcol['tipograf']."'";
        $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta das colunas do grafico");
        if($eselcol['result'] >= 1) {
        }
        else {
            $c++;
        }
    }

    if($_POST['dados'] == "F") {
        if($_POST['agd'] == "NULL") {
            $agd = "NULL";
        }
        else {
            $agd = val_vazio($_POST['agd']);
        }
    }
    else {
        $agd  = val_vazio($_POST['agd']);
    }
    if($_POST['tipograf'] == "NULL") {
        $tg = "NULL";
        $tipograf = $_POST['tipograf'];
    }
    else {
        $tg = $_POST['tipograf'];
        $tipograf = "'".$_POST['tipograf']."'";
    }
    $formula = val_vazio($_POST['idformula']);
    $tabula = val_vazio($_POST['tabulacao']);

    if($tipodados == "" OR $dados == "" OR $apresdados == "" OR $_POST['tipograf'] == "" OR $freq == "") {
        $msg = "OS campos ''Tipos Dados'',''Dados'', ''ApresentaÃ§Ã£o Dados'' e ''Tipo Grafico'' nÃ£o podem estar vazios";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
        break;
    }
    if($dados == "F" && $formula == "NULL") {
        $msg = "Quando os dados do relatÃ³rio for ''FORMULA'' Ã© necessÃ¡rio selecionar 1 formula para cadastro!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
        break;
    }
    if($dados == "TAB" && $_POST['tabulacao'] == "") {
        $msg = "Quando os dados do relatÃ³rio for ''TABULAÇÃO'' Ã© necessÃ¡rio selecionar uma tabulaÃ§Ã£o para cadastro!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
        break;
    }
    else {
        if($apresdados == "TABELA" && $tg != "NULL") {
            $msg = "Se o tipo de apresentaÃ§Ã£o dos dados for ''TABELA'', o tipo de grÃ¡fico tem que ser ''NENHUM''";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
            break;
        }
        if($apresdados == "GRAFICO" && ($tg == "NULL" OR count($paramgraf) < 13)) {
            $msg = "Se o tipo de apresentaÃ§Ã£o dos dados for ''GRAFICO'', o tipo de grÃ¡fico tem que ser diferente ''NENHUM''";
            header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
            break;
        }
        if(($apresdados == "TABELA" && $tg == "NULL") OR ($apresdados == "GRAFICO" && $tg != "NULL")) {
            $verif = "SELECT *, COUNT(*) as result FROM relresultcorpo WHERE tipodados='$tipodados' AND dados='$dados' AND apresdados='$apresdados' AND tipografico=$tipograf AND frequenciadados='$freq' AND idrelresult='$idrel' AND agrupadados=$agd AND idformula=$formula AND idrelresultcorpo<>'$idestru'";
            $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ("erro na query de consulta da estrutura cadastrada");
            if($everif['result'] >= 1) {
                $msg = "JÃ¡ existe estrutura do mesmo relatÃ³rio cadastrado com os mesmos parametros";
                header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
                break;
            }
            else {
                $selestru = "SELECT * FROM relresultcorpo WHERE idrelresultcorpo='$idestru'";
                $eestru = $_SESSION['fetch_array']($_SESSION['query']($selestru)) or die ("erro na query de consulta da estrutura do corpo cadastrada");
                $agdbd = val_vazio($eestru['agrupadados']);
                $tgbd = str_replace("'","",val_vazio($eestru['tipografico']));
                $formbd = str_replace("'","",val_vazio($eestru['idformula']));
                if($eestru['apresdados'] == "GRAFICO") {
                    foreach($dadosparam as $dpbd) {
                        $paramgrafbd[$dpbd] = val_vazio($eestru[$dpbd]);
                    }
                }
                else {
                    foreach($dadosparam as $dpbd) {
                        $paramgrafbd[$dpbd] = "NULL";
                    }
                }

                foreach($paramgrafbd as $kparam => $param) {
                    if($param != $paramgraf[$kparam]) {
                        $compara[$kparam] = $paramgraf[$kparam];
                    }
                    else {
                    }
                }
                //$compara = array_diff($paramgraf, $paramgrafbd);
                if($compara == "") {
                    $comp = 0;
                }
                else {
                    $comp = count($compara);
                }

                if($tipodados == $eestru['tipodados'] && $freq == $eestru['frequenciadados'] && $dados == $eestru['dados'] && $_POST['tabulacao'] == $eestru['tabulacao'] && $apresdados == $eestru['apresdados'] && $tg == $tgbd && $_POST['agd'] == str_replace("'", "", $agdbd) && str_replace("'","",  val_vazio($_POST['idformula'])) == $formbd && $comp == 0 && $c == 0) {
                    $msg = "Nenhum dado foi alterado, processo nÃ£o executado!!!";
                    header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
                    break;
                }
                else {
                    foreach($compara as $kpg => $pg) {
                        if($pg == "NULL") {
                            $pgup[] = $kpg."=".$pg."";
                        }
                        else {
                            $pgup[] = $kpg."=".$pg."";
                        }
                    }
                    if($pgup != "") {
                        $pgup = ",".implode(",",$pgup);
                    }
                    else {
                    }
                    lock(relresultcorpo);
                    $update = "UPDATE relresultcorpo SET tipodados='$tipodados', frequenciadados='$freq', dados='$dados', idformula=$formula, apresdados='$apresdados', tipografico=$tipograf, agrupadados=$agd $pgup WHERE idrelresultcorpo='$idestru'";
                    $eupdate = $_SESSION['query']($update) or die ("erro na query de alteraÃ§Ã£o da estrutura");
                    unlock(relresultcorpo);
                    if($c >= 1) {
                        if($apresdados == "TABELA") {
                            lock(graficocorpo);
                            $delcol = "DELETE FROM graficocorpo WHERE idrelresultcorpo='$idestru'";
                            $edelcol = $_SESSION['query']($delcol) or die ("erro na query para apagar as colunas relacionadas ao grÃ¡fico");
                            $upauto = "ALTER TABLE graficocorpo AUTO_INCREMENT=1";
                            $eupauto = $_SESSION['query']($upauto) or die ("erro na query para atualizar auto_increment");
                            unlock(graficocorpo);
                        }
                        else {
                            lock(graficocorpo);
                            $delcol = "DELETE FROM graficocorpo WHERE idrelresultcorpo='$idestru'";
                            $edelcol = $_SESSION['query']($delcol) or die ("erro na query para apagar as colunas relacionadas ao grÃ¡fico");
                            $upauto = "ALTER TABLE graficocorpo AUTO_INCREMENT=1";
                            $eupauto = $_SESSION['query']($upauto) or die ("erro na query para atualizar auto_increment");
                            foreach($confcol as $kccol => $ccol) {
                                $insertcol = "INSERT INTO graficocorpo (idrelresultcorpo,cor,campo,tipograf) VALUES ($idestru,'".$ccol['cor']."','".$kccol."','".$ccol['tipograf']."')";
                                $einsertcol = $_SESSION['query']($insertcol) or die ("erro na query de inserÃ§Ã£o das colunas do grafico");
                            }
                            unlock(graficocorpo);
                        }
                    }
                    else {
                        if($apresdados == "TABELA") {
                            lock(graficocorpo);
                            $delcol = "DELETE FROM graficocorpo WHERE idrelresultcorpo='$idestru'";
                            $edelcol = $_SESSION['query']($delcol) or die ("erro na query para apagar as colunas relacionadas ao grÃ¡fico");
                            $upauto = "ALTER TABLE graficocorpo AUTO_INCREMENT=1";
                            $eupauto = $_SESSION['query']($upauto) or die ("erro na query para atualizar auto_increment");
                            unlock(graficocorpo);
                        }
                        else {
                        }
                    }

                    if($eupdate) {
                        $msg = "AlteraÃ§Ã£o executada com sucesso!!!";
                        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
                        break;
                    }
                    else {
                        $msg = "Erro no processo de alteraÃ§Ã£o, favor contatar o administrador!!!";
                        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=".$idestru."&sitestru=alt&msgcorpo=".$msg);
                        break;
                    }
                }
            }
        }
    }
}

if(isset ($_POST['delcorpo'])) {
    $idestru = $_POST['idestru'];
    $idrel = $_POST['idrel'];
    $delcorpo = "DELETE FROM relresultcorpo WHERE idrelresultcorpo='$idestru'";
    $edel = $_SESSION['query']($delcorpo) or die ("erro na query que apaga a linha da estrutura");
    $alter = "ALTER TABLE relresultcorpo AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query que atualiza a tabela");
    if($edel) {
        $msg = "Estrutura apagada com sucesso!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
        break;
    }
    else {
        $msg = "Estrutura apagada com sucesso!!!";
        header("location:admin.php?menu=relresult&idrel=".$idrel."&parte=estrutura&tipoestru=corpo&idestru=&sitestru=cad&msgcorpo=".$msg);
        break;
    }
}
?>
