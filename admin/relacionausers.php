<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");

$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
        <link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function() {
                $('#idrelref').change(function() {
                    var idrelref = $(this).val();
                    var idrel = $('#idrelusers').val();
                    $('#iduseradm').load('/monitoria_supervisao/admin/cadreluserfiltro.php',{idrelref:idrelref,idrelusers:idrel,tipouser: 'user_adm',acao:'lista'});
                    $('#iduserweb').load('/monitoria_supervisao/admin/cadreluserfiltro.php',{idrelref:idrelref,idrelusers:idrel,tipouser: 'user_web',acao:'lista'});
                })
            })
            $(document).ready(function() {
                $('#relaciona').click(function() {
                    var iduserweb = $('#iduserweb').val();
                    var iduseradm = $('#iduseradm').val();
                    if(iduseradm == null && iduserweb == null) {
                        $("#spanmsg").html("É necessário informar pelo menos 1 usuário para efetuar o relacionamento");
                        return false;
                    }
                    else {
                    }
                })
            })

        </script>
    </head>
    <body>
    <div style="width: 100%; margin: auto; background-color: <?php echo $cor->corfd_pag;?>">
        <form action="cadreluserfiltro.php" method="post">
            <table width="100%" style="font-family: sans-serif; font-size: 12px; " align="center">
              <tr>
                <td colspan="2" align="center" class="corfd_ntab"><strong>RELACIONAMENTO REFERENCIA</strong></td>
              </tr>
              <tr>
                <td colspan="2"><input name="idrelusers" id="idrelusers" type="hidden" value="<?php echo $_GET['idrel'];?>"/>
                    <select name="idrelref" id="idrelref" style="width:100%">
                        <option value="" selected="selected">SELECIONE...</option>
                        <?php
                        $idsrel = arvoreescalar('','');
                        foreach($idsrel as $idrel) {
                            echo "<option value=\"".$idrel."\">".nomeapres($idrel)."</option>";
                        }
                        ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td width="281" align="center" class="corfd_coltexto"><strong>USUÁRIOS ADM</strong></td>
                <td width="281" align="center" class="corfd_coltexto"><strong>USUÁRIOS WEB</strong></td>
              </tr>
              <tr>
                <td align="center" class="corfd_colcampos">
                    <select name="iduseradm[]" id="iduseradm" style="width:98%" multiple="multiple" size="15">
                        <?php
                        $seluser = "SELECT * FROM user_adm WHERE ativo='S' ORDER BY nomeuser_adm";
                        $eseluser = $_SESSION['query']($seluser) or die (mysql_error());
                        while($lseluser = $_SESSION['fetch_array']($eseluser)) {
                            ?>
                            <option value="<?php echo $lseluser['iduser_adm'];?>"><?php echo $lseluser['nomeuser_adm'];?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
                <td class="corfd_colcampos">
                    <select name="iduserweb[]" id="iduserweb" style="width:98%" multiple="multiple" size="15">
                        <?php
                        $seluser = "SELECT * FROM user_web WHERE ativo='S' ORDER BY nomeuser_web";
                        $eseluser = $_SESSION['query']($seluser) or die (mysql_error());
                        while($lseluser = $_SESSION['fetch_array']($eseluser)) {
                            ?>
                            <option value="<?php echo $lseluser['iduser_web'];?>"><?php echo $lseluser['nomeuser_web'];?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="relaciona" id="relaciona" type="submit" value="Relacionar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="avancar" id="avancar" type="submit" value="Avançar" /></td>
              </tr>
            </table><br/>
            <span id="spanmsg" style="color: #CC0000; font-weight: bold;font-family: sans-serif; font-size: 12px; margin: auto"><?php echo $_GET['msg']; ?></span>
        </form>
    </div>
    </body>
</html>
