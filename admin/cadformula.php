<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

if(isset($_POST['cadastra'])) {
    $tipo = $_POST['tipo'];
    if($tipo == "P") {
        $id = $_POST['planilha'];
        $tab = "planilha";
    }
    if($tipo == "RR") {
        $id = $_POST['rel'];
        $tab = "relresult";
    }
    if($tipo == "RM") {
        $id = $_POST['rel'];
        $tab = "relmoni";
    }
    $titulo = strtoupper(trim($_POST['titulo']));
    $formula = trim($_POST['formula']);

    if($formula == "" OR $titulo == "" OR $tipo == "") {
        $msg = "Os campos ''Titulo, Formula e Tipo'' não podem estar vazios!!!";
        header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&msg=".$msg);
    }
    else {
        if(($tipo == "RM" OR $tipo == "RR") && $id == "") {
            $msg = "Para formulas relacionadas aos relatórios é necessário selecionar 1 relatório!!!";
            header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&msg=".$msg);
        }
        else {
            $cadform = "INSERT INTO formulas (nomeformulas, tipo, id$tab, formula) VALUES ('$titulo', '$tipo', '$id', '$formula')";
            $ecad = $_SESSION['query']($cadform) or die ("erro na query de cadastro da formula");

            if($ecad) {
                $msg = "Formula cadastrada com sucesso!!!";
                header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&msg=".$msg);
            }
            else {
                $msg = "Erro no processo de cadastamento, favor contatar o administrador!!!";
                header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&msg=".$msg);
            }
        }
    }
}

if(isset($_POST['carraltera'])) {
    $idformula = $_POST['id'];
    header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&idformula=".$idformula);
}

if(isset($_POST['altera'])) {
    $idformula = $_POST['id'];
    $titulo = strtoupper(trim($_POST['titulo']));
    $tipo = $_POST['tipo'];
    $formula = trim($_POST['formula']);
    if($tipo == "RR") {
        $tab = "relresult";
        $id = $_POST['rel'];
    }
    if($tipo == "RM") {
        $tab = "relmoni";
        $id = $_POST['rel'];
    }
    if($tipo == "P") {
        $tab = "planilha";
        $id = $_POST['planilha'];
    }

    if($tipo == "" OR $titulo == "" OR $formula == "") {
        $msg = "Os campos ''Titulo, Formula e Tipo'' não podem estar vazios!!!";
        header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&idformula=".$idformula."&msg=".$msg);
    }
    else {
        if(($tipo == "RM" OR $tipo == "RR") && $id == "") {
            $msg = "Para formulas relacionadas aos relatórios é necessário selecionar 1 relatório!!!";
            header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&idformula=".$idformula."&msg=".$msg);
        }
        else {
            $selform = "SELECT * FROM formulas WHERE idformulas='$idformula'";
            $eselform = $_SESSION['fetch_array']($_SESSION['query']($selform)) or die ("erro na query de consulta da formula cadastrada");
            if($titulo == $eselform['nomeformulas'] AND $tipo == $eselform['tipo'] AND $id == $eselform['id'.$tab] AND $formula == $eselform['formula']) {
                $msg = "Nenhum dado foi alterado, processo não executado!!!";
                header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&idformula=".$idformula."&msg=".$msg);
            }
            else {
                $update = "UPDATE formulas SET nomeformulas='$titulo', tipo='$tipo', id$tab='$id', formula='$formula' WHERE idformulas='$idformula'";
                $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o da formula");
                if($eupdate) {
                    $msg = "Formula alterada com sucesso!!!";
                    header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&idformula=".$idformula."&msg=".$msg);
                }
                else {
                    $msg = "Erro no processo de alteração da formula, favor contatar o administrador!!!";
                    header("location:/monitoria_supervisao/admin/admin.php?menu=formulas&idformula=".$idformula."&msg=".$msg);
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $idformula = $_POST['id'];
    $delete =  "DELETE FROM formulas WHERE idformulas='$idformula'";
    $edelete = $_SESSION['query']($delete) or die ("erro na query para apagar a linha");
    if($edelete) {
        $msg = "FORMULA apagada com sucesso!!!";
        header("location:admin.php?menu=formulas&msg=".$msg);
    }
    else {
        $msg = "Erro no processo para apagar a formula, favor contatar o admnistrador!!!";
        header("location:admin.php?menu=formulas&msg=".$msg);
    }
}

if(isset($_POST['novo'])) {
    header("location:admin.php?menu=formulas");
}

?>
