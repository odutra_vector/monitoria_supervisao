<?php

session_start();
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$dtini = $_POST['dtini'];
$dtfim = $_POST['dtfim'];

if(isset($_POST['apaga'])) {
    $arq = $_POST['arq'];
    unlink($rais."/monitoria_supervisao/logs/".$_SESSION['nomecli']."/$arq");
    if(unlink) {
        ?>
        <script type="text/javascript">
            alert("Arquivo apagado com sucesso");
            window.location = '/monitoria_supervisao/admin/admin.php?menu=log';
        </script>
        <?php
    }
    else {
        ?>
        <script type="text/javascript">
            alert("Erro ao apagar o arquivo");
            window.location = '/monitoria_supervisao/admin/admin.php?menu=log';
        </script>
        <?php
    }
}
else {

    if($dtini != "" && $dtfim != "") {
        $dt = str_replace("/","",$dtini)."-".str_replace("/", "", $dtfim);
        $dtsql = "BETWEEN '".data2banco($dtini)."' and '".data2banco($dtfim)."'";
    }
    else {
        $dt = "semdata";
        $dtsql = "";
    }

    $coldt = array('dimen_estim' => 'datacad','dimen_mod' => 'datacad','ebook' => 'datacad','login_adm' => 'data',
                   'login_web' => 'data','moni_login' => 'data','moni_pausa' => 'data','monitor' => 'datacad','periodo' => 'datacad',
                   'user_adm' => 'datacad','user_web' => 'datacad');
    $tabelas = array('dimen_estim' => 'datacad,horacad,iduser','dimen_mod' => 'datacad,horacad,iduser',
                     'ebook' => 'datacad,horacad,iduser','login_adm' => 'login_adm.iduser_adm,nomeuser_adm,ip,data,hora',
                     'login_web' => 'login_web.iduser_web,nomeuser_web,ip,data,hora','moni_login' => 'moni_login.idmoni_login,nomemonitor,ip,data,login,logout',
                     'moni_pausa' => 'moni_pausa.data,nomemonitor,nomemotivo,moni_pausa.horaini,moni_pausa.horafim,moni_pausa.tempo,lib_super,nomeuser_adm,obs','monitor' => 'idmonitor,nomemonitor,datacad,ativo,senha_ini',
                     'periodo' => 'idperiodo,iduser,tabuser,datacad,hora','user_adm' => 'iduser_adm,nomeuser_adm,ativo,senha_ini,datacad,dataalt,horaalt',
                     'user_web' => 'iduser_web,nomeuser_web,ativo,senha_ini,datacad,dataalt,horaalt');
    foreach($tabelas as $tab => $cols) {
        $colunas = explode(",",$cols);
        $name = $tab."-".$dt."-".date('dmY');
        
        if(file_exists($rais."/monitoria_supervisao/logs/".$_SESSION['nomecli'])) {
        }
        else {
            $cria = mkdir($rais."/monitoria_supervisao/logs/".$_SESSION['nomecli']);
        }
        if(file_exists($rais."/monitoria_supervisao/logs/".$_SESSION['nomecli']."/$name")) {
            unlink($rais."/monitoria_supervisao/logs/".$_SESSION['nomecli']."/$name");
        }
        else {
        }
        $arq = fopen($rais."/monitoria_supervisao/logs/".$_SESSION['nomecli']."/$name.csv", "w");
        $es = fwrite($arq,implode(";",$colunas)."\r\n");
        if($tab == "login_web" OR $tab == "login_adm" OR $tab == "moni_login") {
            $tabuser = array('login_adm' => 'user_adm','login_web' => 'user_web','moni_login' => 'monitor');
            $inner = "INNER JOIN $tabuser[$tab] ON $tabuser[$tab].id$tabuser[$tab]=$tab.id$tabuser[$tab]";
        }
        else {
            if($tab == "moni_pausa") {
                $inner = "INNER JOIN motivo ON motivo.idmotivo = moni_pausa.idmotivo INNER JOIN user_adm ON user_adm.iduser_adm = moni_pausa.iduser_adm INNER JOIN monitor ON monitor.idmonitor = moni_pausa.idmonitor";
            }
            else {
                $inner = "";
            }
        }
        if($dtini != "" && $dtfim != "") {
            $seltab = "SELECT $cols FROM $tab $inner WHERE $coldt[$tab] $dtsql";
        }
        else {
            $seltab = "SELECT $cols FROM $tab $inner";
        }
        $eseltab = $_SESSION['query']($seltab) or die ("erro na query de consulta das tabelas");
        while($lseltab = $_SESSION['fetch_array']($eseltab)) {
            $linha = array();
            foreach($colunas as $col) {
                $part = explode(".",$col);
                if(count($part) == 1) {
                    if(eregi("data",$part[0])) {
                        $linha[] = banco2data($lseltab[$part[0]]);
                    }
                    else {
                        $linha[] = $lseltab[$part[0]];
                    }
                }
                else {
                    if(eregi("data",$part[1])) {
                        $linha[] = banco2data($lseltab[$part[1]]);
                    }
                    else {
                        $linha[] = $lseltab[$part[1]];
                    }
                }
            }
            $es = fwrite($arq,implode(";",$linha)."\r\n");
        }
        chmod($name, 0777);
        fclose($arq);
    }
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        $.unblockUI();
    })
</script>
