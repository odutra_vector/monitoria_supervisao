<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['cadastra'])) {
    $nome = strtoupper($_POST['nome']);
    $caracter = "[]$[><}{)(:;!?*%&#@]";
    $selfluxo = "SELECT COUNT(*) as result FROM fluxo WHERE nomefluxo='$nome'";
    $efluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die (mysql_error());
    if($nome == "") {
        $msg = "O campo ''Nome'' deve estar preenchido para efetuar cadastro!!!";
        header("location:admin.php?menu=estrufluxo&msg=".$msg);
    }
    else {
        if($efluxo['result'] >= 1) {
            $msg = "O nome do Fluxo já está cadastrado na base de dados, favor escolher outro!!!";
            header("location:admin.php?menu=estrufluxo&msg=".$msg);
        }
        else {
            if(eregi($caracter, $nome)) {
                $msg = "O nome do ''Fluxo'' não pode conter caracter inválidos ''$caracter''!!!";
                header("location:admin.php?menu=estrufluxo&msg=".$msg);
            }
            else {
                $cad = "INSERT INTO fluxo (nomefluxo, atvfluxo) VALUE ('$nome', 'S')";
                $ecad = $_SESSION['query']($cad) or die (mysql_error());
                if($ecad) {
                    $msg = "''Fluxo'' cadastrado com sucesso!!!";
                    header("location:admin.php?menu=estrufluxo&msg=".$msg);
                }
                else {
                    $msg = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                    header("location:admin.php?menu=estrufluxo&msg=".$msg);
                }
            }
        }
     }
}

if(isset($_POST['altera'])) {
    $pesq = $_POST['pesquisa'];
    $nome = strtoupper($_POST['nome']);
    $id = $_POST['idfluxo'];
    $atv = $_POST['ativo'];
    $selfluxo = "SELECT COUNT(*) as result FROM fluxo WHERE nomefluxo='$nome' AND idfluxo<>$id";
    $efluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die (mysql_error());
    $verif = "SELECT * FROM fluxo WHERE idfluxo='$id'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die (mysql_error());
    if($nome == "") {
        $msgf = "O campo ''nome'' não pode estar vazio!!!";
        header("location:admin.php?menu=estrufluxo&id=".$id."&pesquisa=".$pesq."&msgf=".$msgf);
     }
     else {
          if($efluxo['result'] >= 1) {
             $msgf = "Já existe fluxo cadastrado com o mesmo nome!!!";
             header("location:admin.php?menu=estrufluxo&id=".$id."&pesquisa=".$pesq."&msgf=".$msgf);
          }
          else {
               if($nome == $everif['nomefluxo'] AND $atv == $everif['atvfluxo']) {
                  $msgf = "Nada foi alterado, processo não executado!!!";
                  header("location:admin.php?menu=estrufluxo&id=".$id."&pesquisa=".$pesq."&msgf=".$msgf);
               }
               else {
                    $alter = "UPDATE fluxo SET nomefluxo='$nome', atvfluxo='$atv' WHERE idfluxo='$id'";
                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                    if($ealter) {
                        $msgf = "Fluxo alterado com sucesso!!!";
                        header("location:admin.php?menu=estrufluxo&id=".$id."&pesquisa=".$pesq."&msgf=".$msgf);
                    }
                    else {
                        $msgf = "Erro durante o processo de alteraçã, favor contatar a administraçã!!!";
                        header("location:admin.php?menu=estrufluxo&id=".$id."&pesquisa=".$pesq."&msgf=".$msgf);
                    }
               }
          }
     }
}

if(isset($_POST['apaga'])) {
    $pesq = $_POST['pesquisa'];
    $idfluxo = $_POST['idfluxo'];

    $veriffluxo = "SELECT COUNT(*) as result FROM monitoria_fluxo mf INNER JOIN monitoria m ON m.idmonitoria = mf.idmonitoria
                   INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros INNER JOIN rel_fluxo rf ON rf.idfluxo = cr.idfluxo
                   WHERE rf.idfluxo = '$idfluxo'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($veriffluxo)) or die ("erro na query para consulta se o fluxo jÃ¡ possui monitoria associada");
    $alter = "ALTER TABLE fluxo AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query para corrigir auto increment");
    $vconffluxo = "SELECT COUNT(*) as result FROM conf_rel WHERE idfluxo='$idfluxo'";
    $evconf = $_SESSION['fetch_array']($_SESSION['query']($vconffluxo)) or die ("erro na query de consulta de configuraÃ§Ã£o");
    if($everif['result'] >= 1) {
        $msgf = "O fluxo não pode ser apagado pois já existem monitorias realizadas vinculadas ao fluxo!!!";
	header("location:admin.php?menu=estrufluxo&id=".$idfluxo."&pesquisa=".$pesq."&msgf=".$msgf);
    }
    else {
        if($evconf['result'] >= 1) {
            $msgf = "O fluxo não pode ser apagado pois existem relacionamentos configurados com este fluxo, favor verificar!!!";
            header("location:admin.php?menu=estrufluxo&id=".$idfluxo."&pesquisa=".$pesq."&msgf=".$msgf);
        }
        else {
            $del = "DELETE FROM fluxo WHERE idfluxo='$idfluxo'";
            $edel = $_SESSION['query']($del) or die ("erro na query para apagar o fluxo desejado");
            if($del) {
                $msg = "FLuxo apagado com sucesso!!!";
                header("location:admin.php?menu=estrufluxo&id=".$idfluxo."&pesquisa=".$pesq."&msg=".$msgf);
            }
            else {
                $msg = "Erro na execussão do processo, favor contatar o administrador!!!";
                header("location:admin.php?menu=estrufluxo&id=".$idfluxo."&pesquisa=".$pesq."&msg=".$msgf);
            }
        }
    }
}
?>