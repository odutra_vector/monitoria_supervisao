<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['cadastra'])) {
    $nome = strtoupper($_POST['nome']);
    $acesso = $_POST['acesso'];
    $tipo = $_POST['tipo'];
    if($_POST['qtde'] == "") {
        $qtde = "NULL";
    }
    else {
        $qtde = "'".$_POST['qtde']."'";
    }
    $tempo = $_POST['tempo'];
    $caracter = "[]$[><}{)(:;!?*%&#@]";
    $selmotivo = "SELECT COUNT(*) as result FROM motivo WHERE nomemotivo='$nome'";
    $emotivo = $_SESSION['fetch_array']($_SESSION['query']($selmotivo)) or die (mysql_error());
    $sqltipo = "SELECT vincula FROM tipo_motivo WHERE idtipo_motivo='$tipo'";
    $esqltipo = $_SESSION['fetch_array']($_SESSION['query']($sqltipo)) or die ("erro na query de consulta do tipo do motivo");
    if($nome == "" OR $tipo == "") {
        $msgm = "O campo ''NOME ou o TIPO'' não podem estar vazios!!!";
        header("location:admin.php?menu=motivo&msgm=".$msgm);
    }
    else {
        if($esqltipo['vincula'] == "automatico" && $tempo == "") {
            $msgm = "Quando o motivo for automatico, o tempo não pode ser vazio ou 0 !!!";
            header("location:admin.php?menu=motivo&msgm=".$msgm);
        }
        else {
            if(eregi($caracter, $nome)) {
                $msgm = "No ''Nome'' não poder existir caracter inválido ''$caracter''!!!";
                header("location:admin.php?menu=motivo&msgm=".$msgm);
            }
            else {
                if($emotivo['result'] >= 1) {
                    $msgm = "Este ''Nome'' já está cadastrado para um Motivo!!!";
                    header("location:admin.php?menu=motivo&msgm=".$msgm);
                } 
                else {
                    $cad = "INSERT INTO motivo (nomemotivo, idtipo_motivo, acesso,ativo, qtde, tempo) VALUE ('$nome', '$tipo','$acesso','S', $qtde, '$tempo')";
                    $ecad = $_SESSION['query']($cad) or die (mysql_error());
                    if($ecad) {
                        $msgm = "Cadastrao efetuado com sucesso!!!";
                        header("location:admin.php?menu=motivo&msgm=".$msgm);
                    }
                    else {
                        $msgm = "Ocorreu algum erro no momento da cadastramento, favor contatar o administrador!!!";
                        header("location:admin.php?menu=motivo&msgm=".$msgm);
                    }
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
  $id = $_POST['id'];
  $selmotivo = "SELECT * FROM motivo m 
                INNER JOIN tipo_motivo tp ON tp.idtipo_motivo = m.idtipo_motivo WHERE m.idmotivo='$id'";
  $emotivo = $_SESSION['fetch_array']($_SESSION ['query']($selmotivo)) or die ("erro na query de consutla do motivo");
  if($emotivo['nomemotivo'] == "MONITORAR" OR $emotivo['nomemotivo'] == "INICIAR") {
    $msgmi = "Este motivo não pode ser apagado, pois já foi utilizado na configuração do sistema";
    header("location:admin.php?menu=motivo&msgmi=".$msgmi);
  }
  else {
    if($emotivo['vincula'] == "regmonitoria") {
      $selreg = "SELECT COUNT(*) as result FROM regmonitoria WHERE idmotivo='$id'";
      $eselreg = $_SESSION['fetch_array']($_SESSION['query']($selreg)) or die ("erro na query de consulta dos registros");
      if($eselreg['result'] >= 1) {
        $msgmi = "Este motivo não pode ser apagado pois já existe registro relacionado a ele!!!";
        header("location:admin.php?menu=motivo&msgi=".$msgmi);
      }
      else {
        $del = "DELETE FROM motivo WHERE idmotivo='$id'";
        $edel = $_SESSION['query']($del) or die ("erro na query que apaga o motivo");
        $alter = "ALTER TABLE motivo AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die ("erro na query de alteraÃ§Ã£o do auto incremento");
        if($edel) {
          $msgmi = "Moitovo apagado com sucesso!!!";
          header("location:admin.php?menu=motivo&msgmi=".$msgmi);
        }
        else {
          $msgmi = "Erro no processo para apagar o motivo, favor contatar o administrador!!!";
          header("location:admin.php?menu=motivo&msgmi=".$msgmi);
        }
      }
    }

    if($emotivo['vincula'] == "moni_pausa") {
      $selreg = "SELECT COUNT(*) as result FROM moni_pausa WHERE idmotivo='$id'";
      $eselreg = $_SESSION['fetch_array']($_SESSION['query']($selreg)) or die ("erro na query de consulta dos registro");
      if($eselreg['result'] >= 1) {
        $msgmi = "Este motivo não pode ser apagado pois já existe registro relacionado a ele!!!";
        header("location:admin.php?menu=motivo&msgi=".$msgmi);
      }
      else {
        $del = "DELETE FROM motivo WHERE idmotivo='$id'";
        $edel = $_SESSION['query']($del) or die ("erro na query que apaga o motivo");
        if($edel) {
          $msgmi = "Moitov apagado com sucesso!!!";
          header("location:admin.php?menu=motivo&msgmi=".$msgmi);
        }
        else {
          $msgmi = "Erro no processo para apagar o motivo, favor contatar o administrador!!!";
          header("location:admin.php?menu=motivo&msgmi=".$msgmi);
        }
      }
    }
  }
}

if(isset($_POST['altera'])) {
  $nome = strtoupper($_POST['nome']);
  $id = $_POST['id'];
  $tipo = strtoupper($_POST['tipo']);
  $acesso = $_POST['acesso'];
  if($_POST['qtde'] == "") {
      $qtde = "NULL";
  }
  else {
    $qtde = "'".$_POST['qtde']."'";
  }
  if($_POST['tempo'] == "") {
  	$tempo = "NULL";
  }
  else {
	$tempo = "'".$_POST['tempo']."'";
  }
  $atv = $_POST['ativo'];
  $motivo = "SELECT * FROM motivo WHERE idmotivo='$id'";
  $emo = $_SESSION['fetch_array']($_SESSION['query']($motivo)) or die (mysql_error());
  $selmotivo = "SELECT COUNT(*) as result FROM motivo WHERE nomemotivo='$nome' AND idtipo_motivo='$tipo' AND idmotivo<>$id";
  $emotivo = $_SESSION['fetch_array']($_SESSION['query']($selmotivo)) or die (mysql_error());
  if($nome == "") {
    $msgmi = "O campo ''Nome'' nÃ£o pode estar vazio!!!";
    header("location:admin.php?menu=motivo&msgmi=".$msgmi);
  }
  else {
    if($_POST['tempo'] != "" && !is_numeric($_POST['tempo'])) {
        $msgmi = "O campo ''Tempo'' só pode conter números ou ser vazio!!!";
        header("location:admin.php?menu=motivo&msgmi=".$msgmi);
    }
    else {
        if($nome == $emo['nomemotivo'] && $tipo == $emo['idtipo_motivo'] && $acesso == $emo['acesso'] && $atv == $emo['ativo'] && $_POST['qtde'] == $emo['qtde'] && $_POST['tempo'] == $emo['tempo']) {
          $msgmi = "Não houve nenhuma alteração, processo não efetuado!!!";
          header("location:admin.php?menu=motivo&msgmi=".$msgmi);
        }
        else {
            if($emotivo['result'] >= 1) {
              $msgmi = "Este nome já está cadastrado!!!";
              header("location:admin.php?menu=motivo&msgmi=".$msgmi);
            }
            else {
              $alt = "UPDATE motivo SET nomemotivo='$nome', idtipo_motivo='$tipo', acesso='$acesso', ativo='$atv', qtde=$qtde, tempo=$tempo WHERE idmotivo='$id'";
              $ealt = $_SESSION['query']($alt) or die (mysql_error());
              if($ealt) {
                $msgmi = "Cadastro alterado com sucesso!!!";
                header("location:admin.php?menu=motivo&msgmi=".$msgmi);
              }
              else {
                $msgmi = "Erro no processamento da solicitaÃ§Ã£o, favor contatar o administrador!!!";
                header("location:admin.php?menu=motivo&msgmi=".$msgmi);
              }
            }
        }
    }
  }
}
?>