<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');

$tab = new TabelasSql();

$id = mysql_real_escape_string($_GET['id']);
$selmoni = "SELECT * FROM monitor WHERE idmonitor='$id'";
$emoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_error());
$cpf1 = substr($emoni['CPF'], 0, 3);
$cpf2 = substr($emoni['CPF'], 3, 3);
$cpf3 = substr($emoni['CPF'], 6, 3);
$cpfdig = substr($emoni['CPF'], 9, 2);
$cpf = $cpf1.".".$cpf2.".".$cpf3."-".$cpfdig;
$entra = explode(":", $emoni['entrada']);
$saida = explode(':', $emoni['saida']);
$filtro = "SELECT nomefiltro_nomes, idfiltro_nomes FROM filtro_nomes ORDER BY nivel LIMIT 3";
$efiltro = $_SESSION['query']($filtro) or die ("erro na query para consulta filtro");
$nfiltro = $_SESSION['num_rows']($efiltro);
if($nfiltro >= 1) {
    while($lfiltro = $_SESSION['fetch_array']($efiltro)) {
        $filtros[$lfiltro['idfiltro_nomes']] = $lfiltro['nomefiltro_nomes'];
    }
}
$filtros = array_reverse($filtros);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem tÃ­tulo</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.4.4.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $("#hentra").mask("99:99");
   $("#hsaida").mask("99:99");
   $("#cpf").mask("999.999.999-99");
   
   $('#edita').click(function() {
       var hentra = $('#hentra').val();
       var hsaida = $('#hsaida').val();
       if(hentra == hsaida) {
           alert('A hora de entrada não pode ser igual a hora de saida');
           return false;
       }
       else {
           var entrah = hentra.substr(0, 2);
           var entram = hentra.substr(3, 2);
           var saidah = hsaida.substr(0, 2);
           var saidam = hsaida.substr(3, 2);
           if(entrah > saidah) {
               alert('A hora de entrada não pode ser maior que a hora de saida');
               return false;
           }
           else {
               if(entrah == saidah && entram > saidam) {
                   alert('A hora de entrada não pode ser maior que a hora de saida');
                   return false;
               }
               else {
               }
           }
       }
   })
})
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadmonitor.php" method="post">
<table width="722" border="0">
  <tr>
    <td class="corfd_coltexto" colspan="6"><strong>CADASTRO DE MONITOR</strong></td>
  </tr>
  <tr>
    <td width="92" bgcolor="#999999"><strong>Nome</strong></td>
    <td width="298" bgcolor="#FFFFFF"><input style="width:250px; border: 1px solid #333" maxlength="100" name="nome" type="text" value="<?php echo $emoni['nomemonitor']; ?>" /><input name="id" type="hidden" value="<?php echo $id; ?>" /></td>
    <td width="74" bgcolor="#999999"><strong>CPF</strong></td>
    <td colspan="3" bgcolor="#FFFFFF">
    <input style="width:100px; border: 1px solid #333; text-align:center" name="cpf" id="cpf" type="text" value="<?php echo $cpf; ?>" /></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><strong>Responsável</strong></td>
    <td bgcolor="#FFFFFF"><select name="resp">
    <?php
	$selresp = "SELECT * FROM user_adm WHERE iduser_adm='".$emoni['iduser_resp']."'";
	$eresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
	echo "<option selected=\"selected\" value=\"".$eresp['iduser_adm']."\">".$eresp['nomeuser_adm']."</option>";
	$respdb = array($eresp['iduser_adm']);
	$opresp = array();
	$seluser = "SELECT * FROM user_adm";
	$euser = $_SESSION['query']($seluser) or die (mysql_error());
	while($luser = $_SESSION['fetch_array']($euser)) {
		array_push($opresp, $luser['iduser_adm']);
	}
	$difresp = array_diff($opresp, $respdb);
	foreach($difresp as $resp) {
		$selnome = "SELECT nomeuser_adm FROM user_adm WHERE iduser_adm='$resp'";
		$enome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
		echo "<option value=\"".$resp."\">".$enome['nomeuser_adm']."</option>";
	}
	?>
    </select></td>
    <td bgcolor="#999999"><strong>Hora Ent.</strong></td>
    <td width="90" bgcolor="#FFFFFF"><input style="width:50px; border: 1px solid #333; text-align:center" maxlength="5" name="hentra" id="hentra" type="text" value="<?php echo $entra['0'].":".$entra['1']; ?>" /></td>
    <td width="90" bgcolor="#999999"><strong>Hora Saida</strong></td>
    <td width="52" bgcolor="#FFFFFF"><input style="width:50px; border: 1px solid #333; text-align:center" maxlength="5" name="hsaida" id="hsaida" type="text" value="<?php echo $saida['0'].":".$saida['1']; ?>" /></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><strong>Usuário</strong></td>
    <td bgcolor="#FFFFFF"><input style="width:100px; border: 1px solid #333; text-align:center" maxlength="20" name="user" type="text" value="<?php echo $emoni['usuario']; ?>" /></td>
    <td bgcolor="#999999"><strong>Ativo</strong></td>
    <td bgcolor="#FFFFFF"><select name="ativo"><option value="<?php echo $emoni['ativo']; ?>"><?php echo $emoni['ativo']; ?></option>
    <?php
	    $atvdb = array($emoni['ativo']);
		$opatv = array('S','N');
		$difatv = array_diff($opatv, $atvdb);
    	foreach($difatv as $atv) {
			echo "<option value=\"".$atv."\">".$atv."</option>";
		}
	?>
    </select></td>
    <td bgcolor="#999999"><strong>Senha Ini.</strong></td>
    <td bgcolor="#FFFFFF"><select name="senha_ini"><option value="<?php echo $emoni['senha_ini']; ?>"><?php echo $emoni['senha_ini']; ?></option>
    <?php
	    $senhadb = array($emoni['senha_ini']);
		$opsenha = array('S','N');
		$difsenha = array_diff($opsenha, $senhadb);
    	foreach($difsenha as $senha) {
			echo "<option value=\"".$senha."\">".$senha."</option>";
		}
	?>
    </select></td>
  </tr>
  <tr>
  	<td class="corfd_coltexto"><strong>Tipo Monitor</strong></td>
    <td class="corfd_colcampos">
    	<select name="tmonitor" id="tmonitor">
    	<?php
          $tmoni = array('I','E');
          foreach($tmoni as $t) {
               if($t == $emoni['tmonitor']) {
                    echo "<option value=\"$t\" selected=\"selected\">$t</option>";	
               }
               else {
                    echo "<option value=\"$t\">$t</option>";
               }
          }
          ?>
        </select>
    </td>
  </tr>
  <?php
  if(isset($_GET['altera'])) {
	  echo "<tr>";
	  	echo "<td colspan=\"6\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"edita\" id=\"edita\" type=\"submit\" value=\"Alterar\" />";
		echo " <input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"reset\" type=\"submit\" value=\"Reset Senha\" /> <input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"canced\" type=\"submit\" value=\"Cancelar\" /></td>";
	  echo "</tr>";
  }
  if(isset($_GET['dimen'])) {
	  echo "<td colspan=\"6\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"volta\" type=\"submit\" value=\"Voltar\" /></td>";
  }
  ?>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font><hr /><br />
<div style="width:1024px;">
<table width="1024">
  <tr>
    <td class="corfd_coltexto" align="center" colspan="7"><strong>META X REALIZADO</strong></td>
  </tr>
  <tr>
    <td width="466" bgcolor="#999999"><strong><?php echo $efiltro['nomefiltro_nomes']; ?></strong></td>
    <td width="90" align="center" bgcolor="#999999"><strong>META MES</strong></td>
    <td width="90" align="center" bgcolor="#999999"><strong>META SEMANA</strong></td>
    <td width="90" align="center" bgcolor="#999999"><strong>META DIA</strong></td>
    <td width="90" align="center" bgcolor="#999999"><strong>REALIZADO MES</strong></td>
    <td width="90" align="center" bgcolor="#999999"><strong>REALIZADO SEMANA</strong></td>
    <td width="90" align="center" bgcolor="#999999"><strong>REALIZADO DIA</strong></td>
  </tr>
</table>
</div>
  <?php
  $data = date("d/m/Y");
  $dtatu_dia = substr($data,0,2);
  $dtatu_mes = substr($data,3,2);
  $dtatu_ano = substr($data,6,4);
  $dtatu = mktime(0,0,0,$dtatu_mes, $dtatu_dia,$dtatu_ano);
  $periodo = periodo();
  echo "<div style=\"width:1024px; height:200px; overflow:auto\">";
  echo "<table width=\"1024\">";
  $semana = array('1' => 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thursday','5' => 'Friday','6' => 'Saturday', '7' => 'Sunday');
  $dia = jddayofweek(cal_to_jd(CAL_GREGORIAN, $dtatu_mes, $dtatu_dia, $dtatu_ano));
  foreach($semana as $num => $dias) {
    if($dia == $num) {
      $numero = $num;
    }
    else {
    }
  }
  for($i = 1; $i < $numero; $i++) {
    $dtatu -= 86400;
  }
  if($i == $numero) {
  	$dtatu = data2banco($data);
  }
  if($i != $numero) {
  	$dtatu = date('Y-m-d', $dtatu);
  }
  $ultdiames = ultdiames();
  $pdiames = $dtatu_ano."-".$dtatu_mes."-01";
  $filtro = strtolower($efiltro['nomefiltro_nomes']);
  $adimemoni = $tab->AliasTab(dimen_moni);
  $arel = $tab->AliasTab(rel_filtros);
  $seldimen = "SELECT $adimemoni.idrel_filtros, $adimemoni.iddimen_mod, $adimemoni.idmonitor, $adimemoni.idperiodo, $adimemoni.meta_m, $adimemoni.meta_s, $adimemoni.meta_d
	      FROM dimen_moni $adimemoni
	      INNER JOIN rel_filtros $arel ON $arel.idrel_filtros = $adimemoni.idrel_filtros
	      WHERE $adimemoni.idmonitor = '$id' AND $adimemoni.idperiodo = '$periodo' AND $adimemoni.ativo='S'";
  $edimen = $_SESSION['query']($seldimen) or die ("erro na execusÃ£o da query de consulta do dimensionamento cadastrado");
  while($ldimen = $_SESSION['fetch_array']($edimen)) {
          $ndados = array();
          $selrel = "SELECT * FROM rel_filtros WHERE idrel_filtros='".$ldimen['idrel_filtros']."'";
          $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relacionamento");
          foreach($filtros as $ft) {
              $seldados = "SELECT * FROM filtro_dados WHERE idfiltro_dados='".$eselrel['id_'.strtolower($ft)]."'";
              $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta do nome do filtro");
              $ndados[] = $eseldados['nomefiltro_dados'];
          }
	  echo "<tr>";
	  echo "<td width=\"466\" bgcolor=\"#FFFFFF\"><a href=\"/monitoria_supervisao/admin/admin.php?menu=relestrudet&idrel=".$ldimen['idrel_filtros']."\" style=\"text-decoration:none\">".implode(" - ",$ndados)."</a></td>";
	  echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\">".$ldimen['meta_m']."</td>";
	  echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\">".$ldimen['meta_s']."</td>";
	  echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\">".$ldimen['meta_d']."</td>";
	  $realm = "SELECT COUNT(*) as qtdemoni FROM monitoria WHERE idrel_filtros='".$ldimen['idrel_filtros']."' AND idmonitor='".$ldimen['idmonitor']."' AND data BETWEEN '$pdiames' AND '$ultdiames'";
	  $erealm = $_SESSION['fetch_array']($_SESSION['query']($realm)) or die ("erro no processo de somatÃ³ria das monitorias");
	  $reals = "SELECT COUNT(*) as qtdemoni FROM monitoria WHERE idrel_filtros='".$ldimen['idrel_filtros']."' AND idmonitor='".$ldimen['idmonitor']."' AND data BETWEEN '$dtatu' AND '".data2banco($data)."'";
	  $ereals = $_SESSION['fetch_array']($_SESSION['query']($reals)) or die ("erro no processo de somatÃ³ria das monitorias");
	  $reald = "SELECT COUNT(*) as qtdemoni FROM monitoria WHERE idrel_filtros='".$ldimen['idrel_filtros']."' AND idmonitor='".$ldimen['idmonitor']."' AND data='".data2banco($data)."'";
	  $ereald = $_SESSION['fetch_array']($_SESSION['query']($reald)) or die ("erro no processo de somatÃ³ria das monitorias");
	  echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\">".$erealm['qtdemoni']."</td>";
	  echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\">".$ereals['qtdemoni']."</td>";
	  echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\">".$ereald['qtdemoni']."</td>";
	  echo "</tr>";
	  $tometam = $tometam + $ldimen['meta_m'];
	  $tometas = $tometas + $ldimen['meta_s'];
	  $tometad = $tometad + $ldimen['meta_d'];
	  $torealm = $torealm + $erealm['qtdemoni'];
	  $toreals = $toreals + $ereals['qtdemoni'];
	  $toreald = $toreald + $ereald['qtdemoni'];

  }
  echo "</table></div>";
  echo "<div style=\"width:1024px;\">";
  echo "<table width=\"1024\">";
  echo "<tr>";
    echo "<td width=\"466\" bgcolor=\"#FFFFFF\"><strong>TOTAL</strong></td>";
    echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\"><strong>".$tometam."</strong></td>";
    echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\"><strong>".$tometas."</strong></td>";
    echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\"><strong>".$tometad."</strong></td>";
    echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\"><strong>".$torealm."</strong></td>";
    echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\"><strong>".$toreals."</strong></td>";
    echo "<td width=\"90\" bgcolor=\"#FFFFFF\" align=\"center\"><strong>".$toreald."</strong></td>";
  echo "</tr>";
  echo "</table></div>";
  ?>
</div>
</body>
</html>
