<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");

$meses = array('01' => 'JANEIRO','02' => 'FEVEREIRO','03' => 'MARCO','04' => 'ABRIL','05' => 'MAIO','06' => 'JUNHO','07' => 'JULHO','08' => 'AGOSTO','09' => 'SETEMBRO','10' => 'OUTUBRO','11'=> 'NOVEMBRO','12' => 'DEZEMBRO');
$semana = array('Sunday' => 'Domingo','Monday' => 'Segunda','Tuesday' => 'Terca','Wednesday' => 'Quarta','Thursday' => 'Quinta','Friday' => 'Sexta','Saturday' => 'Sabado');
$mes = $_POST['mes'];
$ano = $_POST['ano'];
$numbermes = array_search($mes, $meses);
$dataini = data2banco($_POST['dtinimoni']);
$dtini_ano = substr($dataini, 0, 4);
$dtini_mes = substr($dataini, 5, 2);
$dtini_dia = substr($dataini, 8, 2);
$datafim = data2banco($_POST['dtfimmoni']);
$dtfim_ano = substr($datafim, 0, 4);
$dtfim_mes = substr($datafim, 5, 2);
$dtfim_dia = substr($datafim, 8, 2);
$dtinictt = data2banco($_POST['dtinictt']);
$dtfimctt = data2banco($_POST['dtfimctt']);
$compdatas = "SELECT '$dataini' > '$datafim' as result";
$ecompdatas = $_SESSION['fetch_array']($_SESSION['query']($compdatas));
if($dtinictt != "" && $dtfimctt != "") {
    $compctt = "SELECT '$dtinictt' > '$dtfimctt' as result";
    $ecompctt = $_SESSION['fetch_array']($_SESSION['query']($compctt));
}
$iduser = $_SESSION['usuarioID'];
$data = date('Y-m-d');
$hora = date('H:i:s');
$dtiniinter = mktime(0,0,0,$dtini_mes,$dtini_dia,$dtini_ano); // timestamp da data inicial para cadastro do intervalo de datas
$dtfiminter = mktime(0,0,0,$dtfim_mes,$dtfim_dia,$dtfim_ano); // timestamp da data final para cadastro do intervalo de datas
$dtini = mktime(0,0,0,$dtini_mes,$dtini_dia,$dtini_ano); // timestamp da data inicial
$dtfim = mktime(0,0,0,$dtfim_mes,$dtfim_dia,$dtfim_ano); // timestamp da data final
$dias = 0;
if(isset($_POST['cadastra'])) {
    if($dataini =="" || $datafim == "") {
        $msg = "As datas inicial e final precisam estar preenchidas para cadastros do perÃ­odo!!!";
        header("location:admin.php?menu=calendario&msg=".$msg);
    }
    else {
        if(verifdata($_POST['dtinimoni']) != true || verifdata($_POST['dtfimmoni']) != true) {
            $msg = "As datas informadas nÃ£o sÃ£o vÃ¡lidas!!!";
            header("location:admin.php?menu=calendario&msg=".$msg);
        }
        else {
            if($ecompdatas['result'] >= 1 OR $ecompctt['result'] >= 1) {
                $msg = "A data final não pode ser menor que a data inicial, tanto no período quando na data do contato!!!";
                header("location:admin.php?menu=calendario&msg=".$msg);
            }
            else {
                $intervaldt = array();
                $sdias = "SELECT DATEDIFF('$datafim','$dataini') as dias";
                $edias = $_SESSION['fetch_array']($_SESSION['query']($sdias)) or die ("erro na query de consulta dos dias do perÃ­odo");
                $dias = $edias['dias'] + 1;
                $cad = 0;
                $comparadt = array();
                $countcalen = "SELECT COUNT(*) as result FROM periodo";
                $eselcount = $_SESSION['fetch_array']($_SESSION['query']($countcalen)) or die (mysql_error());
                if($eselcount['result'] >= 1) {
                    $selcalen = "SELECT nmes,ano,dataini, datafim FROM periodo";
                    $eselcalen = $_SESSION['query']($selcalen) or die (mysql_error());
                    while($lselcalen = $_SESSION['fetch_array']($eselcalen)) {
                        if($lselcalen['nmes'] == $mes && $lselcalen['ano'] == $ano) {
                            $cad++;
                        }
                        $dtinidb_ano = substr($lselcalen['dataini'],0,4);
                        $dtinidb_mes = substr($lselcalen['dataini'],5,2);
                        $dtinidb_dia = substr($lselcalen['dataini'],8,2);
                        $dtfimdb_ano = substr($lselcalen['datafim'],0,4);
                        $dtfimdb_mes = substr($lselcalen['datafim'],5,2);
                        $dtfimdb_dia = substr($lselcalen['datafim'],8,2);
                        $dinidb = mktime(0,0,0,$dtinidb_mes, $dtinidb_dia, $dtinidb_ano);
                        $dfimdb = mktime(0,0,0,$dtfimdb_mes, $dtfimdb_dia, $dtfimdb_ano);
                        $intervaldtdb[] = date('Y-m-d',$dinidb);
                        while($dinidb <= $dfimdb) {
                            if(in_array(date('Y-m-d',$dinidb), $intervaldt)) {
                                $comparadt[] = date('Y-m-d',$dinidb);
                            }
                            $dinidb += 86400;
                            $intervaldtdb[] = date('Y-m-d',$dinidb);
                        }
                    }
                }
                if(!empty($comparadt) OR $cad >= 1) {
                    $msg = "Já existe período cadastrado com as datas informadas ou o Mês e Ano já estão cadastrados!!!";
                    header("location:admin.php?menu=calendario&msg=".$msg);
                }
                else {
                    if($dtinictt != "" && $dtfimctt != "") {
                        $cadcalen = "INSERT INTO periodo (mes, nmes, ano, dataini, datafim, dtinictt,dtfimctt,dias, diasprod, datacad, hora, iduser, tabuser) VALUE ('$numbermes', '$mes', '$ano', '$dataini', '$datafim','$dtinictt','$dtfimctt','$dias', '$dias', '$data', '$hora', '$iduser', '".$_SESSION['user_tabela']."')";
                    }
                    else {
                        $cadcalen = "INSERT INTO periodo (mes, nmes, ano, dataini, datafim, dias, diasprod, datacad, hora, iduser, tabuser) VALUE ('$numbermes', '$mes', '$ano', '$dataini', '$datafim', '$dias', '$dias', '$data', '$hora', '$iduser', '".$_SESSION['user_tabela']."')";
                    }
                    $ecadcalen = $_SESSION['query']($cadcalen) or die (mysql_error());
                    $idperiodo = $_SESSION['insert_id']();
                    //$semana = array('Sunday' => 'Domingo','Monday' => 'Segunda','Tuesday' => 'Terca','Wednesday' => 'Quarta','Thursday' => 'Quinta','Friday' => 'Sexta','Saturday' => 'Sabado');
                    //$dias = array('Segunda' => '0', 'Terca' => '1','Quarta' => '2','Quinta' => '3','Sexta' => '4','Sabado' => '5','Domingo' => '6');
                    InsertSemana($dtiniinter, $dtfiminter, $datafim, $idperiodo);

                    if($ecadcalen) {
                        $msg = "Cadastro efetuado com sucesso!!!";
                        header("location:admin.php?menu=calendario&msg=".$msg);
                    }
                    else {
                        $msg = "Erro durante o cadastro, favor contatar o administrador!!!";
                        header("location:admin.php?menu=calendario&msg=".$msg);
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $idperiodo = $_POST['id'];
    $mes = $_POST['nmes'];
    $numbermes = array_search($mes, $meses);
    $selperiodo = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
    $eselper = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_error());
    if($dataini =="" || $datafim == "") {
        $msgi = "As datas inicial e final precisam estar preenchidas para altração do período!!!";
        header("location:admin.php?menu=calendario&msgi=".$msgi);
    }
    else {
        if($ecompdatas['result'] >= 1 OR $ecompctt['result'] >= 1) {
          $msgi = "A data final não pode ser menor que a data inicial, tanto no período quando na data do contato!!!";
          header("location:admin.php?menu=calendario&msgi=".$msgi);
        }
        else {
            $intervaldt = array();
            $sdias = "SELECT DATEDIFF('$datafim','$dataini') as dias";
            $edias = $_SESSION['fetch_array']($_SESSION['query']($sdias)) or die ("erro na query de consulta dos dias do perÃ­odo");
            $dias = $edias['dias'] + 1;
            $cad = 0;
            $comparadt = array();
            $countcalen = "SELECT COUNT(*) as result FROM periodo WHERE idperiodo<>$idperiodo";
            $eselcount = $_SESSION['fetch_array']($_SESSION['query']($countcalen)) or die (mysql_error());
            if($eselcount['result'] >= 1) {
                $selcalen = "SELECT nmes,ano,dataini, datafim FROM periodo WHERE idperiodo<>$idperiodo";
                $eselcalen = $_SESSION['query']($selcalen) or die (mysql_error());
                while($lselcalen = $_SESSION['fetch_array']($eselcalen)) {
                    if($mes == $lselcalen['nmes'] && $lselcalen['ano'] == $eselper['ano']) {
                        $cad++;
                    }
                    $dtinidb_ano = substr($lselcalen['dataini'],0,4);
                    $dtinidb_mes = substr($lselcalen['dataini'],5,2);
                    $dtinidb_dia = substr($lselcalen['dataini'],8,2);
                    $dtfimdb_ano = substr($lselcalen['datafim'],0,4);
                    $dtfimdb_mes = substr($lselcalen['datafim'],5,2);
                    $dtfimdb_dia = substr($lselcalen['datafim'],8,2);
                    $dinidb = mktime(0,0,0,$dtinidb_mes, $dtinidb_dia, $dtinidb_ano);
                    $dfimdb = mktime(0,0,0,$dtfimdb_mes, $dtfimdb_dia, $dtfimdb_ano);
                    $intervaldtdb = array($dinidb);
                    while($dinidb <= $dfimdb) {
                      if(in_array($dinidb, $intervaldt)) {
                          $comparadt[date('Y-m-d',$dinidb)] = $dinidb;
                      }
                      $dinidb += 86400;
                      array_push($intervaldtdb, $dinidb);
                    }
                }
            }
            if(!empty($comparadt) OR $cad >= 1) {
              $msgi = "Já existe período cadastrado com as datas informadas ou o Mês e Ano já estão cadastrados!!!";
              header("location:admin.php?menu=calendario&msgi=".$msgi);
            }
            else {
                if($numbermes == $eselper['mes'] && $mes == $eselper['nmes'] && $dataini == $eselper['dataini'] && 
                   $datafim == $eselper['datafim'] && $dias == $eselper['dias'] && $dtinictt == $eselper['dtinictt'] && $dtfimctt == $eselper['dtfimctt']) {
                   $msgi = "Nenhum dado foi alterado, processo não executado!!!";
                   header("location:admin.php?menu=calendario&msgi=".$msgi);
                }
                else {
                    if($dtinictt == "" || $dtfimctt == "") {
                        $dtinictt = "NULL";
                        $dtfimctt = "NULL";
                    }
                    else {
                        $dtinictt = val_vazio($dtinictt);
                        $dtfimctt = val_vazio($dtfimctt);
                    }
                   $alter = "UPDATE periodo SET mes='$numbermes', nmes='$mes', dataini='$dataini', datafim='$datafim', dtinictt=$dtinictt,dtfimctt=$dtfimctt,dias='$dias', iduser='$iduser', tabuser='".$_SESSION['user_tabela']."' WHERE idperiodo='$idperiodo'";
                   $ealter = $_SESSION['query']($alter) or die (mysql_error());
                   $countsem = "SELECT MIN(dataini) as dtinimin, MAX(datafim) as dtfimmax, COUNT(*) as result FROM interperiodo WHERE idperiodo='$idperiodo'";
                   $ecountsem = $_SESSION['query']($countsem) or die ("erro na query de consulta da quantidade de semanadas cadastradas");
                   while($lcountsem = $_SESSION['fetch_array']($ecountsem)) {
                       if($lcountsem['result'] >= 1) {
                           $delinter = "DELETE FROM interperiodo WHERE idperiodo='$idperiodo'";
                           $edelinter = $_SESSION['query']($delinter) or die ("erro na query para apagar o intervalo");
                           $alter = "ALTER TABLE interperiodo AUTO_INCREMENT=1";
                           $altsem = InsertSemana($dtiniinter, $dtfiminter, $datafim , $idperiodo);
                       }
                       else {
                           //$dtiniinter = mktime(0,0,0,substr($dataini,5,2),substr($dataini,8,2),substr($dataini,0,4));
                           //$dtfiminter = mktime(0,0,0,substr($datafim,5,2),substr($datafim,8,2),substr($datafim,0,4));
                           $altsem = InsertSemana($dtiniinter, $dtfiminter, $datafim , $idperiodo);
                       }
                   }
                   if($ealter) {
                      $msgi = "Alteração realizada com sucesso!!!";
                      header("location:admin.php?menu=calendario&msgi=".$msgi);
                   }
                   else {
                      $msgi = "Erro no processo de alteração, favor contatar o administrador!!!";
                      header("location:admin.php?menu=calendario&msgi=".$msgi);
                   }
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $idperiodo = $_POST['id'];
    $mes = $_POST['nmes'];
    $numbermes = array_search($mes, $meses);
    $intervaldt = array();
    while($dtini <= $dtfim) {
      $dias[] = $dias++;
      $dtini += 86400;
      $intervaldt[] = $dtini;
    }
    $comparadt = array();
    $verifup = "SELECT p.dataini, p.datafim FROM upload u 
                INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                WHERE u.idperiodo='$idperiodo'";
    $everifup = $_SESSION['query']($verifup) or die (mysql_error());
    while($lverifup = $_SESSION['fetch_array']($everifup)) {
        $dtinidb_ano = substr($lverifup['dataini'],0,4);
        $dtinidb_mes = substr($lverifup['dataini'],5,2);
        $dtinidb_dia = substr($lverifup['dataini'],8,2);
        $dtfimdb_ano = substr($lverifup['datafim'],0,4);
        $dtfimdb_mes = substr($lverifup['datafim'],5,2);
        $dtfimdb_dia = substr($lverifup['datafim'],8,2);
        $dinidb = mktime(0,0,0,$dtinidb_mes, $dtinidb_dia, $dtinidb_ano);
        $dfimdb = mktime(0,0,0,$dtfimdb_mes, $dtfimdb_dia, $dtfimdb_ano);
        $intervaldtdb = array($dinidb);
        while($dinidb <= $dfimdb) {
            if(in_array($dinidb, $intervaldt)) {
                $comparadt[] = $dinidb;
            }
            $dinidb += 86400;
            array_push($intervaldtdb, $dinidb);
        }
    }
    if(!empty($comparadt)) {
        $msgi = "O periodo não pode ser apagado pois já existe upload efetuado dentro do período!!!";
        header("location:admin.php?menu=calendario&msgi=".$msgi);
    }
    else {
        $delper = "DELETE FROM periodo WHERE idperiodo='$idperiodo'";
        $edelper = $_SESSION['query']($delper) or die (mysql_error());
        $alter = "ALTER TABLE periodo AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die ("erro na query de alteraÃ§Ã£o do AUTO INCREMENT da tabela periodo");
        $delinter = "DELETE FROM interperiodo WHERE idperiodo='$idperiodo'";
        $edelinter = $_SESSION['query']($delinter) or die ("erro na query que apaga o intervalo cadastrado para o periodo");
        $alter = "ALTER TABLE interperiodo AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die ("erro na query de alteraÃ§Ã£o do AUTO INCREMENT da tabela interperiodo");
        if($ealter) {
          $msgi = "Período apagado com sucesso!!!";
          header("location:admin.php?menu=calendario&msgi=".$msgi);
        }
        else {
          $msgi = "Erro no processo executado, favor contatar o administrador!!!";
          header("location:admin.php?menu=calendario&msgi=".$msgi);
        }
    }
}

if(isset($_POST['atualiza'])) {
    $iduser = $_SESSION['usuarioID'];
    $dias = $_POST['data'];
    $idperiodo = $_POST['idperiodo'];
    if($dias == "") {
      $seldias = "SELECT COUNT(*) as result FROM diasdescon WHERE idperiodo='$idperiodo'";
      $eseldias = $_SESSION['fetch_array']($_SESSION['query']($seldias)) or die (mysql_error());
      if($eseldias['result'] >= 1) {
        $delcad = "DELETE FROM diasdescon WHERE idperiodo='$idperiodo'";
        $edelcad = $_SESSION['query']($delcad) or die (mysql_error());
        $sdias = "SELECT dias FROM periodo WHERE idperiodo='$idperiodo'";
        $esdias = $_SESSION['fetch_array']($_SESSION['query']($sdias)) or die ("erro na query de consulta do perÃ­odo");
        $diasprod = $esdias['dias'];
        $atudias = "UPDATE periodo SET diasprod='$diasprod' WHERE idperiodo='$idperiodo'";
        $eatudias = $_SESSION['query']($atudias) or die ("erro na query de atualizaÃ§Ã£o do periodo");
        $msgp = "Datas atualizadas!!!";
        header("location:admin.php?menu=calendario&idperiodo=".$idperiodo."&msgp=".$msgp);
      }
      else {
        $msgp = "Não foi selecionada nenhuma data, favor selecionar uma!!!";
        header("location:admin.php?menu=calendario&idperiodo=".$idperiodo."&msgp=".$msgp);
      }
    }
    else {
        $delcad = "DELETE FROM diasdescon WHERE idperiodo='$idperiodo'";
        $edelcad = $_SESSION['query']($delcad) or die (mysql_error());
        $countd = 0;
        foreach($dias as $dia) {
            $countd++;
            $caddias = "INSERT INTO diasdescon (idperiodo, data, iduser) VALUE ('$idperiodo', '".data2banco($dia)."', '$iduser')";
            $ecaddias = $_SESSION['query']($caddias);
        }
        if($ecaddias) {
          $sdias = "SELECT dias FROM periodo WHERE idperiodo='$idperiodo'";
          $esdias = $_SESSION['fetch_array']($_SESSION['query']($sdias)) or die ("erro na query de consulta do perÃ­odo");
          $diasprod = $esdias['dias'] - $countd;
          $atudias = "UPDATE periodo SET diasprod='$diasprod' WHERE idperiodo='$idperiodo'";
          $eatudias = $_SESSION['query']($atudias) or die ("erro na query de atualizaÃ§Ã£o do periodo");
          $msgp = "Datas atualizadas!!!";
          header("location:admin.php?menu=calendario&idperiodo=".$idperiodo."&msgp=".$msgp);
        }
        else {
          $msgp = "Erro no processo de atualização, favor contatar o adminsitrador!!!";
          header("location:admin.php?menu=calendario&idperiodo=".$idperiodo."&msgp=".$msgp);
        }
    }
}

if(isset($_GET['semperiodo'])) {
    $idperiodo = $_GET['idperiodo'];
    $delinter = "DELETE FROM interperiodo WHERE idperiodo='$idperiodo'";
    $edelinter = $_SESSION['query']($delinter) or die ("erro na query que apaga as semanas do perÃ­odo");
    $alter = "ALTER TABLE interperiodo AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query de atualizaÃ§Ã£o do AUTO_INCREMENT");
    $selper = "SELECT dataini, datafim FROM periodo WHERE idperiodo='$idperiodo'";
    $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta do periodo");
    $datafim = $eselper['datafim'];
    $dtiniinter = mktime(0, 0, 0, substr($eselper['dataini'],5,2), substr($eselper['dataini'],8,2), substr($eselper['dataini'],0,4));
    $dtfiminter = mktime(0, 0, 0, substr($eselper['datafim'],5,2), substr($eselper['datafim'],8,2), substr($eselper['datafim'],0,4));
    $altsem = InsertSemana($dtiniinter, $dtfiminter, $datafim,  $idperiodo);
    $msgs = "Cadastro de semanas apagado e efetuado reset com sucesso!!!";
    header("location:admin.php?menu=calendario&idperiodo=".$idperiodo."&msgs=".$msgs);
}

if(isset($_POST['atuinter'])) {
    $verifdt = 0;
    $idperiodo = $_POST['idperiodo'];
    $idinter = $_POST['idinter'];
    $compinifim = "SELECT '$datafim' < '$dataini' as result";
    $ecompinifim = $_SESSION['fetch_array']($_SESSION['query']($compinifim)) or die ("erro na query de comparaÃ§Ã£o da data inicial com a final");
    if($ecompinifim['result'] >= 1) {
        $msg = "A data final da semana não pode ser menor que a data inicial";
        header("location:semanas.php?idperiodo=$idperiodo&msg=$msg");
    }
    else {
        $selper = "SELECT * FROM periodo WHERE idperiodo<>'$idperiodo'";
        $eselper = $_SESSION['query']($selper) or die ("erro na query de consulta do periodo");
        while($lselper = $_SESSION['fetch_array']($eselper)) {
            $datas = array($dataini,$datafim);
            foreach($datas as $dt) {
                $compdatas = "SELECT '$dt' >= '".$lselper['dataini']."' AND  '$dt' <= '".$lselper['datafim']."' as datas";
                $ecompdatas = $_SESSION['fetch_array']($_SESSION['query']($compdatas)) or die ("erro na query de comparaÃ§Ã£o da datas");
                if($ecompdatas['datas'] >= 1) {
                    $verifdt++;
                }
            }
        }
        if($verifdt >= 1) {
            $msg = "Já existem períodos contendo as datas informadas não sendo possÃ­vel sua alteração";
            header("location:semanas.php?idperiodo=$idperiodo&msg=$msg");
        }
        else {
            $selinter = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo' AND idinterperiodo='$idinter'";
            $lselinter = $_SESSION['fetch_array']($_SESSION['query']($selinter)) or die ("erro na query de consulta do intervalo selecionado para alteraÃ§Ã£o");    
            $semana = $lselinter['semana'];
            if($lselinter['dataini'] == "$dataini" && $lselinter['datafim'] == "$datafim") {
                $nullatu = 1;
            }
            else {
                $proxsem = $lselinter['semana'] + 1;
                $contprox = 1;
                $c = 0;
                while($c < $contprox) {
                    $selprox = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo' AND semana='$proxsem' AND idinterperiodo<>$idinter";
                    $eselprox = $_SESSION['query']($selprox) or die ("erro na query de consulta do prÃ³ximo periodo");
                    $nprox = $_SESSION['num_rows']($eselprox);
                    if($nprox >= 1) {
                        $lselprox = $_SESSION['fetch_array']($eselprox);
                        $verifdtfim = "SELECT '$datafim' <= '".$lselprox['datafim']."' as result";
                        $everifdtfim = $_SESSION['fetch_array']($_SESSION['query']($verifdtfim)) or die ("erro na query de comparaÃ§Ã£o da data final com a prÃ³xima semana");
                        if($everifdtfim['result'] >= 1) {
                            $adddata = "SELECT DATE_FORMAT(DATE_ADD('$datafim', INTERVAL 1 DAY),'%Y-%m-%d') as data";
                            $eadddata = $_SESSION['fetch_array']($_SESSION['query']($adddata)) or die ("erro na query para adacionar um 1 dia na data");
                            $datainiprox = $eadddata['data'];
                            $updateprox = "UPDATE interperiodo SET dataini='$datainiprox' WHERE idinterperiodo='".$lselprox['idinterperiodo']."'";
                            $eupdateprox = $_SESSION['query']($updateprox) or die ("erro na query de atualizaÃ§Ã£o da semana seguinte");
                            $update = "UPDATE interperiodo SET dataini='$dataini',datafim='$datafim' WHERE idperiodo='$idperiodo' AND idinterperiodo='$idinter'";
                            $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o do perÃ­odo");
                            $c = 1;
                        }
                        else {
                            $delinter = "DELETE FROM interperiodo WHERE idinterperiodo='".$lselprox['idinterperiodo']."'";
                            $edelinter = $_SESSION['query']($delinter) or die ("erro na query para apagar o intervalor");
                            $proxsem++;
                        }
                    }
                    else {
                        $update = "UPDATE interperiodo SET dataini='$dataini',datafim='$datafim' WHERE idperiodo='$idperiodo' AND idinterperiodo='$idinter'";
                        $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o do perÃ­odo");
                        $c = 1;
                    }
                }
            }
            if($nullatu == 1) {
                $msg = "Não houve nenhuma alteração, processo não executado";
                header("location:semanas.php?idperiodo=$idperiodo&msg=$msg");
            }
            else {
                $check = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo' AND idinterperiodo='$idinter' ORDER BY semana";
                $echeck = $_SESSION['query']($check) or die ("erro na query de consulta do periodo apÃ³s atualizaÃ§Ã£o");
                while($lcheck = $_SESSION['fetch_array']($echeck)) {
                    $proxcheck = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo' AND semana='".($lcheck['semana'] + 1)."'";
                    $eproxcheck = $_SESSION['query']($proxcheck) or die ("erro na query de consulta do prox perÃ­odo");
                    $nproxcheck = $_SESSION['num_rows']($eproxcheck);
                    if($nproxcheck >= 1) {
                        $lproxcheck = $_SESSION['fetch_array']($eproxcheck);
                        $difdata = "SELECT DATEDIFF('".$lcheck['datafim']."','".$lproxcheck['dataini']."') as dif";
                        $edifdata = $_SESSION['fetch_array']($_SESSION['query']($difdata)) or die ("erro na query de comparaÃ§Ã£o da data inicial com a final");
                        if($edifdata['dif'] > 1) {
                            $newdata = date('Y-m-d',mktime(0, 0, 0, substr($lcheck['datafim'], 5, 2), substr($lcheck['datafim'], 8, 2), substr($lcheck['datafim'], 0, 4)) - (86400 * $edifdata['dif']));
                            $updateprox = "UPDATE interperiodo SET dataini='$newdata' WHERE idinterperiodo='".$lproxcheck['idinterperiodo']."'";
                            $eupdateprox = $_SESSION['query']($updateprox) or die ("erro na query de atualizaÃ§Ã£o da dataf final do perÃ­odo");
                        }
                    }
                    $antcheck = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo' AND semana='".($lcheck['semana'] - 1)."'";
                    $eantcheck = $_SESSION['query']($antcheck) or die ("erro na query de consulta do prox perÃ­odo");
                    $nantcheck = $_SESSION['num_rows']($eantcheck);
                    if($nantcheck >= 1) {
                        $lantcheck = $_SESSION['fetch_array']($eantcheck);
                        $difdata = "SELECT DATEDIFF('".$lcheck['dataini']."','".$lantcheck['datafim']."') as dif";
                        $edifdata = $_SESSION['fetch_array']($_SESSION['query']($difdata)) or die ("erro na query de comparaÃ§Ã£o da data inicial com a final");
                        if($edifdata['dif'] > 1) {
                            $newdata = date('Y-m-d',mktime(0, 0, 0, substr($lantcheck['datafim'], 5, 2), substr($lantcheck['datafim'], 8, 2), substr($lantcheck['datafim'], 0, 4)) + (86400 * ($edifdata['dif'] - 1)));
                            $updateprox = "UPDATE interperiodo SET datafim='$newdata' WHERE idinterperiodo='".$lantcheck['idinterperiodo']."'";
                            $eupdateprox = $_SESSION['query']($updateprox) or die ("erro na query de atualizaÃ§Ã£o da dataf final do perÃ­odo");
                        }
                    }
                }
                $countinter = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo' ORDER BY dataini";
                $ecount = $_SESSION['query']($countinter) or die ("erro na query de consulta da quantidade de semanas");
                $ncount = 1;
                while($lcount = $_SESSION['fetch_array']($ecount)) {
                    $atusem = "UPDATE interperiodo SET semana='$ncount' WHERE idinterperiodo='".$lcount['idinterperiodo']."'";
                    $eatusem = $_SESSION['query']($atusem) or die ("erro na query de atualizaÃ§Ã£o da semana");
                    $ncount++;
                }
                
                $seldatas = "SELECT MIN(dataini) as dataini,MAX(datafim) as datafim FROM interperiodo WHERE idperiodo='$idperiodo'";
                $eseldatas = $_SESSION['fetch_array']($_SESSION['query']($seldatas)) or die ("erro na query de consulta do periodo");
                $sdias = "SELECT DATEDIFF('".$eseldatas['datafim']."','".$eseldatas['dataini']."') as dias";
                $edias = $_SESSION['fetch_array']($_SESSION['query']($sdias)) or die ("erro na query de contagem dos dias");
                $dias = $edias['dias'] + 1;
                $selprod = "SELECT COUNT(*) as result FROM diasdescon WHERE idperiodo='$idperiodo'";
                $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die ("erro na query de consulta dos dias desconsiderados");
                $diasprod = $dias - $eselprod['result'];
                $atudias = "UPDATE periodo SET dias='$dias', diasprod='$diasprod', dataini='".$eseldatas['dataini']."', datafim='".$eseldatas['datafim']."' WHERE idperiodo='$idperiodo'";
                $eatudias = $_SESSION['query']($atudias) or die ("erro na query para atualizaÃ§Ã£o dos dias do perÃ­odo");
                if($eupdate) {
                    $msg = "Período atualizado com sucesso!!!";
                    header("location:semanas.php?idperiodo=$idperiodo&msg=$msg");
                }
                else {
                    $msg = "Erro na query de atualização do período!!!";
                    header("location:semanas.php?idperiodo=$idperiodo&msg=$msg");
                }   
            }
        }
    }
}

?>