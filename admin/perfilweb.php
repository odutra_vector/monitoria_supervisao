<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
$idperfil = $_GET['idperfil'];
if(isset($_GET['idperfil'])) {
    $selperf = "SELECT *, COUNT(*) as result FROM perfil_web WHERE idperfil_web='$idperfil'";
    $eselperf = $_SESSION['fetch_array']($_SESSION['query']($selperf)) or die ("erro na query de consulta do perfil");
}
else {
}
?>

<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<div id="conteudo" class="corfd_pag">
<form action="cadperfilweb.php" method="post">
<table width="425" border="0">
  <tr>
  	<td class="corfd_ntab" colspan="4" align="center"><strong>CADASTRO PERFIL WEB</strong></td>
  </tr>
  <tr>
    <td width="163" class="corfd_coltexto"><strong>NOME</strong></td>
    <td width="252" class="corfd_colcampos"><input type="hidden" name="id" id="id" value="<?php if(isset($_GET['idperfil'])) { if($eselperf['result'] >= 1) { echo $idperfil; } else {} } else {}?>" /><input style="width:250px; border: 1px solid #69C; text-align:center" name="nome" type="text" value="<?php if(isset($_GET['idperfil'])) { if($eselperf['result'] >= 1){ echo $eselperf['nomeperfil_web']; } else {} } else {}?>" /></td>

  </tr>
  <tr>
    <td width="163" class="corfd_coltexto"><strong>NÍVEL</strong></td>
    <td width="252" class="corfd_colcampos">
        <select name="nivel">
        <?php
        $niveis = array('1','2','3','4','5','6','7','8','9','10');
        if(isset($_GET['idperfil'])) {
            if($eselperf['result'] >= 1) {
                echo "<option selected=\"selected\" value=\"".$eselperf['nivel']."\">".$eselperf['nivel']."</option>";
                foreach($niveis as $nivel) {
                    if($nivel == $eselperf['nivel']) {
                    }
                    else {
                        echo "<option value=\"".$nivel."\">".$nivel."</option>";
                    }
                }
            }
            else {

            }
        }
        else {
            foreach($niveis as $nivel) {
                echo "<option value=\"".$nivel."\">".$nivel."</option>";
            }
        }
        ?>
        </select>
    </td>
  </tr>
  <tr>
      <td class="corfd_coltexto"><strong>MONITORIA EXTERNA</strong></td>
      <td class="corfd_colcampos">
          <select name="visuext" id="visuext">
          <?php
          $visu = array('N','S');
          foreach($visu as $v) {
              if($v == $eselperf['visuext']) {
                  echo "<option value=\"$v\" selected=\"selected\">$v</option>";
              }
              else {
                  echo "<option value=\"$v\">$v</option>";
              }
          }
          ?>
          </select>
      </td>
  </tr>
  <?php
      if(isset($_GET['idperfil'])) {
          if($eselperf['result'] >= 1) {
            echo "<tr>";
                echo "<td class=\"corfd_coltexto\"><strong>ATIVO</strong></td>";
                echo "<td class=\"corfd_colcampos\">";
                    echo "<select name=\"ativo\">";
                        echo "<option value=\"".$eselperf['ativo']."\">".$eselperf['ativo']."</option>";
                        $ativo = array('S','N');
                        foreach($ativo as $atv) {
                            if($atv == $eselperf['ativo']) {

                            }
                            else {
                                echo "<option value=\"".$atv."\">".$atv."</option>";
                            }
                        }
                    echo "</select>";
                echo "</td>";
            echo "</tr>";
          }
          else {
          }
      }
      else {

      }
    ?>
  <tr>
    <td class="corfd_coltexto"><strong>ÁREAS</strong></td>
    <td colspan="4" class="corfd_colcampos">
    <select multiple="multiple" style="width:250px; height:120px" name="areas[]" id="areas">
            <?php
            if(isset($_GET['idperfil'])) {
                if($eselperf['result'] >= 1) {
                    $perfisdb = explode(",",$eselperf['areas']);
                    $perfis = array('IMPORTACAO','RELATORIOS','MONITORIAS','SELECAO','CALIBRAGEM','EBOOK','PRODUCAO');
                    foreach($perfis as $perf) {
                        if(in_array($perf,$perfisdb)) {
                            echo "<option value=\"".$perf."\" selected=\"selected\">".$perf."</option>";
                        }
                        else {
                            echo "<option value=\"".$perf."\">".$perf."</option>";
                        }
                    }
                }
                else {
                    
                }
            }
            else {
            ?>
            <option value="IMPORTACAO">IMPORTACAO</option>
            <option value="RELATORIOS">RELATORIOS</option>
            <option value="MONITORIAS">MONITORIAS</option>
            <option value="CALIBRAGEM">CALIBRAGEM</option>
            <option value="PRODUCAO">PRODUCAO</option>
            <option value="SELECAO">SELEÇÃO</option>
            <option value="EBOOK">EBOOK</option>
            <?php
            }
            ?>
    </select>
    </td>
  </tr>
   <tr>
    <td class="corfd_coltexto"><strong>FILTROS ARVORE?</strong></td>
    <td class="corfd_colcampos">
        <select name="filtro_arvore" id="filtro_arvore">
            <?php
            $filt = array('S','N');
            foreach($filt as $fa) {
                    if($fa == $eselperf['filtro_arvore']) {
                            echo "<option value=\"$fa\" selected=\"selected\">$fa</option>";
                    }
                    else {
                            echo "<option value=\"$fa\">$fa</option>";
                    }
            }
            ?>
        </select>
    </td>
   </tr>
   <tr>
      <?php
      if(isset($_GET['idperfil'])) {
          if($eselperf['result'] >= 1) {
            echo "<td colspan=\"4\"><input style=\"border: 1px solid #FFF; height: 19px; background-image:url(/monitoria_supervisao/images/button.jpg);\" name=\"altera\" type=\"submit\" value=\"Alterar\" /> <input style=\"border: 1px solid #FFF; height: 19px; background-image:url(/monitoria_supervisao/images/button.jpg);\" name=\"novo\" type=\"submit\" value=\"Novo\" /></td>";
          }
          else {
              echo "<td colspan=\"4\"><input style=\"border: 1px solid #FFF; height: 19px; background-image:url(/monitoria_supervisao/images/button.jpg);\" name=\"cadastra\" type=\"submit\" value=\"Cadastrar\" /></td>";
          }
      }
      else {
        echo "<td colspan=\"4\"><input style=\"border: 1px solid #FFF; height: 19px; background-image:url(/monitoria_supervisao/images/button.jpg);\" name=\"cadastra\" type=\"submit\" value=\"Cadastrar\" /></td>";
      }
      ?>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font><p></p>
<hr />
<form action="" method="GET">
<table width="374" border="0">
  <tr>
    <td class="corfd_ntab" align="center" colspan="2"><strong>PESQUISA DE PERFIL</strong></td>
  </tr>
  	<td class="corfd_coltexto"><strong>ID</strong></td>
  	<td class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" name="id" type="text" value="<?php echo $_GET['idperfil']; ?>" /></td>
  <tr>
    <td width="73" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="310" class="corfd_colcampos"><input style="width:250px; border: 1px solid #69C; text-align:center" name="nome" type="text" value="<?php echo $_GET['nome']; ?>" /><input name="menu" value="perweb" type="hidden" /><input name="p" value="1" type="hidden" /></td>
  </tr>
  <tr>
    <td><input style="border: 1px solid #FFF; height: 19px; background-image:url(/monitoria_supervisao/images/button.jpg);" name="pesquisa" type="submit" value="Pesquisar" /></td>
  </tr>
</table></form><br />
<div style="width:100%; overflow: auto; height: 300px">
<table width="80%" border="0">
    <tr>
    <td width="43" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="231" align="center" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="200" align="center" class="corfd_coltexto"><strong>AREAS</strong></td>
    <td width="45" align="center" class="corfd_coltexto"><strong>Nível</strong></td>
    <td width="45" align="center" class="corfd_coltexto"><strong>Mo.Ext</strong></td>
    <td width="70" align="center" class="corfd_coltexto"><strong>Ativo</strong></td>
    <td width="175"></td>
  </tr>
<?php
$id = mysql_real_escape_string($_GET['id']);
$perfil = mysql_real_escape_string($_GET['nome']);
$caracter = mysql_real_escape_string(strtoupper($_REQUEST['nome']));
if($id == "" && $perfil == "") {
  $selp = "SELECT * FROM perfil_web ORDER BY idperfil_web DESC";
  $seldb = "SELECT * FROM perfil_web ORDER BY idperfil_web";
}
if($id != "" && $perfil == "") {
  $selp = "SELECT * FROM perfil_web WHERE idperfil_web='$id' ORDER BY idperfil_web DESC";
  $seldb = "SELECT * FROM perfil_web WHERE idperfil_web='$id' ORDER BY idperfil_web";
}
if($id == "" && $perfil != "") {
  $selp = "SELECT * FROM perfil_web WHERE UPPER(nomeperfil_web) LIKE '$caracter%' ORDER BY idperfil_web DESC";
  $seldb = "SELECT * FROM perfil_web WHERE  UPPER(nomeperfil_web) LIKE '$caracter%' ORDER BY idperfil_web";
}
if($id != "" && $perfil != "") {
  $selp = "SELECT * FROM perfil_web WHERE idperfil_web='$id' AND UPPER(nomeperfil_web) LIKE '$caracter%' ORDER BY idperfil_web DESC";
  $seldb = "SELECT * FROM perfil_web WHERE idperfil_web='$id' AND UPPER(nomeperfil_web) LIKE '$caracter%' ORDER BY idperfil_web";
}
$sel = "$seldb";
$esel = $_SESSION['query']($sel) or die (mysql_error());
while($lsel = $_SESSION['fetch_array']($esel)) {
        $areasdb = explode(",",$lsel['areas']);
        echo "<form action=\"cadperfilweb.php\" method=\"post\">";
        echo "<tr>";
        echo "<td class=\"corfd_colcampos\" align=\"center\"><input style=\"width:60px; text-align:center; border: 1px solid #fff\" readonly=\"readonly\" name=\"id\" type=\"hidden\" value=\"".$lsel['idperfil_web']."\" />".$lsel['idperfil_web']."</td>";
        echo "<td class=\"corfd_colcampos\" align=\"center\">".$lsel['nomeperfil_web']."</td>";
        echo "<td class=\"corfd_colcampos\" align=\"center\">";
        foreach($areasdb as $a) {
            echo $a."<br/>";
        }
        echo "</td>";
        echo "<td class=\"corfd_colcampos\" align=\"center\">".$lsel['nivel']."</td>";
        echo "<td class=\"corfd_colcampos\" align=\"center\">".$lsel['visuext']."</td>";
        echo "<td class=\"corfd_colcampos\" align=\"center\">".$lsel['ativo']."</td>";
        echo "<td><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"carrega\" type=\"submit\" value=\"Carregar\" /> <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"apagar\" /></td>";
        echo "</tr></form>";
}
?>
</table>
</div>
<font color="#FF0000"><strong><?php echo $_GET['msgi'] ?></strong></font><p></p>
</div>