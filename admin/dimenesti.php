<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
$periodo = mysql_real_escape_string($_GET['periodo']);
$idrel = mysql_real_escape_string($_GET['idrel']);
$tpmoni = mysql_real_escape_string($_GET['tpmoni']);
$pesq = mysql_real_escape_string($_GET['pesquisa']);
$seltpmoni = "SELECT *, COUNT(*) as result FROM param_moni INNER JOIN conf_rel ON conf_rel.idparam_moni = param_moni.idparam_moni WHERE conf_rel.idrel_filtros='$idrel'";
$etpmoni = $_SESSION['fetch_array']($_SESSION['query']($seltpmoni)) or die (mysql_error());
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dataini.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/datafim.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtini.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dtfim.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   $("#tma").mask("99:99:99");
   $("#tta").mask("999:99:99");
   $("#tm").mask("99:99:99");
   $("#ttm").mask("999:99:99");
   $('#calcula').click(function() {
       var idrel = $('#idrel').val();
       var periodo = $('#periodo').val();
       var tma = $('#tma').val();
       var tmm = $('#tm').val();
       var qtdgrava = $('#qtdgrava').val();
       var qtdoper = $('#qtdoper').val();
       var qtdemoni = $('qtdemonitor').val();
       var tpmoni = $('#tpmoni').val();
       var multiplica = $('#multiplica').val();
       if(tpmoni == "O") {
           if(tma == "" && tmm == "" && qtdoper == "" && multiplica == "") {
               alert('Não é possível exfetuar o calculo de meta com algum dos campos abaixo vazio!!!\n\
                      Tempo Atendimento\n\
                      Tempo Monitoria\n\
                      Quantidade Operadores\n\
                      Multiplicador');
                return false;
           }
           else {
           }
       }
       if(tpmoni == "G") {
          if(tma == "" && tmm == "" && qtdgrava == "") {
               alert('Não é possível exfetuar o calculo de meta com algum dos campos abaixo vazio!!!\n\
                      Tempo Atendimento\n\
                      Tempo Monitoria\n\
                      Quantidade Gravações');
                return false;
           }
           else {
           }
       }
       $.post("calculameta.php", {idrel: idrel, periodo: periodo, tma: tma, tmm: tmm, qtdgrava: qtdgrava, qtdoper: qtdoper,qtdemoni: qtdemoni, tpmoni: tpmoni, multiplica: multiplica}, function(retorno) {
           var valores = retorno.split("/");
           $('#tta').attr("value",valores[0]);
           $('#ttm').attr("value",valores[1]);
           $('#qtdepa').attr("value",valores[2]);
           $('#qtdemonitor').attr("value",valores[3]);
           $('#hrmonitor').attr("value",valores[4]);
           $('#metamonitor').attr("value",valores[5]);
           $('#percpa').attr("value",valores[6]);
           $('#percmeta').attr("value",valores[7]);
           $('#estima').attr("value",valores[8]);
           $('#msem').attr("value",valores[9]);
           $('#mdias').attr("value",valores[10]);
       });
   });
   
   $("#altera").click(function() {
       <?php
       if($etpmoni['tipomoni'] == "G") {
         ?>
         var qtde = $("#qtdgrava").val();
         var campo = "Gravação";
         <?php
       }
       else {
           ?>
         var qtde = $("#qtdoper").val();
         var campo = "Operador";
         <?php
       }
       ?>
       if(qtde == "") {
           alert("O campo Quantidade de "+campo+" não pode estar vazio");
           return false;
       }
   });
   
   $("#cadastra").click(function() {
       var ttm = $('#ttm').val();
       var tta = $('#tta').val();
       if(ttm == "" || tta == "") {
           alert("Para efetuar o cadastramento ou alteração do dimensionamento é necessário efetuar o calulo da meta");
           return false;
       }
       else {
       }
   })
});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="caddimensiona.php" method="post">
<?php
if($etpmoni['result'] == 0) {
    echo "<table width=\"930\">";
        echo "<tr>";
            echo "<td align=\"center\" bgcolor=\"#FFFFFF\" style=\"color:#F00\"><strong>O RELACIONAMENTO NÃO POSSUI CONFIGURAÇÕES DEFINIDA</strong></td>";
        echo "<tr>";
    echo "</table>";
}
else {
    $seldimen = "SELECT *, COUNT(*) as result FROM dimen_mod dm INNER JOIN periodo p ON p.idperiodo = dm.idperiodo WHERE dm.idrel_filtros='$idrel' AND dm.idperiodo='$periodo'";
    $edimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
    $semanas = "SELECT COUNT(*) as result FROM interperiodo WHERE idperiodo='$periodo'";
    $esemanas = $_SESSION['fetch_array']($_SESSION['query']($semanas)) or die ("erro na query de consulta do intervalo de semana");
    $mmes = $edimen['estimativa'];
    $msem = round($mmes / $esemanas['result']);
    $mdias = round($mmes / $edimen['diasprod']);
    if($edimen['result'] >= 1) {
      $colunas = 5;
      $tamanho = 984;
    }
    else {
      $colunas = 4;
      $tamanho = 784;
    }
    ?>
    <table width="<?php echo $tamanho;?>" style="background-color: #FFF">
    <tr>
    <td class="corfd_ntab" align="center" colspan="<?php echo $colunas;?>"><strong>META</strong></td>
    </tr>
    <tr>
    <td class="corfd_coltexto"><strong>Período</strong></td>
    <td align="center" class="corfd_colcampos">
    <?php
      $selper = "SELECT * FROM periodo WHERE idperiodo='$periodo'";
      $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die (mysql_error());
      ?>
      <input type="hidden" name="periodo" id="periodo" value="<?php echo $periodo;?>">
      <input type="text" style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="nperiodo" value="<?php echo $eselper['nmes'];?><?php echo "-".substr($eselper['datafim'],0,4);?>"></td>
    <td width="185" class="corfd_coltexto"><strong>Tempo Médio Atendimento</strong></td>
    <td width="72" align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" name="tma" id="tma" type="text" value="<?php echo $edimen['tma'];?>" /></td>
    <?php
    if($edimen['result'] >= 1) {
        ?>
        <td width="200" class="corfd_coltexto" align="center"><strong>Monitores</strong></td>
        <?php
    }
    else {
    }
    ?>
    </tr>
    <tr>
    <td width="136" class="corfd_coltexto"><strong>ID Relacionamento</strong></td>
    <td width="75" align="center" class="corfd_colcampos">
        <input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="id" id="idrel" type="text" value="<?php echo $idrel;?>"/>
        <input name="iddimen" type="hidden" value="<?php echo $edimen['iddimen_mod'];?>"/>
        <input name="pesquisa" type="hidden" value="<?php echo $pesq;?>"/>
        <input name="tpmoni" id="tpmoni" type="hidden" value="<?php echo $etpmoni['tipomoni'];?>"/>
        <input type="hidden" name="nome" value="<?php echo $_GET['nome'];?>"/>
        <input type="hidden" name="ativop" id="ativop" value="<?php echo $_GET['ativo'];?>" />
    </td>
    <td class="corfd_coltexto"><strong>Tempo Total Atendimento</strong></td>
    <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="tta" id="tta" type="text" value="<?php echo $edimen['tta'];?>" /></td>
    <?php
    if($edimen['result'] >= 1) {
        ?>
        <td rowspan="9" class="corfd_colcampos"><select name="monitor[]" multiple="multiple" style="width:400px; border: 1px solid #333; text-align:center" size="10">
        <?php
        $cdimenmod = "SELECT COUNT(*) as result FROM dimen_moni WHERE idrel_filtros='".$idrel."' AND idperiodo='".$periodo."' AND ativo='S'";
        $ecdimenmod = $_SESSION['fetch_array']($_SESSION['query']($cdimenmod)) or die (mysql_error());
        $idmonidb = array();
        if($ecdimenmod['result'] >= 1) {
            $sdimenmod = "SELECT * FROM dimen_moni WHERE idrel_filtros='".$idrel."' AND idperiodo='".$periodo."' AND ativo='S'";
            $esdimenmod = $_SESSION['query']($sdimenmod) or die (mysql_error());
            while($lsdimenmod = $_SESSION['fetch_array']($esdimenmod)) {
              $selmonitor = "SELECT nomemonitor FROM monitor WHERE idmonitor='".$lsdimenmod['idmonitor']."'";
              $emonitor = $_SESSION['fetch_array']($_SESSION['query']($selmonitor)) or die (mysql_error());
              $idmonidb[] = $lsdimenmod['idmonitor'];
            ?>
            <option selected="selected" value="<?php echo $lsdimenmod['idmonitor'];?>"><?php echo $emonitor['nomemonitor'];?></option>
            <?php
            }
        }
        $selmoni = "SELECT idmonitor FROM monitor WHERE ativo='S' ORDER BY nomemonitor";
        $eselmoni = $_SESSION['query']($selmoni) or die (mysql_error());
        $idmoniop = array();
        while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
          $idmoniop[] = $lselmoni['idmonitor'];
        }
        $difmoni = array_diff($idmoniop, $idmonidb);
        foreach($difmoni as $idmonitor) {
          $nmonitor = "SELECT nomemonitor FROM monitor WHERE idmonitor='".$idmonitor."' ORDER BY nomemonitor";
          $enmonitor = $_SESSION['fetch_array']($_SESSION['query']($nmonitor)) or die (mysql_error());
          ?>
          <option value="<?php echo $idmonitor;?>"><?php echo $enmonitor['nomemonitor'];?></option>
          <?php
        }
        ?>
        </select></td>
        <?php
    }
    ?>
    </tr>
    <tr>
    <?php
    if($etpmoni['tipomoni'] == "G") {
        ?>
        <td class="corfd_coltexto"><strong>Qtde. Gravações</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" name="qtdgrava" id="qtdgrava" type="text" value="<?php echo $edimen['qnt_grava'];?>" /></td>
        <?php
    }
    if($etpmoni['tipomoni'] == "O") {
        ?>
        <td class="corfd_coltexto"><strong>Qtde. Operadores</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" name="qtdoper" id="qtdoper" type="text" value="<?php echo $edimen['qnt_oper'];?>" /></td>
        <?php
    }
    ?>
    <td class="corfd_coltexto"><strong>Tempo monitoria</strong></td>
    <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" name="tm" id="tm" type="text" value="<?php echo $edimen['tm'];?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Qtde. Externa</strong></td>
        <td class="corfd_colcampos" align="center"><input type="text" name="qtdeext" id="qtdeext" value="<?php echo $edimen['qtdeext'];?>" style="width:120px; text-align:center; border: 1px solid #69F" /></td>
        <td class="corfd_coltexto"><strong>Tempo Total Monitoria</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="ttm" id="ttm" type="text" value=\"<?php echo $edimen['ttm'];?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Multiplicador</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" name="multiplica" id="multiplica" type="text" value="<?php echo $edimen['multiplicador'];?>"
    <?php
    if($etpmoni['tipomoni'] == "G") {
            echo "disabled=\"disabled\"";
    }
    if($etpmoni['tipomoni'] == "O") {
    }
                ?>
    /></td>
      <td class="corfd_coltexto"><strong>Qtde. Monitores</strong></td>
      <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" name="qtdemonitor" id="qtdemonitor" type="text" value="<?php echo $edimen['qnt_monitores'];?>" /></td>
    </tr>
    <tr>
      <td class="corfd_coltexto"><strong>Qtde. PA</strong></td>
      <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="qtdepa" id="qtdepa" type="text" value="<?php echo $edimen['qnt_pad'];?>" /></td>
      <td class="corfd_coltexto"><strong>Meta /p Monitor</strong></td>
      <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="metamonitor" id="metamonitor" type="text" value="<?php echo $edimen['meta_monitor'];?>" /></td>
    </tr>
    <tr>
      <td class="corfd_coltexto"><strong>Horas p/ Monitor</strong></td>
      <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="hrmonitor" id="hrmonitor" type="text" value="<?php echo $edimen['hs_monitor'];?>" /></td>
      <td class="corfd_coltexto"><strong>Perc. da Meta</strong></td>
      <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="percmeta" id="percmeta" type="text" value="<?php echo $edimen['percpartmeta'];?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Perc. PA</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="percpa" id="percpa" type="text" value="<?php echo $edimen['percpartpa'];?>" /></td>
        <td class="corfd_coltexto"><strong>Dias Período</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="dias" id="dias" type="text" value="<?php echo $edimen['diasprod'];?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Meta Mês</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="estima" id="estima" type="text" value="<?php echo $mmes;?>" /></td>
        <td class="corfd_coltexto"><strong>Meta Dia</strong></td>
        <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="mdias" id="mdias" type="text" value="<?php echo $mdias;?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Meta Semana</strong></td>
          <td align="center" class="corfd_colcampos"><input style="width:120px; border: 1px solid #69F; text-align:center" readonly="readonly" name="msem" id="msem" type="text" value="<?php echo $msem;?>" /></td>
          <td class="corfd_colcampos"></td>
          <td class="corfd_colcampos"></td>
    </tr>
    <?php
    if($edimen['result'] >= 1) {
        ?>
        <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="altera" id="altera" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="apaga" type="submit" value="Apagar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="calcula" id="calcula" type="button" value="Calcular" /></td>
        <td align="center"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="vincmoni" type="submit" value="Vincular" /><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="desvmoni" type="submit" value="Desvincular" /></td>
        <?php
    }
    else {
        ?>
         <td colspan="4" class="corfd_colcampos"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="cadastra" id="cadastra" type="submit" value="Cadastrar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="calcula" id="calcula" type="button" value="Calcular" /></td>
         <?php
    }
    ?>
    </tr>
    </table>
    <?php
}
?>
    <font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
  </form>
</div>
</body>
</html>