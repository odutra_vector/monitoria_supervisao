<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$jaberta = $_POST['jaberta'];
$grupo = strtoupper(addslashes($_POST['descri']));
$comple = addslashes($_POST['comple']);
$valor = str_replace(",", ".",$_POST['valor']);
$caracter = "[]$[><}{)(:;!?*%&#@]";
if($valor == "") {
    $valor = "NULL";
}
$idgrupo = $_POST['idgrupo'];
$vinc = $_POST['vinc'];
if($_POST['ativo'] == "SIM") {
    $ativo = "S";
}
if($_POST['ativo'] == "NAO") {
    $ativo = "N";
}
$fg = $_POST['fg'];

if(isset($_POST['cadastra'])) {
    if($grupo == "") {
	$msg = "Os campos ''Grupo'' e ''Valor'' precisam estar preenchidos para efetuar cadastro!!!";
	header("location:admin.php?menu=grupo&msg=".$msg);
    }
    else {
        if(eregi($caracter, $valor)) {
            $msg = "O campo ''Valor'' sÃ³ pode conter nÃºmeros e virgula!!!";
            header("location:admin.php?menu=grupo&msg=".$msg);
        }
        else {
            if($valor < 0) {
                $msg = "O campo ''Valor'' nÃ£o pode ser negativo!!!";
                header("location:admin.php?menu=grupo&msg=".$msg);
            }
            else {
                $cadsub = "INSERT INTO grupo (descrigrupo, complegrupo, valor_grupo, ativo, filtro_vinc, idrel,fg) VALUE ('$grupo', '$comple', $valor, 'S', '$vinc', '000000','$fg')";
                $execad = $_SESSION['query']($cadsub) or die (mysql_error());
                $cons = mysql_insert_id();
                if($execad) {
                    header("location:admin.php?menu=relgrupo&id=".$cons);
                }
                else {
                    $msg = "Ocorreu algum no processo de cadastramento, favor contatar o administrador!!!";
                    header("location:admin.php?menu=grupo&msg=".$msg);
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $sel = "SELECT * FROM grupo WHERE idgrupo='$idgrupo' GROUP BY idgrupo";
    $esel = $_SESSION['fetch_array']($_SESSION['query']($sel)) or die (mysql_error());
    if($esel['filtro_vinc'] == "S") {
      $soma = "SELECT SUM( DISTINCT valor_sub ) as soma FROM subgrupo WHERE idsubgrupo IN (SELECT DISTINCT idrel FROM grupo WHERE idgrupo='$idgrupo')";
      $esoma = $_SESSION['fetch_array']($_SESSION['query']($soma)) or die (mysql_error());
    }
    if($esel['filtro_vinc'] == "P") {
      $soma = "SELECT SUM( DISTINCT valor_perg ) as soma FROM pergunta WHERE idpergunta IN (SELECT DISTINCT idrel FROM grupo WHERE idgrupo='$idgrupo')";
      $esoma = $_SESSION['fetch_array']($_SESSION['query']($soma)) or die (mysql_error());
    }
    if($grupo == "") {
	$msgi = "O campo ''Grupo'' precisa estar preenchido para alteraÃ§Ã£o do cadastro!!!";
	header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
    }
    else {
        if(eregi($caracter, $valor)) {
            $msgi = "O campo ''Valor'' sÃ³ pode conter nÃºmeros e virgula!!!";
            header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
        }
        else {
            if($grupo == $esel['descrigrupo'] && $comple == $esel['complegrupo'] && $valor == $esel['valor_grupo'] && $ativo == $esel['ativo'] && $vinc == $esel['filtro_vinc'] && $fg == $esel['fg']) {
                $msgi = "Nenhum campo foi alterado, o processo não poderia ser executado!!!";
                header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
            }
            else {
                if($valor < 0) {
                    $msgi = "O campo ''Valor'' não pode ser negativo!!!";
                    header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
                }
                else {
                    if($valor < $esoma['soma'] && $$esel['fg'] == "N") {
                        $msgi = "O valor do ''GRUPO'' não pode ser menor que a somatória dos sub-grupos!!!";
                        header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
                    }
                    else {
                        $check = "SELECT COUNT(*) as result FROM planilha WHERE idgrupo='$idgrupo'";
                        $echeck = $_SESSION['fetch_array']($_SESSION['query']($check)) or die (mysql_error());
                        if($echeck['result'] >= 1 AND $ativo == "N") {
                            $msgi = "O grupo já está vinculado a uma planilha, não podendo ser inativado!!!";
                            header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
                        }
                        else {
                            $verif = "SELECT COUNT(*) as result FROM grupo WHERE idgrupo='$idgrupo'";
                            $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die (mysql_error());
                            if($everif['result'] >= 1 && $esel['idrel'] != '000000' && $vinc != $esel['filtro_vinc']) {
                                $msgi = "O ''Vinculo'' do relacionamento nÃ£o pode ser alterado se algum item jÃ¡ estiver relacionado, favor retirar todos relacionamentos existentes para este grupo!!!";
                                header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
                            }
                            else {
                                if($valor != "") {
                                    $tab = "tab='N'";
                                }
                                else {
                                    $tab = "tab='S'";
                                }
                                if($ativo == $esel['ativo']) {
                                    $alter = "UPDATE grupo SET descrigrupo='$grupo', complegrupo='$comple', valor_grupo=$valor, filtro_vinc='$vinc', fg='$fg', $tab WHERE idgrupo='$idgrupo'";
                                }
                                else {
                                    if($ativo == "N") {
                                        $alter = "UPDATE grupo SET descrigrupo='$grupo', complegrupo='$comple', valor_grupo=$valor, ativo='$ativo',posicao=0, filtro_vinc='$vinc', fg='$fg', $tab WHERE idgrupo='$idgrupo'";
                                    }
                                    else {
                                        $alter = "UPDATE grupo SET descrigrupo='$grupo', complegrupo='$comple', valor_grupo=$valor, ativo='$ativo', filtro_vinc='$vinc', fg='$fg', $tab WHERE idgrupo='$idgrupo'";
                                    }
                                }
                                
                                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                                if($ealter) {
                                    header("location:admin.php?menu=relgrupo&id=".$idgrupo."&jaberta=".$jaberta);
                                }
                                else {
                                    $msgi = "Ocorreu algum no processo de alteraÃ§Ã£o, favor contatar o administrador!!!";
                                    header("location:admin.php?menu=grupodet&msgi=".$msgi."&id=".$idgrupo."&jaberta=".$jaberta);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['alterarel'])) {
    $idgrupo = $_POST['idgrupo'];
    header("location:admin.php?menu=relgrupo&id=".$idgrupo);
}

if(isset($_POST['volta'])) {
  header("location:admin.php?menu=grupo");
}
?>