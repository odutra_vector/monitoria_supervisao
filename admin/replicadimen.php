<?php
session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");

$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="/monitoria_supervisao/style.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.6.2.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#replicadimen").click(function() {
            var ref = $("#referencia").val();
            var cad = $("#referenciado").val();
            if(ref == "" || cad == "") {
                $("#spanmsg").html("Favor preencher os campos REFERÊNCIA E CADASTRO");
                return false;
            }
        })
    })
</script>
</head>
<body>
        <div style="width:500px; margin:auto; background-color:<?php echo $cor->corfd_pag;?>; text-align: center">
        <form action="caddimensiona.php" method="post">
            <table width="416" align="center" style=" font-size: 12px; font-family: sans-serif">
              <tr>
                <th scope="col" align="center" class="corfd_ntab" colspan="2"><strong>REPLICAR DIMENSIONAMENTO</strong></th>
              </tr>
              <tr>
                <td width="196" align="center" class="corfd_coltexto"><strong>PERÍODO REFERÊNCIA</strong></td>
                <td width="204" align="center" class="corfd_coltexto"><strong>PERÍODO CADASTRO</strong></td>
              </tr>
              <tr>
                <td class="corfd_colcampos" align="center">
                        <select name="referencia" id="referencia">
                        <option selected="selected" disabled="disabled" value="">Selecione...</option>
                        <?php
                        $seldimen = "SELECT dm.idperiodo, nmes, ano  FROM dimen_mod dm
                                    INNER JOIN periodo p ON p.idperiodo = dm.idperiodo
                                    GROUP BY idperiodo ORDER BY ano DESC, mes DESC";
                        $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta dos período cadastrados");
                        while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                            echo "<option value=\"".$lseldimen['idperiodo']."\">".$lseldimen['nmes']." - ".$lseldimen['ano']."</option>";
                        }
                        ?>
                    </select>
                </td>
                <td class="corfd_colcampos" align="center">
                        <select name="referenciado" id="referenciado">
                        <option disabled="disabled" selected="selected" value="">Selecione...</option>
                        <?php
                            $seldimen = "SELECT * FROM periodo GROUP BY idperiodo ORDER BY ano DESC, mes DESC";
                            $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta dos período cadastrados");
                            while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                                echo "<option value=\"".$lseldimen['idperiodo']."\">".$lseldimen['nmes']." - ".$lseldimen['ano']."</option>";
                            }
                        ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td colspan="2" align="center"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)"  name="replicadimen" id="replicadimen" type="submit" value="CADASTRAR" /></td>
              </tr>
            </table><br/>
            <span id="spanmsg" style="color: #EA071E; font-weight: bold; font-family: sans-serif; font-size: 12px"><?php echo $_GET['msg']; ?></span>
            </form>
        </div>	
</body>
</html>
