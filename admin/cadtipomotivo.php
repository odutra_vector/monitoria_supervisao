<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['cadastra'])) {
    $nome = strtoupper($_POST['nome']);
    $caracter = "[]$[><}{)(:;!?*%&#@]";
    $vinculo = $_POST['vinculo'];
    $seltipo = "SELECT COUNT(*) as result FROM tipo_motivo WHERE nometipo_motivo='$nome' AND vincula='$vinculo'";
    $etipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo))  or die (mysql_error());
    if($nome == "" OR $vinculo == "") {
        $msgt = "O campo ''Nome ou Vinculo'' não pode estar vazio!!!";
        header("location:admin.php?menu=motivo&msgt=".$msgt);
    }
    else {
        if(eregi($caracter, $nome)) {
            $msgt = "No ''Nome'' não poder existir caracter inválido ''$caracter''!!!";
            header("location:admin.php?menu=motivo&msgt=".$msgt);
        }
        else {
            if($etipo['result'] >= 1) {
              $msgt = "Este ''Nome'' já está cadastrado na base para este tipo de vinculo!!!";
              header("location:admin.php?menu=motivo&msgt=".$msgt);
            }
            else {
                $cad = "INSERT INTO tipo_motivo (nometipo_motivo, vincula, ativo) VALUE ('$nome', '$vinculo', 'S')";
                $ecad = $_SESSION['query']($cad) or die (mysql_error());
                if($ecad) {
                    $msgt = "Cadastro efetuo com sucesso!!";
                    header("location:admin.php?menu=motivo&msgt=".$msgt);
                }
                else {
                    $msgt = "Ocorreu um erro durante o cadastro, favor contatar o administrador";
                    header("location:admin.php?menu=motivo&msgt=".$msgt);
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $id = $_POST['id'];
    $nome = strtoupper($_POST['nome']);
    $vinculo = $_POST['vinculo'];
    $ativo = $_POST['ativo'];
    $seltipo = "SELECT * FROM tipo_motivo WHERE idtipo_motivo='$id'";
    $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
    $tipo = "SELECT COUNT(*) as result FROM tipo_motivo WHERE nometipo_motivo='$nome' AND vincula='$vinculo' AND idtipo_motivo<>$id";
    $etipo = $_SESSION['fetch_array']($_SESSION['query']($tipo)) or die (mysql_error());
    if($nome == $eseltipo['nometipo_motivo'] AND $ativo == $eseltipo['ativo'] AND $vinculo == $eseltipo['vincula']) {
        $msgti = "Não foi efetuada nenhuma alteração, favor tentar novamente!!!";
        header("location:admin.php?menu=motivo&msgti=".$msgti);
    }
    else {
        if($etipo['result'] >= 1) {
            $msgti = "Este ''Nome'' já está cadastrado na base para este tipo de motivo!!!";
            header("location:admin.php?menu=motivo&msgti=".$msgti);
        }
        else {
            $alt = "UPDATE tipo_motivo SET nometipo_motivo='$nome', vincula='$vinculo', ativo='$ativo' WHERE idtipo_motivo='$id'";
            $ealt = $_SESSION['query']($alt) or die (mysql_error());
            if($ealt) {
                $msgti = "Cadastro altrado com sucesso!!!";
                header("location:admin.php?menu=motivo&msgti=".$msgti);
            }
            else {
                $msgti = "Ocorreu algum erro na alteração, favor contatar o administrador!!!";
                header("location:admin.php?menu=motivo&msgti=".$msgti);
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $id = $_POST['id'];
    $nome = $_POST['nome'];
    $selmotivo = "SELECT COUNT(*) as result FROM motivo WHERE idtipo_motivo='$id'";
    $emotivo = $_SESSION['fetch_array']($_SESSION['query']($selmotivo)) or die (mysql_error());
    if($emotivo['result'] >= 1) {
        $msgti = "Este ''tipo'' não pode ser apagado, pois já está relacionado com um motivo";
        header("location:admin.php?menu=motivo&msgti=".$msgti);
    }
    else {
        $del = "DELETE FROM tipo_motivo WHERE idtipo_motivo='$id'";
        $edel = $_SESSION['query']($del) or die (mysql_error());
        $alter = "ALTER TABLE tipo_motivo AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die ("erro na query de alteração do auto incremento");
        if($edel) {
            $msgti = "Motivo apagado com sucesso!!!";
            header("location:admin.php?menu=motivo&msgti=".$msgti);
        }
        else {
            $msgti = "Ocorreu algum erro no processo, favor contatar o administrador";
            header("location:admin.php?menu=motivo&msgti=".$msgti);
        }
    }
}
?>