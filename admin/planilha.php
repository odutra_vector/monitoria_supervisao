<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once('functionsadm.php');
$idplan = $_GET['id'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
		$("div[id*='plan']").hide();
		<?php
		if($idplan != "") {
			echo "$('#plan".$idplan."').show();\n";
		}
		else {
		}
		?>
		$('#selplanilha').change(function() {
			var idplan = $(this).val();
			$("div[id*='plan']").hide();
			$('#plan'+idplan).show();
		})
	})
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadplanilha.php" method="post">
<table width="580" border="0">
  <tr>
    <td height="15" colspan="6" class="corfd_ntab" align="center"><strong>CADASTRO DE PLANILHA</strong></td>
  </tr>
  <tr>
    <td width="119" height="26" class="corfd_coltexto"><strong>Nome</strong></td>
    <td colspan="5" class="corfd_colcampos"><input style="width:475px; border: 1px solid #69C" maxlength="50" name="nome" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Complemento</strong></td>
    <td colspan="5" class="corfd_colcampos"><textarea style="width:475px; border: 1px solid #69C" name="comple" rows="3"></textarea></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tabulação</strong></td>
    <td width="63" class="corfd_colcampos">
        <select style="border: 1px solid #69C" name="tab">
            <option value="N">NAO</option>
            <option value="S">SIM</option>
        </select>
    </td>
    <td width="79" class="corfd_coltexto"><strong>Ativo WEB</strong></td>
    <td width="122" class="corfd_colcampos">
        <select style="border: 1px solid #69C" name="atv_web">
            <option value="S">SIM</option>
            <option value="N">NAO</option>    
        </select>
    </td>
    <td width="88" class="corfd_coltexto"><strong>Ativo Monitor</strong></td>
    <td width="83" class="corfd_colcampos">
        <select style="border: 1px solid #69C" name="atv_moni">
            <option value="S">SIM</option>
            <option value="N">NAO</option>    
        </select>
    </td>
    </tr>
	<tr>
    <td colspan="7"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" type="submit" value="Cadastra" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font><hr /><br />
<table width="508">
  <tr>
    <td width="96" class="corfd_coltexto"><strong>PLANILHAS</strong></td>
    <td width="400" class="corfd_colcampos">
    	<select name="selplanilha" id="selplanilha" style="width:400px">
        <?php
        if(isset($idplan)) {
            echo "<option value=\"\"></option>";
        }
        else {
            echo "<option value=\"\" selected=\"selected\"></option>";
        }
        $plans = "SELECT DISTINCT(idplanilha), descriplanilha FROM planilha WHERE ativo='S' GROUP BY idplanilha ORDER BY idplanilha";
        $eplans = $_SESSION['query']($plans) or die ("erro na query de levantamento das planilhas cadastradas");
        while($lselplans = $_SESSION['fetch_array']($eplans)) {
                if(isset($_GET['id'])) {
                        if($lselplans['idplanilha'] == $_GET['id']) {
                                echo "<option value=\"".$lselplans['idplanilha']."\" selected=\"selected\">".$lselplans['idplanilha']." - ".$lselplans['descriplanilha']."</option>";
                        }
                        else {
                                echo "<option value=\"".$lselplans['idplanilha']."\">".$lselplans['idplanilha']." - ".$lselplans['descriplanilha']."</option>";
                        }
                }
                else {
                        echo "<option value=\"".$lselplans['idplanilha']."\">".$lselplans['idplanilha']." - ".$lselplans['descriplanilha']."</option>";
                }
        }
        ?>
    	</select>
    </td>
  </tr>
</table>
<hr />
<?php
$plans = "SELECT * FROM planilha";
$eplans = $_SESSION['query']($plans) or die (mysql_error());
while($lplans = $_SESSION['fetch_array']($eplans)) {
    $plan = "SELECT * FROM planilha WHERE idplanilha='".$lplans['idplanilha']."'";
    $eplan = $_SESSION['fetch_array']($_SESSION['query']($plan)) or die (mysql_error());
    ?>
    <div id="plan<?php echo $eplan['idplanilha'];?>">
    <form action="cadplanilha.php" method="post">
    <table width="788" border="0">
    <tr>
      <td width="70" class="corfd_coltexto"><strong>CÓDIGO</strong></td>
      <td width="108" class="corfd_colcampos"><input style="width:60px; border: 1px solid #69C; text-align:center" readonly="readonly" name="idplan" type="text" value="<?php echo $eplan['idplanilha'];?>" /></td>
      <td width="95" class="corfd_coltexto"><strong>ATIVO WEB</strong></td>
      <td width="69" class="corfd_colcampos"><select name="atv_web">
      <?php
      if($eplan['ativoweb'] == "S") {
	  $atv_web = "SIM";
      }
      if($eplan['ativoweb'] == "N") {
	  $atv_web = "NAO";
      }
      ?>
      <option value="<?php echo $atv_web;?>"><?php echo $atv_web;?></option>
      <?php
      $atvwebdb = array($atv_web);
      $opatvweb = array('SIM','NAO');
      $difatv = array_diff($opatvweb, $atvwebdb);
      foreach($difatv as $atvweb) {
        ?>
	<option value="<?php echo $atvweb;?>"><?php echo $atvweb;?></option>
        <?php
      }
      ?>
      </select></td>
      <td width="101" class="corfd_coltexto"><strong>ATIVO MONI</strong></td>
      <td width="74" class="corfd_colcampos"><select name="atv_moni">
      <?php
      if($eplan['ativomoni'] == "S") {
	  $atvmo = "SIM";
      }
      if($eplan['ativomoni'] == "N") {
	  $atvmo = "NAO";
      }
      ?>
      <option value="<?php echo $atvmo;?>"><?php echo $atvmo;?></option>
      <?php
      $atvmodb = array($atvmo);
      $opatvmo = array('SIM','NAO');
      $difmo = array_diff($opatvmo, $atvmodb);
      foreach($difmo as $atvmoni) {
          ?>
	  <option value="<?php echo $atvmoni;?>"><?php echo $atvmoni;?></option>
          <?php
      }
      ?>
      </select></td>
      <td width="73" class="corfd_coltexto"><strong>TABULAÇÃO</strong></td>
      <td width="104" class="corfd_colcampos"><select name="tab">
      <?php
      if($eplan['tabplanilha'] == "N") {
	  $tab = "NAO";
      }
      if($eplan['tabplanilha'] == "S") {
	  $tab = "SIM";
      }
      ?>
      <option value="<?php echo $tab;?>"><?php echo $tab;?></option>
      <?php
      $tabdb = array($tab);
      $optab = array('SIM','NAO');
      $diftab = array_diff($optab, $tabdb);
      foreach($diftab as $tabs) {
	  ?>
          <option value="<?php echo $tabs;?>"><?php echo $tabs;?></option>
          <?php
      }
      ?>
      </select></td>
      </tr>
      <tr>
      <td class="corfd_coltexto"><strong>PLANILHA</strong></td>
      <td colspan="7" class="corfd_colcampos"><input style="width:700px; border: 1px solid #69C" name="nome" type="text" value="<?php echo $eplan['descriplanilha'];?>" /></td>
      </tr>
      <tr>
      <td class="corfd_coltexto"><strong>DESCRIÇÃO</strong></td>
      <td colspan="7" class="corfd_colcampos"><textarea style="width:700px; border: 1px solid #69C" name="comple" rows="3"><?php echo $eplan['compleplanilha'];?></textarea></td>
      </tr>
          <tr>
          <td class="corfd_coltexto"><strong>PLANILHA VINCULADAS</strong></td>
          <td colspan="1" class="corfd_colcampos"><select style="width:180px; border: 1px solid #69C;" name="plansdepende" readonly="readonly" disabled="disabled" multiple="multiple" size="3">
          <?php
          $selvinc = "SELECT planvinc, idrel_filtros FROM vinc_plan WHERE idplanilha='".$lplans['idplanilha']."' AND ativo='S'";
          $eselvinc = $_SESSION['query']($selvinc) or die ("erro na query de consulta dos vinculos de planilha");
          $plansdep = array();
          while($lselvinc = $_SESSION['fetch_array']($eselvinc)) {
            $plansdep[$lselvinc['idrel_filtros']] = $lselvinc['planvinc'];
          }
          foreach($plansdep as $ativo => $plandep) {
                        if($plandep == "") {
                        }
                        else {
                            $selnplan = "SELECT DISTINCT(idplanilha), descriplanilha FROM planilha WHERE idplanilha='$plandep'";
                            $enplan = $_SESSION['fetch_array']($_SESSION['query']($selnplan)) or die ("erro na query de consulta do nome da planilha");
                            ?>
                        <option value="<?php echo $plandep;?>"><?php echo $enplan['descriplanilha'];?></option>
                        <?php
                        }
          }
          ?>
	  </select></td>
          <td class="corfd_coltexto" colspan="2"><strong>RELACIONAMENTO</strong></td>
          <td colspan="4" class="corfd_colcampos"><select style="width:350px; border: 1px solid #69C;" name="plansdepende" readonly="readonly" disabled="disabled" multiple="multiple" size="3">
          <?php
          foreach($plansdep as $k => $p) {
              ?>
              <option value="<?php echo $k;?>"><?php echo nomeapres($k);?></option>
              <?php
          }
          ?>
          </select></td>
        </tr>
        <?php

      $countaval = "SELECT COUNT(*) as result FROM rel_aval WHERE idplanilha='".$eplan['idplanilha']."'";
      $caval = $_SESSION['fetch_array']($_SESSION['query']($countaval)) or die (mysql_error());
      $selaval = "SELECT * FROM rel_aval WHERE idplanilha='".$eplan['idplanilha']."'";
      $eselaval = $_SESSION['query']($selaval) or die (mysql_error());
      while($lselaval = $_SESSION['fetch_array']($eselaval)) {
          ?>
        <tr>
      <?php
      $avalnome = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$lselaval['idaval_plan']."'";
      $eavalnome = $_SESSION['fetch_array']($_SESSION['query']($avalnome)) or die (mysql_error());
      ?>
      <td class="corfd_coltexto"><strong>AVALIAÇÃO</strong></td>
      <td class="corfd_colcampos"><input style="width:180px; border: 1px solid #69C; text-align:center" readonly="readonly" name="avalplan" type="text" value="<?php echo $eavalnome['nomeaval_plan'];?>" />
      <input name="idrelaval" type="hidden" value="<?php echo $lselaval['idrel_aval'];?>" /><input style="width:60px; border: 1px solid #69C; text-align:center" readonly="readonly" name="idplanrel" type="hidden" value="<?php echo $eplan['idplanilha'];?>" /></td>
      <td class="corfd_coltexto"><strong>VALOR</strong></td>
      <td colspan="5" class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" readonly="readonly" name="valoraval" type="text" value="<?php echo $lselaval['valor'];?>" /></td>
      </tr>
      <?php
      }
      ?>
      <tr>
        <td class="corfd_coltexto"><strong>TABULAÇÃO</strong></td>
        <td class="corfd_colcampos" colspan="7">
            <select name="tabula[]" id="tabula" multiple="multiple" size="5" style="width:280px">
            <?php
            $tabbd = explode(",",$lplans['idtabulacao']);
            $seltab = "SELECT * FROM tabulacao WHERE ativo='S' GROUP BY idtabulacao";
            $eseltab = $_SESSION['query']($seltab) or die ("erro na query de consulta das tabulaÃ§Ãµes");
            while($lseltab = $_SESSION['fetch_array']($eseltab)) {
                if(in_array($lseltab['idtabulacao'],$tabbd)) {
                ?>
                    <option value="<?php echo $lseltab['idtabulacao'];?>" selected="selected"><?php echo $lseltab['nometabulacao'];?></option>
                <?php
                }
                else {
                ?>
                <option value="<?php echo $lseltab['idtabulacao'];?>"><?php echo $lseltab['nometabulacao'];?></option>
                <?php
                }
            }
            ?>
             </select>
        </td>
      </tr>
      </table>
      <table width="1006" border="0">
      <tr>
        <td width="72" class="corfd_coltexto" align="center"><strong>ID GRUPO</strong></td>
        <td width="380" class="corfd_coltexto" align="center"><strong>GRUPO</strong></td>
        <td width="60" class="corfd_coltexto" align="center"><strong>VALOR</strong></td>
        <td width="70" class="corfd_coltexto" align="center"><strong>TABULA</strong></td>
        <td width="120" align="center" class="corfd_coltexto"><strong>AVALIAÇÃO</strong></td>
        <td width="150" align="center" class="corfd_coltexto"><strong>PERGUNTAS F.G</strong></td>
        <td width="210" align="center" class="corfd_coltexto"><strong>PLAN RELACIONAL</strong></td>
        <td width="50" align="center" class="corfd_coltexto"><strong>ATIVO</strong></td>
      </tr>
      <?php
      $countplan = "SELECT COUNT(*) as result FROM planilha WHERE idplanilha='".$lplans['idplanilha']."' AND idgrupo='000000'";
      $ecountp = $_SESSION['fetch_array']($_SESSION['query']($countplan)) or die (mysql_error());
      $plangrup = "SELECT * FROM planilha WHERE idplanilha='".$lplans['idplanilha']."' ORDER BY posicao, ativo";
      $eplangrup = $_SESSION['query']($plangrup) or die (mysql_error());
      while($lplangrup = $_SESSION['fetch_array']($eplangrup)) {
	  if($ecountp['result'] != 1) {
	    $selgrup = "SELECT * FROM grupo WHERE idgrupo='".$lplangrup['idgrupo']."'";
	    $eselgrup = $_SESSION['fetch_array']($_SESSION['query']($selgrup)) or die (mysql_error());
	    $avalgrup = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$lplangrup['idaval_plan']."'";
	    $eavalgrup = $_SESSION['query']($avalgrup) or die (mysql_error());
	    $egrup = $_SESSION['fetch_array']($eavalgrup);
	    if($lplangrup['tab'] == "N") {
		$tabula = "NAO";
	    }
	    if($lplangrup['tab'] == "S") {
		$tabula = "SIM";
	    }
            ?>
	    <tr>
	    <td class="corfd_colcampos" align="center" style="padding: 5px;"><?php echo $eselgrup['idgrupo'];?></td>
	    <td class="corfd_colcampos" style="padding: 5px;"><a style="color:#000;" href="admin.php?menu=grupodet&id=<?php echo $eselgrup['idgrupo'];?>&jaberta=sim" target="blank"><?php echo $eselgrup['descrigrupo'];?></a></td>
	    <td class="corfd_colcampos" align="center"><?php echo $eselgrup['valor_grupo'];?></td>
	    <td class="corfd_colcampos" align="center"><?php echo $tabula;?></td>
	    <td class="corfd_colcampos" align="center"><?php echo $egrup['nomeaval_plan'];?></td>
            <?php
            if($eselgrup['filtro_vinc'] == "P") {
	  	$selfg = "SELECT DISTINCT(pergunta.idpergunta) FROM grupo INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel WHERE grupo.idgrupo='".$lplangrup['idgrupo']."' AND (avalia='FG' OR avalia='FGM')";
            }
            if($eselgrup['filtro_vinc'] == "S") {
		$selfg = "SELECT DISTINCT(pergunta.idpergunta) FROM grupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                         INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
			WHERE grupo.idgrupo='".$lplangrup['idgrupo']."' AND (avalia='FG' OR avalia='FGM')";
            }
            $eselfg = $_SESSION['query']($selfg) or die ("erro na query para identificar perguntas de FG");
            while($lselfg = $_SESSION['fetch_array']($eselfg)) {
                $ids[] = $lselfg['idpergunta'];
            }
            ?>
            <td class="corfd_colcampos" align="center" title="<?php echo implode(",",$ids);?>">
            <?php
                $l = 0; 
                foreach($ids as $id) { 
                    $l++; 
                    if($l <= 4) { 
                        if($l == 4) {
                            $fg .= "$id...";
                        }
                        else {
                            $fg .= "$id, ";
                        }
                    }    
                } 
                echo $fg;?>
            </td>
            <?php
            $pergrel = "SELECT ps.idpergunta, ps.avaliaplan, idrel_avaliaplan FROM planilha p
                        INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                        LEFT JOIN subgrupo s ON s.idsubgrupo = g.idrel AND g.filtro_vinc='S'
                        LEFT JOIN pergunta ps ON ps.idpergunta = s.idpergunta AND g.filtro_vinc='S' OR ps.idpergunta = g.idrel AND g.filtro_vinc='P'
                        WHERE p.idplanilha='".$lplangrup['idplanilha']."' AND p.idgrupo='".$lplangrup['idgrupo']."' AND avaliaplan<>''
                        GROUP BY ps.idresposta ORDER BY g.idgrupo";
            $epergrel = $_SESSION['query']($pergrel) or die ("erro na query de consulta das perguntas que vunculam planilha");
            $numpergrel = $_SESSION['num_rows']($epergrel);
            if($numpergrel >= 1) {
                $arraypergrel = array();
                while($lpergrel = $_SESSION['fetch_array']($epergrel)) {
                    $nplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='".$lpergrel['avaliaplan']."'";
                    $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die ("erro na query de consulta do nome da planilha vinculada");
                    $arraypergrel[$lpergrel['idpergunta']][$lpergrel['avaliaplan']] = $enplan['descriplanilha'];
                }
                $idsperg = array();
                foreach($arraypergrel as $perg => $plan) {
                    $idsperg[] = "<title=\"".current($plan)."\">".$perg."(".key($plan).")</title>";
                }
                ?>
                <td class="corfd_colcampos" align="center"><?php echo implode(",",$idsperg);?></td>
                <?php
            }
            else {
                ?>
                <td class="corfd_colcampos" align="center"></td>
                <?php
            }
            ?>
                <td class="corfd_colcampos" align="center"><?php echo $lplangrup['ativo'];?></td>
	    </tr>
            <?php
		$ids = array();
                $fg = "";
	  }
      }
      ?>
      <tr>
	<td><input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="exclui" type="submit" value="Excluir" /></td>
        <td colspan="4"><input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="altera" type="submit" value="Alterar" />
            <?php
            if($eplan['idaval_plan'] == "0000" AND $eplan['tabplanilha'] == "N") {
		?>
                <input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="cadaval" type="submit" value="Cad. Avaliação/ Plan. Vinc." />
		<?php
            }
            else {
            }
            if($eplan['idaval_plan'] != "0000" AND $eplan['tabplanilha'] == "N") {
                ?>
                <input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="altaval" type="submit" value="Avaliação/Plan. Vinc." />
		<?php
            }
            else {
            }
            ?>
            <input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="alterarel" type="submit" value="Alterar Relacional" />
            <input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="visu" type="submit" value="Visualizar" /></td>
      </tr>
      </table></form>
      </div>
    <?php
    }
?>
<font color="#FF0000"><strong><?php echo $_GET['msgi']; ?></strong></font>
</div>
</body>
</html>