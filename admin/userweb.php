<?php

session_start();
include_once($_SESSION['raiz'].'/monitoria_supervisao/config/conexao.php');
include_once($_SESSION['raiz'].'/monitoria_supervisao/selcli.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function($){
    $('#cadastra').click(function() {
        var nome = $('#nome').val();
        var user = $('#user').val();
        var cpf = $('#cpf').val();
        var email = $('#email').val();
        var confemail = $('#confemail').val();
        if(nome == "" || cpf == "" || user == "" || email == "" || confemail == "") {
            alert('Os campos abaixo precisam estar preenchidos:\n\
                    Nome\n\
                    CPF\n\
                    Usuario\n\
                    E-mail\n\
                    Configuração E-mail');
             return false;
        }
        else {
        }
    });
	
    $("#importar").click(function() {
        var arquivo = $("#arquivo").val();
        if(arquivo == "") {
            alert("O arquivo contendo os dados dos usuários não foi informado");
            return false;	
        }
    });
    
   $("#cpf").mask("999.999.999-99");
   $("#cpfpesq").mask("999.999.999-99");
   $('#tabuserweb').tablesorter();
});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="caduserweb.php" method="post" enctype="multipart/form-data">
<table width="643" border="0">
  <tr>
  	<td class="corfd_ntab" colspan="4" align="center"><strong>CADASTRO DE USUÁRIOS WEB</strong></td>
  </tr>
  <tr>
    <td width="131" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="216" class="corfd_colcampos"><input style="width:200px; border: 1px solid #69C; text-align:center" name="nome" id="nome" maxlength="100" type="text" /></td>
    <td width="76" class="corfd_coltexto"><strong>CPF</strong></td>
    <td width="202" class="corfd_colcampos">
      <label>
        <input style="width:100px; border: 1px solid #69C; text-align:center" type="text" name="cpf" id="cpf" />
      </label>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Perfil</strong></td>
    <td class="corfd_colcampos">
    <select name="perfil">
    <?php

	$sql = "SELECT * FROM perfil_web WHERE ativo='S'";
	$exe = $_SESSION['query']($sql) or die (mysql_error());
	while($lperfil = $_SESSION['fetch_array']($exe)) {
		echo "<option value=\"".$lperfil['idperfil_web']."\">".$lperfil['nomeperfil_web']."</option>";
	}
	?>
    </select></td>
    <td class="corfd_coltexto"><strong>E-mail</strong></td>
    <td class="corfd_colcampos">
      <label>
        <input style="width:200px; border: 1px solid #69C; text-align:center" type="text" maxlength="100" name="email" id="email" />
      </label>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Usuario</strong></td>
    <td class="corfd_colcampos"><input style="width:100px; border: 1px solid #69C; text-align:center; text-align:center" maxlength="20" name="user" id="user" type="text" /></td>
    <td class="corfd_coltexto"><strong>Importação</strong></td>
    <td class="corfd_colcampos"><select name="import">
    <option value="S">S</option>
    <option value="N">N</option>
    </select></td>
  </tr>
  <tr>
   <td class="corfd_coltexto"><strong>Conf. E-mail</strong></td>
  <td class="corfd_colcampos"><select name="confemail" id="confemail">
   <?php
   $selconf = "SELECT idconfemailenv FROM confemailenv WHERE tipo='USUARIO'";
   $eselconf = $_SESSION['query']($selconf) or die ("erro na consulta das configurações cadastradas");
   while($lconf = $_SESSION['fetch_array']($eselconf)) {
	   echo "<option value=\"".$lconf['idconfemailenv']."\">".$lconf['idconfemailenv']."</option>";
   }
   ?>
  </select></td>
  <td class="corfd_coltexto"><strong>Calibragem</strong></td>
  <td class="corfd_colcampos"><select name="calib">
  <option value="N">N</option>
  <option value="S">S</option>
  </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tela Apresentação</strong></td>
    <td class="corfd_colcampos">
        <select name="confapres[]" multiple="multiple" size="4">
        <option value="" disabled="disabled">Selecione...</option>
        <option value="CALIB">CALIBRAGEM</option>
        <option value="REL">RELATÓRIOS</option>
        <option value="EBOOK">EBOOK</option>
    	</select>
    </td>
    <td class="corfd_coltexto"><strong>Seleção Audios</strong></td>
    <td class="corfd_colcampos">
    	<select name="selecao" id="selecao">
        	<option value="N">N</option>
            <option value="S">S</option>
        </select>  
     </td>
  </tr>
  <tr>
  	<td colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastra" type="submit" value="Cadastrar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg); margin-left:10px" name="importar" id="importar" type="submit" value="Importar" /> <input type="file" name="arquivo" id="arquivo" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
<hr />
<form action="" method="post">
<table width="421" border="0">
  <tr>
  	<td class="corfd_ntab" colspan="2" align="center"><strong>PESQUISA DE USUÁRIOS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="159" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="252" class="corfd_colcampos"><input name="menu" type="hidden" value="userweb" /><input style="width:250px; border: 1px solid #69C; text-align:center" name="nome" type="text" value="<?php echo $_POST['nome']; ?>" /></td>
  </tr>
  <tr>
    <td width="159" class="corfd_coltexto"><strong>E-mail</strong></td>
    <td width="252" class="corfd_colcampos"><input style="width:250px; border: 1px solid #69C; text-align:center" name="email" type="text" value="<?php echo $_POST['email']; ?>" title="Busca aproximada do texto digitado" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>CPF</strong></td>
    <td class="corfd_colcampos">
    <label>
      <input style="width:100px; border: 1px solid #69C; text-align:center" type="text" name="CPF" id="cpfpesq" value="<?php echo $_POST['CPF']; ?>" />
    </label></td>
  </tr>
  <tr>
  <td class="corfd_coltexto"><strong>Perfil</strong></td>
  <td class="corfd_colcampos"><select name="perfil"><option value=""></option>
  <?php
  $sql = "SELECT * FROM perfil_web";
  $exe = $_SESSION['query']($sql) or die (mysql_error());
  while($lper = $_SESSION['fetch_array']($exe)) {
    $selected=($_POST['perfil'] == $lper['idperfil_web'] ? "SELECTED" : "");
    echo "<option ";
    if($lper['idperfil_web'] == $_POST['perfil']) {
      echo "selected=\"selected\"";
    }
    else {
    }
    echo "value=\"".$lper['idperfil_web']."\">".$lper['nomeperfil_web']."";
  }
  ?>
  </select></td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesquisa" type="submit" value="Pesquisar" /></td>
  </tr>
</table>
</form><br />
<div style="width: 1024px; height: 400px; overflow: auto">
<font color="#FF0000"><strong><?php echo $_GET['msg2'] ?></strong></font>
<?php
$dados .= "<table width=\"1000\" border=\"0\" id=\"tabuserweb\">";
$dadosexp .= "<table width=\"1000\" border=\"0\" id=\"tabuserweb\">";
  $dados .= "<thead>";
  $dadosexp .= "<thead>";
    $dados .= "<th width=\"146\" align=\"center\" class=\"corfd_coltexto\"><strong>ID</strong></th>";
    $dadosexp .= "<th width=\"146\" align=\"center\" class=\"corfd_coltexto\"><strong>ID</strong></th>";
    $dados .= "<th width=\"250px\" align=\"center\" class=\"corfd_coltexto\"><strong>Nome</strong></th>";
    $dadosexp .= "<th width=\"250px\" align=\"center\" class=\"corfd_coltexto\"><strong>Nome</strong></th>";
    $dados .= "<th width=\"90\" align=\"center\" class=\"corfd_coltexto\"><strong>CPF</strong></th>";
    $dadosexp .= "<th width=\"90\" align=\"center\" class=\"corfd_coltexto\"><strong>CPF</strong></th>";
    $dados .= "<th width=\"35\" align=\"center\" class=\"corfd_coltexto\"><strong>Perfil</strong></th>";
    $dadosexp .= "<th width=\"35\" align=\"center\" class=\"corfd_coltexto\"><strong>Perfil</strong></th>";
    $dados .= "<th width=\"116\" align=\"center\" class=\"corfd_coltexto\"><strong>E-mail</strong></th>";
    $dadosexp .= "<th width=\"116\" align=\"center\" class=\"corfd_coltexto\"><strong>E-mail</strong></th>";
    $dados .= "<th width=\"67\" align=\"center\" class=\"corfd_coltexto\"><strong>Usuario</strong></th>";
    $dadosexp .= "<th width=\"67\" align=\"center\" class=\"corfd_coltexto\"><strong>Usuario</strong></th>";
    $dados .= "<th width=\"60\" align=\"center\" class=\"corfd_coltexto\"><strong>Ativo</strong></th>";
    $dadosexp .= "<th width=\"60\" align=\"center\" class=\"corfd_coltexto\"><strong>Ativo</strong></th>";
    $dados .= "<th width=\"61\" align=\"center\" class=\"corfd_coltexto\"><strong>S_Ini</strong></th>";
    $dadosexp .= "<th width=\"61\" align=\"center\" class=\"corfd_coltexto\"><strong>S_Ini</strong></th>";
    $dados .= "<th width=\"65\" align=\"center\" class=\"corfd_coltexto\"><strong>Dt.Cad</strong></th>";
    $dadosexp .= "<th width=\"65\" align=\"center\" class=\"corfd_coltexto\"><strong>Dt.Cad</strong></th>";
    $dados .= "<th></th>";
  $dados .= "</thead>";
  $dadosexp .= "</thead>";
    $dados .= "<tbody>";
    $dadosexp .= "<tbody>";
    $nome = mysql_real_escape_string($_POST['nome']);
    $email = mysql_real_escape_string($_POST['email']);
    $cpf = mysql_real_escape_string(str_replace("-", "", $_POST['CPF']));
    $cpf = str_replace(".", "", $cpf);
    $perfil = mysql_real_escape_string($_POST['perfil']);
    if($nome != "") {
        $where[] = "nomeuser_web LIKE '%$nome%'";
    }
    if($cpf != "") {
        $where[] = "cpf='$cpf'";
    }
    if($email != "") {
        $where[] = "email LIKE '%$email%'";
    }
    if($perfil != "") {
        $where[] = "idperfil_web='$perfil'";
    }
    if($where != "") {
        $where = "WHERE ".implode(" AND ",$where);
    }
    else {
        $where = "";
    }
    $seldb = "SELECT * FROM user_web $where ORDER BY nomeuser_web";
        $sql = "$seldb";
        $exe = $_SESSION['query']($sql) or die (mysql_error());
        while($luser = $_SESSION['fetch_array']($exe)) {
            $cpf1 = substr($luser['CPF'], 0, 3);
            $cpf2 = substr($luser['CPF'], 3, 3);
            $cpf3 = substr($luser['CPF'], 6, 3);
            $cpfdig = substr($luser['CPF'], 9, 2);
            $cpf = $cpf1.".".$cpf2.".".$cpf3."-".$cpfdig;
            $selperfil = "SELECT nomeperfil_web FROM perfil_web WHERE idperfil_web='".$luser['idperfil_web']."'";
            $eperfil = $_SESSION['fetch_array']($_SESSION['query']($selperfil)) or die (mysql_error());
            $dados .= "<tr>";
            $dadosexp .= "<tr>";
            $dados .= "<form action=\"caduserweb.php\" method=\"post\">";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\"><input name=\"id\" type=\"hidden\" style=\"text-align:center; width:50px; border: 1px solid #FFF;\" readonly=\"readonly\" value=\"".$luser['iduser_web']."\" />".$luser['iduser_web']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['iduser_web']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['nomeuser_web']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['nomeuser_web']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$cpf."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$cpf."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$eperfil['nomeperfil_web']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$eperfil['nomeperfil_web']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['email']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['email']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['usuario']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['usuario']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['ativo']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['ativo']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['senha_ini']."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".$luser['senha_ini']."</td>";
            $dados .= "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:80px; border: 1px solid #FFF; text-align:center; text-align:center\" readonly=\"readonly\" name=\"dt.cad\" type=\"hidden\" value=\"".banco2data($luser['datacad'])."\" />".banco2data($luser['datacad'])."</td>";
            $dadosexp .= "<td align=\"center\" class=\"corfd_colcampos\">".banco2data($luser['datacad'])."</td>";
            $dados .= "<td><input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"edita\" type=\"submit\" value=\"Editar\" /></td>";
            $dados .= "</form>";
            $dados .= "</tr>";
            $dadosexp .= "</tr>";
        }
    $dados .= "</tbody>";
    $dadosexp .= "</tbody>";
$dados .= "</table>";
$dadosexp .= "</table>";
echo "<table width=\"1024\">";
    echo "<tr>";
        echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=usuarios_web\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">EXCEL</div></a></td>";
        echo "<td><a style=\"text-decoration:none; text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=WORD&nome=usuarios_web\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">WORD</div></a></td>";
        echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" target=\"blank\" href=\"/monitoria_supervisao/users/export.php?tipo=PDF&nome=usuarios_web\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">PDF</div></a></td>";
    echo "</tr>";
echo "</table><br/>";
$_SESSION['dadosexp'] = $dados;
echo $dados;
?>
</div>
</div>
</body>
</html>
