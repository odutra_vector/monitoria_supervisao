<?php

ini_set('max_execution_time', 3600);
ini_set('memory_limit', '1024M');

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.smtp.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.phpmailer.php');
include_once($rais.'/monitoria_supervisao/classes/class.envemail.php');
//include_once($rais.'/monitoria_supervisao/classes/class.conectaftp.php');
include_once("functionsadm.php");


$url = "/monitoria_supervisao/inicio.php?menu=importa";
$escala = array("M" => "1000000","G" => "1000000000","K" => "1000");
$mimes = mimes();

$idrel = $_POST['idrel'];

if(isset($_POST['enviar'.$idrel])) {
  //$periodo = periodo(datas);
  $idparam = $_POST['idparam'];
  $tipoimp = $_POST['opcaoimp'];
  $fator = ini_get('upload_max_filesize');
  $maxpostiniphp = substr(ini_get('upload_max_filesize'),0,-1)*($escala[substr($fator,-1)]);
  if($_POST['opcaoimp'] == "R") {
    $selcaminho = "SELECT camaudio, tparquivo, tpcompress, tipomoni, tipoimp, tiposervidor,endereco,loginftp,senhaftp FROM param_moni pm"
            . " INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni WHERE cr.idrel_filtros='$idrel'";
    $ecaminho = $_SESSION['fetch_array']($_SESSION['query']($selcaminho)) or die (mysql_error());
  }
  else {
    $selparam = "SELECT camaudio, tparquivo, tpcompress, tipomoni, tipoimp, tiposervidor,endereco,loginftp,senhaftp FROM param_moni WHERE idparam_moni='$idparam'";
    $ecaminho = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
  }
  $tiposerver = $ecaminho['tiposervidor'];
  $camaudio = $ecaminho['camaudio'];
  if($ecaminho['tiposervidor'] == "FTP") {
      $conftp = ftp_connect($ecaminho['endereco']);
      $lofinftp = ftp_login($conftp, $ecaminho['loginftp'], base64_decode($ecaminho['senhaftp']));
      $dir = ftp_chdir($conftp, $camaudio);
  }
  $tparq = explode(",",$ecaminho['tparquivo']);
  foreach($tparq as $arrayarq) {
      foreach($mimes as $mime) {
          $ext = explode(",",$mime);
          if($ext[1] == $arrayarq) {
            $acarquivo[] = $ext[0];
          }
          else {
          }
      }
  }
  $tpcomp = explode(",",$ecaminho['tpcompress']);
  foreach($tpcomp as $arraycomp) {
      foreach($mimes as $mime) {
          $extc = explode(",",$mime);
          if($extc[1] == $arraycomp) {
            $acaudio[] = $extc[0];
          }
          else {
          }
      }
  }
  $extproibida = array("application/zip" => "zip","multipart/x-zip" => "zip","application/x-compressed" => "zip","application/x-rar-compressed" => "rar","application/x-gzip" => "gz","application/x-rar" => "rar");
  //carregando arquivo de dados
  $arquivoup = $_FILES['arquivoup'.$idrel]['tmp_name'];
  $tamanhoarq = $_FILES['arquivoup'.$idrel]['size'];
  $nomearq = $_FILES['arquivoup'.$idrel]['name'];
  $tipoarq = $_FILES['arquivoup'.$idrel]['type'];
  
  //carregando arquivo de audio
  $audiosup = $_FILES['audioup'.$idrel]['tmp_name'];
  $tamanhoaudios = $_FILES['audioup'.$idrel]['size'];
  $nomearqaudios = $_FILES['audioup'.$idrel]['name'];
  $tipoaudio = $_FILES['audioup'.$idrel]['type'];
  if(file_exists($camaudio)) {
    if(array_key_exists($tipoarq,$extproibida)) {
        $msgu = "O arquivo contendo os dados não deve estar comprimido!!!";
        header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
    }
    else {
          $selconfemail = "SELECT COUNT(*) as result, idconfemailenv FROM relemailimp WHERE idrel_filtros='$idrel'";
          $eselconfemail = $_SESSION['fetch_array']($_SESSION['query']($selconfemail)) or die ("erro na query de consulta do relacionamento de configuraÃƒÂ§ÃƒÂ£o");
          if($tamanhoaudios > $maxpostiniphp) {
              $msgu = "O arquivo contendo os audios é maior que o permitido 500MB!!!";
              header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
          }
          else {
              $periodo = periodo();
              $selsemana = "SELECT *,count(*) as r FROM interperiodo WHERE idperiodo='$periodo' AND '".date('Y-m-d')."' between dataini AND datafim";
              $eselsemana = $_SESSION['fetch_array']($_SESSION['query']($selsemana));
              $dataini = data2banco($eselsemana['dataini']);
              $datafim = data2banco($eselsemana['datafim']);
              $semana = 1;
              $seldatas = "SELECT p.nmes,p.ano,it.dataini,it.datafim,it.idperiodo,it.semana FROM interperiodo it 
                           INNER JOIN periodo p ON p.idperiodo = it.idperiodo 
                           WHERE it.idperiodo='$periodo' AND it.semana='$semana'";
              $eseldatas = $_SESSION['query']($seldatas) or die ("erro na query de consulta das datas que serÃƒÂ£o utilizadas");
              $ndatas = $_SESSION['num_rows']($eseldatas);
              if($ndatas >= 1) {
                  $lseldatas = $_SESSION['fetch_array']($eseldatas);
              }
              $mesano = $lseldatas['nmes']."_".$lseldatas['ano'];
              $selimp = "SELECT importacao, COUNT(*) as result FROM upload WHERE idperiodo='$periodo' AND semana='$semana' AND idrel_filtros='$idrel'";
              $eselimp = $_SESSION['fetch_array']($_SESSION['query']($selimp)) or die ("erro na query de consulta da importaÃƒÂ§ÃƒÂ£o");
              if($eselimp['result'] == 0) {
                  $imp = 1;
              }
              else {
                  $imp = $eselimp['result'] + 1;
              }
              $dataatu = date('d/m/Y');
              $horaatu = date('H:i:s');
              $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel DESC";
              $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
              $linhas = file($arquivoup, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
              $lvazio = 0;
              $abrearq = fopen($arquivoup, "rb");
              $conteudo = fread($abrearq, filesize($arquivoup));
              $linhasv = explode("\n", $conteudo);
              if($semana == "" OR $eselsemana['r'] ==0) {
                  if($semana == "") {
                      $msgu = "O campo semana não pode estar vazio!!!";
                      header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
                  }
                  if($eselsemana['r'] == 0) {
                      $msgu = "Não existe calendário cadastro para esta data, favor verficar!!!";
                      header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
                  }
              }
              else {
                  if($periodo == "") {
                      $msgu = "O campo período não pode estar vazio!!!";
                      header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
                  }
                  else {
                      if($ecaminho['tipoimp'] == "A") {
                          if($tamanhoaudios == 0) {
                              $errotam = TRUE;
                          }
                          if(!in_array($tipoaudio, $acaudio)) {
                              $erroext = true;
                          }
                      }
                      if($ecaminho['tipoimp'] == "LA") {
                          if($tamanhoaudios == 0 OR $tamanhoarq == 0) {
                              $errotam = TRUE;
                          }
                          if(!in_array($tipoarq, $acarquivo) OR !in_array($tipoaudio, $acaudio)) {
                              $erroext = true;
                          }
                      }
                      if($ecaminho['tipoimp'] == "L" OR $ecaminho['tipoimp'] == "I") {
                          if($tamanhoarq == 0) {
                              $errotam = TRUE;
                          }
                          if(!in_array($tipoarq, $acarquivo)) {
                              $erroext = true;
                          }
                      }
                      if($errotam == true) {
                          $msgu = "O/s arquivo/s estão vazios ou não foram enviados os arquivos necessários, favor verificar!!!";
                          header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
                      }
                      else {
                          if($erroext == true) {
                              $msgu = "A extensão do arquivo de audios não está liberada para upload no servidor, favor contatar o administrador!!!";
                              header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$count&tipoimp=$tipoimp");
                          }
                          else {
                              $selrel = "SELECT * FROM rel_filtros WHERE idrel_filtros='$idrel'";
                              $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                              while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                                $selnomedados = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$eselrel['id_'.strtolower($lfiltros['nomefiltro_nomes'])]."'";
                                $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnomedados)) or die (mysql_error());
                                $pastas = scandir($camaudio);
                                if($conftp) {
                                    $cria = ftp_mkdir($conftp, $eselnome['nomefiltro_dados']);
                                    $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                                    $newdir = ftp_chdir($conftp, $camaudio);
                                }
                                else {
                                    if(in_array($eselnome['nomefiltro_dados'], $pastas)) {
                                      $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                                    }
                                    else {
                                      mkdir("$camaudio/".$eselnome['nomefiltro_dados'], 0777);
                                      $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                                    }
                                    $ultimofilt = $lfiltros['nomefiltro_nomes'];
                                }
                              }
                              if($conftp) {
                                  $criaarq = $camaudio."/".$mesano;
                                  ftp_mkdir($conftp, $criaarq);
                              }
                              else {
                                  $criaarq = $camaudio."/".$mesano;
                                  mkdir($criaarq, 0777);
                              }
                             if($ecaminho['tipomoni'] == "G") {
                                  $upload = "INSERT INTO upload (tabfila, opcaoimp, idrel_filtros, iduser, tabuser, camupload, idperiodo,semana,importacao, dataup, horaup, dataimp, horaimp,qtdeimp,qtdeerro) VALUE ('fila_grava', '$tipoimp', '$idrel','".$_SESSION['usuarioID']."','".$_SESSION['user_tabela']."', '','$periodo','$semana','$imp', '".data2banco($dataatu)."','$horaatu', '0000-00-00', '00:00:00','0','0')";
                             }
                             else {
                                  $upload = "INSERT INTO upload (tabfila, opcaoimp, idrel_filtros, iduser, tabuser, camupload, idperiodo,semana,importacao, dataup, horaup, dataimp, horaimp,qtdeimp,qtdeerro) VALUE ('fila_oper', '$tipoimp','$idrel','".$_SESSION['usuarioID']."','".$_SESSION['user_tabela']."', '','$periodo','$semana','$imp', '".data2banco($dataatu)."','$horaatu', '0000-00-00', '00:00:00','0','0')";
                             }
                              $eupload = $_SESSION['query']($upload);
                              $idupload = str_pad($_SESSION['insert_id'](), 6, '0', STR_PAD_LEFT);
                              if($tipoimp == "U") {
                                  if($conftp) {
                                      $criaarq = "$criaarq/$idupload-$idparam";
                                      ftp_mkdir($conftp, $criaarq);
                                  }
                                  else {
                                      $criaarq = "$criaarq/$idupload-$idparam";
                                      mkdir($criaarq, 0777);
                                  }
                              }
                              else {
                                  if($conftp) {
                                      $criaarq = "$criaarq/$idupload";
                                      ftp_mkdir($conftp, $criaarq);
                                  }
                                  else {
                                      $criaarq = "$criaarq/$idupload";
                                      mkdir($criaarq, 0777);
                                  }
                              }
                              $update = "UPDATE upload SET camupload='$criaarq' WHERE idupload='$idupload'";
                              $eupdate = $_SESSION['query']($update) or die ("erro na query de atualização do upload");
                              if($ecaminho['tipoimp'] == "A") {
                                  $tipo = pathinfo($_FILES['audioup'.$idrel]['name'],PATHINFO_EXTENSION);
                                  if($tipo == "rar") {
                                      $countarq = unrar($audiosup,$nomearqaudios,$criaarq, $idrel);
                                  }
                                  if($tipo == "zip") {
                                      $countarq = unzip($audiosup, $criaarq, $idrel);
                                  }
                                  if($countarq >= 1) {
                                      if ($eupload) {
                                          $msgu = "''$countarq'' arquivos salvos no servidor!!!$erromail";
                                          header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                      }
                                      else {
                                            $msgu = "Ocorreu um erro no processo de upload, favor contatar o administrador!!!$erromail";
                                            header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                      }
                                  }
                                  else {
                                      if($countarq == -1) {
                                          $msgu = "Não foi possível descompactar o arquivo, o arquivo está com erro ou a pasta do upload não pode ser criada, favor contatar o ADMINISTRADOR";
                                          header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                      }
                                      else {
                                        $msgu = "Nenhum arquivo foi importado para o servidor, favor verificar!!!";
                                        header("location:$url&idrel=$idrel&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                      }
                                  }
                              }
                              if($ecaminho['tipoimp'] == "LA") {
                                  $tipo = pathinfo($_FILES['audioup'.$idrel]['name'],PATHINFO_EXTENSION);
                                  if($tipo == "rar") {
                                      $countarq = unrar($audiosup,$nomearqaudios,$criaarq, $idrel);
                                  }
                                  if($tipo == "zip") {
                                      $countarq = unzip($audiosup, $criaarq, $idrel);
                                  }
                                  for($l = 0; $l < count($linhasv); $l++) {
                                    if($tamanhoarq == 0) {
                                    }
                                    else {
                                      if($linhasv[$l] == "" && filesize($arquivo) != 0) {
                                          $lvazio = $lvazio + 1;
                                      }
                                      if($linhasv[$l] != "" && filesize($arquivo) != 0) {
                                          $lvazio = $lvazio;
                                      }
                                    }
                                  }
                                  $data = date('d.m.Y');
                                  $novonome = $idrel."-".$idupload."-".$data.".".pathinfo($nomearq,PATHINFO_EXTENSION);
                                  if(file_exists($criaarq."/".$novonome) && $tiposerver == "LOCAL") {
                                    $msgu = "Já foi enviado um arquivo para esta/e ''".$ultimofilt."'' nesta data!!!";
                                    header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&tipoimp=$tipoimp&lvazias=".$lvazio);
                                  }
                                  else {
                                      if($tiposerver == "FTP") {
                                          $upftp = ftp_put($conftp, $novonome,$arquivoup, FTP_BINARY);
                                          ftp_close($conftp);
                                      }
                                      else {
                                          move_uploaded_file($arquivoup, $criaarq."/".$nomearq);
                                          rename($criaarq."/".$nomearq, $criaarq."/".$novonome);
                                      }
                                      if((file_exists($criaarq."/".$novonome) && $tiposerver == "LOCAL") OR $tiposerver == "FTP") {
                                          /* $mail = new ConfigMail();
                                           $mail->IsSMTP();
                                           $mail->IsHTML(true);
                                           $mail->SMTPAuth = true;
                                           $mail->idconf = $eselconfemail['idconfemailenv'];
                                           $mail->relapres = $relapres;
                                           $mail->tipoacao = "upload";
                                           $mail->upload = $idupload;
                                           $mail->idrel = $idrel;
                                           $mail->config();
                                           $selconfemail = "SELECT * FROM relemailimp r WHERE r.idrel_filtros='$idrel'";
                                           $eselconfemail = $_SESSION['fetch_array']($_SESSION['query']($selconfemail)) or die ("erro na query de consulta dos e-mails para envio de mensagem");
                                           $usersadm = explode(",",$eselconfemail['usersadm']);
                                           $usersweb = explode(",",$eselconfemail['usersweb']);
                                           foreach($usersadm as $useradm) {
                                               if($useradm == "") {
                                               }
                                               else {
                                                   $selnome = "SELECT * FROM user_adm WHERE iduser_adm='$useradm'";
                                                   $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome));
                                                   $email = $eselnome['email'];
                                                   $nome = $eselnome['nomeuser_adm'];
                                                   //$mail->SetFrom($eselnome['email'],$eselnome['nomeuser_adm']);
                                                   $mail->AddAddress("$email", "$nome");
                                                   $mail->Send();
                                               }
                                               $mail->ClearAllRecipients();
                                           }
                                           foreach($usersweb as $userweb) {
                                               if($usersweb == "") {
                                               }
                                               else {
                                                   $selnome = "SELECT * FROM user_web WHERE iduser_web='$userweb'";
                                                   $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome));
                                                   $email = $eselnome['email'];
                                                   $nome = $eselnome['nomeuser_web'];
                                                   //$mail->SetFrom($eselnome['email'],$eselnome['nomeuser_adm']);
                                                   $mail->AddAddress("$email", "$nome");
                                                   $mail->Send();
                                               }
                                               $mail->ClearAllRecipients();
                                           }*/
                                           if ($eupload) {
                                             $msgu = "''$countarq'' arquivos salvos no servidor!!!";
                                             header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&audiosup=$nomearqaudios&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                           }
                                           else {
                                             $msgu = "Erro durante o processo de inserção das dados na base, favor contatar o administrador!!!";
                                             header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&audiosup=$nomearqaudios&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                           }
                                      }
                                      else {
                                          $msgu = "Erro durante o processo de upload, favor contatar o administrador!!!";
                                          header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&audiosup=$nomearqaudios&msgu=$msgu&qtdarq=$countarq&tipoimp=$tipoimp&retorno=1");
                                      }
                                  }
                              }
                              if($ecaminho['tipoimp'] == "L" OR $ecaminho['tipoimp'] == "I") {
                                  for($l = 0; $l < count($linhasv); $l++) {
                                  if($tamanhoarq == 0) {
                                  }
                                  else {
                                      if($linhasv[$l] == "" && $tamanhoarq != 0) {
                                          $lvazio = $lvazio + 1;
                                      }
                                      if($linhasv[$l] != "" && $tamanhoarq != 0) {
                                          $lvazio = $lvazio;
                                      }
                                    }
                                  }
                                  fclose($arquivoup);
                                  $data = date('d.m.Y');
                                  $novonome = $idrel."-".$idupload."-".$data.".".pathinfo($nomearq,PATHINFO_EXTENSION);
                                  if(file_exists($criaarq."/".$novonome)) {
                                    $msgu = "Já foi enviado um arquivo para esta/e ''".$ultimofilt."'' nesta data!!!";
                                    header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&tipoimp=$tipoimp&retorno=1");
                                  }
                                  else {
                                      if($conftp) {
                                          $chdir = ftp_chdir($conftp, $criaarq);
                                          $enviaftp = ftp_nb_fput($conftp, $novonome, $arquivoup,FTP_BINARY);
                                      }
                                      else {
                                          move_uploaded_file($arquivoup, $criaarq."/".$nomearq);
                                          rename($criaarq."/".$nomearq, $criaarq."/".$novonome);
                                      }
                                      if((file_exists($criaarq."/".$novonome) && $tiposerver == "LOCAL") OR $tiposerver == "FTP") {                  
                                          /*  $mail = new ConfigMail();
                                            $mail->IsSMTP();
                                            $mail->IsHTML(true);
                                            $mail->SMTPAuth = true;
                                            $mail->idconf = $eselconfemail['idconfemailenv'];
                                            $mail->relapres = $relapres;
                                            $mail->tipoacao = "upload";
                                            $mail->upload = $idupload;
                                            $mail->idrel = $idrel;
                                            $mail->config();
                                            $selconfemail = "SELECT * FROM relemailimp r WHERE r.idrel_filtros='$idrel'";
                                            $eselconfemail = $_SESSION['fetch_array']($_SESSION['query']($selconfemail)) or die ("erro na query de consulta dos e-mails para envio de mensagem");
                                            $usersadm = explode(",",$eselconfemail['usersadm']);
                                            $usersweb = explode(",",$eselconfemail['usersweb']);
                                          foreach($usersadm as $useradm) {
                                                if($useradm == "") {
                                                }
                                                else {
                                                    $selnome = "SELECT * FROM user_adm WHERE iduser_adm='$useradm'";
                                                    $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome));
                                                    $email = $eselnome['email'];
                                                    $nome = $eselnome['nomeuser_adm'];
                                                    $mail->AddAddress("$email", "$nome");
                                                    $mail->Send();
                                                }
                                                $mail->ClearAllRecipients();
                                          }
                                          foreach($usersweb as $userweb) {
                                                if($userweb == "") {
                                                }
                                                else {
                                                    $selnome = "SELECT * FROM user_web WHERE iduser_web='$userweb'";
                                                    $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome));
                                                    $email = $eselnome['email'];
                                                    $nome = $eselnome['nomeuser_web'];
                                                    $mail->AddAddress("$email", "$nome");
                                                    $mail->Send();
                                                }
                                                $mail->ClearAllRecipients();
                                          }*/
                                          if($eupload) {
                                            $msgu = "O arquivo foi enviado ao servidor com sucesso!!!";
                                            header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&tipoimp=$tipoimp&retorno=1");
                                          }
                                          else {
                                            $msgu = "Erro durante o processo de inserção das dados na base, favor contatar o administrador!!!";
                                            header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&retorno=1");
                                          }
                                      }
                                      else {
                                          $msgu = "O arquivo para importação não foi enviado ao servidor, pois a pasta pode estar protegida, favor contatar o administrador";
                                          header("location:$url&idrel=$idrel&msgu=$msgu&lvalidas=$num_linhas&lvazias=$lvazio&tipoimp=$tipoimp&retorno=1");
                                      }
                                  }
                              }                                    
                          }
                      }
                  }
              }
          }
    }
  }
  else {
      $msgu = "O caminho informado no parametro não é válido, favor contatar o ADMINISTRADOR";
      header("location:$url&idrel=$idrel&msgu=$msgu&tipoimp=$tipoimp&retorno=1");
  }
}

if(isset($_POST['carrega'])) {
  $verifup = "SELECT COUNT(*) as result FROM upload WHERE imp='N'";
  $everifup = $_SESSION['fetch_array']($_SESSION['query']($verifup)) or die (mysql_error());
  $idrel = implode(",",$_POST['idupload']);
  $nomeimp = $_POST['nomeimp'];
  $urlimp = "/monitoria_supervisao/inicio.php?menu=importa";
  if($everifup['result'] == 0) {
    $msgimp = "Não existe upload aguardando importação!!!";
    header("location:".$urlimp."&idrelup=".$idrel."&nomeimp=".$nomeimp."&carrega=carrega&msgimp=".$msgimp);
  }
  else {
      if($idrel == "") {
        $msgimp = "Favor selecionar uma linha para carregar arquivos!!!";
        header("location:".$urlimp."&idrelup=".$idrel."&nomeimp=".$nomeimp."&carrega=carrega&msgimp=".$msgimp);
      }
      else {
        if(count($_POST['idupload']) > 1) {
              $msgimp = "Favor selecionar somente uma linha para carregar arquivos!!!";
              header("location:".$urlimp."&idrelup=".$idrel."&nomeimp=".$nomeimp."&carrega=carrega&msgimp=".$msgimp);
        }
        else {
          header("location:".$urlimp."&idrelup=".$idrel."&nomeimp=".$nomeimp."&carrega=carrega");
        }
      }
  }
}

if(isset($_POST['importa'])) {
  ob_start();
  $acao = $_POST['acaoimport'];
  $verifup = "SELECT COUNT(*) as result FROM upload WHERE imp='N'";
  $everifup = $_SESSION['fetch_array']($_SESSION['query']($verifup)) or die (mysql_error());
  if($everifup['result'] == 0) {
    $msgimp = "Não existe upload aguardando importação!!!";
    header("location:".$url."&divisao=&relapres=&msgimp=".$msgimp);
  }
  else {
    if(isset($_POST['idupload']) AND count($_POST['idupload']) == 1) {
      $idup = implode(",",$_POST['idupload']);
      $selup = "SELECT fg.idfila_grava,u.camupload, u.opcaoimp,pm.camaudio, u.idrel_filtros, pm.idparam_moni, pm.semdados,pm.tparquivo, 
                    pm.tiposervidor,pm.endereco,pm.loginftp,pm.senhaftp FROM upload u
                    INNER JOIN conf_rel cr ON cr.idrel_filtros = u.idrel_filtros
                    INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni 
                    LEFT JOIN fila_grava fg on fg.idupload = u.idupload
                    WHERE u.idupload='$idup' GROUP BY u.idupload";
      $eselup = $_SESSION['fetch_array']($_SESSION['query']($selup)) or die (mysql_error());
      if($eselup['idfila_grava'] != "") {
            $msgimp = "O UPLOAD já possui fila Gerada, favor contatar o Administrador!!!";
            header("location:".$url."&divisao=&relapres=&msgimp=".$msgimp);
      }
      else {
        $checkmail = "SELECT COUNT(*) as result FROM relemailimp WHERE idrel_filtros='".$eselup['idrel_filtros']."'";
        $echeck = $_SESSION['fetch_array']($_SESSION['query']($checkmail));
        if($echeck['result'] == 0 && $eselup['opcaoimp'] == "R") {
            $msgimp = "O RELACIONAMENTO não possui configuração de envio de e-mail cadastrada, favor verificar!!!";
            header("location:".$url."&divisao=&relapres=&msgimp=".$msgimp);
        }
        else {
            $tiposerver = $eselup['tiposervidor'];
            $semdados = $eselup['semdados'];
            if($eselup['tiposervidor'] == "FTP") {
                $ftp = ftp_connect($eselup['endereco']);
                $login = ftp_login($ftp, $eselup['loginftp'], base64_decode($eselup['senhaftp']));
                $ftpdir = ftp_chdir($ftp, $eselup['camupload']);
            }
            else {
            }
            $idrel = $eselup['idrel_filtros'];
            $caminho = $eselup['camupload'];

            if($eselup['opcaoimp'] == "U") {
                $quebracam = explode("/",$eselup['camupload']);
                $param = explode("-",$quebracam[count($quebracam) - 1]);
                $idparam = $param[1];
                $tipoimp = $eselup['opcaoimp'];
            }
            else {
                $idparam = $eselup['idparam_moni'];
                $tipoimp = $eselup['opcaoimp'];
            }
            $tiposarq = explode(",",$eselup['tparquivo']);
            $selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
            $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
            if($tiposerver == "FTP") {
                $listadir = ftp_nlist($ftp, $eselup['camupload']);
                foreach($listadir as $dir) {
                    $partext = explode(".",$dir);
                    $extarq = end($partext);
                    if(in_array($extarq, $tiposarq)) {
                        if(eregi("logimp", $dir) OR eregi("logverif",$dir)) {
                        }
                        else {
                        $caminhoarq = $caminho."/".$dir;
                        $arq = $dir;
                        }
                    }
                    else {
                    }
                }
            }
            else {
                $verificadir = scandir($caminho);
                foreach($verificadir as $dir) {
                    $partext = explode(".",$dir);
                    $extarq = end($partext);
                    if(in_array($extarq, $tiposarq)) {
                        if(eregi("logimp", $dir) OR eregi("logverif",$dir)) {
                        }
                        else {
                        $caminhoarq = $caminho."/".$dir;
                        }
                    }
                    else {
                    }
                }
            }
            //$arquivo = end(explode("/",$caminhoarq));
            if($tiposerver == "FTP") {
                $path = $eselup['camupload'];
                $nome = basename($arq);
                $temp = ftp_get($ftp,$rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/$nome",$arq,FTP_BINARY);
                $caminhoarq = $rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/$nome";
                $conteudo = fopen($rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/$nome","r");
            }
            else {
                $vdados = fopen($caminhoarq, "r");
                $conteudo = fread($vdados, filesize($caminhoarq));
            }
            fclose($vdados);
            $linhas = file($caminhoarq, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            $lvazio = 0;
            $linhasv = explode("\n", $conteudo);
            for($l = 0; $l < count($linhasv); $l++) {
                if(filesize($caminhoarq) == 0) {
                    $lvazio = "O arquivo está vazio!!!";
                }
                if($linhasv[$l] == "" && filesize($caminhoarq) != 0) {
                    $lvazio = $lvazio + 1;
                }
                if($linhasv[$lv] != "" && filesize($caminhoarq) != 0) {
                    $lvazio = $lvazio;
                }
            }
            if($tiposerver == "FTP") {
                $arquivos = ftp_nlist($ftp, $caminho);
            }
            else {    
                $arquivos = scandir($caminho);
            }
            if($eselparam['tipoimp'] == "LA") {
                if(count($arquivos) <= 0) {
                    $qtdarquivos = "a pasta está vazia";
                }
                else {
                    $qtdarquivos = count($arquivos);
                }
                if(filesize($caminhoarq) == 0 && $qtdarquivos <= 0) {
                    $msgimp = "A importação não pode ser executada, pois o arquivo e a pasta estão vazios";
                    header("location:".$url."&diraudios=".$_POST['audios']."&msgimp=".$msgimp);
                }
                else {
                    if(filesize($caminhoarq) == 0 && $qtdarquivos > 0) {
                        $msgimp = "A importação não pode ser executada, pois o arquivo está vazio!!!";
                        header("location:".$url."&diraudios=".$_POST['audios']."&msgimp=".$msgimp);
                    }
                    else {
                        if(filesize($caminhoarq) > 0 && $qtdarquivos <= 0) {
                            $msgimp = "A importação não pode ser executada, pois a pasta está vazia!!!";
                            header("location:".$url."&diraudios=".$_POST['audios']."&msgimp=".$msgimp);
                        }
                        else {
                            if(filesize($caminhoarq) > 0 && $qtdarquivos > 0) {
                                if($_POST['acaoimport'] == "") {
                                    $msgimp = "É necessário informar a ação que será tomada, ''VERIFICAR'' ou ''IMPORTAR''!!!";
                                    header("location:".$url."&msgimp=".$msgimp);
                                }
                                else {
                                    echo importa($tipoimp,$eselparam['tipoimp'],$idup, $idrel, $caminho, $caminhoarq, $dataini, $datafim, $acao,$tiposerver,$ftp);
                                }
                            }
                        }
                    }
                }
            }
            if($eselparam['tipoimp'] == "A") {
                if($_POST['acaoimport'] == "") {
                    $msgimp = "É necessário informar a ação que será tomada, ''VERIFICAR'' ou ''IMPORTAR''!!!";
                    header("location:".$url."&msgimp=".$msgimp);
                }
                else {
                    if((count($arquivos) <= 0 && $tiposerver == "LOCAL") OR (count($listadir) <= 0 && $tiposerver == "FTP")) {
                        $qtdarquivos = "a pasta está vazia";
                        $msgimp = "a pasta está vazia";
                        header("location:".$url."&msgimp=".$msgimp);
                    }
                    else {
                    $qtdarquivos = count($arquivos);
                        if($qtdarquivos > 0) {
                            echo importa($tipoimp,$eselparam['tipoimp'],$idup, $idrel, $caminho, '',$dataini, $datafim, $acao,$tiposerver,$ftp);
                        }
                    }
                }
            }
            if($eselparam['tipoimp'] == "L" OR $eselparam['tipoimp'] == "I") {
                if($_POST['acaoimport'] == "") {
                    $msgimp = "É necessário informar a ação que será tomada, ''VERIFICAR'' ou ''IMPORTAR''!!!";
                    header("location:".$url."&msgimp=".$msgimp);
                }
                else {
                    if(!is_file($caminhoarq)) {
                        $msgimp = "O caminho informado não é um arquivo";
                        header("location:".$url."&msgimp=".$msgimp);
                    }
                    else {
                        if(filesize($caminhoarq) == 0) {
                            $lvazio = "O arquivo está vazio!!!";
                            header("location:".$url."&msgimp=".$msgimp);
                        }
                        else {
                            if(filesize($caminhoarq) > 0) {
                                echo importa($tipoimp,$eselparam['tipoimp'],$idup, $idrel, $caminho, $caminhoarq, $dataini, $datafim, $acao,$tiposerver,$ftp);
                                $ret = ob_get_contents();
                            }
                        }
                    }
                }
            }
        }
      }
    }
    else {
        if(count($_POST['idupload']) > 1) {
            $msgimp = "Só pode ser selecionado somente 1 upload por vez para executar importação!!!";
            header("location:".$url."&relapres=&msgimp=".$msgimp);
        }
        if(!isset($_POST['idupload'])) {
            $msgimp = "É necessário selecionar um upload para executar importação!!!";
            header("location:".$url."&relapres=&msgimp=".$msgimp);
        }
    }
  }
}

if(isset($_POST['apaga'])) {
    $idup = implode(",",$_POST['idupload']);
    $relapres = $_POST['nomeimp'];
    if($idup == "") {
      $msgimp = "Favor selecionar uma linha para apagar upload!!!";
      header("location:".$url."&relapres=&msgimp=".$msgimp);
    }
    else {
        $selup = "SELECT p.nmes,p.ano,u.tabuser,u.idrel_filtros,u.iduser,u.camupload,tiposervidor,endereco,loginftp,senhaftp, 
                  u.dataup,u.semana,pm.camaudio FROM upload u
                  INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                  INNER JOIN conf_rel cf ON cf.idrel_filtros = u.idrel_filtros
                  INNER JOIN param_moni pm ON pm.idparam_moni = cf.idparam_moni
                  WHERE u.idupload='$idup'";
        $eselup = $_SESSION['fetch_array']($_SESSION['query']($selup)) or die (mysql_error());
        $idrel = $eselup['idrel_filtros'];
        $mes = $eselup['nmes'];
        $ano = $eselup['ano'];
        $userup = $eselup['iduser'];
        $tabuser = $eselup['tabuser'];
        if(count($_POST['idupload']) > 1) {
            $msgimp = "Só é possível apagar 1 upload por vez!!!";
            header("location:".$url."&relapres=&msgimp=".$msgimp);
        }
        else {
            if(($userup != $_SESSION['usuarioID'] && $tabuser != $_SESSION['user_tabela']) && $_SESSION['usuarioperfil'] != "01") {
                $msgimp = "A alteração não pode ser executada pois o usuário não tem perfil para esta ação";
                header("location:$url&msgimp=".$msgimp);
            }
            else {
                $logs = array('logimp','logverif');
                $dir = $eselup['camupload'];
                if($eselup['tiposervidor'] == "FTP") {
                    $conftp = ftp_connect($eselup['endereco']);
                    $login = ftp_login($conftp, $eselup['loginftp'], base64_decode($eselup['senhaftp']));
                    $chdir = ftp_chdir($conftp, $eselup['camupload']);
                    $dirftp = ftp_nlist($conftp, $eselup['camupload']);
                    foreach($dirftp as $arq) {
                        $delarq = ftp_delete($conftp, $arq);
                    }
                    $apagadir = ftp_rmdir($conftp, $dir."/.");
                    $apagadir = ftp_rmdir($conftp, $dir."/..");
                    //$chdir = ftp_chdir($conftp, $dir);
                    $apagadir = ftp_rmdir($conftp, $dir);
                    $dir = str_replace("/".$idup, "", $dir);
                    $apagadir = ftp_rmdir($conftp, $dir);
                    $mesup = "/".$eselup['nmes']."_".$eselup['ano'];
                    $dir = str_replace($mesup,"",$dir);
                    $filtros = "SELECT * FROM filtro_nomes ORDER BY nivel ASC";
                    $efiltros = $_SESSION['query']($filtros) or die (mysql_error());
                    while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                        $selfiltros = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados=(SELECT id_".strtolower($lfiltros['nomefiltro_nomes'])." FROM rel_filtros WHERE idrel_filtros='$idrel')";
                        $eselfiltros = $_SESSION['fetch_array']($_SESSION['query']($selfiltros)) or die ("erro na query de consulta dos filtros cadastrados");
                        $apagadir = ftp_rmdir($conftp, $dir);
                        $dir = str_replace("/".$eselfiltros['nomefiltro_dados'], "", $dir);
                    }
                    foreach($logs as $log) {
                        $arq = "$log-$idrel-".$eselup['nmes']."-".$eselup['ano']."-".str_replace("/",".",banco2data($eselup['dataup'])).".txt";
                        $del = unlink($rais."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/log/$arq");
                    }
                }
                else {
                    $abredir = scandir($dir);
                    foreach($abredir as $arquivo) {
                      chmod($dir."/".$arquivo, 0755);
                      $apagaarq = unlink($dir."/".strtoupper($arquivo));
                      $apagaarq = unlink($dir."/".$arquivo);
                    }        
                    $remove = rmdir($dir);
                    $dir = str_replace("/$idup","",$dir);
                    $remove = rmdir($dir);
                    $mesano = $mes."_".$ano;
                    $dir = str_replace("$mesano","", $dir);
                    $filtros = "SELECT * FROM filtro_nomes";
                    $efiltros = $_SESSION['query']($filtros) or die (mysql_error());
                    while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                      $remove = rmdir($dir);
                      $nomefiltro = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados=(SELECT id_".strtolower($lfiltros['nomefiltro_nomes'])." FROM rel_filtros WHERE idrel_filtros='$idrel')";
                      $enome = $_SESSION['fetch_array']($_SESSION['query']($nomefiltro)) or die (mysql_error());
                      $dir = str_replace("".$enome['nomefiltro_dados']."/","",$dir);
                    }
                }
                if($eselup['camupload'] != $dir) {
                  $delup = "DELETE FROM upload WHERE idupload='$idup'";
                  $edelup = $_SESSION['query']($delup) or die (mysql_error());
                  $alter = "ALTER TABLE upload AUTO_INCREMENT=1";
                  $ealter = $_SESSION['query']($alter) or die ("erro na query para autalizar o campo auto incremento");
                  $msgimp = "Upload removido do servidor com sucesso!!!";
                  header("location:".$url."&relapres=&msgimp=".$msgimp);
                }
                else {
                  $msgimp = "Ocorreu um erro no processo para apagar o upload, favor contatar o administrador!!!";
                  header("location:".$url."&relapres=&msgimp=".$msgimp);
                }
            }
        }
    }
}

if(isset($_POST['alteraup'])) {
    if(count($_POST['idupload']) > 1) {
        $msgimp = "A alteração só pode ser efetuada em 1 upload por vez!!!";
        header("location:$url&msgimp=".$msgimp);
    }
    else {
        $comp = 0;
        $idup = $_POST['idupload'];
        $idrel = $_POST['idrel'];
        $selup = "SELECT u.iduser, u.tabuser, camupload, u.idperiodo, it.semana, u.idrel_filtros,p.nmes, p.ano FROM upload u
                 INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                 INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana 
                 WHERE idupload='$idup'";
        $eselup = $_SESSION['fetch_array']($_SESSION['query']($selup)) or die ("erro na query para consulta do upload");
        $semana = $eselup['semana'];
        $userup = $eselup['iduser'];
        $tabuser = $eselup['tabuser'];
        if(($userup != $_SESSION['usuarioID'] && $tabuser != $_SESSION['user_tabela']) && $_SESSION['usuarioperfil'] != "01") {
            echo "2";
        }
        else {
            $camant = $eselup['camupload'];
            if($idrel != $eselup['idrel_filtros']) {
                //remanejamento dos arquivos para o novo diretÃƒÂ³rio
                $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel DESC";
                $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
                $selrel = "SELECT * FROM rel_filtros rf 
                                INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros 
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni 
                                WHERE rf.idrel_filtros='$idrel'";
                $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                $camaudio = $eselrel['camaudio'];
                while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                    $selnomedados = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$eselrel['id_'.strtolower($lfiltros['nomefiltro_nomes'])]."'";
                    $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnomedados)) or die (mysql_error());
                    $pastas = scandir($camaudio);
                    if(in_array($eselnome['nomefiltro_dados'], $pastas)) {
                      $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                    }
                    else {
                      mkdir("$camaudio/".$eselnome['nomefiltro_dados'], 0777);
                      $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                    }
                    $ultimofilt = $lfiltros['nomefiltro_nomes'];
                }
                $camaudio = $camaudio."/".$eselup['nmes']."_".$eselup['ano'];
                mkdir("$camaudio",0777);
                $camaudio = $camaudio."/".$idup;
                mkdir("$camaudio",0777);
                $files = scandir($camant);
                foreach($files as $file) {
                    copy($camant."/".$file, $camaudio."/".$file);
                }
                foreach($files as $del) {
                    unlink($camant."/".$del);
                }
                $pastas = explode("/",$camant);
                $camremove = $camant;
                krsort($pastas);
                foreach($pastas as $pasta) {
                    if($pasta == "") {
                    }
                    else {
                        rmdir($camremove);
                        $part = "/".$pasta;
                        $camremove = str_replace($part, "", $camremove);
                    }
                }
                $update = "UPDATE upload SET idrel_filtros='$idrel', camupload='$camaudio', semana='$semana' WHERE idupload='$idup'";
                $eupdata = $_SESSION['query']($update) or die ("erro na query para executar o update dos dados");

            }
            else {
                $camupload = $eselup['camupload'];
                $update = "UPDATE upload SET semana='$semana' WHERE idupload='$idup'";
                $eupdata = $_SESSION['query']($update) or die ("erro na query para executar o update dos dados");
            }
            if($eupdata) {
                echo "1";
            }
            else {
                echo "0";
            }
        }
    }
}

if(isset($_POST['alteraimp'])) {
    $idrelimp = $_POST['idrelimp'];
    if(count($_POST['idupimp']) > 1) {
        $imprel = "Para alteração de importação só poderá ser selecionado 1 registro por vez!!!";
        header("location:$url&idrel=".$idrelimp."&imprel=".$imprel."#imp");
    }
    else {
        $idup = implode(",",$_POST['idupimp']);
        $idrel = $_POST['relimp'.$idup];
        if($idup == "") {
            $imprel = "Favor selecionar 1 registro para relializar alteração!!!";
            header("location:$url&idrel=".$idrel."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
        }
        else {
            if($_SESSION['usuarioperfil'] != "01") {
                $imprel = "O usuário não tem perfil cadastrado para alterar importações já realizadas!!!";
                header("location:$url&idrel=".$idrel."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
            }
            else {
                $selup = "SELECT * FROM upload u
                          INNER JOIN rel_filtros rf ON rf.idrel_filtros = u.idrel_filtros
                          INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                          INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                          INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                          INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
                          WHERE u.idupload='$idup'";
                $lselup = $_SESSION['fetch_array']($_SESSION['query']($selup)) or die ("erro na query de consulta do upload");
                $camant = $lselup['camupload'];
                $semana = $lselup['semana'];
                $periodo = $lselup['idperiodo'];
                if($idrel == $lselup['idrel_filtros'] && $semana == $lselup['semana']) {
                    $imprel = "Nenhum dado foi alterado, processo nÃƒÂ£o executado!!!";
                    header("location:$url&idrel=".$idrel."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
                }
                else {
                    $comp = 0;
                    if($idrel != $lselup['idrel_filtros']) {
                        //remanejamento dos arquivos para o novo diretÃƒÂ³rio
                        $verifrel = "SELECT rf.idrel_filtros, pm.tipomoni FROM rel_filtros rf
                                    INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                                    INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni WHERE rf.idrel_filtros='$idrel'";
                        $everifrel = $_SESSION['fetch_array']($_SESSION['query']($verifrel)) or die ("erro na consulta dos parametros do relacionamento");
                        if($everifrel['tipomoni'] == "G") {
                            $fila = "fila_grava";
                            $aliasf = "fg";
                        }
                        if($everifrel['tipomoni'] == "O") {
                            $fila = "fila_oper";
                            $aliasf = "fo";
                        }
                        if($fila != $lselup['tabfila']) {
                            $imprel = "A alteração não pode ser realizada, pois o tipo de monitoria utilizada na importação é diferente do tipo de monitoria do relacinamento escolhido!!!";
                            header("location:$url&idrel=".$idrel."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
                            break;
                        }
                        else {
                            $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel DESC";
                            $efiltros = $_SESSION['query']($selfiltros) or die ("erro na consulta dos filtros cadastrados");
                            $selrel = "SELECT * FROM rel_filtros rf INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni WHERE rf.idrel_filtros='$idrel'";
                            $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na consulta do relacionamento para identificar o caminho dos audios");
                            $camaudio = $eselrel['camaudio'];
                            while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                                $selnomedados = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$eselrel['id_'.strtolower($lfiltros['nomefiltro_nomes'])]."'";
                                $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnomedados)) or die (mysql_error());
                                $pastas = scandir($camaudio);
                                if(in_array($eselnome['nomefiltro_dados'], $pastas)) {
                                  $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                                }
                                else {
                                  mkdir("$camaudio/".$eselnome['nomefiltro_dados'], 0777);
                                  $camaudio = $camaudio."/".$eselnome['nomefiltro_dados'];
                                }
                                $ultimofilt = $lfiltros['nomefiltro_nomes'];
                            }
                            $criaarq = $camaudio."/".$lselup['nmes']."_".$lselup['ano'];
                            mkdir($criaarq, 0777);
                            $criaarq = $criaarq."/".$idup;
                            mkdir($criaarq, 0777);
                            $dir = scandir($camant);
                            $update = "UPDATE upload SET idrel_filtros='$idrel', camupload='$criaarq', semana='$semana' WHERE idupload='$idup'";
                            $eupdate = $_SESSION['query']($update) or die ("erro na query para executar o update dos dados");
                            $upfila = "UPDATE $fila SET idrel_filtros='$idrel', camaudio='$criaarq' WHERE idupload='$idup'";
                            $eupfila = $_SESSION['query']($upfila) or die ("erro na query de atualização da fila");

                            // lista monitorias realizadas e registros realizados
                            $tabelas = array('monitoria' => 'm', 'regmonitoria' => 'r');

                            foreach($tabelas as $tab => $alias) {
                                $selmoni = "SELECT * FROM $tab $alias
                                                    INNER JOIN $fila $aliasf ON $aliasf.id$fila = $alias.idfila
                                                    INNER JOIN upload u ON u.idupload = $aliasf.idupload
                                                    INNER JOIN conf_rel cr ON cr.idrel_filtros = u.idrel_filtros
                                                    INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                                                    WHERE u.idupload='$idup'";
                                $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta das monitorias realizadas para o upload");
                                while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                                    $grava = explode("/",$lselmoni['gravacao']);
                                    $file = explode(".",end($grava));
                                    $tpaudios = explode(",",$lselmoni['tpaudio']);
                                    if(in_array(strtolower($file[1]),$tpaudios)) {
                                        if($tab == "monitoria") {
                                            $monitorias[$lselmoni['id'.$tab]] = $criaarq."/".basename($lselmoni['gravacao']);
                                        }
                                        else {
                                            $regmonitorias[$lselmoni['id'.$tab]] = $criaarq."/".basename($lselmoni['gravacao']);
                                        }
                                    }
                                    else {
                                    }
                                    if(in_array(strtoupper($file[1]),$tpaudios)) {
                                        if($tab == "monitoria") {
                                            $monitorias[$lselmoni['id'.$tab]] = $criaarq."/".basename($lselmoni['gravacao']);
                                        }
                                        else {
                                            $regmonitorias[$lselmoni['id'.$tab]] = $criaarq."/".basename($lselmoni['gravacao']);
                                        }
                                    }

                                    //$audio = array_intersect($file, $tpaudios);
                                }
                            }

                            foreach($monitorias as $idmoni => $audiomoni) {
                                $upmoni = "UPDATE monitoria SET idrel_filtros='$idrel', gravacao='$audiomoni' WHERE idmonitoria='$idmoni'";
                                $eupmoni = $_SESSION['query']($upmoni) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o das monitorias realizadas");
                            }

                            foreach($regmonitorias as $idmoni => $audiomoni) {
                                $upmoni = "UPDATE regmonitoria SET idrel_filtros='$idrel', gravacao='$audiomoni' WHERE idregmonitoria='$idmoni'";
                                $eupmoni = $_SESSION['query']($upmoni) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o das monitorias realizadas");
                            }

                            //lista registros realizados


                            foreach($dir as $arq) {
                                if($arq == "." OR $arq == "..") {
                                }
                                else {
                                    copy($camant."/".$arq, $criaarq."/".$arq);
                                }
                            }
                            foreach($dir as $d) {
                                chmod($camant."/".$d, 0755);
                                $apagaarq = unlink($camant."/".$d);
                            }
                            $pastas = explode("/",$camant);
                            $camremove = $camant;
                            krsort($pastas);
                            foreach($pastas as $pasta) {
                                if($pasta == "") {
                                }
                                else {
                                    rmdir($camremove);
                                    $part = "/".$pasta;
                                    $camremove = str_replace($part, "", $camremove);
                                }
                            }
                        }
                    }
                    else {
                        $update = "UPDATE upload SET semana='$semana' WHERE idupload='$idup'";
                        $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o do upload");
                    }
                    if($eupdate) {
                        $imprel = "AtualizaÃƒÂ§ÃƒÂ£o do Relacionamento ''".$idrel."'' UPLOAD ''".$idup."'' efetuado com sucesso!!!";
                        header("location:$url&idrel=".$idrel."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
                    }
                    else {
                        $imprel = "Ococrreu um erro no processo de alteraÃƒÂ§ÃƒÂ£o, favor contatar o administrador!!!";
                        header("location:$url&idrel=".$idrel."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
                    }
                }
            }
        }
    }
}

if(isset($_POST['exclui'])) {
    $idup = implode(",",$_POST['idupimp']);
    $idrel = $_POST['relimp'.$idup];
    $idrelimp = $_POST['idrelimp'];
    $dataini = $_POST['dtiniimp'];
    $datafim = $_POST['dtfimimp'];
    if(count($_POST['idupimp']) > 1) {
        $imprel = "Só é possóvel executar 1 exclusão por vez!!!";
        header("location:$url&idrel=".$idrelimp."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
    }
    else {
        if($_SESSION['usuarioperfil'] != "01") {
            $msg = "O usuário não tem perfil para apagar uma importação, favor contatar o administrador!!!";
            header("location:$url&imprel=".$msg."#imp");
        }
        else {
            if($idup == "") {
                $imprel = "Favor selecionar 1 registro para relializar a exclusão!!!";
                header("location:$url&idrel=".$idrelimp."&dataini=".$dataini."&datafim=".$datafim."&imprel=".$imprel."#imp");
            }
            else {
                $selup = "SELECT tabfila FROM upload WHERE idupload='$idup'";
                $eselup = $_SESSION['fetch_array']($_SESSION['query']($selup)) or die (mysql_error());
                $tabfila = $eselup['tabfila'];
                $selmoni = "SELECT COUNT(*) as fila, COUNT(idmonitoria) as result FROM $tabfila
                            LEFT JOIN monitoria m ON m.idfila = $tabfila.id$tabfila
                            WHERE idupload='$idup'";
                $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta de monitorias");
                if($eselmoni['result'] >= 1) {
                    $moni++;
                }
                if($moni >= 1) {
                    ?>
                    <script type="text/javascript">
                        var exclui = confirm('Existem Monitoria realizadas para esta importação, você tem certeza que deseja excluir a importação, as monitorias serão apagadas!!!');
                        if(exclui == true) {
                            window.location = '/monitoria_supervisao/admin/apagaimporta.php?tabfila=<?php echo $tabfila;?>&idup=<?php echo $idup;?>';
                        }
                        else {
                            return false
                        }
                    </script>
                    <?php
                }
                else {
                    ?>
                    <script type="text/javascript">
                        var exclui = confirm('Você tem certeza que deseja excluir a importação!!!');
                        if(exclui == true) {
                            window.location = '/monitoria_supervisao/admin/apagaimporta.php?tabfila=<?php echo $tabfila;?>&idup=<?php echo $idup;?>';
                        }
                        else {
                            window.location = '<?php echo $url;?>';
                        }
                    </script>
                    <?php
                }
            }
        }
    }
}

if(isset($_POST['param'])) {
    $idparam = $_POST['id'];
    $idrel = $_POST['idrel'];
    $selparam = "SELECT * FROM param_moni WHERE idparam_moni=$idparam";
    $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam));
    if($eselparam['tipoimp'] == "A" || $eselparam['tipoimp'] == "S") {
        ?>
        <span style="font-weight: bold">AUDIOS:</span><input type="file" id="audioup<?php echo $idrel;?>" name="audioup<?php echo $idrel;?>" style="padding: 5px"/><br/>
        <input type="hidden" id="tipoimp<?php echo $idrel;?>" name="tipoimp<?php echo $idrel;?>" value="<?php echo $eselparam['tipoimp'];?>" />
        <?php
    }
    if($eselparam['tipoimp'] == "I" || $eselparam['tipoimp'] == "SD" || $eselparam['tipoimp'] == "L") {
        ?>
        <span style="font-weight: bold">ARQUIVO:</span><input type="file" id="arquivoup<?php echo $idrel;?>" name="arquivoup<?php echo $idrel;?>" style="padding: 5px"/><br/>
        <input type="hidden" id="tipoimp<?php echo $idrel;?>" name="tipoimp<?php echo $idrel;?>" value="<?php echo $eselparam['tipoimp'];?>" />
        <?php
    }
    if($eselparam['tipoimp'] == "LA") {
        ?>
        <span style="font-weight: bold">AUDIOS:</span><input type="file" id="audioup<?php echo $idrel;?>" name="audioup<?php echo $idrel;?>" style="padding: 5px"/><br/>
        <span style="font-weight: bold">ARQUIVOS:</span><input type="file" id="arquivoup<?php echo $idrel;?>" name="arquivoup<?php echo $idrel;?>" style="padding: 5px"/><br/>
        <input type="hidden" id="tipoimp<?php echo $idrel;?>" name="tipoimp<?php echo $idrel;?>" value="<?php echo $eselparam['tipoimp'];?>" />
        <?php
    }
}

?>