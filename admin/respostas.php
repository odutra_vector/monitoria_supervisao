<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

$idresp = $_GET['idresp'];
$idperg = $_GET['id'];
if($idresp != "") {
    $selresp = "SELECT * FROM pergunta WHERE idresposta='$idresp' AND idpergunta='$idperg'";
    $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na query de consulta da resposta cadastrada");
    $selperg = "SELECT * FROM pergunta WHERE idpergunta='$idperg'";
    $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da pergunta cadastrada");
}
else {
    $selperg = "SELECT * FROM pergunta WHERE idpergunta='$idperg'";
    $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da pergunta cadastrada");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
        $('#avaliacao').change(function() {
            var avalia = $(this).val();
            if(avalia == "TABULACAO") {
                $('#valor_resp').attr('value','');
                $('#valor_resp').attr('readonly',true);
            }
            else {
                $('#valor_resp').removeAttr("readonly");
            }
        })
        
        $("#cadastra").click(function() {
            var resp = $("#resp").val();
            var comple = $("#comple").val();
            var valor = $("#valor_resp").val();
            var avaliacao = $("#avaliacao").val();
            
            if(avaliacao == "TABULACAO") {
                if(resp == "") {
                    alert("O campo Resposta é obrigatórios");
                    return false;
                }
                else {
                }
            }
            else {
                if(resp == "" || (valor == "" && (avaliacao == "AVALIACAO" || avaliacao == "FG" || avaliacao == "FGM"))) {
                    alert("O campo Resposta e Valor são obrigatórios");
                    return false;
                }
                else {
                    
                }
            }
        })
        
        $("#altera").click(function() {
            var resp = $("#resp").val();
            var comple = $("#comple").val();
            var valor = $("#valor_resp").val();
            var avaliacao = $("#avaliacao").val();
            
            if(avaliacao == "TABULACAO") {
                if(resp == "") {
                    alert("O campo Resposta é obrigatórios");
                    return false;
                }
                else {
                }
            }
            else {
                if(resp == "" || (valor == "" && (avaliacao == "AVALIACAO" || avaliacao == "FG" || avaliacao == "FGM"))) {
                    alert("O campo Resposta e Valor são obrigatórios");
                    return false;
                }
                else {
                    
                }
            }
        });
        
        $("#idrel_origem").change(function() {
            var id = $(this).val();
            if(id == "loadfiltros") {
                $("#idrel_origem").load("/monitoria_supervisao/admin/cadresp.php",{loadfiltros:id});
            }
        });
        
        $("#idrel_filtro").change(function() {
            var id = $(this).val();
            if(id == "loadfiltros") {
                $("#idrel_filtro").load("/monitoria_supervisao/admin/cadresp.php",{loadfiltros:id});
            }
        });
    })

</script>
</head>
<body>
    <form action="cadresp.php" method="post">
<table width="851">
  <tr>
    <td class="corfd_ntab" colspan="8" align="center"><strong>CADASTRO DE RESPOSTA</strong></td>
  </tr>
  <tr>
    <td  width="200" class="corfd_coltexto"><strong>PERGUNTA</strong></td>
    <td class="corfd_colcampos" colspan="5"><?php echo $eselperg['descripergunta'];?></td>
    <td class="corfd_coltexto"><strong>VALOR</strong></td>
    <td class="corfd_colcampos"><?php echo $eselperg['valor_perg'];?></td>
  </tr>
  <tr>
    <td width="160" class="corfd_coltexto"><strong>RESPOSTA</strong></td>
    <td colspan="7" class="corfd_colcampos"><input type="hidden" name="idresp" id="idresp" value="<?php if($idresp != "") { echo $idresp;} else {}?>" /><input name="idperg" id="idperg" type="hidden" value="<?php echo $idperg;?>" /><input type="text" name="resp" id="resp" maxlength="1000" style="width:670px; border: 1px solid #9CF" value="<?php if($_GET['idresp'] != "") { echo $eselresp['descriresposta']; } else {}?>" /></td>
  </tr>
  <tr>
    <td height="28" class="corfd_coltexto"><strong>COMPLEMENTO</strong></td>
    <td colspan="7" class="corfd_colcampos"><textarea name="comple" id="comple" maxlength="1000" style="width:670px; border: 1px solid #9CF; height:60px"><?php if($idresp != "") { echo $eselresp['compleresposta'];} else {}?></textarea>
    </td>
  </tr>
  <tr>
    <td height="24" class="corfd_coltexto"><strong>Avaliação</strong></td>
    <td width="223" class="corfd_colcampos">
            <?php
            if($eselperg['valor_perg'] == "" OR $eselperg['valor_perg'] == "NULL") {
                ?>
                <input type="text" style="width:150px; border: 1px solid #9CF; text-align:center" value="TABULACAO" id="avaliacao" name="avaliacao" readonly="readonly" />
                <?php
            }
            else {
                ?>
                <select name="avaliacao" id="avaliacao">
                <?php

                if($idresp != "") {
                }
                else {
                    echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\">SELECIONE...</option>";
                }
                if($eselperg['modcalc'] == "PERC") {
                    $tipos = array('AVALIACAO_OK','PERC','FG','FGM');
                }
                else {
                    $tipos = array('AVALIACAO_OK','AVALIACAO','SUPERACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','TABULACAO','FG','FGM');
                }
                foreach($tipos as $t) {
                    if($t == $eselresp['avalia']) {
                            echo "<option value=\"".$t."\" selected=\"selected\" >".$t."</option>";
                    }
                    else {
                            echo "<option value=\"".$t."\">".$t."</option>";
                    }
                }
                ?>
                </select>
                <?php
            }
            ?>
    </td>
    <td width="139" class="corfd_coltexto"><strong>Posição</strong></td>
    <td width="124" class="corfd_colcampos">
    	<select name="posicao" id="posicao">
            <?php
            $selpos = "SELECT MAX(posicao) as posicao, COUNT(*) as result FROM pergunta WHERE idpergunta='$idperg'";
            $lselpos = $_SESSION['fetch_array']($_SESSION['query']($selpos)) or die ("erro na query de consulta das respostas cadastradas");
            $posimax = $lselpos['posicao'];
            for($i = 0; $i <= ($posimax + 1); $i++) {
                $selcad = "SELECT idresposta, posicao, COUNT(*) as result FROM pergunta WHERE idpergunta='$idperg' AND posicao='$i'";
                $eselcad = $_SESSION['fetch_array']($_SESSION['query']($selcad)) or die ("erro na query de consulta da posicao");
                if($eselcad['result'] >= 1) {
                    if($idresp != "") {
                        if($idresp == $eselcad['idresposta']) {
                            echo "<option value=\"".$i."\" selected=\"selected\">".$i."</option>";
                        }
                        else {
                            if($i == "0") {
                                echo "<option value=\"".$i."\">".$i."</option>";
                            }
                            else {
                            }
                        }
                    }
                    else {
                    }
                }
                else {
                    echo "<option value=\"".$i."\">".$i."</option>";
                }
            }
            ?>
        </select>
    </td>
    <td width="88" class="corfd_coltexto"><strong>Ativo</strong></td>
    <td width="79" class="corfd_colcampos">
    	<select name="ativo" id="ativo">
        <?php
        $ativo = array('S','N');
        foreach($ativo as $atv) {
            if($eselresp['ativo_resp'] == $atv) {
            	echo "<option value=\"".$atv."\" selected=\"selected\">".$atv."</option>";
            }
            else {
            	echo "<option value=\"".$atv."\">".$atv."</option>";
            }
        }    
        ?>
        </select>
    </td>
    <td width="78" class="corfd_coltexto"><strong>Padrão</strong></td>
    <td width="68" class="corfd_colcampos">
        <select name="padrao" id="padrao">
            <?php
            $padrao = array('N','S');
            foreach($padrao as $pa) {
                if($eselresp['padrao'] == $pa) {
                    echo "<option value=\"$pa\" selected=\"selected\">$pa</option>";
                }
                else {
                    echo "<option value=\"$pa\">$pa</option>";
                }
            }
            ?>
        </select>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto" rowspan="2"><strong>Avalia Planilha</strong></td>
    <td class="corfd_colcampos" rowspan="2">
    	<select name="idplanilha" id="idplanilha" style="width:220px">
            <?php
            if($idresp != "") {
                if($eselresp['avaliaplan'] == "") {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                }
                else {
                    echo "<option value=\"\">SELECIONE...</option>";
                }
            }
            else {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
            }
            $selplan = "SELECT DISTINCT(idplanilha),descriplanilha FROM planilha WHERE ativo='S'";
            $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das planilhas cadastradas");
            while($lselplan = $_SESSION['fetch_array']($eselplan)) {
                if($lselplan == "") {
                        echo "<option value=\"\" disabled=\"disabled\">NÃO EXISTEM PLANILHAS CADASTRADAS</option>";
                }
                else {
                        if($eselresp['avaliaplan'] == $lselplan['idplanilha']) {
                        echo "<option value=\"".$lselplan['idplanilha']."\" selected=\"selected\">".$lselplan['descriplanilha']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lselplan['idplanilha']."\">".$lselplan['descriplanilha']."</option>";
                    }
                }
            }
            ?>
    	</select>
    </td>
    <td class="corfd_coltexto"><strong>Relaciona Origem</strong></td>
    <td class="corfd_colcampos" colspan="5">
        <select name="idrel_origem" id="idrel_origem" style="width:350px">
        <?php
            if($idresp != "") {
                if($eselresp['idrel_origem'] != "") {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                    echo "<option value=\"".$eselresp['idrel_origem']."\" selected=\"selected\">".nomeapres($eselresp['idrel_origem'])."</option>";
                    echo "<option value=\"loadfiltros\" >CARREGAR...</option>";
                }
                else {
                    echo "<option value=\"\" >SELECIONE...</option>";
                    echo "<option value=\"loadfiltros\" >CARREGAR...</option>";
                }
            }
            else {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                echo "<option value=\"loadfiltros\" >CARREGAR...</option>";
            }
            ?>
        </select>
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Relacionamento</strong></td>
    <td class="corfd_colcampos" colspan="5">
        <select name="idrel_filtro" id="idrel_filtro" style="width:350px">
            <?php
            if($idresp != "") {
                if($eselresp['idrel_avaliaplan'] != "") {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                    echo "<option value=\"".$eselresp['idrel_avaliaplan']."\" selected=\"selected\">".nomeapres($eselresp['idrel_avaliaplan'])."</option>";
                    echo "<option value=\"loadfiltros\" >CARREGAR...</option>";
                }
                else {
                    echo "<option value=\"\" >SELECIONE...</option>";
                    echo "<option value=\"loadfiltros\" >CARREGAR...</option>";
                }
            }
            else {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
                echo "<option value=\"loadfiltros\" >CARREGAR...</option>";
            }
            ?>
        </select>
    </td>
  </tr>
  <tr id="trvalor">
    <td class="corfd_coltexto"><strong>Valor Resp.</strong></td>
    <td class="corfd_colcampos" colspan="7">
        <input name="valor_resp" id="valor_resp" 
            <?php 
            if($eselresp['avalia'] == "TABULACAO") { 
                echo "readonly=\"readonly\"";    
            } 
            else {
            }
            ?> 
           value="<?php if($idresp != "") { echo $eselresp['valor_resp'];} else {}?>" type="text" style="width:50px; border: 1px solid #9CF; text-align:center"  /></td>
  </tr>
  <tr>
    <td colspan="9">
    <?php
	if($idresp != "") {
	?>
    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altera" id="altera" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novo" type="submit" value="Novo" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="finaliza" type="submit" value="Finaliza" />
    <?php
	}
	else {
	?>
    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastra" type="submit" value="Cadastrar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="volta" type="submit" value="Voltar" />
    <?php
	}
	?>
    </td>
  </tr>
</table><font color="#FF0000"><strong><?php echo $_GET['msgi']; echo $_GET['msg'];?></strong></font></form><hr />
<table width="1021">
  <tr>
    <td class="corfd_ntab" align="center" colspan="8"><strong>RESPOSTAS CADASTRADAS</strong></td>
  </tr>
  <tr>
    <td width="51" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="250" align="center" class="corfd_coltexto"><strong>RESPOSTA</strong></td>
    <td width="115" align="center" class="corfd_coltexto"><strong>AVALIAÇÃO</strong></td>
    <td width="91" align="center" class="corfd_coltexto"><strong>VALOR</strong></td>
    <td width="34" align="center" class="corfd_coltexto"><strong>POS.</strong></td>
    <td width="50" align="center" class="corfd_coltexto"><strong>ATIVO</strong></td>
    <td width="177" align="center" class="corfd_coltexto"><strong>PLANILHA VINC.</strong></td>
    <td width="100" align="center" class="corfd_coltexto"><strong>RELACIONA</strong></td>
    <td width="73"></td>
  </tr>
  <?php
  $respcad = "SELECT pe.avalia, pe.idpergunta, pe.idresposta, pe.valor_resp, pe.descriresposta, pe.ativo_resp, pe.posicao, pe.avaliaplan, pe.idrel_avaliaplan, p.descriplanilha 
              FROM pergunta pe 
              LEFT JOIN planilha p ON p.idplanilha = pe.avaliaplan 
              WHERE pe.idpergunta='$idperg' GROUP BY pe.idresposta ORDER BY posicao";
  $erespcad = $_SESSION['query']($respcad) or die ("erro na query de consulta das respostas cadastradas"); 
  while($lrespcad = $_SESSION['fetch_array']($erespcad)) {
    if($lrespcad['idresposta'] == "0") {
  ?>
    <tr>
        <td align="center" class="corfd_colcampos" colspan="8"><strong>NÃO EXISTEM RESPOSTAS CADASTRADAS</strong></td>
    </tr>
    <?php
    }
    else {
    ?>
    <form action="cadresp.php" method="post">
      <tr>
        <td class="corfd_colcampos" align="center"><input name="idresp" id="idresp" type="hidden" value="<?php echo $lrespcad['idresposta'];?>" /><input name="idperg" id="perg" type="hidden" value="<?php echo $lrespcad['idpergunta'];?>" /><?php echo $lrespcad['idresposta'];?></td>
        <td class="corfd_colcampos" align="center"><?php echo $lrespcad['descriresposta'];?></td>
        <td class="corfd_colcampos" align="center"><?php echo $lrespcad['avalia'];?></td>
        <td class="corfd_colcampos" align="center"><?php echo $lrespcad['valor_resp'];?></td>
        <td class="corfd_colcampos" align="center"><?php echo $lrespcad['posicao'];?></td>
        <td class="corfd_colcampos" align="center"><?php echo $lrespcad['ativo_resp'];?></td>
        <td class="corfd_colcampos" align="center"><?php echo $lrespcad['descriplanilha'];?></td>
        <td class="corfd_colcampos" align="center" ><input style="border: 0px; width: 100px" type="text" name="resposta" title="<?php echo nomeapres($lrespcad['idrel_avaliaplan']);?>" value="<?php echo $lrespcad['idrel_avaliaplan'];?>" /></td>
        <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="carrega" type="submit" value="Carregar" /></td>
      </tr>
    </form>
  <?php
    }
  }
  ?>
</table>
</body>
</html>
