<?php

session_start();
header('Content-type: text/html; charset=utf-8');
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");

protegePagina();

$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);

$iduser = $_GET['id'];
$idsrel = $_GET['idsrel'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.6.2.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script text="text/javascript">
$(document).ready(function() {
     var iduser = '<?php echo $iduser;?>';
     var idsrel = '<?php echo $idsrel;?>';
     $("#vincuser").click(function() {
          var acoes = $("#acoes").val();
          if(acoes == null) {
               alert('Favor selecionar um tipo de acão para vincular usuários');
          }
          else {
               for(var i = 0;i < acoes.length;i++) {
                    acoes[i]
                    if(acoes[i] == "FLUXO") {
                         var fluxo = 1;
                    }
               }
               if(fluxo == 1) {
                    if($("#fluxo").val() == "") {
                         alert('Favor selecionar o fluxo para relacionar');
                    }
                    else {
                         var relfluxo = $("#relfluxo").val();
                         if(relfluxo == null) {
                              alert('Favor selecionar os motivos do fluxo escolhido que o usuário será vinculado');
                         }
                         else {
                              $.blockUI({ message: '<strong>AGUARDE EXECUTANDO A OPERAÇÃO...</strong>', css: { 
                              border: 'none', 
                              padding: '15px', 
                              backgroundColor: '#000', 
                              '-webkit-border-radius': '10px', 
                              '-moz-border-radius': '10px', 
                              opacity: .5,
                              color: '#fff'
                              }})
                              $.post("cadrelenvmail.php",{vincuser:"vincuser",idsrel:idsrel,iduser:iduser,acoes:acoes,tipouser:'<?php echo $_GET['tipouser'];?>',relfluxo:relfluxo,comando:'<?php echo $_GET['comando'];?>'}, function(ret) {
                                   $.unblockUI();
                                   alert(ret);
                              });
                         }
                    }
               }
               else {
                    $.blockUI({ message: '<strong>AGUARDE EXECUTANDO A OPERAÇÃO...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }})
                    $.post("cadrelenvmail.php",{vincuser:"vincuser",idsrel:idsrel,iduser:iduser,acoes:acoes,tipouser:'<?php echo $_GET['tipouser'];?>',comando:'<?php echo $_GET['comando'];?>'}, function(ret) {
                        $.unblockUI();
                        alert(ret);
                    });
               }
          }
     });
     
     $("#fluxo").change(function() {
          var fluxo = $(this).val();
          $("#relfluxo").load("cadrelenvmail.php",{fluxo:"fluxo",idfluxo:$("#fluxo").val()});
     })
});
</script>
</head>
     <body style="font-family:Verdana, Geneva, sans-serif;font-size:11px;">
          <div>
               <table width="300px" align="center">
                    <tr>
                         <td class="corfd_coltexto" align="center"><strong>VINCULAR AÇÕES</strong></td>
                    </tr>
                    <tr style="height: 50px;background-color: #EAEAEA">
                         <td align="center">
                              <select multiple="multiple" name="acoes[]" id="acoes" style="height: 50px; width: 250px; text-align: center">
                                   <option value="FG">FG</option>
                                   <option value="IMPORTACAO">IMPORTAÇÃO</option>
                                   <option value="FLUXO">FLUXO</option>
                              </select>
                         </td>
                    </tr>
                    <tr style="background-color: #EAEAEA">
                         <td align="center">
                              <select name="fluxo" id="fluxo" style="width: 250px">
                                   <option value="" disabled="disabled" selected="selected">SELECIONE...</option>
                                   <?php
                                   $selfluxo = "SELECT idfluxo,nomefluxo FROM fluxo";
                                   $eselfluxo = $_SESSION['query']($selfluxo) or die (mysql_error());
                                   while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                                        ?>
                                        <option value="<?php echo $lselfluxo['idfluxo'];?>"><?php echo $lselfluxo['nomefluxo'];?></option>
                                        <?php
                                   }
                                   ?>
                              </select><br/>
                              <select name="relfluxo" id="relfluxo" multiple="multiple" style="width: 250px; height: 150px">
                                   
                                   
                              </select>
                         </td>
                    </tr>
                    <tr>
                         <td align="center"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)"  name="vincuser" id="vincuser" type="submit" value="VINCULAR/DESVINCULA" /></td>
                    </tr>
               </table>
          </div>
     </body>
</html>