<?php

session_start();
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once('functionsadm.php');

$idrel = $_POST['idrel'];
$idperiodo = $_POST['idperiodo'];
$speriodo = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
$eperiodo = $_SESSION['fetch_array']($_SESSION['query']($speriodo)) or die (mysql_error());
if($eperiodo['dtinictt'] == "" || $eperiodo['dtfimctt'] == "") {
    $datas[] = substr($eperiodo['dataini'],0,4)."-".substr($eperiodo['dataini'],5,2)."-01";
    $datas[] = ultdiames(substr($datas[0],8,2).substr($datas[0],5,2).substr($datas[0],0,4));
}
else {
    $datas[] = $eperiodo['dtinictt'];
    $datas[] = $eperiodo['dtfimctt'];
}

?>
<script type="text/javascript">
    $(document).ready(function() {
        $("a[id*='excluir']").click(function() {
            var confirma = confirm("Você deseja excluir a seleção?");
            if(confirma) {
                var valor = $(this).attr('name');
                var idup = valor.substr(7);
                var idrel = $("#idrelselecao").val();
                var idperiodo = $("#periodo").val();
                $.post("/monitoria_supervisao/admin/cadselecao.php", {excluir:"excluir",idup:idup},function(retorno) {
                    alert(retorno);
                    $("#dadosorigem").load("/monitoria_supervisao/admin/histselecao.php",{idrel:idrel,idperiodo:idperiodo});
                })
            }
            else {
                return false;
            }
        })
    })
</script>
<fieldset style="height:250px;padding-top:10px; border: 2px solid #999"><legend class="corfd_ntab" style="font-weight: bold;width:200px; border: 2px solid #999;padding: 8px; margin-left: 5px">ARQUIVOS ENVIADOS E TRATADOS</legend>
    <div style="height:230px; width:1020px; overflow:auto">
    <?php
    $selfiltros = "SELECT nomefiltro_nomes as nome FROM filtro_nomes ORDER BY nivel DESC";
    $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
    while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
        $filtros[] = $lfiltros['nome'];
    }

    $caminho = "/monitoria_supervisao/basesaleatoria/".$_SESSION['nomecli'];
    $selrel = "SELECT * FROM rel_filtros WHERE idrel_filtros='$idrel'";
    $eselrel = $_SESSION['query']($selrel) or die (mysql_error());
    $nselrel = $_SESSION['num_rows']($eselrel);
    if($nselrel >= 1) {
        while($lselrel = $_SESSION['fetch_array']($eselrel)) {
            foreach($filtros as $col) {
                $selnome = "SELECT nomefiltro_dados as nome FROM filtro_dados WHERE idfiltro_dados='".$lselrel['id_'.strtolower($col)]."'";
                $eselnome = $_SESSION['query']($selnome) or die (mysql_error());
                $nselnome = $_SESSION['num_rows']($eselnome);
                if($nselnome >= 1) {
                    while($lnome = $_SESSION['fetch_array']($eselnome)) {
                        $caminho .= "/".$lnome['nome'];
                    }
                }
            }
        }
    }
    $caminho .= "/".$eperiodo['nmes']."_".$eperiodo['ano'];
    $arquivos = scandir($caminho);
    ?>
    <table border = "0" WIDTH="1100">
        <thead>
            <tr class="corfd_coltexto" style="font-weight: bold">
                <?php
                if($_SESSION['user_tabela'] == "user_adm") {
                    echo "<td></td>";
                }
                $c = 0;
                $selcol = "SHOW COLUMNS FROM upselecao";
                $eselcol = $_SESSION['query']($selcol) or die (mysql_erro());
                while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                    if($lselcol['Field'] != "tab_user") {
                        $columns[] = $lselcol['Field'];
                        $c++;
                        if($c < 4) {
                            $align = "align=\"center\"";
                        }
                        else {
                            $align = "";
                        }
                        if($lselcol['Field'] == "nomesistema") {
                            ?>
                            <th <?php echo $align;?>>SELEÇÃO GERADA</th>
                            <?php
                        }
                        else {
                            if($lselcol['Field'] == "idupselecao") {
                                ?>
                                <th <?php echo $align;?> width="50px"><?php echo "IDUP";?></th>
                                <?php
                            }
                            else {
                                if($lselcol['Field'] == "data") {
                                    ?>
                                    <th <?php echo $align;?> width="60px"><?php echo $lselcol['Field'];?></th>
                                    <?php
                                }
                                else {
                                    if($lselcol['Field'] == "caminhoarq") {
                                    }
                                    else {
                                        if($lselcol['Field'] == "qtdesel") {
                                            ?>
                                            <th <?php echo $align;?>>QTDESOL</th>
                                            <?php
                                        }
                                        else {
                                            if($lselcol['Field'] == "qtdeefetiva") {
                                                ?>
                                                <th <?php echo $align;?>>QTDESEL</th>
                                                <?php
                                            }
                                            else {
                                            ?>
                                            <th <?php echo $align;?>><?php echo strtoupper(str_replace("id","",$lselcol['Field']));?></th>
                                            <?php
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
                <th width="80">MAPA ORIGINAL</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $c = 0;
            $selup = "SELECT us.idupselecao,us.data,us.hora,us.qtdeenv,us.descarte,us.qtdesel,us.qtdeexced,us.qtdeselexced,us.qtdeefetiva,us.nomearquivo,us.nomesistema,tab_user,iduser FROM upselecao us
                      INNER JOIN selecao s ON s.idupselecao = us.idupselecao
                      WHERE datactt BETWEEN '$datas[0]' AND '$datas[1]' AND idrel_filtros='$idrel' GROUP BY s.idupselecao ORDER BY us.idupselecao DESC";
            $eselup = $_SESSION['query']($selup) or die (mysql_error());
            $nup = $_SESSION['num_rows']($eselup);
            if($nup >= 1) {
                while($lselup = $_SESSION['fetch_array']($eselup)) {
                ?>
                <tr class="corfd_colcampos">
                    <?php
                    if($_SESSION['user_tabela'] == "user_adm") {
                        ?>
                        <td><a name="excluir<?php echo $lselup['idupselecao'];?>" id="excluir" style=" text-decoration: none"><img src="/monitoria_supervisao/images/exit.png"/></a></td>
                        <?php
                    }
                    foreach($columns as $colsel) {
                        
                        $c = 0;
                        if($c < 4) {
                            $align = "align=\"center\"";
                        }
                        else {
                            $align = "";
                        }
                        if($colsel != "tab_user") {
                            if($colsel == "iduser") {
                                $seluser = "SELECT nome".$lselup['tab_user']." FROM ".$lselup['tab_user']." WHERE id".$lselup['tab_user']."='$lselup[$colsel]'";
                                $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die (mysql_error());
                                ?>
                                <td <?php echo $align;?>><?php echo $eseluser['nome'.$lselup['tab_user']];?></td>
                                <?php
                            }
                            else {
                                if($colsel == "nomesistema") {
                                    ?>
                                    <td <?php echo $align;?>><a target="_blank" href="<?php echo $caminho."/selecao-".$lselup[$colsel];?>" style="text-decoration:none;color:#000"><?php echo "selecao-".$lselup[$colsel];?></a></td>
                                    <?php
                                }
                                else {
                                    if($colsel == "caminhoarq") {
                                    }
                                    else {
                                        if($colsel == "data") {
                                            ?>
                                            <td <?php echo $align;?>><?php echo banco2data($lselup[$colsel]);?></td>
                                            <?php
                                        }
                                        else {
                                            ?>
                                            <td <?php echo $align;?>><?php echo $lselup[$colsel];?></td>
                                            <?php
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ?>
                    <td><a target="_blank" href="<?php echo $caminho."/".$lselup['nomesistema'];?>" style="text-decoration:none;color:#000"><?php echo $lselup['nomesistema'];?></a></td>
                </tr>
                <?php
                }
            }
            ?>
        </tbody>
    </table>
    </div>
</fieldset>
