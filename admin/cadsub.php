<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$jaberta = $_POST['jaberta'];
$idsub = $_POST['idsub'];
$sub = strtoupper(trim($_POST['descri']));
if($_POST['valor'] == "") {
  $valor = "NULL";
}
else {
  $valor = str_replace(",",".", $_POST['valor']);
}
$comple = $_POST['comple'];
if($_POST['ativo'] == "SIM") {
    $ativo = "S";
}
if($_POST['ativo'] == "NAO") {
    $ativo = "N";
}
$caracter = "[]$[><}{)(:;!?*%&#@]";

if(isset($_POST['cadastra'])) {
  if($sub == "") {
      $msg = "Todos os campos precisam estar preenchidos para efetuar o cadastro!!!";
      header("location:admin.php?menu=subgrupo&msg=".$msg);
      break;
  }
  if(eregi($caracter, $valor)) {
      $msg = "O campo ''Valor'' sÃ³ pode conter nÃºmeros e virgula!!!";
      header("location:admin.php?menu=subgrupo&msg=".$msg);
      break;
  }
  if($valor < 0) {
      $msg = "O campo ''Valor'' nÃ£o pode ser negativo!!!";
      header("location:admin.php?menu=subgrupo&msg=".$msg);
      break;
  }
  else {
      $insert = "INSERT INTO subgrupo (descrisubgrupo, complesubgrupo, valor_sub, ativo, idpergunta) VALUE ('$sub', '$comple', $valor, 'S', '000000')";
      $exeinsert = $_SESSION['query']($insert) or die (mysql_error());
      if($exeinsert) {
          $cons = mysql_insert_id();
          header("location:admin.php?menu=relsub&id=".$cons);
      }
      else {
          $msg = "Ocorreu algum erro no processo de cadastro, favor contatar o administrador!!!";
          header("location:admin.php?menu=subgrupo&msg=".$msg);
      }
   }
}

if(isset($_POST['altera'])) {
  $consult = "SELECT * FROM subgrupo WHERE idsubgrupo='$idsub'";
  $econs = $_SESSION['fetch_array']($_SESSION['query']($consult)) or die (mysql_error());
  $soma = "SELECT SUM( DISTINCT valor_perg ) as soma FROM pergunta WHERE idpergunta IN (SELECT DISTINCT idpergunta FROM subgrupo WHERE idsubgrupo='$idsub')";
  $esoma = $_SESSION['fetch_array']($_SESSION['query']($soma)) or die (mysql_error());
  if($sub == "") {
      $msgi = "Todos os campos precisam estar preenchidos para alteraÃ§ao do Sub-grupo!!!";
      header("location:admin.php?menu=subdet&msgi=".$msgi."&id=".$idsub."&jaberta=".$jaberta);
      break;
  }
  if(eregi($caracter, $v_sub)) {
      $msgi = "O campo ''Valor'' so pode conter numeros e virgula!!!";
      header("location:admin.php?menu=subdet&msgi=".$msgi."&id=".$idsub."&jaberta=".$jaberta);
      break;
  }
  if($sub == $econs['descrisubgrupo'] && $valor == $econs['valor'] && $comple == $econs['complesubgrupo'] && $ativo == $econs['ativo']) {
      $msgi = "Nenhum campo foi alterado, favor verificar!!!";
      header("location:admin.php?menu=subdet&msgi=".$msgi."&id=".$idsub."&jaberta=".$jaberta);
      break;
  }
  if($valor < $esoma['soma']) {
      $msgi = "O valor do ''Sub-Grupo'' nÃ£o pode ser menor que a soma das perguntas relacionadas!!!";
      header("location:admin.php?menu=subdet&msgi=".$msgi."&id=".$idsub."&jaberta=".$jaberta);
      break;
  }
  else {
      $alter = "UPDATE subgrupo SET descrisubgrupo='$sub', complesubgrupo='$comple', valor_sub=$valor, ativo='$ativo' WHERE idsubgrupo='$idsub'";
      $exealter = $_SESSION['query']($alter) or die (mysql_error());
      if($exealter) {
	  header("location:admin.php?menu=relsub&id=".$idsub."&jaberta=".$jaberta);
      }
      else {
	  $msgi = "Ocorreu algum erro no processo de alteraÃ§ao, favor contatar um administrador!!!";
	  header("location:admin.php?menu=subdet&msg=".$msgi."&id=".$idsub."&jaberta=".$jaberta);
	  break;
      }
  }      
}
if(isset($_POST['alterarel'])) {
      $idsub = $_POST['idsub'];
      header("location:admin.php?menu=relsub&id=".$idsub);
}
if(isset($_POST['volta'])) {
  header("location:admin.php?menu=subgrupo");
}
?>