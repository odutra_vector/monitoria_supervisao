<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
?>

<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.4.4.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("input[id*='cadastra']").click(function() {
           var submit = $(this).attr("id");
           if(submit == "cadastratipo") {
               var nome = $("#nome").val();
               var vinculo = $("#vinculo").val();
               if(nome == ""|| vinculo == "") {
                alert("Os campos Nome e Vinculo, não podem estar vazios");
                return false;
               }
           }
           if(submit == "cadastramot") {
               var nome = $("#nomemot").val();
               var tipocad = $("#tipocad").val();
               var acesso = $("#acesso").val();
               if(nome == ""|| tipocad == ""||acesso == "") {
                   alert("Para cadastro de motivo é necessário informar Nome, Tipo e Acesso");
                   return false;
               }
           }
        });
    });
</script>
<div id="conteudo" class="corfd_pag">
<form action="cadtipomotivo.php" method="post">
<table width="276">
  <tr>
    <td align="center" class="corfd_ntab" colspan="2"><strong>CADASTRO DE TIPOS DE MOITVOS</strong></td>
  </tr>
  <tr>
    <td width="62" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="202" class="corfd_colcampos"><input style="width:200px; border: 1px solid #9CF" maxlength="50" name="nome" id="nome" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Vinculo</strong></td>
    <td class="corfd_colcampos">
        <select name="vinculo" id="vinculo">
    <option value="">Selecione...</option>
    <option value="moni_pausa">MONITOR</option>
    <option value="regmonitoria">REGISTROS</option>
    <option value="monitoria">MONITORIA</option>
    <option value="automatico">AUTOMATICO</option>
    </select>
    </td>
  </tr>
  <tr>
  	<td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastratipo" type="submit" value="Cadastrar" /></td>
  </tr> 
  </tr>
</table>
<font color="#FF0000"><strong><?php echo $_GET['msgt']; ?></strong></font>
</form><br />
<font color="#FF0000"><strong><?php echo $_GET['msgti']; ?></strong></font>
<table width="600">
  <tr>
    <td align="center" class="corfd_ntab" colspan="4"><strong>TIPOS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td align="center" class="corfd_coltexto"><strong>NOME</strong></td>
    <td align="center" class="corfd_coltexto"><strong>VINCULO</strong></td>
    <td align="center" class="corfd_coltexto"><strong>ATIVO</strong></td>
    <td width="200px"></td>
  </tr>
  <?php
  $seltipos = "SELECT * FROM tipo_motivo";
  $eseltipos = $_SESSION['query']($seltipos) or die ("erro na query de consulta dos tipos cadastrados");
  while($lseltipo = $_SESSION['fetch_array']($eseltipos)) {
	  echo "<form action=\"cadtipomotivo.php\" method=\"post\">";
	  echo "<tr>";
	  echo "<td class=\"corfd_colcampos\"><input style=\"width:50px; border: 1px solid #9Cf; text-align:center\" name=\"id\" readonly=\"readonly\" type=\"text\" value=\"".$lseltipo['idtipo_motivo']."\" /></td>";
	  echo "<td class=\"corfd_colcampos\"><input style=\"width:200px; border: 1px solid #9Cf; text-align:center\" name=\"nome\" type=\"text\" value=\"".$lseltipo['nometipo_motivo']."\" /></td>";
	  echo "<td class=\"corfd_colcampos\"><select style=\"width:100px;\" name=\"vinculo\">";
	  $vinculaop = array('regmonitoria','moni_pausa','monitoria','automatico');
	  foreach($vinculaop as $vincula) {
		  if($vincula == $lseltipo['vincula']) {
                      echo "<option value=\"".$vincula."\" selected=\selected\">".$vincula."</option>";
		  }
		  else {
		  	echo "<option value=\"".$vincula."\">".$vincula."</option>";
		  }
	  }
	  echo "</select></td>";
	  echo "<td class=\"corfd_colcampos\"><select name=\"ativo\">";
	  echo "<option value=\"".$lseltipo['ativo']."\" selected=\"selected\">".$lseltipo['ativo']."</option>";
	  $ativoop = array('S','N');
	  foreach($ativoop as $ativo) {
		  if($ativo == $lseltipo['ativo']) {
		  }
		  else {
			  echo "<option value=\"".$ativo."\">".$ativo."</option>";
		  }
	  }
	  echo "</select></td>";
	  echo "<td width=\"200px\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)\" name=\"altera\" type=\"submit\" value=\"alterar\" /> <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"apagar\" /></td>";
	  echo "</tr></form>";
  }
  ?>
</table><br />

<hr />
<form action="cadmotivo.php" method="post">
<table width="266" border="0" id="motivo">
  <tr>
    <td class="corfd_ntab" colspan="2" align="center"><strong>CADASTRO DE MOTIVOS</strong></td>
  </tr>
  <tr>
    <td width="48" class="corfd_coltexto"><strong>Nome</strong></td>
        <td width="208" class="corfd_colcampos"><input style="width:200px; border: 1px solid #9CF" maxlength="50" name="nome" id="nomemot" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tipo</strong></td>
    <td class="corfd_colcampos"><select name="tipo" id="tipocad">
    <option selected="selected" disabled="disabled" value="">Selecione um TIPO</option>
    <?php
    $stipo = "SELECT * FROM tipo_motivo";
    $estipo = $_SESSION['query']($stipo) or die ("erro na query de seleção dos ''tipos''");
    while($lstipo = $_SESSION['fetch_array']($estipo)) {
	echo "<option value=\"".$lstipo['idtipo_motivo']."\">".$lstipo['nometipo_motivo']."</option>";
    }
    ?>
    </select>
    </td>
  </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Acesso</strong></td>
        <td class="corfd_colcampos">
            <select name="acesso" id="acesso">
                <option value="E">EXTERNO</option>
                <option value="I">INTERNO</option>
            </select>
        </td>
    </tr>
    <tr class="qtde" class="corfd_colcampos">
        <td class="corfd_coltexto"><strong>Quantidade</strong></td>
        <td class="corfd_colcampos"><input style="width:50px; border: 1px solid black; text-align:center; border: 1px solid #9CF" type="text" name="qtde" id="qtde" maxlength="3" /></td>
    </tr>
    <tr class="intervalo">
        <td class="corfd_coltexto"><strong>Tempo</strong></td>
        <td class="corfd_colcampos"><input style="width:50px; border: 1px solid #000; text-align:center; border: 1px solid #9CF" maxlength="5" type="text" name="tempo" id="tempo" tittle="Tempo em segundos" /></td>
    </tr>
  </tr>
  <tr>
        <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastramot" type="submit" value="Cadastrar" /></td>
  </tr>
</table>
<font color="#FF0000"><strong><?php echo $_GET['msgm']; ?></strong></font>
</form><br />
<font color="#FF0000"><strong><?php echo $_GET['msgmi']; ?></strong></font>
<table width="500" border="0">
  <tr>
    <td class="corfd_ntab" align="center" colspan="7"><strong>MOTIVOS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="36" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="117" align="center" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="89" align="center" class="corfd_coltexto"><strong>Tipo</strong></td>
    <td width="70" align="center" class="corfd_coltexto"><strong>Acesso</strong></td>
    <td width="43" align="center" class="corfd_coltexto"><strong>Qtde.</strong></td>
    <td width="73" align="center" class="corfd_coltexto"><strong>Tempo</strong></td>
    <td width="43" align="center" class="corfd_coltexto"><strong>Ativo</strong></td>
    <td width="0"></td>
    <td width="11"></td>
  </tr>
  <?php
  $selmotivo = "SELECT * FROM motivo";
  $emotivo = $_SESSION['query']($selmotivo) or die (mysql_error());
  while($lmotivo = $_SESSION['fetch_array']($emotivo)) {
	  echo "<form action=\"cadmotivo.php\" method=\"POST\">";
	  echo "<tr>";
	  	echo "<td class=\"corfd_colcampos\"><input style=\"width:50px; border: 1px solid #9CF; text-align:center\" name=\"id\" readonly=\"readonly\" type=\"text\" value=\"".$lmotivo['idmotivo']."\" /></td>";
		echo "<td class=\"corfd_colcampos\"><input style=\"width:200px; border: 1px solid #9CF; text-align:center\" name=\"nome\" type=\"text\" value=\"".$lmotivo['nomemotivo']."\" /></td>";
		echo "<td class=\"corfd_colcampos\"><select style=\"width:200px;\" name=\"tipo\">";
			$seltipo = "SELECT * FROM tipo_motivo";
			$eseltipo = $_SESSION['query']($seltipo) or die ("erro na query de seleÃ§Ã£o do tipo dos motivos");
			while($lseltipo = $_SESSION['fetch_array']($eseltipo)) {
			  if($lseltipo['idtipo_motivo'] != $lmotivo['idtipo_motivo']) {
                              echo "<option value=\"".$lseltipo['idtipo_motivo']."\">".$lseltipo['nometipo_motivo']."</option>";
                          }
                          else {
                              echo "<option value=\"".$lseltipo['idtipo_motivo']."\" selected=\"selected\">".$lseltipo['nometipo_motivo']."</option>";
                          }
			}
		echo "</select></td>";
                echo "<td class=\"corfd_colcampos\"><select name=\"acesso\" id=\"acesso\">";
                $acessos = array('E' => "EXTERNO",'I' => "INTERNO");
                foreach($acessos as $kac => $ac) {
                    if($kac == $lmotivo['acesso']) {
                        echo "<option value=\"$kac\" selected=\"selected\">$ac</option>";
                    }
                    else {
                        echo "<option value=\"$kac\">$ac</option>";
                    }
                }
                echo "</select></td>";
                echo "<td class=\"corfd_colcampos\"><input style=\"width:40px; border: 1px solid #9CF; text-align:center\" type=\"text\" name=\"qtde\" id=\"qtde\" maxlength=\"3\" value=\"".$lmotivo['qtde']."\" /></td>";
                echo "<td class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #9CF; text-align:center\" type=\"text\" maxlength=\"7\" name=\"tempo\" id=\"tempo\" value=\"".$lmotivo['tempo']."\" /></td>";
                echo "<td class=\"corfd_colcampos\"><select name=\"ativo\">";
		echo "<option value=\"".$lmotivo['ativo']."\">".$lmotivo['ativo']."</otpion>";
		$atvmdb = array($lmotivo['ativo']);
		$opatvm = array('S','N');
		$difatv = array_diff($opatvm, $atvmdb);
		foreach($difatv as $atv) {
		  echo "<option value=\"".$atv."\">".$atv."</option>";
		}
		echo "</select></td>";
	echo "<td><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)\" name=\"altera\" type=\"submit\" value=\"alterar\" /></td>";
	echo "<td><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"apagar\" /></td>";
	echo "</tr></form>";
  }
  ?>
</table>
</div>
