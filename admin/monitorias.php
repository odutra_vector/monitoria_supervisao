<?php
include_once ($rais."/monitoria_supervisao/functionsadm.php");

$iduser = $_SESSION['usuarioID'];
$idmoni = $_GET['idmonitoria'];
$selfilt = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
$eselfilt = $_SESSION['query']($selfilt) or die (mysql_error());
$nfilt = $_SESSION['num_rows']($eselfilt);
while($lselfilt = $_SESSION['fetch_array']($eselfilt)) {
    if($_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])] != "") {
        $filtrosp[$lselfilt['nomefiltro_nomes']] = "id_".strtolower($lselfilt['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lselfilt['nomefiltro_nomes'])]."'";
    }
    else {
    }
}

if(isset($_POST['idoper'])) {
    $idoper = $_POST['idoper'];
}
if(isset($_GET['idoper']) && !isset($_POST['pesq'])) {
    $idoper = $_GET['idoper'];
}
if(isset($_POST['idfluxo'])) {
    $idfluxo = $_POST['idfluxo'];
}
if(isset($_GET['idfluxo'])) {
    $idfluxo = $_GET['idfluxo'];
}
if(isset($_POST['idmoni'])) {
    $idmoni = $_POST['idmoni'];
}
else {
    $idmoni = $_GET['idmoni'];
}
if(isset($_POST['fg'])) {
    $fg = $_POST['fg'];
}
else {
    $fg = $_GET['fg'];
}
if(isset($_POST['alerta'])) {
    $exalertass = $_POST['alerta'];
    foreach($exalertass as $ex) {
        if($ex != "") {
            $imalerta[] = $ex;
        }
    }
    $alerta = implode(",",$imalerta);
}
else {
    $alerta = implode(",",$_GET['alerta']);
}
if(isset($_POST['pesq'])) {
    if($idmoni != "") {
    }
    else {
        $dtini = data2banco($_POST['dtinimoni']);
        $dtfim = data2banco($_POST['dtfimmoni']);
    }
}
else {
}
if($idmoni != "") {
    $selmoni = "SELECT m.alertas,m.idmonitoria, m.idplanilha, m.data, m.idrel_filtros, m.idfila, m.tabfila, m.idoperador, o.operador, m.idsuper_oper, s.super_oper, ra.idaval_plan, ap.nomeaval_plan, m.idmonitoria_fluxo, m.checkfg,pm.checkfg as checkfgpm, COUNT(*) as result FROM monitoria m
                INNER JOIN operador o ON o.idoperador = m.idoperador
                INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                INNER JOIN rel_aval ra ON ra.idplanilha = m.idplanilha
                INNER JOIN aval_plan ap ON ap.idaval_plan = ra.idaval_plan
                INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                WHERE m.idmonitoria='$idmoni'";
    $lselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query para consultar a monitoria");
    $idoper = $lselmoni['idoperador'];
    $nomeoper = $lselmoni['operador'];
    $idsuper = $lselmoni['idsuper_oper'];
    $nomesuper = $lselmoni['super_oper'];
    $data = $lselmoni['data'];
    $idaval[] = $lselmoni['idaval_plan'];
    $nomeaval[] = $lselmoni['nomeaval_plan'];
    $idrel = $lselmoni['idrel_filtros'];
    $idfila = $lselmoni['idfila'];
    $tipomoni = $lselmoni['tabfila'];
    $monifluxo = $lselmoni['idmonitoria_fluxo'];
    
    $selfluxo = "SELECT idfluxo, COUNT(*) as result FROM monitoria_fluxo mf
                INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo WHERE idmonitoria_fluxo='$monifluxo'";
    $eselfluxo = $_SESSION['fetch_array']($_SESSION['query']($selfluxo)) or die ("erro na query de consulta do fluxo");
    $idfluxo = $eselfluxo['idfluxo'];
    
}

?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/monitoria_supervisao/js/custom-theme/jquery-ui-1.8.2.custom.css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/dataini.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/datafim.js"></script>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
    <?php
    if($idmoni != "") {
    ?>
        $('#dadosmoni').show();
    <?php
    }
    else {
    ?>
        $('#dadosmoni').hide();
    <?php
    }
    ?>
        $('#idrel').change(function() {
            var idrel = $(this).val();
            var idoper = $('#idoper').val();
            if(idrel == "") {

            }
            else {
                $('#idoper').load('/monitoria_supervisao/admin/carrdadosmoni.php',{idrel: idrel, select: 'oper'});
                $('#idfluxo').load('/monitoria_supervisao/admin/carrdadosmoni.php',{idrel: idrel,idoper: idoper, select: 'fluxo'});
            }
        })

        $('#idoper').change(function() {
            var idoper = $(this).val();
            var idrel = $('#idrel').val();
            var idmoni = $('#idmonitoria').val();
            $('#idfluxo').load('/monitoria_supervisao/admin/carrdadosmoni.php',{idrel: idrel,idoper: idoper, idmoni:idmoni, select: 'fluxo'});
        })

        $('#idrelmoni').change(function() {
            var idrel = $(this).val();
            var idoper = $('#idopermoni').val();
            var idmoni = $('#idmonitoria').val();
            if(idrel == "") {
            }
            else {
                $('#idopermoni').load('/monitoria_supervisao/admin/carrdadosmoni.php',{idrel: idrel, idmoni: idmoni, select: 'opermoni'});
                $('#idsupermoni').load('/monitoria_supervisao/admin/carrdadosmoni.php',{idrel: idrel,idoper: idoper, idmoni: idmoni, select: 'supermoni'});
            }
        })

        $('#tabmoni').tablesorter();
        
        $('#pesq').click(function() {
            var dtini = $('#dtinimoni').val();
            var dtfim = $('#dtfimmoni').val();
            var idmoni = $('#idmoni').val();
            
            if(idmoni == "" && (dtini == "" || dtfim == "")) {
                alert("Quando o ID da monitoria não for informado é necessário informar a data");
                return false;
            }
            else {
            }
        })
        
        $('#limpa').click(function() {
            $('#idmoni').attr('value','');
            $('#dtinimoni').attr('value','');
            $('#dtfimmoni').attr('value','');
            $('#idoper').attr('value','');
            $('#idfluxo').attr('value','');
            <?php
            $sf = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
            $esf = $_SESSION['query']($sf) or die (mysql_error());
            while($lsf = $_SESSION['fetch_array']($esf)) {
                echo "$('#filtro_".strtolower($lsf['nomefiltro_nomes'])."').attr('value','');\n";
            }
            ?>
            $('#fg').attr('value','');
        });
        
        $('#marcar').click(function() {
            var bmarcar = $("#bmarcar").val();
            if(bmarcar == 0) {
                $("#marcar").attr("value","Desmarcar");
                $("#bmarcar").attr("value","1");
                $("input[id*='idcheck']").each(function() {
                  $(this).attr("checked", "checked");
                })
            }
            else {
                $("#marcar").attr("value","Marcar Todas");
                $("#bmarcar").attr("value","0");
                $("input[id*='idcheck']").each(function() {
                  $(this).removeAttr("checked");
                })
            }
        });
        
        $("#apagafila").click(function() {
            var confirma = confirm("Você confirma a exclusão do REGISTRO da fila de MONITORIAS?");
            if(confirma) {
            }
            else {
                return false;
            }        
        });
        
        $("#apagamoni").click(function() {
            var confirma = confirm("Você confirma a exclusão da MONITORIA e de todos seus registros, e o retorno do audio a fila de MONITORIA");
            if(confirma) {
            }
            else {
                return false;
            }        
        });
        
        $("#envalerta").click(function() {
            var ids = "";
            $("input[id*='idcheck']").each(function() {
                if(this.checked) {
                    if(ids == "") {
                        ids = $(this).val();
                    }
                    else {
                        ids = ids+","+$(this).val();
                    }
                }
            })
            if(ids == "") {
                alert("É necessáro selelecionar 1 monitoria para enviar alerta");
                return false;
            }
            else {
                $.blockUI({ message: '<strong>AGUARDE ENVIANDO E-MAILS...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
                $.post('/monitoria_supervisao/admin/envalerta.php', {ids:ids},function(retorno) {
                    if(retorno == 0) {
                        $.unblockUI();
                        var msg = "Erro no processo para envio de e-mail, favor verficiar as configurações do Servidor de E-mail e as configurações no sistema de Monitoria";
                        $.blockUI({ message: '<strong>'+msg.toUpperCase()+'</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px',
                            color: '#fff'
                            }
                        })
                        setTimeout($.unblockUI,4000);
                    }
                    else {
                        $.unblockUI();
                        var msg = retorno+" - e-mail/s enviados com sucesso!!!";
                        $.blockUI({ message: '<strong>'+msg.toUpperCase()+'</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px',
                            color: '#fff'
                            }
                        })
                        setTimeout($.unblockUI,4000);
                    }
                })
            }
        })
    })
</script>
<style>
    #apagafila {
        border: 1px solid #FFF; 
        height: 18px; 
        background-color: #EA969B;
    }
    
    #apagafila:hover {
        background-color: #E65E5E;
    }
</style>
<div id="conteudo">
<form action="" method="post">
<table width="1000">
  <tr>
    <td width="134" class="corfd_coltexto"><strong>ID MONITORIA</strong></td>
    <td width="321" class="corfd_colcampos"><input name="idmoni" id="idmoni" style="width:100px; border: 1px solid #9CF; text-align:center" value="<?php echo $idmoni;?>" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>DATA MONITORIA</strong></td>
    <td class="corfd_colcampos">
        <input name="dtinimoni" id="dtinimoni" value="<?php echo banco2data($dtini);?>" type="text" style="width:100px; border: 1px solid #9CF; text-align:center"/> A <input name="dtfimmoni" id="dtfimmoni" value="<?php echo banco2data($dtfim);?>" type="text" style="width:100px; border: 1px solid #9CF; text-align:center"/>
    </td>
  </tr>
  <?php
  $periodo = periodo ();

  $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
  $efiltro = $_SESSION['query']($selfiltros) or die ("erro na consulta dos nomes dos filtros");
  while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
          $filtros[] = strtolower($lfiltros['idfiltro_nomes']);
	  $nome = $lfiltros['nomefiltro_nomes'];
	  $idfiltro = "id_".strtolower($lfiltros['nomefiltro_nomes']);
	  if($_SESSION['user_tabela'] == "user_web") {
	  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM userwebfiltro
                            INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = userwebfiltro.idrel_filtros
                            INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                            INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                            INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                            WHERE iduser_web='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
	  }
	  if($_SESSION['user_tabela'] == "user_adm") {
	  $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM useradmfiltro
                            INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = useradmfiltro.idrel_filtros
                            INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                            INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                            INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                            INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                            WHERE iduser_adm='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados ORDER BY nomefiltro_dados";
	  }
	  $efiltrodados = $_SESSION['query']($filtrodados) or die ("erro na consulta dos filtros realcionados os usuário");
	  echo "<tr>";
	  	echo "<td class=\"corfd_coltexto\"><strong>".$nome."</strong></td>";
		echo "<td colspan=\"3\" class=\"corfd_colcampos\"><select style=\"width:350px; border: 1px solid #333;\" name=\"filtro_".strtolower($nome)."\" id=\"filtro_".strtolower($nome)."\">";
		echo "<option value=\"\"";
		if(isset($_POST["filtro_".$nome]) OR $varget["filtro_".$nome] != "") {
		}
		else {
			//echo "selected=\"selected\" disabled=\"disabled\"";
		}
		echo ">Selecione....</option>";
		while($lfiltrodados = $_SESSION['fetch_array']($efiltrodados)) {
			if(in_array($lfiltrodados['idindice'],$idsindice)) {
			}
			else {
				$idsindice[] = $lfiltrodados['idindice'];
			}
			if(in_array($lfiltrodados['idfluxo'],$idsfluxo)) {
			}
			else {
				$idsfluxo[] = $lfiltrodados['idfluxo'];
			}
			if($lfiltrodados[$idfiltro] == $_POST['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $varget['filtro_'.strtolower($nome)]) {
				$select = "selected=\"selected\"";
			}
			else {
				$select = "";
			}
			echo "<option value=\"".$lfiltrodados[$idfiltro]."\" $select >".$lfiltrodados['nomefiltro_dados']."</option>";
		}
	  	echo "</select><td>";
	 echo "</tr>";
  }
  ?>
  <tr>
    <td class="corfd_coltexto"><strong>OPERADOR</strong></td>
    <td class="corfd_colcampos">
        <select name="idoper" id="idoper" style="width:350px">
        <?php
        if($idrel != "") {
            $seloper = "SELECT o.idoperador, o.operador FROM operador o
                                INNER JOIN monitoria m ON m.idoperador = o.idoperador
                                INNER JOIN rel_filtros rf ON  rf.idrel_filtros = m.idrel_filtros
                                WHERE m.idrel_filtros='$idrel' GROUP BY o.idoperador ORDER BY o.operador";
            $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consutla dos operadores relacionados");
        ?>
            <option value="">SELECIONE...</option>
        <?php
            while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                    if($idoper == $lseloper['idoperador']) {
                    ?>
                        <option value="<?php echo $lseloper['idoperador'];?>" selected="selected"><?php echo $lseloper['operador'];?></option>
                    <?php
                    }
                    else {
                    ?>
                        <option value="<?php echo $lseloper['idoperador'];?>"><?php echo $lseloper['operador'];?></option>
                    <?php
                    }
            }
        }
        else {
            if($idoper != "") {
                $seloper = "SELECT o.idoperador, o.operador FROM operador o
                            INNER JOIN monitoria m ON m.idoperador = o.idoperador
                            INNER JOIN rel_filtros rf ON  rf.idrel_filtros = m.idrel_filtros GROUP BY o.idoperador ORDER BY o.operador";
                $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consutla dos operadores relacionados");
                ?>
                <option value="" selected="selected">SELECIONE...</option>
                <?php
                while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                    if($idoper == $lseloper['idoperador']) {
                        ?>
                        <option value="<?php echo $lseloper['idoperador'];?>" selected="selected"><?php echo $lseloper['operador'];?></option>
                        <?php
                    }
                    else {
                        ?>
                        <option value="<?php echo $lseloper['idoperador'];?>"><?php echo $lseloper['operador'];?></option>
                        <?php
                    }
                }
            }
            else {
                $seloper = "SELECT o.idoperador, o.operador FROM operador o
                                INNER JOIN monitoria m ON m.idoperador = o.idoperador
                                INNER JOIN rel_filtros rf ON  rf.idrel_filtros = m.idrel_filtros GROUP BY o.idoperador ORDER BY o.operador";
                $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consutla dos operadores relacionados");
                    ?>
                <option value="" selected="selected">SELECIONE...</option>
                <?php
                while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                ?>
                    <option value="<?php echo $lseloper['idoperador'];?>"><?php echo $lseloper['operador'];?></option>
                <?php
                }
            }
        }
        ?>
        </select>        
    </td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>FLUXO</strong></td>
    <td class="corfd_colcampos">
        <select name="idfluxo" id="idfluxo">
        <?php
        if($idrel != "") {
            $sqlfilt['idrel_filtros'] = "rf.idrel_filtros='".$idrel."'";
        }
        else {
        }
        if($idoper != "") {
            $sqlfilt['idoperador'] = "o.idoperador='".$idoper."'";
        }
        else {
        }
        if($sqlfilt == "") {
        }
        else {
            $sqlfilt = "WHERE ".implode(" AND ",$sqlfilt)."";
        }

        if($idfluxo != "") {
        $selfluxo = "SELECT f.idfluxo, f.nomefluxo FROM fluxo f
                     INNER JOIN conf_rel cr ON cr.idfluxo = f.idfluxo
                     INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros
                     INNER JOIN monitoria m ON m.idrel_filtros = rf.idrel_filtros
                     INNER JOIN operador o ON o.idoperador = m.idoperador
                     $sqlfilt GROUP BY f.idfluxo";
        $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta do fluxo");
        ?>
            <option value="">SELECIONE...</option>
            <?php
            while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                    if($idfluxo == $lselfluxo['idfluxo']) {
                    ?>
                        <option value="<?php echo $lselfluxo['idfluxo'];?>" selected="selected"><?php echo $lselfluxo['nomefluxo'];?></option>
                    <?php
                    }
                    else {
                    ?>
                        <option value="<?php echo $lselfluxo['idfluxo'];?>" ><?php echo $lselfluxo['nomefluxo'];?></option>
                    <?php
                    }
            }
        }
        else {
           $selfluxo = "SELECT f.idfluxo, f.nomefluxo FROM fluxo f
                     INNER JOIN conf_rel cr ON cr.idfluxo = f.idfluxo
                     INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros                         
                     INNER JOIN monitoria m ON m.idrel_filtros = rf.idrel_filtros
                     INNER JOIN operador o ON o.idoperador = m.idoperador
                     $sqlfilt GROUP BY f.idfluxo";
            $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta do fluxo");
            ?>
            <option value="" selected="selected">SELECIONE...</option>
            <?php
            while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                if($idfluxo == $lselfluxo['idfluxo']) {
                    ?>
                    <option value="<?php echo $lselfluxo['idfluxo'];?>" selected="selected"><?php echo $lselfluxo['nomefluxo'];?></option>
                    <?php
                }
                else {
                    ?>
                    <option value="<?php echo $lselfluxo['idfluxo'];?>" ><?php echo $lselfluxo['nomefluxo'];?></option>
                    <?php
                }
            }
        }
        ?>
        </select>
    </td>
  </tr>
  <tr>
      <td class="corfd_coltexto"><strong>ALERTA</strong></td>
      <td class="corfd_colcampos">
          <select name="alerta[]" id="alerta" multiple="multiple">
              <?php
              if($alerta == "") {
                  ?>
                  <option value="" selected="selected">SELECIONE...</option>
                  <?php
              }
              else {
                  ?>
                  <option value="">SELECIONE...</option>
                  <?php
              }
              $selalerta = "SELECT * FROM alertas";
              $ealerta = $_SESSION['query']($selalerta) or die (mysql_error());
              while($lalerta = $_SESSION['fetch_array']($ealerta)) {
                  if(in_array($lalerta['idalertas'],$imalerta)) {
                      ?>
                      <option value="<?php echo $lalerta['idalertas'];?>" selected="selected"><?php echo $lalerta['nomealerta'];?></option>
                      <?php
                  }
                  else {
                      ?>
                      <option value="<?php echo $lalerta['idalertas'];?>"><?php echo $lalerta['nomealerta'];?></option>
                      <?php
                  }
              }
              ?>
          </select>
      </td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>NCG</strong></td>
    <td class="corfd_colcampos">
        <select name="fg" id="fg">
        <?php
        if($fg != "") {
                echo "<option value=\"\">SELECIONE...</option>";
        }
        else {
                echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";
        }
        $tipofg = array('checkfg' => "CHECK",'ncheck' => 'NÃO CHECK','ambos' => 'TUDO NCG');
        foreach($tipofg as $kfg => $tfg) {
                if($kfg == $fg) {
                        echo "<option selected=\"selected\" value=\"$kfg\">$tfg</option>";
                }
                else {
                        echo "<option value=\"$kfg\">$tfg</option>";
                }
        }
        ?>
        </select>
    </td>
  </tr>
  <tr>
    <td colspan="2">
        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesq" id="pesq" type="submit" value="Pesquisar" /> 
        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="limpa" id="limpa" type="button" value="Limpar Filtros" />
        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="envalerta" id="envalerta" type="button" value="Enviar Alerta" /> 
        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="marcar" id="marcar" type="button" value="Marcar Todas" /><input name="bmarcar" id="bmarcar" vale="0" type="hidden"/>
    </td>
  </tr>
</table>
</form><br /><br />
<div id="dadosmoni">
    <?php
    if($lselmoni['result'] >= 1) {
    ?>
    <form action="altmonitorias.php" method="post">
    <table width="1000">
      <tr>
        <td width="118" class="corfd_coltexto"><strong>ID MONITORIA</strong></td>
        <td width="75" class="corfd_colcampos" align="center"><input name="idmoni" id="idmonitoria" value="<?php echo $idmoni;?>" type="hidden" /><?php echo $idmoni;?></td>
        <td width="88" class="corfd_coltexto"><strong>DATA</strong></td>
        <td width="97" class="corfd_colcampos" align="center"><input name="data" id="data" value="<?php echo $idmoni;?>" type="hidden" /><?php echo banco2data($data);?></td>
        <td width="99" class="corfd_coltexto"><strong>ID FILA</strong></td>
        <td width="503" class="corfd_colcampos"><input name="idfila" id="idfila" value="<?php echo $idmoni;?>" type="hidden" /><?php echo $idfila;?></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>RELACIONAMENTO</strong></td>
        <td class="corfd_colcampos" colspan="5">
            <select name="idrel" id="idrelmoni" style="width:800px">
                <?php
                $idreldb = arvoreescalar("", $_SESSION['uduarioID']);
                foreach($idreldb as $id) {
                    $selrel = "SELECT rf.idrel_filtros,count(*) as r FROM rel_filtros rf "
                            . " INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros WHERE cr.ativo='S' AND rf.idrel_filtros='$id'";
                    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta dos relacionamentos");
                    if($eselrel['r'] >= 1) {
                        if($eselrel['idrel_filtros'] == $idrel) {
                                echo "<option value=\"".$eselrel['idrel_filtros']."\" selected=\"selected\">".nomeapres($idrel)."</option>";
                        }
                        else {
                                echo "<option value=\"".$eselrel['idrel_filtros']."\">".nomeapres($eselrel['idrel_filtros'])."</option>";
                        }
                    }
                }
                ?>
            </select>
        </td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>OPERADOR</strong></td>
        <td class="corfd_colcampos" colspan="5">
            <select name="idoper" id="idopermoni" style="width:500px">
            <?php
            if($idmoni != "") {
                if($tipomoni == "fila_oper") {
                    $innerop = "INNER JOIN fila_oper fo ON fo.idoperador = o.idoperador
                                INNER JOIN upload u ON u.idupload = fo.idupload
                                INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana";
                    $innerso = "INNER JOIN fila_oper fo ON fo.idsuper_oper = so.idsuper_oper
                                INNER JOIN upload u ON u.idupload = fo.idupload
                                INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana";
                    $alias = "fo";
                }
                if($tipomoni == "fila_grava") {
                    $innerop = "INNER JOIN fila_grava fg ON fg.idoperador = o.idoperador
                                INNER JOIN upload u ON u.idupload = fg.idupload
                                INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana";
                    $innerso = "INNER JOIN fila_grava fg ON fg.idsuper_oper = so.idsuper_oper
                                INNER JOIN upload u ON u.idupload = fg.idupload
                                INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana";
                    $alias = "fg";
                }
                $seloper = "SELECT o.idoperador, o.operador FROM operador o $innerop GROUP BY o.idoperador ORDER BY o.operador";
                $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consulta dos operadores cadastrados");
                while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                    if($lseloper['idoperador'] == $idoper) {
                        echo "<option value=\"".$lseloper['idoperador']."\" selected=\"selected\">".$lseloper['operador']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lseloper['idoperador']."\">".$lseloper['operador']."</option>";
                    }
                }
            }
            else {
            }
            ?>
            </select>
        </td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>SUPERVISOR</strong></td>
        <td class="corfd_colcampos" colspan="5">
            <select name="idsuper" id="idsupermoni" style="width:500px">
                <?php
                if($idmoni != "") {
                    $selsuper = "SELECT so.idsuper_oper, so.super_oper FROM super_oper so $innerso GROUP BY so.idsuper_oper ORDER BY so.super_oper";
                    $eselsuper = $_SESSION['query']($selsuper) or die ("erro na query de consulta dos supervisores relacionados ao relacionamento");
                    while($lselsuper = $_SESSION['fetch_array']($eselsuper)) {
                        if($lselsuper['idsuper_oper'] == $idsuper) {
                            echo "<option value=\"".$lselsuper['idsuper_oper']."\" selected=\"selected\">".$lselsuper['super_oper']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lselsuper['idsuper_oper']."\">".$lselsuper['super_oper']."</option>";
                        }
                    }
                }
                else {
                }
                ?>
            </select>

        </td>
      </tr>
      <?php
      $selaval = "SELECT ra.idaval_plan, ap.nomeaval_plan FROM rel_aval ra
                  INNER JOIN aval_plan ap ON ap.idaval_plan = ra.idaval_plan
                  WHERE idplanilha='".$lselmoni['idplanilha']."'";
      $eselaval = $_SESSION['query']($selaval) or die ("erro na query para listagem das avaliações da planilha");
      $naval = $_SESSION['num_rows']($eselaval);
      if($naval >= 1) {
          while($lselaval = $_SESSION['fetch_array']($eselaval)) {
              $avalplan[$lselaval['nomeaval_plan']] = $lselaval['idaval_plan'];
          }
          foreach($avalplan as $nap => $ap) {
              $selnota = "SELECT valor_final_aval, valor_fg FROM moniavalia WHERE idplanilha='".$lselmoni['idplanilha']."' AND idmonitoria='$idmoni' AND idaval_plan='$ap'";
              $eselnota = $_SESSION['fetch_array']($_SESSION['query']($selnota)) or die ("erro na query de consulta da nota da monitoria");
              ?>
              <tr>
                  <td class="corfd_coltexto"><strong><?php echo $nap;?></strong></td>
                  <td class="corfd_colcampos" colspan="6" style="font-size: 13px"><?php echo $eselnota['valor_final_aval'];?></td>
              </tr>
              <tr>
                  <td class="corfd_coltexto"><strong><?php echo $nap;?> NCG</strong></td>
                  <td class="corfd_colcampos" colspan="6" style="font-size: 13px"><?php echo $eselnota['valor_fg'];?></td>
              </tr>
              <?php
          }
      }
      else {
      }
      //if($lselmoni['checkfgpm'] == "N") {
      //}
      //else {
      ?>
      <tr>
        <td class="corfd_coltexto"><strong>Check NCG</strong></td>
        <td class="corfd_colcampos">
            <?php
            if($lselmoni['checkfg'] != "") {
                echo "<input name=\"checkfg\" id=\"checkfg\" value=\"".$lselmoni['checkfg']."\" type=\"hidden\"/>";
                echo $lselmoni['checkfg'];
            }
            else {
                echo "<select name=\"checkfg\" id=\"checkfg\">";
                echo "<option value=\"\"></option>";
                $check = array('NCG OK', 'NCG NOK');
                foreach($check as $c) {
                        echo "<option value=\"$c\">$c</option>";	
                }
                echo "</select>";
            }
            ?>
        </td>
        <td>
            <?php
            if($lselmoni['checkfg'] == "NCG OK") {
                echo " <input name=\"reenvia\" id=\"reenvia\" type=\"submit\" style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" value=\"REENVIAR NCG\" />";
            }
            else {
            }                
            ?>
        </td>
      </tr>
      <?php
      //}
      ?>
      <tr>
        <td class="corfd_ntab" colspan="6" align="center"><strong>FLUXO</strong></td>
      </tr>
      <tr>
        <td colspan="6">
            <table border="0" width="1000" style="border: 2px solid #CCC">
                <thead>
                    <tr>
                        <th width="78" class="corfd_coltexto"><strong>DATA FLUXO</strong></th>
                        <th width="197" class="corfd_coltexto"><strong>USUÁRIO</strong></th>
                        <th width="81" class="corfd_coltexto"><strong>TIPO USER</strong></th>
                        <th width="131" class="corfd_coltexto"><strong>STATUS</strong></th>
                        <th width="187" class="corfd_coltexto"><strong>DEFINIÇÃO</strong></th>
                        <th width="239" class="corfd_coltexto"><strong>OBS</strong></th>
                        <th width="53"></th>
                    </tr>
                </thead>
                <tbody>
            <?php
            $selfluxo = "SELECT mf.tabuser, mf.iduser, mf.idmonitoria_fluxo, s.nomestatus, d.nomedefinicao, mf.obs, rf.tipo, mf.data FROM monitoria_fluxo mf
                        INNER JOIN status s ON s.idstatus = mf.idstatus
                        INNER JOIN definicao d ON d.iddefinicao = mf.iddefinicao
                        INNER JOIN monitoria m ON m.idmonitoria = mf.idmonitoria
                        INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                        INNER JOIN rel_fluxo rf ON rf.idfluxo = cr.idfluxo AND rf.idstatus = mf.idstatus AND rf.iddefinicao = mf.iddefinicao
                        WHERE mf.idmonitoria='$idmoni' ORDER BY mf.idmonitoria_fluxo";
            $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta do fluxo");
            while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                ?>
                <form action="altmonitorias.php" method="post">
                <tr>
                    <td align="center" class="corfd_colcampos"><input name="idmoni" value="<?php echo $idmoni;?>" type="hidden" /><input name="idmonifluxo" type="hidden" style="width:70px; border: 1px solid #fff" value="<?php echo $lselfluxo['idmonitoria_fluxo'];?>" /><?php echo banco2data($lselfluxo['data']);?></td>
                    <?php
                    $seluser = "SELECT id".$lselfluxo['tabuser'].", nome".$lselfluxo['tabuser']." FROM ".$lselfluxo['tabuser']." WHERE id".$lselfluxo['tabuser']."='".$lselfluxo['iduser']."'";
                    $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do usuário");
                    ?>
                    <td align="center" class="corfd_colcampos"><?php echo $eseluser['nome'.$lselfluxo['tabuser']];?></td>
                    <td align="center" class="corfd_colcampos"><?php echo $lselfluxo['tabuser'];?></td>
                    <td align="center" class="corfd_colcampos"><?php echo $lselfluxo['nomestatus'];?></td>
                    <td align="center" class="corfd_colcampos"><?php echo $lselfluxo['nomedefinicao'];?></td>
                    <td align="center" class="corfd_colcampos"><textarea name="obs" id="obs" style="width:239px; height: 60px; border: 1px solid #9cf"><?php echo $lselfluxo['obs'];?></textarea></td>
                    <td>
                        <?php
                        if($lselfluxo['idmonitoria_fluxo'] == $lselmoni['idmonitoria_fluxo'] && $lselfluxo['tipo'] != "inicia") {
                        ?>
                        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg);width:70px" name="altfluxo" id="altfluxo" type="submit" value="Alterar" /><br/><br/>
                        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg);width:70px" name="apagafluxo" id="apagafluxo" type="submit" value="Apagar" />
                        <?php
                        }
                        else {
                        ?>
                            <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altfluxo" id="altfluxo" type="submit" value="Alterar" />
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                </form>
                    <?php
            }
            ?>
                </tbody>
            </table>
        </td>
      </tr>
      <tr>
          <td colspan="6">
              <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg); margin-right: 10px" name="altmoni" id="altmoni" type="submit" value="Alterar" /> 
              <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg);margin-right: 10px" name="altmoniaval" id="altmoniaval" target="blank" type="button" onclick="javascript:window.open('alteramonitoria.php?idmoni=<?php echo $idmoni;?>');" value="Alterar Avaliação" /> 
              <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg);margin-right: 10px" name="apagamoni" id="apagamoni" type="submit" value="Excluir Monitoria" /> 
              <input name="apagafila" id="apagafila" type="submit" value="Excluir da Fila" />
          </td>
      </tr>
    </table>
    <font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
    </form>
    <?php
    }
    else {
    }
    ?>
</div><br /><hr/>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
<div id="monitorias" style="width:auto; height: 450px; overflow: auto">
    <table width="1024">
        <tr>
            <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=monitorias"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">EXCEL</div></a></td>
            <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome=monitorias"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">WORD</div></a></td>
            <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome=monitorias"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">PDF</div></a></td>
        </tr>
    </table><br/>
    <?php
      if($filtrosp != "") {
          $where[] = implode(" AND ",$filtrosp);
      }
      else {
      }
      if($idoper != "") {
          $where[] = "o.idoperador='".$idoper."'";
      }
      else {
      }
      if($idfluxo != "") {
          $where[] = "f.idfluxo='".$idfluxo."'";
      }
      if($alerta != "") {
          foreach($imalerta as $a) {
            $walerta[] = "m.alertas LIKE '%$a%'";
          }
          if(count($walerta) >= 2) {
              $where[] = "(".implode(" OR ",$walerta).")";
          }
          if(count($walerta) == 1) {
              $where[] = $walerta[0];
          }
      }
      if($ckeckfg != "") {
          $where[] = "(m.checkfg='' OR m.checkfg IS NULL) AND m.qtdefg >= 1";
      }
      else {
      }
      if($fg != "") {
          if($fg == "checkfg") {
              $where[] = "m.checkfg IS NOT NULL";  
          }
          if($fg == "ncheck") {
              $where[] = "(m.checkfg='' OR m.checkfg IS NULL) AND m.qtdefg >= 1";  
          }
          if($fg == "ambos") {
              $where[] = "(((m.checkfg='' OR m.checkfg IS NULL) AND m.qtdefg >= 1) OR m.checkfg IS NOT NULL)";
          }
      }
      else {
      }
      if($dtini != "" && $dtfim != "") {
          $where[] = "m.data BETWEEN '$dtini' AND '$dtfim'";
      }
      if($idmoni) {
          $where = "WHERE m.idmonitoria='$idmoni'";
      }
      if($where != "" AND $idmoni == "") {
          $where = "WHERE ".implode(" AND ",$where);
      }

      $selmoni = "SELECT m.alertas,m.idmonitoria,m.qtdefg,m.checkfg,mo.nomemonitor,m.data,m.idfila,m.idrel_filtros,m.idoperador,m.idsuper_oper,m.situacao, o.operador,s.super_oper,m.datactt FROM monitoria m
                  INNER JOIN operador o ON o.idoperador = m.idoperador
                  INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor
                  INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper
                  INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                  INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                  LEFT JOIN fila_grava fg ON fg.idfila_grava = m.idfila
                  LEFT JOIN fila_oper fo ON fo.idfila_oper = m.idfila
                  INNER JOIN fluxo f ON f.idfluxo = cr.idfluxo $where ORDER BY m.idmonitoria DESC LIMIT 1000";
      $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta das monitorias");
      $nlinhas = $_SESSION['num_rows']($eselmoni);
      echo "<tr class=\"corfd_colcampos\"><td colspan=\"15\"><strong>RESULTADOS: $nlinhas</strong></td></tr>";
      $dados .= "<table width=\"1500\" id=\"tabmoni\">";
      $dados .= "<thead>";
      $dados .= "<tr>";
        $dados .= "<td></td>";
        $dados .= "<th width=\"90\" align=\"center\" class=\"corfd_coltexto\"><strong>ID MONI</strong></th>";
        $dados .= "<th width=\"50\" align=\"center\" class=\"corfd_coltexto\"><strong>SIT.</strong></th>";
        $dados .= "<th width=\"50\" align=\"center\" class=\"corfd_coltexto\"><strong>ALERTAS</strong></th>";
        $dados .= "<th width=\"50\" align=\"center\" class=\"corfd_coltexto\"><strong>NCG</strong></th>";
        $dados .= "<th width=\"50\" align=\"center\" class=\"corfd_coltexto\"><strong>NCG.CHECK</strong></th>";
        $dados .= "<th width=\"88\" align=\"center\" class=\"corfd_coltexto\"><strong>DATA</strong></th>";
        $dados .= "<th width=\"85\" align=\"center\" class=\"corfd_coltexto\"><strong>DATA CTT</strong></th>";
        $dados .= "<th width=\"85\" align=\"center\" class=\"corfd_coltexto\"><strong>ID FILA</strong></th>";
        $dados .= "<th width=\"607\" align=\"center\" class=\"corfd_coltexto\"><strong>MONITOR</strong></th>";
        $dados .= "<th width=\"607\" align=\"center\" class=\"corfd_coltexto\"><strong>RELACIONANMETO</strong></th>";
        $dados .= "<th width=\"455\" align=\"center\" class=\"corfd_coltexto\"><strong>OPERADOR</strong></th>";
        $dados .= "<th width=\"458\" align=\"center\" class=\"corfd_coltexto\"><strong>SUPERVISOR</strong></th>";
      $dados .= "</tr>";
      $dados .= "</thead>";
      $dados .= "<tbody>";
      while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
          $dados .= "<tr>";
          $dados .= "<td class=\"corfd_colcampos\"><input type=\"checkbox\" name=\"idcheck".$lselmoni['idmonitoria']."\" id=\"idcheck".$lselmoni['idmonitoria']."\" value=\"".$lselmoni['idmonitoria']."\" /></td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\"><a href=\"/monitoria_supervisao/admin/admin.php?menu=monitorias&idmoni=".$lselmoni['idmonitoria']."#".$lselmoni['idmonitoria']."\">".$lselmoni['idmonitoria']."</a></td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['situacao']."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">";
          $alertas = $lselmoni['alertas'];
          if($alertas != "") {
            $salerta = "SELECT * FROM alertas WHERE idalertas IN ($alertas)";
            $ealertas = $_SESSION['query']($salerta) or die (mysql_error());
            while($lalertas = $_SESSION['fetch_array']($ealertas)) {
                $dados .= "<input type=\"text\" name=\"".$lalertas['idalertas']."\" value=\"\" style=\"border:0px;width:10px;height:10px;background-color:#".$lalertas['coralerta']."\" title=\"".$lalertas['nomealerta']."\" />&nbsp&nbsp";
            }
          }
          else {
          }
          $dados .= "</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['qtdefg']."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['checkfg']."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\"><a href=\"#".$lselmoni['idmonitoria']."\"></a>".banco2data($lselmoni['data'])."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".banco2data($lselmoni['datactt'])."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['idfila']."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['nomemonitor']."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".nomevisu($lselmoni['idrel_filtros'])."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['operador']."</td>";
          $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$lselmoni['super_oper']."</td>";
          $dados .= "</tr>";
      }
       $dados .= "</tbody>";
    $dados .= "</table>";
    $_SESSION['dadosexp'] = $dados;
    echo $dados;
    ?>
</div>    
</div>
