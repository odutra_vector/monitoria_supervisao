<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#gerar').click(function() {
            var dtinictt = $('#dtinictt').val();
            var dtfimctt = $('#dtfimctt').val();
            if(dtinictt == "" || dtfimctt == "") {
                alert('As datas de CONTATO devem estar preenchidas');
                return false;
            }
            else {
                $.blockUI({ message: '<strong>AGUARDE GERANDO BASE...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
            }
        })

        <?php
        if($_GET['retorno'] == 1) {
            ?>
            $.unblockUI();
            <?php
        }
        else {
        }
        ?>
    })
</script>
<div style="float: left;width: 1024px;" class="corfd_pag">
    <form action="base.php" method="post">
    <?php
    include_once($rais."/monitoria_supervisao/users/function_filtros.php");
    scripts_filtros();
    unset($_SESSION['varsconsult']);
    filtros_divs();
    ?>
    <div style="float: left; width:1024px; padding-bottom: 20px">
        <br/>
        <table>
            <tr>
                <td><input style="border:1px solid #333; height: 18px; background-image:url(../images/button.jpg); text-align:center" type="submit" name="gerar" id="gerar" value="GERAR" /></td>
            </tr>
        </table>
    </div>
    </form>
    <fieldset style="border:2px solid #999;">
        <legend style="margin-left:20px;padding:5px; border:2px solid #333; background-color:#FFF;font-weight:bold ">
            BASES EXPORTADAS
        </legend>
        <div style="width: 990px; float:left; overflow: auto; height: 300px; padding-top: 10px; padding-left: 10px">
            <font color="#FF0000"><strong><?php echo $_GET['msg'];?></strong></font>
            <table width="600">
            <?php
            $arqs = scandir($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']);
            foreach($arqs as $arq) {
                if($arq == "." OR $arq == "..") {
                }
                else {
                    ?>
                    <form action="base.php" method="post">
                      <tr>
                          <td width="80%" scope="col" class="corfd_colcampos"><input name="caminhobase" id="caminhobase" value="<?php echo $arq;?>" type="hidden"/><a href="/monitoria_supervisao/baseexport/<?php echo $_SESSION['nomecli']."/".utf8_encode($arq)."";?>" target="_blank"><?php echo basename($arq);?></a></td>
                        <td width="20%" scope="col"><input style="border:1px solid #FFF; height: 18px; background-image:url(../images/button.jpg); text-align:center" type="submit" name="apagar" id="apagar" value="APAGAR" /></td>
                      </tr>
                    </form>
                    <?php
                }
            }
            ?>
            </table>
        </div>
    </fieldset><br/>
</div>
