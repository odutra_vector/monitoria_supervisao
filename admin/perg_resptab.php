<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");

if(isset($_GET['idperg'])) {
    $idperg = $_GET['idperg'];
}
if(isset($_GET['idresp'])) { 
    $idresp = $_GET['idresp'];
}

$selperg = "SELECT * FROM perguntatab WHERE idperguntatab='$idperg'";
$eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta da pergunta cadastrada");
$nperg = $_SESSION['num_rows']($eselperg);
if($nperg >= 1) {
    $lselperg = $_SESSION['fetch_array']($eselperg);
}
else {
}

$selresp = "SELECT rt.idrespostatab, rt.descrirespostatab,  rt.ativo FROM respostatab rt WHERE rt.idrespostatab='$idresp'";
$eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta da resposta");
$nresp = $_SESSION['num_rows']($eselresp);
if($nresp >= 1) {
    $lselresp = $_SESSION['fetch_array']($eselresp);
}
else {
}
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div id="conteudo">
            <form action="cadtabula.php" method="post">
      	  <table width="512">
              <tr>
                <td align="center" class="corfd_ntab" colspan="2"><strong>CADASTRO DE PERGUNTAS</strong></td>
              </tr>
              <tr>
                <td width="116" class="corfd_coltexto"><strong>DESCRIÇÃO</strong></td>
                <td width="384" class="corfd_colcampos"><input name="idpergunta" id="idpergunta" type="hidden" value="<?php if($nperg >= 1) { echo $lselperg['idperguntatab']; } else {}?>" /><input name="descriperg" id="descriperg" style="width:380px; border: 1px solid #9CF" value="<?php if($nperg >= 1) { echo $lselperg['descriperguntatab']; } else {}?>" /></td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td class="corfd_colcampos">
                <select name="atvperg" id="atvperg">
                <?php
                $ativo = array('S','N');
                foreach($ativo as $atvperg) {
                    if($atvperg == $lselperg['ativo']) {
                        echo "<option value=\"".$atvperg."\" selected=\"selected\">".$atvperg."</option>";
                    }
                    else {
                        echo "<option value=\"".$atvperg."\">".$atvperg."</option>";
                    }
                }
                ?>
                </select>
                </td>
              </tr>
              <tr>
              	<td colspan="2">
                <?php
                if($idperg != "") {
                ?>
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="alteraperg" type="submit" value="Alterar" />
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novaperg" type="submit" value="Novo" />
                <?php	
                }
                else {
                ?>
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastraperg" type="submit" value="Cadastrar" />
                <?php
                }?>
                </td>
              </tr>
          </table>
            <font color="#FF0000"><strong><?php echo $_GET['msgp'] ?></strong></font>
            </form><br /><br />
          <div style="height:300px; overflow:auto">
            <font color="#FF0000"><strong><?php echo $_GET['msgpa'] ?></strong></font>
          <table width="737">
              <tr>
                <td class="corfd_ntab" align="center" colspan="3"><strong>PERGUNTAS CADASTRADAS</strong></td>
              </tr>
              <tr>
                <td width="55" align="center" class="corfd_coltexto"><strong>ID</strong></td>
                <td width="473" align="center" class="corfd_coltexto"><strong>DESCRIÇÃO</strong></td>
                <td width="36" align="center" class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td width="153"></td>
              </tr>
              <?php
              $selpergs = "SELECT * FROM perguntatab";
              $eselpeergs = $_SESSION['query']($selpergs) or die ("erro na query de consutla das perguntas cadastradas");
              while($lselpergs = $_SESSION['fetch_array']($eselpeergs)) {
              ?>
              <form action="cadtabula.php" method="post">
              <tr>
                <td class="corfd_colcampos" align="center"><input name="idpergunta" id="idperguntacad" type="hidden" value="<?php echo $lselpergs['idperguntatab'];?>" /><?php echo $lselpergs['idperguntatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lselpergs['descriperguntatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lselpergs['ativo'];?></td>
                <td>
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="carregaperg" type="submit" value="Carregar" />
                    <?php
                    $selmonitab = "SELECT COUNT(*) as result FROM monitabulacao WHERE idperguntatab='".$lselpergs['idperguntatab']."'";
                    $eselmonitab = $_SESSION['fetch_array']($_SESSION['query']($selmonitab)) or die ("erro na query de consulta das monitorias que contenham a resposta");
                    if($eselmonitab['result'] >= 1) {
                    }
                    else  {
                    ?>
                        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="apagaperg" type="submit" value="Apagar" />
                    <?php
                    }
                    ?>
                </td>
              </tr>
              </form>
              <?php
              }
              ?>
          </table>
        </div>
            <form action="cadtabula.php" method="post">
        <table width="507" border="0">
          <tr>
            <td class="corfd_ntab" colspan="2" align="center"><strong>CADASTRO DE RESPOSTAS</strong></td>
          </tr>
          <tr>
            <td width="147" class="corfd_coltexto"><strong>DESCRIÇÃO</strong></td>
            <td width="343" class="corfd_colcampos"><input name="idresposta" id="idresposta" type="hidden" value="<?php if($nresp >= 1) { echo $lselresp['idrespostatab'];} else {}?>" /><input name="descriresp" id="descriresp" value="<?php if($nresp >= 1) { echo $lselresp['descrirespostatab'];} else {}?>" style="width:350px; border: 1px solid #9cf" /></td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>ATIVO</strong></td>
            <td class="corfd_colcampos">
                <select name="atvresp" id="atvresp">
                <?php
                $ativoresp = array('S','N');
                foreach($ativoresp as $atvresp) {
                    if($atvresp == $lselresp['ativo']) {
                        echo "<option value=\"".$atvresp."\" selected=\"selected\">".$atvresp."</option>";
                    }
                    else {
                        echo "<option value=\"".$atvresp."\">".$atvresp."</option>";
                    }
                }
                ?>
                </select>
            </td>
          </tr>
          <tr>
            <td colspan="2">
            <?php
            if($idresp != "") {
            ?>
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altresp" type="submit" value="Alterar" />
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novaresp" type="submit" value="Novo" />
            <?php
            }
            else {
            ?>
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadresp" type="submit" value="Cadastrar" />
            <?php              		
            }
            ?>               
            </td>
          </tr>
        </table><font color="#FF0000"><strong><?php echo $_GET['msgr'] ?></strong></font>
        </form><br /><br />
      <div style="height:300px; overflow:auto">
      	  <table width="800" border="0">
              <tr>
                <td class="corfd_ntab" colspan="3" align="center"><strong>RESPOSTAS CADASTRADAS</strong></td>
              </tr>
              <tr>
                <td width="60" align="center" class="corfd_coltexto"><strong>ID</strong></td>
                <td width="500" align="center" class="corfd_coltexto"><strong>DESCRIÇÃO</strong></td>
                <td width="36" align="center" class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td width="150"></td>
              </tr>
              <?php
              $selresps = "SELECT rt.idrespostatab,rt.descrirespostatab,rt.ativo FROM respostatab rt";
              $eselresps = $_SESSION['query']($selresps) or die ("erro na query de consulta das respostas cadastradas");
              while($lselresps = $_SESSION['fetch_array']($eselresps)) {

              ?>
              <form action="cadtabula.php" method="post">
              <tr>
                <td class="corfd_colcampos" align="center"><input name="idresposta" id="idrespcad" type="hidden" value="<?php echo $lselresps['idrespostatab'];?>" /><?php echo $lselresps['idrespostatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lselresps['descrirespostatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lselresps['ativo'];?></td>
                <td>
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="carregaresp" type="submit" value="Carregar" />
                    <?php
                    $selmonitab = "SELECT COUNT(*) as result FROM monitabulacao WHERE idrespostatab='".$lselresps['idrespostab']."'";
                    $eselmonitab = $_SESSION['fetch_array']($_SESSION['query']($selmonitab)) or die ("erro na query de consulta das monitorias que contenham a resposta");
                    if($eselmonitab['result'] >= 1) {
                    }
                    else {
                    ?>
                        <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="apagaresp" type="submit" value="Apagar" />
                    <?php
                    }
                    ?>
                </td>
              </tr>
              </form>
              <?php
              }
              ?>
          </table>      
      </div>
    </div>
</body>
</html>
