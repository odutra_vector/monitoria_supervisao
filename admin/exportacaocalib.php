<script type="text/javascript">
    $(document).ready(function() {
        $("#mes").change(function() {
            var mes = $("#mes").val();
            $("#calib").load("/monitoria_supervisao/admin/basecalib.php",{lista:'lista',mes:mes});
        })
        $("#gera").click(function() {
            var mes = $("#mes").val();
            if(mes == "") {
                alert('Favor selecionar o mes');
                return false;	
            }
            else {
                $.blockUI({ message: '<strong>AGUARDE GERANDO BASE...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
            }
        })
        $("li[class*='li']").hide();
        var abre = false;
        $("a").click(function() {
            var grupo = $(this).attr('name');
            if(abre == false) {
                $(".li"+grupo).show();
                abre = true;
            }
            else {
                $(".li"+grupo).hide();
                abre = false;
            }
        })
        
        $("input[id*='del']").click(function() {
            var mes = $(this).attr('name');
            var del = confirm('Você deseja apagar os dados de '+mes);
            if(del == true) {
                $.blockUI({ message: '<strong>AGUARDE APAGANDO BASE...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
                window.location = "basecalib.php?b="+mes;
            }
            else {
                return false;
            }
        })
         <?php
        if($_GET['retorno'] == 1) {
            ?>
            alert('<?php echo $_GET['msg'];?>');
            $.unblockUI();
            <?php
        }
        else {
        }
        ?>                   
    });
</script>
</head>
<body>
    <div id="conteudo">
    <div id="divmes"><br/>
            <form action="basecalib.php" method="post">
                <table width="314">
                  <tr>
                    <td width="40" class="corfd_coltexto"><strong>MÊS</strong></td>
                    <td width="156" class="corfd_colcampos">
                        <select name="mes" id="mes">
                            <option selected="selected" disabled="disabled" value="">SELECIONE O PERIODO...</option>
                            <?php
                            $ano = date('Y');
                            $anos = array($ano,($ano-1));
                            $mes = array('01' => 'JANEIRO','02' => 'FEVEREIRO','03' => 'MARCO','04' => 'ABRIL','05' => 'MAIO','06' => 'JUNHO','07' => 'JULHO','08'=> 'AGOSTO','09' => 'SETEMBRO','10' => 'OUTUBRO','11' => 'NOVEMBRO','12' => 'DEZEMBRO');
                                for($i = 0;$i <= 1;$i++) {
                                    foreach($mes as $nmes => $m) {
                                        echo "<option value=\"$nmes-$anos[$i]\">$m-$anos[$i]</option>";
                                    }
                                }
                            ?>
                        </select>
                    </td>
                    <td width="102"><input name="gera" id="gera" value="EXPORTAR"  type="submit" style="width:150px;background:#EAAC2F; border:2px solid #FFF; font-size: 13px; font-weight: bolder;border-radius:5px" /></td>
                  </tr>
                </table>
            </form>
        </div><br/>
        <div id="calib" style="height:300px; overflow:auto">
            <br/>
            <table width="100%">
              <tr>
              	<td class="corfd_ntab" colspan="7" align="center"><strong>LISTA DE CALIBRAGENS</strong></td>
              </tr>
              <tr>
                <td width="7%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>ID CALIB.</strong></td>
                <td width="7%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>DATA INI</strong></td>
                <td width="7%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>DATA FIM</strong></td>
                <td width="11%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>ID MONITORIA</strong></td>
                <td width="34%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>RELACIONAMENTO</strong></td>
                <td width="26%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>ESPECIALISTA</strong></td>
                <td width="8%" class="corfd_coltexto" style="text-align:center; font-weight:bold"><strong>FINALIZADO</strong></td>
              </tr>
            </table>
        </div><br/><br/>
        <div id="bases">
        <ul style="list-style:none; padding: 5px">
        <?php
        $arq = str_replace("C:", "",$_SERVER['DOCUMENT_ROOT'])."/monitoria_supervisao/basecalib/".$_SESSION['nomecli']."";
        if(file_exists($arq)) {
            $arquivos = scandir($arq);
            foreach($arquivos as $mes) {
                $arquivos = "";
                if($mes == "." OR $mes == "..") {
                }
                else {
                    ?>
                    <li style="font-weight:bold; padding: 2px; font-size: 14px" id="li<?php echo $mes;?>"><?php 
                    echo "<a href=\"#\" style=\"text-decoration:none;color:#000\" name=\"$mes\">$mes</a> <input type=\"button\" style=\"border:0px;width:15px;height:15px;background-image:url(/monitoria_supervisao/images/exit.png)\" name=\"$mes\" id=\"deleta$mes\"/>";
                    $files = str_replace("C:", "",$_SERVER['DOCUMENT_ROOT'])."/monitoria_supervisao/basecalib/".$_SESSION['nomecli']."/$mes";
                    if(file_exists($files)) {
                        $arquivos = scandir($files);
                        foreach($arquivos as $f) {
                            if($f == "." or $f == "..") {
                            }
                            else {
                                ?>
                                <li style="margin-left:20px; padding: 2px" class="li<?php echo $mes;?>"><a target="_blank" href="/monitoria_supervisao/basecalib/<?php echo "".$_SESSION['nomecli']."/$mes/$f";?>" style="text-decoration:none;font-size: 12px"><?php echo $f;?></a></li>
                                <?php
                            }
                        }
                        ?>
                    </li>
                    <?php
                    }
                    else {
                    }
                }
            }
        }
        else {
        }
        ?>
        </ul>
        </div>
    </div>
</body>
</html>
