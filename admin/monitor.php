<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem tÃ­tulo</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $("#cpf").mask("999.999.999-99");
   $("#cpfpesq").mask("999.999.999-99");
   $("#hentra").mask("99:99");
   $("#hsaida").mask("99:99");
   $('#tabmonitor').tablesorter();
   
   $('#cadastra').click(function() {
       var hentra = $('#hentra').val();
       var hsaida = $('#hsaida').val();
       var nome = $('#nome').val();
       var cpf = $('#cpf').val();
       var user = $('#user').val();
       var resp = $('#resp').val();
       if(nome == ""|| cpf == ""|| user == "" || resp == "" || hentra == "" || hsaida == "") {
            alert('Os campos abaixo não podem estar vazios:\n\
                    Nome\n\
                    CPF\n\
                    Usuário\n\
                    Resposável\n\
                    Hora Ent.\n\
                    Hora Saida');
       }
       if(hentra == hsaida) {
           alert('A hora de entrada não pode ser igual a hora de saida');
           return false;
       }
       else {
           var entrah = hentra.substr(0, 2);
           var entram = hentra.substr(3, 2);
           var saidah = hsaida.substr(0, 2);
           var saidam = hsaida.substr(3, 2);
           if(entrah > saidah) {
               alert('A hora de entrada não pode ser maior que a hora de saida');
               return false;
           }
           else {
               if(entram > saidam) {
                   alert('A hora de entrada não pode ser maior que a hora de saida');
                   return false;
               }
               else {
               }
           }
       }
   })

   $("input[id*='logout']").click(function() {
       var ok = confirm('VocÃª tem certeza que desejar deslogar o usuÃ¡rio, o mesmo pode estar realizando monitorias neste momento!!!!');
       if(ok == true) {
       }
       else {
           return false;
       }
   })
});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadmonitor.php" method="post">
<table width="769">
  <tr>
    <td class="corfd_ntab" colspan="6" align="center"><strong>CADASTRO DE MONITOR</strong></td>
  </tr>
  <tr>
    <td width="93" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="265" class="corfd_colcampos"><input style="width:250px; border: 1px solid #69C" name="nome" id="nome" maxlength="100" type="text" /></td>
    <td width="64" class="corfd_coltexto"><strong>CPF</strong></td>
    <td class="corfd_colcampos">
    <input style="width:100px; border: 1px solid #69C; text-align:center" name="cpf" id="cpf" type="text" />
    </td>
    <td class="corfd_coltexto"><strong>Usuário</strong></td>
    <td colspan="5" class="corfd_colcampos"><input style="width:100px; border: 1px solid #69C" maxlength="20" name="user" id="user" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Responsável</strong></td>
    <td class="corfd_colcampos"><select name="resp" id="resp">
    <?php
	$seluser = "SELECT * FROM user_adm WHERE ativo='S'";
	$euser = $_SESSION['query']($seluser) or die (mysql_error());
	while($luser = $_SESSION['fetch_array']($euser)) {
		echo "<option value=\"".$luser['iduser_adm']."\">".$luser['nomeuser_adm']."</option>";
	}
	?>
    </select></td>
    <td class="corfd_coltexto"><strong>Hora Ent.</strong></td>
    <td width="140" class="corfd_colcampos">
    <input style="width:50px; border: 1px solid #69C; text-align:center" name="hentra" id="hentra" type="text" />
	</td>
    <td width="77" class="corfd_coltexto"><strong>Hora Saida</strong></td>
    <td width="102" class="corfd_colcampos"><input style="width:50px; border: 1px solid #69C; text-align:center" name="hsaida" id="hsaida" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tipo Monitor</strong></td>
    <td class="corfd_colcampos" colspan="5">
        <select name="tmonitor" id="tmonitor">
            <?php
            $tmoni = array('I','E');
            foreach($tmoni as $t) {
                    echo "<option value=\"$t\">$t</option>";
            }
            ?>
        </select>
    </td>
  </tr>
  <tr>
    <td colspan="6"><input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastra" type="submit" value="Cadastrar" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font><hr /><br />
<form action="" method="post">
<table width="418" border="0">
  <tr>
    <td class="corfd_ntab" colspan="2" align="center"><strong>PESQUISA DE MONITORES</strong></td>
  </tr>
  <tr>
    <td width="159" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="252" class="corfd_colcampos"><input name="menu" value="monitor" type="hidden" /> <input style="width:250px; border: 1px solid #69C; text-align:center" maxlength="100" name="nome" type="text" value="<?php echo $_POST['nome']; ?>" /><input name="p" value="1" type="hidden" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>CPF</strong></td>
    <td class="corfd_colcampos">
    <input style="width:100px; border: 1px solid #69C; text-align:center"  type="text" name="cpf" id="cpfpesq" value="<?php echo $_POST['cpf']; ?>" />
    </td>
  </tr>
  <tr>
  <td class="corfd_coltexto"><strong>ResponsÃ¡vel</strong></td>
  <td class="corfd_colcampos"><select name="resp"><option selected="selected" disabled="disabled" value="">SELECIONE</option>
  <?php
  $sql = "SELECT * FROM user_adm";
  $exe = $_SESSION['query']($sql) or die (mysql_error());
  while($lnome = $_SESSION['fetch_array']($exe)) {
	  $selected=($_POST['resp'] == $lnome['id'] ? "SELECTED" : "");
	  echo "<option ";
	  if($lnome['iduser_adm'] == $_POST['resp']) {
	    echo "selected=\"selected\"";
	  }
	  else {
	  }
	  echo "value=\"".$lnome['iduser_adm']."\">".$lnome['nomeuser_adm']."</option>";
  }
  ?>
  </select></td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesquisa" type="submit" value="Pesquisar" /></td>
  </tr>
</table>
</form><br />
<div style="width: 1024px; height: 400px; overflow: auto">
<font color="#FF0000"><strong><?php echo $_GET['msgi']; ?></strong></font>
<?php
$dados .= "<table width=\"1100\" border=\"0\" id=\"tabmonitor\">";
  $dados .= "<thead>";
    $dados .= "<th width=\"53\" class=\"corfd_coltexto\" align=\"center\"><strong>ID</strong></th>";
    $dados .= "<th width=\"251\" class=\"corfd_coltexto\" align=\"center\"><strong>NOME</strong></th>";
    $dados .= "<th width=\"144\" class=\"corfd_coltexto\" align=\"center\"><strong>RESP.</strong></th>";
    $dados .= "<th width=\"87\" class=\"corfd_coltexto\" align=\"center\"><strong>USUÁRIO</strong></th>";
    $dados .= "<th width=\"87\" class=\"corfd_coltexto\" align=\"center\"><strong>STATUS</strong></th>";
    $dados .= "<th width=\"68\" class=\"corfd_coltexto\" align=\"center\"><strong>ENTRADA</strong></th>";
    $dados .= "<th width=\"50\" class=\"corfd_coltexto\" align=\"center\"><strong>SAIDA</strong></th>";
    $dados .= "<th width=\"45\" class=\"corfd_coltexto\" align=\"center\"><strong>ATIVO</strong></th>";
    $dados .= "<th width=\"47\" class=\"corfd_coltexto\" align=\"center\" title=\"SENHA INICIAL ALTERADA\"><strong>S.I</strong></th>";
    $dados .= "<th width=\"47\" class=\"corfd_coltexto\" align=\"center\" title=\"TIPO MONITOR\"><strong>T. M</strong></th>";
    $dados .= "<th width=\"220\"></th>";
  $dados .= "</thead>";
  $dados .= "<tbody>";
  $dadosexp = $dados;
  $nome = mysql_real_escape_string($_POST['nome']);
  $cpf = mysql_real_escape_string(str_replace("-", "", $_POST['cpf']));
  $cpf = str_replace(".", "", $cpf);
  $resp = mysql_real_escape_string($_POST['resp']);
  $caracter = addslashes(strtoupper($nome));
  if($nome != "") {
      $where[] = "UPPER(nomemonitor) LIKE '$caracter%'";
  }
  if($cpf != "") {
      $where[] = "cpf='$cpf'";
  }
  if($resp != "") {
      $where[] = "iduser_resp='$resp'";
  }
  if($where != "") {
      $where = "WHERE ".implode(" AND ",$where);
  }
  else {
      $where == "";
  }
  $seldb = "SELECT * FROM monitor $where ORDER BY nomemonitor";
  $monitor = "$seldb";
    $emonitor = $_SESSION['query']($monitor) or die (mysql_error());
    while($lmonitor = $_SESSION['fetch_array']($emonitor)) {
      $selresp = "SELECT nomeuser_adm FROM user_adm WHERE iduser_adm='".$lmonitor['iduser_resp']."'";
      $eresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die (mysql_error());
      $cpf1 = substr($lmonitor['CPF'], 0, 3);
      $cpf2 = substr($lmonitor['CPF'], 3, 3);
      $cpf3 = substr($lmonitor['CPF'], 6, 3);
      $cpfdig = substr($lmonitor['CPF'], 9, 2);
      $cpf = $cpf1.".".$cpf2.".".$cpf3."-".$cpfdig;
      $dados .= "<tr>";
      $dadosexp .= "<tr>";
      $dados .= "<form action=\"cadmonitor.php\" method=\"post\">";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\"><input style=\"width:50px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" name=\"id\" type=\"hidden\" value=\"".$lmonitor['idmonitor']."\" />".$lmonitor['idmonitor']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['idmonitor']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['nomemonitor']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['nomemonitor']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$eresp['nomeuser_adm']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$eresp['nomeuser_adm']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['usuario']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['usuario']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['status']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['status']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['entrada']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['entrada']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['saida']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['saida']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['ativo']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['ativo']."</td>";
      $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['senha_ini']."</td>";
      $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['senha_ini']."</td>";
	  $dados .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['tmonitor']."</td>";
          $dadosexp .= "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lmonitor['tmonitor']."</td>";
      $dados .= "<td><input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"altera\" type=\"submit\" value=\"Alterar\" /> <input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"dimen\" type=\"submit\" value=\"Dimen.\" /> <input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"logout\" id=\"logout".$lmonitor['idmonitor']."\" type=\"submit\" value=\"Logout\" /> </td>";
      $dados .= "</form>";
      $dados .= "</tr>";
      $dadosexp .= "</tr>";
    }
  $dados .= "</tbody>";
  $dadosexp .= "</tbody>";
$dados .= "</table>";
$dadosexp .= "</table>";
echo "<table width=\"1024\">";
    echo "<tr>";
        echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=usuarios_adm\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">EXCEL</div></a></td>";
        echo "<td><a style=\"text-decoration:none; text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=WORD&nome=usuarios_adm\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">WORD</div></a></td>";
        echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" target=\"blank\" href=\"/monitoria_supervisao/users/export.php?tipo=PDF&nome=usuarios_adm\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">PDF</div></a></td>";
    echo "</tr>";
echo "</table><br/>";
$_SESSION['dadosexp'] = $dadosexp;
echo $dados;
?>
</div>
</div>
</body>
</html>
