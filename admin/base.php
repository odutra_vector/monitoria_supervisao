<?php

session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.monitoria.php');
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$arq = $_POST['caminhobase'];

if(isset($_POST['gerar'])) {
    $tab = new TabelasSql();
    $filtros = "SHOW COLUMNS FROM rel_filtros";
    $efiltros = $_SESSION['query']($filtros) or die ("erro na query de consulta dos filtros");
    while($lselfiltros = $_SESSION['fetch_array']($efiltros)) {
        if($lselfiltros['Field'] == "idrel_filtros") {
        }
        else {
            $filtrostab[] = strtoupper(str_replace("id_","",$lselfiltros['Field']));
            $f[$lselfiltros['Field']] = str_replace("id","filtro",$lselfiltros['Field']);
            $colf[] = $tab->AliasTab(strtolower(str_replace("id_","",$lselfiltros['Field']))).".nomefiltro_dados as ".str_replace("id_","",$lselfiltros['Field'])."";
            $innerf[] = "INNER JOIN filtro_dados ".$tab->AliasTab(strtolower(str_replace("id_","",$lselfiltros['Field'])))." ON ".$tab->AliasTab(strtolower(str_replace("id_","",$lselfiltros['Field']))).".idfiltro_dados=".$tab->AliasTab(rel_filtros).".".$lselfiltros['Field']."";
        }
    }

    // variaveis, tabelas e alias de tabelas
    $var = array('dtinimoni' => 'data','dtfimmoni' => 'data','dtinictt' => 'datactt','dtfimctt' => 'datactt',
        'super_oper' => 'super_oper','oper' => 'operador','nomemonitor' => 'nomemonitor','fg' => 'qtdefg',
        'fluxo' => 'idfluxo','atustatus' => 'idatustatus','indice' => 'idindice','intervalo' => 'idintervalo_notas',
        'plan' => 'idplanilha','aval' => 'idaval_plan','grupo' => 'idgrupo','sub' => 'idsubgrupo',
        'perg' => 'idpergunta','resp' => 'idresposta','idalerta' => 'alertas');
    $tabelas = array('dtinimoni' => 'monitoria','dtfimmoni' => 'monitoria','dtinictt' => 'monitoria','dtfimctt' => 'monitoria',
            'super_oper' => 'super_oper','oper' => 'operador','nomemonitor' => 'monitor','fg' => 'monitoria','fgm' => 'monitoria','fluxo' => 'fluxo',
            'atustatus' => 'rel_fluxo','indice' => 'indice','intervalo' => 'intervalo_notas','plan' => 'moniavalia','aval' => 'moniavalia',
            'grupo' => 'moniavalia','sub' => 'moniavalia','perg' => 'moniavalia','resp' => 'moniavalia','idalerta' => 'monitoria');
    foreach($tabelas as $col => $t) {
        $aliascol[$col] = $tab->AliasTab($t);
        $aliastab[$t] = $tab->AliasTab($t);
    }
    $aliastab['monitoria_fluxo'] = $tab->AliasTab(monitoria_fluxo);
    $aliastab['moniavalia'] = $tab->AliasTab(moniavalia);
    $aliastab['rel_fluxo'] = $tab->AliasTab(rel_fluxo);
    $aliastab['fila_grava'] = $tab->AliasTab(fila_grava);
    $aliastab['status'] = $tab->AliasTab(status);
    $aliastab['indice'] = $tab->AliasTab(indice);
    $aliastab['intervalo_notas'] = $tab->AliasTab(intervalo_notas);
    $aliastab['conf_rel'] = $tab->AliasTab(conf_rel);
    $aliastab['param_moni'] = $tab->AliasTab(param_moni);
    $aliastab['status'] = $tab->AliasTab(status);
    $aliastab['definicao'] = $tab->AliasTab(definicao);
    $aliastab = array_unique($aliastab);

    //levantamento dos rel_filtros
    $seljv = "SELECT * FROM filtro_nomes WHERE ativo='S'";
    $eseljv = $_SESSION['query']($seljv) or die ("erro na query de consulta dos filtros para JV");
    while($lseljv = $_SESSION['fetch_array']($eseljv)) {
        if($_POST['filtro_'.strtolower($lseljv['nomefiltro_nomes'])] != "") {
            $whererel[] = "id_".strtolower($lseljv['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lseljv['nomefiltro_nomes'])]."'";
        }
    }
    if($whererel != "") {
        $selidsrel = "SELECT rf.idrel_filtros FROM rel_filtros rf"
                . " INNER JOIN useradmfiltro uf ON uf.idrel_filtros = rf.idrel_filtros"
                . " WHERE ".implode(" AND ",$whererel)." AND iduser_adm='".$_SESSION['usuarioID']."'";
        $eselids = $_SESSION['query']($selidsrel) or die ("erro na query de consulta dos relacionamentos");
        while($lselids = $_SESSION['fetch_array']($eselids)) {
            $idssql[] = $lselids['idrel_filtros'];
        }
    }


    // valores para montar o where da tabela monitoria
    $innerunico = array('monitor','operador');
    $innerspe = array('rel_filtros','status','fluxo','atustatus','indice','intervalo');
    foreach($var as $post => $colpost) {
        if($_POST[$post] != "") {
            if(eregi("filtro_",$post)) {
                if(key_exists($colpost, $where)) {
                }
                else {
                    $where[$colpost] = $tab->AliasTab(rel_filtros).".idrel_filtros='".$_POST[$post]."'";
                }
            }
            else {
                if($post == "dtinimoni" OR $post == "dtfimmoni") {
                    if(key_exists($colpost, $where)) {
                    }
                    else {
                        $where[$colpost] = $aliascol[$post].".data BETWEEN '".data2banco($_POST['dtinimoni'])."' AND '".data2banco($_POST['dtfimmoni'])."'";
                    }
                }
                else {
                    if($post == "dtinictt" OR $post == "dtfimctt") {
                        if(key_exists($colpost, $where)) {
                        }
                        else {
                            $where[$colpost] = $aliascol[$post].".datactt BETWEEN '".data2banco($_POST['dtinictt'])."' AND '".data2banco($_POST['dtfimctt'])."'";
                        }
                    }
                    else {
                        if($post == "intervalo") {
                            if(key_exists($colpost, $where)) {
                            }
                            else {
                                $selinter = "SELECT numini,numfim FROM intervalo_notas WHERE idintervalo_notas='".$_POST[$post]."'";
                                $eselinter = $_SESSION['fetch_array']($_SESSION['query']($selinter)) or die ("erro na query de consulta do intervalo");
                                $where[$colpost] = $aliastab['moniavalia'].".valor_fg BETWEEN '".$eselinter['numini']."' AND '".$eselinter['numfim']."'";
                            }
                        }
                        else {
                            if($post == "fg" OR $post == "fgm") {
                                if(key_exists("fg", $where)) {
                                }
                                else {
                                    $where[$colpost] = $aliascol[$post].".$colpost >= 1";
                                }
                                if(key_exists("fgm", $where)) {
                                }
                                else {
                                    $where[$colpost] = $aliascol[$post].".$colpost >= 1";
                                }
                            }
                            else {
                                if(key_exists($colpost, $where)) {
                                }
                                else {
                                    if($post == "idalerta") {
                                        $where[$colpost] = $aliascol[$post].".$colpost LIKE '%".$_POST[$post]."%'";
                                    }
                                    else {
                                        $where[$colpost] = $aliascol[$post].".$colpost='".$_POST[$post]."'";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //if(in_array($tabelas[$post],$innerunico)) {
                //$inner[] = "INNER JOIN $colpost $aliascol[$post] ON $alias[$post].$colpost=".$tab->AliasTab(monitoria).".$colpost";
            //}
            //if(in_array($tabelas[$post],$innerspe)) {
                //if($post == "") {

                //}
            //}
        }
        else {
        }
    }
    if($idssql != "") {
        $where['idrel_filtros'] = "$aliastab[monitoria].idrel_filtros IN ('".implode("','",$idssql)."')";
    }

    if($_POST['plan'] != "") {
        $selplan = "SELECT descriplanilha, idplanilha FROM planilha WHERE idplanilha='".$_POST['plan']."'";
        $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta do nome da planilha");
        $planilhas[$eselplan['idplanilha']] = $eselplan['descriplanilha'];
    }
    else {
        if($whererel != "") {
            $wrelplan = implode(" AND ",$whererel);
            $selplan = "SELECT idplanilha_web FROM conf_rel cr
                        INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros WHERE $wrelplan GROUP BY cr.idrel_filtros";
            $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das planilhas vinculadas ao relacionamento");
            while($lselplan = $_SESSION['fetch_array']($eselplan)) {
                $selnome = "SELECT descriplanilha, idplanilha FROM planilha WHERE ativoweb='S' AND idplanilha IN (".$lselplan['idplanilha_web'].") GROUP BY idplanilha";
                $eselnome = $_SESSION['query']($selnome) or die ("erro na query de consulta do nome da planilha");
                while($lselnome = $_SESSION['fetch_array']($eselnome)) {
                    $selmonip = "SELECT COUNT(*) as result FROM monitoria WHERE idplanilha='".$lselnome['idplanilha']."' AND datactt BETWEEN '".data2banco($_POST['dtinictt'])."' AND '".data2banco($_POST['dtfimctt'])."'";
                    $eselmonip = $_SESSION['fetch_array']($_SESSION['query']($selmonip)) or die ("erro na query de consulta das monitorias por planilha");
                    if($eselmonip['result'] > 0) {
                        if(!key_exists($lselnome['idplanilha'], $planilhas)) {
                            $planilhas[$lselnome['idplanilha']] = $lselnome['descriplanilha'];
                        }
                    }
                }

            }
        }
        else {
            $selplan = "SELECT descriplanilha, idplanilha FROM planilha WHERE ativoweb='S' GROUP BY idplanilha";
            $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das planilhas");
            while($lselplan = $_SESSION['fetch_array']($eselplan)) {
                $selmonip = "SELECT COUNT(*) as result FROM monitoria WHERE idplanilha='".$lselplan['idplanilha']."' AND datactt BETWEEN '".data2banco($_POST['dtinictt'])."' AND '".data2banco($_POST['dtfimctt'])."'";
                $eselmonip = $_SESSION['fetch_array']($_SESSION['query']($selmonip)) or die ("erro na query de consulta das monitorias por planilha");
                if($eselmonip['result'] > 0) {
                    if(!key_exists($lselplan['idplanilha'], $planilhas)) {
                        $planilhas[$lselplan['idplanilha']] = $lselplan['descriplanilha'];
                    }
                }
            }
        }
    }

    foreach($planilhas as $idplan => $descri) {        
          $arraycol = array();
          $dados = array();
          $grupos = array();
          $subgrupos = array();
          $perguntas = array();
          $respostas = array();
          $perguntastab = array();
          $nomeplan = $eselplan['descriplanilha'];
          $data = date('dmY');
          $hora = date('His');
          $nomearq = "base-".utf8_decode($descri)."-".str_replace("/","",$_POST['dtinictt'])."-".str_replace("/","",$_POST['dtfimctt'])."-$data-$hora".".csv";
            if(file_exists($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli'])) {
            }
            else {
                $cria = mkdir($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']);
            }
	    if(file_exists($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']."/$nomearq")) {
	        unlink($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']."/$nomearq");
	        $criarq = fopen($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']."/$nomearq",'w');
                $cri++;
	    }
	    else {
	        $criarq = fopen($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']."/$nomearq",'w');
                $cri++;
	    }

                $arraycol[] = "ID MONITORIA";
                $arraycol[] = "ID PRINCIPAL";
                if($_SESSION['nomecli'] == "ITAU") {
                    $arraycol[] = "IDVENDA";
                    $idvenda = ",".$aliastab['fila_grava'].".idmonitoriavenda";
                }
                //$html ="<table width=\"auto\">";
                //$html .= "<tr>";
				//$html .= "<th scope=\"col\" align=\"center\"><strong>ID MONITORIA</strong></th>";
                foreach($filtrostab as $nome) {
                        $arraycol[] = $nome;
                        //$html .= "<th scope=\"col\" align=\"center\"><strong>$nome</strong></th>";
                }
                $arraycol[] = "MONITOR";
                $arraycol[] = "OPERADOR";
                if($_SESSION['nomecli'] != "ITAU") {
                    if($_SESSION['nomecli'] == "PJ_ITAU") {
                        $arraycol[] = "CNPJ CLIENTE";
                        $nomecli = $aliastab['fila_grava'].".cnpj_cliente,";
                    }
                }
                else {
                    $arraycol[] = "NOME CLIENTE";
                    if($_SESSION['nomecli'] == "ITAU") {
                        $arraycol[] = "IDM1";
                        $idm1 = ",".$aliastab['fila_grava'].".idm1";
                    }
                    $nomecli = $aliastab['fila_grava'].".nome_cliente,";
                }
                $arraycol[] = "TURNO";
                $arraycol[] = "STATUS_REAL";
                $arraycol[] = "DATA ADMISSAO";
                $arraycol[] = "Cnpj_cpf_idcontrato";
                $arraycol[] = "DT MONITORIA";
                $arraycol[] = "HORA MONITORIA";
                $arraycol[] = "TEMPO MONI";
                $arraycol[] = "DT CONTATO";
                $arraycol[] = "HR CONTATO";
                $arraycol[] = utf8_decode("DURAÇÃO");
                $arraycol[] = "ALERTAS";
                $arraycol[] = "STATUS";
                $arraycol[] = "DEFINICAO";
                $arraycol[] = utf8_decode("DATA AÇÃO");
                //$arraycol[] = "OBSERVAÇÃO";
                $arraycol[] = "NOTA";
                $arraycol[] = "NOTA SEM FG";
                $arraycol[] = "QTDE. RESP. FG";
                $arraycol[] = "FG POR BLOCO";
                $selplan = "SELECT p.idgrupo,descrigrupo,g.tab FROM planilha p
                                    INNER JOIN grupo g ON g.idgrupo = p.idgrupo
                                    WHERE idplanilha='$idplan' and p.ativo='S' GROUP BY p.idgrupo ORDER BY p.posicao";
                $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta da planilha");
                while($lselplan = $_SESSION['fetch_array']($eselplan)) {
                    if($lselplan['tab'] == "N") {
                        $grupos[$lselplan['idgrupo']] = $lselplan['descrigrupo'];
                    }
                    else {
                        $seltab = "SELECT idpergunta,descripergunta FROM grupo g
                                         INNER JOIN pergunta p ON p.idpergunta = g.idrel
                                         WHERE idgrupo='".$lselplan['idgrupo']."' GROUP BY idpergunta";
                        $eseltab = $_SESSION['query']($seltab) or die (mysql_error());
                        while($lseltab = $_SESSION['fetch_array']($eseltab)) {
                            $perguntastab[$lselplan['idgrupo']][$lseltab['idpergunta']] = $lseltab['descripergunta'];
                        }
                    }
                }
                foreach($grupos as $idgrupo => $ngrupo) {
                    $arraycol[] = utf8_decode($ngrupo);
                    //$html .= "<th scope=\"col\" align=\"center\"><strong>$ngrupo</strong></th>";
                    $selsub = "SELECT s.idsubgrupo,s.descrisubgrupo,valor_sub,g.tab FROM grupo g
                                    INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                                    WHERE g.idgrupo='$idgrupo' AND g.ativo='S' GROUP BY s.idsubgrupo ORDER BY g.posicao";
                    $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta do sub grupo");
                    while($lselsub = $_SESSION['fetch_array']($eselsub)) {
                        if(key_exists($lselsub['idsubgrupo'], $subgrupos) OR ($lselsub['valor_sub'] == "" OR $lselsub['valor_sub'] == "NULL")) {
                        }
                        else {
                            $arraycol[] = $lselsub['descrisubgrupo'];
                            //$html .= "<th scope=\"col\" align=\"center\"><strong>".$lselsub['descrisubgrupo']."</strong></th>";
                            $subgrupos[$idgrupo][$lselsub['idsubgrupo']] = $lselsub['descrisubgrupo'];
                            $selpergsub = "SELECT * FROM subgrupo s
                                                    INNER JOIN pergunta p ON p.idpergunta = s.idpergunta
                                                    WHERE s.idsubgrupo='".$lselsub['idsubgrupo']."' AND s.ativo='S' GROUP BY p.idpergunta ORDER BY s.posicao";
                            $eselpergsub = $_SESSION['query']($selpergsub) or die ("erro na query de consulta das perguntas do subgrupo");
                            while($lselpergsub = $_SESSION['fetch_array']($eselpergsub)) {
                                if(key_exists($lselpergsub['idpergunta'], $perguntas) OR ($eseperg['valor_perg'] == "" OR $eseperg['valor_perg'] == "NULL")) {
                                }
                                else {
                                    if($lselpergsub['tab'] == "S") {
                                        $perguntastab[$lselpergsub['idsubgrupo']][$lselpergsub['idpergunta']] = $lselpergsub['descripergunta'];
                                    }
                                    else {
                                        $arraycol[] = utf8_decode($lselpergsub['descripergunta']);
                                        //$html .= "<th scope=\"col\" align=\"center\"><strong>".$lselpergsub['descripergunta']."</strong></th>";
                                        $perguntas[$lselpergsub['idsubgrupo']][$lselpergsub['idpergunta']] = $lselpergsub['descripergunta'];
                                        $selresp = "SELECT * FROM pergunta WHERE idpergunta='".$lselperg['idpergunta']."' AND ativo_resp='S' ORDER BY p.posicao";
                                        $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
                                        while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                            if(key_exists($lselresp['idresposta'], $repsosta)) {
                                            }
                                            else {
                                                if($lselresp['avalia'] == 'AVALIACAO_OK') {
                                                }
                                                else {
                                                    $arraycol[] = utf8_decode($lselresp['descriresposta']);
                                                    //$html .= "<th scope=\"col\" align=\"center\"><strong>".$lselresp['descriresposta']."</strong></th>";
                                                    $respostas[$lselresp['idpergunta']][$lselresp['idresposta']] = $lselresp['descriresposta'];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $selperg = "SELECT p.idpergunta,p.valor_perg,p.descripergunta,g.tab FROM grupo g
                                        INNER JOIN pergunta p ON p.idpergunta = g.idrel
                                        WHERE g.idgrupo='$idgrupo' AND g.ativo='S' GROUP BY p.idpergunta ORDER BY g.posicao";
                    $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas relacionadas");
                    while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                        if(key_exists($lselperg['idpergunta'], $perguntas) OR ($lselperg['valor_perg'] == "" OR $lselperg['valor_perg'] == "NULL")) {
                            if($lselperg['tab'] == "S") {
                                if(!key_exists($lselperg['idpergunta'], $perguntastab[$idgrupo])) {
                                    $perguntastab[$idgrupo][$lselperg['idpergunta']] = $lselperg['descripergunta'];
                                }
                            }
                        }
                        else {
                            if($lselperg['tab'] == "S") {
                                $perguntastab[$idgrupo][$lselperg['idpergunta']] = $lselperg['descripergunta'];
                            }
                            else {
                                $arraycol[] = utf8_decode($lselperg['descripergunta']);
                                //$html .= "<th scope=\"col\" align=\"center\"><strong>".$lselperg['descripergunta']."</strong></th>";
                                $perguntas[$idgrupo][$lselperg['idpergunta']] = $lselperg['descripergunta'];
                                $selresp = "SELECT * FROM pergunta WHERE idpergunta='".$lselperg['idpergunta']."' AND ativo_resp='S' ORDER BY posicao";
                                $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
                                while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                    if(key_exists($lselresp['idresposta'], $repsosta)) {
                                    }
                                    else {
                                        if($lselresp['avalia'] == 'AVALIACAO_OK') {
                                        }
                                        else {
                                            $arraycol[] = utf8_decode($lselresp['descriresposta']);
                                            //$html .= "<th scope=\"col\" align=\"center\"><strong>".$lselresp['descriresposta']."</strong></th>";
                                            $respostas[$lselresp['idpergunta']][$lselresp['idresposta']] = $lselresp['descriresposta'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach($perguntas as $id => $pergs) {
                    foreach($pergs as $idpe => $nperg) {
                        $selfg = "SELECT avalia, valor_perg FROM pergunta WHERE idpergunta='".$idpe."'";
                        $eselfg = $_SESSION['fetch_array']($_SESSION['query']($selfg)) or die ("erro na query de consulta do tipo de avaliação da pergunta");
                        $avaliafora = array('FG','TABULACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','FGM','SUPERACAO');
                        if(in_array($eselfg['avalia'],$avaliafora) OR $eselfg['valor_perg'] == "100") {
                        }
                        else {
                            $arraycol[] = utf8_decode($nperg);
                            //$html .= "<th align=\"center\"><strong>Nota Impacto - ".$nperg."</strong></th>";
                        }
                    }
                }
                $arraycol[] = "NCG CHECK";
                foreach($perguntastab as $gtab) {
                    foreach($gtab as $idperg => $descri) {
                        $arraycol[] = $descri;
                        $arraycol[] = "OBS - $descri";
                    }
                }
                $linha = fwrite($criarq, utf8_decode(implode(";",$arraycol)."\r\n"));
                usleep(1000);
              //$html .= "</tr>";
              if($where != "") {
                $where['idplanilha'] = $tab->AliasTab(moniavalia).".idplanilha='$idplan'";
                $wheremoni = "WHERE ".implode(" AND ",$where);
              }

            $monitoria = "SELECT ".$aliastab['monitoria'].".alertas,".$aliastab['monitoria'].".idmonitoria,$nomecli TIMEDIFF(monita.horafim, monita.horaini) as tempo,".$aliastab['moniavalia'].".valor_fg,
                            ".$aliastab['moniavalia'].".valor_final_aval,".$aliastab['monitoria'].".data,".$aliastab['monitoria'].".horaini,".$aliastab['monitoria'].".datactt,".$aliastab['monitoria'].".horactt,".$aliastab['monitoria'].".tmpaudio,
                            ".$aliastab['monitoria'].".obs, ".implode(",",$colf).",".$aliastab['monitor'].".nomemonitor,".$aliastab['operador'].".operador,".$aliastab['status'].".nomestatus, ".$aliastab['definicao'].".nomedefinicao,".$aliastab['monitoria_fluxo'].".data as dtstatus,
                            ".$aliastab['monitoria'].".checkfg,".$aliastab['fila_grava'].".turno,".$aliastab['fila_grava'].".status_real,".$aliastab['fila_grava'].".data_de_admissao,".$aliastab['fila_grava'].".ra_cnpj_cpf_idcontrato $idm1 $idvenda FROM monitoria ".$aliastab['monitoria']."
                            INNER JOIN rel_filtros ".$tab->AliasTab(rel_filtros)." ON ".$tab->AliasTab(rel_filtros).".idrel_filtros=".$aliastab['monitoria'].".idrel_filtros
                            ".implode(" ",$innerf)."
                            INNER JOIN fila_grava ".$aliastab['fila_grava']." ON ".$aliastab['fila_grava'].".idfila_grava=".$aliastab['monitoria'].".idfila
                            INNER JOIN moniavalia ".$aliastab['moniavalia']." ON ".$aliastab['moniavalia'].".idmonitoria=".$aliastab['monitoria'].".idmonitoria
                            INNER JOIN conf_rel ".$aliastab['conf_rel']." ON ".$aliastab['conf_rel'].".idrel_filtros=".$aliastab['monitoria'].".idrel_filtros
                            INNER JOIN param_moni ".$aliastab['param_moni']." ON ".$aliastab['param_moni'].".idparam_moni=".$aliastab['conf_rel'].".idparam_moni
                            INNER JOIN indice ".$aliastab['indice']." ON ".$aliastab['indice'].".idindice=".$aliastab['param_moni'].".idindice
                            INNER JOIN intervalo_notas ".$aliastab['intervalo_notas']." ON ".$aliastab['intervalo_notas'].".idindice=".$aliastab['indice'].".idindice
                            INNER JOIN monitoria_fluxo ".$aliastab['monitoria_fluxo']." ON ".$aliastab['monitoria_fluxo'].".idmonitoria_fluxo=".$aliastab['monitoria'].".idmonitoria_fluxo
                            INNER JOIN rel_fluxo ".$aliastab['rel_fluxo']." ON ".$aliastab['rel_fluxo'].".idrel_fluxo=".$aliastab['monitoria_fluxo'].".idrel_fluxo
                            INNER JOIN fluxo ".$aliastab['fluxo']." ON ".$aliastab['fluxo'].".idfluxo=".$aliastab['rel_fluxo'].".idfluxo
                            INNER JOIN status ".$aliastab['status']." ON ".$aliastab['status'].".idstatus=".$aliastab['rel_fluxo'].".idatustatus
                            INNER JOIN definicao ".$aliastab['definicao']." ON ".$aliastab['definicao'].".iddefinicao=".$aliastab['monitoria_fluxo'].".iddefinicao
                            INNER JOIN monitor ".$aliastab['monitor']." ON ".$aliastab['monitor'].".idmonitor=".$aliastab['monitoria'].".idmonitor
                            INNER JOIN operador ".$aliastab['operador']." ON ".$aliastab['operador'].".idoperador=".$aliastab['monitoria'].".idoperador
                            INNER JOIN super_oper ".$aliastab['super_oper']." ON ".$aliastab['super_oper'].".idsuper_oper=".$aliastab['monitoria'].".idsuper_oper
                            $wheremoni GROUP BY ".$aliastab['monitoria'].".idmonitoria ORDER BY ".$aliastab['monitoria'].".idmonitoria";
            $emonitoria = $_SESSION['query']($monitoria) or die ("erro na query de consulta das monitorias");
            while($lmonitoria = $_SESSION['fetch_array']($emonitoria)) {
            $selprinc = "SELECT idmonitoria, count(*) as result FROM monitoria WHERE idmonitoriavinc='".$lmonitoria['idmonitoria']."'";
            $eselprinc = $_SESSION['fetch_array']($_SESSION['query']($selprinc)) or die (mysql_error());
            $dados = array();
            $nomeal = array();
              //$html .= "<tr>";
                $dados[] = $lmonitoria['idmonitoria'];
                $dados[] = $eselprinc['idmonitoria'];
                if($_SESSION['nomecli'] == "ITAU") {
                    $dados[] = utf8_decode($lmonitoria['idmonitoriavenda']);
                }
                //$html .= "<td align=\"center\">".$lmonitoria['idmonitoria']."</td>";
                foreach($filtrostab as $ft) {
                    $dados[] = $lmonitoria[strtolower($ft)];
                    //$html .= "<td align=\"center\">".$lmonitoria[strtolower($ft)]."</td>";
                }
                $dados[] = utf8_decode($lmonitoria['nomemonitor']);
                $dados[] = utf8_decode($lmonitoria['operador']);
                if($_SESSION['nomecli'] == "ITAU") {
                    $dados[] = utf8_decode($lmonitoria['nome_cliente']);
                    $dados[] = utf8_decode($lmonitoria['idm1']);
                }
                else {
                    if($_SESSION['nomecli'] == "PJ_ITAU") {
                        $dados[] = utf8_decode($lmonitoria['cnpj_cliente']);
                    }
                }
                $dados[] = $lmonitoria["turno"];
                $dados[] = $lmonitoria["status_real"];
                $dados[] = banco2data($lmonitoria["data_de_admissao"]);
                $dados[] = $lmonitoria["ra_cnpj_cpf_idcontrato"];
                $dados[] = banco2data($lmonitoria['data']);
                $dados[] = $lmonitoria['horaini'];
                $dados[] = $lmonitoria['tempo'];
                $dados[] = banco2data($lmonitoria['datactt']);
                $dados[] = $lmonitoria['horactt'];
                $dados[] = $lmonitoria['tmpaudio'];
                if($lmonitoria['alertas'] == "") {
                    $dados[] = "";
                }
                else {
                    $selalertas = "SELECT nomealerta FROM alertas WHERE idalertas IN (".$lmonitoria['alertas'].")";
                    $ealertas = $_SESSION['query']($selalertas) or die (mysql_error());
                    while($lalertas = $_SESSION['fetch_array']($ealertas)) {
                        $nomeal[] = $lalertas['nomealerta'];
                    }
                    $dados[] = implode(",",$nomeal);
                }
                $dados[] = utf8_decode($lmonitoria['nomestatus']);
                $dados[] = utf8_decode($lmonitoria['nomedefinicao']);
                $dados[] = banco2data($lmonitoria['dtstatus']);
                //$dados[] = $lmonitoria['obs'];
                $dados[] = str_replace(".",",",$lmonitoria['valor_fg']);
                $dados[] = str_replace(".",",",$lmonitoria['valor_final_aval']);
                //$html .= "<td align=\"center\">".$lmonitoria['nomemonitor']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['operador']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['data']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['horaini']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['datactt']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['tmpaudio']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['nomestatus']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['obs']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['valor_fg']."</td>";
                //$html .= "<td align=\"center\">".$lmonitoria['valor_final_aval']."</td>";
                $fg = 0;
                $fgporresp = 0;
                $idpergunta = "";
                $qtdefg = "SELECT * FROM moniavalia WHERE idmonitoria='".$lmonitoria['idmonitoria']."'";
                $eqtdefg = $_SESSION['query']($qtdefg) or die ("erro na query de consulta das resposta com FG por pergunta");
                while($lqtdefg = $_SESSION['fetch_array']($eqtdefg)) {
                    //if($lmonitoria['idmonitoria'] == "60230") {
                    //    "OK";
                    //}
                    //else {
                    //}
                    if($lqtdefg['avalia'] == "FG" OR $lqtdefg['avalia'] == "FGM") {
                        $fg++;
                    }
                    if(($lqtdefg['avalia'] == "FG" OR $lqtdefg['avalia'] == "FGM") && $idpergunta != $lqtdefg['idpergunta']) {
                        $fgporresp++;
                        $idpergunta = $lqtdefg['idpergunta'];
                    }
                }
                $dados[] = $fg;
                $dados[] = $fgporresp;
                //$html .= "<td align=\"center\">".$fg."</td>";
                //$html .= "<td align=\"center\">".$fgporresp."</td>";
                reset($respostas);
                reset($perguntas);
                reset($grupos);
                reset($subgrupo);
                foreach($grupos as $idg => $ng) {
                    $selavalia = "SELECT COUNT(*) as result FROM moniavalia ma
                                    INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                    WHERE idmonitoria='".$lmonitoria['idmonitoria']."' AND idgrupo='$idg'
                                    AND p.avalia IN ('FG','FGM','AVALIACAO','PERC','SUPERCAO')";
                    $eselavalia = $_SESSION['fetch_array']($_SESSION['query']($selavalia)) or die ("erro na query de consulta da quantidade de respostas");
                    $dados[] = $eselavalia['result'];
                    //$html .= "<td align=\"center\">".$eselavalia['result']."</td>";
                    foreach($subgrupos[$idg] as $ids => $ns) {
                        $selavalia = "SELECT COUNT(*) as result FROM moniavalia ma
                                        INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                        WHERE idmonitoria='".$lmonitoria['idmonitoria']."'
                                        AND p.avalia IN ('FG','FGM','AVALIACAO','PERC','SUPERCAO')";
                        $eselavalia = $_SESSION['fetch_array']($_SESSION['query']($selavalia)) or die ("erro na query de consulta da quantidade de respostas");
                        $dados[] = $eselavalia['result'];
                        //$html .= "<td align=\"center\">".$eselavalia['result']."</td>";
                        foreach($perguntas[$ids] as $idp => $np) {
                            $selavalia = "SELECT COUNT(*) as result FROM moniavalia ma
                                                  INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                                  WHERE idmonitoria='".$lmonitoria['idmonitoria']."'
                                                  AND p.avalia IN ('FG','FGM','AVALIACAO','PERC','SUPERCAO')";
                            $eselavalia = $_SESSION['fetch_array']($_SESSION['query']($selavalia)) or die ("erro na query de consulta da quantidade de perguntas");
                            $dados[] = $eselavalia['result'];
                            //$html .= "<td align=\"center\">".$eselavalia['result']."</td>";
                            foreach($respostas[$idp] as $idr => $nr) {
                                $selavalia = "SELECT COUNT(*) as result FROM moniavalia ma
                                                     INNER JOIN pergunta p ON p.idreposta = ma.idresposta
                                                     WHERE idmonitoria='".$lmonitoria['idmonitoria']."' AND ma.idresposta='$idr'
                                                     AND p.avalia IN ('FG','FGM','AVALIACAO','PERC','SUPERCAO')";
                                $eselavalia = $_SESSION['fetch_array']($_SESSION['query']($selavalia)) or die ("erro na query de consulta da quantidade de respostas");
                                $dados[] = $eselavalia['result'];
                                //$html .= "<td align=\"center\">".$eselavalia['result']."</td>";
                            }
                        }
                    }
                    foreach($perguntas[$idg] as $idp => $np) {
                        $selavalia = "SELECT COUNT(*) as result FROM moniavalia ma
                                             INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                             WHERE idmonitoria='".$lmonitoria['idmonitoria']."' AND ma.idpergunta='$idp'
                                             AND p.avalia IN ('FG','FGM','AVALIACAO','PERC','SUPERCAO')";
                        $eselavalia = $_SESSION['fetch_array']($_SESSION['query']($selavalia)) or die ("erro na query de consulta da quantidade de perguntas");
                        $dados[] = $eselavalia['result'];
                        //$html .= "<td align=\"center\">".$eselavalia['result']."</td>";
                        foreach($respostas[$idp] as $idr => $nr) {
                            $selavalia = "SELECT COUNT(*) as result FROM moniavalia ma
                                                  INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                                  WHERE idmonitoria='".$lmonitoria['idmonitoria']."' AND ma.idresposta='$idr'
                                                  AND p.avalia IN ('FG','FGM','AVALIACAO','PERC','SUPERCAO')";
                            $eselavalia = $_SESSION['fetch_array']($_SESSION['query']($selavalia)) or die ("erro na query de consulta da quantidade de respostas");
                            $dados[] = $eselavalia['result'];
                            //$html .= "<td align=\"center\">".$eselavalia['result']."</td>";
                        }
                    }
                }

                $avaliafora = array('FG','TABULACAO','REDIST_GRUPO','REDIST_AVAL','PROP_AVAL','FGM','SUPERACAO');
                foreach($perguntas as $kgrupos => $idsperg) {
                    $selgurpo = "SELECT fg FROM grupo WHERE idgrupo='$kgrupos' ORDER BY posicao";
                    $eselgrupo = $_SESSION['fetch_array']($_SESSION['query']($selgurpo)) or die (mysql_error());
                    foreach($idsperg as $kperg => $nomep) {
                        $fora = 0;
                        $selpertab = "SELECT avalia,valor_perg FROM pergunta WHERE idpergunta='$kperg'";
                        $eselpergtab = $_SESSION['query']($selpertab) or die (mysql_error());
                        while($row = $_SESSION['fetch_array']($eselpergtab)) {
                            if(in_array($row['avalia'],$avaliafora) OR $row['valor_perg'] == "100") {
                                $fora++;
                            }
                        }
                        if($fora == 0) {
                            $novovalor = "";
                            $cfg = 0;
                            $acres = 0;
                            $valorfim = 0;
                            $seldif = "SELECT p.avalia,valor_final_perg,ma.valor_perg,valor_final_aval,ma.valor_resp,p.valor_resp as vr FROM moniavalia ma
                                            INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                            WHERE idmonitoria='".$lmonitoria['idmonitoria']."' AND ma.idpergunta='$kperg'";
                            $eseldif = $_SESSION['query']($seldif) or die ("erro na consulta da diferença de valores entre a pergunta e o avaliado");
                            while($lseldif = $_SESSION['fetch_array']($eseldif)) {
                                $checkfg = "select (count(*) - sum(if(avalia='FG',1,0))) as result from pergunta where idpergunta='$kperg'";
                                $echeckfg = $_SESSION['fetch_array']($_SESSION['query']($checkfg)) or die ("erro na query de verificação se a pergunta só tem respostas FG");
                                if($lseldif['avalia'] == "FG" OR $lseldif['avalia'] == "FGM" OR $eselgrupo['fg'] == "S") {
                                    $cfg++;
                                }
                                else {
                                    if($lseldif['avalia'] == "AVALIACAO_OK") {
                                        $valorfim = str_replace(".",",",$lseldif['valor_final_aval']);
                                    }
                                    else {
                                        $acres = $lseldif['valor_perg'] - $lseldif['valor_final_perg'];
                                        $valorfim = str_replace(".",",",$lseldif['valor_final_aval']);
                                    }
                                }
                            }
                            if($cfg == 0 && $acres > 0 && $valorfim != "") {
                                $novovalor = $valorfim + str_replace(".",",",$acres);
                                $dados[$kperg] = $novovalor;
                                //$html .= "<td align=\"center\">$novovalor</td>";
                            }
                            if($cfg == 0 && $acres == 0 && $valorfim != "") {
                                $dados[$kperg] = $valorfim;
                            }
                        }
                    }
                }
                $dados[] = $lmonitoria['checkfg'];
                foreach($perguntastab as $gruptab) {
                    foreach($gruptab as $idpergtab => $descritab) {
                        $resp = array();
                        $obs = "";
                        $descriresp = "SELECT descriresposta,obs FROM moniavalia ma
                                       INNER JOIN pergunta p ON p.idresposta = ma.idresposta
                                       WHERE idmonitoria='".$lmonitoria['idmonitoria']."' AND ma.idpergunta='$idpergtab'";
                        $edescriperg = $_SESSION['query']($descriresp) or die (mysql_error());
                        while($ldescriperg = $_SESSION['fetch_array']($edescriperg)) {
                            $resp[] = $ldescriperg['descriresposta'];
                            $obs = $ldescriperg['obs'];
                        }
                        $dados[] = implode("#",$resp);
                        $dados[] = $obs;
                    }
                }
                $linha = fwrite($criarq,  utf8_decode(implode(";",$dados))."\r\n");
                usleep(1000);
              //$html .= "</tr>";
            }
            unset($lmonitoria);
        }
        if(count($planilhas) > $cri) {
            $msg = "BASES GERADAS, PORÉM FORAM SOLICITADO?/S ".count($planilhas)." ARQUIVO/S DE PLANILHA E FORAM SALVOS ".$cria." ARQUIVO/S. FAVOR VERIFICAR!!!";
        }
        else {
            $msg = "BASE GERADA COM SUCESSO!!!";
        }
        header("location:/monitoria_supervisao/admin/admin.php?menu=exportacao&retorno=1&msg=$msg");
    ?>
    <!--<script type="text/javascript">
        alert('Base criada e salva com sucesso!!!');
        window.location = '/monitoria_supervisao/admin/admin.php?menu=exportacao';
    </script>-->
    <?php
    //$html .= "</table>";
    //header ("Last-Modified: ".date('d/m/Y H:i:s')."");
    //header ("Cache-Control: no-cache, must-revalidate");
    //header ("Content-type: application/x-msexcel");
    //header ("Content-Disposition: attachment; filename=\"base\"");
    //header ("Pragma: no-cache");
    //header ("Expires: 0");
    //$html = utf8_encode($html);
    //echo $html;
}

if(isset($_POST['apagar'])) {
    $exclui = unlink($rais."/monitoria_supervisao/baseexport/".$_SESSION['nomecli']."/".$arq);
    if($exclui) {
        ?>
        <script type="text/javascript">
            alert('Arquivo apagado com sucesso!!!');
            window.location = '/monitoria_supervisao/admin/admin.php?menu=exportacao';
        </script>
        <?php
    }
    else {
        ?>
        <script type="text/javascript">
            alert('Erro no processo para apagar o arquivo!!!');
            window.location = '/monitoria_supervisao/admin/admin.php?menu=exportacao';
        </script>
        <?php
    }
}
?>
