<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
        $('#tabperg').tablesorter();
        $('.perc').hide();
        $('#modcalc').change(function() {
            var tipo = $(this).val();
            if(tipo == "PERC") {
                $('.perc').show();
            }
            else {
                $('.perc').hide();            
            }
        })
        
        $("#cadastra").click(function() {
            var perg = $("#descri").val();
            var comple = $("#comple").val();
            if(perg =="" || comple == "") {
               alert("Os campos Pergunta e Descrição precisam estar preenchidos");
               return false;
            }
            else {
            }
        })
    })
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadperg.php" accept-charset="utf-8" method="post" name="cadperg">
<table width="793" border="0">
    <tr>
        <td colspan="2" class="corfd_ntab" align="center"><strong>CADASTRO DE PERGUNTAS</strong></td>
    </tr>
    <tr>
        <td width="84" class="corfd_coltexto"><strong>Pergunta</strong></td>
        <td width="702" class="corfd_colcampos"><input maxlength="1000" style="width:700px; border: 1px solid #69C" name="descri" id="descri" type="text" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Descrição</strong></td>
        <td class="corfd_colcampos"><textarea style="border: 1px solid #69C; width:700px" maxlength="1000" name="comple" id="comple" rows="3"></textarea></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Valor</strong></td>
        <td class="corfd_colcampos" style="color:#F00"><input style=" text-align:center; width:50px; border: 1px solid #69C" maxlength="6" name="valor" type="text" /> <strong>Somente números, com 2 casas decimais</strong></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Resposta</strong></td>
        <td class="corfd_colcampos">
            <select name="tipo_resp">
        	<?php
                $t_resp = array('U' => 'UNICA','M' => 'MULTIPLA');
                foreach($t_resp as $ktipo => $tipo_resp) {
                        echo "<option value=\"".$ktipo."\">".$tipo_resp."</option>";
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Tipo</strong></td>
        <td class="corfd_colcampos">
            <select name="modcalc" id="modcalc">
                <?php
                $tipos = array("ABS","PERC");
                foreach($tipos as $tipo) {
                    echo "<option value=\"$tipo\">$tipo</option>";
                }
                ?>
            </select>
        </td>
    </tr>
    <tr class="perc">
        <td class="corfd_coltexto"><strong>Percentual Perda</strong></td>
        <td class="corfd_colcampos" style="color: #F00"><input name="percperda" id="percperda" style="width:40px; border: 1px solid #69C; text-align: center" /> <strong>Percentual em decimal</strong></td>
    </tr>
    <tr class="perc">
        <td class="corfd_coltexto"><strong>Máx. Repetições</strong></td>
        <td class="corfd_colcampos" style="color: #F00"><input name="maxrep" id="maxrep" style="width:40px; border: 1px solid #69C; text-align: center" /> <strong>Somente números</strong></td>
    </tr>
    <tr>
        <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastra" type="submit" value="Cadastrar" /></td>
    </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
<hr /><br />
<form action="" method="get">
<table width="490" border="0">
<tr>
<td class="corfd_ntab" colspan="2" align="center"><strong>PESQUISA DE PERGUNTAS</strong></td>
</tr>
  <tr>
    <td width="81" class="corfd_coltexto"><strong>ID.</strong></td>
    <td width="402" class="corfd_colcampos"><input name="menu" value="perguntas" type="hidden" /><input style="text-align:center; width:70px; border: 1px solid #69C" name="id" type="text" value="<?php $pesq = "SELECT * FROM pergunta ORDER BY id DESC LIMIT 1";
    echo $_GET['id'];
    ?>" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Pergunta</strong></td>
    <td class="corfd_colcampos"><input style="width:400px; border: 1px solid #69C" maxlength="200" name="perg" type="text" value="<?php echo $_GET['perg']; ?>" /></td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesquisa" type="submit" value="Pesquisar" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msgp'] ?></strong></font><br />
<div style="width:1024px; height: 250px; overflow:auto">
<font color="#FF0000"><strong><?php echo $_GET['msgi']; ?></strong></font>
<table width="940" id="tabperg">
    <thead>
	<th width="74" align="center" class="corfd_coltexto"><strong>ID</strong></th>
	<th width="671" align="center" class="corfd_coltexto"><strong>PERGUNTAS</strong></th>
	<th width="62" align="center" class="corfd_coltexto"><strong>VALOR</strong></th>
	<th width="55" align="center" class="corfd_coltexto"><strong>REDISTRIBUI</strong></th>
        <th width="55" align="center" class="corfd_coltexto"><strong>ATV</strong></th>
    </thead>
<?php
echo "<tbody>";
  $id = mysql_real_escape_string($_GET['id']);
  $perg = mysql_real_escape_string($_REQUEST['perg']);
  $caracter = mysql_real_escape_string($_REQUEST['perg']);
if($id == "" && $perg == "") {
  $sel = "SELECT DISTINCT idpergunta FROM pergunta ORDER BY idpergunta DESC";
}
if($id != "" && $perg == "") {
  $sel = "SELECT DISTINCT idpergunta FROM pergunta WHERE idpergunta='$id' ORDER BY idpergunta DESC";
}
if($id == "" && $perg != "") {
  $sel = "SELECT DISTINCT idpergunta FROM pergunta WHERE UPPER(descripergunta) LIKE '$caracter%' ORDER BY idpergunta DESC";
}
if($id != "" && $perg != "") {
  $sel = "SELECT DISTINCT idpergunta FROM WHERE idpergunta='$id' AND UPPER(descripergunta) LIKE '$caracter%'  pergunta ORDER BY idpergunta DESC";
}
$sqldis = "$sel";
$exedis = $_SESSION['query']($sqldis) or die (mysql_error());
while($ldis = $_SESSION['fetch_array']($exedis)) {
        $redist = "SELECT COUNT(*) as result FROM pergunta WHERE idpergunta='".$ldis['idpergunta']."' AND (avalia='REDIST_GRUPO' OR avalia='REDIST_AVAL' OR avalia='PROP_AVAL')";
        $eredist = $_SESSION['fetch_array']($_SESSION['query']($redist)) or die ("erro na query de consulta se a pergunta tem alguma resposta que redistribui valor");
        $sqlcons = "SELECT * FROM pergunta WHERE idpergunta='".$ldis['idpergunta']."' LIMIT 1";
        $execons = $_SESSION['query']($sqlcons) or die (mysql_error());
        while($linha = $_SESSION['fetch_array']($execons)) {
        echo "<tr>";
                echo "<td width=\"74\" bgcolor=\"#FFFFFF\" align=\"center\">".$linha['idpergunta']."</td>";
                echo "<td width=\"650\" bgcolor=\"#FFFFFF\" align=\"center\"><a style=\"text-decoration:none; color:#000;\" href=\"admin.php?menu=pergdet&id=".$linha['idpergunta']."\">".$linha['descripergunta']."</a></td>";
                echo "<td width=\"62\" bgcolor=\"#FFFFFF\" align=\"center\">".$linha['valor_perg']."</td>";
                if($eredist['result'] >= 1) {
                        $redist = "S";
                }
                else {
                        $redist = "N";
                }
                echo "<td width=\"55\" bgcolor=\"#FFFFFF\" align=\"center\">".$redist."</td>";
                echo "<td width=\"55\" bgcolor=\"#FFFFFF\" align=\"center\">".$linha['ativo']."</td>";
        echo "</tr>";
        }
}
echo "</tbody>";
?>
</table>
</div>
</div>
</body>
</html>