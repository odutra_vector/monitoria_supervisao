<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

$nome = strtoupper(trim($_POST['nome']));
$qtde = trim($_POST['qtde']);
$idrel = $_POST['idrel'];
$idplanilha = $_POST['plan'];
$idavalia = $_POST['aval'];
$idgrupo = $_POST['grupo'];
$idsubgrupo = $_POST['sub'];
$idpergunta = $_POST['perg'];
$idresposta = $_POST['resp'];

if(isset($_POST['cadastrarel'])) {
    $verif = "SELECT COUNT(*) as result FROM rel_resp_especifica WHERE nome='$nome'";
    $everirf = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ("erro na query de consulta do nome");
    if($everif['result'] >= 1) {
        $msg = "O nome informado já está cadastrado na base";
        header("location:admin.php?menu=relespe&msg=$msg");
        break;
    }
    else {
        if(is_numeric($qtde)) {
            $insert = "INSERT INTO rel_resp_especifica (nome,qtde) VALUES ('$nome','$qtde')";
            $einsert = $_SESSION['query']($insert) or die ("erro na query de inserção do nome");
            if($einsert) {
                $msg = "Relatório criado com sucesso";
                header("location:admin.php?menu=relespe&msg=$msg");
                break;
            }
            else {
                $msg = "Erro para criar o relatório, favor contatar o administrador";
                header("location:admin.php?menu=relespe&msg=$msg");
                break;
            }
        }
        else {
            $msg = "O campo quantidade só pode conter numeros";
            header("location:admin.php?menu=relespe&msg=$msg");
            break;
        }
    }
}

if(isset($_POST['carregarel'])) {
    $idrel = $_POST['idrelespe'];
    header("location:admin.php?menu=relespe&idrel=$idrel");
    break;
}

if(isset($_POST['apagarel'])) {
    $idrel = $_POST['idrelespe'];
    $del = "DELETE FROM rel_resp_especifica WHERE idrel_resp_especifica='$idrel'";
    $edel = $_SESSION['query']($del) or die ("erro na query para apagar o relatório");
    $alter = "ALTER TABLE rel_resp_especifica AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query de auto incremento");
    $alter = "ALTER TABLE resp_especificas AUTO_INCREMENT=1";
    $ealter = $_SESSION['query']($alter) or die ("erro na query de auto incremento");
    if($edel) {
        $msg = "Relatório apagado com sucesso";
        header("location:admin.php?menu=relespe&msgd=$msg");
        break;
    }
    else {
        $msg = "Erro no processo para apagar o relatório, favor contatar o administrador";
        header("location:admin.php?menu=relespe&msgd=$msg");
        break;
    }
}

if(isset($_POST['novorel'])) {
    header("location:admin.php?menu=relespe");
    break;
}

if(isset($_POST['cadastraresp'])) {
    $idrel = $_POST['idrelespe'];
    $selresp = "SELECT COUNT(*) as result FROM resp_especificas WHERE idplanilha='$idplanilha' AND idresposta='$idresposta' AND idrel_resp_especifica='$idrel'";
    $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na query de consulta das respostas relacionadas");
    if($eselresp['result'] >= 1) {
        $msg = "A planilha e resposta já estão relacionados com este relatório";
        header("location:admin.php?menu=relespe&idrel=$idrel&msgre=$msg");
        break;
    }
    else {
        $insert = "INSERT INTO resp_especificas (idplanilha,idresposta,idrel_resp_especifica) VALUES ('$idplanilha','$idresposta','$idrel')";
        $einsert = $_SESSION['query']($insert) or die ("erro na query de inserção da resposta");
        if($einsert) {
            $msg = "Resposta vinculada com sucesso";
            header("location:admin.php?menu=relespe&idrel=$idrel&msgre=$msg");
            break;
        }
        else {
            $msg = "Erro no relacionamento da pergunta, favor contatar o administrador";
            header("location:admin.php?menu=relespe&idrel=$idrel&msgre=$msg");
            break;
        }
    }
}

if(isset ($_POST['relaciona'])) {
    $idrelespe = $_POST['idrelespe'];
    $idsrel =$_POST['idrel_filtros'];
    $idsrel = trim(implode(",",$idsrel));
    if($idsrel == "") {
        $idsrel = "NULL";
    }
    else {
        $idsrel = "'$idsrel'";
    }
    $verif = "SELECT COUNT(*) as result FROM rel_resp_especifica WHERE idrel_filtros=$idsrel AND idrel_resp_especifica='$idrelespe'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ("erro na query de verificação dos dados");
    if($everif['result'] >= 1) {
        $msg = "Nenhum dado foi alterado, processo não executado";
        header("location:admin.php?menu=relespe&idrel=$idrelespe&msgf=$msg");
        break;
    }
    else {
        $up = "UPDATE rel_resp_especifica SET idrel_filtros=$idsrel WHERE idrel_resp_especifica='$idrelespe'";
        $eup = $_SESSION['query']($up) or die (mysql_error());
        if($eup) {
            $msg = "Alteração efetuada com sucesso";
            header("location:admin.php?menu=relespe&idrel=$idrelespe&msgf=$msg");
            break;
        }
        else {
            $msg = "Ocorreu um erro no processo de alteração, favor contatar o administrador";
            header("location:admin.php?menu=relespe&idrel=$idrelespe&msgf=$msg");
            break;
        }
    }
}

if(isset($_POST['alterarel'])) {
    $selrel = "SELECT * FROM rel_resp_especifica WHERE idrel_resp_especifica='$idrel'";
    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
    
    if($eselrel['nome'] == $nome && $qtde == $eselrel['qtde']) {
        $msg = "Alteração não executada, pois o nome não foi alterado";
        header("location:admin.php?menu=relespe&msg=$msg&idrel=$idrel");
        break;
    }
    else {
        if(is_numeric($qtde)) {
            $verif = "SELECT COUNT(*) as result FROM rel_resp_especifica WHERE nome='$nome' AND idrel_resp_especifica<>'$idrel'";
            $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die (mysql_error());
            if($everif['result'] >= 1) {
                $msg = "Alteração não executada, pois o nome informado já está cadastrado em outro relatório";
                header("location:admin.php?menu=relespe&msg=$msg&idrel=$idrel");
                break;
            }
            else {
                $alter = "UPDATE rel_resp_especifica SET nome='$nome',qtde='$qtde' WHERE idrel_resp_especifica='$idrel'";
                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                if($ealter) {
                    $msg = "Alteração efetuada com sucesso!!!";
                    header("location:admin.php?menu=relespe&msg=$msg&idrel=$idrel");
                    break;
                }
                else {
                    $msg = "Erro para alterar o relatório, favor contatar o administrador";
                    header("location:admin.php?menu=relespe&msg=$msg&idrel=$idrel");
                    break;
                }
            }
        }
        else {
            $msg = "O campo quantidade só pode conter números!!!";
            header("location:admin.php?menu=relespe&msg=$msg&idrel=$idrel");
            break;
        }
    }
}
?>
