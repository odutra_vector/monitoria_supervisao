<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem tÃ­tulo</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
            $('#fluxo').tablesorter();
            $("select[name=fluxo]").change(function() {
                   $("select[name=status]").html('<option value="0">Carregando...</option>');
                   $("select[name=status]").load('carregastatus.php',{fluxo:$(this).val(),tipo:'status'});
                   $("select[name=atustatus]").load('carregastatus.php',{fluxo:$(this).val(),tipo:'status'});
                   $("select[name=definicao]").html('<option value="0">Carregando...</option>');
                   $("select[name=definicao]").load('carregastatus.php',{fluxo:$(this).val(),tipo:'defini'});
            })
        })
</script>
</head>
<body>
<div id="tudo">
<div id="conteudo" class="corfd_pag">
<form action="cadrelfluxo.php" method="post">
<table width="753" border="0">
  <tr>
    <td align="center" colspan="4" class="corfd_ntab"><strong>RELACIONAMENTO DO FLUXO</strong></td>
  </tr>
  <tr>
    <td width="141" class="corfd_coltexto"><strong>FLUXO</strong></td>
    <td width="263" class="corfd_colcampos"><select style="width:250px; border: 1px solid #666" name="fluxo"><option value="0" disabled="disabled" selected="selected">Selecione um Fluxo</option>
    <?php
	$fluxo = "SELECT * FROM fluxo WHERE atvfluxo='S'";
	$efluxo = $_SESSION['query']($fluxo) or die (mysql_error());
	while($lfluxo = $_SESSION['fetch_array']($efluxo)) {
	    echo "<option value=\"".$lfluxo['idfluxo']."\">".$lfluxo['nomefluxo']."</option>";
	}
	?>
    </select>
    </td>
    <td width="131" class="corfd_coltexto"><strong>VIS. ADM</strong></td>
    <td width="200" class="corfd_colcampos"><select style="width:200px; border: 1px solid #666" size="5" name="visadm[]" multiple="multiple">
    <?php
	$padm = "SELECT * FROM perfil_adm";
	$epadm = $_SESSION['query']($padm) or die (mysql_error());
	while($lepadm = $_SESSION['fetch_array']($epadm)) {
		echo "<option value=\"".$lepadm['idperfil_adm']."\">".$lepadm['nomeperfil_adm']."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>ESTATUS</strong></td>
    <td class="corfd_colcampos"><select style="width:250px; border: 1px solid #666" name="status">
    </select>
    </td>
    <td class="corfd_coltexto"><strong>VIS. WEB</strong></td>
    <td class="corfd_colcampos"><select style="width:200px; border: 1px solid #666" size="3" name="visweb[]" multiple="multiple">
    <?php
    $pweb = "SELECT * FROM perfil_web";
	$epweb = $_SESSION['query']($pweb) or die (mysql_error());
	while($lpweb = $_SESSION['fetch_array']($epweb)) {
		echo "<option value=\"".$lpweb['idperfil_web']."\">".$lpweb['nomeperfil_web']."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>DEFINIÇÃO</strong></td>
    <td class="corfd_colcampos"><select style="width:250px; border: 1px solid #666" name="definicao">
    </select></td>
    <td class="corfd_coltexto"><strong>RESP. ADM</strong></td>
    <td class="corfd_colcampos"><select style="width:200px; border: 1px solid #666" size="5" name="respadm[]" multiple="multiple">
    <?php
	$padm = "SELECT * FROM perfil_adm";
	$epadm = $_SESSION['query']($padm) or die (mysql_error());
	while($lepadm = $_SESSION['fetch_array']($epadm)) {
		echo "<option value=\"".$lepadm['idperfil_adm']."\">".$lepadm['nomeperfil_adm']."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>PRÓX. Status</strong></td>
    <td class="corfd_colcampos"><select style="width:250px; border: 1px solid #666" name="atustatus">
    </select></td>
    <td class="corfd_coltexto"><strong>RESP. WEB</strong></td>
    <td class="corfd_colcampos"><select style="width:200px; border: 1px solid #666" size="5" name="respweb[]" multiple="multiple">
    <?php
    $pweb = "SELECT * FROM perfil_web";
	$epweb = $_SESSION['query']($pweb) or die (mysql_error());
	while($lpweb = $_SESSION['fetch_array']($epweb)) {
		echo "<option value=\"".$lpweb['idperfil_web']."\">".$lpweb['nomeperfil_web']."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>TIPO</strong></td>
    <td class="corfd_colcampos"><select style="width:250px; border: 1px solid #666" name="tipo">
    <?php
    $tipos = array('inicia','contesta','editar','cancela','finaliza','senhaoper','sequencia');
    foreach($tipos as $tipo) {
	echo "<option value=\"".$tipo."\">".$tipo."</option>";
    }
    ?>
    </select>
    </td>
    <td class="corfd_coltexto"><strong>CLASSIFICAÇÃO</strong></td>
    <td class="corfd_colcampos">
    	<select name="classifica" id="classifica" style="width:150px; border: 1px solid #666">
        <option value="">Nenhuma</option>
        <?php
		$classifica = array('replica','treplica','erro','feedback');
		foreach($classifica as $class) {
			echo "<option value=\"".$class."\">".$class."</option>";
		}
		?>
        </select>
    </td>
  </tr>
  <tr>  
    <td class="corfd_coltexto"><strong>ATIVO REL.</strong></td>
    <td colspan="3" class="corfd_colcampos"><select name="ativo">
    <?php
	$atv = array('S','N');
	foreach($atv as $ativo) {
		echo "<option value=\"".$ativo."\">".$ativo."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadrel" type="submit" value="Cadastrar" /></td>
  </tr>
</table>
<font color="#FF0000"><strong><?php echo $_GET['msgi']; ?></strong></font></form>
<hr />
<form action="" method="get">
<table width="201" border="0">
  <tr>
    <td colspan="2" class="corfd_ntab"><strong>PESQUISA DE FLUXO</strong></td>
  </tr>
  <tr>
    <td width="50" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="141" class="corfd_colcampos"><select name="id">;
    <?php
	$selcount = "SELECT COUNT(*) as result FROM fluxo";
	$eselcount = $_SESSION['fetch_array']($_SESSION['query']($selcount)) or die ("erro na query de contagem dos fluxos");
	if($eselcount['result'] >= 1) {
		echo "<option value=\"\">TODOS</option>";
		$selfluxo = "SELECT * FROM fluxo";
		$eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta dos fluxos");
		while($lfluxo = $_SESSION['fetch_array']($eselfluxo)) {
			echo "<option value=\"".$lfluxo['idfluxo']."\"";
			if($lfluxo['idfluxo'] == $_GET['id']) {
				echo "selected=\"selected\"";
			}
			else {
			}
			echo ">".$lfluxo['nomefluxo']."</option>";
		}
	}
	else {
		echo "<option value=\"\">NÃ£o existe fluxo cadastrado</option>";
	}
	?>
    </select><input type="hidden" name="menu" value="fluxo" /></td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesquisa" type="submit" value="Pesquisar" /></td>
  </tr>
</table>
</form><br />
<div style="width:1024px; height:300px; overflow:auto">
<table width="1317" border="0" id="fluxo">
  <thead>
    <th width="38" align="center" class="corfd_coltexto"><strong>ID</strong></th>
    <th width="153" class="corfd_coltexto" align="center"><strong>Fluxo</strong></th>
    <th width="175" class="corfd_coltexto" align="center"><strong>Status</strong></th>
    <th width="39" class="corfd_coltexto" align="center"><strong>Fase</strong></th>
    <th width="209" class="corfd_coltexto" align="center"><strong>Defini</strong></th>
    <th width="139" class="corfd_coltexto" align="center"><strong>PrÃ³x. Status</strong></th>
    <th width="61" class="corfd_coltexto" align="center"><strong>Tipo</strong></th>
    <th width="92" class="corfd_coltexto" align="center"><strong>Vis. ADM</strong></th>
    <th width="93" class="corfd_coltexto" align="center"><strong>Vis. WEB</strong></th>
    <th width="82" class="corfd_coltexto" align="center"><strong>Resp. ADM</strong></th>
    <th width="82" class="corfd_coltexto" align="center"><strong>Resp. WEB</strong></th>
    <th width="39" class="corfd_coltexto" align="center"><strong>ATV</strong></th>
    <th width="61"></th>
  </thead>
  <tbody>
  <?php
  $id = $_GET['id'];
if($_GET['id'] == "") {
  $sel = "SELECT * FROM rel_fluxo ORDER BY idfluxo, idrel_fluxo";
}
if($_GET['id'] != "") {
  $sel = "SELECT * FROM rel_fluxo WHERE idfluxo='$id' ORDER BY idfluxo, idrel_fluxo";
}
  $fluxo = "$sel";
  $efluxo = $_SESSION['query']($fluxo) or die (mysql_error());
  while($lfluxo = $_SESSION['fetch_array']($efluxo)) {
      $nfluxo = "SELECT nomefluxo FROM fluxo WHERE idfluxo='".$lfluxo['idfluxo']."'";
      $enfluxo = $_SESSION['fetch_array']($_SESSION['query']($nfluxo)) or die ("erro na execusÃ£o da query");
      $nstatus = "SELECT nomestatus, fase FROM status WHERE idstatus='".$lfluxo['idstatus']."'";
      $enstatus = $_SESSION['fetch_array']($_SESSION['query']($nstatus)) or die (mysql_error());
      $ndefini = "SELECT nomedefinicao FROM definicao WHERE iddefinicao='".$lfluxo['iddefinicao']."'";
      $endefini = $_SESSION['fetch_array']($_SESSION['query']($ndefini)) or die (mysql_error());
      $natustatus = "SELECT nomestatus FROM status WHERE idstatus='".$lfluxo['idatustatus']."'";
      $enatustatus = $_SESSION['fetch_array']($_SESSION['query']($natustatus)) or die (mysql_error());
      echo "<tr>";
            echo "<form action=\"cadrelfluxo.php\" method=\"post\">";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\"><input type=\"hidden\" value=\"".$lfluxo['idfluxo']."\" name=\"idfluxo\" /><input style=\"width:30px; border: 1px solid #FFF; text-align:center; font-size:10px\" readonly=\"readonly\" name=\"id\" type=\"text\" value=\"".$lfluxo['idrel_fluxo']."\" /></td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$enfluxo['nomefluxo']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$enstatus['nomestatus']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$enstatus['fase']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$endefini['nomedefinicao']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$enatustatus['nomestatus']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lfluxo['tipo']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\"><a href=\"admin.php?menu=peradm&jaberta=sim\" target=\"blank\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center; font-size:10px\" readonly=\"readonly\" name=\"visadm\" type=\"text\" value=\"".$lfluxo['vis_adm']."\" /></a></td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lfluxo['vis_web']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lfluxo['resp_adm']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lfluxo['resp_web']."</td>";
            echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lfluxo['ativo']."</td>";
            echo "<td><input style=\"border:1px solid #333; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"altera\" type=\"submit\" value=\"alterar\" /></td>";
      echo "</form></tr>";
  }
  ?>
  </tbody>
</table>
</div>
<font color="#FF0000"><strong><?php echo $_GET['msgf']; ?></strong></font>
</div>
</div>
</body>
</html>
