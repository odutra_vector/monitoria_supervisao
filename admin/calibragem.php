<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
if(isset($_GET['id'])) {
    $id = $_GET['id'];
    $selconf = "SELECT * FROM conf_calib WHERE idconf_calib='$id'";
    $eselconf = $_SESSION['fetch_array']($_SESSION['query']($selconf)) or die ("erro na query de consulta da configuraÃ§Ã£o cadastra");
    $camposdb = explode(",",$eselconf['camposag']);
}
else {
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem Título</title>
<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function() {
    <?php
    if(isset($_GET['id'])) {
        echo "$('tr.id').show();\n";
        if($eselconf['bloq'] == "S") {
            echo "$('tr.prazo').show();\n";
        }
        else {
            echo "$('tr.prazo').hide();\n";
        }
    }
    else {
        echo "$('tr.id').hide();\n";
        echo "$('tr.prazo').hide();\n";
    }
    ?>
    $("#bloq").change(function() {
        var bloq = $(this).val();
        if(bloq == "N") {
            $('tr.prazo').hide();
        }
        if(bloq == "S") {
            $('tr.prazo').show();
        }
    })
    $("#cadastra").click(function() {
        var campos = $("#camposag").val();
        var usermoni = $("#usermoni").val();
        var userweb = $("#userweb").val();
        var useradm = $("#useradm").val();
        if(campos == null) {
            alert("É necessário informar quais coluna serão apresentadas na pesquisa de monitorias para calibragem");
            return false;
        }
        else {
            if(usermoni == null && userweb == null && useradm == null) {
                alert("E necessário informar pelo menos 1 usuário que irá participar da calibragem");
                return false;
            }
            else {
            }
        }
    })
    $("#alteracad").click(function() {
        var campos = $("#camposag").val();
        var usermoni = $("#usermoni").val();
        var userweb = $("#userweb").val();
        var useradm = $("#useradm").val();
        if(campos == null) {
            alert("É necessário informar quais coluna serão apresentadas na pesquisa de monitorias para calibragem");
            return false;
        }
        else {
            if(usermoni == null && userweb == null && useradm == null) {
                alert("E necessário informar pelo menos 1 usuário que irá participar da calibragem");
                return false;
            }
            else {
            }
        }
    })
})
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadconfcalib.php" method="post">
<table width="915">
  <tr>
    <td colspan="4" class="corfd_ntab" align="center"><strong>PARAMETROS CALIBRAGEM</strong></td>
  </tr>
    <tr class="id">
        <td class="corfd_coltexto"><strong>ID</strong></td>
        <td class="corfd_colcampos" colspan="3"><input style="width: 50px; border: 1px solid #69C; text-align:center" readonly="readonly" type="text" name="id" value="<?php
        if(isset($_GET['id'])) {
            echo $eselconf['idconf_calib'];
        }
        else {
        }
        ?>" /></td>
    </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Intercâmbio</strong></td>
    <td colspan="3" class="corfd_colcampos"><select name="intercambio">
        <?php
        $inter = array('N','S');
        foreach($inter as $i) {
            if($i == $eselconf['intercambio']) {
                $select = "selected=\"selected\"";
            }
            else {
                $select = "";
            }
            ?>
            <option value="<?php echo $i;?>" <?php echo $select;?>><?php echo $i;?></option>
            <?php
        }
        ?>
        </td>
  </tr>
  <tr>
    <td width="145" class="corfd_coltexto"><strong>Configurações E-mail</strong></td>
    <td class="corfd_colcampos" colspan="3"><select name="confemail">
    <?php
    if(isset($_GET['id'])) {
        echo "<option value=\"".$eselconf['idconfemailenv']."\" selected=\"selected\">".$eselconf['idconfemailenv']."</option>";
	$selconf = "SELECT idconfemailenv FROM confemailenv WHERE tipo='CALIB'";
	$econf = $_SESSION['query']($selconf) or die ("erro na query de consulta das configuraÃ§Ãµes de e-mail cadastradas");
	while($lconf = $_SESSION['fetch_array']($econf)) {
            if($lconf['idconfemailenv'] == $eselconf['idconfemailenv']) {
            }
            else {
		echo "<option value=\"".$lconf['idconfemailenv']."\">".$lconf['idconfemailenv']."</option>";
            }
	}
    }
    else {
        echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\" >Selecione...</option>";
        $selconf = "SELECT idconfemailenv FROM confemailenv WHERE tipo='CALIB'";
	$econf = $_SESSION['query']($selconf) or die ("erro na query de consulta das configuraÃ§Ãµes de e-mail cadastradas");
	while($lconf = $_SESSION['fetch_array']($econf)) {
            echo "<option value=\"".$lconf['idconfemailenv']."\">".$lconf['idconfemailenv']."</option>";
	}
    }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Bloqueia Usuário</strong></td>
    <td class="corfd_colcampos" colspan="3"><select name="bloq" id="bloq">
    <?php
    $bloqops = array('N','S');
        if(isset($_GET['id'])) {
            echo "<option value=\"".$eselconf['bloq']."\" selected=\"selected\">".$eselconf['bloq']."</option>";
            foreach($bloqops as $bloqop) {
                if($bloqop == $eselconf['bloq']) {
                }
                else {
                    echo "<option value=\"".$bloqop."\">".$bloqop."</option>";
                }
            }
        }
        else {
            echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\">Selecione...</option>";
            foreach($bloqops as $bloqop) {
                echo "<option value=\"".$bloqop."\">".$bloqop."</option>";
            }
        }
    ?>
    </select></td>
  </tr>
    <tr class="prazo">
        <td class="corfd_coltexto"><strong>Prazo</strong></td>
        <td class="corfd_colcampos" colspan="3"><input style="width:70px; border: 1px solid #69C; text-align:center" type="text" name="prazo" id="prazo"
        <?php
        if(isset($_GET['id'])) {
            echo "value=\"".$eselconf['tempobloq']."\"";
        }
        else {
        }
        ?>/><font color="#FF0000"><strong> O prazo após vencer o perído para avaliação deve ser informado em hs, somente numeros</strong></font></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Campos Agenda</strong></td>
        <td class="corfd_colcampos" colspan="6">
            <select name="camposag[]" id="camposag" multiple="multiple" style="height: 200px">
                <?php
                $tabelas = new TabelasSql;
                $camposmoni = array($tabelas->AliasTab(monitoria).".idmonitoria" => "idmonitoria",$tabelas->AliasTab(monitoria).".data" => "data",
                                    $tabelas->AliasTab(monitor).".nomemonitor" => "nomemonitor",$tabelas->AliasTab(monitoria).".idrel_filtros" => "relacionamento");
                foreach($camposmoni as $aliascampo => $campo) {
                    if(in_array($aliascampo,$camposdb)) {
                        echo "<option value=\"$aliascampo\" selected=\"selected\">$campo</option>";
                    }
                    else {
                        echo "<option value=\"$aliascampo\">$campo</option>";
                    }
                }
                $aliases = array('OPERADOR' => $tabelas->AliasTab(operador),'SUPER_OPER' => $tabelas->AliasTab(super_oper),'DADOS' => $tabelas->AliasTab(fila_grava));
                $selcampos = "SELECT * FROM coluna_oper";
                $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query para levantar os campos");
                while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
                  if(eregi("id",$lselcampos['nomecoluna'])) {
                  }  
                  else {
                      $option = $aliases[$lselcampos['incorpora']].".".$lselcampos['nomecoluna'];
                      if(in_array($option,$camposdb)) {
                        echo "<option value=\"$option\" selected=\"selected\">".$lselcampos['nomecoluna']."</option>";  
                      }
                      else {
                        echo "<option value=\"$option\">".$lselcampos['nomecoluna']."</option>";
                      }
                  }
                }
                ?>
            </select> 
        </td>
    </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Acesso Estatistica</strong></td>
    <td width="250" class="corfd_colcampos"><select name="usermoni[]" id="usermoni" multiple="multiple" style="width:250px; height:150px; border: 1px solid #69C">
    <optgroup  label="MONITOR">
    <?php
    if(isset($_GET['id'])) {
        $usersmoni = explode(",",$eselconf['idusers_acesso']);
        $selmoni = "SELECT idmonitor, nomemonitor FROM monitor WHERE ativo='S' ORDER BY nomemonitor";
        $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
        while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
            $monitor = $lselmoni['idmonitor']."-monitor";
                if(in_array($monitor, $usersmoni)) {
                    echo "<option value=\"".$lselmoni['idmonitor']."-monitor\" selected=\"selected\">".$lselmoni['nomemonitor']."</option>";
                }
                else {
                    echo "<option value=\"".$lselmoni['idmonitor']."-monitor\">".$lselmoni['nomemonitor']."</option>";
                }
        }
    }
    else {
	$selmoni = "SELECT idmonitor, nomemonitor FROM monitor WHERE ativo='S'";
	$eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
	while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
		echo "<option value=\"".$lselmoni['idmonitor']."-monitor\">".$lselmoni['nomemonitor']."</option>";
        }
    }
    ?>
    </optgroup>
    </select></td>
    <td width="250" class="corfd_colcampos"><select name="userweb[]" id="userweb" multiple="multiple" style="width:250px; height:150px; border: 1px solid #69C">
    <optgroup  label="USER WEB">
    <?php
    if(isset($_GET['id'])) {
        $selmoni = "SELECT iduser_web, nomeuser_web FROM user_web WHERE ativo='S' ORDER BY nomeuser_web";
        $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
        while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                if(in_array($lselmoni['iduser_web']."-user_web", $usersmoni)) {
                    echo "<option value=\"".$lselmoni['iduser_web']."-user_web\" selected=\"selected\">".$lselmoni['nomeuser_web']."</option>";
                }
                else {
                    echo "<option value=\"".$lselmoni['iduser_web']."-user_web\">".$lselmoni['nomeuser_web']."</option>";
                }
        }
    }
    else {
	$selmoni = "SELECT iduser_web, nomeuser_web FROM user_web WHERE ativo='S'";
	$eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
	while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
		echo "<option value=\"".$lselmoni['iduser_web']."-user_web\">".$lselmoni['nomeuser_web']."</option>";
        }
    }
    ?>
    </optgroup></select></td>
    <td width="250" class="corfd_colcampos"><select name="useradm[]" id="useradm" multiple="multiple" style="width:250px; height:150px; border: 1px solid #69C">
    <optgroup  label="USER ADM">
    <?php
    if(isset($_GET['id'])) {
        $selmoni = "SELECT iduser_adm, nomeuser_adm FROM user_adm WHERE ativo='S' ORDER BY nomeuser_adm";
        $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
        while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                if(in_array($lselmoni['iduser_adm']."-user_adm", $usersmoni)) {
                    echo "<option value=\"".$lselmoni['iduser_adm']."-user_adm\" selected=\"selected\">".$lselmoni['nomeuser_adm']."</option>";
                }
                else {
                    echo "<option value=\"".$lselmoni['iduser_adm']."-user_adm\">".$lselmoni['nomeuser_adm']."</option>";
                }
        }
    }
    else {
	$selmoni = "SELECT iduser_adm, nomeuser_adm FROM user_adm WHERE ativo='S'";
	$eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
	while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
		echo "<option value=\"".$lselmoni['iduser_adm']."-user_adm\">".$lselmoni['nomeuser_adm']."</option>";
        }
    }
    ?>
    </optgroup></select></td>
  </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Ativo</strong></td>
        <td>
            <select name="ativo">
            <?php
            $opativo = array('S','N');
            if(isset($_GET['id'])) {
                echo "<option value=\"".$eselconf['ativo']."\" selected=\"selected\">".$eselconf['ativo']."</option>";
                foreach($opativo as $atv) {
                    if($atv == $eselconf['ativo']) {
                    }
                    else {
                        echo "<option value=\"".$atv."\">".$atv."</option>";
                    }
                }
            }
            else {
                foreach($opativo as $atv) {
                    echo "<option value=\"".$atv."\">".$atv."</option>";
                }
            }
            ?>

            </select>
        </td>
    </tr>
  <tr>
      <td colspan="4">
    <?php
    if(isset($_GET['id'])) {
        echo "<input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px\" name=\"alteracad\" id=\"alteracad\" type=\"submit\" value=\"Alterar\" /> <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px\" name=\"novo\" type=\"submit\" value=\"Novo\" />";
    }
    else {
    echo "<input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg); width:100px\" name=\"cadastra\" id=\"cadastra\" type=\"submit\" value=\"Cadastrar\" />";
    }
    ?>
    </td>
  </tr>
</table>
<font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font>
</form>
<hr />
<table width="800">
  <tr>
    <td class="corfd_ntab" colspan="7" align="center"><strong>PARAMETROS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="48" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="42" align="center" class="corfd_coltexto"><strong>Bloq</strong></td>
    <td width="58" align="center" class="corfd_coltexto"><strong>Prazo HS</strong></td>
    <td width="250" align="center" class="corfd_coltexto"><strong>Campos AG</strong></td>
    <td width="350" align="center" class="corfd_coltexto"><strong>Users Acesso</strong></td>
    <td width="41" align="center" class="corfd_coltexto"><strong>Conf. E-mail</strong></td>
    <td width="41" align="center" class="corfd_coltexto"><strong>Ativo</strong></td>
    <td width="220"></td>
  </tr>
  <?php
  $selconf = "SELECT * FROM conf_calib";
  $eselconf = $_SESSION['query']($selconf) or die ("erro na query de consulta das configuraÃ§Ãµes cadastradas");
  while($lselconf = $_SESSION['fetch_array']($eselconf)) {
        $listacampos = explode(",",$lselconf['camposag']);
        $listacampos = implode("<br/>",$listacampos);
	echo "<form action=\"cadconfcalib.php\" method=\"post\">";
  	echo "<tr>";
	echo "<td width=\"48\" bgcolor=\"#FFFFFF\"><input style=\"width:45px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"text\" name=\"id\" value=\"".$lselconf['idconf_calib']."\" /></td>";
	echo "<td width=\"42\" bgcolor=\"#FFFFFF\"><input style=\"width:40px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"text\" name=\"bloq\" value=\"".$lselconf['bloq']."\" /></td>";
	echo "<td width=\"88\" bgcolor=\"#FFFFFF\"><input style=\"width:55px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"text\" name=\"prazo\" value=\"".$lselconf['tempobloq']."\" /></td>";
        echo "<td width=\"250\" bgcolor=\"#FFFFFF\">".$listacampos."</td>";
	echo "<td width=\"350\"><select style=\"width:350px; height: 200px; border: 1px solid #FFF\" multiple=\"multiple\" name=\"users\">";
	$users = explode(",", $lselconf['idusers_acesso']);
	$userscount = count($users);
	foreach($users as $chave => $user) {
		$part = explode("-",$user);
		$tabuser = $part[1];
		if($chave == 0) {
			$grupo = $part[1];
		}
		else {
			$grupo = explode("-",$users[$chave-1]);
			$grupo = $grupo[1];
		}
		$iduser = $part[0];
		if($chave == 0) {
			echo "<optgroup label=\"".strtoupper($tabuser)."\">";
		}
		else {
			if($tabuser == $grupo) {
			}
			else {
				echo "<optgroup label=\"".strtoupper($tabuser)."\">";
			}
		}
		$seluser = "SELECT id$tabuser, nome$tabuser FROM $tabuser WHERE id$tabuser='$iduser'";
		$eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser)) or die ("erro na query de consulta do usuÃ¡rio relacionado");
		echo "<option value=\"".$eseluser['id'.$tabuser]."\">".$eseluser['nome'.$tabuser]."</option>";
                $gruposeg = $users[$chave+1];
                $gruposeg = explode("-",$gruposeg);
                $gruposeg = $gruposeg[1];
                if($tabuser == $gruposeg) {

                }
                else {
			echo "</optgroup>";
		}
	}
	echo "</select></td>";
	echo "<td width=\"41\" bgcolor=\"#FFFFFF\"><input style=\"width:38px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"text\" name=\"confemail\" value=\"".$lselconf['idconfemailenv']."\" /></td>";
	echo "<td width=\"41\" bgcolor=\"#FFFFFF\"><input style=\"width:38px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"text\" name=\"ativo\" value=\"".$lselconf['ativo']."\" /></td>";
	echo "<td width=\"350\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg); width:80px; text-align:center\" name=\"altera\" type=\"submit\" value=\"Alterar\" /></td>";
	echo "</tr>";
	echo "</form>";
  }
  ?>
</table>
</div>
</body>
</html>