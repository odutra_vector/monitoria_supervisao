<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");

$cor = new CoresSistema();

protegePagina();

$idperiodo = $_GET['periodo'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
    <script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../styleadmin.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        #tudo {
            width:1200px;
            overflow: auto;
            margin:auto;
            font-family:Verdana, Geneva, sans-serif;
            font-size:11px;
        }
        #rodape {
            width:1146px;
            height:60px;
            text-align:center;
            background-color:#666;
            clear:both;
        }
    </style>
    <script type="text/javascript">
    	$(document).ready(function() {
            var periodo = '<?php echo $_GET['periodo'];?>';
            $('.dimenfiltros').hide();
            $('.tragrupa').hide();
            $('#tipocons').change(function() {
                var tipocons = $(this).val();
                $('.dimenfiltros').hide();
                $('.dimenfiltrostab').hide();
                $('.dimenmoni').hide();
                $('.dimenrel').hide();
                if(tipocons == "todos") {
                    $('.dimenmoni').show();
                    $('.dimenrel').show();
                    $('.tragrupa').hide();
                }
                if(tipocons == "monitor") {
                    $('.dimenmoni').show();
                    $('.tragrupa').hide();
                }
                if(tipocons == "relacionamento") {
                    $('.dimenrel').show();
                    $('.tragrupa').hide();
                }
                if(tipocons == "filtro") {
                    $('.tragrupa').show();
                }
            });

            $('#agrupa').change(function() {
                var agrupa = $(this).val();
                if(agrupa != "") {
                    $('.dimenfiltros').show();
                    $('.dimenfiltrostab').show();
                    $('.dimenfiltrostab').load('dimenfiltros.php?idperiodo=<?php echo $idperiodo;?>&agrupa='+agrupa);
                }
                else {
                }
            })
        });
    </script>
    <?php
    $cor->Cores();
    ?>
    <title>Untitled Document</title>
    </head>
    <body>
    <div id="tudo" class="corfd_pag">
      <div id="tudo2" class="corfd_pag">
        <table width="278" border="0">
          <tr>
            <td width="112" class="corfd_coltexto"><strong>VISUALIZAR POR:</strong></td>
            <td width="156" class="corfd_colcampos"><select name="tipocons" id="tipocons">
              <option value="todos">TODOS</option>
              <option value="monitor">MONITOR</option>
              <option value="relacionamento">RELACIONAMENTO</option>
              <option value="filtro">FILTRO</option>
            </select></td>
          </tr>
          <tr class="tragrupa">
            <td class="corfd_coltexto"><strong>AGRUPA POR:</strong></td>
            <td class="corfd_colcampos">
                <select name="agrupa" id="agrupa">
                    <option value="">SELECIONE...</option>
                    <?php
                    $filtros = filtros();
                    foreach($filtros as $f) {
                        echo "<option value=\"".strtoupper($f)."\">".strtoupper($f)."</option>";
                    }
                    ?>
                </select>
            </td>
          </tr>
        </table>
        <br />
        <br />
        <hr />
        <table width="1200" class="dimenmoni">
            <tr class="corfd_ntab" align="center"><td><strong>DIMENSIONAMENTO MONITOR</strong></td></tr>
        </table>
        <table width="1024" class="dimenmoni" style="font-size:10px">
            <thead>
              <tr>
                <th width="221" class="corfd_coltexto" title="MONITOR"><strong>MONITOR</strong></th>
                <th width="394" align="center" class="corfd_coltexto" title="RELACIONAMENTO"><strong>RELACIONAMENTO</strong></th>
                <th width="133" align="center" class="corfd_coltexto" title="Meta MES"><strong>Meta MES</strong></th>
                <th width="126" align="center" class="corfd_coltexto" title="Meta SEMANA"><strong>Meta SEMANA</strong></th>
                <th width="126" align="center" class="corfd_coltexto" title="Meta DIA"><strong>Meta DIA</strong></th>
              </tr>
            </thead>
            <tbody>
          <?php
              $selmoni = "SELECT * FROM monitor WHERE ativo='S' ORDER BY nomemonitor";
              $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
              while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                  $moni = 0;
                  $cdimen = "SELECT COUNT(*) as result FROM dimen_moni dmo
                               INNER JOIN monitor m ON m.idmonitor = dmo.idmonitor
                               INNER JOIN dimen_mod dm ON dm.iddimen_mod = dmo.iddimen_mod
                               WHERE dmo.idmonitor='".$lselmoni['idmonitor']."' AND dm.idperiodo='$idperiodo' AND dmo.ativo='S'";
                  $ecdimen = $_SESSION['fetch_array']($_SESSION['query']($cdimen)) or die ("erro na query de consulta do dimensionamento do monitor");
                  $seldimen = "SELECT m.nomemonitor, dm.idrel_filtros, dmo.meta_m, dmo.meta_s, dmo.meta_d FROM dimen_moni dmo
                               INNER JOIN monitor m ON m.idmonitor = dmo.idmonitor
                               INNER JOIN dimen_mod dm ON dm.iddimen_mod = dmo.iddimen_mod
                               WHERE dmo.idmonitor='".$lselmoni['idmonitor']."' AND dm.idperiodo='$idperiodo' AND dmo.ativo='S'";
                  $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento do monitor");
                  while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {


              ?>
                    <tr>
                        <?php
                        if($moni == 0) {
                    	?>
                        <td class="corfd_colcampos" rowspan="<?php echo $ecdimen['result'];?>"><?php echo $lseldimen['nomemonitor'];?></td>
                        <?php
                        }
                        else {
                        }
                        ?>
                        <td class="corfd_colcampos" align="center"><?php echo nomeapres($lseldimen['idrel_filtros']);?></td>
                        <td class="corfd_colcampos" align="center"><?php echo $lseldimen['meta_m'];?></td>
                        <td class="corfd_colcampos" align="center"><?php echo $lseldimen['meta_s'];?></td>
                        <td class="corfd_colcampos" align="center"><?php echo $lseldimen['meta_d'];?></td>
                    </tr>
            <?php
                    $ttmes = $ttmes + $lseldimen['meta_m'];
                    $ttsemana = $ttsemana + $lseldimen['meta_s'];
                    $ttdia = $ttdia + $lseldimen['meta_d'];
                    $moni++;
                  }
              }
              ?>
                <tr>
                    <td class="corfd_colcampos" colspan="2"><strong>TOTAL</strong></td>
                    <td class="corfd_colcampos" align="center"><?php echo $ttmes;?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $ttsemana;?></td>
                    <td class="corfd_colcampos" align="center"><?php echo $ttdia;?></td>
                </tr>
            </tbody>
        </table>
        <br /><br />
        <table width="1200" class="dimenrel">
            <tr class="corfd_ntab" align="center"><td><strong>DIMENSIONAMENTO RELACIONAMENTO</strong></td></tr>
        </table>
        <table width="1200" class="dimenrel" style="font-size:10px">
            <thead>
              <tr>
                <td width="250" class="corfd_coltexto" title="Relacionamento"><strong>RELCIONAMENTO</strong></td>
                <td width="81" align="center" class="corfd_coltexto"><strong>Qtde. Gravação</strong></td>
                <td width="81" align="center" class="corfd_coltexto"><strong>Qtde. Operadores</strong></td>
                <td width="72" align="center" class="corfd_coltexto" title="Multiplicador"><strong>Multiplicador</strong></td>
                <td width="61" align="center" class="corfd_coltexto" title="Estimativa"><strong>Estimativa</strong></td>
                <td width="57" align="center" class="corfd_coltexto" title="Meta Dia"><strong>Meta Dia</strong></td>
                <td width="50" align="center" class="corfd_coltexto" title="Tempo Médio de Atendimento"><strong>TMA</strong></td>
                <td width="50" align="center" class="corfd_coltexto" title="Tempo Total de Atendimento"><strong>TTA</strong></td>
                <td width="54" align="center" class="corfd_coltexto" title="Tempo de Médio Monitoria"><strong>TMM</strong></td>
                <td width="50" align="center" class="corfd_coltexto" title="Tempo Total de Monitoria"><strong>TTM</strong></td>
                <td width="62" align="center" class="corfd_coltexto" title="Quantidade de PA por dia"><strong>Qtde.PA. DIA</strong></td>
                <td width="56" align="center" class="corfd_coltexto" title="Média de Monitores por Relacionamento"><strong>Média Monitores/Relacional</strong></td>
                <td width="54" align="center" class="corfd_coltexto" title="Horas por Monitor"><strong>Hs. Monitor</strong></td>
                <td width="57" align="center" class="corfd_coltexto" title="Meta Monitor"><strong>Meta Monitor</strong></td>
                <td width="47" align="center" class="corfd_coltexto" title="Percentual de Participação na PA"><strong>Perc. PA %</strong></td>
                <td width="51" align="center" class="corfd_coltexto" title="Percentual de Participação na Meta"><strong>Perc. Meta %</strong></td>
                <td width="87" align="center" class="corfd_coltexto" title="Data do Cadastro"><strong>Data Cad.</strong></td>
              </tr>
            </thead>
            <tbody>
          <?php
            $seldimen = "SELECT dm.idrel_filtros, pm.tipomoni, dm.qnt_oper, dm.qnt_grava,dm.multiplicador,dm.estimativa,dm.tma,dm.tta,dm.tm,dm.ttm,dm.qnt_pad,
                         dm.qnt_monitores,dm.hs_monitor,dm.meta_monitor,(dm.estimativa / p.diasprod) as metadia, dm.percpartpa,dm.percpartmeta,dm.datacad FROM dimen_mod dm
                         INNER JOIN periodo p ON p.idperiodo = dm.idperiodo
                         INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                         INNER JOIN conf_rel cr ON cr.idrel_filtros = dm.idrel_filtros
                         INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                         WHERE dm.idperiodo='$idperiodo'";
            $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query para consultar o dimensionamento");
            while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
              $tipomoni = $lseldimen['tipomoni'];
              ?>
              <tr>
                <td class="corfd_colcampos"><?php echo nomeapres($lseldimen['idrel_filtros']);?>
                  <?php
                    if($tipomoni == "G" && $lseldimen['qnt_oper'] != 0) {
                        echo "<img src=\"/monitoria_supervisao/images/interrogacao_16x16.png\" width=\"16\" height=\"16\" title=\"A configuração do relacionamento estÃƒÂ¡ conflitante com o Dimenionamento, favor verificar!!!\" />";
                    }
                    if($tipomoni == "O" && $lseldimen['qnt_grava'] != 0) {
                        echo "<img src=\"/monitoria_supervisao/images/interrogacao_16x16.png\" width=\"16\" height=\"16\" title=\"A configuração do relacionamento estÃƒÂ¡ conflitante com o Dimenionamento, favor verificar!!!\" />";
                    }
                    ?></td>
                <td class="corfd_colcampos" align="center"><?php if($tipomoni == "G") { echo $lseldimen['qnt_grava']; } else { echo "--";}?></td>
                <td class="corfd_colcampos" align="center"><?php if($tipomoni == "O") { echo $lseldimen['qnt_oper']; } else { echo "--";}?></td>
                <td class="corfd_colcampos" align="center"><?php if($tipomoni == "O") { echo $lseldimen['multiplicador']; } if($tipomoni == "G") { echo "--"; }?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['estimativa'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo number_format($lseldimen['metadia'],0);?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['tma'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['tta'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['tm'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['ttm'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['qnt_pad'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['qnt_monitores'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['hs_monitor'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['meta_monitor'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['percpartpa'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['percpartmeta'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo banco2data($lseldimen['datacad']);?></td>
              </tr>
              <?php
            }
            $calcdimen = "SELECT dm.idrel_filtros, pm.tipomoni, SUM(dm.qnt_oper) as qnt_oper, SUM(dm.qnt_grava) as qnt_grava,AVG(dm.multiplicador) as multiplicador,
                         SUM(dm.estimativa) as estimativa,(SUM(dm.estimativa) / p.diasprod) as metadia,SUM(TIME_TO_SEC(dm.tma)) as tma,SUM(TIME_TO_SEC(dm.tta)) as tta,
                         SUM(TIME_TO_SEC(dm.tm)) as tm,SUM(TIME_TO_SEC(dm.ttm)) as ttm,SUM(dm.qnt_pad) as qnt_pad,
                         AVG(dm.qnt_monitores) as qnt_monitores,SEC_TO_TIME(SUM(TIME_TO_SEC(dm.hs_monitor))) as hs_monitor,SUM(dm.meta_monitor) meta_monitor,SUM(dm.percpartpa) as percttpa,SUM(dm.percpartmeta) as percttmeta,dm.datacad FROM dimen_mod dm
                         INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                         INNER JOIN periodo p ON p.idperiodo = dm.idperiodo
                         INNER JOIN conf_rel cr ON cr.idrel_filtros = dm.idrel_filtros
                         INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                         WHERE dm.idperiodo='$idperiodo' GROUP BY dm.idperiodo";
            $ecalcdimen = $_SESSION['fetch_array']($_SESSION['query']($calcdimen)) or die ("erro na query para consultar o dimensionamento");
            ?>
              <tr>
                  <td class="corfd_colcampos"><strong>TOTAL</strong></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['qnt_grava'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['qnt_oper'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo round($ecalcdimen['multiplicador'],1);?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['estimativa'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['metadia'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo sec_hora($ecalcdimen['tma']);?></td>
                  <td class="corfd_colcampos" align="center"><?php echo sec_hora($ecalcdimen['tta']);?></td>
                  <td class="corfd_colcampos" align="center"><?php echo sec_hora($ecalcdimen['tm']);?></td>
                  <td class="corfd_colcampos" align="center"><?php echo sec_hora($ecalcdimen['ttm']);?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['qnt_pad'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo round($ecalcdimen['qnt_monitores'],2);?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['hs_monitor'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['meta_monitor'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['percttpa'];?></td>
                  <td class="corfd_colcampos" align="center"><?php echo $ecalcdimen['percttmeta'];?></td>
                  <td class="corfd_colcampos" align="center"></td>
              </tr>
            </tbody>
        </table>
        <br /><br />
        <table width="1024" class="dimenfiltros">
            <tr class="corfd_ntab" align="center"><td><strong>DIMENSIONAMENTO</strong></td></tr>
        </table>
        <table width="1146" class="dimenfiltrostab" style="font-size:10px">         
        </table>
        <br />
        <div id="rodape">
          <table align="center" width="344" border="0">
            <tr>
              <td width="224" align="center"><img src="/monitoria_supervisao/images/logo/Logo_Flor_vector_75X35.png" width="75" height="38" /></td>
            </tr>
            <tr>
              <td height="15" align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
            </tr>
          </table>
        </div>
      </div>
    </div>
    </body>
    </html>
