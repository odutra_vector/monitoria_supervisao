<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

if(isset($_POST['relaciona'])) {
    $idplan = $_POST['idplan'];
    $idsgrupo = $_POST['idgrupo'];
    $idssoma = $_POST['idgrupo'];
    $pos = $_POST['posicao'];
    if($_POST['tab'] == "SIM") {
	$tab = "S";
    }
    if($_POST['tab'] == "NAO") {
	$tab = "N";
    }
    $relaval = $_POST['relaval'];
    if($relaval == "") {
        $vaval = "";
    }
    if($relaval != "") {
        $valoraval = "SELECT valor FROM rel_aval WHERE idplanilha='$idplan' AND idaval_plan='$relaval'";
        $evaloraval = $_SESSION['fetch_array']($_SESSION['query']($valoraval)) or die (mysql_error());
        $vaval = $evaloraval['valor'];
    }
    if($idsgrupo == "") {
	$msg = "É necessário selecionar um grupo para cadastrar um relacionamento!!!";
	header("location:admin.php?menu=relplan&msg=".$msg."&id=".$idplan);
    }
    else {
        if($relaval == "") {
            $msg = "É necessário cadastrar a avaliação relacionada a planilha";
            header("location:admin.php?menu=relplan&msg=".$msg."&id=".$idplan);
        }
        else {
            $vplan = "SELECT * FROM planilha WHERE idplanilha='$idplan'";
            $evplan = $_SESSION['fetch_array']($_SESSION['query']($vplan)) or die (mysql_error());
            $soma = "SELECT DISTINCT planilha.idgrupo, grupo.valor_grupo, filtro_vinc FROM planilha
                       INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                       WHERE planilha.idplanilha='$idplan' AND planilha.idaval_plan='$relaval' AND grupo.fg='N'";
            $esoma = $_SESSION['query']($soma) or die (mysql_error());
            while($lesoma = $_SESSION['fetch_array']($esoma)) {
                 if($lesoma['filtro_vinc'] == "P") {
                      $verifsup = "SELECT count(*) as result FROM grupo g
                                   INNER JOIN pergunta p ON p.idpergunta = g.idrel
                                   WHERE idgrupo='".$lesoma['idgrupo']."' AND p.avalia='SUPÈRACAO'";
                      $everifsup = $_SESSION['fetch_array']($_SESSION['query']($verifsup)) or die(mysql_error());
                 }
                 else {
                      $verifsup = "SELECT COUNT(*) as result FROM grupo g
                                       INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                                       INNER JOIN pergunta p ON p.idpergunta = s.idpergunta
                                       WHERE avalia='SUPERACAO' AND g.idgrupo='".$lesoma['idgrupo']."'";
                      $everifsup = $_SESSION['fetch_array']($_SESSION['query']($verifsup)) or die(mysql_error());
                 }
                 if($everifsup['result'] == 0) {
                  $somagrup = $lesoma['valor_grupo'] + $somagrup;
                 }
            }
            while(list(,$idgrupsoma) = each($idssoma)) {
                  $vgrupcad = "SELECT * FROM grupo
                              WHERE idgrupo='$idgrupsoma' AND fg='N'";
                  $evgrup = $_SESSION['query']($vgrupcad) or die ("erro na query de consulta do grupo");
                  $ngrup = $_SESSION['num_rows']($evgrup);
                  if($ngrup == 0) {
                      $evgrupcad = "";
                  }
                  else {
                      $evgrupcad = $_SESSION['fetch_array']($evgrup) or die (mysql_error());
                      if($evgrupcad['filtro_vinc'] == "P") {
                          $verifcond = "SELECT COUNT(*) as result FROM grupo g
                                       INNER JOIN pergunta p ON p.idpergunta = g.idrel
                                       WHERE avalia='SUPERACAO' AND g.idgrupo='$idgrupsoma'";
                          $everif = $_SESSION['fetch_array']($_SESSION['query']($verifcond)) or die ("erro na query de consulta das respostas");
                      }
                      if($evgrupcad['filtro_vinc'] == "S") {
                          $verifcond = "SELECT COUNT(*) as result FROM grupo g
                                       INNER JOIN subgrupo s ON s.idsubgrupo = g.idrel
                                       INNER JOIN pergunta p ON p.idpergunta = s.idpergunta
                                       WHERE avalia='SUPERACAO' AND g.idgrupo='$idgrupsoma'";
                          $everif = $_SESSION['fetch_array']($_SESSION['query']($verifcond)) or die ("erro na query de consulta das respostas");
                      }
                  }
                  if($everif['result'] >= 1) {
                  }
                  else {
                      $v_grup = $evgrupcad['valor_grupo'] + $v_grup;
                  }
            }
            $somagrup = $v_grup + $somagrup;
            if($vaval == "") {
                foreach($idsgrupo as $idgrupo) {
                    $checkrel = "SELECT count(*) as r FROM planilha WHERE idplanilha='$idplan' and idgrupo='$idgrupo'";
                    $echeckrel = $_SESSION['fetch_array']($_SESSION['query']($checkrel)) or die (mysql_error());
                    if($echeckrel['r'] == 0) {
                        $selgrup = "SELECT COUNT(*) as result FROM planilha WHERE idplanilha='$idplan' AND idgrupo='000000'";
                        $eselgrup = $_SESSION['fetch_array']($_SESSION['query']($selgrup)) or die (mysql_error());
                        $ngrupo = $_SESSION['num_rows']($eselgrup);
                        if($eselgrup['result'] == 1) {
                            $cadrel = "UPDATE planilha SET idgrupo='$idgrupo', idaval_plan='$relaval', posicao='$pos', tab='$tab', tabplanilha='".$evplan['tabplanilha']."',ativoweb='".$evplan['ativoweb']."',ativomoni='".$evplan['ativomoni']."' WHERE idplanilha='$idplan'";
                            $ecadrel = $_SESSION['query']($cadrel) or die (mysql_error());
                            $acao = "update";
                        }
                        else {
                            $dados = "SELECT * FROM planilha WHERE idplanilha='$idplan' LIMIT 1";
                            $edados = $_SESSION['fetch_array']($_SESSION['query']($dados)) or die (mysql_error());
                            $cadrel = "INSERT INTO planilha (idplanilha, descriplanilha, compleplanilha, tabplanilha, ativoweb, ativomoni, idgrupo, idaval_plan, posicao, tab) VALUE ('$idplan', '".$edados['descriplanilha']."', '".$edados['compleplanilha']."',
                            '".$edados['tabplanilha']."', '".$edados['ativoweb']."', '".$edados['ativomoni']."', '$idgrupo', '$relaval', '$pos', '$tab')";
                            $ecadrel = $_SESSION['query']($cadrel) or die (mysql_error());
                            $acao = "insert";
                        }
                    }
                }
                if($ecadrel) {
                    $msg = "Relacionamento efetuado com sucesso!!!";
                    header("location:admin.php?menu=relplan&msg=".$msg."&id=".$idplan);
                }
                else {
                    $msg = "Ocorreu algum erro no processo de relacionamento, favor contatar o administrador!!!";
                    header("location:admin.php?menu=relplan&msg=".$msg."&id=".$idplan);
                }   
            }
            else {
                if($somagrup > $vaval) {
                     $msg = "O valor da soma do relacionamento dos ''Grupos'' não pode ser maior que o valor da ''Avaliação''";
                     header("location:admin.php?menu=relplan&id=".$idplan."&msg=".$msg);
                }
                if($somagrup <= $vaval) {
                    foreach($idsgrupo as $idgrupo) {
                        $checkrel = "SELECT count(*) as r FROM planilha WHERE idplanilha='$idplan' and idgrupo='$idgrupo'";
                        $echeckrel = $_SESSION['fetch_array']($_SESSION['query']($checkrel)) or die (mysql_error());
                        if($echeckrel['r'] == 0) {
                            $selgrup = "SELECT COUNT(*) as result FROM planilha WHERE idplanilha='$idplan' AND idgrupo='000000'";
                            $eselgrup = $_SESSION['fetch_array']($_SESSION['query']($selgrup)) or die (mysql_error());
                            $ngrupo = $_SESSION['num_rows']($eselgrup);
                            if($eselgrup['result'] == 1) {
                                $cadrel = "UPDATE planilha SET idgrupo='$idgrupo', idaval_plan='$relaval', posicao='$pos', tab='$tab', tabplanilha='".$evplan['tabplanilha']."',ativoweb='".$evplan['ativoweb']."',ativomoni='".$evplan['ativomoni']."' WHERE idplanilha='$idplan'";
                                $ecadrel = $_SESSION['query']($cadrel) or die (mysql_error());
                            }
                            else {
                                $dados = "SELECT * FROM planilha WHERE idplanilha='$idplan' LIMIT 1";
                                $edados = $_SESSION['fetch_array']($_SESSION['query']($dados)) or die (mysql_error());
                                $cadrel = "INSERT INTO planilha (idplanilha, descriplanilha, compleplanilha, tabplanilha, ativoweb, ativomoni, idgrupo, idaval_plan, posicao, tab) VALUE ('$idplan', '".$edados['descriplanilha']."', '".$edados['compleplanilha']."',
                                '".$edados['tabplanilha']."', '".$edados['ativoweb']."', '".$edados['ativomoni']."', '$idgrupo', '$relaval', '$pos', '$tab')";
                                $ecadrel = $_SESSION['query']($cadrel) or die (mysql_error());
                            }
                        }
                    }
                    if($ecadrel) {
                        $msg = "Relacionamento efetuado com sucesso!!!";
                        header("location:admin.php?menu=relplan&msg=".$msg."&id=".$idplan);
                    }
                    else {
                        $msg = "Ocorreu algum erro no processo de relacionamento, favor contatar o administrador!!!";
                        header("location:admin.php?menu=relplan&msg=".$msg."&id=".$idplan);
                    } 
                }
            }
        }
    }
}

if(isset($_POST['finaliza'])) {
    $idplan = $_POST['idplan'];
    $msgi = "Processo de relacionamento finalizado!!!";
    header("location:admin.php?menu=planilha&msgi=".$msgi."&id=".$idplan);
}

if(isset($_POST['desrel'])) {
    $idplan = $_POST['idplan'];
    $idgrupo = $_POST['idgrupo'];
    $verif = "SELECT COUNT(*) as result FROM planilha WHERE idplanilha='$idplan'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or dir (mysql_error());
    $verifmoni = "SELECT COUNT(*) as result FROM moniavalia WHERE idplanilha='$idplan' AND idgrupo='$idgrupo'";
    $everifmoni = $_SESSION['fetch_array']($_SESSION['query']($verifmoni)) or die (mysql_error());
    if($everifmoni['result'] >= 1) {
        $msgi = "O Relacionamento nÃ£o pode ser apagado pois existem monitorias relacionadas a este paramentro!!!";
        header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
    }
    else {
        if($everif['result'] == 1) {
            $del = "UPDATE planilha SET idgrupo='000000', posicao='0' WHERE idplanilha='$idplan'";
            $edel = $_SESSION['query']($del) or die (mysql_error());
        }
        if($everif['result'] > 1) {
            $del = "DELETE FROM planilha WHERE idplanilha='$idplan' AND idgrupo='$idgrupo'";
            $edel = $_SESSION['query']($del) or die (mysql_error());
        }
        if($edel) {
            $msgi = "Relacionamento apagado com sucesso!!!";
            header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
        }
        else {
            $msgi = "Ocorreu um erro no processo, favor contatar o administrador!!!";
            header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
        }
    }
}

if(isset($_POST['altrel'])) {
    $idplan = $_POST['idplan'];
    $idgrupo = $_POST['idgrupo'];
    $pos = $_POST['posicao'];
    $ativo = $_POST['ativo'];
    $selrel = "SELECT * FROM planilha WHERE idplanilha='$idplan' AND idgrupo='$idgrupo'";
    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta dos dados da planilha");
    if($ativo == "N" && $pos != 0) {
        $msgi = "Quando o GRUPO estiver inativo, a posiÃ§Ã£o sÃ³ pode ser ''0''!!!";
        header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
    }
    else {
        if($ativo == $eselrel['ativo'] && $pos == $eselrel['posicao']) {
          $msgi = "Nenhum dado foi alterado, processo nÃ£o executado!!!";
          header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
        }
        else {
            $update = "UPDATE planilha SET ativo='$ativo', posicao='$pos' WHERE idplanilha='$idplan' AND idgrupo='$idgrupo'";
            $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o dos dados");
            if($eupdate) {
                $msgi = "Alteração realizada com sucesso!!!";
                header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
            }
            else {
                $msgi = "Ocorreu um erro no processo, favor contatar o administrador!!!";
                header("location:admin.php?menu=relplan&msgi=".$msgi."&id=".$idplan);
            }
        }
    }
}
   

?>