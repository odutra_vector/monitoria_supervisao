<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadindice.php" method="post">
<table width="359">
  <tr>
    <td class="corfd_ntab" colspan="4" align="center"><strong>CADASTRO DE INDICES</strong></td>
  </tr>
  <tr>
    <td width="85"  class="corfd_coltexto"><strong>Nome</strong></td>
    <td colspan="3" class="corfd_colcampos"><input style="width:260px; border: 1px solid #69C" maxlength="50" name="nome" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Valor Inicio</strong></td>
    <td width="98" class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" maxlength="6" name="valini" id="valini" type="text" /></td>
    <td width="84" class="corfd_coltexto"><strong>Valor Fim</strong></td>
    <td width="72" class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" maxlength="6" name="valfim" id="valfim" type="text" /></td>
  </tr>
    <tr>
    <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" type="submit" value="Cadastrar" /></td>
  </tr>
</table></form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font><br /><br />
<font color="#FF0000"><strong><?php echo $_GET['msgi'] ?></strong></font>
<table width="662">
  <tr>
    <td class="corfd_ntab" colspan="4" align="center"><strong>INDICES CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="32" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="262" align="center" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="72" align="center" class="corfd_coltexto"><strong>Nota Ini</strong></td>
    <td width="72" align="center" class="corfd_coltexto"><strong>Nota Fim</strong></td>
    <td width="200"></td>
  </tr>
  <?php
  $selindice = "SELECT * FROM indice";
  $eindice = $_SESSION['query']($selindice) or die ("erro na query de consulta da tabela ''indice''");
  while($lindice = $_SESSION['fetch_array']($eindice)) {
  echo "<form action=\"cadindice.php\" method=\"post\">";
  echo "<tr>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:30px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" maxlength=\"2\" name=\"id\" type=\"text\" value=\"".$lindice['idindice']."\" /></td>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:260px; border: 1px solid #FFF; text-align:center\" maxlength=\"50\" name=\"nome\" type=\"text\" value=\"".$lindice['nomeindice']."\" /></td>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center\" maxlength=\"6\" name=\"valini\" type=\"text\" value=\"".$lindice['inicio']."\" /></td>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center\" maxlength=\"6\" name=\"valfim\" type=\"text\" value=\"".$lindice['fim']."\" /></td>";
    echo "<td><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"altera\" type=\"submit\" value=\"Alterar\" />
    <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"Apagar\" /></td>";
  echo "</tr></form>";
  }
  ?>
</table><hr />
<form action="cadindice.php" method="post">
<table width="355">
  <tr>
    <td class="corfd_coltexto" align="center" colspan="4"><strong>FAIXA DE NOTAS</strong></td>
  </tr>
  <tr>
    <td width="79" class="corfd_coltexto"><strong>Indice</strong></td>
    <td colspan="3" class="corfd_colcampos"><select  name="indice">
    <option value="" selected="selected" disabled="disabled">Selecione...</option>
    <?php
    $countind = "SELECT COUNT(*) as result FROM indice";
    $ecount = $_SESSION['fetch_array']($_SESSION['query']($countind)) or die ("erro na query de contagem dos indices");
    if($ecount['result'] == 0) {
      echo "<option value=\"\">Não existe indice cadastrado...</option>";
    }
    else {
      $selind = "SELECT * FROM indice";
      $eind = $_SESSION['query']($selind) or die ("erro na consulta dos indices");
      while($lind = $_SESSION['fetch_array']($eind)) {
	  echo "<option value=\"".$lind['idindice']."\">".$lind['nomeindice']."</option>";
      }
    }
    ?>    
    </select></td>
  </tr>
    <tr>
    <td class="corfd_coltexto"><strong> Nome Faixa</strong></td>
    <td colspan="3" class="corfd_colcampos"><input style="width:260px; border: 1px solid #69C" maxlength="50" name="nome" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Nota Ini</strong></td>
    <td width="94" class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" maxlength="6" name="notaini" id="notafim" type="text" /></td>
    <td width="86" class="corfd_coltexto"><strong>Nota Fim</strong></td>
    <td width="76" class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" maxlength="6" name="notafim" id="notaini" type="text" /></td>
  </tr>
  <tr>
    <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadfaixa" type="submit" value="Cadastrar" /></td>
  </tr>
</table><font color="#FF0000"><strong><?php echo $_GET['msgf'] ?></strong></font>
</form><br /><br />
<font color="#FF0000"><strong><?php echo $_GET['msgfi'] ?></strong></font>
<table width="776">
  <tr>
    <td class="corfd_ntab" align="center" colspan="5"><strong>INTERVALOS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="32" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="262" align="center" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="72" align="center" class="corfd_coltexto"><strong>Nota Ini</strong></td>
    <td width="72" align="center" class="corfd_coltexto"><strong>Nota Fim</strong></td>
    <td width="75" align="center" class="corfd_coltexto"><strong>Indice</strong></td>
    <td width="235"></td>
  </tr>
  <?php
  $selinterval = "SELECT * FROM intervalo_notas ORDER BY idindice, numfim";
  $einterval = $_SESSION['query']($selinterval) or die ("erro na query de consulta dos intervalos");
  while($linterval = $_SESSION['fetch_array']($einterval)) {
  echo "<form action=\"cadindice.php\" method=\"post\">";
  echo "<tr>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:30px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" maxlength=\"2\" name=\"id\" type=\"text\" value=\"".$linterval['idintervalo_notas']."\" /></td>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:260px; border: 1px solid #FFF; text-align:center\" maxlength=\"50\" name=\"nome\" type=\"text\" value=\"".$linterval['nomeintervalo_notas']."\" /></td>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center\" maxlength=\"6\" name=\"notaini\" type=\"text\" value=\"".$linterval['numini']."\" /></td>";
    echo "<td class=\"corfd_colcampos\"><input style=\"width:70px; border: 1px solid #FFF; text-align:center\" maxlength=\"6\" name=\"notafim\" type=\"text\" value=\"".$linterval['numfim']."\" /></td>";
    echo "<td align=\"center\"><select name=\"idindice\">";
    $selnindice = "SELECT nomeindice FROM indice WHERE idindice='".$linterval['idindice']."'";
    $enindice = $_SESSION['fetch_array']($_SESSION['query']($selnindice)) or die ("erro na query de consulta do nome do indice");
    echo "<option value=\"".$linterval['idindice']."\">".$enindice['nomeindice']."</option>";
    $indiceop = "SELECT * FROM indice";
    $eindiceop = $_SESSION['query']($indiceop) or die ("erro na query de consulta dos indices");
    while($lindiceop = $_SESSION['fetch_array']($eindiceop)) {
      if($lindiceop['idindice'] == $linterval['idindice']) {
      }
      else {
	echo "<option value=\"".$lindiceop['idindice']."\">".$lindiceop['nomeindice']."</option>";
      }
    }
  echo "</select></td>";
  echo "<td><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"altfaixa\" type=\"submit\" value=\"Alterar\" />
  <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"apagafaixa\" type=\"submit\" value=\"Apagar\" /></td>";
  echo "</tr></form>";
  }
  ?>
</table>
</div>
</body>
</html>