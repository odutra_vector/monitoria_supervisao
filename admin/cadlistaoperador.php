<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$file = $_FILES['arquivo']['tmp_name'];
$tipo = $_FILES['arquivo']['type'];
$name = $_FILES['arquivo']['name'];
$filtron = $_POST['filtro_nomes'];
$filtrod = $_POST['filtro_dados'];
$dataatu = date("Y-m-d");
$iduser = $_SESSION['usuarioID'];
$wfiltron = "idfiltro_nomes='$filtron'";
$wfiltrod = "idfiltro_dados='$filtrod'";

if($file == "" || $filtron == "" || $filtrod == "") {
    $msg = "Os dados para envio do arquivo não estão preenchidos!!!";
    header("location:admin.php?menu=listaoperador&msg=$msg");
    break;
}
else {
    $dados = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES );
    if($dados == "") {
        $msg = "O arquivo está vazio, favor verificar!!!";
        header("location:admin.php?menu=listaoperador&msg=$msg");
        break;
    }
    if(count($dados) == 1) {
        $msg = "O arquivo só possui 1 linha, que é considerado o nome das colunas!!!";
        header("location:admin.php?menu=listaoperador&msg=$msg");
        break;
    }
    else {
        $selcol = "SHOW COLUMNS FROM listaoperador";
        $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta das colunas da lista de operadores");
        while($lselcol = $_SESSION['fetch_array']($eselcol)) {
            if($lselcol['Field'] == "idlistaoperador" OR $lselcol['Field'] == "idfiltro_nomes" OR $lselcol['Field'] == "idfiltro_dados" OR $lselcol['Field'] == "dataatu" OR $lselcol['Field'] == "iduser_adm") {
            }
            else {
                $cols[] = $lselcol['Field'];
            }
        }
        foreach($dados as $posi => $dado) {
            $dadossel = array();
            if($posi == 0) {
                $colsarq = explode(";",$dado);
            }
            else {
                if(count($colsarq) != count($cols)) {
                    $msg = "As colunas que contam no arquivo são diferentes das colunas do banco de dados";
                    header("location:admin.php?menu=listaoperador&msg=$msg");
                    break;
                }
                else {
                    $valsarq = explode(";",$dado);
                    $data = array_search("datainicio", $cols);
                    $valsarq[$data] = data2banco($valsarq[$data]);
                    foreach($colsarq as $ps => $col) {
                        $dadossel[$cols[$ps]] = $cols[$ps]."='$valsarq[$ps]'";
                    }
                    $seldados = "SELECT idlistaoperador, COUNT(*) as result FROM listaoperador WHERE ".$dadossel['operador']."";
                    $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta dos dados na tabela lista_operador");
                    if($eseldados['result'] >= 1) {
                        $dadossel['idfiltro_nomes'] = "idfiltro_nomes='$filtron'";
                        $dadossel['idfiltro_dados'] = "idfiltro_dados='$filtrod'";
                        $uplista = "UPDATE listaoperador SET ".implode(",",$dadossel).", dataatu='$dataatu',iduser_adm='$iduser' WHERE idlistaoperador='".$eseldados['idlistaoperador']."'";
                        $euplista = $_SESSION['query']($uplista) or die ("erro na query de atulização da lista de operadores");
                    }
                    else {
                        $colsinsert = $cols;
                        $colsinsert[] = "idfiltro_nomes";
                        $colsinsert[] = "idfiltro_dados";
                        $valsarq[] = $filtron;
                        $valsarq[] = $filtrod;
                        $insert = "INSERT INTO listaoperador (".implode(",",$colsinsert).",dataatu,iduser_adm) VALUES ('".implode("','",$valsarq)."','$dataatu','$iduser')";
                        $einsert = $_SESSION['query']($insert) or die ("erro na query de inserção dos dados na tabela lista_operador");
                    }
                }
            }
        }
        if($einsert OR $euplista) {
            $msg = "Dados inseridos com sucesso!!!";
            header("location:admin.php?menu=listaoperador&msg=$msg");
            break;
        }
        else {
            $msg = "Erro na query de inserção dos dados, favor contatar o administrador!!!";
            header("location:admin.php?menu=listaoperador&msg=$msg");
            break;
        }
    }    
}
?>
