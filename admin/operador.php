<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once("functionsadm.php");

$classtab = new TabelasSql();
$periodo = periodo(datas);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.css" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-autocomplete/jquery.autocomplete.js" type="text/javascript"></script>
<link rel="stylesheet" href="/monitoria_supervisao/js/jquery-autocomplete/lib/thickbox.css" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
        $('#taboper').tablesorter();
        $('#hist').tablesorter();
        $('.DATE').mask("99/99/9999");
        $('.TIME').mask("99:99:99");
        $('#cpfpesq').mask('999.999.999-99');
        
        $("#delete").click(function() {
            var id = prompt("INFORME O CÓDIGO DO OPERADOR QUE ASSUIRÁ AS MONITORIAS E REGISTROS DESTE OPERADOR QUE SERÁ DELETADO");
            var iddel = $("#idoperador").val();
            if(id == "") {
                alert("FAVOR INFORMAR UM ID DE OPERADOR PARA REPASSAR OS DADOS!!!");
                return false;
            }
            else {
                $.post("/monitoria_supervisao/admin/cadoperador.php",{delete:'delete',iddel:iddel,id:id},function(ret) {
                    if(ret >= 1) {
                        alert("OPERADOR DELETADO COM SUCESSO E DADOS TRANSFERIDOS COM SUCESSO");
                        window.location = '/monitoria_supervisao/admin/admin.php?menu=operador';
                    }
                    else {
                        if(ret == -1) {
                            alert("O VALOR INFORMADO, NÃO É UM NÚMERO");
                        }
                        else {
                            alert("OPERADOR DELETADO COM SUCESSO, SEM TRANSFERENCIA DE DADOS");
                            window.location = '/monitoria_supervisao/admin/admin.php?menu=operador';
                        }
                    }
                });
            }
        });
        
        $("#gerasenha").click(function() {
            $.blockUI({ message: '<strong>AGUARDE GERANDO SENHAS...</strong>', css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5,
            color: '#fff'
            }});
            <?php 
            $filtros = array('cpfpesq' => 'cpfoper','operadorpesq' => 'operador','supervisorpesq' => 'super_oper','duplicado'=> 'duplicado','tmoni'=> 'tmoni','rel' => 'rel');
            foreach($filtros as $chave => $col) {
                $vars[] = "$chave:$chave";
                ?>
                var <?php echo $chave;?> = $("#<?php echo $chave;?>").val();
                <?php
            }
            ?>
            $.post("cadoperador.php",{gerasenha:'base',<?php echo implode(",",$vars);?>},function(ret) {
                $.unblockUI();
                alert(ret);
                $("#alink").remove();
                $("#delsenhas").remove();
                $("#liarquivo").html('<a id=\"alink\" href=\"/monitoria_supervisao/logs/<?php echo str_replace(" ", "_", $_SESSION['nomecli']);?>/arquivo_senha.csv\" target=\"_blank\" style=\"text-decoration: none\">ARQUIVO DE SENHA</a><input type=\"button\" name=\"delsenhas\" id=\"delsenhas\" value=\"\" style=\"background-color: transparent; background-image: url('+"'"+'/monitoria_supervisao/images/exit.png'+"'"+'); margin-left: 10px; width: 15px; height: 15x; border: 0px" />');
            });
        });
        
        $("#reset").click(function() {
            $.blockUI({ message: '<strong>AGUARDE GERANDO SENHA...</strong>', css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5,
            color: '#fff'
            }});
            <?php
            foreach($filtros as $chave => $col) {
                ?>
                var <?php echo $chave;?> = $("#<?php echo $chave;?>").val();
                <?php
            }
            $vars[] = "idoper:idoper";
            ?>
            var idoper = $("#reset").attr('name');
            $.post("cadoperador.php",{reset:'operador',<?php echo implode(",",$vars);?>},function(ret) {
                $.unblockUI();
                alert(ret);
            });
        });
        
        $("#delsenhas").live('click',function() {
            var confirma = confirm("Você deseja realmente deletar o arquivo? Não será possível recuperar as senhas");
            if(confirma) {
                $.post("cadoperador.php",{delsenha:'delsenha'},function(ret) {
                    alert(ret);
                    $("#alink").remove();
                    $("#delsenhas").remove();
                });
            }
        });
    })
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="" method="post">
    <table style="padding: 5px">
      <tr>
        <td class="corfd_ntab" colspan="2" align="center"><strong>PESQUISAR</strong></td>
      </tr>
      <tr>
        <td width="86" class="corfd_coltexto"><strong>CPF</strong></td>
        <td width="352" class="corfd_colcampos"><input type="text" style="width:150px; border: 1px solid #729fcf; text-align:center" name="cpfpesq" id="cpfpesq" value="<?php if(isset($_POST['cpfpesq'])) { echo $_POST['cpfpesq']; } else { }?>" /></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>NOME</strong></td>
        <td class="corfd_colcampos"><input name="operadorpesq" id="operadorpesq" type="text" style="width:350px; border: 1px solid #729fcf" value="<?php if(isset($_POST['operadorpesq'])) { echo $_POST['operadorpesq']; } else { }?>" /></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>SUPERVISOR</strong></td>
        <td class="corfd_colcampos"><input name="supervisorpesq" id="supervisorpesq" type="text" style="width:350px; border: 1px solid #729fcf" value="<?php if(isset($_POST['supervisorpesq'])) { echo $_POST['supervisorpesq']; } else { }?>" /></td>
      </tr>
      <tr>
          <td class="corfd_coltexto"><strong>OP. DUPLICADO</strong></td>
          <td class="corfd_colcampos">
              <select name="duplicado" id="duplicado">
                  <?php
                  $duplica = array('A' => 'TUDO','N' => 'N','S' => 'S');
                  foreach($duplica as $kdp => $dp) {
                    if($kdp == $_POST['duplicado']) {
                        echo "<option value=\"$kdp\" selected=\"selected\">$dp</option>";
                    }  
                    else {
                        echo "<option value=\"$kdp\">$dp</option>";
                    }
                  }
                  ?>
              </select>
          </td>
      </tr>
      <tr>
          <td class="corfd_coltexto"><strong>TIPO MONITORIA</strong></td>
          <td class="corfd_colcampos">
              <select name="tmoni" id="tmoni">
                  <?php
                  $tmoni = array('G' => 'GRAVAÇÃO','O' => 'ON-LINE');
                  foreach($tmoni as $kt => $t) {
                    if($kt == $_POST['tmoni']) {
                        echo "<option value=\"$kt\" selected=\"selected\">$t</option>";
                    }
                    else {
                        echo "<option value=\"$kt\">$t</option>";
                    }
                  }
                  ?>
              </select>
          </td>
      </tr>
      <tr>
      	<?php
            $selult = "SELECT MIN(nivel), nomefiltro_nomes, idfiltro_nomes FROM filtro_nomes WHERE ativo='S'";
            $eselult = $_SESSION['fetch_array']($_SESSION['query']($selult)) or die ("erro na query de consulta do filtro de menor nível");
            echo "<td class=\"corfd_coltexto\"><strong>".$eselult['nomefiltro_nomes']."</strong></td>";
            echo "<td class=\"corfd_colcampos\"><select name=\"rel\" id=\"rel\">";
            if(isset($_POST['rel'])) {
                if($_POST['rel'] == "") {
                    echo "<option value=\"\" selected=\"selected\">TODOS...</option>";
                }
                else {
                    echo "<option value=\"\">TODOS...</option>";
                }
            }
            else {
                echo "<option value=\"\" selected=\"selected\">TODOS...</option>";
            }
            $selrel = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".$eselult['idfiltro_nomes']."'";
            $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consutla dos relacionamentos ativos");
            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                    if($lselrel['idfiltro_dados'] == $_POST['rel']) {
                        $select = "selected=\"selected\"";
                    }
                    else {
                        $select = "";
                    }
                    echo "<option value=\"".$lselrel['idfiltro_dados']."\" $select>".$lselrel['nomefiltro_dados']."</option>";
            }
            echo "</select></td>";
	  ?>
      </tr>
      <tr>
          <td colspan="2"><input style="border: 1px solid #FFF; height: 18px;width:80px; background-image:url(../images/button.jpg)" name="pesq" type="submit" value="Pesquisar" /> <input type="button" name="gerasenha" id="gerasenha" value="Gerar Senhas" style=" margin-left: 15px;border: 1px solid #FFF; height: 18px;padding-left: 10px; padding-right: 10px; background-image:url(../images/button.jpg)" /></td>
      </tr>
    </table>
</form><br/>
<fieldset style="margin: 5px; padding: 10px; font-size: 12px; font-weight: bold; border: 1px solid #000 ">
    <legend style=" background-color: #FFF; padding: 4px">ARQUIVO DE SENHAS:</legend>
    <ul>
        <li style="list-style:none" id="liarquivo">
            <?php
            if(file_exists($_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/logs/".str_replace(" ", "_", $_SESSION['nomecli'])."/arquivo_senha.csv")) {
            ?>
            <a id="alink" href="<?php echo "/monitoria_supervisao/logs/".str_replace(" ", "_", $_SESSION['nomecli'])."/arquivo_senha.csv";?>" target="_blank" style=" text-decoration: none">ARQUIVO DE SENHA</a><input type="button" name="delsenhas" id="delsenhas" value="" style=" background-color: transparent; background-image: url('/monitoria_supervisao/images/exit.png'); margin-left: 10px; width: 15px; height: 15x; border: 0px" />
            <?php
            }
            ?>
            </li>
    </ul>
</fieldset><br/>
<hr /><br/><br/>
<?php
if(isset($_GET['idoper']) && !isset($_POST['pesq'])) {
    $idoper = $_GET['idoper'];
    $selco = "SHOW COLUMNS FROM operador";
    $eselco = $_SESSION['query']($selco) or die ("erro na query de consulta das colunas da tabela operador");
    while($lselco = $_SESSION['fetch_array']($eselco)) {
        $selcampo = "SELECT tipo FROM coluna_oper WHERE nomecoluna='".$lselco['Field']."'";
        $eselcampo = $_SESSION['query']($selcampo) or die ("erro na consulta do campo na tabela colunas");
        $ecount = $_SESSION['num_rows']($eselcampo);
        if($ecount >= 1) {
            $lselcampos = $_SESSION['fetch_array']($eselcampo);
            if($lselco['Field'] == "idsuper_oper") {
                $colsoper[$lselcampos['tipo']]['idsuper_oper'] = $lselco['Field'];
                $colsoper[$lselcampos['tipo']]['super_oper'] = "SUPERVISOR";
                $colsquery[] = "o.idsuper_oper";
                $colsquery[] = "so.super_oper";
            }
            else {
                $colsoper[$lselcampos['tipo']][$lselco['Field']] = $lselco['Field'];
                $colsquery[] = "o.".$lselco['Field'];
            }
        }
        else {
        }
    }
?>
<form action="cadoperador.php" method="post">
<div id="dadosoper" style="width:1020px; border: 1px solid #FFF">
     <table width="550">
          <tr>
            <td width="683" colspan="2" align="center" class="corfd_ntab"><strong>DADOS OPERADOR</strong></td>
          </tr>
     </table>
	<div id="dados" style="width:1020px; float:left;">
        <table width="550">
          <?php
          $filas = array('fila_grava','fila_oper');
          $filtros = array();
          foreach($filas as $ft) {
              $selfiltoper = "SELECT DISTINCT(".$classtab->AliasTab($ft).".idrel_filtros) as ffilt FROM operador o
                              INNER JOIN ".$ft." ".$classtab->AliasTab($ft)." ON ".$classtab->AliasTab($ft).".idoperador = o.idoperador
                              INNER JOIN upload ".$classtab->AliasTab(upload)." ON ".$classtab->AliasTab(upload).".idupload = ".$classtab->AliasTab($ft).".idupload
                              WHERE o.idoperador='$idoper'";
              $eselfilt = $_SESSION['query']($selfiltoper) or die ("erro na query de consulta dos filtros do operador");
              while($lfilt = $_SESSION['fetch_array']($eselfilt)) {
                  $filtros[$ft] = $lfilt['ffilt'];
              }
          }
          $seloper = "SELECT ".implode(",",$colsquery)." FROM operador o INNER JOIN super_oper so ON so.idsuper_oper = o.idsuper_oper WHERE idoperador='$idoper'";
          $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consulta dos dados do operador");
          while($lseloper = $_SESSION['fetch_array']($eseloper)) {
              foreach($colsoper as $ktipo => $tipo) {
                  foreach($tipo as $kco => $co) {
                      if($ktipo == "DATE") {
                         $dadosoper[$ktipo][$co] = banco2data($lseloper[$kco]);
                      }
                      else {
                          $dadosoper[$ktipo][$co] = $lseloper[$kco];
                      }
                  }
              }
          }
          foreach($dadosoper as $k => $d) {
              foreach($d as $kdados => $dado) {
                  if($kdados != "idsuper_oper") {
              ?>
              <tr>
                <td width="17" class="corfd_coltexto"><strong><?php echo strtoupper($kdados);?></strong></td>
                <?php
                if($kdados == "SUPERVISOR") {
                    $idssuper[$dadosoper['INT']['idsuper_oper']] = $dado;
                ?>
                <td width="320" class="corfd_colcampos">
                    <select name="idsuper_oper" id="idsuper_oper" style="width:320px">
                        <?php
                        $seltroca = "SELECT nomefiltro_nomes,idfiltro_nomes, COUNT(*) as result FROM filtro_nomes WHERE trocafiltro='S'";
                        $etroca = $_SESSION['fetch_array']($_SESSION['query']($seltroca)) or die ("erro na query de consulta do filtro de troca");
                        if($etroca['result'] >= 1) {
                            $fdados = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".$etroca['idfiltro_nomes']."'";
                            $efdados = $_SESSION['query']($fdados) or die ("erro na query de consulta dos dados cadastrados");
                            while($ldados = $_SESSION['fetch_array']($efdados)) {
                                $idfiltrodados[] = $ldados['idfiltro_dados'];
                            }
                            $ftroca = "SELECT rf.idrel_filtros FROM rel_filtros rf
                                       INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros 
                                       WHERE rf.id_".strtolower($etroca['nomefiltro_nomes'])." IN('".implode("','",$idfiltrodados)."') AND cr.trocafiltro='S'";
                            $etroca = $_SESSION['query']($ftroca) or die ("erro na query de levatamento dos rel_filtros com troca");
                            while($ltroca = $_SESSION['fetch_array']($etroca)) {
                                $filtrostroca[] = $ltroca['idrel_filtros'];
                            }
                        }
                        foreach($filtros as $filt) {
                            if(!in_array($filt,$filtrostroca)) {
                                $filtrostroca[] = $filt;
                            }
                        }
                        foreach($filtrostroca as $trel) {
                            foreach($filas as $ftrel) {
                                $selsuper = "SELECT DISTINCT(".$classtab->AliasTab($ftrel).".idsuper_oper),".$classtab->AliasTab(super_oper).".super_oper FROM $ftrel ".$classtab->AliasTab($ftrel)."
                                            INNER JOIN upload ".$classtab->AliasTab(upload)." ON ".$classtab->AliasTab(upload).".idupload = ".$classtab->AliasTab($ftrel).".idupload
                                            INNER JOIN periodo ".$classtab->AliasTab(periodo)." ON ".$classtab->AliasTab(periodo).".idperiodo = ".$classtab->AliasTab(upload).".idperiodo
                                            INNER JOIN super_oper ".$classtab->AliasTab(super_oper)." ON ".$classtab->AliasTab(super_oper).".idsuper_oper = ".$classtab->AliasTab($ftrel).".idsuper_oper
                                            WHERE ".$classtab->AliasTab(periodo).".mes='".date('m')."'";
                                $eselsuper = $_SESSION['query']($selsuper) or die ("erro na query de consulta dos supervisores relacionados no periodo");
                                while($lselsuper = $_SESSION['fetch_array']($eselsuper)) {
                                    if(!array_key_exists($lselsuper['idsuper_oper'],$idsuper)) {
                                        $idssuper[$lselsuper['idsuper_oper']] = $lselsuper['super_oper'];
                                    }
                                }
                            }
                        }
                        foreach($idssuper as $idsuper_oper => $super) {
                            if($idsuper_oper == $idsuper) {
                                ?>
                                <option value="<?php echo $idsuper_oper;?>" selected="selected"><?php echo $super;?></option>
                                <?php
                            }
                            else {
                                ?>
                                <option value="<?php echo $idsuper_oper;?>"><?php echo $super;?></option>
                                <?php
                            }
                        }
                        ?>                        
                    </select>
                </td>
                <?php
                }
                if($kdados != "idsuper_oper" && $kdados != "SUPERVISOR") {
                    if($kdados == "idoperador" OR $kdados == "dtbase" OR $kdados == "horabase") {
                        $bloq = "readonly=\"readonly\"";
                    }
                    else {
                        $bloq = "";
                    }
                ?>
                <td width="318" class="corfd_colcampos"><input class="<?php echo $k;?>" type="text" <?php echo $bloq;?> style="width:320px; border: 1px solid #729fcf" name="<?php echo $kdados;?>" id="<?php echo $kdados;?>" value="<?php echo strtoupper($dado);?>" /></td>
                <?php
                }
                ?>
                </tr>
                <?php
                }
            }
          }
          ?>
        </table><font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font><br />
    </div>
    <div id="histoper" style="width:1020px; float:left; overflow:auto; height:200px">
    	<table width="967" id="hist">
          <thead>
              <tr class="corfd_colcampos">
                <th width="69" align="center" class="corfd_coltexto"><strong>ID REL.</strong></th>
                <th width="69" align="center" class="corfd_coltexto"><strong>IDUPLOAD</strong></th>
                <th width="69" align="center" class="corfd_coltexto"><strong>ID FILA</strong></th>
                <th width="102" align="center" class="corfd_coltexto"><strong>TIPO FILA</strong></th>
                <th width="359" align="center" class="corfd_coltexto"><strong><?php echo strtoupper($_SESSION['filtro']); ?></strong></th>
                <th width="247" align="center" class="corfd_coltexto"><strong>SUPERVISOR</strong></th>
                <th width="77" align="center" class="corfd_coltexto"><strong>DATA INI</strong></th>
                <th width="85" align="center" class="corfd_coltexto"><strong>DATA FIM</strong></th>
              </tr>
          </thead>
            <tbody>
              <?php
              foreach($filtros as $tab => $idrel) {
                    $alias = $classtab->AliasTab($tab);
                    $selfila = "SELECT $alias.id$tab, $alias.idupload,$alias.idrel_filtros, ".$classtab->AliasTab(interperiodo).".dataini, ".$classtab->AliasTab(interperiodo).".datafim, so.super_oper FROM $tab $alias 
                                INNER JOIN super_oper so ON so.idsuper_oper = $alias.idsuper_oper
                                INNER JOIN upload ".$classtab->AliasTab(upload)." ON ".$classtab->AliasTab(upload).".idupload = $alias.idupload
                                INNER JOIN interperiodo ".$classtab->AliasTab(interperiodo)." ON ".$classtab->AliasTab(interperiodo).".idperiodo = ".$classtab->AliasTab(upload).".idperiodo AND ".$classtab->AliasTab(interperiodo).".semana = ".$classtab->AliasTab(upload).".semana
                                WHERE $alias.idrel_filtros='$idrel' AND $alias.idoperador='$idoper' ORDER BY ".$classtab->AliasTab(interperiodo).".dataini DESC";
                    $eselfila = $_SESSION['query']($selfila) or die ("erro na query de consulta da fila");
                    while($lselfila = $_SESSION['fetch_array']($eselfila)) {
                        echo "<tr>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$idrel."</td>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselfila['idupload']."</td>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselfila['id'.$tab]."</td>";
			echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".strtoupper($tab)."</td>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".nomeapres($idrel)."</td>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselfila['super_oper']."</td>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".banco2data($lselfila['dataini'])."</td>";
                        echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".banco2data($lselfila['datafim'])."</td>";
                        echo "</tr>";
                    }
              }
              ?>
            </tbody>
        </table>
    </div>
    <table width="50%">
      <tr>
          <td><input style="border: 1px solid #FFF; height: 18px; width:80px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="altera" id="altera" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; margin-left: 10px;padding-left: 10px; padding-right: 10px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="<?php echo $idoper;?>" id="reset" type="button" value="Reset Senha" /><input type="button" name="delete" id="delete" value="DELETA OP." style=" margin-left: 10px; background-color: #E65E5E; color:#000; border: 0px; padding: 5px; font-weight: bold" /></td>
      </tr>
    </table>
    <br/>
</div><br /><br />
</form>
<?php
}
    $selcolumns = "SHOW COLUMNS FROM operador";
    $eselcolumns = $_SESSION['query']($selcolumns) or die ("erro na query de consulta das colunas da tabela operador");
    while($lselcolumns = $_SESSION['fetch_array']($eselcolumns)) {
        $tabcolumns[] = $lselcolumns['Field'];
    }
    foreach($tabcolumns as $columns) {
        if($columns != "idoperador" && $columns != "idsuper_oper" && $columns != "dtbase" && $columns != "horabase") {
            $coltab[] = ",o.".$columns;
        }
    }
    $sqlinner[] = "INNER JOIN super_oper s ON s.idsuper_oper = o.idsuper_oper";
    $coltab = implode(" ",$coltab);
    if(isset($_POST['pesq'])) {
        $cpf = str_replace(".", "", $_POST['cpfpesq']);
        $cpf = str_replace("-", "", $cpf);
        $opcoes = array('operador' => $_POST['operadorpesq'], 'cpf' => $cpf, 'supervisor' => $_POST['supervisorpesq'], 'rel' => $_POST['rel'], 'tmoni' => $_POST['tmoni']);
        $count = 0;
        foreach($opcoes as $verif) {
            if($verif != ""){
                $count++;
            }
        }
        if($count >= 1) {
                foreach($opcoes as $keyopcao => $opcao) {
                    if($keyopcao == "operador") {
                        if($opcao != "") {
                            $sqlwhere[] = "o.operador LIKE '%".$opcao."%'";
                        }
                    }
                    if($keyopcao == "supervisor") {
                        if($opcao != "") {
                            $sqlwhere[] = "s.super_oper LIKE '%".$opcao."%'";
                        }
                    }
                    if($keyopcao == "cpf") {
                        if($opcao != "") {
                            $sqlwhere[] = "o.cpfoper='".$opcao."'";
                        }
                    }
                    if($keyopcao == "rel") {
                        $selrel = "SELECT rf.idrel_filtros FROM rel_filtros rf
                                   INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros 
                                   WHERE rf.id_".strtolower($eselult['nomefiltro_nomes'])."='$opcao'";
                        $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relacionamentos");
                        if($opcao != "") {
                            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                                $idrels[] = $lselrel['idrel_filtros'];
                            }
                            if($_POST['tmoni'] == "G") {
                                $sqlwhere[] = "rfg.idrel_filtros IN ('".implode("','",$idrels)."')";
                            }
                            if($_POST['tmoni'] == "O") {
                                $sqlwhere[] = "rfo.idrel_filtros IN ('".implode("','",$idrels)."')";
                            }
                        }
                    }
                }
                if($_POST['tmoni'] == "G") {
                    $sqlinner[] = "INNER JOIN fila_grava fg ON fg.idoperador = o.idoperador";
                    $sqlinner[] = "INNER JOIN rel_filtros rfg ON rfg.idrel_filtros = fg.idrel_filtros";
                }
                if($_POST['tmoni'] == "O") {
                    $sqlinner[] = "INNER JOIN fila_oper fo ON fo.idoperador = o.idoperador";
                    $sqlinner[] = "INNER JOIN rel_filtros rfo ON rfo.idrel_filtros = fo.idrel_filtros";
                }
                if($sqlwhere == "") {
                }
                else {
                    $sqlwhere = "WHERE ".implode(" OR ",$sqlwhere);
                }
                $seldados = "SELECT DISTINCT(o.idoperador), o.idsuper_oper, o.dtbase, o.horabase, s.super_oper $coltab FROM operador o ".implode(" ",$sqlinner)." $sqlwhere GROUP BY o.idoperador ORDER BY o.operador";
        }
        else {
            $seldados = "SELECT DISTINCT(o.idoperador), o.idsuper_oper, o.dtbase, o.horabase, s.super_oper $coltab FROM operador o ".implode(" ",$sqlinner)." GROUP BY o.idoperador ORDER BY o.operador";
        }
    }
    else {
        $seldados = "";
    }
    if($seldados != "") {
        $eseldados = $_SESSION['query']($seldados) or die ("erro na query de consutla dos dados dos operadores");
    }
    $qtde = 0;
    ?>
    <div class="listoperador" style="width:1020px; height:500px; overflow:auto">
    <?php
    $dados .= "<table width=\"1500px\" id=\"taboper\">";
    $dados .= "<thead>";
    //echo "<tr class=\"corfd_coltexto\">";
    $selcampos = "SHOW COLUMNS FROM operador";
    $eselcampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos campos da tabela");
    while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
        $cols[] = $lselcampos['Type']."-".$lselcampos['Field'];
        if($lselcampos['Field'] == "idsuper_oper") {
            $dados .= "<th align=\"center\" class=\"corfd_coltexto\"><strong>SUPERVISOR</strong></th>";
        }
        else {
            $dados .= "<th align=\"center\" class=\"corfd_coltexto\"><strong>".strtoupper($lselcampos['Field'])."</strong></th>";
        }
    }
    $dados .= "<th align=\"center\" class=\"corfd_coltexto\">".$eselult['nomefiltro_nomes']."</th>";
    //echo "</tr>";
    $dados .= "</thead>";
    $dados .= "<tbody>";
    if($eseldados == "") {
    }
    else {
        while($lseldados = $_SESSION['fetch_array']($eseldados)) {
            $filtrosop = array();
            $self = "SELECT * FROM fila_grava fg
                     INNER JOIN rel_filtros rf ON rf.idrel_filtros = fg.idrel_filtros
                     INNER JOIN filtro_dados fd on fd.idfiltro_dados = rf.id_".strtolower($eselult['nomefiltro_nomes'])."
                     WHERE fg.idoperador='".$lseldados['idoperador']."' GROUP BY rf.id_".strtolower($eselult['nomefiltro_nomes'])."";
            $eself = $_SESSION['query']($self) or die ("erro na query de consulta dos relacionamentos do operador");
            while($lself = $_SESSION['fetch_array']($eself)) {
                $filtrosop[] = $lself['nomefiltro_dados'];
            }
            if($_POST['duplicado'] == "A") {
                $qtde++;
                $dados .= "<tr bgcolor=\"#FFFFFF\">";
                    foreach($cols as $col) {
                        $divcol = explode("-",$col);
                        $type = trim($divcol[0]);
                        $coluna = trim($divcol[1]);
                        if($coluna == "idoperador" OR $coluna == "operador") {
                            $dados .= "<td align=\"center\"><a style=\"text-decoration:none; color:#000\" href=\"admin.php?menu=operador&visu=det&idoper=".$lseldados['idoperador']."\"><input type=\"hidden\" readonly=\"readonly\" name=\"idoperador\" id=\"idoperador\" value=\"".$lseldados[$coluna]."\" />".$lseldados[$coluna]."</a></td>";
                        }
                        if($coluna == "idsuper_oper") {
                            $dados .= "<td align=\"center\">".$lseldados['super_oper']."</td>";
                        }
                        if($type == "date") {
                            $dados .= "<td align=\"center\">".banco2data($lseldados[$coluna])."</td>";
                        }
                        if($type != "date" && $coluna != 'idsuper_oper' && $coluna != "idoperador" && $coluna != "operador") {
                            $dados .= "<td align=\"center\">".$lseldados[$coluna]."</td>";
                        }
                    }
                    $dados .= "<td>".implode(",",$filtrosop)."</td>";
                $dados .= "</tr>";
            }
            if($_POST['duplicado'] == "N") {
                if(count($filtrosop) >= 2) {
                }
                else {
                    $qtde++;
                    $dados .= "<tr bgcolor=\"#FFFFFF\">";
                    foreach($cols as $col) {
                        $divcol = explode("-",$col);
                        $type = trim($divcol[0]);
                        $coluna = trim($divcol[1]);
                            if($coluna == "idoperador" OR $coluna == "operador") {
                                $dados .= "<td align=\"center\"><a style=\"text-decoration:none; color:#000\" href=\"admin.php?menu=operador&visu=det&idoper=".$lseldados['idoperador']."\"><input type=\"hidden\" readonly=\"readonly\" name=\"idoperador\" id=\"idoperador\" value=\"".$lseldados[$coluna]."\" />".$lseldados[$coluna]."</a></td>";
                            }
                            if($coluna == "idsuper_oper") {
                                $dados .= "<td align=\"center\">".$lseldados['super_oper']."</td>";
                            }
                            if($type == "date") {
                                $dados .= "<td align=\"center\">".banco2data($lseldados[$coluna])."</td>";
                            }
                            if($type != "date" && $coluna != 'idsuper_oper' && $coluna != "idoperador" && $coluna != "operador") {
                                $dados .= "<td align=\"center\">".$lseldados[$coluna]."</td>";
                            }
                    }
                    $dados .= "<td>".implode(",",$filtrosop)."</td>";
                    $dados .= "</tr>";
                }
            }
            if($_POST['duplicado'] == "S") {
                if(count($filtrosop) <= 1) {
                }
                else {
                    $qtde++;
                    $dados .= "<tr bgcolor=\"#FFFFFF\">";
                    foreach($cols as $col) {
                        $divcol = explode("-",$col);
                        $type = trim($divcol[0]);
                        $coluna = trim($divcol[1]);
                        if($coluna == "idoperador" OR $coluna == "operador") {
                            $dados .= "<td align=\"center\"><a style=\"text-decoration:none; color:#000\" href=\"admin.php?menu=operador&visu=det&idoper=".$lseldados['idoperador']."\"><input type=\"hidden\" readonly=\"readonly\" name=\"idoperador\" id=\"idoperador\" value=\"".$lseldados[$coluna]."\" />".$lseldados[$coluna]."</a></td>";
                        }
                        if($coluna == "idsuper_oper") {
                            $dados .= "<td align=\"center\">".$lseldados['super_oper']."</td>";
                        }
                        if($type == "date") {
                            $dados .= "<td align=\"center\">".banco2data($lseldados[$coluna])."</td>";
                        }
                        if($type != "date" && $coluna != 'idsuper_oper' && $coluna != "idoperador" && $coluna != "operador") {
                            $dados .= "<td align=\"center\">".$lseldados[$coluna]."</td>";
                        }
                    }
                    $dados .= "<td>".implode(",",$filtrosop)."</td>";
                    $dados .= "</tr>";
                }
            }
        }
    }
    $dados .= "</tbody>";
    $dados .= "</table>";
    echo "<table width=\"1024\">";
        echo "<tr>";
            echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=filamonitoria\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">EXCEL</div></a></td>";
            echo "<td><a style=\"text-decoration:none; text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=WORD&nome=filamonitoria\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">WORD</div></a></td>";
            echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" target=\"blank\" href=\"/monitoria_supervisao/users/export.php?tipo=PDF&nome=filamonitoria\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">PDF</div></a></td>";
        echo "</tr>";
    echo "</table><br/>";
    $_SESSION['dadosexp'] = $dados;
    echo "<table>";
    echo "<tr>";
        echo "<td class=\"corfd_ntab\" width=\"140px\"><strong>QUANTIDADE</strong></td>";
        echo "<td class=\"corfd_colcampos\" width=\"50px\">$qtde</td>";
    echo "</tr>";
    echo "</table>";
    echo $dados;
    ?>
</div>
</div>
</body>
</html>