<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
$tabelas = new TabelasSql();
$tabelas->ColunasTabRel();
if(isset($_GET['idrelmoni'])) {
  $idrelmoni = $_GET['idrelmoni'];
  $srelmoni = "SELECT * FROM relmoni WHERE idrelmoni='$idrelmoni'";
  $lrmoni = $_SESSION['fetch_array']($_SESSION['query']($srelmoni)) or die ("erro na consulta do relatÃ³rio selecionado");
  $nomerel = $lrmoni['nomerelmoni'];
  $qtdecoldb = $lrmoni['qtdecol'];
  $filtrosdb = explode(",",$lrmoni['filtros']);
  if($lrmoni['idsperfiladm'] == "") {
      $idperfdmdb = "";
  }
  else {
    $idperfadmdb = explode(",",$lrmoni['idsperfiladm']);
  }
  if($lrmoni['idsperfilweb'] == "") {
    $idperfwebdb = "";  
  }
  else {
    $idperfwebdb = explode(",",$lrmoni['idsperfilweb']);
  }
  $vincr = $lrmoni['vincrel'];
  $ativo = $lrmoni['ativo'];
  $tabtercdb = explode(",",$lrmoni['tabterc']);

  //GRAFICO 
  $selgraf = "SELECT * FROM grafrelmoni WHERE idrelmoni='$idrelmoni'";
  $eselgraf = $_SESSION['query']($selgraf) or die ("erro na query de consulta do grafico cadastrado");
  $graf = $_SESSION['num_rows']($eselgraf);
  if($graf >= 1) {
      $lgraf = $_SESSION['fetch_array']($eselgraf);
  }
  else {
  }
}
else {
}

?>

<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript" src="../js/jscolor/jscolor.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    <?php
    if($lgraf['tipograf'] != "") {
    }
    else {
    ?>
        $('#background').attr('disabled','disabled');
        $('#cordados').attr('disabled','disabled');
        $('#width').attr('disabled','disabled');
        $('#heigth').attr('disabled','disabled');
        $('#margin').attr('disabled','disabled');
        $('#marginbuttom').attr('disabled','disabled');
        $('#valores').attr('disabled','disabled');
        $('#pointwidth').attr('disabled','disabled');
        $('#rotacaoval').attr('disabled','disabled');
        $('#posicaoval_x').attr('disabled','disabled');
        $('#posicaoval_y').attr('disabled','disabled');
        $('#legenda').attr('disabled','disabled');
        $('#direcaoleg').attr('disabled','disabled');
        $('#posicaoleg_x').attr('disabled','disabled');
        $('#posicaoleg_y').attr('disabled','disabled');
        $('#bgcolorleg').attr('disabled','disabled');
        $('#bdcolorleg').attr('disabled','disabled');
        $('#rotacaocat').attr('disabled','disabled');
        $('#posicaocat').attr('disabled','disabled');
    <?php
    }
    ?>
    $('#tabpri').change(function() {
        var campo = 'id'+$(this).val();
        $('#groupby').attr('value',campo);
    })
    $('#tabsec').change(function() {
        var tabsec = $(this).val();
        $('#vincrel').load("/monitoria_supervisao/admin/vincrel.php",{tabsec: tabsec});
    })
    $('#tipograf').live("change",function() {
        var tipograf = $(this).val();
        if(tipograf == "") {
            $('#background').attr('disabled','disabled');
            $('#cordados').attr('disabled','disabled');
            $('#width').attr('disabled','disabled');
            $('#heigth').attr('disabled','disabled');
            $('#marginbuttom').attr('disabled','disabled');
            $('#valores').attr('disabled','disabled');
            $('#pointwidth').attr('disabled','disabled');
            $('#rotacaoval').attr('disabled','disabled');
            $('#posicaoval_x').attr('disabled','disabled');
            $('#posicaoval_y').attr('disabled','disabled');
            $('#legenda').attr('disabled','disabled');
            $('#direcaoleg').attr('disabled','disabled');
            $('#posicaoleg_x').attr('disabled','disabled');
            $('#posicaoleg_y').attr('disabled','disabled');
            $('#bgcolorleg').attr('disabled','disabled');
            $('#bdcolorleg').attr('disabled','disabled');
            $('#rotacaocat').attr('disabled','disabled');
            $('#posicaocat').attr('disabled','disabled');
        }
        else {
            $('#background').removeAttr('disabled','disabled');
            $('#cordados').removeAttr('disabled','disabled');
            $('#width').removeAttr('disabled','disabled');
            $('#heigth').removeAttr('disabled','disabled');
            $('#marginbuttom').attr('disabled','disabled');
            $('#valores').removeAttr('disabled','disabled');
            $('#pointwidth').removeAttr('disabled','disabled');
            $('#rotacaoval').removeAttr('disabled','disabled');
            $('#posicaoval_x').removeAttr('disabled','disabled');
            $('#posicaoval_y').removeAttr('disabled','disabled');
            $('#legenda').removeAttr('disabled','disabled');
            $('#direcaoleg').removeAttr('disabled','disabled');
            $('#posicaoleg_x').removeAttr('disabled','disabled');
            $('#posicaoleg_y').removeAttr('disabled','disabled');
            $('#bgcolorleg').removeAttr('disabled','disabled');
            $('#bdcolorleg').removeAttr('disabled','disabled');
            $('#rotacaocat').removeAttr('disabled','disabled');
            $('#posicaocat').removeAttr('disabled','disabled');
        }
    })
    $('#cadrel').submit(function() {
        var tabpri = $('#tabpri').val();
        var tabsec = $('#tabsec').val();
        var tabterc = $('#tabterc').val();
        var nome = $('#nome').val();
        var groupby = $('#groupby').val();
        var qtdecampos = $('#qtdecampos').val();
        var perfadm = $('#perfadm').val();
        var perfweb = $('#perfweb').val();
        var graf = $('#tipograf').val();
        var bgcolorgraf = $('#bgcolorgraf').val();
        var width = $('#width').val();
        var heigth = $('#heigth').val();
        if(tabpri == "" || tabsec == "" || nome == "" || groupby == "" || qtdecampos == "" || perfadm == "" && perfweb == "") {
            if(graf != "") {
                if(bgcolorgraf == "" || width == "" || heigth == "") {
                   alert('Os campos abaixo nÃ£o podem estar vazios\n\
                        Nome\n\
                        Tabela PrimÃ¡ria\n\
                        Agrupa por\n\
                        Tabela SecundÃ¡ria\n\
                        Quantidade de Campos\n\
                        Perfil ADM\n\
                        Perfil WEB\n\
                        Cor Fundo\n\
                        Comprimento\n\
                        Altura\n\
                    ');
                    return false;
                 }
                 else {
                     alert('Os campos abaixo nÃ£o podem estar vazios\n\
                        Nome\n\
                        Tabela PrimÃ¡ria\n\
                        Agrupa por\n\
                        Tabela SecundÃ¡ria\n\
                        Quantidade de Campos\n\
                        Perfil ADM\n\
                        Perfil WEB\n\
                    ');
                    return false;
                 }
            }
            else {
                alert('Os campos abaixo nÃ£o podem estar vazios\n\
                    Nome\n\
                    Tabela PrimÃ¡ria\n\
                    Agrupa por\n\
                    Tabela SecundÃ¡ria\n\
                    Quantidade de Campos\n\
                    Perfil ADM\n\
                    Perfil WEB\n\
                ');
                return false;
            }
        }
        else {
        }
    })
})
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tabrelmoni').tablesorter();
    })
</script>
<div id="conteudo">
        <form action="cadrelmoni.php" method="post" id="cadrel">
            <div style="width:1024px; float: left" class="corfd_pag">
            <div style="width:600px; float:left">
                <table width="577" style="border: 2px solid #FFF">
                  <tr>
                    <td colspan="4" class="corfd_ntab" align="center"><strong>PARAMETROS RELATÓRIO</strong></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>* Nome</strong></td>
                    <td class="corfd_colcampos" colspan="3"><input name="id" type="hidden" value="<?php echo $idrelmoni; ?>" /><input style="width:400px; border: 1px solid #69C; text-align:center" maxlength="50" name="nome" id="nome" type="text" value="<?php echo $nomerel; ?>" /></td>
                  </tr>
                  <tr>
                    <td width="138" class="corfd_coltexto"><strong>* Tabela Primária</strong></td>
                    <td width="152" class="corfd_colcampos"><select style="width:150px; border: 1px solid #69C" name="tabpri" id="tabpri">
                    <?php
                      if(isset($_GET['idrelmoni'])) {
                        echo "<option value=\"\" disabled=\"disabled\">Selecione...</option>";
                        foreach($tabelas->coltabrel as $key => $tabpri) {
                                if(key($tabpri) == $lrmoni['tabpri']) {
                                    echo "<option value=\"".key($tabpri)."\" selected=\"selected\">".key($tabpri)."</option>";
                                }
                                else {
                                  echo "<option value=\"".key($tabpri)."\">".key($tabpri)."</option>";
                                }
                            }
                        }
                        else {
                            echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione...</option>";
                            foreach($tabelas->coltabrel as $key => $tabpri) {
                              echo "<option value=\"".key($tabpri)."\">".key($tabpri)."</option>";
                            }
                        }
                    ?>
                    </select></td>
                    <td width="115" rowspan="4" class="corfd_coltexto"><strong>Obriga Filtros</strong></td>
                    <td width="150" rowspan="4" class="corfd_colcampos"><select style="width:150px; border: 1px solid #69C; height: 250px" name="obfiltros[]" id="obfiltros" multiple="multiple" size="10">
                    <?php
                      $selcampos = "SELECT * FROM filtro_nomes ORDER BY nivel";
                      $ecampos = $_SESSION['query']($selcampos) or die ("erro na query de consulta dos filtros");
                      while($lcampos = $_SESSION['fetch_array']($ecampos)) {
                          $nfiltros[strtolower($lcampos['nomefiltro_nomes'])] = $lcampos['nomefiltro_nomes'];
                      }
                      $filt = array('fg' => "FALHA GRAVE",'fgm' => "FALHA GRAVISSIMA",'dtinimoni' => "INI MONI",'dtfimmoni' => "FIM MONI",'dtinictt' => "INI CTT",
                          'dtfimctt' => "FIM CTT",'monitor' => "MONITOR",'super' => "SUPERVISOR",'oper' => "OPERADOR",'plan' => "PLANILHA",'aval' => "AVALIACAO",
                          'grupo' => "GRUPO",'sub' => "SUB-GRUPO",'perg' => "PERGUNTA",'resp' => "RESPOSTA");
                      $filtros = array_merge($nfiltros,$filt);
                      foreach($filtros as $idfiltro => $filtro) {
                          if(in_array($idfiltro,$filtrosdb)) {
                              echo "<option value=\"".$idfiltro."\" selected=\"selected\">".$filtro."</option>";
                          }
                          else {
                            echo "<option value=\"".$idfiltro."\">".$filtro."</option>";
                          }
                      }
                    ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>* Agrupa por</strong></td>
                    <td class="corfd_colcampos">
                            <input name="groupby" id="groupby" style="width:150px; border: 1px solid #69C" value="<?php if(isset($_GET['idrelmoni'])) { echo $lrmoni['agrupa']; } else {}?>"/>
                        </td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>* Tabela Secundária</strong></td>
                    <td width="152" class="corfd_colcampos"><select style="width:150px; border: 1px solid #69C" name="tabsec" id="tabsec">
                    <?php
                    $tabsecop = array('monitoria', 'regmonitoria','moni_pausa');
                    if(isset($_GET['idrelmoni'])) {
                        echo "<option value=\"\" disabled=\"disabled\">Selecione...</option>";
                        foreach($tabsecop as $tabsec) {
                          if($tabsec == $lrmoni['tabsec']) {
                              echo "<option value=\"".$tabsec."\" selected=\"selected\">".$tabsec."</option>";
                          }
                          else {
                            echo "<option value=\"".$tabsec."\">".$tabsec."</option>";
                          }
                        }
                    }
                    else {
                        echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione...</option>";
                        foreach($tabsecop as $tabsec) {
                            echo "<option value=\"".$tabsec."\">".$tabsec."</option>";
                        }
                    }

                    ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>* Tabelas Terciárias</strong></td>
                    <td width="152" class="corfd_colcampos"><select style="width:150px; border: 1px solid #69C; height: 200px" size="5" name="tabterc[]" id="tabterc" multiple="multiple">
                    <?php
                      if(isset($_GET['idrelmoni'])) {
                        echo "<option value=\"\" disabled=\"disabled\">Selecione...</option>";
                        foreach($tabelas->coltabrel as $key => $tabterc) {
                          if(in_array(key($tabterc),$tabtercdb)) {
                                  echo "<option value=\"".key($tabterc)."\" selected=\"selected\">".key($tabterc)."</option>";
                          }
                          else {
                            echo "<option value=\"".key($tabterc)."\">".key($tabterc)."</option>";
                          }
                        }
                      }
                      else {
                        echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione...</option>";
                    foreach($tabelas->coltabrel as $key => $tabterc) {
                        echo "<option value=\"".key($tabterc)."\">".key($tabterc)."</option>";
                    }
                      }
                    ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td width="115" class="corfd_coltexto"><strong>* Qtde. Colunas</strong></td>
                    <td class="corfd_colcampos" colspan="3"><input style="width:70px; border: 1px solid #69C; text-align:center" name="qtdecampos" id="qtdecampos" type="text" value="<?php if(isset($_GET['idrelmoni'])) { echo $qtdecoldb; } else {}?>" /></td>
                  </tr>
                  <tr>
                    <td height="88" class="corfd_coltexto"><strong>* Perfil ADM</strong></td>
                    <td class="corfd_colcampos"><select style="width:150px; border: 1px solid #69C" multiple="multiple" size="5" name="perfadm[]" id="perfadm">
                    <?php
                    if(isset($_GET['idrelmoni'])) {
                      $perfiladm = "SELECT * FROM perfil_adm";
                      $eperfiladm = $_SESSION['query']($perfiladm) or die ("erro na query de consulta do perfil");
                      while($lperfiladm = $_SESSION['fetch_array']($eperfiladm)) {
                          if(in_array($lperfiladm['idperfil_adm'], $idperfadmdb)) {
                              echo "<option value=\"".$lperfiladm['idperfil_adm']."\" selected=\"selected\">".$lperfiladm['nomeperfil_adm']."</option>";
                          }
                          else {
                            echo "<option value=\"".$lperfiladm['idperfil_adm']."\">".$lperfiladm['nomeperfil_adm']."</option>";
                          }
                      }
                    }
                    else {
                      echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\">Selecione...</option>";
                      $perfiladm = "SELECT * FROM perfil_adm";
                      $eperfiladm = $_SESSION['query']($perfiladm) or die ("erro na query de consulta do perfil");
                      while($lperfiladm = $_SESSION['fetch_array']($eperfiladm)) {
                          echo "<option value=\"".$lperfiladm['idperfil_adm']."\">".$lperfiladm['nomeperfil_adm']."</option>";
                      }
                    }
                    ?>
                    </select></td>
                    <td class="corfd_coltexto"><strong>* Perfil WEB</strong></td>
                    <td class="corfd_colcampos">
                    <select style="width:150px; border: 1px solid #69C" multiple="multiple" size="5" name="perfweb[]" id="perfweb">
                    <?php
                    if(isset($_GET['idrelmoni'])) {
                      $perfilweb = "SELECT * FROM perfil_web";
                      $eperfilweb = $_SESSION['query']($perfilweb) or die ("erro na query de consutla do perfil WEB");
                      while($lperfilweb = $_SESSION['fetch_array']($eperfilweb)) {
                        if(in_array($lperfilweb['idperfil_web'], $idperfwebdb)) {
                            echo "<option value=\"".$lperfilweb['idperfil_web']."\" selected=\"selected\">".$lperfilweb['nomeperfil_web']."</option>";
                        }
                        else {
                          echo "<option value=\"".$lperfilweb['idperfil_web']."\">".$lperfilweb['nomeperfil_web']."</option>";
                        }
                      }
                    }
                    else {
                      echo "<option value=\"\" selected=\"selected\" disabled=\"disabled\">Selecione...</option>";
                      $perfilweb = "SELECT * FROM perfil_web";
                      $eperfilweb = $_SESSION['query']($perfilweb) or die ("erro na query de consutla do perfil WEB");
                      while($lperfilweb = $_SESSION['fetch_array']($eperfilweb)) {
                        echo "<option value=\"".$lperfilweb['idperfil_web']."\">".$lperfilweb['nomeperfil_web']."</option>";
                      }
                    }
                    ?>
                    </select></td>
                  </tr>
                  <tr id="vinc">
                    <td class="corfd_coltexto"><strong>Vincula Rel.</strong></td>
                    <td class="corfd_colcampos"><select style="width:150px; border: 1px solid #69C" name="vincrel" id="vincrel">
                    <?php
                    if(isset($_GET['idrelmoni'])) {
                        $relvinc = "SELECT * FROM relmoni WHERE tabsec='".$lrmoni['tabsec']."' AND ativo='S'";
                        $erelvinc = $_SESSION['query']($relvinc) or die ("erro na query de consulta dos relatÃ³rios criados com a tabela secundÃ¡ria");
                        if($lrmoni['vincrel'] == "00") {
                            echo "<option value=\"\" selected=\"selected\">Selecione...</opiton>";
                        }
                        else {
                        }
                        while($lvinc = $_SESSION['fetch_array']($erelvinc)) {
                            if($lvinc['idrelmoni'] == $lrmoni['vincrel']) {
                                echo "<option value=\"".$lvinc['idrelmoni']."\" selected=\"selected\">".$lvinc['nomerelmoni']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lvinc['idrelmoni']."\">".$lvinc['nomerelmoni']."</option>";
                            }
                        }
                    }
                     else {
                      echo "<option selected=\"selected\" value=\"\">Selecione...</option>";
                     }
                     ?>
                     </select></td>
                     <td class="corfd_coltexto"><strong>Ativo</strong></td>
                     <td class="corfd_colcampos"><select name="ativo">
                      <?php
                    if(isset($_GET['idrelmoni'])) {
                      echo "<option value=\"".$ativo."\">".$ativo."</option>";
                      $ativoop = array ('S','N');
                      foreach($ativoop as $atv) {
                        if($atv == $ativo) {
                        }
                        else {
                          echo "<option value=\"".$atv."\">".$atv."</option>";
                        }
                      }
                    }
                    else {
                      $ativoop = array('S','N');
                      foreach($ativoop as $atv) {
                        echo "<option value=\"".$atv."\">".$atv."</option>";
                      }
                    }
                      ?>
                          </select>
                     </td>
                  </tr>
                  </table>
              </div>
              <div style="width:420px; float:left;">
                <table width="420" style="border: 2px solid #FFF">
                    <tr>
                        <td class="corfd_ntab" align="center" colspan="4"><strong>GRÁFICO</strong></td>
                    </tr>
                    <tr>
                    <td width="83" class="corfd_coltexto"><strong>Tipo Gráfico</strong></td>
                    <td width="108" class="corfd_colcampos" colspan="2"><input type="hidden" name="idgraf" value="<?php if(isset($_GET['idrelmoni'])) { echo $lgraf['idgrafrelmoni'];} else {}?>" />
                        <select name="tipograf" id="tipograf">
                        <?php
                        $tipograf = array('' => 'NENHUM', 'bar' => 'BARRAS','column' => 'COLUNAS', 'line' => 'LINHAS', 'spline' => 'CURVAS', 'bar,spline' => 'BARRAS, CURVAS', 'bar,line' => 'BARRAS, LINHAS', 'column,line' => 'COLUNAS, LINHAS', 'column,spline' => 'COLUNAS, CURVAS');
                        foreach($tipograf as $tipo => $nome) {
                            if($graf >= 1) {
                                if($tipo == $lgraf['tipograf']) {
                                    echo "<option value=\"".$tipo."\" selected=\"selected\">".$nome."</option>";
                                }
                                else {
                                    echo "<option value=\"".$tipo."\">".$nome."</option>";
                                }
                            }
                            else {
                                echo "<option value=\"".$tipo."\">".$nome."</option>";
                            }
                        }
                        ?>
                        </select>
                    </td>
                  </tr>
                  <tr>
                      <td width="78" class="corfd_coltexto"><strong>Cor Fundo</strong></td>
                      <td width="131" class="corfd_colcampos"><input style="width:50px; background-color:#FFF; border: 1px solid #69C" maxlength="6" class="color" name="background" id="background" value="<?php if($graf >= 1) { echo $lgraf['background']; } else {}?>"/></td>
                      <td class="corfd_coltexto"><strong>Cor Dados</strong></td>
                      <td width="131" class="corfd_colcampos"><input style="width:50px; background-color:#FFF; border: 1px solid #69C" maxlength="6" class="color" name="cordados" id="cordados" value="<?php if($graf >= 1) { echo $lgraf['cordados']; } else {}?>"/></td>
                  </tr>
                  <tr>
                    <td bgcolor="#FFCC99" align="center" colspan="4"><strong>Tamanho</strong></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>Comprimento</strong></td>
                    <td class="corfd_colcampos"><input name="width" id="width" style="width:50px; border: 1px solid #69C; text-align: center" maxlength="4" type="text" value="<?php if($graf >= 1) { echo $lgraf['width']; } else {}?>"/></td>
                    <td class="corfd_coltexto"><strong>Altura</strong></td>
                    <td class="corfd_colcampos"><input name="heigth" id="heigth" style="width:50px; border: 1px solid #69C; text-align: center " maxlength="4" type="text" value="<?php if($graf >= 1) { echo $lgraf['heigth']; } else {}?>"/></td>
                  </tr>
                  <tr>
                    <td bgcolor="#FFCC99" align="center" colspan="4"><strong>Dados</strong></td>
                  </tr>
                  <tr>
                      <td class="corfd_coltexto"><strong>Margem Topo</strong></td>
                      <td class="corfd_colcampos"><input name="margin" id="margin" style="width:50px; border: 1px solid #69C; text-align: center " maxlength="3" type="text" value="<?php if($graf >= 1) { echo $lgraf['margin']; } else {}?>"/></td>
                      <td class="corfd_coltexto"><strong>Margem Base</strong></td>
                      <td class="corfd_colcampos" colspan="3"><input name="marginbottom" id="marginbottom" style="width:50px; border: 1px solid #69C; text-align: center " maxlength="3" type="text" value="<?php if($graf >= 1) { echo $lgraf['marginbottom']; } else {}?>"/></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>Valores</strong></td>
                    <td class="corfd_colcampos">
                    	<select name="valores" id="valores">
                            <?php
                            $vals = array('S','N');
                            foreach($vals as $v) {
                                if($graf >= 1) {
                                    if($v == $lgraf['valores']) {
                                        echo "<option value=\"".$v."\" selected=\"selected\">".$v."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$v."\">".$v."</option>";
                                    }
                                }
                                else {
                                    echo "<option value=\"".$v."\">".$v."</option>";
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td class="corfd_coltexto"><strong>RotaÃ§Ã£o</strong></td>
                    <td class="corfd_colcampos">
                    	<select name="rotacaoval" id="rotacaoval">
                            <?php
                            $graus = array('NAO','45','90','180','360');
                            foreach($graus as $g) {
                                if($graf >= 1) {
                                    if($g == $lgraf['rotacaoval']) {
                                        echo "<option value=\"".$g."\" selected=\"selected\">".$g."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$g."\">".$g."</option>";
                                    }
                                }
                                else {
                                    echo "<option value=\"".$g."\">".$g."</option>";
                                }
                            }
                            ?>
                        </select>
                    </td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>PosiÃ§Ã£o X</strong></td>
                    <td class="corfd_colcampos"><input name="posicaoval_x" id="posicaoval_x" maxlength="3" type="text" style="width:50px; border: 1px solid #69C; text-align:center" value="<?php if($graf >= 1) { echo $lgraf['posicaoval_x']; } else {}?>"/></td>
                    <td class="corfd_coltexto"><strong>PosiÃ§Ã£o Y</strong></td>
                    <td class="corfd_colcampos"><input name="posicaoval_y" id="posicaoval_y" type="text" maxlength="3" style="width:50px; border: 1px solid #69C; text-align:center" value="<?php if($graf >= 1) { echo $lgraf['posicaoval_y']; } else {}?>"/></td>
                  </tr>
                  <tr>
                    <td bgcolor="#FFCC99" align="center" colspan="4"><strong>Legenda</strong></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>Legenda</strong></td>
                    <td class="corfd_colcampos">
                    	<select name="legenda" id="legenda">
                            <?php
                            $leg = array('NAO','SIM');
                            foreach($leg as $l) {
                                if($graf >= 1) {
                                    if($l == $lgraf['legenda']) {
                                        echo "<option value=\"".$l."\" selected=\"selected\">".$l."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$l."\">".$l."</option>";
                                    }
                                }
                                else {
                                    echo "<option value=\"".$l."\">".$l."</option>";
                                }
                             }
                            ?>
                        </select>
                    </td>
                    <td class="corfd_coltexto"><strong>DireÃ§Ã£o</strong></td>
                    <td class="corfd_colcampos">
                    	<select name="direcaoleg" id="direcaoleg">
                            <?php
                            $direcao = array('' => 'Nenhum', 'H' => 'HORIZONTAL', 'V' => 'VERTICAL');
                            foreach($direcao as $kd => $d) {
                                if($graf >= 1) {
                                    if($kd == $lgraf['direcaoleg']) {
                                        echo "<option value=\"".$kd."\" selected=\"selected\">".$d."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$kd."\">".$d."</option>";
                                    }
                                }
                                else {
                                    echo "<option value=\"".$kd."\">".$d."</option>";
                                }
                            }
                            ?>
                        </select>
                    </td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>PosiÃ§Ã£o X</strong></td>
                    <td class="corfd_colcampos"><input type="text" name="posicaoleg_x" id="posicaoleg_x" maxlength="3" style="width:50px; border: 1px solid #69C; text-align:center" value="<?php if($graf >= 1) { echo $lgraf['posicaoleg_x']; } else {}?>"/></td>
                    <td class="corfd_coltexto"><strong>PosiÃ§Ã£o Y</strong></td>
                    <td class="corfd_colcampos"><input type="text" name="posicaoleg_y" id="posicaoleg_y" maxlength="3" style="width:50px; border: 1px solid #69C; text-align:center" value="<?php if($graf >= 1) { echo $lgraf['posicaoleg_y']; } else {}?>"/></td>
                  </tr>
                  <tr>
                    <td width="78" class="corfd_coltexto"><strong>Cor Fundo</strong></td>
                    <td width="131" class="corfd_colcampos"><input class="color" maxlength="6" style="width:50px; background-color:#FFF; border: 1px solid #69C" name="bgcolorleg" id="bgcolorleg" value="<?php if($graf >= 1) { echo $lgraf['bgcolorleg']; } else {}?>"/></td>
                    <td width="78" class="corfd_coltexto"><strong>Cor Borda</strong></td>
                    <td width="131" class="corfd_colcampos"><input class="color" maxlength="6" style="width:50px; background-color:#FFF; border: 1px solid #69C" name="bdcolorleg" id="bdcolorleg" value="<?php if($graf >= 1) { echo $lgraf['bdcolorleg']; } else {}?>"/></td>
                  </tr>
                  <tr>
                    <td bgcolor="#FFCC99" align="center" colspan="4"><strong>Categoria</strong></td>
                  </tr>
                  <tr>
                    <td class="corfd_coltexto"><strong>RotaÃ§Ã£o</strong></td>
                    <td class="corfd_colcampos">
                    	<select name="rotacaocat" id="rotacaocat">
                            <?php
                            $graus = array('0','45','90','180','360');
                            foreach($graus as $g) {
                                if($graf >= 1) {
                                    if($g == $lgraf['rotacaocat']) {
                                        echo "<option value=\"".$g."\" selected=\"selected\">".$g."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$g."\">".$g."</option>";
                                    }
                                }
                                else {
                                    echo "<option value=\"".$g."\">".$g."</option>";
                                }
                            }
                            ?>
                        </select>
                    </td>
                    <td class="corfd_coltexto"><strong>PosiÃ§Ã£o</strong></td>
                    <td class="corfd_colcampos">
                    	<select name="posicaocat" id="posicaocat">
                            <?php
                            $poscat = array('' => 'Nulo', 'E' => 'ESQUERDA', 'D' => 'DIREITA');
                            foreach($poscat as $kp => $pc) {
                                if($graf >= 1) {
                                    if($pc == $lgraf['posicaocat']) {
                                        echo "<option value=\"".$kp."\" selected=\"selected\">".$pc."</option>";
                                    }
                                    else {
                                        echo "<option value=\"".$kp."\">".$pc."</option>";
                                    }
                                }
                                else {
                                    echo "<option value=\"".$kp."\">".$pc."</option>";
                                }
                            }
                            ?>
                        </select>
                    </td>
                  </tr>
                </table>
              </div>
              <div style="width: 1024px; float:left; ">
                  <table>
                      <tr>
                      <?php
                        if(isset($_GET['idrelmoni'])) {
                          echo "<td colspan=\"5\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"altera\" type=\"submit\" value=\"Alterar\" /> <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"novo\" type=\"submit\" value=\"Novo\" /></td>";
                        }
                        else {
                          echo "<td colspan=\"5\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"cadastra\" type=\"submit\" value=\"Cadastrar\" /></td>";
                        }
                      ?>
                      </tr>
                  </table>
              </div>
            </div>
        <font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
        </form>
    <div style="width:1024px; float:left" class="corfd_pag">
        <form action="" method="get">
        <br></br><br></br>
        <table width="248">
          <tr>
            <td class="corfd_ntab" colspan="2"><strong>PESQUISA RELATÃ“RIOS</strong></td>
          </tr>
          <tr>
            <td width="63" class="corfd_coltexto"><strong>CÃ³d.</strong></td>
            <td width="173" class="corfd_colcampos"><input name="menu" type="hidden" value="relmoni" /><input style="width:70px; border: 1px solid #69C; text-align:center" type="text" name="cod" id="nome" value="<?php echo $_GET['cod']; ?>" /></td>
          </tr>
          <tr>
            <td class="corfd_coltexto"><strong>Nome</strong></td>
            <td class="corfd_colcampos"><input style="width:180px; border: 1px solid #69C; text-align:center" type="text" name="nome" id="nome" value="<?php echo $_GET['nome']; ?>" /></td>
          </tr>
          <tr>
            <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesquisa" type="submit" value="Pesquisar" /></td>
          </tr>
        </table>
        </form>
    </div>
<div style="width:1024px; height: 300px; overflow: auto; float:left" class="corfd_pag">
<font color="#FF0000"><strong><?php echo $_GET['msgi'] ?></strong></font>
<table width="1200" id="tabrelmoni">
    <thead>
      <tr>
        <th width="27" align="center" class="corfd_coltexto"><strong>ID</strong></th>
        <th width="163" align="center" class="corfd_coltexto"><strong>NOME</strong></th>
        <th width="114" align="center" class="corfd_coltexto"><strong>TAB. PRI.</strong></th>
        <th width="96" align="center" class="corfd_coltexto"><strong>AGRUPA POR</strong></th>
        <th width="126" align="center" class="corfd_coltexto"><strong>TAB. SEC.</strong></th>
        <th width="134" align="center" class="corfd_coltexto"><strong>TAB. TERC.</strong></th>
        <th width="36" align="center" class="corfd_coltexto"><strong>ATIVO</strong></th>
        <th width="287"></th>
      </tr>
    </thead>
<?php
if(isset($_GET['pesquisa'])) {
	$cod = mysql_real_escape_string($_GET['cod']);
	$nome = mysql_real_escape_string($_GET['nome']);
	if($cod == "" && $nome != "") {
		$sql = "WHERE UPPER(nomerelmoni) LIKE '%$nome%'";
	}
	if($cod != "" && $nome == "") {
		$sql = "WHERE idrelmoni='$cod'";
	}
	if($cod != "" && $nome != "") {
		$sql = "WHERE UPPER(nomerelmoni) LIKE '%$nome' AND idrelmoni='$cod'";
	}
	if($cod == "" && $nome == "") {
		$sql = "";
	}
	$selrel = "SELECT * FROM relmoni $sql";
}
if(isset($_GET['org'])) {
    $org = $_GET['org'];
    $selrel = "SELECT * FROM relmoni ORDER BY $org";
}
if(!isset($_GET['pesquisa']) && !isset($_GET['org'])) {
	$sql = "";
	$selrel = "SELECT * FROM relmoni $sql";
}
//echo "<table width:500px>";
        echo "<tbody>";
	$eselrel = $_SESSION['query']($selrel) or die ("erro na consulta dos relatÃ³rio");
	while($lselrel = $_SESSION['fetch_array']($eselrel)) {
		echo "<form action=\"cadrelmoni.php\" method=\"POST\">";
		echo "<tr>";
		echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['idrelmoni']."<input type=\"hidden\" name=\"id\" value=\"".$lselrel['idrelmoni']."\"></td>";
		echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['nomerelmoni']."<input type=\"hidden\" name=\"nome\" value=\"".$lselrel['nomerelmoni']."\"></td>";
		echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['tabpri']."<input type=\"hidden\" name=\"tabpri\" value=\"".$lselrel['tabpri']."\"></td>";
                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['agrupa']."<input type=\"hidden\" name=\"agrupa\" value=\"".$lselrel['agrupa']."\"></td>";
                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['tabsec']."<input type=\"hidden\" name=\"tabsec\" value=\"".$lselrel['tabsec']."\"></td>";
                echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['tabterc']."<input type=\"hidden\" name=\"tabterc\" value=\"".$lselrel['tabterc']."\"></td>";
		echo "<td bgcolor=\"#FFFFFF\" align=\"center\">".$lselrel['ativo']."<input type=\"hidden\" name=\"ativo\" value=\"".$lselrel['ativo']."\"></td>";
		echo "<td><input style=\"border: 1px solid #FFF; height: 18px; width:70px; background-image:url(../images/button.jpg)\" name=\"carrega\" type=\"submit\" value=\"Carregar\" /> <input style=\"border: 1px solid #FFF; height: 18px; width:70px; background-image:url(../images/button.jpg)\" name=\"relcol\" type=\"submit\" value=\"Relcionar\" /> <input style=\"border: 1px solid #FFF; height: 18px; width:70px; background-image:url(../images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"Apagar\" /></td>";
  		echo "</tr></form>";
	}
        echo "</tbody>";
?>
</table>
</div>
</div>