<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/users/combofiltros_<?php echo strtolower($_SESSION['nomecli']);?>.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataini').mask('99/99/9999');
        $('#datafim').mask('99/99/9999');
        
        $('#idfluxo').change(function() {   
            var fluxo = $(this).val();
            $('#idstatus').load('carregastatus.php',{fluxo:fluxo,tipo:'status'});
        })
        
        $("#idperiodo").change(function() {
            var idperiodo = $(this).val();
            $.post("/monitoria_supervisao/admin/carregaper.php",{idperiodo:idperiodo,con: 'per'},function(valor) {
               var dados = valor.split(",");
               $('#dataini').attr('value',dados[0]);
               $('#datafim').attr('value',dados[1]);
            });
        })
        
        $('#finaliza').click(function() {
            var dataini = $('#dataini').val();
            var datafim = $('#datafim').val();
            var fluxo = $('#idfluxo').val();
            var nomefluxo = $('#idfluxo').find('option').filter(':selected').text();
            
            if(dataini == "" || datafim == "" || fluxo == "") {
                alert('Para finalizar as monitorias, é necessário informar "DATA INICIAL", "DATA FINAL" e "FLUXO"');
                return false;
            }
            else {
                var confirma = confirm('Você deseja confirmar a finalização das monitorias com os filtros abaixo:\n\
                                        Data Inicial:'+dataini+'\n\
                                        Data Final:'+datafim+'\n\
                                        FLuxo:'+nomefluxo+'');
                if(confirma) {
                    
                }
                else {
                    return false;
                }
            }
        })
    })
</script>
<div id="conteudo">
    <div style="width:1024px;">
        <form action="" method="post">
            <table width="451">
              <tr>
                <td class="corfd_ntab" colspan="2" align="center"><strong>FILNALIZAÇÃO AUTOMÁTICA</strong></td>
              </tr>
              <tr>
                <td width="102" class="corfd_coltexto"><strong>PERÍODO</strong></td>
                <td width="331" class="corfd_colcampos">
                    <select name="idperiodo" id="idperiodo">
                        <option value="">SELECIONE...</option>
                            <?php
                            $selperiodo = "SELECT * FROM periodo ORDER BY ano DESC, mes DESC";
                            $eselperiodo = $_SESSION['query']($selperiodo) or die ("erro na query de consulta dos períodos");
                            while($lselperiodo = $_SESSION['fetch_array']($eselperiodo)) {
                                ?>
                                <option value="<?php echo $lselperiodo['idperiodo'];?>"><?php echo $lselperiodo['nmes']."/".$lselperiodo['ano'];?></option>
                                <?php
                            }
                            ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>INTERVALO</strong></td>
                <td class="corfd_colcampos"><input name="dataini" id="dataini" style="border: 1px solid #CCC; width:80px; text-align: center" /><strong> À </strong><input name="datafim" id="datafim" style="border: 1px solid #CCC; width:80px; text-align: center" /></td>
              </tr>
                  <?php
                  $periodo = periodo ();

                  $iduser = $_SESSION['usuarioID'];
                  $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                  $efiltro = $_SESSION['query']($selfiltros) or die ("erro na consulta dos nomes dos filtros");
                  while($lfiltros = $_SESSION['fetch_array']($efiltro)) {
                          $filtros[strtolower($lfiltros['nomefiltro_nomes'])] = $lfiltros['idfiltro_nomes'];
                          $nome = $lfiltros['nomefiltro_nomes'];
                          $idfiltro = "id_".strtolower($lfiltros['nomefiltro_nomes']);
                          if($_SESSION['user_tabela'] == "user_web") {
                          $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM userwebfiltro
                                                         INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = userwebfiltro.idrel_filtros
                                                         INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                                                         INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                                                         INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                                                         INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                                                         WHERE iduser_web='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados";
                          }
                          if($_SESSION['user_tabela'] == "user_adm") {
                          $filtrodados = "SELECT DISTINCT(rel_filtros.$idfiltro), filtro_dados.nomefiltro_dados, fn.nivel, idindice, idfluxo FROM useradmfiltro
                                                         INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = useradmfiltro.idrel_filtros
                                                         INNER JOIN conf_rel ON conf_rel.idrel_filtros = rel_filtros.idrel_filtros
                                                         INNER JOIN param_moni ON param_moni.idparam_moni = conf_rel.idparam_moni
                                                         INNER JOIN filtro_dados ON filtro_dados.idfiltro_dados = rel_filtros.$idfiltro
                                                         INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = filtro_dados.idfiltro_nomes
                                                         WHERE iduser_adm='$iduser' AND filtro_dados.idfiltro_nomes = '".$lfiltros['idfiltro_nomes']."' AND filtro_dados.ativo='S' GROUP BY filtro_dados.idfiltro_dados";
                          }
                          $efiltrodados = $_SESSION['query']($filtrodados) or die ("erro na consulta dos filtros realcionados os usuário");
                          echo "<tr>";
                                echo "<td class=\"corfd_coltexto\"><strong>".$nome."</strong></td>";
                                echo "<td class=\"corfd_colcampos\"><select style=\"width:300px;border: 1px solid #333;\" name=\"filtro_".strtolower($nome)."\" id=\"filtro_".strtolower($nome)."\">";
                                echo "<option value=\"\">Selecione....</option>";
                                while($lfiltrodados = $_SESSION['fetch_array']($efiltrodados)) {
                                    if(in_array($lfiltrodados['idindice'],$idsindice)) {
                                    }
                                    else {
                                        $idsindice[] = $lfiltrodados['idindice'];
                                    }
                                    if(in_array($lfiltrodados['idfluxo'],$idsfluxo)) {
                                    }
                                    else {
                                        $idsfluxo[] = $lfiltrodados['idfluxo'];
                                    }
                                    if($lfiltrodados[$idfiltro] == $_POST['filtro_'.strtolower($nome)] OR $lfiltrodados[$idfiltro] == $varget['filtro_'.strtolower($nome)]) {
                                            $select = "selected=\"selected\"";
                                    }
                                    else {
                                            $select = "";
                                    }
                                    echo "<option value=\"".$lfiltrodados[$idfiltro]."\" $select >".$lfiltrodados['nomefiltro_dados']."</option>";
                                }
                                echo "</select><td>";
                         echo "</tr>";
                  }
                  ?>
              <tr>
                <td class="corfd_coltexto"><strong>FLUXO</strong></td>
                <td class="corfd_colcampos">
                    <select name="idfluxo" id="idfluxo">
                        <option value="" selected="selected">SELECIONE...</option>
                    <?php
                        $selfluxo = "SELECT * FROM fluxo";
                        $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta do fluxo");
                        while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                            ?>
                            <option value="<?php echo $lselfluxo['idfluxo'];?>"><?php echo $lselfluxo['nomefluxo'];?></option>
                            <?php
                        }
                        ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>STATUS</strong></td>
                <td class="corfd_colcampos"><select name="idstatus" id="idstatus"></select></td>
              </tr>
              <tr>
                <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="finaliza" id="finaliza" type="submit" value="Finalizar" /></td>
              </tr>
            </table>
        </form>
    </div>
    <div style="width:1024px">
            <br /><hr />
        <?php
        if(isset($_POST['finaliza'])) {
            ?>
            <table width="318">
              <tr>
                <td colspan="3" class="corfd_ntab" align="center"><strong>RESULTADO DAS FINALIZAÇÕES</strong></td>
              </tr>
                <?php
            $seladm = "SELECT iduser_adm FROM user_adm WHERE cpf='00000000000'";
            $eseladm = $_SESSION['fetch_array']($_SESSION['query']($seladm)) or die ("erro na query para consultar o usuário admin");
            $iduseradm = $eseladm['iduser_adm'];
            $dataini = data2banco($_POST['dataini']);
            $datafim = data2banco($_POST['datafim']);
            $idfluxo = $_POST['idfluxo'];
            $idstatus = $_POST['idstatus'];
            $data = date('Y-m-d');
            $hora = date('H:i:s');

            // obtém filtros de relacionamento
            foreach($filtros as $nomeflt => $flt) {
                if($_POST['filtro_'.$nomeflt] != "") {
                    $filtrosrel['id_'.$nomeflt] = "id_".$nomeflt."='".$_POST['filtro_'.$nomeflt]."'";
                }
                else {
                }
            }
            if($filtrosrel != "") {
                $selrel = "SELECT * FROM rel_filtros WHERE ".implode($filtrosrel);
                $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta dos relacionamentos");
                while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                    $idsrel[] = $lselrel['idrel_filtros'];
                }
                $whererel = "AND idrel_filtros IN ('".implode("','",$idsrel)."')";
            }
            else {
                $whererel = "";
            }

            $obs = 'Monitorria atulizada pelo fechamento automático';

            // inicia processso de finalização

            $seltotalmoni = "SELECT COUNT(*) as result FROM monitoria WHERE data BETWEEN '$dataini' AND '$datafim' AND situacao='A' $whererel";
            $eseltotal = $_SESSION['fetch_array']($_SESSION['query']($seltotalmoni)) or die ("erro na query de contagem da monitorias");

            if($eseltotal['result'] == 0) {
                ?>
                <tr>
                    <td class="corfd_colcampos" style="color:#FF0000" colspan="4"><strong>NÃO EXISTEM MONITORIAS REALIZADAS NO PERÍODO</strong></td>
                </tr>
                <?php                    
            }
            else {
                if($idstatus != "") {
                    $relfluxo = "SELECT rf.idrel_fluxo,rf.idstatus,s.nomestatus,rf.iddefinicao,d.tipodefinicao FROM rel_fluxo rf
                                 INNER JOIN status s ON s.idstatus = rf.idstatus
                                 INNER JOIN definicao d ON d.iddefinicao = rf.iddefinicao
                                 WHERE rf.idfluxo='$idfluxo' AND rf.idstatus='$idstatus' AND d.tipodefinicao='automatico' AND rf.tipo='finaliza'";
                }
                else {
                    $relfluxo = "SELECT rf.idrel_fluxo,rf.idstatus,s.nomestatus,rf.iddefinicao,d.tipodefinicao,rf.idatustatus FROM rel_fluxo rf
                                 INNER JOIN status s ON s.idstatus = rf.idstatus
                                 INNER JOIN definicao d ON d.iddefinicao = rf.iddefinicao
                                 WHERE rf.idfluxo='$idfluxo' AND d.tipodefinicao='automatico' AND rf.tipo='finaliza'";
                }
                $eselrel = $_SESSION['query']($relfluxo) or die ("erro na query de consulta dos relacionamentos do fluxo");
                $nrel = $_SESSION['num_rows']($eselrel);
                if($nrel >= 1) {
                    $i = 0;
                    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                        $i++;
                        $idsrel[$i]['idrel_fluxo'] = $lselrel['idrel_fluxo'];
                        $idsrel[$i]['idstatus'] = $lselrel['idstatus'];
                        $idsrel[$i]['nomestatus'] = $lselrel['nomestatus'];
                        $idsrel[$i]['iddefinicao'] = $lselrel['iddefinicao'];
                        $idsrel[$i]['idatustatus'] = $lselrel['idatustatus'];
                        $idsrel[$i]['tipodefinicao'] = $lselrel['tipodefinicao'];
                    }
                    $totalmonitorias = "";
                    foreach($idsrel as $idrel) {
                        $count = 0;
                        $monitotal = "SELECT COUNT(*) as result FROM monitoria m
                                    INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                    INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo
                                    WHERE m.data BETWEEN '$dataini' AND '$datafim' AND rf.idatustatus='".$idrel['idstatus']."' AND rf.idfluxo='$idfluxo' AND situacao='A' $whererel";
                        $cmonitotal = $_SESSION['fetch_array']($_SESSION['query']($monitotal)) or die (mysql_error());
                        if($cmonitotal['result'] >= 1) {
                            $selmoni = "SELECT * FROM monitoria m
                                        INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                        INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo
                                        WHERE m.data BETWEEN '$dataini' AND '$datafim' AND rf.idatustatus='".$idrel['idstatus']."' AND rf.idfluxo='$idfluxo' AND situacao='A' $whererel";
                            $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de seleção das monitorias");
                            while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                                $tab = array('monitoria','monitoria_fluxo','rel_fluxo');
                                $count++;
                                $atufluxo = "INSERT monitoria_fluxo (idmonitoria,tabuser,iduser,idrel_fluxo,idstatus,iddefinicao,data,hora,obs) 
                                             VALUES ('".$lselmoni['idmonitoria']."','user_adm','$iduseradm','".$idrel['idrel_fluxo']."','".$idrel['idstatus']."','".$idrel['iddefinicao']."','$data','$hora','$obs')";
                                $eatufluxo = $_SESSION['query']($atufluxo) or die ("erro na query de atualização das monitorias para fechamento");
                                $idmonifluxo = $_SESSION['insert_id']();
                                $atumoni = "UPDATE monitoria SET idmonitoria_fluxo='$idmonifluxo' WHERE idmonitoria='".$lselmoni['idmonitoria']."'";
                                $eatumoni = $_SESSION['query']($atumoni) or die ("erro na atualização da monitoria");
                            }
                        }
                        $moniatu[$idrel['nomestatus']] = $cmonitotal['result'].",".$count;
                    }
                    ?>
                      <tr>
                        <td width="180" class="corfd_coltexto"><strong>TOTLA DE MONITORIAS DO PERÍODO</strong></td>
                        <td width="126" class="corfd_colcampos" colspan="2" align="center"><?php echo $eseltotal['result'];?></td>
                      </tr>
                          <?php
                          foreach($moniatu as $katu => $atu) {
                              $val = explode(",",$atu);
                              $totalatu = $val[0];
                              $aturea = $val[1];
                              ?>
                              <tr>
                                  <td class="corfd_coltexto"><strong><?php echo $katu;?></strong></td>
                                  <td class="corfd_colcampos" align="center"><?php echo $totalatu;?></td>
                                  <td class="corfd_colcampos" align="center"><?php echo $aturea;?></td>
                              </tr>
                              <?php
                              $somatotal = $somatotal + $totalatu;
                              $somaatu = $somaatu + $aturea;
                          }
                          ?>
                      <tr>
                        <td class="corfd_coltexto"><strong>TOTAL FINALIZADO</strong></td>
                        <td class="corfd_colcampos" align="center"><?php echo $somatotal;?></td>
                        <td class="corfd_colcampos" align="center"><?php echo $somaatu;?></td>
                      </tr>
                    <?php   
                    if($somatotal == $somaatu) {
                        $msg = "FECHAMENTO REALIZADO COM SUCESSO, SEM OCORRÊNCIAS";
                        ?>
                        <tr>
                            <td class="corfd_colcampos" colspan="3" style="color:#F00"><strong><?php echo $msg;?></strong></td>
                        </tr>
                        <?php
                    }
                    else {
                        $msg = "FECHAMENTO REALIZADO, PORÉM EXISTEM INCONSISTENCIAS";
                        ?>
                        <tr>
                            <td class="corfd_colcampos" colspan="3" style="color:#F00"><strong><?php echo $msg;?></strong></td>
                        </tr>
                        <?php
                    }
                }
                else {
                    ?>
                    <tr>
                        <td class="corfd_colcampos" style="color:#FF0000" colspan="3"><strong>NÃO EXISTEM RELACIONAMENTO DE FLUXO CADASTRADOS PARA FINALIZAÇÃO AUTOMÁTICA</strong></td>
                    </tr>
                    <?php
                }
            }
                ?>
                </table>
                <?php
        }
        else {  
        }
        ?>
    </div>
</div>
