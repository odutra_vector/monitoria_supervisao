<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/monitoria_supervisao/js/custom-theme/jquery-ui-1.8.2.custom.css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#cadperiodo').tablesorter();
        
        $(".data").datepicker({
            changeMonth: true,
            changeYear: true,
            //stepMonths: 3,
            buttonImage: '/images/datepicker.gif',
            dateFormat: 'dd/mm/yy',  
            dayNames: [  
            'Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'  
            ],  
            dayNamesMin: [  
            'D','S','T','Q','Q','S','S','D'  
            ],  
            dayNamesShort: [  
            'Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'  
            ],  
            monthNames: [  
            'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro',  
            'Outubro','Novembro','Dezembro'  
            ],  
            monthNamesShort: [  
            'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set',  
            'Out','Nov','Dez'  
            ],  
            nextText: 'Próximo',  
            prevText: 'Anterior'

        });
        
        $("#cadastra").live('click',function() {
            var dtini = $("#dtinimoni").val();
            var dtfim = $("#dtfimmoni").val();
            var inictt = $("#dtinictt").val();
            var fimctt = $("#dtfimctt").val();
            if(dtini == "" || dtfim == "") {
                alert("Favor preencher a data de trabalho do período");
                return false;
            }
            else {
            }
        });
        
        $("input[id*='altera']").live('click',function() {
            var pegaid = $(this).attr("id");
            var id = pegaid.substr(6);
            var dtini = $("#dtinimoni"+id).val();
            var dtfim = $("#dtfimmoni"+id).val();
            if(dtini == "" || dtfim == "") {
                alert("Favor preencher a data de trabalho do período");
                return false;
            }
            else {
            }
        })
    })
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadcalendario.php" method="post">
<table width="500" style="border:1px solid #999">
  <tr>
    <td class="corfd_ntab" colspan="4" align="center"><strong>PERÍODO MONITORIA</strong></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>MÊS</strong></td>
    <td class="corfd_colcampos"><select name="mes">
    <?php
	$meses = array('01' => 'JANEIRO','02' => 'FEVEREIRO','03' => 'MARCO','04' => 'ABRIL','05' => 'MAIO','06' => 'JUNHO','07' => 'JULHO','08' => 'AGOSTO','09' => 'SETEMBRO','10' => 'OUTUBRO','11'=> 'NOVEMBRO','12' => 'DEZEMBRO');
	foreach($meses as $km => $mes) {
                if($km == date(m)) {
                    echo "<option value=\"".$mes."\" selected=\"selected\">".$mes."</option>";
                }
                else {
                    echo "<option value=\"".$mes."\">".$mes."</option>";
                }
	}
	?>
    </select></td>
      <td class="corfd_coltexto"><strong>ANO</strong></td>
      <td class="corfd_colcampos"><select name="ano" id="ano">
      <?php
      $ano = date(Y);
      for($i = 0; $i <= 10; $i++) {
        $anos[] = $ano;
        $ano++;
      }
      foreach($anos as $a) {
        if($a == date(Y)) {
            echo "<option value=\"".$a."\" selected=\"selected\">".$a."</option>";
        }
        else {
            echo "<option value=\"".$a."\">".$a."</option>";
        }
      }
      ?>
      </select></td>
  </tr>
  <tr>
    <td width="65" class="corfd_coltexto"><strong>DATA INI</strong></td>
    <td width="81" class="corfd_colcampos"><input class="data" type="text" name="dtinimoni" id="dtinimoni" /></td>
    <td width="65" class="corfd_coltexto" class="corfd_coltexto"><strong>DATA FIM</strong></td>
    <td width="75" class="corfd_colcampos"><input class="data" type="text" name="dtfimmoni" id="dtfimmoni" /></td>
  </tr>
  <tr>
    <td width="65" class="corfd_coltexto"><strong>DATA INI CTT</strong></td>
    <td width="81" class="corfd_colcampos"><input class="data" type="text" name="dtinictt" id="dtinictt" /></td>
    <td width="65" class="corfd_coltexto" class="corfd_coltexto"><strong>DATA FIM CTT</strong></td>
    <td width="75" class="corfd_colcampos"><input class="data" type="text" name="dtfimctt" id="dtfimctt" /></td>
  </tr>
  <tr>
    <td width="72" class="corfd_colcampos" colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)"  name="cadastra" id="cadastra" type="submit" value="Cadastrar" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font>
<hr />

<font color="#FF0000"><strong><?php echo $_GET['msgi']; ?></strong></font><br/>
<table width="780">
  <tr>
    <td colspan="9" class="corfd_ntab" align="center"><strong>PERÍODOS CADASTRADOS</strong></td>
  </tr>
</table>
<div style="overflow: auto; height: 250px; border: 1px solid #000">
<table width="1000" id="cadperiodo">
    <thead>
      <tr>
        <th width="40" class="corfd_coltexto" align="center"><strong>ID</strong></th>
        <th width="30" class="corfd_coltexto" align="center"><strong>Mes</strong></th>
        <th width="90" class="corfd_coltexto" align="center"><strong>Nome Mes</strong></th>
        <th width="90" class="corfd_coltexto" align="center"><strong>Ano</strong></th>
        <th width="66" class="corfd_coltexto" align="center"><strong>Data Ini</strong></th>
        <th width="66" class="corfd_coltexto" align="center"><strong>Data Fim</strong></th>
        <th width="66" class="corfd_coltexto" align="center"><strong>DtIni Ctt.</strong></th>
        <th width="66" class="corfd_coltexto" align="center"><strong>DtFim Ctt.</strong></th>
        <th width="30" class="corfd_coltexto" align="center"><strong>Dias</strong></th>
        <th width="66" class="corfd_coltexto" align="center"><strong>Data Cad.</strong></th>
        <th width="58" class="corfd_coltexto" align="center"><strong>Hora</strong></th>
        <th width="148" class="corfd_coltexto" align="center"><strong>UsuÃ¡rio</strong></th>
        <th width="137"></th>
      </tr>
    </thead>
    <tbody>
  <?php
  $countcal = "SELECT COUNT(*) as result FROM periodo";
  $ecountcal = $_SESSION['fetch_array']($_SESSION['query']($countcal)) or die (mysql_error());
  if($ecountcal['result'] >= 1) {
	  $selcal = "SELECT * FROM periodo ORDER BY dataini";
	  $eselcal = $_SESSION['query']($selcal) or die (mysql_error());
	  while($lselcal = $_SESSION['fetch_array']($eselcal)) {
		  $nuser = "SELECT * FROM user_adm WHERE iduser_adm='".$lselcal['iduser']."'";
		  $enuser = $_SESSION['fetch_array']($_SESSION['query']($nuser)) or die (mysql_error());
		  echo "<form action=\"cadcalendario.php\" method=\"post\">";
		  echo "<tr>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><a href=\"admin.php?menu=calendario&idperiodo=".$lselcal['idperiodo']."\"><input style=\"width:40px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"text\" name=\"id\" value=\"".$lselcal['idperiodo']."\" /></a></td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><input style=\"width:30px; border: 1px solid #FFF; text-align:center\" type=\"hidden\" name=\"mes\" value=\"".$lselcal['mes']."\" />".$lselcal['mes']."</td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><select name=\"nmes\">";
		  echo "<option selected=\"selected\" value=\"".$lselcal['nmes']."\" />".$lselcal['nmes']."</option>";
		  $mesdb = array($lselcal['nmes']);
		  foreach($meses as $lmes) {
                      if($lmes != $lselcal['nmes']) {
                        echo "<option value=\"".$lmes."\">".$lmes."</option>";
                      }
		  }
		  echo "</select></td>";
                  echo "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lselcal['ano']."</td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><input class=\"data\" style=\"width:70px; border: 1px solid #FFF; text-align:center; font-size:12px\" type=\"text\" name=\"dtinimoni\" id=\"dtinimoni".$lselcal['idperiodo']."\" value=\"".banco2data($lselcal['dataini'])."\" /></td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><input class=\"data\" style=\"width:70px; border: 1px solid #FFF; text-align:center;font-size:12px\" type=\"text\" name=\"dtfimmoni\" id=\"dtfimmoni".$lselcal['idperiodo']."\" value=\"".banco2data($lselcal['datafim'])."\" /></td>";
                  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><input class=\"data\" style=\"width:70px; border: 1px solid #FFF; text-align:center;font-size:12px\" type=\"text\" name=\"dtinictt\" value=\"".banco2data($lselcal['dtinictt'])."\" /></td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\"><input class=\"data\" style=\"width:70px; border: 1px solid #FFF; text-align:center;font-size:12px\" type=\"text\" name=\"dtfimctt\" value=\"".banco2data($lselcal['dtfimctt'])."\" /></td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lselcal['dias']."</td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\">".banco2data($lselcal['datacad'])."</td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\">".$lselcal['hora']."</td>";
		  echo "<td align=\"center\" bgcolor=\"#FFFFFF\">".$enuser['nomeuser_adm']."</td>";
		  echo "<td width=\"156\" align=\"left\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)\" name=\"altera\" id=\"altera".$lselcal['idperiodo']."\" type=\"submit\" value=\"Alterar\" /> <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"Apagar\" /></td>";
		  echo "</tr></form>";
	  }
  }
  else {
  }
  ?>
    </tbody>
</table>
</div><br />
<?php
$calendario = $_GET['idperiodo'];
if(isset($_GET['idperiodo'])) {
    if(file_exists($rais. "/monitoria_supervisao/admin/visuperiodo.php")) {
        include_once "visuperiodo.php";
    }
    else {
    }
}
else {
}
?>
</div>
</body>
</html>
