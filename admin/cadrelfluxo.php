<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

if(isset($_POST['cadrel'])) {
    $fluxo = $_POST['fluxo'];
    $visadm = implode(",",$_POST['visadm']);
    $status = $_POST['status'];
    $visweb = implode(",",$_POST['visweb']);
    $defini = $_POST['definicao'];
    $respweb = implode(",",$_POST['respweb']);
    $respadm = implode(",",$_POST['respadm']);
    $atustatus = $_POST['atustatus'];
    $ativo = $_POST['ativo'];
    $tipo = $_POST['tipo'];
    $classifica = val_vazio($_POST['classifica']);
    $seldefini = "SELECT idrel_fluxo,tipodefinicao, COUNT(*) as result FROM rel_fluxo rf
                  INNER JOIN definicao d ON d.iddefinicao = rf.iddefinicao
                  WHERE rf.iddefinicao='$defini' AND rf.idfluxo='$fluxo'";
    $edefini = $_SESSION['fetch_array']($_SESSION['query']($seldefini)) or die (mysql_error());
    $seltipo = "SELECT COUNT(*) as result FROM rel_fluxo WHERE idfluxo='$fluxo' AND tipo='inicia'";
    $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die ("erro na query de consulta do tipo do status");
    $selfase = "SELECT COUNT(*) as result FROM rel_fluxo WHERE idfluxo='$fluxo'";
    $efase = $_SESSION['fetch_array']($_SESSION['query']($selfase)) or die (mysql_error());
    if($fluxo == "" || $status == "" || $defini == "" || $atustatus == "" || $ativo == "" || $tipo == "") {
        $msgi = "Nenhum campo pode estar vazio para criar o relacionamento do fluxo";
        header("location:admin.php?menu=fluxo&msgi=".$msgi);
    }
    else {
        if(($visadm == "" && $visweb == "") || ($respweb == "" && $respadm == "")) {
          $msgi = "Os campos de ''VisualizaÃ§Ã£o ADM e WEB e ResposÃ¡vel ADM e WEB'' devem conter pelo menos 1 usuÃ¡rio!!!";
          header("location:admin.php?menu=fluxo&msgi=".$msgi);
        }
        else {
            if($edefini['result'] >= 1) {
              $msgi = "Esta ''definição'' já está cadastra para outro relacionamento!!!";
              header("location:admin.php?menu=fluxo&msgi=".$msgi);
            }
            else {
                if($eseltipo['result'] >= 1 && $tipo == "inicia") {
                    $msgi = "SÃ³ pode existir um Ãºnico STATUS com tipo ''INICIA''!!!";
                    header("location:admin.php?menu=fluxo&msgi=".$msgi);
                }
                else {
                    $cad = "INSERT INTO rel_fluxo (idfluxo, idstatus, iddefinicao, idatustatus, tipo, classifica, vis_adm, vis_web, resp_adm, resp_web, ativo)
                    VALUES ('$fluxo','$status', '$defini','$atustatus','$tipo',$classifica,'$visadm','$visweb','$respadm','$respweb','$ativo')";
                    $ecad = $_SESSION['query']($cad) or die (mysql_error());
                    if($ecad) {
                      $msgi = "Relacionamento efetuado com sucesso!!!";
                      header("location:admin.php?menu=fluxo&msgi=".$msgi);
                    }
                    else {
                      $msgi = "Erro no processo de relacionamento, favor contatar o administrador!!!";
                      header("location:admin.php?menu=fluxo&msgi=".$msgi);
                    }
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
  $id = $_POST['idrel'];
  $status = $_POST['status'];
  $defini = $_POST['defini'];
  $selreg = "SELECT COUNT(*) as result FROM reg_fluxo WHERE status='$status' OR defini='$defini'";
  $ereg = $_SESSION['fetch_array']($_SESSION['query']($selreg)) or die (mysql_error());
  if($ereg['result'] >= 1) {
     $msgf = "Este status foi inserido na tabela ''reg_fluxo'', nÃ£o podendo ser apagado!!!";
     header("location:admin.php?menu=fluxo&msgf=".$msgf);
  }
  else {
      $del = "DELETE FROM rel_fluxo WHERE idrel_fluxo='$id'";
      $edel = $_SESSION['query']($del) or die (mysql_error());
      $alter = "ALTER TABLE rel_fluxo AUTO_INCREMENT=1";
      $ealter = $_SESSION['query']($alter) or die (mysql_error());
      if($edel) {
	  $msgf = "Relacionamento apagado com sucesso!!!";
	  header("location:admin.php?menu=fluxo&msgf=".$msgf);
      }
      else {
	  $msgf = "Ocorreu na execuÃ§Ã£o do processo, favor contatar o administrador!!!";
	  header("location:admin.php?menu=fluxo&msgf=".$msgf);
      }
  }
}

if(isset($_POST['altera'])) {
  $id = $_POST['id'];
  header("location:admin.php?menu=altrelfluxo&id=".$id);
}

if(isset($_POST['volta'])) {
  $idfluxo = $_POST['fluxo'];
  header("location:admin.php?menu=fluxo&id=".$idfluxo."pesquisar=Pesquisar");
}

if(isset($_POST['altcad'])) {
    $id = $_POST['id'];
    $fluxo = $_POST['fluxo'];
    $visadm = implode(",",$_POST['visadm']);
    $status = $_POST['status'];
    $visweb = implode(",",$_POST['visweb']);
    $defini = $_POST['definicao'];
    $respweb = implode(",",$_POST['respweb']);
    $respadm = implode(",",$_POST['respadm']);
    $atustatus = $_POST['atustatus'];
    $ativo = $_POST['ativo'];
    $tipo = $_POST['tipo'];
    $classifica = val_vazio($_POST['classifica']);
    $seltipo = "SELECT COUNT(*) as result FROM rel_fluxo WHERE idrel_fluxo<>'$id' AND idfluxo='$fluxo' AND tipo='inicia'";
    $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die ("erro na consulta do tipo inicia no fluxo");
    $selrel = "SELECT * FROM rel_fluxo WHERE idrel_fluxo='$id'";
    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
    $verifdup = "SELECT COUNT(*) as result FROM rel_fluxo WHERE idstatus='$status' AND iddefinicao='$defini' AND idfluxo='$fluxo' AND idrel_fluxo<>$id";
    $everifdup = $_SESSION['fetch_array']($_SESSION['query']($verifdup)) or die ("erro na query de consulta da duplicidade do relacionamento");
    if($fluxo == "" || $status == "" || $defini == "" || $atustatus == "" || $ativo == "" || $tipo == "") {
      $msgi = "Nenhum campo pode estar vazio para atualizar o relacionamento do fluxo";
      header("location:admin.php?menu=altrelfluxo&id=".$id."&msgi=".$msgi);
    }
    else {
        if($fluxo == $eselrel['idfluxo'] && $_POST['classifica'] == $eselrel['classifica'] && $visadm == $eselrel['vis_adm'] && $status == $eselrel['idstatus'] && $visweb == $eselrel['vis_web'] && $defini == $eselrel['iddefinicao'] && $respweb == $eselrel['resp_web'] && $respadm == $eselrel['resp_adm'] && $atustatus == $eselrel['idatustatus'] && $ativo == $eselrel['ativo'] && $tipo == $eselrel['tipo']) {
          $msgi = "Nenhum campo foi alterado, processo nÃ£o executado!!!";
          header("location:admin.php?menu=altrelfluxo&id=".$id."&msgi=".$msgi);
        }
        else {
            if($eseltipo['result'] >= 1 && $tipo == "inicia") {
                $msgi = "JÃ¡ existe um relacionamento cadastrado como ''Inicio'', nÃ£o podendo existir dois relacionamentos com este ''tipo''!!!";
                header("location:admin.php?menu=altrelfluxo&id=".$id."&msgi=".$msgi);
            }
            else {
                if($everifdup['result'] >= 1) {
                    $msgi = "Existe uma duplicidade na alteraÃ§Ã£o desejada, favor verificar!!!";
                    header("location:admin.php?menu=altrelfluxo&id=".$id."&msgi=".$msgi);
                }
                else {
                    $update = "UPDATE rel_fluxo SET idstatus='$status', iddefinicao='$defini', idatustatus='$atustatus', tipo='$tipo', classifica=$classifica, vis_adm='$visadm', vis_web='$visweb', resp_adm='$respadm',
                              resp_web='$respweb', ativo='$ativo' WHERE idrel_fluxo='$id'";
                    $eupdate = $_SESSION['query']($update) or die ("erro na query de UPDATE");
                    if($eupdate) {
                        $msgi = "Filtro atualizado com sucesso!!!";
                        header("location:admin.php?menu=altrelfluxo&id=".$id."&msgi=".$msgi);
                    }
                    else {
                        $msgi = "Erro no processo de atualizaÃ§Ã£o, favor contatar o administrador";
                        header("location:admin.php?menu=altrelfluxo&id=".$id."&msgi=".$msgi);
                    }
                }
            }
        }
    }
}

?>