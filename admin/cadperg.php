<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$jaberta = $_POST['jaberta'];
$idperg = $_POST['idperg'];
$perg = addslashes(trim($_POST['descri']));
$ativo = $_POST['ativo'];
if($_POST['valor'] == "") {
  $v_perg = "NULL";
}
else {
  $v_perg = str_replace(",", ".", $_POST['valor']);
}
$comple = addslashes(trim($_POST['comple']));
$tresp = $_POST['tipo_resp'];
$modcalc = val_vazio($_POST['modcalc']);
if($_POST['modcalc'] == 'ABS') {
    $percperda = "NULL";
    $maxrep = "NULL";
}
else {
    $percperda = val_vazio(str_replace(",",".",$_POST['percperda']));
    $maxrep = val_vazio(str_replace(",", "", $_POST['maxrep']));
}
$caracter = "[]$[><}{)(:;!?*%&#@]";

if(isset($_POST['cadastra'])) {
    if($perg == "" || $comple == "") {
        $msg = "Todos os campos precisam estar preenchidos para efetuar o cadastro!!!";
        header("location:admin.php?menu=perguntas&msg=".$msg);
    }
    else {
        if(eregi($caracter, $v_perg)) {
            $msg = "No campo valor só pode constar números e virgula!!!";
            header("location:admin.php?menu=perguntas&msg=".$msg);
        }
        else {
            if($v_perg < 0) {
                $msg = "O valor da pergunta nÃ£o pode ser menor que ''0''!!!";
                header("location:admin.php?menu=perguntas&msg=".$msg);
            }
            else {
                if($modcalc == "PERC" && ($percperda == "NULL" OR !is_numeric($_POST['percperda']) OR $maxrep == "NULL" OR !is_numeric($_POST['maxrep']))) {
                    $msg = "Quando o modo de calculo dor percentual, é obrigatório o preenchimento dos campos ''Percentual de Perda'' e ''Max. Repetições''";
                    header("location:admin.php?menu=perguntas&msg=".$msg);
                }
                else {
                    $cadperg = "INSERT INTO pergunta (descripergunta, complepergunta, valor_perg, ativo, tipo_resp,modcalc,percperda,maxrep, avalia, valor_resp, ativo_resp) VALUE ('$perg', '$comple', $v_perg, 'S', '$tresp', $modcalc,$percperda,$maxrep,NULL, NULL, NULL)";
                    $execad = $_SESSION['query']($cadperg) or die (mysql_error());
                    $id = $_SESSION['insert_id']();
                    if($execad) {
                        //$msg = "Cadastro efetuado com sucesso!!!";
                        header("location:admin.php?menu=respostas&id=$id");
                    }
                    else {
                        $msg = "Ocorreu um erro no processo de cadastro da pergunta, favor contatar o administrador!!!";
                        header("location:admin.php?menu=perguntas&msg=".$msg);
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    if($perg == "" || $comple == "") {
        $msgi = "Todos os campos precisam estar preenchidos para que a alteraÃ§Ã£o seja efetuada!!!";
        header("location:admin.php?menu=pergdet&msgi=".$msgi."&id=".$idperg."&jaberta=".$jaberta);
        break;
    }
    else {
        if(eregi($caracter, $v_perg)) {
            $msgi = "No campo de valor só poderá ser digitado números e virtugla!!!";
            header("location:admin.php?menu=pergdet&msgi=".$msgi."&id=".$idperg."&jaberta=".$jaberta);
        }
        else {
            if($v_perg < 0) {
                $msgi = "O valor da pergunta não pode ser menor que ''0''!!!";
                header("location:admin.php?menu=pergdet&msgi=".$msgi."&id=".$idperg."&jaberta=".$jaberta);
            }
            else {
                if($modcalc == "PERC" && ($percperda == "NULL" OR !is_numeric($_POST['percperda']) OR $maxrep == "NULL" OR !is_numeric($_POST['maxrep']))) {
                    $msg = "Quando o modo de calculo dor percentual, é obrigatório o preenchimento dos campos ''Percentual de Perda'' e ''Max. Repetições''";
                    header("location:admin.php?menu=perguntas&msg=".$msg);
                }
                else {
                    $respval = "SELECT MAX(valor_resp) as valor FROM pergunta WHERE idpergunta='$idperg' AND (avalia<>'FG' AND avalia<>'FGM')";
                    $erespval = $_SESSION['fetch_array']($_SESSION['query']($respval)) or die (mysql_error());
                    if($v_perg < $erespval['valor']) {
                        $msgi = "O valor da pergunta nÃ£o pode ser menor que o valor das respostas!!!";
                        header("location:admin.php?menu=pergdet&msgi=".$msgi."&id=".$idperg."&jaberta=".$jaberta);
                    }
                    else {
                        $consult = "SELECT * FROM pergunta WHERE idpergunta='$idperg'";
                        $execons = $_SESSION['fetch_array']($_SESSION['query']($consult)) or die (mysql_error());
                        if($execons['valor_perg'] == "") {
                            $valordb = "NULL";
                        }
                        else {
                            $valordb = $execons['valor_perg'];
                        }
                        if($perg == $execons['descripergunta'] && $comple == $execons['complepergunta'] && $v_perg == $valordb && $ativo == $execons['ativo'] && $tresp == $execons['tipo_resp'] &&
                            $_POST['modcalc'] == $execons['modcalc'] && $_POST['percperda'] == $execons['percperda'] && $_POST['maxrep'] == $execons['maxrep']) {
                            $msgi = "Nenhum campo foi alterado, operação não realizada!!!";
                            header("location:admin.php?menu=pergdet&msgi=".$msgi."&id=".$idperg."&jaberta=".$jaberta);
                        }
                        else {
                            $checksub = "SELECT COUNT(*) as result FROM subgrupo WHERE idpergunta='$idperg'";
                            $echecksub = $_SESSION['fetch_array']($_SESSION['query']($checksub)) or die (mysql_error());
                            $checkgrupo = "SELECT COUNT(*) as result FROM grupo WHERE idrel='$idperg'";
                            $echeckgrupo = $_SESSION['fetch_array']($_SESSION['query']($checkgrupo)) or die (mysql_error());
                            if(($echecksub['result'] >= 1 || $echeckgrupo['result'] >= 1) && $ativo == "N") {
                                $msgi = "A pergunta já está relacionada com um Grupo ou Sub-grpupo, não podendo ser inativada!!!";
                                header("location:admin.php?menu=pergdet&msgi=".$msgi."&id=".$idperg."&jaberta=".$jaberta);
                            }
                            else {
                                $alter = "UPDATE pergunta SET descripergunta='$perg', complepergunta='$comple', valor_perg=$v_perg, ativo='$ativo', tipo_resp='$tresp', modcalc=$modcalc,percperda=$percperda,maxrep=$maxrep WHERE idpergunta='$idperg'";
                                echo $alter;
                                $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                if($exealt) {
                                    header("location:admin.php?menu=respostas&id=".$idperg."&jaberta=".$jaberta);
                                }
                                else {
                                    $msgi = "Ocorreu algum erro no processo de alteração, favor contatar o administrador!!!";
                                    header("location:admin.php?menu=pergdet&msgi=".$msgi);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['alteraperg'])) {
    $idperg = $_POST['idperg'];
    header("location:admin.php?menu=respostas&id=".$idperg."&jaberta=".$_POST['jaberta']);
}

if(isset($_POST['volta'])) {
  header("location:admin.php?menu=perguntas");
}
?>