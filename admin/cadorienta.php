<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");

$iduser = $_SESSION['usuarioID'];
$nomeuser = $_SESSION['usuarioNome'];

$mimes = array("323" => "text/h323",
"acx" => "application/internet-property-stream",
"ai" => "application/postscript",
"aif" => "audio/x-aiff",
"aifc" => "audio/x-aiff",
"aiff" => "audio/x-aiff",
"asf" => "video/x-ms-asf",
"asr" => "video/x-ms-asf",
"asx" => "video/x-ms-asf",
"au" => "audio/basic",
"avi" => "video/x-msvideo",
"axs" => "application/olescript",
"bas" => "text/plain",
"bcpio" => "application/x-bcpio",
"bin" => "application/octet-stream",
"bmp" => "image/bmp",
"c" => "text/plain",
"cat" => "application/vnd.ms-pkiseccat",
"cdf" => "application/x-cdf",
"cer" => "application/x-x509-ca-cert",
"class" => "application/octet-stream",
"clp" => "application/x-msclip",
"cmx" => "image/x-cmx",
"cod" => "image/cis-cod",
"cpio" => "application/x-cpio",
"crd" => "application/x-mscardfile",
"crl" => "application/pkix-crl",
"crt" => "application/x-x509-ca-cert",
"csh" => "application/x-csh",
"css" => "text/css",
"dcr" => "application/x-director",
"der" => "application/x-x509-ca-cert",
"dir" => "application/x-director",
"dll" => "application/x-msdownload",
"dms" => "application/octet-stream",
"doc" => "application/msword",
"dot" => "application/msword",
"dvi" => "application/x-dvi",
"dxr" => "application/x-director",
"eps" => "application/postscript",
"etx" => "text/x-setext",
"evy" => "application/envoy",
"exe" => "application/octet-stream",
"fif" => "application/fractals",
"flr" => "x-world/x-vrml",
"gif" => "image/gif",
"gtar" => "application/x-gtar",
"gz" => "application/x-gzip",
"h" => "text/plain",
"hdf" => "application/x-hdf",
"hlp" => "application/winhlp",
"hqx" => "application/mac-binhex40",
"hta" => "application/hta",
"htc" => "text/x-component",
"htm" => "text/html",
"html" => "text/html",
"htt" => "text/webviewhtml",
"ico" => "image/x-icon",
"ief" => "image/ief",
"iii" => "application/x-iphone",
"ins" => "application/x-internet-signup",
"isp" => "application/x-internet-signup",
"jfif" => "image/pipeg",
"jpe" => "image/jpeg",
"jpeg" => "image/jpeg",
"jpg" => "image/jpeg",
"js" => "application/x-javascript",
"latex" => "application/x-latex",
"lha" => "application/octet-stream",
"lsf" => "video/x-la-asf",
"lsx" => "video/x-la-asf",
"lzh" => "application/octet-stream",
"m13" => "application/x-msmediaview",
"m14" => "application/x-msmediaview",
"m3u" => "audio/x-mpegurl",
"man" => "application/x-troff-man",
"mdb" => "application/x-msaccess",
"me" => "application/x-troff-me",
"mht" => "message/rfc822",
"mhtml" => "message/rfc822",
"mid" => "audio/mid",
"mny" => "application/x-msmoney",
"mov" => "video/quicktime",
"movie" => "video/x-sgi-movie",
"mp2" => "video/mpeg",
"mp3" => "audio/mpeg",
"mpa" => "video/mpeg",
"mpe" => "video/mpeg",
"mpeg" => "video/mpeg",
"mpg" => "video/mpeg",
"mpp" => "application/vnd.ms-project",
"mpv2" => "video/mpeg",
"ms" => "application/x-troff-ms",
"mvb" => "application/x-msmediaview",
"nws" => "message/rfc822",
"oda" => "application/oda",
"p10" => "application/pkcs10",
"p12" => "application/x-pkcs12",
"p7b" => "application/x-pkcs7-certificates",
"p7c" => "application/x-pkcs7-mime",
"p7m" => "application/x-pkcs7-mime",
"p7r" => "application/x-pkcs7-certreqresp",
"p7s" => "application/x-pkcs7-signature",
"pbm" => "image/x-portable-bitmap",
"pdf" => "application/pdf",
"pfx" => "application/x-pkcs12",
"pgm" => "image/x-portable-graymap",
"pko" => "application/ynd.ms-pkipko",
"pma" => "application/x-perfmon",
"pmc" => "application/x-perfmon",
"pml" => "application/x-perfmon",
"pmr" => "application/x-perfmon",
"pmw" => "application/x-perfmon",
"pnm" => "image/x-portable-anymap",
"pot" => "application/vnd.ms-powerpoint",
"ppm" => "image/x-portable-pixmap",
"pps" => "application/vnd.ms-powerpoint",
"ppt" => "application/vnd.ms-powerpoint",
"prf" => "application/pics-rules",
"ps" => "application/postscript",
"pub" => "application/x-mspublisher",
"qt" => "video/quicktime",
"ra" => "audio/x-pn-realaudio",
"ram" => "audio/x-pn-realaudio",
"ras" => "image/x-cmu-raster",
"rgb" => "image/x-rgb",
"rmi" => "audio/mid",
"roff" => "application/x-troff",
"rtf" => "application/rtf",
"rtx" => "text/richtext",
"scd" => "application/x-msschedule",
"sct" => "text/scriptlet",
"setpay" => "application/set-payment-initiation",
"setreg" => "application/set-registration-initiation",
"sh" => "application/x-sh",
"shar" => "application/x-shar",
"sit" => "application/x-stuffit",
"snd" => "audio/basic",
"spc" => "application/x-pkcs7-certificates",
"spl" => "application/futuresplash",
"src" => "application/x-wais-source",
"sst" => "application/vnd.ms-pkicertstore",
"stl" => "application/vnd.ms-pkistl",
"stm" => "text/html",
"svg" => "image/svg+xml",
"sv4cpio" => "application/x-sv4cpio",
"sv4crc" => "application/x-sv4crc",
"t" => "application/x-troff",
"tar" => "application/x-tar",
"tcl" => "application/x-tcl",
"tex" => "application/x-tex",
"texi" => "application/x-texinfo",
"texinfo" => "application/x-texinfo",
"tgz" => "application/x-compressed",
"tif" => "image/tiff",
"tiff" => "image/tiff",
"tr" => "application/x-troff",
"trm" => "application/x-msterminal",
"tsv" => "text/tab-separated-values",
"txt" => "text/plain",
"uls" => "text/iuls",
"ustar" => "application/x-ustar",
"vcf" => "text/x-vcard",
"vrml" => "x-world/x-vrml",
"wav" => "audio/x-wav",
"wcm" => "application/vnd.ms-works",
"wdb" => "application/vnd.ms-works",
"wks" => "application/vnd.ms-works",
"wmf" => "application/x-msmetafile",
"wps" => "application/vnd.ms-works",
"wri" => "application/x-mswrite",
"wrl" => "x-world/x-vrml",
"wrz" => "x-world/x-vrml",
"xaf" => "x-world/x-vrml",
"xbm" => "image/x-xbitmap",
"xla" => "application/vnd.ms-excel",
"xlc" => "application/vnd.ms-excel",
"xlm" => "application/vnd.ms-excel",
"xls" => "application/vnd.ms-excel",
"xlt" => "application/vnd.ms-excel",
"xlw" => "application/vnd.ms-excel",
"xof" => "x-world/x-vrml",
"xpm" => "image/x-xpixmap",
"xwd" => "image/x-xwindowdump",
"z" => "application/x-compress",
"zip" => "application/zip");

$seltipo = "SELECT tipoarq, tamanho, camarq, count(*) as result FROM param_book";
$eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
$tipoarqdb = explode(";",$eseltipo['tipoarq']);
foreach($tipoarqdb as $arq) {
  if(array_key_exists($arq, $mimes)) {
    $aceitos["$arq"] = $mimes[$arq];
  }
  else {
  }
}

if(isset($_POST['cadastra'])) {
  if($eseltipo['result'] == "0") {
      $msg = "É necessário efetuar o cadastro dos parametros do E-BOOK!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
  }
  else {
    $selparam = "SELECT * FROM param_book";
    $eparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
    $caminho = $eparam['camarq'];
    $datarec = data2banco($_POST['datarec']);
    $dtrec_ano = substr($datarec,0,4);
    $dtrec_mes = substr($datarec,5,2);
    $dtrec_dia = substr($datarec,8,2);
    $dtrec = mktime(0,0,0,$dtrec_mes, $dtrec_dia, $dtrec_ano);
    $userenv = $_POST['cliente'];
    $data = date('Y-m-d');
    $dtatu_ano = substr($data,0,4);
    $dtatu_mes = substr($data,5,2);
    $dtatu_dia = substr($data,8,2);
    $dtatu = mktime(0,0,0,$dtatu_mes, $dtatu_dia, $dtatu_ano);
    $hora = date('H:i:s');
    $tipo = $_POST['tipo'];
    $assunto = $_POST['assunto'];
    $subassunto = $_POST['subassunto'];
    $arquivo = $_FILES['arquivo']['tmp_name'];
    $tamanhoarq = $_FILES['arquivo']['size'];
    $nomearq = $_FILES['arquivo']['name'];
    $tipoarq = $_FILES['arquivo']['type'];
    $titulo = $_POST['titulo'];
    $palavra = $_POST['palavras'];
    $descri = $_POST['descri'];
    $proced = $_POST['proced'];
    if(strlen($descri) > 1000) {
        $msg = "O campo ''descrição'' não pode ter mais que 1000 caracteres!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
    }
    if(strlen($proced) > 1000) {
        $msg = "O campo ''procedimento'' nÃ£o pode ter mais que 1000 caracteres!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
    }
    $selorienta = "SELECT COUNT(*) as result FROM ebook WHERE categoria='$assunto' AND subcategoria='$subassunto' AND titulo='$titulo'";
    $eselorienta = $_SESSION['fetch_array']($_SESSION['query']($selorienta)) or die (mysql_error());
    if($titulo == "" || $assunto == "" || $palavra == "" || $subassunto == "" || $datarec == "") {
        $msg = "Nenhum dos campos''data recebimento, titulo, assunto, palavra chave ou sub-assunto'' pode estar vazio!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
    }
    if($dtrec > $dtatu) {
        $msg = "A data do recebimento da orientaÃ§Ã£o nÃ£o pode ser maior que a data atual!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
    }
    if($eselorienta['result'] >= 1) {
        $msg = "Esta orientaÃ§Ã£o jÃ¡ estÃ¡ cadastrada no sistema!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
    }
    else {
        if($tipo == "ARQUIVO") {
            if($arquivo == "") {
                $msg = "Quando o tipo for ''arquivo'' é necessário informar um arquivo para enviar ao servidor!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            if($descri != "" || $proced != "") {
                $msg = "Quando o tipo for ''arquivo'' os campos ''DescriÃ§Ã£o e Procedimento'' devem ficar vazios!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            if($tamanhoarq > $eseltipo['tamanho']) {
                $msg = "O tamanho do arquivo Ã© maior do que o permitido!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            if(!in_array($tipoarq, $aceitos)) {
                $msg = "A extensÃ£o do arquivo nÃ£o estÃ¡ permitida para upload!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            else {
                if(!in_array($tipoarq, $aceitos)) {
                    $msg = "A extesÃ£o do arquivo nÃ£o estÃ¡ autorizada para upload no servidor!!!";
                    header("location:admin.php?menu=orienta&msg=".$msg);
                }
                else {
                    $caminhoup = $rais.$caminho."/".$nomearq;
                    move_uploaded_file($arquivo, $caminhoup);
                    $cadorienta = "INSERT INTO ebook (arquivo, titulo, palavra, datareceb, iduser_env, datacad, horacad, dataalt, horaalt, tporienta, tipo, categoria, subcategoria, ativo, iduser, tabuser) VALUE ('$caminhoup', '$titulo', '$palavra', '$datarec', '$userenv', '$data', '$hora', '$data', '$hora', '$tipo', '$tipoarq', '$assunto', '$subassunto', 'S', '$iduser','".$_SESSION['user_tabela']."')";
                    $ecad = $_SESSION['query']($cadorienta) or die (mysql_error());
                    if($ecad) {
                        $msg = "OrientaÃ§Ã£o cadastrada com sucesso!!!";
                        header("location:admin.php?menu=orienta&msg=".$msg);
                    }
                    else {
                        $msg = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                        header("location:admin.php?menu=orienta&msg=".$msg);
                    }
                }
            }
        }
        if($tipo == "TEXTO") {
            if($arquivo != "") {
                $msg = "Quando o tipo for ''texto'' o campos ''Arquivo'' deve ficar vazio!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            if($descri == "") {
                $msg = "Quando o tipo for ''texto'' o campo ''DescriÃ§Ã£o'' nÃ£o pode estar vazio!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            else {
                $cadorienta = "INSERT INTO ebook (titulo, palavra, descri, procedimento, datareceb, iduser_env, datacad, horacad, dataalt, horaalt, tporienta, categoria, subcategoria, ativo, iduser,tabuser) VALUE ('$titulo', '$palavra', '$descri', '$proced', '$datarec', '$userenv', '$data', '$hora', '$data', '$hora', '$tipo', '$assunto', '$subassunto', 'S', '$iduser','".$_SESSION['user_tabela']."')";
                $ecad = $_SESSION['query']($cadorienta) or die (mysql_error());
                if($ecad) {
                    $msg = "OrientaÃ§Ã£o cadastrada com sucesso!!!";
                    header("location:admin.php?menu=orienta&msg=".$msg);
                }
                else {
                    $msg = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                    header("location:admin.php?menu=orienta&msg=".$msg);
                }
            }
        }
    }
  }
}

if(isset($_POST['alteracad'])) {
  $id = $_POST['id'];
  header("location:admin.php?menu=altorienta&id=".$id);
  break;
}

if(isset($_POST['volta'])) {
  header("location:admin.php?menu=orienta");
  break;
}

if(isset($_POST['apaga'])) {
  $id = $_POST['id'];
  $seltipo = "SELECT tporienta,arquivo FROM ebook WHERE idebook='$id'";
  $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
  if($eseltipo['tporienta'] == "ARQUIVO") {
    $caminho = $eseltipo['arquivo'];
    $delarq = unlink($caminho);
  }
  if($eseltipo['tporienta'] == "TEXTO") {
  }
  $delorienta = "DELETE FROM ebook WHERE idebook='$id'";
  $edel = $_SESSION['query']($delorienta) or die (mysql_error());
  $alter = "ALTER TABLE ebook AUTO_INCREMENT=1";
  $ealter = $_SESSION['query']($alter) or die (mysql_error());
  if($edel) {
    $msg = "OrientaÃ§Ã£o apagada com sucesso!!!";
    header("location:admin.php?menu=orienta&msg=".$msg);
    break;
  }
  else {
    $msg = "Ocorreu um erro no processo solicitado, favor contatar o administrador!!!";
    header("location:admin.php?menu=orienta&msg=".$msg);
    break;
  }
}

if(isset($_POST['altera'])) {
  if($eseltipo['result'] == "0") {
      $msg = "É necessário efetuar o cadastro dos parametros do E-BOOK!!!";
        header("location:admin.php?menu=orienta&msg=".$msg);
        break;
  }
  else {
    $id = $_POST['id'];
    $datarec = data2banco($_POST['datarec']);
    $dataalt = date('Y-m-d');
    $horaalt = date('H:i:s');
    $ativo = $_POST['ativo'];
    $tipo = $_POST['tipo'];
    $userenv = $_POST['userenv'];
    $assunto = $_POST['assunto'];
    $subassunto = $_POST['subassunto'];
    $titulo = $_POST['titulo'];
    $palavra = $_POST['palavras'];
    $descri = $_POST['descri'];
    $proced = $_POST['proced'];
    $arquivo = $_FILES['arquivo']['tmp_name'];
    $tamanhoarq = $_FILES['arquivo']['size'];
    $nomearq = $_FILES['arquivo']['name'];
    $tipoarq = $_FILES['arquivo']['type'];
    $selorienta = "SELECT * FROM ebook WHERE idebook='$id'";
    $eorienta = $_SESSION['fetch_array']($_SESSION['query']($selorienta)) or die (mysql_error());
    if($tipo == "TEXTO") {
        if($titulo == $eorienta['titulo'] && $palavra == $eorienta['palavra'] && $assunto == $eorienta['categoria'] && 
            $subassunto == $eorienta['subcategoria'] && $descri == $eorienta['descri'] && $proced == $eorienta['procedimento'] && 
            $ativo == $eorienta['ativo'] && $userenv == $eorienta['iduser_env']) {
            $msg = "Nenhum campo foi alterado, processo nÃ£o executado!!!";
            header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
            break;
        }
        if($titulo == "" || $descri == "") {
            $msg = "Os campos ''Titulo, Descrição ou Procedimento não podem estar vazios!!!";
            header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
            break;
        }
        else {
            $update = "UPDATE ebook SET dataalt='$dataalt', horaalt='$horaalt', iduser_env='$userenv', titulo='$titulo', palavra='$palavra', descri='$descri', procedimento=".val_vazio($proced).", categoria='$assunto', subcategoria='$subassunto', ativo='$ativo' WHERE idebook='$id'";
            $eupdate = $_SESSION['query']($update) or die (mysql_error());
            if($eupdate) {
                $msg = "AlteraÃ§Ã£o realizada com sucesso!!!";
                header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                break;
            }
            else {
                $msg = "Ocorreu um erro no processo de alteraÃ§Ã£o, favor contatar o administrador!!!";
                header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                break;
            }
        }
    }
    if($tipo == "ARQUIVO") {
        $nomedb = explode("/",$eorienta['arquivo']);
        $nomedb = end($nomedb);
        if($nomearq != "") {
            if($titulo == "") {
                $msg = "Os campos ''Titulo ou Arquivo não podem estar vazios!!!''";
                header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                break;
            }
            if($tamanhoarq > $eseltipo['tamanho']) {
                $msg = "O tamanho do arquivo Ã© maior do que o permitido!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            if(!in_array($tipoarq, $aceitos) && $nomearq != "") {
                $msg = "A extenção do arquivo nÃ£o está permitida para upload!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            else {
                $caminhodel = $eorienta['arquivo'];
                $apaga = unlink($caminhodel);
                if($apaga) {
                    $caminho = $eseltipo['camarq'];
                    $caminhoup = $rais.$caminho."/".$nomearq;
                    move_uploaded_file($arquivo, $caminhoup);
                    $update = "UPDATE ebook SET dataalt='$dataalt', horaalt='$horaalt', iduser_env='$userenv', titulo='$titulo', palavra='$palavra', arquivo='$caminhoup', categoria='$assunto', subcategoria='$subassunto', ativo='$ativo' WHERE idebook='$id'";
                    $eupdate = $_SESSION['query']($update) or die (mysql_error());
                    if($eupdate) {
                        $msg = "AlteraÃ§Ã£o realizada com sucesso!!!";
                        header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                        break;
                    }
                    else {
                        $msg = "Ocorreu um erro no processo de alteração, favor contatar o administrador!!!";
                        header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                        break;
                    }
                }
                else {
                    $msg = "Erro no processo para apagar o arquivo anterior, o mesmo não existe, favor contatar o administrador!!!";
                    header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                    break;
                }
            }
        }
        else {
            if($titulo == $eorienta['titulo'] && $palavra == $eorienta['palavra'] && $assunto == $eorienta['categoria'] && $subassunto == $eorienta['subcategoria'] && $ativo == $eorienta['ativo'] && $userenv == $eorienta['iduser_env']) {
                $msg = "Nenhum campo foi alterado, processo nÃ£o executado!!!";
                header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                break;
            }
            if($titulo == "") {
                $msg = "Os campos ''Titulo ou Arquivo não podem estar vazios!!!''";
                header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                break;
            }
            if($tamanhoarq > $eseltipo['tamanho']) {
                $msg = "O tamanho do arquivo Ã© maior do que o permitido!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            if(!in_array($tipoarq, $aceitos) && $nomearq != "") {
                $msg = "A extenção do arquivo nÃ£o está permitida para upload!!!";
                header("location:admin.php?menu=orienta&msg=".$msg);
                break;
            }
            else {
                $update = "UPDATE ebook SET dataalt='$dataalt', horaalt='$horaalt', iduser_env='$userenv', titulo='$titulo', palavra='$palavra', categoria='$assunto', subcategoria='$subassunto', ativo='$ativo' WHERE idebook='$id'";
                $eupdate = $_SESSION['query']($update) or die (mysql_error());
                if($eupdate) {
                    $msg = "Alteração realizada com sucesso!!!";
                    header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                    break;
                }
                else {
                    $msg = "Ocorreu um erro no processo de alteração, favor contatar o administrador!!!";
                    header("location:admin.php?menu=altorienta&id=".$id."&msg=".$msg);
                    break;
                }
            }
        }
    }
  }
}


?>