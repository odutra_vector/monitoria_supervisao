<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");
$id = $_GET['id'];
$columns = "SHOW COLUMNS FROM ebook";
$ecolumns = $_SESSION['query']($columns) or die (mysql_error());
while($lcolumns = $_SESSION['fetch_array']($ecolumns)) {
    $colebook[] = "e.".$lcolumns['Field'];
}
$selorienta = "SELECT ".implode(",",$colebook)." FROM ebook e
                        LEFT JOIN user_web ub ON iduser_web = e.iduser_env
                        WHERE idebook='$id'";
$eorienta = $_SESSION['fetch_array']($_SESSION['query']($selorienta)) or die (mysql_error());
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.4.4.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tabela tr.conteudo').hide();
    $('#tabela tr.descri').hide();
    $('#tabela tr.proced').hide();
    $("#tipo_1").click(function(){
        $('#tabela tr.conteudo').hide();
        $('#tabela tr.descri').show();
        $('#tabela tr.proced').show();
    })
    $("#tipo_0").click(function() {
        $('#tabela tr.conteudo').show();
        $('#tabela tr.descri').hide();
        $('#tabela tr.proced').hide();
    })
})

jQuery(function($){
   $("#datarec").mask("99/99/9999");
				});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="cadorienta.php" method="post" enctype="multipart/form-data">
  <table width="846" id="tabela">
  <tr>
    <td class="corfd_coltexto" colspan="2" align="center"><strong>ALTERAÇÃO DE ORIENTAÇÃO</strong></td>
  </tr>
  <tr>
    <td width="136" bgcolor="#999999"><strong>ID</strong></td>
    <td><input style="width:70px; border: 1px solid #333; text-align:center" name="id" readonly="readonly" value="<?php echo $id; ?>" /></td>
  </tr>
  <tr>
    <td width="136" bgcolor="#999999"><strong>Data Recebimento</strong></td>
    <td><input style="width:80px; border: 1px solid #333; text-align:center" name="datarec" id="datarec" readonly="readonly" value="<?php echo banco2data($eorienta['datareceb']); ?>" /></td>
  </tr>
  <tr>
  	<td width="136" bgcolor="#999999"><strong>Ativo</strong></td>
    <td>
      <label><input type="radio" name="ativo" value="S" id="ativo_0" <?php if($eorienta['ativo'] == "S") { echo "checked=\"checked\""; } else {}?> /><strong>SIM</strong></label>
      <label><input type="radio" name="ativo" value="N" id="ativo_1" <?php if($eorienta['ativo'] == "N") { echo "checked=\"checked\""; } else {}?> /><strong>NÃO</strong></label>
    </td>
    </tr>
    <tr>
    <td width="136" bgcolor="#999999"><strong>Tipo</strong></td>
    <td width="710">
            <label><input type="radio" name="tipo" value="ARQUIVO" readonly="readonly" id="tipo_0" <?php if($eorienta['tporienta'] == "ARQUIVO") { echo "checked=\"checked\""; } else {} ?> /><strong>ARQUIVO</strong></label>
            <label><input type="radio" name="tipo" value="TEXTO" readonly="readonly" id="tipo_1" <?php if($eorienta['tporienta'] == "TEXTO") { echo "checked=\"checked\""; } else {} ?> /><strong>TEXTO</strong></label>
    </td>
  </tr>
  <tr>
    <td width="136" bgcolor="#999999"><strong>Enviado Por</strong></td>
    <td>
        <select name="userenv">
        <?php
        if($eorienta['iduser_env'] == "SEM USUARIO") {
            echo "<option selected=\"selected\" value=\"SEM USUARIO\">SEM USUÁRIO</option>";
        }
        else {
            echo "<option value=\"SEM USUARIO\">SEM USUÁRIO</option>";
        }
        $userenvdb = $eorienta['iduser_env'];
        $seluser = "SELECT nomeuser_web, iduser_web FROM user_web";
        $eseluser = $_SESSION['query']($seluser) or die (mysql_error());
        while($lseluser = $_SESSION['fetch_array']($eseluser)) {
            if($lseluser['iduser_web'] == $eorienta['iduser_env']) {
                echo "<option selected=\"selected\" value=\"".$lseluser['iduser_web']."\">".$lseluser['nomeuser_web']."</option>";
            }
            else {
                echo "<option value=\"".$lseluser['iduser_web']."\">".$lseluser['nomeuser_web']."</option>";
            }
        }
        ?>
        </select>
    </td>
  </tr>
  <tr>
    <td width="136" bgcolor="#999999"><strong>Assunto</strong></td>
    <td width="710"><select style="width:200px; border: 1px solid #333" name="assunto">
    <?php
    $selassunto = "SELECT * FROM cat_ebook";
    $eselassunto = $_SESSION['query']($selassunto) or die (mysql_error());
    while($lassunto = $_SESSION['fetch_array']($eselassunto)) {
        echo "<option value=\"".$lassunto['nomecat_ebook']."\">".$lassunto['nomecat_ebook']."</option>";
    }
    ?>
    </select></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><strong>Sub-Assunto</strong></td>
    <td><select style="width:200px; border: 1px solid #333" name="subassunto">
    <?php
    $selsubassunto = "SELECT * FROM subebook";
    $esubassunto = $_SESSION['query']($selsubassunto) or die (mysql_error());
    while($lsubassunto = $_SESSION['fetch_array']($esubassunto)) {
        echo "<option value=\"".$lsubassunto['nomesubebook']."\">".$lsubassunto['nomesubebook']."</option>";
    }
    ?>
    </select></td>
  </tr>
  <tr>
    <td bgcolor="#999999"><strong>Titulo</strong></td>
    <td><input style="width:400px; border: 1px solid #333" name="titulo" type="text" value="<?php echo $eorienta['titulo'];?>" /></td>
  </tr>
  <tr>
  	<td bgcolor="#999999"><strong>Palavras Chave</strong></td>
    <td><textarea style="width:700px; height:40px;" name="palavras"><?php echo $eorienta['palavra']; ?></textarea></td>
  </tr>
  <?php
  if($eorienta['tporienta'] == "ARQUIVO") {
        echo "<tr>";
        echo "<td bgcolor=\"#999999\"><strong>Arquivo Atual</strong></td>";
        echo "<td><a href=\"".str_replace($rais,"",$eorienta['arquivo'])."\" target=\"_blank\">".$eorienta['titulo']."</a></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td bgcolor=\"#999999\"><strong>Novo Arquivo</strong></td>";
        echo "<td><input type=\"file\" name=\"arquivo\"/></td>";
        echo "</tr>";
  }
  if($eorienta['tporienta'] == "TEXTO") {
        echo "<tr>";
        echo "<td bgcolor=\"#999999\"><strong>Descrição</strong></td>";
        echo "<td><textarea style=\"width:700px; height:200px;\"  name=\"descri\">".$eorienta['descri']."</textarea></td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td bgcolor=\"#999999\"><strong>Procedimento</strong></td>";
        echo "<td><textarea style=\"width:700px; height:200px;\"  name=\"proced\">".$eorienta['procedimento']."</textarea></td>";
        echo "</tr>";
  }
  ?>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)"  name="altera" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)"  name="volta" type="submit" value="Voltar" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font><br /><br />
</div>
</body>
</html>