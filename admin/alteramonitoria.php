<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Alterar Monitoria</title>
<?php

session_start();

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.monitoria.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$idmoni = $_GET['idmoni'];
$selmoni = "SELECT * FROM monitoria WHERE idmonitoria='$idmoni'";
$eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta da monitoria");
$cor = new CoresSistema();
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);

?>
<link href="../stylemonitoria.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#alteramoni').click(function() {
                $.blockUI({ message: '<strong>AGUARDE ANALISANDO MONITORIA...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }
                })
                var lenidsplan = $("[name*='idsplan']").serialize();
                var arrayids = lenidsplan.split("&");
                var tamanho = arrayids.length;
                var idsplan = new Array();
                var i = 0;
                for(i = 0; i < tamanho; i++) {
                    var limpa = arrayids[i].replace("%5B%5D","");
                    idsplan[i] = limpa;
                }
                var lenidsmoni = $("[name*='idsmoni']").serialize();
                var arrayidsm = lenidsmoni.split("&");
                var tamanho = arrayidsm.length;
                var idsmoni = new Array();
                var i = 0;
                for(i = 0; i < tamanho; i++) {
                    var limpa = arrayidsm[i].replace("%5B%5D","");
                    idsmoni[i] = limpa;
                }
                var vars = $("#tabmoni").serialize("id");
                vars = decodeURIComponent(vars.replace('/\][()*+-$#&@!?|',""));

                $.post("/monitoria_supervisao/exemoni.php", {vars: vars, idsplan: idsplan,idsmoni:idsmoni,alteramoni:'alteramoni'}, function(valor) {
                    var dados = valor.split("*");
                    var retorno = dados[0];
                    var msg = dados[1];
                    if(retorno >= 1) {
                        $.unblockUI();
                        $.blockUI({ message: '<strong>'+msg+'</strong>', css: { 
                            border: 'none', 
                            padding: '15px', 
                            backgroundColor: '#000', 
                            '-webkit-border-radius': '10px', 
                            '-moz-border-radius': '10px',
                            color: '#fff'
                            }
                        })
                        setTimeout($.unblockUI,4000);
                        return false;
                    }
                    else {
                        //var direciona = dados[2];
                        alert(msg);
                        $.unblockUI();
                        window.location.reload()
                        //window.location = direciona;
                    }
                });
            })
        })
</script>
</head>
<body>
    <div id="tudo" style="font-family: arial ;font-size: 11px">
        <div id="conteudo" class="corfd_pag">
        <?php
            $moni = new Planilha();
            $moni->alteramoni = "S";
            $moni->iduser = $_SESSION['usuarioID'];
            $moni->perfiluser = $_SESSION['usuarioperfil'];
            $moni->tabuser = $_SESSION['user_tabela'];
            $moni->idrel = $eselmoni['idrel_filtros'];
            $moni->pagina = "MONITORIA";
            $moni->DadosPlanVisualiza();
            $moni->MontaPlan_corpovisu();
         ?>
         <!--<script type="text/javascript" src="/monitoria_supervisao/delaudiotmp.php?caminho=<?php //echo $moni->caminho;?>"></script>-->
         <table width="1024">
          <tr>
            <td align="center"><input style="border:1px solid #FFF; width:65px; height:17px; background-image:url(../images/button.jpg); text-align:center" name="alteramoni" id="alteramoni" type="button" value="ALTERA" /></td>
          </tr>
        </table>

        </div>
        <div id="rodape">
            <table align="center" width="344" border="0">
                <tr>
                    <td align="center" style="color:#FFF"><strong>VECTOR DSI TEC</strong></td>
                </tr>
                <tr>
                    <td height="15" align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
