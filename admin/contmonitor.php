<?php

$idmonitor = $_GET['idmonitor'];
$idsuper = $_GET['idsuper'];
if($_GET['idmonitor'] != "" OR $_GET['idsuper'] != "") {
    if($_GET['idmonitor'] != "") {
        $vars[] = "idmonitor='".$idmonitor."'";
    }
    if($_GET['idsuper'] != "") {
        $vars[] = "iduser_adm='".$idsuper."'";
    }
    $wsqlmoni = "WHERE m.ativo='S' AND ".implode(" AND ", $vars);
}
else {
    $wsqlmoni = "WHERE m.ativo='S'";
}
$selfiltro = "SELECT *, MIN(nivel) as minfiltro FROM filtro_nomes";
$efiltro = $_SESSION['fetch_array']($_SESSION['query']($selfiltro)) or die ("erro na consulta do menor filtro cadastrado");
$filtro = strtolower($efiltro['nomefiltro_nomes']);

?>

<link href="/monitoria_supervisao/styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	$(document).ready(function() {
		$('#tabmoni').tablesorter();
		$('#tabaval').tablesorter();
	})

        function libera (URL){
            //pega a resolução do visitante
            // Definindo meio da tela
            var esquerda = (screen.width - 800)/2;
            var topo = (screen.height - 500)/2;

           window.open(URL,"libera",'height=500, width=800, top='+topo+', left='+esquerda+',scrollbars=YES,resizable=NO,Menubar=0,Status=0,Toolbar=0');
        }
        
        window.setInterval(function() {
            window.location.reload();
        }, 100000);
</script>
<div>
    <div style="width:1024px; height:334px; border-bottom: 2px solid #F00">
    <form action="" method="get" id="formsuper">
    <table width="498">
      <tr>
        <td width="93" class="corfd_coltexto"><strong>SUPERVISOR</strong></td>
        <td width="269" class="corfd_colcampos">
            <input type="hidden" name="menu" value="contmonitor" />
            <select name="idsuper" id="supervisor" style="width:300px">
                <option value="">SELECIONE...</option>
                <?php
                $selsuper = "SELECT * FROM user_adm WHERE ativo='S'";
                $eselsuper = $_SESSION['query']($selsuper) or die ("erro na query de consulta dos usuários");
                while($lselsuper = $_SESSION['fetch_array']($eselsuper)) {
                    if($lselsuper['iduser_adm'] == $idsuper) {
                        echo "<option value=\"".$lselsuper['iduser_adm']."\" selected=\"selected\">".$lselsuper['nomeuser_adm']."</option>";
                    }
                    else {
                        echo "<option value=\"".$lselsuper['iduser_adm']."\">".$lselsuper['nomeuser_adm']."</option>";
                    }
                }
                ?>
            </select>
        </td>
        <td width="120"><input style="border: 1px solid #fff; height: 18px; background-image:url(../monitoria_supervisao/images/button.jpg)" readonly="readonly" name="pessuper" type="submit" value="OK" /></td>
      </tr>
    </table>
    </form>
    	<div style="width:600px; height:300px; float:left; border: 2px solid #999; overflow:auto" id="status">
          <table width="585">
              <tr>
                <td align="center" class="corfd_ntab"><strong>STATUS MONITORES</strong></td>
              </tr>
          </table>
          <table width="585" id="tabmoni">
              <thead>
                <th width="230" class="corfd_coltexto" align="left"><strong>MONITOR</strong></th>
                <th width="130" align="center" class="corfd_coltexto"><strong>HORA TRAB.</strong></th>
                <th width="50" align="center" class="corfd_coltexto"><strong>TEMPO</strong></th>
                <th width="158" align="center" class="corfd_coltexto"><strong>STATUS</strong></th>
              </thead>
              <tbody>
              <?php
              $datas = periodo(datas);
              $selmoni = "SELECT * FROM monitor m INNER JOIN user_adm ua ON ua.iduser_adm = m.iduser_resp $wsqlmoni ORDER BY nomemonitor";
              $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos monitores");
              while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                  $selpausas = "SELECT COUNT(*) as result FROM moni_pausa WHERE data BETWEEN '".$datas['dataini']."' AND '".$datas['datafim']."' AND horafim='00:00:00' AND idmonitor='".$lselmoni['idmonitor']."'";
                  $eselpausas = $_SESSION['fetch_array']($_SESSION['query']($selpausas)) or die ("erro na query de contagem das pausas inconsistentes");
                  if($eselpausas['result'] >= 1 AND $lselmoni['status'] != "PAUSA") {
                      $color = "bgcolor=\"#B87272\"";
                      $status = "<a href=\"#\" onclick=\"javascript:libera('/monitoria_supervisao/admin/libera.php?idmonitor=".$lselmoni['idmonitor']."');\"  title=\"PAUSAS INCONSISTENTES\">INCONSISTENTE</a>";
                  }
                  else {
                      if($lselmoni['status'] == "PAUSA") {
                          $confpausa = "SELECT mp.horaini, m.tempo, mp.horafim, mp.idmoni_pausa, vincula,nomemotivo FROM moni_pausa mp
                                        INNER JOIN motivo m ON m.idmotivo = mp.idmotivo
                                        INNER JOIN tipo_motivo tm ON tm.idtipo_motivo = m.idtipo_motivo
                                        WHERE mp.idmonitor='".$lselmoni['idmonitor']."' ORDER BY mp.idmoni_pausa DESC, data DESC LIMIT 1";
                          $econf = $_SESSION['fetch_array']($_SESSION['query']($confpausa)) or die ("erro na query de consulta do status do monitor");
                          $hrini = $econf['horaini'];
                          $hora = date("H:i:s");
                          $tempo = $econf['tempo'];
                          $conftempo = "SELECT (TIME_TO_SEC('$hora') - TIME_TO_SEC('$hrini')) as tempo";
                          $ectempo = $_SESSION['fetch_array']($_SESSION['query']($conftempo)) or die ("erro na query de consulta do tempo em pausa");
                          if($ectempo['tempo'] > $tempo || ($econf['vincula'] == "automatico")) {
                              $color = "bgcolor=\"#B87272\"";
                              $status = "<a href=\"#\" onclick=\"javascript:libera('/monitoria_supervisao/admin/libera.php?libpausa=1&idpausa=".$econf['idmoni_pausa']."&idmonitor=".$lselmoni['idmonitor']."');\">PAUSA - TEMPO EXCEDIDO</a>";
                          }
                          else {
                              $color = "class=\"corfd_colcampos\"";
                              $status = $econf['nomemotivo'];
                          }
                      }
                      else {
                          $color = "class=\"corfd_colcampos\"";
                          $status = $lselmoni['status'];
                      }
                  }
                  echo "<tr>";
                            echo "<td $color><input type=\"hidden\" name=\"idmonitor\" id=\"idmonitor\" value=\"".$lselmoni['idmonitor']."\" /><a href=\"inicio.php?menu=contmonitor&idmonitor=".$lselmoni['idmonitor']."&idsuper=".$idsuper."\">".$lselmoni['nomemonitor']."</a></td>";
                            echo "<td align=\"center\" $color>".$lselmoni['entrada']." - ".$lselmoni['saida']."</td>";
                            $sstatus = "SELECT SEC_TO_TIME((TIME_TO_SEC('".date('H:i:s')."') - TIME_TO_SEC(hstatus))) as tempo FROM monitor WHERE idmonitor='".$lselmoni['idmonitor']."'";
                            $esstatus = $_SESSION['fetch_array']($_SESSION['query']($sstatus)) or die ("erro na query de consulta do status atual do monitor");
                            if($status == "OFFLINE") {
                                $atustatus = "--";
                            }
                            else {
                                $atustatus = $esstatus['tempo'];
                            }
                            echo "<td align=\"center\" $color>$atustatus</td>";
                            echo "<td align=\"center\" $color>$status</td>";
                    echo "</tr>";
              }
              ?>
              </tbody>
            </table>
        </div>
        <div style="width:410px; height:300px; float:left; border: 2px solid #999; overflow: auto" id="relatorios">
            <table width="400">
                <tr>
                    <td align="center" class="corfd_ntab"><strong>CONTESTAÇÕES</strong></td>
                </tr>
            </table>
            <table width="400" id="tabaval">
                    <thead>
                        <th width="262" class="corfd_coltexto"><strong>MONITOR</strong></th>
                        <th width="83" align="center" class="corfd_coltexto"><strong>CONTESTADAS</strong></th>
                        <th width="32" align="center" class="corfd_coltexto"><strong>ERRO</strong></th>
                        <th width="52" align="center" class="corfd_coltexto"><strong>F.B</strong></th>
                    </thead>
                    <tbody>
                      <?php
                      $l = 0;
                      $moniaval = "SELECT idmonitor, nomemonitor FROM monitor m INNER JOIN user_adm ua ON ua.iduser_adm = m.iduser_resp $wsqlmoni ORDER BY nomemonitor";
                      $emoniaval = $_SESSION['query']($moniaval) or die ("erro na query de consulta dos monitores");
                      while($lmoniaval = $_SESSION['fetch_array']($emoniaval)) {
                          $l++;
                          if(is_int($l / 2)) {
                              $color = "bgcolor=\"#FFFFFF\"";
                          }
                          else {
                              $color = "bgcolor=\"#C5D4EA\"";
                          }
                        echo "<tr $color>";
                        echo "<td>".$lmoniaval['nomemonitor']."</td>";
                        $selcont = "SELECT SUM( case classifica when 'replica' then '1' else '0' end ) as ccont,
                                    SUM( case classifica when 'erro' then '1' else '0' end ) as cerro, SUM( case classifica when 'feedback' then '1' else '0' end ) as cfeed FROM monitoria_fluxo mf
                                    INNER JOIN monitoria m ON m.idmonitoria = mf.idmonitoria
                                    INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor
                                    INNER JOIN rel_fluxo rf ON rf.idstatus = mf.idstatus AND rf.iddefinicao = mf.iddefinicao
                                    WHERE mo.idmonitor='".$lmoniaval['idmonitor']."' AND m.data BETWEEN '".$datas['dataini']."' AND '".$datas['datafim']."'";
                        $eselcont = $_SESSION['fetch_array']($_SESSION['query']($selcont)) or die ("erro na query de consulta das monitorias contestadas");
                        if($eselcont['ccont'] == "") {
                            $cont = 0;
                        }
                        else {
                            $cont = $eselcont['ccont'];
                        }
                        if($eselcont['cerro'] == "") {
                            $erro = 0;
                        }
                        else {
                            $erro = $eselcont['cerro'];
                        }
                        if($eselcont['cfeed'] == "") {
                            $feed = 0;
                        }
                        else {
                            $feed = $eselcont['cfeed'];
                        }
                        echo "<td align=\"center\">".$cont."</td>";
                        echo "<td align=\"center\">".$erro."</td>";
                        echo "<td align=\"center\">".$feed."</td>";
                        echo "</tr>";
                      }
                      ?>
                    </tbody>
            </table>
        </div>	
  </div>
    <div style="width:1024px; height:420px">
    	<div style="width:550px; height:420px; float:left; overflow:auto; border: 2px solid #999" id="dimen">
            <table width="550">
              <tr>
                <td class="corfd_ntab" align="center" colspan="7"><strong>META X REALIZADO</strong></td>
              </tr>
            </table> 
              <?php
              $periodo = periodo();
              if($periodo == "") {
                    echo "<tr>";
                    echo "<td align=\"center\" class=\"corfd_colcampos\"><font color=\"#FF0000\"><strong>NÃO EXISTE PERÍODO CADASTRADO, FAVOR CONTATAR SEU SUPERVISOR</strong></font></td>";
                    echo "</td>";
                    echo "</table>";
              }
              else {
                    $selprod = "SELECT * FROM periodo WHERE idperiodo='$periodo'";
                    $eselprod = $_SESSION['fetch_array']($_SESSION['query']($selprod)) or die (mysql_error());
                    $dprod = $eselprod['diasprod'];
                    $diasem = date('Y-m-d');
                    $selsem = "SELECT * FROM interperiodo WHERE idperiodo='$periodo'";
                    $eselsem = $_SESSION['query']($selsem) or die ("erro na query de consulta dos intervalos do período");
                    while($lselsem = $_SESSION['fetch_array']($eselsem)) {
                        $verif = "SELECT '".$diasem."' BETWEEN '".$lselsem['dataini']."'  AND '".$lselsem['datafim']."' as verifdata";
                        $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ("erro na query de consutla do intervalo do período");
                        if($everif['verifdata'] >= 1) {
                            $sem = 1;
                            $dtinisem = $lselsem['dataini'];
                            $dtfimsem = $lselsem['datafim'];
                        }
                        else {
                            $sem = 0;
                        }

                    }
                    $data = $eselprod['dataini'];
                    $dtatu = date('Y-m-d');
                    $diasem = idate(time());
                    $ultdiames = $eselprod['datafim'];
                    $dtatu_dia = substr($data,8,2);
                    $dtatu_mes = substr($data,5,2);
                    $dtatu_ano = substr($data,0,4);
                    $pdiames = $data;
                    $dtatu = date('Y-m-d');
                    $seldimen = "SELECT count(*) as r FROM dimen_mod WHERE idperiodo='$periodo'";
                    $eseldimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
                    if($eseldimen['r'] >= '1') {
                    echo "<div style=\"width:540px; height:400px; float:left; overflow:auto\">";
                    echo "<table width=\"525\">";
                    ?>
                    <thead>
                    <th width="150" class="corfd_coltexto"><strong><?php echo $efiltro['nomefiltro_nomes']; ?></strong></th>
                    <th width="50" align="center" class="corfd_coltexto"><strong><span title="META MES">M. MES</span></strong></th>
                    <th width="50" align="center" class="corfd_coltexto"><strong><span title="META SEMANA">M. SEM</span></strong></th>
                    <th width="50" align="center" class="corfd_coltexto"><strong><span title="META DIA">M. DIA</span></strong></th>
                    <th width="50" align="center" class="corfd_coltexto"><strong><span title="REALIZADO MES">R. MES</span></strong></th>
                    <th width="50" align="center" class="corfd_coltexto"><strong><span title="REALIZADO SEMANA">R. SEM</span></strong></th>
                    <th width="50" align="center" class="corfd_coltexto"><strong><span title="REALIZADO DIA">R. DIA</span></strong></th>
                    </thead>
                    <tbody>
                    <?php
                    $semana = array('1' => 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thursday','5' => 'Friday','6' => 'Saturday', '7' => 'Sunday');
                    /*$dia = jddayofweek(cal_to_jd(CAL_GREGORIAN, $dtatu_mes, $dtatu_dia, $dtatu_ano));
                    foreach($semana as $num => $dias) {
                      if($dia == $num) {
                        $numero = $num;
                      }
                      else {
                      }
                    }
                    for($i = 1; $i < $numero; $i++) {
                      $dtatu -= 86400;
                    }
                    if($i == $numero) {
                      $dtatu = data2banco($data);
                    }
                    if($i != $numero) {
                      $dtatu = date('Y-m-d', $dtatu);
                    }*/
                  if($_GET['idmonitor'] != "") {
                      $wherep = " AND idmonitor='$idmonitor'";
                      $wheret = " AND m.idmonitor='$idmonitor'";
                      $selmoni = "SELECT DISTINCT(m.idrel_filtros) as idrel FROM monitoria m
                                  INNER JOIN fila_grava fg ON fg.idfila_grava = m.idfila
                                  INNER JOIN upload u ON u.idupload = fg.idupload
                                  WHERE m.idmonitor='$idmonitor' AND u.idperiodo='$periodo'";
                      $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos relacionamentos monitorados");
                      while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                          $idsrel[] = $lselmoni['idrel'];
                      }
                      $seldimen = "SELECT * FROM dimen_moni WHERE idmonitor='$iduser' AND idperiodo='$periodo'";
                      $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento");
                      while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                          if(in_array($lseldimen['idrel_filtros'],$idsrel)) {
                          }
                          else {
                              $idsrel[] = $lseldimen['idrel_filtros'];
                          }
                      }
                      $seldimen = "SELECT dm.idrel_filtros,dm.estimativa as meta_m,dm.idperiodo,(dm.estimativa / dm.divperiodo) as meta_s, (dm.estimativa / $dprod) as meta_d  FROM dimen_mod dm
                                   INNER JOIN rel_filtros r ON r.idrel_filtros = dm.idrel_filtros
                                   INNER JOIN filtro_dados fd ON fd.idfiltro_dados = r.id_$filtro
                                   WHERE dm.idperiodo='$periodo' AND dm.idrel_filtros IN ('".implode("','",$idsrel)."') ORDER BY fd.nomefiltro_dados";                     
                  }
                  if($_GET['idsuper'] != "" AND $_GET['idmonitor'] == "") {
                      $inner = "INNER JOIN monitor mo ON mo.idmonitor = m.idmonitor INNER JOIN user_adm ua ON ua.iduser_adm = mo.iduser_resp";
                      $wherep = "AND ua.iduser_adm='$idsuper'";
                      $smoniresp = "SELECT m.idmonitor FROM monitor m INNER JOIN user_adm ua ON ua.iduser_adm = m.iduser_resp WHERE iduser_resp='$idsuper'";
                      $emoniresp = $_SESSION['query']($smoniresp) or die ("erro na query de consulta dos monitores relacionados ao supervisor");
                      while($lmoniresp = $_SESSION['fetch_array']($emoniresp)) {
                          $idsmoni[] = $lmoniresp['idmonitor'];
                      }
                      if($idsmoni == "") {
                          $wheret = "";
                      }
                      else {
                          $wheret = " AND m.idmonitor IN (".implode(",",$idsmoni).")";
                      }
                      
                      $selmoni = "SELECT DISTINCT(m.idrel_filtros) as idrel FROM monitoria m
                                  INNER JOIN fila_grava fg ON fg.idfila_grava = m.idfila
                                  INNER JOIN upload u ON u.idupload = fg.idupload
                                  WHERE m.idmonitor IN ('".implode("','",$idsmoni)."') AND u.idperiodo='$periodo'";
                      $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta dos relacionamentos monitorados");
                      while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
                          $idsrel[] = $lselmoni['idrel'];
                      }
                      foreach($idsmoni as $idm) {
                          $seldimen = "SELECT * FROM dimen_moni WHERE idmonitor='$idm' AND idperiodo='$periodo'";
                          $eseldimen = $_SESSION['query']($seldimen) or die ("erro na query de consulta do dimensionamento");
                          while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
                              if(in_array($lseldimen['idrel_filtros'],$idsrel)) {
                              }
                              else {
                                  $idsrel[] = $lseldimen['idrel_filtros'];
                              }
                          }
                      }
                      $seldimen = "SELECT dm.idrel_filtros,dm.estimativa as meta_m,dm.idperiodo,(dm.estimativa / dm.divperiodo) as meta_s, (dm.estimativa / $dprod) as meta_d  FROM dimen_mod dm
                                   INNER JOIN rel_filtros r ON r.idrel_filtros = dm.idrel_filtros
                                   INNER JOIN filtro_dados fd ON fd.idfiltro_dados = r.id_$filtro
                                   WHERE dm.idperiodo='$periodo' AND dm.idrel_filtros IN ('".implode("','",$idsrel)."') ORDER BY fd.nomefiltro_dados";

                  }
                  if($_GET['idmonitor'] == "" && $_GET['idsuper'] == "") {
                      $seldimen = "SELECT dm.idrel_filtros, fd.nomefiltro_dados, estimativa as meta_m,(dm.estimativa / COUNT(DISTINCT(ip.idinterperiodo))) as meta_s,
                                    (dm.estimativa / p.diasprod) as meta_d, COUNT(DISTINCT(ip.idinterperiodo)) as semanas
                                    FROM dimen_mod dm
                                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower(trim($efiltro['nomefiltro_nomes']))."
                                    INNER JOIN periodo p ON p.idperiodo = dm.idperiodo
                                    INNER JOIN interperiodo ip ON ip.idperiodo = p.idperiodo
                                    WHERE p.idperiodo='$periodo'
                                    GROUP BY dm.idrel_filtros ORDER BY fd.nomefiltro_dados;";
                  }
                  if(!isset($_GET['idmonitor']) && !isset($_GET['idsuper'])) {
                      $seldimen = "SELECT dm.idrel_filtros, fd.nomefiltro_dados, estimativa as meta_m, (dm.estimativa / COUNT(DISTINCT(ip.idinterperiodo))) as meta_s,
                                    (dm.estimativa / p.diasprod) as meta_d, COUNT(DISTINCT(ip.idinterperiodo)) as semanas
                                    FROM dimen_mod dm
                                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower(trim($efiltro['nomefiltro_nomes']))."
                                    INNER JOIN periodo p ON p.idperiodo = dm.idperiodo
                                    INNER JOIN interperiodo ip ON ip.idperiodo = p.idperiodo
                                    WHERE p.idperiodo='$periodo'
                                    GROUP BY dm.idrel_filtros ORDER BY fd.nomefiltro_dados;";
                  }
                  $l = 0;
                  $edimen = $_SESSION['query']($seldimen) or die ("erro na execusão da query de consulta do dimensionamento cadastrado");
                while($ldimen = $_SESSION['fetch_array']($edimen)) {
                    if(in_array($ldimen['idrel_filtros'],$idsrel)) {
                    }
                    else {
                        $idsrel[] = $ldimen['idrel_filtros'];
                    }
                      $l++;
                      if(is_int($l / 2)) {
                          $color = "bgcolor=\"#FFFFFF\"";
                      }
                      else {
                          $color = "bgcolor=\"#C5D4EA\"";
                      }
                      echo "<tr $color>";
                      echo "<td width=\"150\" ><strong>".nomevisu($ldimen['idrel_filtros'])."</strong></td>";
                      echo "<td width=\"50\" align=\"center\">".ceil($ldimen['meta_m'])."</td>";
                      echo "<td width=\"50\" align=\"center\">".ceil($ldimen['meta_s'])."</td>";
                      echo "<td width=\"50\" align=\"center\">".ceil($ldimen['meta_d'])."</td>";
                      if(!isset($_GET['idmonitor']) && !isset($_GET['idsuper'])) {
                      }
                      if($_GET['idmonitor'] == "" && $_GET['idsuper'] == "") {
                      }
                      if($_GET['idsuper'] != "" AND $_GET['idmonitor'] == "") {
                      }
                      //if($_GET['idmonitor'] != "") {
                      $realm = "SELECT COUNT(DISTINCT(idfila)) as qtdemoni FROM monitoria m $inner WHERE idrel_filtros='".$ldimen['idrel_filtros']."' $wheret AND data BETWEEN '".$pdiames."' AND '".$ultdiames."'";
                      $erealm = $_SESSION['fetch_array']($_SESSION['query']($realm)) or die ("erro no processo de somatória das monitorias");
                      $qtdemoni = $erealm['qtdemoni'];
                      //}
                      $reals = "SELECT COUNT(DISTINCT(idfila)) as qtdesem FROM monitoria m $inner WHERE idrel_filtros='".$ldimen['idrel_filtros']."' $wheret AND data BETWEEN '$dtinisem' AND '$dtfimsem'";
                      $ereals = $_SESSION['fetch_array']($_SESSION['query']($reals)) or die ("erro no processo de somatória das monitorias");
                      $reald = "SELECT COUNT(DISTINCT(idfila)) as qtdedia FROM monitoria m $inner WHERE idrel_filtros='".$ldimen['idrel_filtros']."' $wheret AND data='$dtatu'";
                      $ereald = $_SESSION['fetch_array']($_SESSION['query']($reald)) or die ("erro no processo de somatória das monitorias");
                      echo "<td width=\"50\"  align=\"center\">".ceil($qtdemoni)."</td>";
                      echo "<td width=\"50\" align=\"center\">".ceil($ereals['qtdesem'])."</td>";
                      echo "<td width=\"50\" align=\"center\">".ceil($ereald['qtdedia'])."</td>";
                      echo "</tr>";
                      $tometam = $tometam + $ldimen['meta_m'];
                      $tometas = $tometas + $ldimen['meta_s'];
                      $tometad = $tometad + $ldimen['meta_d'];
                      $torealm = $torealm + $qtdemoni;
                      $toreals = $toreals + $ereals['qtdesem'];
                      $toreald = $toreald + $ereald['qtdedia'];

              }
              echo "</tbody>";
              echo "</table>";
              echo "</div>";
              }
            }
            ?>
        </div>
        <div style="width:450px; height:420px; float:left; border: 2px solid #999;" id="producao">
            <?php
            echo "<table width=\"450\">";
                echo "<tr>";
                echo "<td class=\"corfd_ntab\" align=\"center\" colspan=\"7\"><strong>APROVEITAMENTO DO TEMPO</strong></td>";
                echo "</tr>";
            echo "</table>";
            echo "<div style=\"height:370px; overflow:auto;\">";
            echo "<table width=\"450\">";
            echo "<thead>";
                echo "<th class=\"corfd_coltexto\" align=\"center\" title=\"Dia\"><strong>DIA</strong></th>";
                echo "<th class=\"corfd_coltexto\" align=\"center\" title=\"Horas Trabalhadas\"><strong>HORAS</strong></th>";
                echo "<th class=\"corfd_coltexto\" align=\"center\" title=\"Pausas\"><strong>PAUSAS</strong></th>";
                echo "<th class=\"corfd_coltexto\" align=\"center\" title=\"Tempo Total de Monitoria\"><strong>TTM</strong></th>";
                echo "<th class=\"corfd_coltexto\" align=\"center\"><strong><span title=\"Quantidade de Monitores\">MONITORES</span></strong></th>";
                echo "<th class=\"corfd_coltexto\" align=\"center\" title=\"Quantidade Monitoria\"><strong>QTDE.</strong></th>";
                echo "<th class=\"corfd_coltexto\" align=\"center\" title=\"Tempo médio de Monitoria\"><strong>TMM</strong></th>";
            echo "</thead>";
            echo "<tbody>";
            if($periodo == "") {
                echo "<tr>";
                echo "<td align=\"center\" class=\"corfd_colcampos\"><font color=\"#FF0000\"><strong>NÃO EXISTE PERÍODO CADASTRADO, FAVOR CONTATAR SEU SUPERVISOR</strong></font></td>";
                echo "</td>";
                echo "</table>";
            }
            else {
                $seldatas = "SELECT * FROM periodo WHERE idperiodo='".$periodo."'";
                $eseldatas = $_SESSION['fetch_array']($_SESSION['query']($seldatas)) or die ("erro na consulta do periodo para logins");
                $dataini = $eseldatas['dataini'];
                $dtini_ano = substr($dataini, 0, 4);
                $dtini_mes = substr($dataini, 5, 2);
                $dtini_dia = substr($dataini, 8, 2);
                $datafim = date('Y-m-d');
                $dtfim_ano = substr($datafim, 0, 4);
                $dtfim_mes = substr($datafim, 5, 2);
                $dtfim_dia = substr($datafim, 8, 2);
                $dtini = mktime(0,0,0,$dtini_mes,$dtini_dia,$dtini_ano); // timestamp da data inicial
                $dtfim = mktime(0,0,0,$dtfim_mes,$dtfim_dia,$dtfim_ano); // timestamp da data final
                $newdt = $dataini;
                $intervaldt[] = $dataini;
                $difdata = "select datediff('$datafim','$dataini') as data";
                $edifdata = $_SESSION['fetch_array']($_SESSION['query']($difdata)) or die ("erro na comparação das datas");
                for($d = 0;$d < $edifdata['data'];$d++) {
                    $acresdt = "SELECT ADDDATE('$newdt',interval 1 day) as newdata";
                    $ecresdt = $_SESSION['fetch_array']($_SESSION['query']($acresdt)) or die ("erro na query de consulta da nova data");
                    $intervaldt[] = $ecresdt['newdata'];
                    $newdt = $ecresdt['newdata'];
                }
                $i = 0;
                foreach($intervaldt as $dia) {
                  $temposec = 0;
                  $l++;
                  if(is_int($l / 2)) {
                      $color = "bgcolor=\"#FFFFFF\"";
                  }
                  else {
                      $color = "bgcolor=\"#C5D4EA\"";
                  }
                  $diadia = substr($dia,8,2);
                  $diames = substr($dia,5,2);
                  $diaano = substr($dia,0,4);
                  $temptrab = 0;
                  $selpausa = "SELECT COUNT(*) as qtde, SEC_TO_TIME(SUM(TIME_TO_SEC(m.tempo))) as tempo FROM moni_pausa m $inner WHERE m.data='$dia' $wherep";
                  $epausa = $_SESSION['query']($selpausa) or die ("erro na query para obter as pausas");
                  $eselpausa = $_SESSION['fetch_array']($epausa);
                  $countp = $eselpausa['qtde'];
                  if($eselpausa['tempo'] == "") {
                      $pausas = "00:00:00"." - "."0";
                      $link = $pausas;
                  }
                  else {
                      $pausas = $eselpausa['tempo']." - ".$countp;
                      $link = "<a href=\"/monitoria_supervisao/admin/detpausa.php?idmonitor=".$idmonitor."&idsuper=".$idsuper."&dia=".$dia."&height=400&width=500\" class=\"thickbox\" title=\"DETALHE PAUSAS\">".$pausas."</a>";
                  }
                  $sellogins = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC(tempo))) as total FROM moni_login m WHERE data='$dia' AND logout<>'00:00:00' $wheret ORDER BY login DESC";
                  $esellogin = $_SESSION['fetch_array']($_SESSION['query']($sellogins)) or die ("erro na consulta dos logins");
                  if($esellogin['total'] == "") {
                      $tempo = "00:00:00";
                  }
                  else {
                    $tempo = $esellogin['total'];
                  }
                  $selmoni = "SELECT COUNT(DISTINCT(idfila)) as result, COUNT(DISTINCT(idmonitor)) as monitor FROM monitoria m WHERE data='$dia' $wheret";
                  $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na consulta da qtde de monitorias");
                  $tempomoni = "SELECT DISTINCT(idfila), horaini, horafim, (TIME_TO_SEC(horafim) - TIME_TO_SEC(horaini)) as total FROM monitoria m WHERE data='$dia' $wheret";
                  $etempomoni = $_SESSION['query']($tempomoni) or die ("erro na consulta dos tempos das monitorias");
                  while($ltempo = $_SESSION['fetch_array']($etempomoni)) {
                      $temposec = $temposec + $ltempo['total'];
                  }
                  if($temposec == "") {
                      $ttm = "00:00:00";
                  }
                  else {
                      $ttm = sec_hora($temposec);
                  }
                  if($temposec == "") {
                      $tmm = "00:00:00";
                  }
                  else {
                      $sec = round($temposec / $eselmoni['result']);
                      $tmm = sec_hora($sec);
                  }
                  echo "<tr $color>";
                      echo "<td align=\"center\" >".substr($dia,8,2)."</td>";
                      echo "<td align=\"center\" >".$tempo."</td>";
                      echo "<td align=\"center\" >".$link."</td>";
                      echo "<td align=\"center\" >".$ttm."</td>";
                      echo "<td align=\"center\" >".$eselmoni['monitor']."</td>";
                      echo "<td align=\"center\" >".$eselmoni['result']."</td>";
                      echo "<td width=\"75px\" align=\"center\" >".$tmm."</td>";
                  echo "</tr>";
                  $totalm = $totalm + $eselmoni['result'];
                  $totalmt = $totalmt + $eselmoni['monitor'];
                  if($eselmoni['monitor'] >= 1) {
                      $cmoni++;
                  }
                }
                $selttotal = "SELECT SUM( TIME_TO_SEC(tempo)) as total FROM moni_login m
                             WHERE data BETWEEN '$dataini' AND '$datafim' $wheret AND logout<>'00:00:00' ORDER BY login DESC";
                $ettotal = $_SESSION['fetch_array']($_SESSION['query']($selttotal)) or die ("erro na query de consutla do tempo total");
                $horatt = sec_hora($ettotal['total']);
                $seltotal = "SELECT DISTINCT(idfila), SUM(TIME_TO_SEC(horafim) - TIME_TO_SEC(horaini)) as total, SEC_TO_TIME( SUM((TIME_TO_SEC(horafim) - TIME_TO_SEC(horaini)) / ".$totalm.")) as tmm FROM monitoria m WHERE data BETWEEN '$dataini' AND '$datafim' $wheret";
                $eseltotal = $_SESSION['fetch_array']($_SESSION['query']($seltotal)) or die ("erro na query de consutla dos tempos totais de monitoria");
                $seltpausa = "SELECT COUNT(*) as qtde, SEC_TO_TIME(SUM(TIME_TO_SEC(m.tempo))) as tempo FROM moni_pausa m $inner WHERE m.data BETWEEN '$dataini' AND '$datafim' $wherep";
                $etpausa = $_SESSION['query']($seltpausa) or die ("erro na query para obter as pausas");
                $eseltpausa = $_SESSION['fetch_array']($etpausa);
                $countp = $eseltpausa['qtde'];
                if($eseltpausa['tempo'] == "") {
                    $pausast = "00:00:00"." - "."0";
                    $linkt = $pausas;
                }
                else {
                    $pausast = $eseltpausa['tempo']." - ".$countp;
                    $linkt = "<a href=\"/monitoria_supervisao/admin/detpausa.php?idmonitor=".$idmonitor."&idsuper=".$idsuper."&dia=&height=400&width=500\" class=\"thickbox\" title=\"DETALHE PAUSAS\">".$pausast."</a>";
                }
                if($eseltotal['total'] == "") {
                    $tmtotal = "0";
                }
                else {
                    $tmtotal = $eseltotal['total'];
                }
                if($eseltotal['tmm'] == "") {
                    $tmmtotal = "00:00:00";
                }
                else {
                    $tmmtotal = $eseltotal['tmm'];
                }
                echo "</tbody>";
                echo "</table></div>";
            }
            ?>
        </div>
        <?php
        if($periodo == "") {
        }
        else {
            echo "<div style=\"width:510px; float:left\">";
              echo "<table width=\"510\">";
              echo "<tr>";
                echo "<td width=\"150\" class=\"corfd_colcampos\"><strong>TOTAL</strong></td>";
                echo "<td width=\"50\" class=\"corfd_colcampos\" align=\"center\"><strong>".ceil($tometam)."</strong></td>";
                echo "<td width=\"50\" class=\"corfd_colcampos\" align=\"center\"><strong>".ceil($tometas)."</strong></td>";
                echo "<td width=\"50\" class=\"corfd_colcampos\" align=\"center\"><strong>".ceil($tometad)."</strong></td>";
                echo "<td width=\"50\" class=\"corfd_colcampos\" align=\"center\"><strong>".ceil($torealm)."</strong></td>";
                echo "<td width=\"50\" class=\"corfd_colcampos\" align=\"center\"><strong>".ceil($toreals)."</strong></td>";
                echo "<td width=\"50\" class=\"corfd_colcampos\" align=\"center\"><strong>".ceil($toreald)."</strong></td>";
              echo "</tr>";
              echo "</table>";
            echo "</div>";
            echo "<div style=\"width:500px; float:left;margin-left:5px\">";
            echo "<table width=\"490\">";
                echo "<tr>";
                    echo "<td width=\"40px\" class=\"corfd_colcampos\" align=\"center\"><strong>TOTAL</strong></td>";
                    echo "<td width=\"75px\" class=\"corfd_colcampos\" align=\"center\"><strong>".$horatt."</strong></td>";
                    echo "<td width=\"75px\" class=\"corfd_colcampos\" align=\"center\"><strong>".$linkt."</strong></td>";
                    echo "<td width=\"75px\" class=\"corfd_colcampos\" align=\"center\"><strong>".sec_hora($tmtotal)."</strong></td>";
                    echo "<td width=\"50px\" class=\"corfd_colcampos\" align=\"center\"><strong>".round($totalmt/$cmoni)."</strong></td>";
                    echo "<td width=\"50px\" class=\"corfd_colcampos\" align=\"center\"><strong>".$totalm."</strong></td>";
                    echo "<td width=\"75px\" class=\"corfd_colcampos\" align=\"center\"><strong>".$tmmtotal."</strong></td>";
               echo "</tr>";
            echo "</table></div>";
        }
        ?>
    </div>
</div>
