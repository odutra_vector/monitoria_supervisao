<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');

$id = $_POST['idalerta'];
$nome = strtoupper($_POST['nomealerta']);
$auto = $_POST['automatico'];
$ativo = $_POST['ativo'];
$userfg = $_POST['userfg'];
$cor = $_POST['coralerta'];
$idconfemail = $_POST['idconfemailenv'];
$user_adm = val_vazio(implode(",",$_POST['user_adm']));
$user_web = val_vazio(implode(",",$_POST['user_web']));

if(isset($_POST['cadastra'])) {
    $selalerta = "SELECT COUNT(*) as result FROM alertas WHERE nomealerta='$nome'";
    $ealerta = $_SESSION['fetch_array']($_SESSION['query']($selalerta)) or die (mysql_error());
    if($ealerta['result'] >= 1) {
        $msg = "O nome dado para o alerta já está cadastrado!";
        header("location:admin.php?menu=alertas&msg=$msg");
    }
    else {
        $insert = "INSERT INTO alertas (nomealerta,automatico,ativo,user_adm,user_web,userfg,coralerta,idconfemailenv) VALUES ('$nome','$auto','$ativo',$user_adm,$user_web,'$userfg','$cor','$idconfemail')";
        $einert = $_SESSION['query']($insert) or die (mysql_error());
        if($einert) {
            $msg = "Cadastro efetuado com sucesso!";
            header("location:admin.php?menu=alertas&msg=$msg");
        }
        else {
            $msg = "Erro no processo de cadastramento, favor contatar o administrador!";
            header("location:admin.php?menu=alertas&msg=$msg");
        }
    }
}
if(isset($_POST['altera'])) {
    $enviados['nomealerta'] = $nome;
    $enviados['automatico'] = $auto;
    $enviados['ativo'] = $ativo;
    $enviados['user_adm'] = implode(",",$_POST['user_adm']);
    $enviados['user_web'] = implode(",",$_POST['user_web']);
    $enviados['userfg'] = $_POST['userfg'];
    $enviados['coralerta'] = $_POST['coralerta'];
    $enviados['idconfemailenv'] = $_POST['idconfemailenv'];
    $alt = array();
    $selalerta = "SELECT * FROM alertas WHERE idalertas='$id'";
    $ealerta = $_SESSION['fetch_array']($_SESSION['query']($selalerta)) or die (mysql_error());
    $checknome = "SELECT COUNT(*) as result FROM alertas WHERE nomealerta='$nome' AND idalertas<>$id";
    $echecknome = $_SESSION['fetch_array']($_SESSION['query']($checknome)) or die (mysql_error());
    $colunas = array("nomealerta","automatico","ativo","user_adm","user_web","userfg","idconfemailenv","coralerta");
    foreach($colunas as $col) {
        if($enviados[$col] != $ealerta[$col]) {
            $dado = val_vazio($enviados[$col]);
            $alt[] = "$col=$dado";
        }
    }
    if(count($alt) == 0) {
        $msg = "Nenhum dado foi alterado";
        header("location:admin.php?menu=alertas&msg=$msg");
    }
    else {
        if($echecknome['result'] >= 1) {
            $msg = "O nome escolhido já consta cadastrado!";
            header("location:admin.php?menu=alertas&msg=$msg");
        }
        else {
            $altera = "UPDATE alertas SET ".implode(",",$alt)." WHERE idalertas='$id'";
            $ealerta = $_SESSION['query']($altera) or die (mysql_error());
            if($ealerta) {
                $msg = "Alteração efetuada com sucesso!";
                header("location:admin.php?menu=alertas&msg=$msg");
            }
            else {
                $msg = "Erro no processo de alteração, favor contatar o administrador!";
                header("location:admin.php?menu=alertas&msg=$msg");
            }
        }
    }
}
?>
