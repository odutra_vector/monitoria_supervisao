<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

// DADOS RELATORIO
$qtdecampos = $_POST['qtdecampos'];
$nome = strtoupper(trim($_POST['nome']));
$tabpri = $_POST['tabpri'];
$agrupa = $_POST['groupby'];
$tipo = $_POST['tipo'];
$tabsec = $_POST['tabsec'];
$tabterc = implode(",",$_POST['tabterc']);
$vincrel = $_POST['vincrel'];
if($vincrel == "") {
  $vincrel = "00";
}
$obsfiltros = implode(",",$_POST['obfiltros']);
$perfadm = implode(",",$_POST['perfadm']);
$perfweb = implode(",",$_POST['perfweb']);
$ativo = $_POST['ativo'];

// DADOS GRAFICO

$colgraf = "SHOW COLUMNS FROM grafrelmoni";
$ecolgrad = $_SESSION['query']($colgraf) or die ("erro na query de consulta das colunas da tabela grafrelmoni");
while($lcolgraf = $_SESSION['fetch_array']($ecolgrad)) {
    if($lcolgraf['Field'] == "idgrafrelmoni" OR $lcolgraf['Field'] == "idrelmoni") {
    }
    else {
        $colsgraf[] = $lcolgraf['Field'];
    }
}

$tipograf = val_vazio($_POST['tipograf']);
if($tipograf == "NULL") {
    foreach($colsgraf as $col) {
        $valgraf[] = "NULL";
    }
}
else {
    foreach($colsgraf as $col) {
        $valgraf[] = val_vazio($_POST[$col]);
    }
}

if(isset($_POST['cadastra'])) {
  $selrel = "SELECT COUNT(*) as result FROM relmoni WHERE nomerelmoni='$nome'";
  $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query para consultar relatÃ³rioas cadastrado");
  if(!is_numeric($qtdecampos)) {
    $msg = "O campo ''Qtdecampos'' sÃ³ pode conter nÃºmeros!!!";
    header("location:admin.php?menu=relmoni&msg=".$msg);
    break;
  }
  if($eselrel['result'] >= 1) {
    $msg = "O nome de reltÃ³rio informado jÃ¡ estÃ¡ cadastrado na base, favor informar outro!!!";
    header("location:admin.php?menu=relmoni&msg=".$msg);
    break;
  }
  else {
    $insert = "INSERT INTO relmoni (nomerelmoni, qtdecol, filtros, idsperfiladm, idsperfilweb, tabpri, agrupa, tabsec, tabterc, vincrel, ativo) VALUES ('$nome', '$qtdecampos', '$obsfiltros', '$perfadm', '$perfweb', '$tabpri', '$agrupa', '$tabsec', '$tabterc', '$vincrel', '$ativo')";
    $einsert = $_SESSION['query']($insert) or die ("erro na inserÃ§Ã£o do relatÃ³rio");
    $idrel = $_SESSION['insert_id']();
    $colsgraf[] = "idrelmoni";
    $valgraf[] = $idrel;
    $insertgraf = "INSERT INTO grafrelmoni (".implode(",",$colsgraf).") VALUES (".implode(",",$valgraf).")";
    $egraf = $_SESSION['query']($insertgraf) or die ("erro na query de inserÃ§Ã£o do grÃ¡fico");
    if($einsert) {
	$msg = "Base para o relatÃ³rio ''$nome'' foi criado com sucesso!!!";
	header("location:admin.php?menu=relmonicol&id=".$idrel."&nome=".$nome."&msg=".$msg);
	break;
    }
    else {
	$msg = "Erro no processo de criaÃ§Ã£o do relatÃ³rio, favor contatar o administrador!!!";
	header("location:admin.php?menu=relmoni&msg=".$msg);
	break;
    }
  }
}

if(isset($_POST['volta'])) {
  header("location:admin.php?menu=relmoni");
  break;
}

if(isset($_POST['relcol'])) {
  $id = $_POST['id'];
  header("location:admin.php?menu=relmonicol&id=".$id."&nome=".$nome);
  break;
}

if(isset($_POST['apaga'])) {
  $id = $_POST['id'];
  $delrel = "DELETE FROM relmoni WHERE idrelmoni='$id'";
  $edel = $_SESSION['query']($delrel) or die ("erro na query para apagar linha");
  $incre = "ALTER TABLE relmoni AUTO_INCREMENT=1";
  $eincre = $_SESSION['query']($incre) or die ("erro na query para reset do Auto Incremento");
  if($edel) {
    $msgi = "RelatÃ³rio apagado com sucesso!!!";
    header("location:admin.php?menu=relmoni&msgi=".$msgi);
    break;
  }
  else {
    $msgi = "Erro no processo para apagar o relatÃ³rio, favor contatar o administrador!!!";
    header("location:admin.php?menu=relmoni&msgi=".$msgi);
    break;
  }
}

if(isset($_POST['carrega'])) {
  $idrel = $_POST['id'];
  header("location:admin.php?menu=relmoni&idrelmoni=".$idrel);
  break;
}

if(isset($_POST['novo'])) {
  header('location:admin.php?menu=relmoni');
  break;
}

if(isset($_POST['altera'])) {
  $id = $_POST['id'];
  $idgraf = $_POST['idgraf'];
  $selrel = "SELECT * FROM relmoni WHERE idrelmoni='$id'";
  $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relatÃ³rio");
  $selgraf = "SELECT * FROM grafrelmoni WHERE idrelmoni='$id'";
  $eselgraf = $_SESSION['fetch_array']($_SESSION['query']($selgraf)) or die ("erro na query de consulta dos dados do grafico");
  $verifnome = "SELECT COUNT(*) as result FROM relmoni WHERE nomerelmoni='$nome' AND idrelmoni<>'$id'";
  $everif = $_SESSION['fetch_array']($_SESSION['query']($verifnome)) or die ("erro na query de consulta de duplicidade");
  $upgraf = array_combine($colsgraf, $valgraf);
  foreach($upgraf as $kup => $up) {
      if($_POST[$kup] != $eselgraf[$kup]) {
          $valupdate[] = $kup."=".$up;
      }
      else {
      }
  }
  if(!is_numeric($qtdecampos)) {
    $msg = "O campo ''Qtdecampos'' sÃ³ pode conter nÃºmeros!!!";
    header("location:admin.php?menu=relmoni&idrelmoni=".$id."&msg=".$msg);
    break;
  }
  if($qtdecampos == "" || $nome == "" || $tabpri == "" || ($perfadm == "" && $perfweb == "") || $tabsec == "" || $agrupa == "") {
    $msg = "Os campos com asterisco nÃ£o podem estar vazios!!!";
    header("location:admin.php?menu=relmoni&idrelmoni=".$id."&msg=".$msg);
    break;
  }
  if($everif['result'] >= 1) {
    $msg = "O nome informado jÃ¡ consta em outro relatÃ³rio!!!";
    header("location:admin.php?menu=relmoni&idrelmoni=".$id."&msg=".$msg);
    break;
  }
  if($qtdecampos == $eselrel['qtdecol'] && $nome == $eselrel['nomerelmoni'] && $obsfiltros == $eselrel['filtros'] && $perfadm == $eselrel['idsperfiladm'] &&
     $perfweb == $eselrel['idsperfilweb'] && $tabpri == $eselrel['tabpri'] && $agrupa == $eselrel['agrupa'] && $tabsec == $eselrel['tabsec'] &&
     $tabterc == $eselrel['tabterc'] && $vincrel == $eselrel['vincrel'] && $ativo == $eselrel['ativo'] && $valupdate == "") {
     $msg = "Nenhum campo do relatÃ³rio foi alterado, processo nÃ£o executado!!!";
     header("location:admin.php?menu=relmoni&idrelmoni=".$id."&msg=".$msg);
     break;
  }
  else {
    $update = "UPDATE relmoni SET nomerelmoni='$nome', qtdecol='$qtdecampos', filtros='$obsfiltros', idsperfiladm='$perfadm', idsperfilweb='$perfweb',
      tabpri='$tabpri', agrupa='$agrupa', tabsec='$tabsec', tabterc='$tabterc', vincrel='$vincrel', ativo='$ativo' WHERE idrelmoni='$id'";
    $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o do relatÃ³rio");
    if($valupdate == "") {
    }
    else {
        $upgraf = "UPDATE grafrelmoni SET ".implode(",",$valupdate)." WHERE idgrafrelmoni='$idgraf'";
        $eupgraf = $_SESSION['query']($upgraf) or die ("erro na query de update da tabela de grÃ¡ficos");
    }
    if($eupdate) {
      $msg = "RelatÃ³rio atualizado com sucesso!!!";
      header("location:admin.php?menu=relmoni&idrelmoni=".$id."&msg=".$msg);
      break;
    }
    else {
      $msg = "Erro no processo de atualizaÃ§Ã£o, favor contatar o administrador!!!";
      header("location:admin.php?menu=relmoni&idrelmoni=".$id."&msg=".$msg);
      break;
    }
  }
}

if(isset($_POST['relaciona'])) {
  $nome = $_POST['nome'];
  $idrelmoni = $_POST['idrelmoni'];
  $tabelas = $_POST['tabela'];
  $colunas = $_POST['coluna'];
  $tipo = $_POST['tipo'];
  foreach($_POST['serie'] as $posts) {
      if($posts == "NULO") {
          $serie[] = "NULL";
      }
      else {
          if($posts == "") {
          }
          else {
            $serie[] = $posts;
          }
      }
  }
  foreach($_POST['grafico'] as $postg) {
      if($postg == "NULO") {
          $grafico[] = "NULL";
      }
      else {
          if($postg == "") {
          }
          else {
            $grafico[] = "'".$postg."'";
          }
      }
  }
  foreach($_POST['cor'] as $postc) {
      if($postc == "NULO") {
          $cor[] = "NULL";
      }
      else {
          if($postc == "") {
          }
          else {
            $cor[] = "'".$postc."'";
          }
      }
  }
  $nomeapres = $_POST['nomeapres'];
  $posicao = $_POST['posicao'];
  $ordena = $_POST['ordena'];

  foreach($_POST['tabela'] as $t) {
      if($t == "") {

      }
      else {
        $vars['tabelas'][] = $t;
        $comp['tabelas'][] = $t;
      }
  }
  foreach($_POST['coluna'] as $c) {
      if($c == "") {

      }
      else {
        $vars['coluna'][] = $c;
        $comp['coluna'][] = $c;
      }
  }
  foreach($_POST['tipo'] as $tp) {
      if($tp == "") {
      }
      else {
          $vars['tipo'][] = $tp;
          $comp['tipo'][] = $tp;
      }
  }
  foreach($_POST['nomeapres'] as $n) {
      if($n == "") {

      }
      else {
        $vars['nomeapres'][] = $n;
        $comp['nomeapres'][] = $n;
      }
  }
  foreach($_POST['posicao'] as $p) {
      if($p == "") {

      }
      else {
        $vars['posicao'][] = $p;
        $comp['posicao'][] = $p;
      }
  }
  foreach($_POST['ordena'] as $o) {
      if($o == "") {
      }
      else {
        $vars['ordena'][] = $o;
        $comp['ordena'][] = $o;
      }
  }
  foreach($_POST['serie'] as $s) {
      if($s == "") {
      }
      else {
          $vars['serie'][] = $s;
          $comp['serie'][] = $s;
      }
  }
  foreach($_POST['grafico'] as $g) {
      if($g == "") {
      }
      else {
          $vars['grafico'][] = $g;
          $comp['grafico'][] = $g;
      }
  }
  foreach($_POST['cor'] as $c) {
      if($c == "") {
      }
      else {
          $vars['cor'][] = $c;
          $comp['cor'][] = $c;
      }
  }
  $erro = 0;
  foreach($vars as $key => $v) {
        for($i = 0; $i < 9; $i++) {
            $princ = count($v);
            $vetor = count(current($comp));
            if($princ != $vetor) {
                $erro++;
            }
            else {

            }
            $vetor = next($comp);
        }
        $vetor = reset($comp);
  }
  if($tabelas == "" || $colunas == "" || $posicao == "" || $nomeapres == "" || $tipo == "") {
      $msg = "As informaÃ§Ãµes de ''Tabelas'', ''Colunas'', ''Posicao'' e ''Nome Apres.'' nÃ£o podem estar vazios!!!";
      header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msg=".$msg);
      break;
  }
  if($vars['posicao'][0] == "" || $vars['coluna'][0] == "" || $vars['tipo'][0] == "" || $vars['nomeapres'][0] == "" || $vars['tabelas'][0] == "" || $vars['ordena'][0] == "" || $vars['serie'][0] == "" || $vars['grafico'][0] == "" || $vars['cor'][0] == "") {
      $msg = "A primeira linha nÃ£o foi totalmente preenchida!!!";
      header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msg=".$msg);
      break;
  }
  if($erro >= 1) {
      $msg = "Existe inconsistencias no preenchimento das colunas para vinculo no relatÃ³rio!!!";
      header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msg=".$msg);
      break;
  }
  $col = "SELECT * FROM relmoni WHERE idrelmoni='$idrelmoni'";
  $ecol = $_SESSION['fetch_array']($_SESSION['query']($col)) or die (mysql_error());
  $countcol = "SELECT COUNT(*) as result FROM relmonicol WHERE idrelmoni='$idrelmoni'";
  $ecount = $_SESSION['fetch_array']($_SESSION['query']($countcol)) or die ("erro na query de consulta de quantidade de colunas relacionadas");
  foreach($vars['coluna'] as $key => $varcol) {
      if($varcol == "") {

      }
      else {
        $colnaorel = array();
        $selrelcol = "SELECT COUNT(*) as result FROM relmonicol WHERE idrelmoni='$idrelmoni' AND posicao='".$vars['posicao'][$key]."'";
        $eselrelcol = $_SESSION['fetch_array']($_SESSION['query']($selrelcol)) or die ("erro na query de consulta das colunas");
        if($eselrelcol['result'] >= 1) {
            $colnaorel[] = $varcol;
        }
        else {
            $colrel[$key] = $varcol;
        }
      }
  }
  if(!isset($colrel)) {
    $msg = "Todas colunas selecionas jÃ¡ estÃ£o presentes no relatÃ³rio ou possuem posicoes conflitantes!!!";
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msg=".$msg);
    break;
  }
  else {
    foreach($colrel as $keycol => $vcolrel) {
	$insertcol = "INSERT INTO relmonicol (idrelmoni,tabela,coluna,tipo,serie,grafico,cor,nomeapresenta,posicao,ordena) VALUES ('$idrelmoni','$tabelas[$keycol]','$vcolrel','$tipo[$keycol]',$serie[$keycol],$grafico[$keycol],$cor[$keycol],'".strtoupper(trim($nomeapres[$keycol]))."','$posicao[$keycol]','$ordena[$keycol]');";
	$einsert = $_SESSION['query']($insertcol) or die ("erro na inserÃ§Ã£o das colunas no relatÃ³rio");
    }
  }
  if($einsert) {
    if($colnaorel != "") {
      $colnaorel = implode(",",$colnaorel);
      $msg = "Relacionamento efetuado com sucesso e coluna/s ''$colnaorel'' nÃ£o relacionada/s";
    }
    else {
      $msg = "Relacionamento efetuado com sucesso!!!";
    }
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msg=".$msg);
    break;
  }
  else {
    $msg = "Erro no processo de relacionamento, favor contatar o administrador!!!";
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msg=".$msg);
    break;
  }
}

if(isset($_POST['altcol'])) {
  $idrelmoni = $_POST['idrelmoni'];
  $nome = strtoupper(trim($_POST['nomerel']));
  $id = $_POST['id'];
  $nomeapres = strtoupper(trim($_POST['nomeapres']));
  $tipo = $_POST['tipo'];
  $pos = $_POST['pos'];
  $ordena = $_POST['ordena'];
  $selnome = "SELECT COUNT(*) as result FROM relmonicol WHERE idrelmoni='$idrelmoni' AND idrelmonicol<>'$id' AND nomeapresenta='$nomeapres'";
  $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die ("erro na query de consulta do nomeapres");
  $verifalt = "SELECT * FROM relmonicol WHERE idrelmonicol='$id'";
  $everif = $_SESSION['fetch_array']($_SESSION['query']($verifalt)) or die ("erro na query de consulta do nomeapres do id selecionado");
  if($eselnome['result'] >= 1) {
    $msgi = "O nome de coluna informada jÃ¡ existe neste relatÃ³rio!!!";
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msgi=".$msgi);
    break;
  }
  if($everif['nomeapresenta'] == $nomeapres && $tipo == $everif['tipo'] && $pos == $everif['posicao'] && $ordena == $everif['ordena']) {
    $msgi = "Nenhum campo foi alterado, processo nÃ£o executado!!!";
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msgi=".$msgi);
    break;
  }
  else {
      $update = "UPDATE relmonicol SET nomeapresenta='$nomeapres', posicao='$pos', tipo='$tipo', ordena='$ordena' WHERE idrelmonicol='$id'";
      $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o da linha");
    if($eupdate) {
      $msgi = "AtualizaÃ§Ã£o executada com sucesso!!!";
      header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msgi=".$msgi);
      break;
    }
    else {
      $msgi = "Erro no processo de atualizaÃ§Ã£o, favor contatar o administrador!!!";
      header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msgi=".$msgi);
      break;
    }
  }
}

if(isset($_POST['apagacol'])) {
  $id = $_POST['id'];
  $idrelmoni = $_POST['idrelmoni'];
  $nome = $_POST['nomerel'];
  $delcol = "DELETE FROM relmonicol WHERE idrelmonicol='$id'";
  $edelcol = $_SESSION['query']($delcol) or die ("erro na query para apagar a coluna do relatÃ³rio");
  $incre = "ALTER TABLE relmonicol AUTO_INCREMENT=1";
  $eincre = $_SESSION['query']($incre) or die ("erro na query para reset do Auto Incremento");
  if($edelcol) {
    $msgi = "Coluna apagada do relatÃ³rio com sucesso!!!";
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msgi=".$msgi);
    break;
  }
  else {
    $msgi = "Erro no processo para apagar a coluna, favor contatar o administrador!!!";
    header("location:admin.php?menu=relmonicol&id=".$idrelmoni."&nome=".$nome."&msgi=".$msgi);
    break;
  }
}

?>