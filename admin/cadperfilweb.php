<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$nome = strtoupper(trim($_POST['nome']));
$nivel = $_POST['nivel'];
$visuext = $_POST['visuext'];
$areas = implode(",",$_POST['areas']);
$filtros = val_vazio($_POST['filtro_arvore']);

$caracter = "[]$[><}{)(:;,!?*%&#@]";

if(isset($_POST['cadastra'])) {
    $ativo = 'S';
    $selnome = "SELECT COUNT(*) as result FROM perfil_web WHERE nomeperfil_web='$nome'";
    $enome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
    if($nome == "" || $nivel == "") {
        $msg = "Nenhum dos campos poderão estar vazios para cadastro de filtro!!!";
        header("location:admin.php?menu=perweb&msg=".$msg);
    }
    else {
        if(eregi($caracter, $nome)) {
            $msg = "No nome do filtro não poderá existir caracteres invalidos ''$caracter''!!!";
            header("location:admin.php?menu=perweb&msg=".$msg);
        }
        else {
            if($enome['result'] >= 1) {
                $msg = "Já existe filtro cadastrado com este nome, favor informar outro!!!";
                header("location:admin.php?menu=perweb&msg=".$msg);
            }
            else {
                $cad = "INSERT INTO perfil_web (nomeperfil_web, nivel,visuext, areas, filtro_arvore, ativo) VALUE ('$nome', '$nivel','$visuext', '$areas',$filtros, '$ativo')";
                $ecad = $_SESSION['query']($cad) or die (mysql_error());
                if($ecad) {
                  $msg = "Filtro cadastro com sucesso!!!";
                  header("location:admin.php?menu=perweb&msg=".$msg);
                }
                else {
                  $msg = "Ocorreu um erro no cadastramento, favor contatar o administrador!!!";
                  header("location:admin.php?menu=perweb&msg=".$msg);
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $id = $_POST['id'];
    $ativo = $_POST['ativo'];
    $selnome = "SELECT COUNT(*) as result FROM perfil_web WHERE nomeperfil_web='$nome' AND idperfil_web<>'$id'";
    $enome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die ("erro na query de consulta das perfis cadastrados para iddentificar perfis iguais");
    $nid = "SELECT * FROM perfil_web WHERE idperfil_web='$id'";
    $enid = $_SESSION['fetch_array']($_SESSION['query']($nid)) or die ("erro na query de consulta do perfil");
    if($nome == "") {
        $msgi = "O campo ''Nome'' nÃ£o pode estar vazio para alteraÃ§Ã£o do perfil!!!";
        header("location:admin.php?menu=perweb&idperfil=".$id."&msg=".$msgi);
    }
    else {
        if($enome['result'] >= 1) {
              $msgi = "JÃ¡ existe filtro cadastrado com este nome ou filtro informado, favor inserir outro!!!";
              header("location:admin.php?menu=perweb&idperfil=".$id."&msg=".$msgi);
        }
        else {
            if($nome == $enid['nomeperfil_web'] && $areas == $enid['areas'] && $visuext == $enid['visuext'] && $ativo == $enid['ativo'] && $_POST['filtro_arvore'] == $enid['filtro_arvore']) {
                    $msgi = "Nenhum dado foi alterado, processo nÃ£o executado!!!";
                    header("location:admin.php?menu=perweb&idperfil=".$id."&msg=".$msgi);
            }
            else {
                $alter = "UPDATE perfil_web SET nomeperfil_web='$nome', nivel='$nivel',visuext='$visuext', areas='$areas', ativo='$ativo', filtro_arvore=$filtros WHERE idperfil_web='$id'";
                $ealter = $_SESSION['query']($alter) or die (mysql_error());
                if($ealter) {
                    $msgi = "Perfil ''$nome'' alterado com sucesso!!!";
                    header("location:admin.php?menu=perweb&idperfil=".$id."&msg=".$msgi);
                }
                else {
                    $msgi = "Erro no processamento da alteraÃ§Ã£o, favor contatar o administrador!!!";
                    header("location:admin.php?menu=perweb&idperfil=".$id."&msg=".$msgi);
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $id = $_POST['id'];
    $nome = $_POST['nome'];
    $selperfil = "SELECT COUNT(*) as result FROM user_web WHERE idperfil_web='$id'";
    $eperfil = $_SESSION['fetch_array']($_SESSION['query']($selperfil)) or die (mysql_error());
    if($eperfil['result'] >= 1) {
      $msgi = "O perfil nÃ£o ser apagado pois existem usuÃ¡rios relacionados Ã  ele!!!";
      header("location:admin.php?menu=perweb&msgi=".$msgi);
    }
    else {
        $del = "DELETE FROM perfil_web WHERE idperfil_web='$id'";
        $edel = $_SESSION['query']($del) or die (mysql_error());
        $alter = "ALTER TABLE perfil_web AUTO_INCREMENT=1";
        $ealter = $_SESSION['query']($alter) or die (mysql_error());
        if($edel) {
          $msgi = "Perfil apagado com sucesso!!!";
          header("location:admin.php?menu=perweb&msgi=".$msgi);
        }
        else {
          $msgi = "Erro na execuÃ§Ã£o do processo, favor contatar o administrador!!!";
          header("location:admin.php?menu=perweb&msgi=".$msgi);
        }
    }
}

if(isset($_POST['carrega'])) {
    $id = $_POST['id'];
    header("location:admin.php?menu=perweb&idperfil=".$id);
}

if(isset($_POST['novo'])) {
    header("location:admin.php?menu=perweb");
}
?>
