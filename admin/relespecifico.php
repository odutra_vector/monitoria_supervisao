<?php

$idrel = $_GET['idrel'];
if($idrel != "") {
    $selrel = "SELECT *,COUNT(*) as result FROM rel_resp_especifica WHERE idrel_resp_especifica='$idrel'";
    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die ("erro na query de consulta do relatório");
    $idsrel = explode(",",$eselrel['idrel_filtros']);
}
else {
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function() {
        $("#cadastrarel").click(function() {
            var nome = $("#nome").val();
            var qtde = $("#qtde").val();
            if(nome == "" || qtde == "") {
                alert('Favor preencher o campo nome e qtde itens');
                return false;
            }
            else {
            }
        })
        $("#alterarel").click(function() {
            var nome = $("#nome").val();
            var qtde = $("#qtde").val();
            if(nome == "" || qtde == "") {
                alert('Favor preencher o campo nome e qtde itens');
                return false;
            }
            else {
            }
        })
        $("#carregarel").click(function() {
           var idrel = $("#idrelespe").val();
           if(idrel == "") {
               alert("Favor informar o relatório que deseja carregar");
               return false;
           }
           else {
           }
        })
        $("#apagarel").click(function() {
           var idrel = $("#idrelespe").val();
           if(idrel == "") {
               alert("Favor informar o relatório que deseja apagar");
               return false;
           }
           else {
           }
        })
        $("#idrelespe").live('change',function() {
            var idrel = $(this).val();
            $("#listrel").load("/monitoria_supervisao/admin/carregarelespe.php",{idrel:idrel});
        })
        $("#cadastraresp").click(function() {
            var plan = $("#plan").val();
            var resp = $("#resp").val();
            var idrel = $("#idrelespe").val();
            if(plan == "" || resp == "" || idrel == "") {
                alert("O campo Relatório, Planilha e Resposta não podem estar vazios para relacionar respostas");
                return false;
            }
            else {
                
            }
        })
        $("input[id*='deletaresp']").live('click',function() {
            var idrel = $(this).attr('name');
            var del = confirm("Você deseja apagar este resposta?");
            if(del == true) {
                window.location = '/monitoria_supervisao/admin/delespecifico.php?idrel='+idrel;
            }
            else {
                return false;
            }
        })
        $("#relaciona").click(function() {
            var idrel = $("#idrelespe").val();
            if(idrel == "") {
                alert("Favor selecionar 1 relatório");
                return false;
            }
            else {
            }
        })
    })
</script>
<script type="text/javascript" src="/monitoria_supervisao/users/comboplan.js"></script>
</head>
<body>
<div id="conteudo">
    <div><br/>
        <form action="cadrelespecifico.php" method="post">
        <table width="480">
          <tr>
            <td class="corfd_ntab" colspan="2" align="center"><strong>RELATÓRIOS ESPECIFICOS</strong></td>
          </tr>
          <tr>
            <td width="61" class="corfd_coltexto"><strong>NOME</strong></td>
            <td width="202" class="corfd_colcampos"><input type="hidden" name="idrel" value="<?php if(isset($_GET['idrel'])) { echo $_GET['idrel'];} else {}?>"/> <input name="nome" maxlength="200" id="nome" type="text" style="width:200px;border: 1px solid #69C" value="<?php if(isset($_GET['idrel'])) { echo $eselrel['nome'];} else {}?>" /></td>
          </tr>
          <tr>
              <td class="corfd_coltexto"><strong>QTDE ITENS</strong></td>
              <td class="corfd_colcampos"><input type="text" name="qtde" id="qtde" value="<?php if(isset($_GET['idrel'])) { echo $eselrel['qtde'];} else {}?>" maxlength="2" style="width:50px;border: 1px solid #69C; text-align: center" /></td>
          </tr>
          <tr>
            <?php
            if(isset($_GET['idrel'])) {
            ?>
            <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="alterarel" id="alterarel" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novorel" id="novorel" type="submit" value="Novo" /></td>
            <?php
            }
            else {
            ?>
            <td width="201" colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastrarel" id="cadastrarel" type="submit" value="Cadastrar" /></td>
            <?php
            }
            ?>
          </tr>
        </table>
        <font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font>
        </form>
    </div><br/><br/>
    <form action="cadrelespecifico.php" method="post">
        <div style="border:2px solid #69C">
        <table width="auto">
          <tr>
            <td colspan="2" class="corfd_ntab" align="center"><strong>RELATÓRIOS CADASTRADOS</strong></td>
          </tr>
          <tr>
          	<td width="123" class="corfd_colcampos">
            	<select name="idrelespe" id="idrelespe">
                    <?php
                    if(isset($_GET['idrel'])) {
                        ?>
                        <option value="">SELECIONE...</option>
                        <?php
                    }
                    else {
                        ?>
                        <option value="" selected="selected" disabled="disabled">SELECIONE...</option>
                        <?php
                    }
                    $selrel = "SELECT * FROM rel_resp_especifica";
                    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relatório");
                    $nrel = $_SESSION['num_rows']($eselrel);
                    if($nrel == "") {
                    }
                    else {
                        while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                            if($lselrel['idrel_resp_especifica'] == $_GET['idrel']) {
                                echo "<option value=\"".$lselrel['idrel_resp_especifica']."\" selected=\"selected\">".$lselrel['nome']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lselrel['idrel_resp_especifica']."\">".$lselrel['nome']."</option>";
                            }
                        }
                    }
                    ?>
                </select>
            </td>
            <td width="160" class="corfd_colcampos"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="carregarel" id="carregarel" type="submit" value="Carregar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="apagarel" id="apagarel" type="submit" value="Apagar" /></td>
          </tr>
        </table>
        <font color="#FF0000"><strong><?php echo $_GET['msgd']; ?></strong></font>
        <br/>
        <table width="919">
          <tr>
            <td width="50" class="corfd_coltexto" align="center"><strong>ID</strong></td>
            <td width="245" class="corfd_coltexto" align="center"><strong>PLANILHA</strong></td>
            <td width="608" class="corfd_coltexto" align="center"><strong>REPOSTA</strong></td>
          </tr>
        </table>
        <div style="overflow:auto; height:200px;" id="listrel">
            <?php
            if(isset($_GET['idrel'])) {
                ?>
                <table width="1010">
                    <?php
                    $selresp = "SELECT re.id,descriplanilha,descriresposta FROM resp_especificas re
                    INNER JOIN planilha p ON p.idplanilha = re.idplanilha
                    INNER JOIN pergunta pe ON pe.idresposta = re.idresposta
                   WHERE idrel_resp_especifica='$idrel' GROUP BY re.idresposta ORDER BY re.idplanilha,re.idresposta";
                    $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das resposta relacionadas");
                    while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                        echo "<tr>";
                            echo "<td class=\"corfd_colcampos\" width=\"48\" align=\"center\">".$lselresp['id']."</td>";
                            echo "<td class=\"corfd_colcampos\" width=\"248\" align=\"center\">".$lselresp['descriplanilha']."</td>";
                            echo "<td class=\"corfd_colcampos\" width=\"607\">".$lselresp['descriresposta']."</td>";
                            echo "<td width=\"87\"><input type=\"button\" style=\"border:0px;width:15px;height:14px;outline:none; background-image:url(/monitoria_supervisao/images/exit.png)\" name=\"".$lselresp['id']."\" id=\"deletaresp".$lselresp['id']."\" /></td>";
                          echo "</tr>";
                    }
                    ?>
                </table>
            <?php
            }
            else {
            }
            ?>
      </div>
  </div><br/>
  <div style="width:500px;float:left">
    <table width="487">
      <tr>
        <td class="corfd_ntab" align="center" colspan="2"><strong>CADSASTRO PLANILHA/ RESPOSTA</strong></td>
      </tr>
      <tr>
    <td width="98" class="corfd_coltexto"><strong>PLANILHA</strong></td>
    <td width="377" class="corfd_colcampos"><select style="width:370px; border: 1px solid #333; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="plan" id="plan">
    <option value="" <?php if(isset($_POST['plan']) OR $varget['plan'] != "") { } else { echo "selected=\"selected\""; }?> disabled="disabled" style="height:15px;padding:5px">Selecione uma planilha</option>
    <?php
    	$iduser = $_SESSION['usuarioID'];
        if($_SESSION['user_tabela'] == "user_web") {
          $selplan = "SELECT * FROM userwebfiltro INNER JOIN conf_rel ON conf_rel.idrel_filtros = userwebfiltro.idrel_filtros WHERE iduser_web='$iduser'";
        }
        if($_SESSION['user_tabela'] == "user_adm") {
          $selplan = "SELECT * FROM useradmfiltro INNER JOIN conf_rel ON conf_rel.idrel_filtros = useradmfiltro.idrel_filtros WHERE iduser_adm='$iduser'";
        }
        $eselplan = $_SESSION['query']($selplan) or die ("erro na query de consulta das planilhas");
        $listplan = array();
        while($lselplan = $_SESSION['fetch_array']($eselplan)) {
        $plans = explode(",",$lselplan['idplanilha_web']);
        foreach($plans as $p) {
            $listplan[] = $p;
        }
	foreach($plans as $plan) {
            $vincplan = "SELECT planvinc FROM vinc_plan WHERE idplanilha='$plan'";
            $evincplan = $_SESSION['query']($vincplan) or die ("erro na query de consutla das planilha vinculadas");
            while($lvincplan = $_SESSION['fetch_array']($evincplan)) {
                $planvinc[] = $evincplan['planvinc'];
            }
            if($planvinc != "") {
                $listplan = array_merge($listplan, $planvinc);
            }
            else {
            }
            $listplan = array_unique($listplan);
                if(in_array($plan,$listplan)) {
                }
                else {
                    $listplan[] = $plan;
                }
	}
	}
	foreach($listplan as $p) {
                        $nplan = "SELECT DISTINCT(idplanilha), COUNT(idplanilha) as result, descriplanilha FROM planilha WHERE idplanilha='$p' AND ativoweb='S'";
                        $enplan = $_SESSION['fetch_array']($_SESSION['query']($nplan)) or die ("erro na query de consulta do nome da planilha");
                        if($enplan['result'] == 0) {
                        }
                        if($enplan['result'] >= 1) {
                            if($p == $_POST['plan'] OR $p == $varget['plan']) {
                               $select = "selected=\"selected\"";
                            }
                            else {
                               $select = "";
                            }
                            echo "<option value=\"".$p."\" $select style=\"height:15px;padding:5px\">".$enplan['descriplanilha']."</option>";
                        }
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>AVALIAÇÃO</strong></td>
    <td class="corfd_colcampos"><select style="width:370px; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="aval" id="aval">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione uma Avaliação</option>
    <?php
      if(!empty($_POST['aval'])) {
            $varplan = $_POST['plan'];
            $selnaval = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$_POST['aval']."'";
            $enaval = $_SESSION['fetch_array']($_SESSION['query']($selnaval)) or die ("erro na query de consulta do nome da avaliação");
            echo "<option value=\"".$_POST['aval']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enaval['nomeaval_plan']."</option>";
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='".$_POST['plan']."'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
              $idavaldb[] = $lselaval['idaval_plan'];
              $nomeavaldb[] = $lselaval['nomeaval_plan'];
            }
            $avaliaplan = array_combine($idavaldb, $nomeavaldb);
            foreach($avaliaplan as $id => $nome) {
              if($id == $_POST['aval']) {
              }
              else {
               echo "<option value=\"".$id."\" style=\"height:15px;padding:5px\">".$nome."</option>";
              }
            }
      }
      if($varget['aval'] != "") {
            $varplan = $varget['plan'];
            $selnaval = "SELECT nomeaval_plan FROM aval_plan WHERE idaval_plan='".$varget['aval']."'";
            $enaval = $_SESSION['fetch_array']($_SESSION['query']($selnaval)) or die ("erro na query de consulta do nome da avaliação");
            echo "<option value=\"".$varget['aval']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enaval['nomeaval_plan']."</option>";
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='".$varget['plan']."'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
              $idavaldb[] = $lselaval['idaval_plan'];
              $nomeavaldb[] = $lselaval['nomeaval_plan'];
            }
            $avaliaplan = array_combine($idavaldb, $nomeavaldb);
            foreach($avaliaplan as $id => $nome) {
              if($id == $varget['aval']) {
              }
              else {
               echo "<option value=\"".$id."\" style=\"height:15px;padding:5px\">".$nome."</option>";
              }
            }
      }
      if(empty($_POST['aval']) AND $varget['aval'] == "" AND (!empty($_POST['plan']) OR $varget['plan'] != "")) {
          if(!empty ($_POST['plan'])) {
              $varplan = $_POST['plan'];
          }
          if($varget['plan'] != "") {
              $varplan = $varget['plan'];
          }
            $selaval = "SELECT DISTINCT(aval_plan.idaval_plan), aval_plan.nomeaval_plan FROM planilha INNER JOIN aval_plan ON aval_plan.idaval_plan = planilha.idaval_plan WHERE planilha.idplanilha='$varplan'";
            $eselaval = $_SESSION['query']($selaval) or die ("erro na consulta das avaliações cadastradas");
            while($lselaval = $_SESSION['fetch_array']($eselaval)) {
                echo "<option value=\"".$lselaval['idaval_plan']."\" style=\"height:15px;padding:5px\">".$lselaval['nomeaval_plan']."</option>";
            }
      }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>GRUPO</strong></td>
    <td class="corfd_colcampos"><select style="width:370px; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="grupo" id="grupo">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione um Grupo</option>
    <?php
      if(!empty($_POST['grupo'])) {
            $varaval = $_POST['aval'];
            $selngrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='".$_POST['grupo']."'";
            $engrupo = $_SESSION['fetch_array']($_SESSION['query']($selngrupo)) or die ("erro na query para consulta do nome do grupo");
            echo "<option value=\"".$_POST['grupo']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$engrupo['descrigrupo']."</option>";
            $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idaval_plan='".$_POST['aval']."'";
            $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
            while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              $idgrupo[] = $lselgrupo['idgrupo'];
              $nomegrupo[] = $lselgrupo['descrigrupo'];
            }
            $grupoidnome = array_combine($idgrupo, $nomegrupo);
            foreach($grupoidnome as $idg => $nomeg) {
              if($idg == $_POST['grupo']) {
              }
              else {
                echo "<option value=\"".$idg."\" style=\"height:15px;padding:5px\">".$nomeg."</option>";
              }
            }
      }
      if($varget['grupo'] != "") {
            $varaval = $_POST['aval'];
            $selngrupo = "SELECT descrigrupo FROM grupo WHERE idgrupo='".$varget['grupo']."'";
            $engrupo = $_SESSION['fetch_array']($_SESSION['query']($selngrupo)) or die ("erro na query para consulta do nome do grupo");
            echo "<option value=\"".$varget['grupo']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$engrupo['descrigrupo']."</option>";
            $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idaval_plan='".$varget['aval']."'";
            $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
            while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              $idgrupo[] = $lselgrupo['idgrupo'];
              $nomegrupo[] = $lselgrupo['descrigrupo'];
            }
            $grupoidnome = array_combine($idgrupo, $nomegrupo);
            foreach($grupoidnome as $idg => $nomeg) {
              if($idg == $varget['grupo']) {
              }
              else {
                echo "<option value=\"".$idg."\" style=\"height:15px;padding:5px\">".$nomeg."</option>";
              }
            }
      }
      if(empty($_POST['grupo']) AND $varget['grupo'] == "" AND (!empty ($_POST['aval']) OR $varget['aval'] != "")) {
          if(!empty ($_POST['aval'])) {
              $varaval = $_POST['aval'];
          }
          if($varget['aval'] != "") {
              $varaval = $varget['aval'];
          }
          $selgrupo = "SELECT DISTINCT(grupo.idgrupo), grupo.descrigrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo WHERE planilha.idplanilha='".$varplan."' AND planilha.idaval_plan='".$varaval."'";
	  $eselgrupo = $_SESSION['query']($selgrupo) or die ("erro na query de consulta dos grupos relacionados");
	  while($lselgrupo = $_SESSION['fetch_array']($eselgrupo)) {
              echo "<option value=\"".$lselgrupo['idgrupo']."\" style=\"height:15px;padding:5px\">".$lselgrupo['descrigrupo']."</option>";
          }
      }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>SUB-GRUPO</strong></td>
    <td class="corfd_colcampos"><select style="width:370px; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="sub" id="sub">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione um Sub-Grupo</option>
    <?php
      if(!empty($_POST['sub'])) {
            $vargrupo = $_POST['grupo'];
            $selnsub = "SELECT descrisubgrupo FROM subgrupo WHERE idsubgrupo='".$_POST['sub']."'";
            $ensub = $_SESSION['fetch_array']($_SESSION['query']($selnsub)) or die ("erro na query de consulta do sub-grupo");
            echo "<option value=\"".$_POST['sub']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$ensub['descrisubgrupo']."</option>";
            $selsub = "SELECT DISTINCT(subgrupo.idsubgrupo), subgrupo.descrisubgrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                       WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."'";
            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos sub-grupo");
            while($lselsub = $_SESSION['fetch_array']($eselsub)) {
              $idsub[] = $lselsub['idsubgrupo'];
              $nomesel[] = $lselsub['descrisubgrupo'];
            }
            $grupoidsub = array_combine($idsub, $nomesel);
            foreach($grupoidsub as $ids => $nomes) {
              if($ids == $_POST['sub']) {
              }
              else {
                echo "<option value=\"".$ids."\" style=\"height:15px;padding:5px\">".$nomes."</option>";
              }
            }
      }
      if($varget['sub'] != "") {
            $vargrupo = $_POST['grupo'];
            $selnsub = "SELECT descrisubgrupo FROM subgrupo WHERE idsubgrupo='".$varget['sub']."'";
            $ensub = $_SESSION['fetch_array']($_SESSION['query']($selnsub)) or die ("erro na query de consulta do sub-grupo");
            echo "<option value=\"".$varget['sub']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$ensub['descrisubgrupo']."</option>";
            $selsub = "SELECT DISTINCT(subgrupo.idsubgrupo), subgrupo.descrisubgrupo FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                       WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."'";
            $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos sub-grupo");
            while($lselsub = $_SESSION['fetch_array']($eselsub)) {
              $idsub[] = $lselsub['idsubgrupo'];
              $nomesel[] = $lselsub['descrisubgrupo'];
            }
            $grupoidsub = array_combine($idsub, $nomesel);
            foreach($grupoidsub as $ids => $nomes) {
              if($ids == $varget['sub']) {
              }
              else {
                echo "<option value=\"".$ids."\" style=\"height:15px;padding:5px\">".$nomes."</option>";
              }
            }
      }
      if(empty ($_POST['sub']) AND $varget['sub'] == "" AND (!empty($_POST['grupo']) OR $varget['grupo'] != "")) {
          if(!empty ($_POST['grupo'])) {
              $vargrupo = $_POST['grupo'];
          }
          if($varget['grupo'] != "") {
              $vargrupo = $varget['grupo'];
          }
          $selsub = "SELECT DISTINCT(idrel), subgrupo.descrisubgrupo, planilha.idgrupo, grupo.filtro_vinc FROM planilha
	      INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	      INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
	      WHERE planilha.idplanilha='$varplan' AND planilha.idaval_plan='$varaval' AND planilha.idgrupo='$vargrupo' AND filtro_vinc='S'";
          $eselsub = $_SESSION['query']($selsub) or die ("erro na query de consulta dos sub-grupo");
          while($lselsub = $_SESSION['fetch_array']($eselsub)) {
            echo "<option value=\"".$lselsub['idrel']."\" style=\"height:15px;padding:5px\">".$lselsub['descrisubgrupo']."</option>";
          }
      }
    ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>PERGUNTA</strong></td>
    <td class="corfd_colcampos"><select style="width:370px; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="perg" id="perg">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione uma Pergunta</option>
    <?php
    if(!empty($_POST['perg'])) {
      if(!empty ($_POST['grupo'])) {
          $vargrupo = $_POST['grupo'];
          $varperg = $_POST['perg'];
      }
      if($varget['grupo'] != "") {
          $vargrupo = $varget['grupo'];
          $varperg = $varget['perg'];
      }
      $selnperg = "SELECT descripergunta FROM pergunta WHERE idpergunta='".$_POST['perg']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de consulta do nome da pergunta");
      echo "<option value=\"".$_POST['perg']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enperg['descripergunta']."</option>";
      if(!empty($_POST['sub'])) {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha
                     INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                     INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
                     WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."' AND subgrupo.idsubgrupo='".$_POST['sub']."'";
      }
      else {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                     INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                     WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."'";
      }
      $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
      while($lselperg = $_SESSION['fetch_array']($eselperg)) {
	$idperg[] = $lselperg['idpergunta'];
	$nomeperg[] = $lselperg['descripergunta'];
      }
      $pergid = array_combine($idperg, $nomeperg);
      foreach($pergid as $idp => $nomep) {
          if($idp == $_POST['perg']) {
          }
          else {
            echo "<option value=\"".$idp."\" style=\"height:15px;padding:5px\">".$nomep."</option>";
          }
      }
    }
    if($varget['perg'] != "") {
      if(!empty ($_POST['grupo'])) {
          $vargrupo = $_POST['grupo'];
          $varperg = $_POST['perg'];
      }
      if($varget['grupo'] != "") {
          $vargrupo = $varget['grupo'];
          $varperg = $varget['perg'];
      }
      $selnperg = "SELECT descripergunta FROM pergunta WHERE idpergunta='".$varget['perg']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de consulta do nome da pergunta");
      echo "<option value=\"".$varget['perg']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enperg['descripergunta']."</option>";
      if(!empty($varget['sub'])) {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha
                     INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                     INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
                     WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."' AND subgrupo.idsubgrupo='".$varget['sub']."'";
      }
      else {
          $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                     INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                     WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."'";
      }
      $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
      while($lselperg = $_SESSION['fetch_array']($eselperg)) {
	$idperg[] = $lselperg['idpergunta'];
	$nomeperg[] = $lselperg['descripergunta'];
      }
      $pergid = array_combine($idperg, $nomeperg);
      foreach($pergid as $idp => $nomep) {
          if($idp == $varget['perg']) {
          }
          else {
            echo "<option value=\"".$idp."\" style=\"height:15px;padding:5px\">".$nomep."</option>";
          }
      }
    }
    if(empty ($_POST['sub'])) {
        if(empty($_POST['perg']) AND $varget['perg'] == "" AND (!empty($_POST['grupo']) OR $varget['grupo'] != "")) {
            if(!empty ($_POST['perg'])) {
              $varperg = $_POST['perg'];
            }
            if($varget['perg'] != "") {
              $varperg = $varget['perg'];
            }
            $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                                INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                                WHERE planilha.idplanilha='$varplan' AND planilha.idgrupo='$vargrupo'";
            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                echo "<option value=\"".$lselperg['idpergunta']."\" style=\"height:15px;padding:5px\">".$lselperg['descripergunta']."</option>";
            }
        }
    }
    if(!empty ($_POST['sub'])) {
        if(empty($_POST['perg']) AND $varget['perg'] == "" AND (!empty($_POST['sub']) OR $varget['sub'] != "")) {
            if(!empty ($_POST['perg'])) {
              $varperg = $_POST['perg'];
            }
            if($varget['perg'] != "") {
              $varperg = $varget['perg'];
            }
            $selperg = "SELECT DISTINCT(pergunta.idpergunta), pergunta.descripergunta FROM planilha
                                 INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                                 INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
                                 INNER JOIN pergunta ON pergunta.idpergunta = subgrupo.idpergunta
                                WHERE planilha.idplanilha='$varplan' AND planilha.idgrupo='$vargrupo' AND subgrupo.idsubgrupo='$varsub'";
            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                echo "<option value=\"".$lselperg['idpergunta']."\" style=\"height:15px;padding:5px\">".$lselperg['descripergunta']."</option>";
            }
        }
    }
   ?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>RESPOSTA</strong></td>
    <td class="corfd_colcampos"><select style="width:370px; border: 1px solid #69C;height:30px; padding:5px; background-color:#FFF;border-radius:5px" name="resp" id="resp">
    <option selected="selected" disabled="disabled" value="" style="height:15px;padding:5px">Selecione uma Resposta</option>
    <?php
    if(!empty($_POST['resp'])) {
      $selnperg = "SELECT descriresposta FROM pergunta WHERE idresposta='".$_POST['resp']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de para selecionar o nome");
      echo "<option value=\"".$_POST['resp']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enperg['descriresposta']."</option>";
      $selresp = "SELECT * FROM planilha
                          INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                          INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                          WHERE planilha.idplanilha='".$_POST['plan']."' AND planilha.idgrupo='".$_POST['grupo']."' AND pergunta.idpergunta='".$_POST['perg']."'";
      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
      while($lselresp = $_SESSION['fetch_array']($eselresp)) {
	$idresp[] = $lselresp['idresposta'];
	$nomeresp[] = $lselresp['descriresposta'];
      }
      $respid = array_combine($idresp, $nomeresp);
      foreach($respid as $idr => $nomer) {
          if($idr == $_POST['resp']) {
          }
          else {
            echo "<option value=\"".$idr."\" style=\"height:15px;padding:5px\">".$nomer."</option>";
          }
      }
    }
    if($varget['resp'] != "") {
      $selnperg = "SELECT descriresposta FROM pergunta WHERE idresposta='".$varget['resp']."'";
      $enperg = $_SESSION['fetch_array']($_SESSION['query']($selnperg)) or die ("erro na query de para selecionar o nome");
      echo "<option value=\"".$varget['resp']."\" selected=\"selected\" style=\"height:15px;padding:5px\">".$enperg['descriresposta']."</option>";
      $selresp = "SELECT * FROM planilha
                          INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                          INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                          WHERE planilha.idplanilha='".$varget['plan']."' AND planilha.idgrupo='".$varget['grupo']."' AND pergunta.idpergunta='".$varget['perg']."'";
      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
      while($lselresp = $_SESSION['fetch_array']($eselresp)) {
	$idresp[] = $lselresp['idresposta'];
	$nomeresp[] = $lselresp['descriresposta'];
      }
      $respid = array_combine($idresp, $nomeresp);
      foreach($respid as $idr => $nomer) {
          if($idr == $varget['resp']) {
          }
          else {
            echo "<option value=\"".$idr."\" style=\"height:15px;padding:5px\">".$nomer."</option>";
          }
      }
    }
    if(empty ($_POST['resp']) AND $varget['resp'] == "" AND (!empty ($_POST['perg']) OR $varget['perg'] != "")) {
        $selresp = "SELECT * FROM planilha
                          INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
                          INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
                          WHERE planilha.idplanilha='$varplan' AND planilha.idgrupo='$vargrupo' AND pergunta.idpergunta='$varperg'";
      $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
      while($lselresp = $_SESSION['fetch_array']($eselresp)) {
          echo "<option value=\"".$lselresp['idresposta']."\" style=\"height:15px;padding:5px\">".$lselresp['descriresposta']."</option>";
      }
    }
  ?>
    </select></td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastraresp" id="cadastraresp" type="submit" value="Cadastrar" /></td>
  </tr>
    </table><font color="#FF0000"><strong><?php echo $_GET['msgre']; ?></strong></font>
  </div>
  <div style="width:500px; float: right;">
  	<table width="500">
      <tr>
        <td class="corfd_ntab" align="center"><strong>RELACIONAMENTOS</strong></td>
      </tr>
    </table>
    <div style="width:500px; float: right; overflow:auto; height:250px; font-size:9px">
    	<table width="500">
          <tr>
            <td>
            	<select name="idrel_filtros[]" id="idrel_fitros" multiple="multiple" style="height:200px; width:490px; font-size:9px">
                	<?php
                    $rels = arvoreescalar();
                    foreach($rels as $idrel) {
                            $verifatv = "SELECT COUNT(*) as result FROM conf_rel WHERE idrel_filtros='$idrel' AND ativo='S'";
                            $everifatv = $_SESSION['fetch_array']($_SESSION['query']($verifatv)) or die ("erro na query de consulta do relacionamento");
                            if($everifatv['result'] >= 1) {
                                if(in_array($idrel,$idsrel)) {
                                    ?>
                                    <option value="<?php echo $idrel;?>" selected="selected" style="font-size:9px"><?php echo nomeapres($idrel);?></option>
                                    <?php
                                }
                                else {
                                    ?>
                                    <option value="<?php echo $idrel;?>"><?php echo nomeapres($idrel);?></option>
                                    <?php	
                                }
                            }
                            else {
                            }
                    }
                    ?>
                </select>
            </td>
          </tr>
          <tr>
          	<td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="relaciona" id="relaciona" type="submit" value="Relacionar" /></td>
          </tr>
        </table><font color="#FF0000"><strong><?php echo $_GET['msgf']; ?></strong></font>
    </div>
  </div>
  <div style="clear:both"></div>
  </form>
</div>
</body>
</html>
