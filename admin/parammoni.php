<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_GET['idparam'])) {
    $idparam = $_GET['idparam'];
    $selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
    $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die ("erro na query de consulta do parametro consultado");
}
else {
    
}
?>

<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   $("#hpa").mask("99:99:99");
   $("#hmonitor").mask("99:99:99");
   $("#colunas").tablesorter();
   $('#type').change(function() {
       var tipo = $(this).val();
       if(tipo == "TIME" || tipo == "DATE") {
           $('#caracter').attr('readonly','readonly');
       }
       else {
           $('#caracter').removeAttr('readonly');
       }
   })
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    <?php
    if(isset($_GET['idparam'])) {
        if($eselparam['tipoimp'] == "LA") {
            echo "$('tr.tpcompress').show();";
            echo "$('tr.arquivo').show();";
        }
        if($eselparam['tipoimp'] == "A") {
            echo "$('tr.tpcompress').show();";
            echo "$('tr.tparquivo').hide();";
        }
        if($eselparam['tipoimp'] == "I") {
            echo "$('tr.tpcompress').hide();";
            echo "$('tr.tparquivo').show();";
        }
        if($eselparam['tipoimp'] == "L") {
            echo "$('tr.tpcompress').hide();";
            echo "$('tr.tparquivo').show();";
        }
        if($eselparam['tipoimp'] == "SL") {
            echo "$('tr.tpcompress').hide();";
            echo "$('tr.tparquivo').hide();";
        }
    }
    else {
        echo "$('tr.tpcompress').hide();";
        echo "$('tr.tparquivo').hide();";
    }
    ?>
        $("[id*=tipomoni]").change(function() {
            var tipomoni = $("#tipomoni").val();
            if(tipomoni == "G") {
                $("#tipoimp").html('<option value="" disabled="disabled" selected="selected">Selecione...</option><option value="S">SELEÇÃO</option><option value="LA">LISTA AUDIO</option><option value="A">LEITURA AUDIO</option><option value="I">AUDIO INTERNET</option>');
            }
            if(tipomoni == "O") {
                $("#tipoimp").html('<option value="" disabled="disabled" selected="selected">Selecione...</option><option value="L">LISTA</option><option value="SL">SEM LISTA</option>');
            }
         });
         
         $("#tipoimp").change(function() {
             var tipoimp = $("#tipoimp").val();
             if(tipoimp == "LA") {
                 $('tr.tpcompress').show();
                 $('tr.tparquivo').show();
             }
             if(tipoimp == "A") {
                 $('tr.tpcompress').show();
                 $('tr.tparquivo').hide();
             }
             if(tipoimp == "I") {
                 $('tr.tpcompress').hide();
                 $('tr.tparquivo').show();
             }
             if(tipoimp == "L") {
                 $('tr.tpcompress').hide();
                 $('tr.tparquivo').show();
             }
             if(tipoimp == "SL") {
                 $('tr.tpcompress').hide();
                 $('tr.tparquivo').hide();
             }
         });
		 
          $('.ftp').hide();
          <?php
          if($eselparam['tiposervidor'] == "FTP") {
                  echo "$('.ftp').show();\n";
          }
          else {
          }
          ?>
          $('#tiposervidor').change(function() {
                 var server = $(this).val();
                 if(server == "FTP") {
                         $('.ftp').show();
                 }
                 else {
                         $('.ftp').hide();
                 }
          });

         $('#cadparam').submit(function() {
             var indice = $('#indice').val();
             var tipomoni = $('#tipomoni').val();
             var tipoimp = $('#tipoimp').val();
             var camaudio = $('#camaudio').val();
             var ccampos = $('#ccampos').val();
             var cpartes = $('#cpartes').val();
             var tipoaudio = $('#tipoaudio').val();
             var tpcompress = $('#tpcompress').val();
             var tparquivo = $('#tparquivo').val();
             var tiposerver = $('#tiposervidor').val();
             var ip = $('#endereco').val();
             var loginftp = $('#loginftp').val();
             var senhaftp = $('#senhaftp').val();
             var conf = $('#conf').val();
             var hpa = $('#hpa').val();
             var hmonitor = $('hmonitor').val();
             var dataimp = $('#dataimp').val();
             var ultmoni = $('#ultmoni').val();
             var datafim = $('#datafim').val();
             var dultimp = $('#dultimp').val();
             var limmulti = $('#limmult').val();
             if(tipoimp == "LA") {
                 if(indice == "" && tipomoni == "" && tipoimp == "" || camaudio == "" && ccampos == "" && cpartes == "" && tipoaudio == "" && tpcompress == "" && tparquivo == "" && conf == "" && hpa == "" && hmonitor == "") {
                     alert('Os campos abaixo nÃ£o podem estar vazios\n\
                        Indice\n\
                        Tipo Monitoria\n\
                        Tipo Importação\n\
                        Caminho Audios\n\
                        Separa Campos\n\
                        Separa Partes\n\
                        Tipo Audio\n\
                        Tipo CompressÃ£o\n\
                        Tipo Arquivo\n\
                        Conferencia\n\
                        Horas PA\n\
                        Horas Monitor');
                        return false;
                 }
             }
             if(tipoimp == "SL") {
                 if(indice == "" && tipomoni == "" && tipoimp == "" || camaudio == "" && ccampos == "" && cpartes == "" && tipoaudio == "" && conf == "" && hpa == "" && hmonitor == "") {
                     alert('Os campos abaixo nÃ£o podem estar vazios\n\
                        Indice\n\
                        Tipo Monitoria\n\
                        Tipo Importação\n\
                        Caminho Audios\n\
                        Separa Campos\n\
                        Separa Partes\n\
                        Tipo Audio\n\
                        Conferencia\n\
                        Horas PA\n\
                        Horas Monitor');
                        return false;
                 }
             }
             if(tipoimp == "A" || tipoimp == "I" || tipoimp == "L") {
                 if(indice == "" && tipomoni == "" && tipoimp == "" || camaudio == "" && ccampos == "" && cpartes == "" && tipoaudio == "" && tpcompress == "" && conf == "" && hpa == "" && hmonitor == "") {
                     alert('Os campos abaixo nÃ£o podem estar vazios\n\
                        Indice\n\
                        Tipo Monitoria\n\
                        Tipo Importação\n\
                        Caminho Audios\n\
                        Separa Campos\n\
                        Separa Partes\n\
                        Tipo Audio\n\
                        Tipo CompressÃ£o\n\
                        Conferencia\n\
                        Horas PA\n\
                        Horas Monitor');
                        return false;
                 }
             }
             if(tipoimp == "") {
                 if(indice == "" && tipomoni == "" && tipoimp == "" || camaudio == "" && ccampos == "" && cpartes == "" && tipoaudio == "" && tpcompress == "" && tparquivo == "" && conf == "" && hpa == "" && hmonitor == "") {
                     alert('Os campos abaixo nÃ£o podem estar vazios\n\
                            Indice\n\
                            Tipo Monitoria\n\
                            Tipo Importação\n\
                            Caminho Audios\n\
                            Separa Campos\n\
                            Separa Partes\n\
                            Tipo Audio\n\
                            Tipo CompressÃ£o\n\
                            Tipo Arquivo\n\
                            Conferencia\n\
                            Horas PA\n\
                            Horas Monitor');
                     return false;
                 }
             }
             if(dataimp == "S" && $('#pridataimp').val() == "0") {
                 alert('É necessÃ¡rio informar a ordem do filtro ativado');
                 return false;
             }
             if(ultmoni == "S" && $('#priultmoni').val() == "0") {
                 alert('É necessÃ¡rio informar a ordem do filtro ativado');
                 return false;
             }
             if(datafim == "S" && $('#pridatadim').val() == "0") {
                 alert('É necessÃ¡rio informar a ordem do filtro ativado');
                 return false;
             }
             if(dultimp == "S" && $('#pridultimp').val() == "0") {
                 alert('É necessÃ¡rio informar a ordem do filtro ativado');
                 return false;
             }
             if(datafim == "S" && dultimp == "") {
                 alert('Quando a data da ultima importaÃ§Ã£o Ã© considerada como filtro Ã© necessÃ¡rio informar a quantidade de dias retroativos do contato');
                 return false;
             }
             if(tiposerver == "FTP") {
                 if(ip == "" || loginftp == "" || senhaftp == "") {
                     alert('Quando a conexao for por FTP, os campos Endereco,Login,Senha devem estar preenchidos');
                     return false;
                 }
                 else {
                 }
             }
         });
    })
</script>
<div id="conteudo" class="corfd_pag">
<form action="cadparammoni.php" method="post">
<table width="583" border="0" style="border:1px solid #999">
  <tr>
    <td class="corfd_ntab" colspan="2" align="center"><strong>PARAMETROS DADOS OPERADOR</strong></td>
  </tr>
  <tr>
    <td width="201" class="corfd_coltexto"><strong>Incorpora</strong></td>
    <td width="372" class="corfd_colcampos"><select name="incorp" id="incorp">
        <option value="SUPER_OPER">SUPERVISOR</option>
        <option value="OPERADOR">OPERADOR</option>
        <option value="DADOS">DADOS</option>
    </select></td>
  </tr>
  <tr>
    <td width="201" class="corfd_coltexto"><strong>Dado de Entrada</strong></td>
    <td width="372" class="corfd_colcampos"><input style="width:200px; border: 1px solid #69C" maxlength="100" name="coluna" id="coluna" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tipo do dado BD</strong></td>
    <td class="corfd_colcampos">
        <select name="type" id="type">
            <option selected="selected" disabled="disabled" value="">Selecione uma opção</option>
            <option value="VARCHAR">VACHAR</option>
            <option value="TEXT">TEXT</option>
            <option value="INT">INT</option>
            <option value="DATE">DATE</option>
            <option value="TIME">TIME</option>
        </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tamanho do Campo</strong></td>
    <td class="corfd_colcampos"><input style="width:50px; border: 1px solid #69C; text-align:center" name="tamanho" id="tamanho" maxlength="4" type="text" /><strong><font color="#FF0000"> campos utilizado somente para TEXTO/ NUMERO</font></strong></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Catacteres Inválidos</strong></td>
    <td class="corfd_colcampos"><input style="width:150px; border: 1px solid #69C; text-align:center" name="caracter" id="caracter" type="text" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Restrição</strong></td>
    <td class="corfd_colcampos"><input style="width:150px; border: 1px solid #69C; text-align:center" name="restricao" id="restricao" type="text" /> <span style="color: #F00; font-weight: bold; font-size: 13px">* separar os textos por ","</span></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Campo Único</strong></td>
    <td class="corfd_colcampos"><select name="unico">
    <option value="N">N</option>
    <option value="S">S</option>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Exportação</strong></td>
    <td class="corfd_colcampos"><select name="export">
    <option value="0">N</option>
    <option value="1">S</option>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Nome Gravação</strong></td>
    <td class="corfd_colcampos"><select name="ngravacao">
    <option value="0">N</option>
    <option value="1">S</option>
    </select></td>
  </tr>
  <tr>
  	<td colspan="2"><input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" readonly="readonly" name="cadcampos" id="cadcampos" type="submit" value="Cadastrar" /></td>
  </tr>
</table>
<font color="#FF0000"><strong><?php echo $_GET['msgo']; ?></strong></font>
</form><br/><br/>
<div>
     <table width="850" border="0">
       <tr>
         <td class="corfd_ntab" colspan="7" align="center"><strong>CAMPOS CADASTRADOS</strong></td>
       </tr>
     </table>
</div>
<div style="height:300px; overflow:auto; border: 1x solid #000;">
<table width="900" border="0" id="colunas">
    <thead>
      <tr>
        <th class="corfd_coltexto" align="center" width="36"><strong>ID</strong></th>
        <th class="corfd_coltexto" align="center" width="90"><strong>Incorpora</strong></th>
        <th class="corfd_coltexto" align="center" width="185px"><strong>Coluna</strong></th>
        <th width="88" align="center" class="corfd_coltexto"><strong>Tipo</strong></th>
        <th width="30px" align="center" class="corfd_coltexto"><strong>Único</strong></th>
        <th width="30px" align="center" class="corfd_coltexto"><strong>Exportação</strong></th>
        <th width="30px" align="center" class="corfd_coltexto"><strong>N. Gravação</strong></th>
        <th width="57px" align="center" class="corfd_coltexto"><strong>Tamanho</strong></th>
        <th width="215px" align="center" class="corfd_coltexto"><strong>Caracter Inválido</strong></th>
        <th width="215px" align="center" class="corfd_coltexto"><strong>Restrições</strong></th>
      </tr>
    </thead>
    <tbody>
  <?php
  $ops = array('0' => 'N','1' => 'S');
  $colsnotread = array("idoperador","idsuper_oper","operador","super_oper","dtbase","horabase",'cpfoper','cpfsuper','datactt','horactt');
  $selcolunas = "SELECT * FROM coluna_oper";
  $ecolunas = $_SESSION['query']($selcolunas) or die (mysql_error());
  while($lcolunas = $_SESSION['fetch_array']($ecolunas)) {
      if(in_array($lcolunas['nomecoluna'],$colsnotread)) {
          $read = "readonly=\"readonly\"";
      }
      else {
          $read = "";
      }
      echo "<form action=\"cadparammoni.php\" method=\"POST\">";
      echo "<tr>";
            echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:38px; border: 1px solid #FFF; text-align:center\" readonly=\"readonly\" type=\"hidden\" name=\"id\" value=\"".$lcolunas['idcoluna_oper']."\">".$lcolunas['idcoluna_oper']."</td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\">".$lcolunas['incorpora']."</td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:150px; border: 1px solid #FFF; text-align:center\" name=\"coluna\" type=\"text\" $read value=\"".$lcolunas['nomecoluna']."\" /></td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\">".$lcolunas['tipo']."</td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\">".$lcolunas['unico']."</td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\"><select name=\"export\" id=\"export\">";
            foreach($ops as $kop => $op) {
                if($lcolunas['export'] == $kop) {
                    echo "<option value=\"$kop\" selected=\"selected\">$op</option>";
                }
                else {
                    echo "<option value=\"$kop\">$op</option>";
                }
            }
            echo "</select></td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\"><select name=\"ngravacao\" id=\"ngravacao\">";
            foreach($ops as $kop => $op) {
                if($lcolunas['ngravacao'] == $kop) {
                    echo "<option value=\"$kop\" selected=\"selected\">$op</option>";
                }
                else {
                    echo "<option value=\"$kop\">$op</option>";
                }
            }
            echo "</select></td>";
            if($lcolunas['nomecoluna'] == "idoperador" || $lcolunas['nomecoluna'] == "idsuper_oper" || $lcolunas['nomecoluna'] == "dtbase" || $lcolunas['nomecoluna'] == "horabase") {
                 echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"tamanho\" $read value=\"".$lcolunas['tamanho']."\"></td>";
            }
            else {
               echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:60px; border: 1px solid #FFF; text-align:center\" name=\"tamanho\" value=\"".$lcolunas['tamanho']."\"></td>";
            }
            echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:160px; border: 1px solid #FFF; text-align:center\" name=\"caracter\" value=\"".$lcolunas['caracter']."\"></td>";
            echo "<td align=\"center\" class=\"corfd_colcampos\"><input style=\"width:215px; border: 1px solid #FFF; text-align:center\" name=\"restricao\" value=\"".$lcolunas['restricao']."\"></td>";
            
            if($lcolunas['Field'] == "idoper") {
            }
            else {
            echo "<td><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" readonly=\"readonly\" name=\"alteracampo\" type=\"submit\" value=\"alterar\" /></td>";
            }
      echo "</tr></form>";
  }
?>
    </tbody>
<font color="#FF0000"><strong><?php echo $_GET['msgoalt']; ?></strong></font>
</table>
</div><hr /><br /><br/>
<div style="width:1020px">
<form action="cadparammoni.php" method="post" id="cadparam">
<table width="798">
<tr>
    <td width="995" colspan="6" align="center" class="corfd_ntab"><strong>PARAMETROS MONITORIA</strong></td>
</tr>
</table>
<div id="parametros" style="float:left">
<table width="498">
  <tr>
     <td height="15" colspan="2" align="center" bgcolor="#E6919F"><strong>DADOS PARAMETROS</strong></td>
  </tr>   
      <tr>
        <td class="corfd_coltexto"><strong>*Indice Nota</strong></td>
        <td class="corfd_colcampos"><input type="hidden" name="idparam" id="idparam" value="<?php echo $idparam;?>" /><select name="indice" id="indice">
        <option value="" disabled="disabled" selected="selected">Selecione um indice</option>
        <?php
        $selind = "SELECT * FROM indice";
        $eselind = $_SESSION['query']($selind) or die ("erro na query de consulta dos indices");
        while($lselind = $_SESSION['fetch_array']($eselind)) {
            if($eselparam['idindice'] == $lselind['idindice']) {
                echo "<option value=\"".$lselind['idindice']."\" selected=\"selected\">".$lselind['nomeindice']."</option>";
            }
            else {
                echo "<option value=\"".$lselind['idindice']."\">".$lselind['nomeindice']."</option>";
            }
        }
        ?>
        </select></td>
      </tr>
      <tr>
        <td width="184" class="corfd_coltexto"><strong>*Tipo Monitoria</strong></td>
        <td width="302" class="corfd_colcampos"><select name="tipomoni" id="tipomoni">
        <?php
        $tipomoni = array('G' => 'GRAVACAO', 'O' => 'ON-LINE');
        if(isset($_GET['idparam'])) {
          echo "<option value=\"\" disabled=\"disabled\">Selecione um tipo de monitoria</option>";
          foreach($tipomoni as $tm => $textotm) {
              if($eselparam['tipomoni'] == $tm) {
                  echo "<option value=\"".$tm."\" selected=\"selected\">".$textotm."</option>";
              }
              else {
                  echo "<option value=\"".$tm."\">".$textotm."</option>";
              }
          }
        }
        else {
          echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione um tipo de monitoria</option>";
          foreach($tipomoni as $tm => $textotm) {
              echo "<option value=\"".$tm."\">".$textotm."</option>";
          }
        }
        ?>
        </select></td>
        </tr>
      <tr>
      	<td class="corfd_coltexto"><strong>* Check NCG</strong></td>
        <td class="corfd_colcampos">
        	<select name="checkfg" id="checkfg">
            	<?php
                if(isset($_GET['idparam'])) {	
                }
                else {
                        echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione...</option>";
                }
                $check = array('S','N');
                foreach($check as $c) {
                        if($eselparam['checkfg'] == $c) {
                                echo "<option value=\"$c\" selected=\"selected\">$c</option>";	
                        }
                        else {
                                echo "<option value=\"$c\">$c</option>";
                        }
                }
                ?>
            </select>
        </td>
      </tr>
      <tr>
        <td width="184" class="corfd_coltexto"><strong>*Tipo Importação</strong></td>
        <td width="302" class="corfd_colcampos">
            <select name="tipoimp" id="tipoimp">
                <?php
                if(isset($_GET['idparam'])) {
                    echo "<option value=\"\" disabled=\"disabled\">Selecione um tipo de monitoria</option>";
                    if($eselparam['tipomoni'] == "G") {
                        $tipoimp = array('LA' => 'LISTA AUDIO', 'A' => 'LEITURA AUDIO','S' => 'SELEÇÃO', 'I' => 'AUDIO INTERNET');
                        foreach($tipoimp as $ti => $textoti) {
                            if($eselparam['tipoimp'] == $ti) {
                                echo "<option value=\"".$ti."\" selected=\"selected\">".$textoti."</option>";
                            }
                            else {
                                echo "<option value=\"".$ti."\">".$textoti."</option>";
                            }
                        }
                    }
                    if($eselparam['tipomoni'] == "O") {
                        $tipoimp = array('L' => 'LISTA', 'SL' => 'SEL LISTA');
                        foreach($tipoimp as $ti => $textoti) {
                            if($eselparam['tipoimp'] == $ti) {
                                echo "<option value=\"".$ti."\" selected=\"selected\">".$textoti."</option>";
                            }
                            else {
                                echo "<option value=\"".$ti."\">".$textoti."</option>";
                            }
                        }
                    }
                }
                else {
                    echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione um tipo de monitoria</option>";
                }
                ?>
            </select>
        </td>
      </tr>
      <tr>
          <td class="corfd_coltexto"><strong>Sem Dados</strong></td>
          <td class="corfd_colcampos">
              <select name="semdados" id="semdados">
                  <?php
                  $tpdados = array("N","S");
                  foreach($tpdados as $dados) {
                      if($dados == $eselparam['semdados']) {
                          echo "<option value=\"$dados\" selected=\"selected\">$dados</option>";
                      }
                      else {
                          echo "<option value=\"$dados\">$dados</option>";
                      }
                  }
                  ?>
              </select>
          </td>
      </tr>
      <td class="corfd_coltexto"><strong>Tipo Server</strong></td>
        <td colspan="4" class="corfd_colcampos">
        	<select name="tiposervidor" id="tiposervidor">
            	<?php
                $tiposerver = array('LOCAL','FTP');
                foreach($tiposerver as $ts) {
                    if($ts == $eselparam['tiposervidor']) {
                        echo "<option value=\"$ts\" selected=\"selected\">$ts</option>";	
                    }
                    else {
                        echo "<option value=\"$ts\">$ts</option>";	
                    }
                }
                ?>
            </select>
        </td>
      </tr>
      <tr class="ftp">
      	<td height="36" class="corfd_coltexto"><strong>Endereço</strong></td>
        <td colspan="3" class="corfd_colcampos"><input name="endereco" id="endereco" type="text" style="border: 1px solid #69C; width:150px; text-align:center" value="<?php echo $eselparam['endereco'];?>" /></td>
      </tr>
      <tr class="ftp">
      	<td height="36" class="corfd_coltexto"><strong>Login:</strong></td>
        <td colspan="3" class="corfd_colcampos"><input name="loginftp" id="loginftp" type="text" style="border: 1px solid #69C; width:150px; text-align:center" value="<?php echo $eselparam['loginftp'];?>" /></td>
      </tr>
      <tr class="ftp">
      	<td height="26" class="corfd_coltexto"><strong>Senha</strong></td>
        <td colspan="3" class="corfd_colcampos"><input name="senhaftp" id="senhaftp" type="password"style="border: 1px solid #69C; width:150px; text-align:center" value="<?php echo base64_decode($eselparam['senhaftp']);?>" /></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>*Caminho Audios Salvos</strong></td>
        <td class="corfd_colcampos"><input style="width:300px; border: 1px solid #69C" name="camaudio" id="camaudio" type="text" title="Informar o caminho completo desde a raiz do diretÃ³rio e forneÃ§a a permissÃ£o de escrita e leitura na pasta"
            <?php
            if(isset($_GET['idparam'])) {
                echo "value=\"".$eselparam['camaudio']."\"";
            }
            else {
            }
            ?>
        /></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>Separa Campos</strong></td>
        <td class="corfd_colcampos"><input style="width:30px; border: 1px solid #69C; text-align:center" name="ccampos" id="ccampos" maxlength="1" type="text" value="<?php if(isset($_GET['idparam'])) { echo $eselparam['ccampos']; } else {}?>" /><font color="#FF0000"><strong>Campo utilizado somente para Gravação</strong></font></td>
      </tr>
      <tr id="cpartes">
        <td class="corfd_coltexto"><strong>Separa Partes</strong></td>
        <td class="corfd_colcampos"><input style="width:30px; border: 1px solid #69C; text-align:center" name="cpartes" id="cpartes" maxlength="2" type="text" value="<?php if(isset($_GET['idparam'])) { echo $eselparam['cpartes']; } else {}?>" /><font color="#FF0000"><strong>Campo utilizado somente para Gravação</strong></font></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>*Tipo Audio</strong></td>
        <td class="corfd_colcampos">
            <select name="tipoaudio[]" id="tipoaudio" multiple="multiple" style="width:70px; height:70px">
            <?php
            $taudios = array("mp3","wav","gsm","rma");
            $tpaudiodb = explode(",",$eselparam['tpaudio']);
            foreach($taudios as $audio) {
                if(in_array($audio,$tpaudiodb)) {
                    echo "<option value=\"".$audio."\" selected=\"selected\">".$audio."</option>";
                }
                else {
                    echo "<option value=\"".$audio."\">".$audio."</option>";
                }
            }
            ?>
            </select>
         </td>   
      </tr>
        <tr class="tpcompress">
            <td class="corfd_coltexto"><strong>*Tipo Compressão</strong></td>
            <td class="corfd_colcampos">
                <select name="tpcompress[]" id="tpcompress" multiple="multiple" style="width:70px; height:70px">
                <?php
                $tpcompress = array('zip','rar','tar');
                $tpcomdb = explode(",",$eselparam['tpcompress']);
                foreach($tpcompress as $compress) {
                    if(in_array($compress, $tpcomdb)) {
                        echo "<option value=\"".$compress."\" selected=\"selected\">".$compress."</option>";
                    }
                    else {
                        echo "<option value=\"".$compress."\">".$compress."</option>";
                    }
                }
                ?>
                </select>
            </td>
        </tr>
        <tr class="tparquivo">
            <td class="corfd_coltexto"><strong>*Tipo Arquivo Dados</strong></td>
            <td class="corfd_colcampos">
                <select name="tparquivo[]" id="tparquivo" multiple="multiple" style="width:70px; height:70px">
                <?php
                $tparqs = array('txt','csv','xls','xlsx','odt');
                $tparqdb = explode(",",$eselparam['tparquivo']);
                foreach($tparqs as $arq) {
                    if(in_array($arq, $tparqdb)) {
                        echo "<option value=\"".$arq."\" selected=\"selected\">".$arq."</option>";
                    }
                    else {
                        echo "<option value=\"".$arq."\">".$arq."</option>";
                    }
                }
                ?>
                </select>
            </td>
        </tr>
      <tr>
        <td class="corfd_coltexto"><strong>*Conferência</strong></td>
        <td class="corfd_colcampos">
            <select name="conf" id="conf">
                <?php
                $conf = array('N','S');
                foreach($conf as $c) {
                    if($eselparam['conf'] == $c) {
                        echo "<option value=\"".$c."\" selected=\"selected\">".$c."</option>";
                    }
                    else {
                        echo "<option value=\"".$c."\">".$c."</option>";
                    }
                }
                ?>
            </select>
         </td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>*Horas PA</strong></td>
        <td class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" name="hpa" id="hpa" maxlength="8" type="text" value="<?php if(isset($_GET['idparam'])) { echo $eselparam['hpa']; } else {}?>" /></td>
      </tr>
      <tr>
        <td class="corfd_coltexto"><strong>*Horas Monitor</strong></td>
        <td class="corfd_colcampos"><input style="width:70px; border: 1px solid #69C; text-align:center" name="hmonitor" id="hmonitor" maxlength="8" type="text" value="<?php if(isset($_GET['idparam'])) { echo $eselparam['hmonitor']; } else {}?>" /></td>
      </tr>
  </table>
  </div>
  <div id="fila" style="float:left; width:500px">
  	<table width="378">
    	<tr>
        <td bgcolor="#E6919F" align="center" colspan="4"><strong>FILTROS FILA</strong></td>
        </tr>
    	<tr>
        <td width="146" class="corfd_coltexto"><strong>Data Importação</strong></td>
        <td width="60" class="corfd_colcampos">
            <select title="ORDENA FILA" name="dataimp" id="dataimp">
                <?php
                $opcoes = array('N','S');
                $pridataimpdb = explode(",",$eselparam['filtro_dataimp']);
                foreach($opcoes as $diop) {
                    if($pridataimpdb[0] == $diop) {
                        echo "<option value=\"".$diop."\" selected=\"selected\">".$diop."</option>";
                    }
                    else {
                        echo "<option value=\"".$diop."\">".$diop."</option>";
                    }

                }
                ?>
            </select>
        </td>
        <td width="110" class="corfd_coltexto"><strong>Prioridade</strong></td>
        <td width="42" class="corfd_colcampos">
        	<select style="width:40px" title="PRIORIDADE NA CONSTRUÇÃO DA FILA" name="pridataimp" id="pridataimp">
            	<option value="0" selected="selected"></option>
            	<?php
                $pri = array('1','2','3','4','5');
                foreach($pri as $pridataimp) {
                    if($pridataimpdb[1] == $pridataimp) {
                        echo "<option value=\"".$pridataimp."\" selected=\"selected\">".$pridataimp."</option>";
                    }
                    else {
                        echo "<option value=\"".$pridataimp."\">".$pridataimp."</option>";
                    }
                }
                ?>
        	</select>
        </td>
        </tr>
        <tr>
        <td width="146" class="corfd_coltexto"><strong>Ultima Monitoria</strong></td>
        <td width="60" class="corfd_colcampos">
            <select title="ORDENA FILA" name="ultmoni" id="ultmoni">
            <?php
            $priultmonidb = explode(",",$eselparam['filtro_ultmoni']);
            foreach($opcoes as $umop) {
                if($umop == $priultmonidb[0]) {
                    echo "<option value=\"".$umop."\" selected=\"selected\">".$umop."</option>";
                }
                else {
                    echo "<option value=\"".$umop."\">".$umop."</option>";
                }
            }
            ?>
            </select>
        </td>
        <td width="110" class="corfd_coltexto"><strong>Prioridade</strong></td>
        <td width="42" class="corfd_colcampos">
        	<select style="width:40px" title="PRIORIDADE NA CONSTRUÇÃO DA FILA" name="priultmoni" id="priultmoni">
            	<option value="0" selected="selected"></option>
            	<?php
                $pri = array('1','2','3','4','5');
                foreach($pri as $priultmoni) {
                    if($priultmoni == $priultmonidb[1]) {
                        echo "<option value=\"".$priultmoni."\" selected=\"selected\">".$priultmoni."</option>";
                    }
                    else {
                        echo "<option value=\"".$priultmoni."\">".$priultmoni."</option>";
                    }
                }
                ?>
        	</select>
        </td>
        </tr>
        <tr>
        <td width="146" class="corfd_coltexto"><strong>Data Fim Importação</strong></td>
        <td width="60" class="corfd_colcampos">
            <select title="RESTIRNGE FILA" name="datafim" id="datafim">
            <?php
            $pridatafimdb = explode(",",$eselparam['filtro_datafimimp']);
            foreach($opcoes as $dfop) {
                if($pridatafimdb[0] == $dfop) {
                    echo "<option value=\"".$dfop."\" selected=\"selected\">".$dfop."</option>";
                }
                else {
                    echo "<option value=\"".$dfop."\">".$dfop."</option>";
                }
            }
            ?>
            </select>
        </td>
        <td width="110" class="corfd_coltexto"><strong>Prioridade</strong></td>
        <td width="42" class="corfd_colcampos">
        	<select style="width:40px" title="PRIORIDADE NA CONSTRUÇÃO DA FILA" name="pridatafim" id="pridatafim">
            	<option value="0" selected="selected"></option>
            	<?php
                $pri = array('1','2','3','4','5');
                foreach($pri as $pridatafim) {
                    if($pridatafimdb[1] == $pridatafim) {
                        echo "<option value=\"".$pridatafim."\" selected=\"selected\">".$pridatafim."</option>";
                    }
                    else {
                        echo "<option value=\"".$pridatafim."\">".$pridatafim."</option>";
                    }
                }
                ?>
        	</select>        
        </td>
        </tr>
        <tr>
        <td width="146" class="corfd_coltexto"><strong>Dias Ult. Importação</strong></td>
        <td width="60" class="corfd_colcampos"><input type="text" name="dultimp" id="dultimp" title="RESTRINGE FILA" style="width:40px; border: 1px solid #69C; text-align:center" maxlength="3"
            <?php
            $pridultimpdb = explode(",",$eselparam['filtro_dultimp']);
            if(isset($_GET['idparam'])) {
                echo "value=\"".$pridultimpdb[0]."\"";
            }
            else {
            }
            ?>
                                                />
        </td>
        <td width="110" class="corfd_coltexto"><strong>Prioridade</strong></td>
        <td width="42" class="corfd_colcampos">
        	<select style="width:40px" title="PRIORIDADE NA CONSTRUÇÃO DA FILA" name="pridultimp" id="pridultimp">
            	<option value="0" selected="selected"></option>
            	<?php
                $pri = array('1','2','3','4','5');
                foreach($pri as $pridultimp) {
                    if($pridultimpdb[1] == $pridultimp) {
                        echo "<option value=\"".$pridultimp."\" selected=\"selected\">".$pridultimp."</option>";
                    }
                    else {
                        echo "<option value=\"".$pridultimp."\">".$pridultimp."</option>";
                    }
                }
                ?>
        	</select>        
        </td>
        </tr>
        <tr>
        <td width="146" class="corfd_coltexto"><strong>Limita Multiplicador</strong></td>
        <td width="60" class="corfd_colcampos">
            <select title="RESTIRNGE FILA" name="limmulti" id="limmulti">
            <?php
            $prilimmultdb = explode(",",$eselparam['filtro_limmulti']);
            foreach($opcoes as $lmop) {
                if($lmop == $prilimmultdb[0]) {
                    echo "<option value=\"".$lmop."\" selected=\"selected\">".$lmop."</option>";
                }
                else {
                    echo "<option value=\"".$lmop."\">".$lmop."</option>";
                }
            }
            ?>
            </select>
        </td>
        <td width="110" class="corfd_coltexto"><strong>Prioridade</strong></td>
        <td width="42" class="corfd_colcampos">
            <select style="width:40px" title="PRIORIDADE NA CONSTRUÇÃO DA FILA" name="prilimmulti" id="prilimmulti">
            <option value="0" selected="selected"></option>
            <?php
            $pri = array('1','2','3','4','5');
            foreach($pri as $prilimmulti) {
                if($prilimmulti == $prilimmultdb[1]) {
                    echo "<option value=\"".$prilimmulti."\" selected=\"selected\">".$prilimmulti."</option>";
                }
                else {
                    echo "<option value=\"".$prilimmulti."\">".$prilimmulti."</option>";
                }
            }
            ?>
            </select>
        </td>
        </tr>
        <tr>
                <td class="corfd_coltexto"><strong>Ordena Ctt</strong></td>
                <td class="corfd_colcampos">
            	<select name="atvordem" id="atvordem">
            	<?php
                    $ordem = explode(",",$eselparam['ordemctt']);
                    $ordematv = array('N','S');
                    foreach($ordematv as $od) {
                            if($od == $ordem[0]) {
                                    echo "<option value=\"$od\" selected=\"selected\">$od</option>";
                            }
                            else {
                                    echo "<option value=\"$od\">$od</option>";
                            }
                    }
                    ?>
                </select>
            </td>
            <td class="corfd_coltexto"><strong>Modo Ordem</strong></td>
            <td class="corfd_colcampos">
            	<select name="tpordem" id="tpordem">
                  <?php
                  $tpordem = array('C','D');
                  foreach($tpordem as $tp) {
                          if($tp == $ordem[1]) {
                                  echo "<option value=\"$tp\" selected=\"selected\">$tp</option>";  
                          }
                          else {
                                  echo "<option value=\"$tp\">$tp</option>";
                          }
                  }
                  ?>
              </select>
            </td>
        </tr>
    </table>
  </div>
  <div id="button" style="float:left; width:1020">
  <table width="1020">
      <?php
      if(isset($_GET['idparam'])) {
        echo "<td height=\"22\" colspan=\"6\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"alteraparam\" type=\"submit\" value=\"Alterar\" /> <input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"novo\" type=\"submit\" value=\"Novo\" /></td>";
      }
      else {
        echo "<td height=\"22\" colspan=\"6\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" name=\"cadastra\" type=\"submit\" value=\"Cadastrar\" /></td>";
      }
    if(isset($_GET['idparam'])) {
        echo "<font color=\"#FF0000\"><a name=\"alt\"><strong>".$_GET['msgi']."</strong></a></font>";
    }
    else {
        echo "<font color=\"#FF0000\"><strong><a name=\"msg\">".$_GET['msg']."</a></strong></font>";
    }
    ?>
    </table>
    </div>
    </form>
<div id="paramcad" style="float:left">
<br /><br />
<table width="1024" border="0">
  <tr>
   	<td class="corfd_ntab" align="center" colspan="9"><strong>PARAMETROS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="33" align="center" class="corfd_coltexto"><strong>ID</strong></td>
    <td width="83" align="center" class="corfd_coltexto"><strong>Tipo Moni.</strong></td>
    <td width="125" align="center" class="corfd_coltexto"><strong>Tipo Imp.</strong></td>
    <td width="50" align="center" class="corfd_coltexto"><strong>Tipo Audio.</strong></td>
    <td width="310" align="center" class="corfd_coltexto"><strong>Caminho Audios</strong></td>
    <td width="52" align="center" class="corfd_coltexto"><strong>Separa Campos</strong></td>
    <td width="52" align="center" class="corfd_coltexto"><strong>Separa Partes</strong></td>
    <td width="32" align="center" class="corfd_coltexto"><strong>Conf.</strong></td>
    <td width="32" align="center" class="corfd_coltexto"><strong>Ind.</strong></td>
    <td width="81" align="center"></td>
    <td width="73" align="center"></td>
    <td width="73" align="center"></td>
  </tr>
</table>
</div>
<div style="height:250px; overflow:auto; float:left">
  <table width="1030" border="0">
  <?php
  $selparam = "SELECT * FROM param_moni";
  $eparam = $_SESSION['query']($selparam) or die (mysql_error());
  while($lparam = $_SESSION['fetch_array']($eparam)) {
      if($lparam['idparam_moni'] == $_GET['idparam']) {
          $color = "bgcolor=\"#EAB0B0\"";
      }
      else {
          $color = "class=\"corfd_colcampos\"";
      }
	  echo "<form action=\"cadparammoni.php\" method=\"post\">";
	  echo "<tr>";
	  echo "<td align=\"center\" $color width=\"32px\"><input style=\"width:30px; border: 1px solid #FFF; text-align:center\" name=\"idparam\" type=\"hidden\" value=\"".$lparam['idparam_moni']."\" />".$lparam['idparam_moni']."</td>";
	  echo "<td align=\"center\" $color width=\"80px\">";
	  if($lparam['tipomoni'] == "G") {
	    $tmoninome = "GRAVAÇÃO";
	  }
	  if($lparam['tipomoni'] == "O") {
	    $tmoninome = "ON-LINE";
	  }
	  echo "<input style=\"width:80px; border: 1px solid #FFF; text-align:center\" name=\"tipomoni\" type=\"hidden\" value=\"".$tmoninome."\">".$tmoninome."</td>";
	  echo "<td align=\"center\" $color width=\"117px\">";
          $tp = array('LA' => 'LISTAAUDIO','A' => 'LEITURA AUDIO','I' => 'INTERNET','L' => 'LISTA','SL' => 'SEL LISTA','S' => 'SELEÇÃO');
	  echo "<input style=\"width:120px; border: 1px solid #FFF; text-align:center\" name=\"tipoimp\" type=\"hidden\" value=\"".$tp[$lparam['tipoimp']]."\" >".$tp[$lparam['tipoimp']]."</td>";
	  $tpsaudio = explode(",",$lparam['tpaudio']);
          if(count($tpsaudio) <= 3) {
              $tm = count($tpsaudio);
          }
          else {
              $tm = "3";
          }
          echo "<td $color width=\"50px\"><select multiple=\"multiple\" readonly=\"readonly\" name=\"tipoaudio\" style=\"width:50px;\" size=\"".$tm."\" />";
          foreach($tpsaudio as $tpaudio) {
              echo "<option value=\"".$tpaudio."\">".$tpaudio."</option>";
          }
          echo "</select></td>";
	  echo "<td align=\"center\" $color width=\"290px\"><input style=\"width:290px; border: 1px solid #FFF;\" name=\"camaudio\" type=\"hidden\" value=\"".$lparam['camaudio']."\" />".$lparam['camaudio']."</td>";
	  echo "<td align=\"center\" $color width=\"50px\"><input style=\"width:50px; border: 1px solid #FFF; text-align:center\" name=\"ccampos\" type=\"hidden\" value=\"".$lparam['ccampos']."\" />".$lparam['ccampos']."</td>";
	  echo "<td align=\"center\" $color width=\"50px\"><input style=\"width:50px; border: 1px solid #FFF; text-align:center\" name=\"cpartes\" type=\"hidden\" value=\"".$lparam['cpartes']."\" />".$lparam['cpartes']."</td>";
	  echo "<td align=\"center\" $color width=\"30px\"><input style=\"width:30px; border: 1px solid #FFF; text-align:center\" type=\"hidden\" value=\"".$lparam['conf']."\" >".$lparam['conf']."</td>";
	  echo "<td align=\"center\" $color width=\"30px\"><input style=\"width:30px; border: 1px solid #FFF; text-align:center\" type=\"hidden\" value=\"".$lparam['idindice']."\" >".$lparam['idindice']."</td>";
	  echo "<td align=\"center\" width=\"30px\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" readonly=\"readonly\" name=\"altera\" type=\"submit\" value=\"carregar\" /></td>";
	  echo "<td align=\"center\" width=\"85px\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" readonly=\"readonly\" name=\"altrelcamp\" type=\"submit\" value=\"alt.rel.camp.\" /></td>";
	  echo "<td align=\"center\" width=\"50px\"><input style=\"border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)\" readonly=\"readonly\" name=\"apaga\" type=\"submit\" value=\"apagar\" /></td>";
	  echo "</tr></form>";
  }
  ?>
</table>
</div>
</div>