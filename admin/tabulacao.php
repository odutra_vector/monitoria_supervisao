<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");

if($_GET['idtabula'] != "" && $_GET['idreltabula'] == "") {
	$idtabula = $_GET['idtabula'];
	$seltabula = "SELECT * FROM tabulacao WHERE idtabulacao='$idtabula' ORDER BY idtabulacao LIMIT 1";
	$eseltabula = $_SESSION['fetch_array']($_SESSION['query']($seltabula)) or die ("erro na query de consulta da tabulação");
        $crel = "SELECT MAX(posicao) as posicao FROM tabulacao WHERE idtabulacao='$idtabula'";
	$ecrel = $_SESSION['fetch_array']($_SESSION['query']($crel)) or die ("erro na query de contagem dos relacionamentos de pergunta e resposta");
}
if($_GET['idreltabula'] != "" && $_GET['idtabula'] != "") {
	$idtabula = $_GET['idtabula'];
	$idreltab = $_GET['idreltabula'];
	$crel = "SELECT MAX(posicao) as posicao FROM tabulacao WHERE idtabulacao='$idtabula'";
	$ecrel = $_SESSION['fetch_array']($_SESSION['query']($crel)) or die ("erro na query de contagem dos relacionamentos de pergunta e resposta");
	$reltabula = "SELECT t.idtabulacao,t.idreltabulacao,t.nometabulacao,t.obriga,t.posicao,t.ativorel,t.idrespostatab,rt.descrirespostatab,t.idperguntatab,pt.descriperguntatab,t.idproxpergunta FROM tabulacao t
                      INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                      INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab WHERE t.idreltabulacao='$idreltab'";
	$ereltabula = $_SESSION['query']($reltabula) or die ("erro na query de consulta da tabulação");
	$lreltabula = $_SESSION['fetch_array']($ereltabula);
}


?>
<script type="text/javascript" src="/monitoria_supervisao/js/jqueryTreeview/jquery.treeview.css"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jqueryTreeview/jquery.treeview.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#idpergcad').change(function() {
            var idperg = $(this).val();
            var idtab = $('#idtabula').val()
            $('#posicao').load("carrpositab.php",{idperg: idperg, idtabula: idtab});
        })

        $("div[class*='listtab']").hide();
        <?php
        if($idtabula != "") {
           echo "$('.listtab".$idtabula."').show();\n";
        }
        else {
        }
        ?>
        $('#idpergcad').change(function() {
            var idperg = $(this).val();
            $('#proxperg').load('carregaprox.php',{idperg: idperg});
        })
        $('#tabs').change(function() {
            $("div[class*='listtab']").hide();
            var idtab = $(this).val();
            $('.listtab'+idtab).show();
            $('#arvoretab').load('carregatab.php',{idtab: idtab});
        })
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("[class*='resp']").hide();
        $("[class*='perg']").live("click", function() {
            $("[class*=resp]").show();
        })
        //$('#visuarvore').treeview({collapsed: true});
        //$("#ex").treeview({collapsed: true});
    });
</script>
<div id="conteudo">
    <div style="width:1024px;">
            <form action="cadtabula.php" method="post" id="cadrel">
            <?php
            if(($_GET['idreltabula'] != "" OR $_GET['idtabula'] != "") && $_GET['tipo'] == "cadrel") {
            ?>
            <table width="auto" border="0">
              <tr>
                <td class="corfd_ntab" align="center" colspan="2"><strong>CADASTRO DE RELACIONAMENTO PERGUNTA/ RESPOSTA</strong></td>
              </tr>
              <tr>
                <td width="131" class="corfd_coltexto"><strong>TABULAÇÃO</strong></td>
                <td width="507" class="corfd_colcampos"><input name="idreltabula" id="idreltabula" type="hidden" value="<?php echo $idreltab;?>" /><input name="idtabula" id="idtabula" type="hidden" value="<?php echo $idtabula;?>" /><?php if($idreltab != "") { echo $lreltabula['nometabulacao']; } else { echo $eseltabula['nometabulacao'];}?></td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>PERGUNTA</strong></td>
                <td class="corfd_colcampos">
                    <?php
                    if($idreltab != "") {
                    ?>
                        <input type="hidden" name="idpergcad" id="idpergcad" value="<?php echo $lreltabula['idperguntatab'];?>" /><?php echo $lreltabula['descriperguntatab'];?>
                    <?php
                    }
                    else {
                        ?>
                        <select name="idpergcad" id="idpergcad" style="width:450px">
                            <option value="" selected="selected" disabled="disabled">SELECIONE...</option>
                        <?php
                        $selpergcad = "SELECT * FROM perguntatab WHERE ativo='S'";
                        $eselpergcad = $_SESSION['query']($selpergcad) or die ("erro na query de consulta das perguntas cadastradas");
                        while($lselpergcad = $_SESSION['fetch_array']($eselpergcad)) {
                            if($lselpergcad['idperguntatab'] == $lreltabula['idperguntatab']) {
                                echo "<option value=\"".$lselpergcad['idperguntatab']."\" selected=\"selected\">".$lselpergcad['descriperguntatab']."</option>";
                            }
                            else {
                                echo "<option value=\"".$lselpergcad['idperguntatab']."\">".$lselpergcad['descriperguntatab']."</option>";
                            }
                        }
                        ?>
                        </select>
                        <?php
                    }
                    ?>
                </td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>REPOSTA</strong></td>
                <td class="corfd_colcampos">
                    <select name="idrespcad" id="idrespcad">
                    <?php
                    if($idreltab == "") {
                        echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">SELECIONE...</option>";
                    }
                    else {

                    }
                    $selrespcad = "SELECT * FROM respostatab WHERE ativo='S'";
                    $eselrespcad = $_SESSION['query']($selrespcad) or die ("erro na query de consulta das respostas cadastradas");
                    while($lselrespcad = $_SESSION['fetch_array']($eselrespcad)) {
                        if($lselrespcad['idrespostatab'] == $lreltabula['idrespostatab']) {
                            echo "<option value=\"".$lselrespcad['idrespostatab']."\" selected=\"selected\">".$lselrespcad['descrirespostatab']."</option>";
                        }
                        else {
                            echo "<option value=\"".$lselrespcad['idrespostatab']."\">".$lselrespcad['descrirespostatab']."</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
              </tr>
              <tr>
              	<td class="corfd_coltexto"><strong>PROX. PERGUNTA</strong></td>
                <td class="corfd_colcampos">
                    <select name="proxperg" id="proxperg">
                    <?php
                    if($idreltab == "") {
                    ?>
                    <option value="" selected="selected">NENHUMA...</option>
                    <?php
                    }
                    else {
                    ?>
                    <option value="">NENHUMA...</option>
                    <?php
                            $selperg = "SELECT * FROM perguntatab WHERE ativo='S'";
                            $eselperg = $_SESSION['query']($selperg) or die ("erro na query de consulta das perguntas");
                            while($lselperg = $_SESSION['fetch_array']($eselperg)) {
                                if($lselperg['idperguntatab'] == $lreltabula['idproxpergunta']) {
                                ?>
                                <option value="<?php echo $lselperg['idperguntatab'];?>" selected="selected"><?php echo $lselperg['descriperguntatab'];?></option>
                                <?php
                                }
                                else {
                                ?>
                                <option value="<?php echo $lselperg['idperguntatab'];?>"><?php echo $lselperg['descriperguntatab'];?></option>
                            <?php
                                }
                            }
                    }
                    ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>OBRIGA</strong></td>
                <td class="corfd_colcampos">
                    <select name="obriga" id="obriga">
                    <?php
                    $obriga = array('S','N');
                    foreach($obriga as $ob) {
                        if($ob == $lreltabula['obriga']) {
                            echo "<option value=\"".$ob."\" selected=\"selected\">".$ob."</option>";
                        }
                        else {
                            echo "<option value=\"".$ob."\">".$ob."</option>";
                        }
                    }
                    ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>POSIÇÃO</strong></td>
                <td class="corfd_colcampos">
                    <select name="posicao" id="posicao">
                    <?php
                    $i = 1;
                    $ultposi = $ecrel['posicao'] + 1;
                    for($i = 1; $i <= $ultposi; $i++) {
                        if($i == $lreltabula['posicao']) {
                            echo "<option value=\"".$i."\" selected=\"selected\">".$i."</option>";
                        }
                        else {
                            echo "<option value=\"".$i."\">".$i."</option>";
                        }
                    }

                    ?>
                    </select>                
                </td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td class="corfd_colcampos">
                    <select name="atvtab" id="atvtab">
                    <?php
                    $ativo = array('S','N');
                    foreach($ativo as $atv) {
                    if($atv == $lreltabula['ativorel']) {
                        echo "<option value=\"".$atv."\" selected=\"selected\">".$atv."</option>";
                    }
                    else {
                        echo "<option value=\"".$atv."\">".$atv."</option>";
                    }
                    }
                    ?>
                    </select>
                </td>
              </tr>
              <tr>
                  <td colspan="2">
                    <?php
                    if($idreltab != "") {
                    ?>
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="alterarel" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novorel" type="submit" value="Novo" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novatab" type="submit" value="Nova Tabulacão" />
                    <?php
                    }
                    else {
                    ?>
                    <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastrarel" type="submit" value="Cadastrar" />
                    <?php
                    }
                    ?>
                  </td>
              </tr>
            </table>
            <?php
            }
            else {
            ?>
            <table width="386">
              <tr>
                <td class="corfd_ntab" colspan="2" align="center"><strong>CADASTRO DE TABULAÇÃO</strong></td>
              </tr>
              <tr>
                <td width="74" height="18" class="corfd_coltexto"><strong>NOME</strong></td>
                <td width="300" class="corfd_colcampos"><input type="hidden" name="idtabula" id="idtabulacad" value="<?php echo $eseltabula['idtabulacao'];?>" /><input type="text" name="nometab" id="nometab" style="width:300px; border: 1px solid #9CF" value="<?php echo $eseltabula['nometabulacao'];?>" /></td>
              </tr>
              <tr>
                <td class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td class="corfd_colcampos">
                <select name="atvtab" id="atvtab">
                <?php
                $ativo = array('S','N');
                foreach($ativo as $atv) {
                    if($atv == $eseltabula['ativo']) {
                        echo "<option value=\"".$atv."\" selected=\"selected\">".$atv."</option>";
                    }
                    else {
                        echo "<option value=\"".$atv."\">".$atv."</option>";
                    }
                }
                ?>
                </select>
                </td>
              </tr>
              <tr>
              	<td colspan="2">
                <?php
                if($idtabula != "" && $idreltab == "") {
                ?>
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altera" type="submit" value="Alterar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="novotab" type="submit" value="Nono" />
                <?php	
                }
                else {
                ?>
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" type="submit" value="Cadastrar" />
                <?php
                }?>
                </td>
              </tr>
            </table><font color="#FF0000"><strong><?php echo $_GET['msgt'] ?></strong></font>
            <?php
            }
            ?>
            <font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
            </form>
            <br /><br />
            <div style="height:200px; overflow:auto">
            <font color="#FF0000"><strong><?php echo $_GET['msgta'] ?></strong></font>
            <table width="588">
              <tr>
                <td class="corfd_ntab" align="center" colspan="3"><strong>TABULAÇÕES CADASTRADAS</strong></td>
              </tr>
              <tr>
                <td width="49" align="center" class="corfd_coltexto"><strong>ID TAB</strong></td>
                <td width="225" align="center" class="corfd_coltexto"><strong>NOME</strong></td>
                <td width="40" align="center" class="corfd_coltexto"><strong>ATIVO</strong></td>
                <td width="254" align="left" ></td>
              </tr>
              <?php
              $seltab = "SELECT * FROM tabulacao GROUP BY idtabulacao";
              $eseltab = $_SESSION['query']($seltab) or die ("erro na query de consulta da tabulação");
              while($lseltab = $_SESSION['fetch_array']($eseltab)) {
              ?>
                <form action="cadtabula.php" method="post">
              <tr>
                <td class="corfd_colcampos" align="center"><input type="hidden" name="idtabula" id="idtabula" value="<?php echo $lseltab['idtabulacao'];?>" /><a href="admin.php?menu=tabulacao&idtabula=<?php echo $lseltab['idtabulacao'];?>" style="text-decoration:none; color:#000;"><?php echo $lseltab['idtabulacao'];?></a></td>
                <td align="center" class="corfd_colcampos"><a href="admin.php?menu=tabulacao&idtabula=<?php echo $lseltab['idtabulacao'];?>" style="text-decoration:none; color:#000;"><?php echo $lseltab['nometabulacao'];?></a></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseltab['ativo'];?></td>
                <td>
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="carrega" type="submit" value="Carregar" />
                <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadrel" type="submit" value="Relacionar" />
                <?php
                $selmonitab = "SELECT COUNT(*) as result FROM monitabulacao WHERE idtabulacao='".$lseltab['idtabulacao']."'";
                $eselmonitab = $_SESSION['fetch_array']($_SESSION['query']($selmonitab)) or die ("erro na query de cosnulta as monitorias com tabulação");
                if($eselmonitab['result'] >= 1) {
                }
                else {
                    echo "<input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"apaga\" type=\"submit\" value=\"Apagar\" />";
                }
                ?>
                </td>
              </tr>
                </form>
              <?php
              }
              ?>
            </table>
            </div>
      </div><br /><hr />
      <table width="572">
          <tr>
            <td width="163" class="corfd_ntab"><strong>VISUALIZAR TABULAÇÃO</strong></td>
            <td width="397" class="corfd_colcampos">
                <select name="tabs" id="tabs">
                    <option value="">SELECIONE...</option>
                    <?php
                    $seltabs = "SELECT * FROM tabulacao GROUP BY idtabulacao ORDER BY nometabulacao";
                    $eseltabs = $_SESSION['query']($seltabs) or die ("erro na query para listas as tabulações");
                    while($ltabs = $_SESSION['fetch_array']($eseltabs)) {
                        if($ltabs['idtabulacao'] == $idtabula) {
                            echo "<option value=\"".$ltabs['idtabulacao']."\" selected=\"selected\">".$ltabs['nometabulacao']."</option>";
                        }
                        else {
                            echo "<option value=\"".$ltabs['idtabulacao']."\">".$ltabs['nometabulacao']."</option>";
                        }
                    }
                    ?>                    
                </select>
            </td>
          </tr>
      </table><br />
      <?php
      $seltab = "SELECT * FROM tabulacao GROUP BY idtabulacao ORDER BY posicao";
      $eseltab = $_SESSION['query']($seltab) or die ("erro na query de consulta das tabulações");
      while($lseltab = $_SESSION['fetch_array']($eseltab)) {
      ?>
      <div class="listtab<?php echo $lseltab['idtabulacao'];?>">
       <font color="#FF0000"><strong><?php echo $_GET['msgd'] ?></strong></font>
       <table width="1024" border="0">
              <tr>
                <td width="50" class="corfd_coltexto" align="center"><strong>ID TAB.</strong></td>
                <td width="40" class="corfd_coltexto" align="center"><strong>ID. PERG.</strong></td>
                <td width="299" class="corfd_coltexto" align="center"><strong>DESCRIÇÃO PERGUNTA</strong></td>
                <td width="36" class="corfd_coltexto" align="center"><strong>ID RESP.</strong></td>
                <td width="230" class="corfd_coltexto" align="center"><strong>DESCRIÇÃO RESPOSTA</strong></td>
                <td width="48" class="corfd_coltexto" align="center"><strong>PROX. PERG.</strong></td>
                <td width="55" class="corfd_coltexto" align="center"><strong>POSIÇÃO</strong></td>
                <td width="47" class="corfd_coltexto" align="center"><strong>OBRIGA</strong></td>
                <td width="36" class="corfd_coltexto" align="center"><strong>ATIVO</strong></td>
                <td width="141"></td>
              </tr>
        <?php
        $selpergresp = "SELECT t.idreltabulacao, t.idtabulacao,t.idperguntatab,pt.descriperguntatab,t.idrespostatab,rt.descrirespostatab,t.idproxpergunta,t.posicao,t.obriga,t.ativorel FROM tabulacao t
                        INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                        INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab
                        LEFT JOIN perguntatab pr ON pr.idperguntatab = t.idproxpergunta
                        WHERE t.idtabulacao='".$lseltab['idtabulacao']."' ORDER BY t.posicao";
        $eselpergresp = $_SESSION['query']($selpergresp) or die ("erro na query de consulta das perguntas");
        while($lpergresp = $_SESSION['fetch_array']($eselpergresp)) {
        ?>
           <form action="cadtabula.php" method="post">
              <tr>
                <td class="corfd_colcampos" align="center"><input type="hidden" name="idreltab" id="idreltab" value="<?php echo $lpergresp['idreltabulacao'];?>" /><input type="hidden" name="idtabula" id="idtabula" value="<?php echo $lpergresp['idtabulacao'];?>" /><?php echo $lpergresp['idtabulacao'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['idperguntatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['descriperguntatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['idrespostatab'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['descrirespostatab'];?></td>
                <td class="corfd_colcampos" align="center"  title="<?php echo $lpergresp['descriprox'];?>"><?php echo $lpergresp['idproxpergunta'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['posicao'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['obriga'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lpergresp['ativorel'];?></td>
                <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="carregarel" type="submit" value="Carregar" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="delrel" type="submit" value="Apagar" /></td>
            </tr>
           </form>
         <?php
        }
        ?>
      </table></div>
      <?php
      }
      ?>
      <br/>
        <table width="1024" align="center">
          <tr>
            <td width="810" height="15" align="center" class="corfd_ntab"><strong>VISUALIZAÇÃO EM ARVORE DA TABULAÇÃO</strong></td>
          </tr>
        </table>
      <div id="arvoretab">
          
      </div>
</div>
