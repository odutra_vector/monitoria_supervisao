<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem título</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#cadastra').click(function() {
        var nome = $('#nome').val();
        var user = $('#user').val();
        var cpf = $('#cpf').val();
        var email = $('#email').val();
        var confemail = $('#confemail').val();
        if(nome == "" || cpf == "" || user == "" || email == "" || confemail == "") {
            alert('Os campos abaixo precisam estar preenchidos:\n\
                    Nome\n\
                    CPF\n\
                    Usuario\n\
                    E-mail\n\
                    Configuração E-mail');
             return false;
        }
        else {
        }
    })
    
    $('#tabuser_adm').tablesorter();
    $("#cpf").mask("999.999.999-99");
    $("#cpfpesq").mask("999.999.999-99");
});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="caduseradm.php" method="post">
  <table width="648" border="0">
    <tr>
      <td class="corfd_ntab" colspan="4"><strong>CADASTRO DE USUARIOS ADM</strong></td>
    </tr>
    <tr>
      <td width="109" class="corfd_coltexto"><strong>Nome</strong></td>
      <td width="218" class="corfd_colcampos"><input style="width:200px; border: 1px solid #69C; text-align:center" name="nome" id="nome" maxlength="200" type="text" /></td>
      <td class="corfd_coltexto"><strong>Apelido</strong></td>
      <td class="corfd_colcampos"><input style="width:200px; border: 1px solid #69C; text-align:center" name="apelido" id="apelido" maxlength="200" type="text" /></td>
    </tr>
    <tr>
      <td width="81" class="corfd_coltexto"><strong>CPF</strong></td>
      <td width="222" class="corfd_colcampos"><label>
        <input style="width:100px; border: 1px solid #69C; text-align:center" type="text" name="cpf" id="cpf" />
      </label></td>   
      <td class="corfd_coltexto"><strong>E-mail</strong></td>
      <td class="corfd_colcampos"><label>
        <input style="width:200px; border: 1px solid #69C; text-align:center" type="text" maxlength="100" name="email" id="email" />
      </label></td>
    </tr>
    <tr>
      <td class="corfd_coltexto"><strong>Usuario</strong></td>
      <td class="corfd_colcampos"><input style="width:100px; border: 1px solid #69C; text-align:center; text-align:center" maxlength="20" name="user" id="user" type="text" /></td>
      <td class="corfd_coltexto"><strong>Importação</strong></td>
      <td class="corfd_colcampos">
          <select name="import" id="import" style="width: 35px">
            <option value="S">S</option>
            <option value="N">N</option>
        </select>
      </td>
    </tr>
    <tr>
      <td class="corfd_coltexto"><strong>Perfil</strong></td>
      <td class="corfd_colcampos"><select name="perfil" id="perfil">
        <?php

	$sql = "SELECT idperfil_adm, nomeperfil_adm FROM perfil_adm WHERE ativo='S'";
	$exe = $_SESSION['query']($sql) or die (mysql_error());
	while($lperfil = $_SESSION['fetch_array']($exe)) {
		echo "<option value=\"".$lperfil['idperfil_adm']."\">".$lperfil['nomeperfil_adm']."</option>";
	}
	?>
      </select></td>
      <td class="corfd_coltexto"><strong>Calibragem</strong></td>
      <td class="corfd_colcampos"><select name="calib" id="calib">
        <option value="N">N</option>
        <option value="S">S</option>
      </select></td>
    </tr>
    <tr>
      <td class="corfd_coltexto"><strong>Conf. E-mail</strong></td>
      <td class="corfd_colcampos"><select name="confemail" id="confemail">
        <?php
	   $selconf = "SELECT idconfemailenv FROM confemailenv WHERE tipo='USUARIO'";
	   $eselconf = $_SESSION['query']($selconf) or die ("erro na consulta das configuraÃ§Ãµes cadastradas");
	   while($lconf = $_SESSION['fetch_array']($eselconf)) {
		   echo "<option value=\"".$lconf['idconfemailenv']."\">".$lconf['idconfemailenv']."</option>";
	   }
	   ?>
      </select></td>
      <td class="corfd_coltexto"><strong>Seleção Audios</strong></td>
      <td class="corfd_colcampos">
      	<select name="selecao" id="selecao">
      		<option value="N">N</option>
        	<option value="S">S</option>
      	</select>
      </td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>Tela Apresentação</strong></td>
        <td class="corfd_colcampos" colspan="3">
        <select name="confapres[]" id="confapres" multiple="multiple" size="4">
        <option value="" disabled="disabled">Selecione...</option>
        <option value="CALIB">CALIBRAGEM</option>
        <option value="REL">RELATÓRIOS</option>
        <option value="EBOOK">EBOOK</option>
    	</select></td>
    </tr>
    <tr>
      <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="cadastra" id="cadastra" type="submit" value="Cadastrar" /></td>
    </tr>
  </table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
<hr />
<form action="" method="post">
<table width="421" border="0">
  <tr>
  	<td class="corfd_ntab" colspan="2" align="center"><strong>PESQUISA DE USUÁRIOS CADASTRADOS</strong></td>
  </tr>
  <tr>
    <td width="159" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="252" class="corfd_colcampos"><input name="menu" type="hidden" value="useradm" /><input style="width:250px; border: 1px solid #69C; text-align:center" name="nome" type="text" value="<?php echo $_POST['nome']; ?>" />
    <input name="p" value="1" type="hidden" /></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>CPF</strong></td>
    <td class="corfd_colcampos">
    <label>
      <input style="width:100px; border: 1px solid #69C; text-align:center" type="text" name="CPF" id="cpfpesq" value="<?php echo $_POST['CPF']; ?>" />
    </label>
    </td>
  </tr>
  <tr>
  <td class="corfd_coltexto"><strong>Perfil</strong></td>
  <td class="corfd_colcampos"><select name="perfil"><option value=""></option>
  <?php
  $sql = "SELECT idperfil_adm, nomeperfil_adm FROM perfil_adm";
  $exe = $_SESSION['query']($sql) or die (mysql_error());
  while($lper = $_SESSION['fetch_array']($exe)) {
	  $selected=($_POST['perfil'] == $lper['idperfil_adm'] ? "SELECTED" : "");
	  echo "<option $selected value=\"".$lper['idperfil_adm']."\">".$lper['nomeperfil_adm']."";
  }
  ?>
  </select></td>
  </tr>
  <tr>
    <td colspan="2"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="pesquisa" type="submit" value="Pesquisar" /></td>
  </tr>
</table>
</form><br />
<div style="width: 1024px; height: 400px; overflow: auto">
    <font color="#FF0000"><strong><?php echo $_GET['msg2'] ?></strong></font>
    <?php
    $dados .= "<table width=\"1150\" border=\"0\" id=\"tabuser_adm\">";
	$dados .= "<thead>";
            $dados .= "<th width=\"146\" align=\"center\" class=\"corfd_coltexto\"><strong>ID</strong></th>";
            $dados .= "<th width=\"146\" align=\"center\" class=\"corfd_coltexto\"><strong>Nome</strong></th>";
            $dados .= "<th width=\"90\" align=\"center\" class=\"corfd_coltexto\"><strong>CPF</strong></th>";
            $dados .= "<th width=\"35\" align=\"center\" class=\"corfd_coltexto\"><strong>Perfil</strong></th>";
            $dados .= "<th width=\"116\" align=\"center\" class=\"corfd_coltexto\"><strong>E-mail</strong></th>";
            $dados .= "<th width=\"67\" align=\"center\" class=\"corfd_coltexto\"><strong>Usuario</strong></th>";
            $dados .= "<th width=\"60\" align=\"center\" class=\"corfd_coltexto\"><strong>Ativo</strong></th>";
            $dados .= "<th width=\"61\" align=\"center\" class=\"corfd_coltexto\"><strong>S_Ini</strong></th>";
            $dados .= "<th width=\"65\" align=\"center\" class=\"corfd_coltexto\"><strong>Dt.Cad</strong></th>";
            $dados .= "<th width=\"140\"></th>";
            $dados .= "<th width=\"123\"></th>";
     $dados .= "</thead>";
     $dados .= "<tbody>";
     $dadosexp .= $dados;
  $nome = mysql_real_escape_string($_POST['nome']);
  $cpf = mysql_real_escape_string(str_replace("-", "",$_POST['CPF']));
  $cpf = str_replace(".", "", $cpf);
  $perfil = mysql_real_escape_string($_POST['perfil']);
  if($nome != "") {
      $where[] = "nomeuser_adm LIKE '%$nome%'";
  }
  if($cpf != "") {
      $where[] = "cpf='$cpf'";
  }
  if($perfil != "") {
      $where[] = "idperfil_adm='$perfil'";
  }
  if($where != "") {
      $where = "WHERE ".implode(" AND ",$where);
  }
  else {
      $where = "";
  }
  $seldb = "SELECT * FROM user_adm $where ORDER BY nomeuser_adm";  
  $sql = "$seldb";
  $exe = $_SESSION['query']($sql) or die (mysql_error());
  while($luser = $_SESSION['fetch_array']($exe)) {
      $cpf1 = substr($luser['CPF'], 0, 3);
      $cpf2 = substr($luser['CPF'], 3, 3);
      $cpf3 = substr($luser['CPF'], 6, 3);
      $cpfdig = substr($luser['CPF'], 9, 2);
      $cpf = $cpf1.".".$cpf2.".".$cpf3."-".$cpfdig;
      $dados .= "<tr>";
      $dadosexp .= "<tr>";
      $dados .= "<form action=\"caduseradm.php\" method=\"post\">";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\"><input name=\"id\" type=\"hidden\" style=\"text-align:center;width:50px; border: 1px solid #FFF\" readonly=\"readonly\" value=\"".$luser['iduser_adm']."\" />".$luser['iduser_adm']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['iduser_adm']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['nomeuser_adm']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['nomeuser_adm']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$cpf."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$cpf."</td>";
      $selperfil = "SELECT nomeperfil_adm FROM perfil_adm WHERE idperfil_adm='".$luser['idperfil_adm']."'";
      $eperfil = $_SESSION['fetch_array']($_SESSION['query']($selperfil)) or die ("erro na query de consulta de perfil");
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$eperfil['nomeperfil_adm']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$eperfil['nomeperfil_adm']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['email']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['email']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['usuario']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['usuario']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['ativo']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['ativo']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['senha_ini']."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".$luser['senha_ini']."</td>";
      $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".banco2data($luser['datacad'])."</td>";
      $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\">".banco2data($luser['datacad'])."</td>";
      if($_SESSION['usuarioperfil'] != "01" && $luser['idperfil_adm'] == "01") {
      }
      if($_SESSION['usuarioperfil'] != "01" && $luser['idperfil_adm'] != "01") {
        $dados .= "<td><input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"edita\" type=\"submit\" value=\"Editar\" /></td>";
      }
      if($_SESSION['usuarioperfil'] == "01" && $luser['idperfil_adm'] != "01") {
          $dados .= "<td><input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"edita\" type=\"submit\" value=\"Editar\" /></td>";
      }
      if($_SESSION['usuarioperfil'] == "01" && $luser['idperfil_adm'] == "01") {
          $dados .= "<td><input style=\"border:1px solid #FFF; width:60px; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"edita\" type=\"submit\" value=\"Editar\" /></td>";
      }
      $dados .= "</form>";
      $dados .= "</tr>";
      $dadosexp .= "</tr>";
  }
  $dados .= "</tbody>";
  $dadosexp .= "</tbody>";
  $dados .= "</table>";
  $dadosexp .= "</table>";
  echo "<table width=\"1024\">";
    echo "<tr>";
        echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=usuarios_adm\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">EXCEL</div></a></td>";
        echo "<td><a style=\"text-decoration:none; text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=WORD&nome=usuarios_adm\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">WORD</div></a></td>";
        echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" target=\"blank\" href=\"/monitoria_supervisao/users/export.php?tipo=PDF&nome=usuarios_adm\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">PDF</div></a></td>";
    echo "</tr>";
echo "</table><br/>";
$_SESSION['dadosexp'] = $dadosexp;
echo $dados;
?>
</div>
</div>
</body>
</html>
