<?php

header('Content-type: text/html; charset=utf-8');
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");

$cor = new CoresSistema();

protegePagina();
unset($_SESSION['varsconsult']);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="initial-scale=1, maximum-scale=1" />
<meta http-equiv="Content-Type" content="text/html, charset=utf-8"/>
<title>Admin</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script src="/monitoria_supervisao/js/jquery-1.6.2.js" type="text/javascript"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/tablesorter/jquery.tablesorter.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.blockUI.js"></script>
<script type="text/javascript">
history.forward();
</script>
<script type="text/javascript">
    $(document).ready(function () {   
        $("[class*='nav']").hide();
        $("#divopcoes").hide();
        $("#tabcad").hide();
        $(".divsec").hide();
        $(".cadastrosul").hide();
        
        $("body").click(function(event) {
            var id = event.target.id;
            if(id != "confestru" && id != "usuarios" && id != "confgeral" && id != "planilhas" && id != "adicionais") {
                $(".cadastrosul").hide();
            }
        });
        
        $("div[id*='ul']").hover(function () {
            $("#divopcoes").show();
            $(".divpri").css("background-color","#FFFFFF");
            $(this).css("background-color","#EAAC2F");
            $("[class*='nav']").hide();
            $("#tabcad").hide();
            $(".divsec").hide();
            $(".cadastrosul").hide();
            var name = $(this).attr('id');
            //show its submenu
            if(name == "ulcad") {
                $(".divsec").show();
                $("#tabcad").show();
            }
            else {
                $("[class*='nav']").hide();
                $("[name="+name+"]").show();  
            }
       });

       $("div[class*='divsec']").click(function() {
           var id = $(this).attr("id");
           if($("[name='"+id+"']").css('display') == "block") {
               $("[name='"+id+"']").hide();
           }
           else {
               $(".cadastrosul").hide();
               $("[name='"+id+"']").show();
           }
       });
         
   });  
</script>
<?php
$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
?>
<link href="../stylemenu.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php
$origem = $_SERVER['HTTP_REFERER'];
if($_GET['jaberta'] == "sim") {
}
else { ?>
<div id="menu">
    <div style="width: 1024px; margin: auto">
        <div style="float: left; width: 98%;height: 40px;margin-left: 10px;margin-right: 10px;">
            <div class="divpri" id="ulcad">CADASTROS</div>
            <div class="divpri" id="ulconf">CONFIGURAÇÃO RELATÓRIOS</div>
            <div class="divpri" id="ulgest">GESTÃO</div>
            <div>
                <a style="color:#000;" href="/monitoria_supervisao/inicio.php"><div style="float:right; background-color:#FFF; border: 2px solid #EAA43B; width: 100px; height: 18px; padding-top: 6px; text-align: center; font-weight: bold">VOLTAR</div></a>
            </div>
        </div>
        <div style="float: left; width: 98%; height: 40px;margin-top: 5px; margin-left: 10px; margin-right: 10px;" id="divopcoes">
            <table id="tabcad" style="width: 100%; border-spacing: 0">
                <tr>
                    <td>
                        <div class="divsec" id="confestru">CONFIG. DE ESTRUTURA</div><br/>
                        <div>
                            <ul class="cadastrosul" name="confestru">
                                <?php
                                if($_SESSION['usuarioperfil'] == "01") {
                                  echo "<a href=\"admin.php?menu=cliente\" class=\"lia\"><li class=\"subsconfestru\">CLIENTES</li></a>";
                                }
                                echo "<a href=\"admin.php?menu=filtros\" class=\"lia\"><li class=\"subsconfestru\">FILTROS</li></a>";
                                $sql = "SELECT COUNT(*) as result FROM filtro_nomes";
                                $exe = $_SESSION['fetch_array']($_SESSION['query']($sql)) or die (mysql_error());
                                if($exe['result'] >= 1) {
                                        echo "<a href=\"admin.php?menu=estrutura\" class=\"lia\"><li class=\"subsconfestru\">ESTRUTURA</li></a>";
                                }
                                else {
                                }
                                $sql = "SELECT COUNT(*) as result FROM filtro_dados";
                                $exe = $_SESSION['fetch_array']($_SESSION['query']($sql)) or dir (mysql_error());
                                if($exe['result'] >=1) {
                                        echo "<a href=\"admin.php?menu=relestru\" class=\"lia\"><li class=\"subsconfestru\">RELACIONAMENTO ESTRUTURA</li></a>";
                                }
                                else {
                                }
                                ?>
                                <a href="admin.php?menu=indice" class="lia"><li class="subsconfestru">INTERVALO NOTAS</li></a>
                                <a href="admin.php?menu=parammoni" class="lia"><li class="subsconfestru">PARAMETROS MONITORIA</li></a>
                                <a href="admin.php?menu=confemail" class="lia"><li class="subsconfestru">CONFIG. DE ENVIO E-MAIL</li></a>
                                <a href="admin.php?menu=alertas" class="lia"><li class="subsconfestru">ALERTAS</li></a>                             
                            </ul>
                        </div>
                    </td>
                    <td>
                        <div class="divsec" id="usuarios">USUÁRIOS</div><br/>
                        <div>
                            <ul class="cadastrosul" name="usuarios">
                                <a href="admin.php?menu=peradm" class="lia"><li class="subsusers">PERFIL ADMINISTRADOR</li></a>
                                <a href="admin.php?menu=useradm" class="lia"><li class="subsusers">USUÁRIOS ADMINISTRADORES</li></a>
                                <a href="admin.php?menu=perweb" class="lia"><li class="subsusers">PERFIL WEB</li></a>
                                <a href="admin.php?menu=userweb" class="lia"><li class="subsusers">USUÁRIOS WEB</li></a>
                                <a href="admin.php?menu=monitor" class="lia"><li class="subsusers">MONITOR</li></a>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <div class="divsec" id="confgeral">CONFIG. GERAIS</div><br/>
                        <div>
                            <ul class="cadastrosul" name="confgeral">
                                <a href="admin.php?menu=calib" class="lia"><li class="subsconf">CALIBRAGEM</li></a>
                                <a href="admin.php?menu=calendario" class="lia"><li class="subsconf">CALENDÁRIO</li></a>
                                <a href="admin.php?menu=estrufluxo" class="lia"><li class="subsconf">ESTRUTURA FLUXO</li></a>
                                <a href="admin.php?menu=fluxo" class="lia"><li class="subsconf">RELACIONAMENTO FLUXO</li></a>
                                <a href="admin.php?menu=motivo" class="lia"><li class="subsconf">MOTIVO</li></a>
                                <a href="admin.php?menu=dimensiona" class="lia"><li class="subsconf">DIMENSIONAMENTO</li></a>
                                <a href="admin.php?menu=operador" class="lia"><li class="subsconf">OPERADOR</li></a>
                                <a href="admin.php?menu=listaoperador" class="lia"><li class="subsconf">LISTA OPERADOR</li></a>
                                <a href="admin.php?menu=supervisor" class="lia"><li class="subsconf">SUPERVISOR OPERADOR</li></a>
                                <a href="admin.php?menu=monitorias" class="lia"><li class="subsconf">MONITORIAS</li></a>
                                <a href="admin.php?menu=improdutivas" class="lia"><li class="subsconf">IMPRODUTIVAS</li></a>
                                <a href="admin.php?menu=selecao" class="lia"><li class="subsconf">SELEÇÃO</li></a>
                            </ul>
                        </div>
                    </td>
                    <td>
                        <div class="divsec" id="planilhas">PLANILHAS</div><br/>
                        <ul class="cadastrosul" name="planilhas">
                            <a href="admin.php?menu=aval_plan" class="lia"><li class="subsplan">AVALIAÇÕES PLANILHA</li></a>
                            <a href="admin.php?menu=formulas" class="lia"><li class="subsplan">FORMULAS NOTAS/ MÉDIAS</li></a>
                            <a href="admin.php?menu=perguntas" class="lia"><li class="subsplan">PERGUNTAS</li></a>
                            <a href="admin.php?menu=subgrupo" class="lia"><li class="subsplan">SUB-GRUPOS</li></a>
                            <a href="admin.php?menu=grupo" class="lia"><li class="subsplan">GRUPOS</li></a>
                            <a href="admin.php?menu=planilha" class="lia"><li class="subsplan">PLANILHA</li></a>
                            <a href="admin.php?menu=tabulacao" class="lia"><li class="subsplan">TABULAÇÃO</li></a>
                            <a href="admin.php?menu=perg_resptab" class="lia"><li class="subsplan">PERG. RESP. - TABULAÇÃO</li></a>
                        </ul>
                    </td>
                    <td>
                        <div class="divsec" id="adicionais">ADICIONAIS</div><br/>
                        <ul class="cadastrosul" name="adicionais">
                            <a href="admin.php?menu=paramebook" class="lia"><li class="subsadd">CADASTRO E-BOOK</li></a>
                            <a href="admin.php?menu=orienta" class="lia"><li class="subsadd">ORIENTAÇÕES</li></a>
                            <a href="#" class="lia"><li class="subsadd">ASSUNTOS CHAT</li></a>
                        </ul>
                    </td>
                </tr>
            </table>
            <ul class="nav" name="ulconf">  
                <a href="admin.php?menu=relmoni" class="lia"><li>MONITORIAS</li></a>
                <a href="admin.php?menu=relresult" class="lia"><li>RESULTADOS</li></a>
                <a href="admin.php?menu=relespe" class="lia"><li>RESPOSTAS ESPECIFICAS</li></a>
            </ul>  
            <ul class="nav" name="ulgest">
                <a href="admin.php?menu=filamonitoria" class="lia"><li>FILA MONITORIAS</li></a>
                <a href="admin.php?menu=fechamento" class="lia"><li>FECHAMENTO</li></a>
                <a href="admin.php?menu=exportacao" class="lia"><li>EXPORTAÇÃO BASE</li></a>
                <a href="admin.php?menu=exportacaocalib" class="lia"><li>EXPORTAÇÃO BASE CALIB.</li></a>
                <a href="admin.php?menu=exportacaoimp" class="lia"><li>EXPORTAÇÃO IMPORTAÇÕES</li></a>
                <a href="admin.php?menu=log" class="lia"><li>LOG</li></a>
            </ul>
        </div>
    </div>
</div>
<?php } ?>
<div id="tudo">
<div id="conteudo" class="corfd_pag">
<?php
$sql = "SELECT * FROM perfil_adm WHERE idperfil_adm='".$_SESSION['usuarioperfil']."'";
$linha = $_SESSION['fetch_array']($_SESSION['query']($sql)) or die (mysql_error());
$sqltelas = explode(",",$linha['tabelas']);
foreach($sqltelas as $tl) {
    $telas[] = trim($tl);
}
$include_once = $_GET['menu'];
$tabinclude_once = new TabelasSql();
$tabinclude_once->Paginclude_once();
$tabinclude_once->Pagconfig();

if($include_once != "") {
    switch($include_once) {
        case "$include_once";
            $pagina = $tabinclude_once->pagina[$include_once];
            if(in_array($pagina,$telas)) {
                if(file_exists($tabinclude_once->include_once[$include_once])) {
                    include_once $tabinclude_once->include_once[$include_once];
                    break;
                }
                else {
                    include_once ($rais.'/monitoria_supervisao/erro.php');
                    break;
                }
            }
            else {
                if($include_once != "relacionausers") {
                ?>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $("input[type*='submit']").remove();
                    })
                </script>
                <?php
                }
                if(file_exists($tabinclude_once->include_once[$include_once])) {
                    include_once $tabinclude_once->include_once[$include_once];
                    break;
                }
                else {
                    include_once ($rais.'/monitoria_supervisao/erro.php');
                    break;
                }
            }
    }
}
else {
}
?>
</div>
<div id="rodape">
    <table align="center" width="1024" border="0">
        <tr>
            <td align="center" style="color:#FFF"><strong>VECTOR DSI TEC</strong></td>
        </tr>
        <tr>
            <td height="15" align="center"><font color="#FFFFFF">&copy 2010 Todos os direitos reservados</font></td>
        </tr>
    </table>
</div>
</div>
</body>
</html>
