<?php

$pagina = $_GET['menu'];
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');

$tipo = "SISTEMA";
if(isset($_GET['idcli'])) {
    $db = mysql_real_escape_string($_GET['dbcli']);
    $_SESSION['seldb']($db);
    $idcli = mysql_real_escape_string($_GET['idcli']);
    $selcli = "SELECT * FROM dados_cli WHERE iddados_cli='$idcli'";
    $eselcli = $_SESSION['fetch_array']($_SESSION['query']($selcli)) or die ("erro na query de consulta dos dados do cliente");
    $nome = $eselcli['nomedados_cli'];
    $site = $eselcli['site'];
    $imagem = $eselcli['imagem'];
    $tipotopo = $eselcli['tipotopo'];
    $imgbanner = $eselcli['imgbanner'];
    $ass = $eselcli['assdigital'];
    $textass = $eselcli['textoass'];
    $_SESSION['seldb']($_SESSION['selbanco']);
    if($_SESSION['selbanco'] != $db) {
        $dbcli = $db;
    }
    else {
        $dbcli = $_SESSION['selbanco'];
    }
    $cor->AltCorCli($dbcli, $idcli, $tipo);
}
else {
    $cor->AltCorCli($dbcli, $idcli, $tipo);
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/jscolor/jscolor.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        <?php
        if(isset($_GET['idcli'])) {
            if($ass == "S") {
                echo "$('.textoass').show();\n";
            }
            if($ass == "N") {
                echo "$('.textoass').hide();\n";
            }
        }
        else {
            echo "$('.textoass').hide();";
        }
        ?>
        $('#ass').change(function() {
            var ass = $('#ass').val();
            if(ass == "S") {
                $(".textoass").show();
            }
            if(ass == "N") {
                $(".textoass").hide();
            }
        });
        
        $("#cadastrar").click(function() {
            var nome = $("#nome").val();
            var site = $("#site").val();
            var img = $("#imagem").val();
            var ass = $("#ass").val();
            var topo = $("#ptopo").val();
            if(nome == "" || site == "" || img == "" || ass == "" || topo == "") {
                alert("Favor verificar os campos ''Nome, Site, Logo, Painel Topo e Assinatura'', pois todos são obrigatórios");
                return false;
            }
            else {
                $.blockUI({ message: '<strong>AGUARDE GERANDO BANCO DE DADOS...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
            }
        });
        
        $("input[id*='apagar']").click(function() {
            var apagar = confirm("Voce deseja apagar o banco de dados, todos os dados serao perdidos!!!");
            if (apagar == true) {
                $.blockUI({ message: '<strong>AGUARDE APAGANDO BANCO DE DADOS...</strong>', css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5,
                color: '#fff'
                }})
                var db = $(this).attr("name");
                $.post("delbanco.php",{del:"del",nomedb:db},function(ret) {
                    if(ret == 1) {
                        $.unblockUI();
                        window.location = "/monitoria_supervisao/index.php?msg=Banco de dados apagado com sucesso!!!";
                    }
                    else {
                        $.unblockUI();
                        alert("BANCO DE DADOS APAGADO COM SUCESSO");
                        location.reload();
                    }
                });
            }
            else {
                return false;
            }
        })
    })
</script>
<script type="text/javascript" src="../js/tinymce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	// General options
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

	// Theme options
	theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,

	// Example content CSS (should be your site CSS)
	content_css : "css/example.css",

	// Drop lists for link/image/media/template dialogs
	template_external_list_url : "js/template_list.js",
	external_link_list_url : "js/link_list.js",
	external_image_list_url : "js/image_list.js",
	media_external_list_url : "js/media_list.js",

	// Replace values for the template plugin
	template_replace_values : {
		username : "Some User",
		staffid : "991234"
	}
});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag" style="background-color: <?php echo $cor->corfd_pag; ?>">
  <form action="cadcli.php" method="post" name="cadcliente" enctype="multipart/form-data">
  <table width="879" border="0">
    <tr>
    	<td class="corfd_ntab" colspan="4" align="center"><strong>CADASTRO DE CLIENTES</strong></td>
    </tr>
    <tr>
        <td width="166" class="corfd_coltexto"><strong>NOME</strong></td>
        <td class="corfd_colcampos" colspan="4"><input type="hidden" name="idcli" value="<?php if(isset($_GET['idcli'])) { echo $idcli; } else  {}?>" /><input name="cliente" type="hidden" value="<?php if(isset($_GET['idcli'])) { echo $db;} else { }?>"  /><input style="width:150px; border: 1px solid #333; text-align:center" name="nome" id="nome" type="text" value="<?php if(isset($_GET['idcli'])) { echo str_replace("monitoria_", "",$db);} else { if(isset($_GET['nome'])) { echo $_GET['nome'];} else {} }?>" <?php if(isset($_GET['idcli'])) { echo "readonly=\"readonly\"";} else {}?> /></td>
    </tr>
    <tr>
    	<td class="corfd_coltexto"><strong>SITE</strong></td>
        <td class="corfd_colcampos" colspan="4"><input style="width:350px; border: 1px solid #333; text-align:center" name="site" id="site" type="text" value="<?php if(isset($_GET['idcli'])) { echo $site;} else { if(isset($_GET['site'])) { echo $_GET['site'];} else {} }?>" /></td>
    </tr>
    <tr>
    	<td class="corfd_coltexto"><strong>LOGO</strong></td>
        <td width="282" class="corfd_colcampos"><input type="file" name="imagem" id="imagem" style=" border: 1px solid #333;" value="" /></td>
        <td width="121" class="corfd_coltexto"><strong>IMAGEM</strong></td>
        <td width="292" class="corfd_colcampos"><input type="hidden" name="img" value="<?php echo $imagem;?>"/>
            <?php
            if(isset($_GET['idcli'])) {
                echo "<a target=\"_blank\" href=\"/monitoria_supervisao/images/logo/$imagem\">$imagem</a>";
            }
            else {
            }
            ?>
            </td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>PAINEL TOPO</strong></td>
        <td class="corfd_colcampos">
            <select name="topo" id="ptopo">
                <?php
                if(isset($_GET['idcli'])) {
                    echo "<option value=\"\">SELECIONE...</option>";
                }
                else {
                    echo "<option value=\"\" selected=\"selected\">SELECIONE...</option>";	
                }
                $topo = array('BANNER','ORIENTACAO','META');
                foreach($topo as $t) {
                    if($t == $tipotopo) {
                        echo "<option value=\"$t\" selected=\"selected\">$t</option>";
                    }
                    else {
                        echo "<option value=\"$t\">$t</option>";
                    }
                }
                ?>
            </select> 
            <?php
            if(isset($_GET['idcli'])) {
                echo "<a target=\"_blank\" href=\"/monitoria_supervisao/images/logo/$imgbanner\">$imgbanner</a>";
            }
            else {
            }
            ?>
            </td>
        <td class="corfd_coltexto"><strong>IMAGEM BANNER</strong></td>
        <td class="corfd_colcampos"><input name="imgbanner" id="imgbanner" type="file" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>COR FD PAG.</strong></td>
         <td class="corfd_colcampos" colspan="4"><input id="corfd_pag" name="corfd_pag" type="text" class="color" value="<?php echo $cor->corfd_pagdb; ?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>COR FD NOME TABELA.</strong></td>
         <td class="corfd_colcampos" colspan="4"><input id="corfd_ntab" name="corfd_ntab" type="text" class="color" value="<?php echo $cor->corfd_ntabdb; ?>" /></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>COR FD COL. TEXTO</strong></td>
        <td class="corfd_colcampos" colspan="4"><input type="text" name="corfd_coltexto" id="corfd_coltexto" class="color" value="<?php echo $cor->corfd_coltextodb; ?>"/></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>COR FD COL. CAMPO</strong></td>
        <td class="corfd_colcampos" colspan="4"><input type="text" name="corfd_colcampos" id="corfd_colcampos" class="color" value="<?php echo $cor->corfd_colcamposdb; ?>"/></td>
    </tr>
    <tr>
        <td class="corfd_coltexto"><strong>ASS DIGITAL</strong></td>
        <td bgcolor="#FFFFFF" colspan="4">
            <select name="assdigital" id="ass">
            <?php
            $opass = array('S','N');
            if(isset($_GET['idcli'])) {
                echo "<option value=\"".$ass."\" selected=\"seelcted\">".$ass."</option>";
                foreach($opass as $op) {
                    if($op == $ass) {

                    }
                    else {
                        echo "<option value=\"".$op."\">".$op."</option>";
                    }
                }
            }
            else {
                if(isset($_GET['ass'])) {
                    echo "<option value=\"".$_GET['ass']."\" selected=\"selected\">".mysql_real_escape_string($_GET['ass'])."</option>";
                    foreach($opass as $op) {
                        if($op == $_GET['ass']) {

                        }
                        else {
                            echo "<option value=\"".$op."\">".$op."</option>";
                        }
                    }
                }
                else {
                    echo "<option value=\"\">Selecione...</option>";
                    foreach($opass as $op) {
                        echo "<option value=\"".$op."\">".$op."</opiton>";
                    }
                }
            }
            ?>
            </select>
        </td>
    </tr>
    <tr class="textoass">
        <td class="corfd_coltexto"><strong>TEXTO ASS</strong></td>
        <td colspan="4" class="corfd_colcampos"><textarea name="textoass" id="textoass" style="width:700px; height: 400px"><?php if(isset($_GET['idcli'])) { echo $textass;} else { if(isset($_GET['textoass'])) { echo $_GET['textoass']; } else {} }?></textarea></td>
    </tr>
  <tr>
      <?php
      if(isset($_GET['idcli'])) {
          echo "<td colspan=\"2\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"altcad\" type=\"submit\" value=\"Alterar\" /> <input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"novo\" type=\"submit\" value=\"Novo\" /></td>";
      }
      else {
          echo "<td colspan=\"2\"><input style=\"border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)\" name=\"cadastrar\" id=\"cadastrar\" type=\"submit\" value=\"Cadastrar\" /></td>";
      }
      ?>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
<hr />
<table width="400" border="0">
  <tr>
    <td width="152" align="center" class="corfd_coltexto"><strong>CLIENTES CADASTRADOS</strong></td>
    <td width="60"></td>
  </tr>
  <?php
  $consult = "SHOW DATABASES LIKE 'monitoria_%'";
  $econsult = $_SESSION['query']($consult);
	while($dbs = $_SESSION['fetch_assoc']($econsult)) {
            $seldb = $_SESSION['seldb']($dbs['Database (monitoria_%)']);
            $nome = str_replace("monitoria_","", $dbs['Database (monitoria_%)']);
            $sdadoscli = "SELECT iddados_cli FROM dados_cli WHERE nomedados_cli='".$nome."'";
            $edadoscli = $_SESSION['fetch_array']($_SESSION['query']($sdadoscli)) or die (mysql_error());
	  echo "<form action=\"cadcli.php\" method=\"post\" >";
	  echo "<tr>";
	  echo "<td align=\"center\" ><input type=\"hidden\" name=\"idcli\" value=\"".$edadoscli['iddados_cli']."\" /><input name=\"dbname\" type=\"hidden\" value=\"".$dbs['Database (monitoria_%)']."\" /><input style=\"width:200px; border: 1px solid #FFF; text-align:center; background-color:#FFF\" name=\"cliente\" type=\"text\" value=\"".$dbs['Database (monitoria_%)']."\" /></td>";
	  echo "<td> <input style=\"border:1px solid #FFF; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"alterar\" type=\"submit\" value=\"altera\" /></td>";
	  echo "<td> <input style=\"border:1px solid #FFF; heigth:17px; background-image:url(../images/button.jpg); text-align:center\" name=\"".$dbs['Database (monitoria_%)']."\" id=\"apagar\" type=\"button\" value=\"apagar\" /></td>";
	  echo "</tr>";
	  echo "</form>";
	}
  $_SESSION['seldb']($_SESSION['selbanco']);
  ?>
</table>
<font color="#FF0000"><strong><?php echo $_GET['msg2'] ?></strong></font>
</div>
</body>
</html>