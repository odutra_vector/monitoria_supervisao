<?php

ini_set('max_execution_time', 3600);

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.smtp.php');
include_once($rais.'/monitoria_supervisao/phpmailer/class.phpmailer.php');
include_once($rais.'/monitoria_supervisao/classes/class.envemail.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once($rais.'/monitoria_supervisao/classes/excel_reader2.php');

function versao() {
    $pastas = array('/monitoria_clientis','/monitoria_clientis/admin','/monitoria_clientis/classes','/monitoria_clientis/config','/monitoria_clientis/getid3','/monitoria_clientis/monitor','/monitoria_clientis/users');
    $ultatu = 0;

    foreach($pastas as $cam){
        $ca = $_SERVER['DOCUMENT_ROOT']."$cam";
        $arqs = scandir($ca);
        foreach($arqs as $arq) {
            if($arq != "." && $arq != ".." && $arq != ".git") {
                if(is_numeric(strtotime(filemtime($ca."/$arq")))) {
                    if(filemtime($ca."/$arq") > $ultatu) {
                        $ultatu = filemtime($ca."/$arq");
                    }
                }
            }
        }
    }
    echo date("d/m/Y H:i:s", $ultatu);
}

function mimes() {
    return array_unique(array("application/hta,hta",
                "application/java,class",
                "application/java-byte-code,class",
                "application/msword,doc",
                "application/msword,dot",
                "application/octet-stream,*",
                "application/octet-stream,bin",
                "application/octet-stream,class",
                "application/octet-stream,dms",
                "application/octet-stream,zip",
                "application/x-ms-dos-executable,exe",
                "application/x-shellscript,sh",
                "application/octet-stream,lha",
                "application/octet-stream,lzh",
                "application/pdf,pdf",
                "application/rtf,rtf",
                "application/vnd.ms-excel,xla",
                "application/vnd.ms-excel,xlc",
                "application/vnd.ms-excel,xlm",
                "application/vnd.ms-excel,xls",
                "application/x-msexcel,xls",
                "application/xls,xls",
                "application/vnd.ms-excel,xlt",
                "application/vnd.ms-excel,xlw",
                "application/vnd.ms-powerpoint,pot",
                "application/vnd.ms-powerpoint,pps",
                "application/vnd.ms-powerpoint,ppt",
                "application/winhlp,hlp",
                "multipart/x-zip,zip",
                "application/x-zip-compressed,zip",
                "application/x-rar-compressed,rar",
                "application/x-rar,rar",
                "application/x-gzip,gz",
                "application/x-java-class,class",
                "application/x-javascript,js",
                "application/x-mplayer2,asx",
                "application/x-msaccess,mdb",
                "application/x-tar,tar",
                "application/xml,xml",
                "application/x-php,php",
                "application/zip,zip",
                "audio/basic,au",
                "audio/basic,snd",
                "audio/mid,mid",
                "audio/mid,rmi",
                "audio/mpeg,mp3",
                "audio/x-mpegurl,m3u",
                "audio/x-pn-realaudio,ra",
                "audio/x-pn-realaudio,ram",
                "audio/x-wav,wav",
                "image/bmp,bmp",
                "image/gif,gif",
                "image/jpeg,jpe",
                "image/jpeg,jpeg",
                "image/jpeg,jpg",
                "image/x-icon,ico",
                "multipart/x-zip,zip",
                "text/asp,asp",
                "text/css,css",
                "text/html,html",
                "text/html,stm",
                "text/plain,bas",
                "text/plain,c",
                "text/csv,csv",
                "text/x-comma-separated-values,csv",
                "text/plain,h",
                "text/plain,txt",
                "text/richtext,rtx",
                "text/x-component,htc",
                "text/xml,xml",
                "video/mpeg,mp2",
                "video/mpeg,mpa",
                "video/mpeg,mpe",
                "video/mpeg,mpg",
                "video/mpeg,mpeg",
                "video/mpeg,mpv2",
                "video/quicktime,mov",
                "video/quicktime,qt",
                "video/x-la-asf,lsf",
                "video/x-la-asf,lsx",
                "video/x-ms-asf,asf",
                "video/x-ms-asf,asr",
                "video/x-ms-asf,asx",
                "video/x-msvideo,avi",
                "video/x-sgi-movie,movie",
                "x-world/x-vrml,flr",
                "x-world/x-vrml,vrml",
                "x-world/x-vrml,wrl",
                "x-world/x-vrml,wrz",
                "x-world/x-vrml,xaf",
                "x-world/x-vrml,xof",));
}

function geraSenha($tamanho, $maiusculas, $numeros, $simbolos) {
    $lmin = 'abcdefghijklmnopqrstuvwxyz';
    $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $num = '1234567890';
    $simb = '!@#$%*-';
    $retorno = '';
    $caracteres = '';

    $caracteres .= $lmin;
    if ($maiusculas) $caracteres .= $lmai;
    if ($numeros) $caracteres .= $num;
    if ($simbolos) $caracteres .= $simb;

    $len = strlen($caracteres);
    for ($n = 1; $n <= $tamanho; $n++) {
    $rand = mt_rand(1, $len);
    $retorno .= $caracteres[$rand-1];
    }
    return $retorno;
}

function sec_hora ($sec) {
    $seconds = $sec;

    $hours = floor($seconds / 3600);
    $seconds -= $hours * 3600;
    $minutes = floor($seconds / 60);
    $seconds -= $minutes * 60;

    $hora = str_pad($hours, 2, "0",STR_PAD_LEFT).":".str_pad($minutes, 2, "0",STR_PAD_LEFT).":".str_pad($seconds, 2, "0",STR_PAD_LEFT);
    return $hora;
}

function periodo ($info) {

  $data = date('Y-m-d');
  $selperiodo = "SELECT COUNT(*) as result, idperiodo, dataini, datafim, diasprod,dtinictt,dtfimctt FROM periodo
                WHERE '$data' >= dataini AND '$data' <= datafim";
  $eperiodo = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_error());
  if($eperiodo['result'] >= 1) {
      $_SESSION['periodo'] = $eperiodo['idperiodo'];
  }
  else {
    $_SESSION['periodo'] = "";
  }
  if($_SESSION['periodo'] == "") {
    return 0;
  }
  else {
      if($info == "") {
          return $_SESSION['periodo'];
      }
      if($info == "datas") {
        $periodo = array('idperiodo' => $_SESSION['periodo'], 'dataini' => $eperiodo['dataini'], 'datafim' => $eperiodo['datafim'],'diasprod' => $eperiodo['diasprod'],'dtinictt'=> $eperiodo['dtinictt'],'dtfimctt' => $eperiodo['dtfimctt']);
        return $periodo;
      }
  }
}

function primeirodiautil() {
    $data = date('Y-m-01');
    $feriados = getFeriados(date('Y'));
    $i = 0;
    while($i == 0) {
        $primeiroutil = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($data,5,2), substr($data,8,2), substr($data,0,4)));
        if(($primeiroutil == 6 || $primeiroutil == 0) || in_array(substr($data,5,2)."-".substr($data,8,2),  $feriados)) {
            $retorno[] = $data;
            $data = date('Y-m-d',mktime (0, 0, 0, date('m') , substr($data,8,2)+1 , date('Y')));
        }
        else {
            $retorno[] = $data;
            $i = 1;
        }
    }
    return $retorno;
}

function ultdiames ($dt) {
    if($dt == "") {
        $data = date("dmY");
    }
    else {
        $data = $dt;
    }
  $dtatu_mes = substr($data,2,2);
  $dtatu_ano = substr($data,4,4);
  $pdiames = $dtatu_ano."-".$dtatu_mes."-01";
  if($dtatu_mes == "12") {
    $ultdiames = ($dtatu_ano+1)."-01-01";
  }
  else {
    $ultdiames = $dtatu_ano."-".($dtatu_mes+1)."-01";
  }
  $ultdiames = mktime(0,0,0,substr($ultdiames,5,2),substr($ultdiames,8,2),substr($ultdiames,0,4));
  $ultdiames -= 86400;
  $ultdiames = date("Y-m-d", $ultdiames);
  return $ultdiames;
}

function verifdata ($data) {
  $data = str_replace("/","",$data);
  $dt_dia = substr($data, 0, 2);
  $dt_mes = substr($data, 2, 2);
  $dt_ano = substr($data, 4, 4);
  if(checkdate($dt_mes,$dt_dia,$dt_ano)) {
    return true;
  }
  else {
    return false;
  }
}

function checkhora ($value) {
      $hora = substr($value,0,2);
      $min = substr($value,2,2);
      $seg = substr($value,4,2);
      if(eregi("$%}{&*)(#@$!-/;?<>\\", $value) || strlen($value) <> 6) {
            return false;
      }
      if($hora >= 0 && $hora <= 23) {
          if($min >= 0 && $min <= 59) {
              if($seg >= 0 && $seg <= 59) {
                  return true;
              }
              else {
                  return false;
              }
          }
          else {
              return false;
          }
      }
}

function dataimp ($data) {
    $data = substr($data,4,4)."-".substr($data,2,2)."-".substr($data,0,2);
    return $data;

}

function data2banco ($d2b) {
    if(!empty($d2b)){
        $d2b_ano=substr($d2b,6,4);
        $d2b_mes=substr($d2b,3,2);
        $d2b_dia=substr($d2b,0,2);
        if(checkdate($d2b_mes, $d2b_dia,$d2b_ano)) {
            $d2b="$d2b_ano-$d2b_mes-$d2b_dia";
        }
        else {
            $d2b = "0000-00-00";
        }
    }
    return $d2b;
}

function banco2data($b2d) {
    if($b2d=="0000-00-00" or empty($b2d)){
        $b2d="";
        return $b2d;
    }
    else{
        $b2d_ano=substr($b2d,0,4);
        $b2d_mes=substr($b2d,5,2);
        $b2d_dia=substr($b2d,8,2);
        $b2d=$b2d_dia.'/'.$b2d_mes.'/'.$b2d_ano;
        return $b2d;
    }
}

function gerauser($nome) {
    $n = explode(" ",$nome);
    if($n == 1) {
        $user = $n;
        $checkuser = "SELECT count(*) as r FROM user_web WHERE usuario='$user'";
        $echeckuser = $_SESSION['fetch_array']($_SESSION['query']($checkuser)) or die (mysql_error());
        if($echeckuser['r'] >= 1) {
            $user = $n.".".strtolower($_SESSION['nomecli']);
        }
    }
    else {
        $user = $n[0].".".$n[count($n) - 1];
        $checkuser = "SELECT count(*) as r FROM user_web WHERE usuario='$user'";
        $echeckuser = $_SESSION['fetch_array']($_SESSION['query']($checkuser)) or die (mysql_error());
        if($echeckuser['r'] >= 1) {
            if(count($n) > 2) {
                $user = $n[0].".".$n[1];
            }
            else {
                $user = substr($n[0],0,2).".".end($n);
            }
        }
    }
    return strtolower($user);
}

function getFeriados($ano){
  $dia = 86400;
  $datas = array();
  $datas['pascoa'] = easter_date($ano);
  $datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
  $datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
  $datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
  $feriados = array (
              '01-01',
              //'02-01', // Navegantes
              date('m-d',$datas['carnaval']),
              date('m-d',$datas['sexta_santa']),
              date('m-d',$datas['pascoa']),
              '04-21',
              '05-01',
              date('m-d',$datas['corpus_cristi']),
              '07-09',
              '10-12',
              '11-02',
              '11-15',
              '12-25',
           );
  return $feriados;
}

function & tipodado ($dados) {
  $obs = "";
  foreach($dados as $key => $value) {
    $ck = 0;
    $seltipo = "SELECT tipo,tamanho,caracter,restricao FROM coluna_oper WHERE nomecoluna='$key'";
    $etipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
    $caracter = str_split($etipo['caracter']);
    $restri = explode(",", $etipo['restricao']);
    if($caracter[0] != "") {
        foreach($caracter as $char) {
            if(strstr($value,$char)) {
                $obs = "Existem caracteres não permitidos no campo $key.";
                $ck = 1;
                break;
            }
        }
    }
    if($ck == 0) {
        if($etipo['tipo'] == "DATE") {
            if(strlen($value) <> 8 || !is_numeric($value)) {
              $obs = "$obs, O campo $key não está no formato válido ddmmaaaa";
            }
            else {
                $dia = substr($value, 0, 2);
                $mes = substr($value, 2, 2);
                $ano = substr($value, 4, 4);
                if(in_array($value, $restri) || $etipo['restricao'] == "") {
                   if(checkdate($mes, $dia, $ano)) {
                       if($key == "datactt") {
                           $check = "SELECT ('".substr($value, 4, 4)."-".substr($value,2,2)."-".substr($value,0,2)."' > CURDATE()) as r";
                           $echeck = $_SESSION['fetch_array']($_SESSION['query']($check)) or die (mysql_error());
                           if($echeck['r'] == 0) {
                               $obs = $obs;
                           }
                           else {
                               $obs = "A data do contato é superior a data de hoje, não sendo permitida a sua importação!";
                           }
                       }
                       else {
                            $obs = $obs;
                       }
                   }
                   else {
                       $obs = "$obs, O campo $key não é uma data válida";
                   }
                }
                else {
                     $obs = "$obs, O campo $key não possui os caracteres necessários";
                }
            }
        }
        if($etipo['tipo'] == "TIME") {
          $hora = substr($value,0,2);
          $min = substr($value,2,2);
          $seg = substr($value,4,2);
          if(strlen($value) <> 6 || !is_numeric($value)) {
                $obs = "$obs, O campo $key não está no formato correto hhmmss";
          }
          if($hora >= 0 && $hora <= 23) {
              $obs = $obs;
              if($min >= 0 && $min <= 59) {
                  $obs = $obs;
                  if($seg >= 0 && $seg <= 59) {
                       if(in_array($value, $restri) || $etipo['restricao'] == "") {
                            $obs = $obs;
                       }
                       else {
                            $obs = "$obs, O campo $key não possui os caracteres necessários";
                       }
                  }
                  else {
                      $obs = "$obs, O campo $key não é um horário válido";
                  }
              }
              else {
                  $obs = "$obs, O campo $key não é um horário válido";
              }
          }
          else {
              $obs = "$obs, O campo $key não é um horário válido";
          }
        }
        if($etipo['tipo'] == "INT") {
          if(!is_numeric($value)) {
                $obs = "$obs, O campo $key só pode conter números";
          }
          else {
               if(strlen($value) > $etipo['tamanho']) {
                   $obs = "O campo $key possui mais caracteres que o permitido";
               }
               else {
                    if(in_array($value, $restri) || $etipo['restricao'] == "") {
                        if($key == "idmonitoriavenda") {
                            $selvenda = "SELECT count(*) as r FROM monitoria WHERE idmonitoria='$value'";
                            $eselvenda = $_SESSION['fetch_array']($_SESSION['query']($selvenda)) or die (mysql_error());
                            if($eselvenda['r'] >= 1) {
                                $obs = $obs;
                            }
                            else {
                                $obs = "$obs, A monitoria de venda informada, não existe";
                            }
                        }
                        else {
                            $obs = $obs;
                        }
                    }
                    else {
                         $obs = "$obs, O campo $key não possui os caracteres necessários";
                    }
               }
          }
        }
        if($etipo['tipo'] == "VARCHAR") {
            if(!is_string($value)) {
                  $obs = "$obs, O campo $key não é um texto válido";
            }
            else {
                if(strlen($value) > $etipo['tamanho']) {
                    $obs = "O campo $key possui mais caracteres que o permitido";
                }
                else {
                    if(in_array($value, $restri) || $etipo['restricao'] == "") {
                        if($key == "idmonitoriavenda") {
                            $selvenda = "SELECT count(*) as r FROM monitoria WHERE idmonitoria='$value'";
                            $eselvenda = $_SESSION['fetch_array']($_SESSION['query']($selvenda)) or die (mysql_error());
                            if($eselvenda['r'] >= 1) {
                                $obs = $obs;
                            }
                            else {
                                $obs = "$obs, A monitoria de venda informada, não existe";
                            }
                        }
                        else {
                            $obs = $obs;
                        }
                    }
                    else {
                         $obs = "$obs, O campo $key não possui os caracteres necessários";
                    }
                }
            }
        }
    }
  }
  return $obs;
}

function & unzip($audiosup, $camaudio, $idrel,$tiposerver,$conftp) { // função para retirar do zip os audios enviados no upload
         $selparam = "SELECT * FROM param_moni pm INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni WHERE cr.idrel_filtros='$idrel'";
        $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
        $ext = explode(",",$eselparam['tpaudio']);
        $data = date('dmY'); // pega a data atual
        //pegando o caminho completo do arquivo zip
        //abrindo o arquivo zip
        if($tiposerver == "FTP") {
            $audiosup = zip_open($audiosup);
            $count = 0;
            while(($elemento = zip_read($audiosup)) != false) {
                $nome = zip_entry_name($elemento);
                if($nome != '.' && $nome != '..') {
                    if(zip_entry_filesize($elemento)) {
                        $count++;
                        $caminho = removeAcentos(str_replace("C:","",$_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/".$nome));
                        $arquivo = fopen($caminho, 'wb');
                        fwrite($arquivo, zip_entry_read($elemento, zip_entry_filesize($elemento)));
                        $permissao = chmod($caminho,0777);
                        fclose($arquivo);
                        $chdir = ftp_chdir($conftp, $camaudio);
                        $envia = ftp_put($conftp, $nome, $caminho, FTP_BINARY);
                        unlink($caminho);
                    }
                }
            }
        }
        else {
            $audiosup = zip_open($audiosup);
            $count = 0;
            //lendo todo o conteudo do arquivo zip
            while(($elemento = zip_read($audiosup)) != false) {
                    //pegando o nome do arquivo/diretorio em questao
                    $nome = removeAcentos(str_replace("'","",zip_entry_name($elemento)));
                    //descartando os diretorios "." e ".."
                    if($nome != '.' && $nome != '..') {
                        //verificando o tamanho total do elemento
                        if(zip_entry_filesize($elemento)) {
                                $n = $qtdaudios++; // incrementa a quantidade de audios extraida na consulta SQL

				$arquivo = fopen($camaudio."/".$nome, 'wb');
				$narquivo = $camaudio."/".$nome;
				//escrevendo todo o==  conteudo do elemento para o arquivo que
				//acabamos de criar
				fwrite($arquivo, zip_entry_read($elemento, zip_entry_filesize($elemento)));
				//fechando o arquivo que criamos
				fclose($arquivo);
				if(file_exists($narquivo)) {
				    $count++; // conta a quantidade de arquivos realmente copiados se possui tamanho, entao nao eh uma pasta, logo, devemos criar um arquivo com o mesmo nome
				}
				//$permi = chmod($narquivo, '0755');
				//$insertqtd = "INSERT INTO `MONITORIA_TESTE`.`qtdaudios` (`qtd`) VALUES (NULL);";
				//$eqtd = $_SESSION['query']($insertqtd) or die (mysql_error());
				//voltando para o while

                                continue;
                        }
                        else {
                        }
                    }
                    else {
                    }
            }
        }
	return $count;
}

function & unrar($audiosup, $nome, $camaudio, $idrel) { // função para retirar do zip os audios enviados no upload
        $selparam = "SELECT * FROM param_moni pm INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni WHERE cr.idrel_filtros='$idrel'";
	$eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
        $move = move_uploaded_file($audiosup, $camaudio."/".$nome);
        if($move) {
            $ext = explode(",",$eselparam['tpaudio']);
            $data = date('dmY'); // pega a data atual
            //pegando o caminho completo do arquivo rar
            //abrindo o arquivo rar
            $audios = rar_open($camaudio."/".$nome) or die ('failed to open');
            $rarlist = rar_list($audios);
            $count = 0;
            //lendo todo o conteudo do arquivo rar
            foreach($rarlist as $list) {
                if($list->extract($camaudio)) {
                    $count++;
                }
            }
            rar_close($audios);
            unlink($camaudio."/".$nome);
            return $count;
        }
        else {
            return -1;
        }
}

function importa($tipoimp,$tipo,$idup, $idrel, $caminho, $caminhoarq, $dataini, $datafim, $acao,$tiposerver,$ftp) {
    $data = date('dmY');
    $hora = date('His');
    $iduporigem = $idup;
    $arquivo = pathinfo($caminhoarq);

    $dadosup = "SELECT * FROM upload u
                INNER JOIN conf_rel cr ON cr.idrel_filtros = u.idrel_filtros
                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
                LEFT JOIN fila_grava fg ON fg.idupload = u.idupload
                WHERE u.idupload='$idup' GROUP BY u.idupload";
    $edadosup = $_SESSION['fetch_array']($_SESSION['query']($dadosup)) or die ("erro na query de consulta do upload");
    $tabfila = $edadosup['tabfila'];
    $ext = explode(",",$edadosup['tpaudio']);
    if($edadosup['idfila_grava'] != "") {
        $msgimp = "O UPLOAD ''$idup'' não pode ser importado, pois já consta com fila Gerada no Servidor, favor contatar o Administrador";
        header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp);
    }
    else {

        if($tipo == "A") {
            $confere = $edadosup['conf'];
            $tabfila = $edadosup['tabfila'];
            $listarel = scandir($caminho);
            foreach($listarel as $audiorel) {
                if($audiorel != "." && $audiorel != ".." && pathinfo($audiorel,PATHINFO_EXTENSION) != "csv" && pathinfo($audiorel,PATHINFO_EXTENSION) != "xls" && pathinfo($audiorel,PATHINFO_EXTENSION) != "txt") {
                    $extarq = strtolower(pathinfo($audiorel,PATHINFO_EXTENSION));
                    if(in_array($extarq,$ext)) {
                        if($tipoimp == "R") {
                            $rels[0] = $idrel;
                            $linhasrel[$idrel][] = $audiorel;
                        }
                        else {
                            $quebrarel = explode("#",$audiorel);
                            if(is_numeric($quebrarel[0])) {
                                if(!in_array($quebrarel[0], $rels)) {
                                    $rels[] = $quebrarel[0];
                                    $linhasrel[$quebrarel[0]][] = $audiorel;
                                }
                                else {
                                    $linhasrel[$quebrarel[0]][] = $audiorel;
                                }
                            }
                            else {
                                $linhasfora[] = $audiorel;
                            }
                        }
                    }
                    else {
                        $audiosfora[] = $audiorel;
                    }
                }
            }
        }
        if($tipo == "LA" OR $tipo == "I") {
            if($arquivo['extension'] == "xls") {
                $dados = new Spreadsheet_Excel_Reader($caminhoarq);
                $ncol = $dados->sheets[0]['cells'][1];
                foreach($ncol as $n) {
                    $colunas[] = strtolower(trim($n));
                }
                $linhas = $dados->sheets[0]['cells'];
            }
            else {
                $linhas = file($caminhoarq, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); // LÃƒÂª o arquivo linha por linha e ignorando linhas vazias
                $ncol = explode(";", $linhas[0]);
                foreach($ncol as $n) {
                    $colunas[] = strtolower(trim($n)); // transforma a primeira linha( ou descriÃƒÂ§ÃƒÂ£o das colunas) em array
                }
            }
            if(in_array("idrel_filtros",$colunas) && $tipoimp === "U") {
                $colrel = true;
            }
            else {
                if($tipoimp == "R") {
                    $colrel = false;
                }
                else {
                    $colrel = true;
                }
            }
            $keyrel = array_search("idrel_filtros", $colunas);
            foreach($linhas as $krel => $drel) {
                if(($krel != 0 && pathinfo($caminhoarq, PATHINFO_EXTENSION) != "xls") OR (pathinfo($caminhoarq, PATHINFO_EXTENSION) == "xls" && $krel != 1)) {

                    if(pathinfo($caminhoarq, PATHINFO_EXTENSION) == "xls") {
                        $lcolunas = array();
                        foreach($colunas as $pk => $col) {
                            if($drel[$pk + 1] == "") {
                                $lcolunas[] = "";
                            }
                            else {
                               $lcolunas[] = $drel[$pk + 1];
                            }
                        }
                    }
                    else {
                         $lcolunas = explode(";", $drel); // transforma em array linha atual do loop
                    }
                    if($tipoimp == "R") {
                        $rels[0] = $idrel;
                        $linhasrel[$idrel][] = $drel;
                    }
                    else {
                        if(!in_array($lcolunas[$keyrel],$rels)) {
                            if(is_numeric($lcolunas[$keyrel])) {
                                $rels[] = $lcolunas[$keyrel];
                                $linhasrel[$lcolunas[$keyrel]][] = $drel;
                            }
                            else {
                                $linhasfora[] = $drel;
                            }
                        }
                        else {
                            if(is_numeric($lcolunas[$keyrel])) {
                                $linhasrel[$lcolunas[$keyrel]][] = $drel;
                            }
                            else {
                                $linhasfora[] = $drel;
                            }
                        }
                    }
                }
            }
            if($tipo == "LA") {
                $scan = scandir($caminho);
                foreach($scan as $arq) {
                    if($arq != "." && $arq != ".." && pathinfo($arq,PATHINFO_EXTENSION) != "xls" && pathinfo($arq,PATHINFO_EXTENSION) != "csv" && pathinfo($arq,PATHINFO_EXTENSION) != "txt") {
                        if(in_array(strtolower(pathinfo($arq,PATHINFO_EXTENSION)),$ext)) {
                            $audios[] = $arq;
                        }
                        else {
                            $audiosfora[] = $arq;
                        }
                    }
                }
            }
        }

        if($rels[0] == "") {
            if($tipoimp == "R") {
                $msgimp = "A pasta contendo os audios e arquivos está vazia, ocorreu algum erro ou inconsistencia no envio dos dados durante o UPLOAD. Favor verificar.";
            }
            else {
                $msgimp = "O UPLOAD não possui a Coluna ''idrel_filtros'' obrigatório em UPLOAD UNICO";
            }
            header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
        }
        else {
            if($acao == "v") {
                $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
            }
            if($acao == "i" && $tipoimp == "U") {
                $logunico = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
            }
            if(file_exists($nomelog)) {
                unlink($nomelog);
            }

            $lidostt = 0;
            $importtt = 0;
            foreach($rels as $idrel) {
                $delaudios = array();
                $lidos = 0;
                $linhassql = 0;
                $selconfig = "SELECT incorpora, nomecoluna, co.idcoluna_oper, pm.idparam_moni,tamanho,tipo,cp.posicao,semdados,ccampos,cpartes FROM param_moni pm
                              INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                              INNER JOIN camposparam cp ON cp.idparam_moni = pm.idparam_moni
                              INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper WHERE cr.idrel_filtros='$idrel' AND cp.ativo='S' ORDER BY cp.posicao";
                //$selconfig = "SELECT idparam_moni FROM conf_rel WHERE idrel_filtros='$idrel'"; // seleciona o parametro cadastrado para a configuraÃƒÂ§ÃƒÂ£o da importaÃƒÂ§ÃƒÂ£o
                $econfig = $_SESSION['query']($selconfig) or die (mysql_error());
                $ncolunas = $_SESSION['num_rows']($econfig);
                $camposop = array();  /** cria variavel dos campos do operador @camposoperador */
                $campossup = array(); /** cria variavel dos campos do supervisor @camposupervisor */
                $camposdados = array(); /** cria variavel dos campos da fila @camposfila */
                $colrelop = array(); /** cria o array das @chavedooperador */
                $colrelsup = array(); /** cria o array das @chavedosupervisor */
                $colreldados = array(); /** cria o array da @chavedosdados */
                while($lcampos = $_SESSION['fetch_array']($econfig)) { // passa campo á campo
                    if($lcampos['tipo'] == "TEXT") {
                        $descart[] = $lcampos['nomecoluna'];
                    }
                    $semdados = $lcampos['semdados'];
                    $divpartes = $lcampos['cpartes'];
                    $divcampos = $lcampos['ccampos'];
                    $idparam = $lcampos['idparam_moni'];
                    //$selnomecampos = "SELECT incorpora, nomecoluna FROM coluna_oper WHERE idcoluna_oper='".$lcampos['idcoluna_oper']."'"; // seleciona o nome do campos atual do while
                    //$enomecampos = $_SESSION['fetch_array']($_SESSION['query']($selnomecampos)) or die (mysql_error());
                    if($lcampos['incorpora'] == "SUPER_OPER") { // se o campo for para supervisor inclui no array $campossup
                      $campossup[$lcampos['posicao']] = $lcampos['nomecoluna'];
                      if($tipo == "LA" OR $tipo == "I") {
                          if(in_array($lcampos['nomecoluna'],$colunas)) {
                              $colrelsup[array_search($lcampos['nomecoluna'],$colunas)] = $lcampos['nomecoluna'];
                          }
                      }
                      else {
                        $colrelsup[$lcampos['posicao'] - 1] = $lcampos['nomecoluna'];
                      }
                    }
                    if($lcampos['incorpora'] == "OPERADOR") { // se o campo for para o operador inclui no array $camposop
                      $camposop[$lcampos['posicao']] = $lcampos['nomecoluna'];
                      if($tipo == "LA" OR $tipo == "I") {
                          if(in_array($lcampos['nomecoluna'],$colunas)) {
                              $colrelop[array_search($lcampos['nomecoluna'],$colunas)] = $lcampos['nomecoluna'];
                          }
                      }
                      else {
                          if($ttipoimp == "U") {
                              $colrelop[$lcampos['posicao']] = $lcampos['nomecoluna'];
                          }
                          else {
                            $colrelop[$lcampos['posicao'] - 1] = $lcampos['nomecoluna'];
                          }
                      }
                    }
                    if($lcampos['incorpora'] == "DADOS") { // se o campo for de dados inclui no array $camposdados
                        if($lselcampos['nomecoluna'] == "auditor") {
                            $camposdados[$lcampos['posicao']] = "operador";
                            if($tipo == "LA" OR $tipo == "I") {
                                if(in_array($lcampos['nomecoluna'],$colunas)) {
                                    $colreldados[array_search($lcampos['nomecoluna'],$colunas)] = "operador";
                                }
                            }
                            else {
                                $colreldados[$lcampos['posicao'] - 1] = "operador";
                            }
                        }
                        else {
                            $camposdados[$lcampos['posicao']] = $lcampos['nomecoluna'];
                            if($tipo == "LA" OR $tipo == "I") {
                                if(in_array($lcampos['nomecoluna'],$colunas)) {
                                    $colreldados[array_search($lcampos['nomecoluna'],$colunas)] = $lcampos['nomecoluna'];
                                }
                            }
                            else {
                                $colreldados[$lcampos['posicao'] - 1] = $lcampos['nomecoluna'];
                            }
                        }
                    }
                }
                $totalcols = $camposop + $campossup + $camposdados;

                // para importações por lista de audio "LA" ou Internet "I" é criada a chave das colunas do arquivo
                if($tipo === "LA" OR $tipo === "I") {
                    $colnrel = array(); // cria o array das colunas que não constam nos parametros da importação
                    foreach($colunas as $key => $value) {
                        if($tipoimp == "U") {
                            if($value == "gravacao" || $value == "idrel_filtros") {
                                if($value == "gravacao") {
                                   $colgrava["gravacao"] = $key;
                                }
                            }
                            else {
                                if(!in_array($value, $totalcols)) {
                                    $colnrel[] = $value;
                                }
                            }
                        }
                        if($tipoimp == "R") {
                            if($value != "gravacao") {
                                  if(!in_array($value, $totalcols)) {
                                      $colnrel[] = $value;
                                  }
                            }
                            else {
                                if($value == "gravacao") {
                                   $colgrava["gravacao"] = $key;
                                }
                            }
                        }
                    }
                    $ttcolscheck = $colrelop + $colrelsup + $colreldados;
                }

                if(count($colnrel) >= 1 && ($tipo == "LA" OR $tipo == "I")) { // verifica se alguma coluna nÃƒÂ£o consta em nenhuma das tabelas de operador ou supervisor, se sim nÃƒÂ£o executa a importaÃƒÂ§ÃƒÂ£o
                    $colnrels = implode(",", $colnrel);
                    $msgimp = "A/s coluna/s ''$colnrels'' não está/ão relacionada/s com a/s coluna/s cadastrada/s para esta importação, favor verificar!!";
                    header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
                    break;
                }
                else {
                    if(($tipo == "LA" OR $tipo == "I") && !in_array("gravacao", $colunas)) {
                        $msgimp = "O arquivo de dados não contém a colunas 'gravação'!!";
                        header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
                        break;
                    }
                    else {
                        if(count($totalcols) != count($ttcolscheck) && ($tipo == "LA" OR $tipo == "I")) {
                            $msgimp = "O arquivo não tem as colunas obrigatórias, favor verficar!!";
                            header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
                            break;
                        }
                        else {
                            // cria arquivo de log para importação ou verficação
                            if($acao == "i" && $tipoimp == "U") {
                                if(!array_key_exists($idrel,$idsup)) {
                                    $selparam = "SELECT * FROM conf_rel cr"
                                            . " INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni"
                                            . " WHERE cr.idrel_filtros='$idrel'";
                                    $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
                                    $camaudio = $eselparam['camaudio'];
                                    $selrel = "SELECT * FROM rel_filtros WHERE idrel_filtros='$idrel'";
                                    $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                                    $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel DESC";
                                    $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
                                    while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                                        $selnomedados = "SELECT nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$eselrel['id_'.strtolower($lfiltros['nomefiltro_nomes'])]."'";
                                        $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnomedados)) or die (mysql_error());
                                        $pastas = scandir($camaudio);
                                        if(in_array($eselnome['nomefiltro_dados'], $pastas)) {
                                          $camaudio = "$camaudio/".$eselnome['nomefiltro_dados'];
                                        }
                                        else {
                                          mkdir("$camaudio/".$eselnome['nomefiltro_dados'], 0777);
                                          $camaudio = "$camaudio/".$eselnome['nomefiltro_dados'];
                                        }
                                        $ultimofilt = $lfiltros['nomefiltro_nomes'];
                                    }
                                    $mes = array('01' => 'JANEIRO','02' => 'FEVEREIRO','03' => 'MARCO','04' => 'ABRIL','05' => 'MAIO','06' => 'JUNHO','07' => 'JULHO','08' => 'AGOSTO','09' => 'SETEMBRO','10' => 'OUTUBRO','11' => 'NOVEMBRO','12' => 'DEZEMBRO');
                                    $criaarq = $camaudio."/".$mes[date('m')]."_".date('Y');
                                    mkdir($criaarq, 0777);
                                    $newup = "INSERT INTO upload (tabfila,idrel_filtros,iduser,tabuser,opcaoimp,camupload,idperiodo,semana,importacao,dataup,horaup,imp,dataimp,horaimp,qtdeimp,qtdeerro) VALUE ("
                                            . "'$tabfila',$idrel,".$_SESSION['usuarioID'].",'".$_SESSION['user_tabela']."','R','$camaudio',".$edadosup['idperiodo'].",".$edadosup['semana'].",".$edadosup['importacao'].",'".$edadosup['dataup']."','".$edadosup['horaup']."','S','".date('Y-m-d')."','".date('His')."',0,0)";
                                    $enewup = $_SESSION['query']($newup) or die (mysql_error());
                                    $idup = str_pad($_SESSION['insert_id'](), 6, '0', STR_PAD_LEFT);
                                    $idsup[$idrel] = $idup;
                                    $criaarq = "$criaarq/$idup";
                                    mkdir($criaarq, 0777);
                                    $updateup = "UPDATE upload SET camupload='$criaarq' WHERE idupload='$idup'";
                                    $eupdateup = $_SESSION['query']($updateup) or die (mysql_error());
                                }
                                $nomelog = "$criaarq/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                            }
                            if($acao == "i" && $tipoimp == "R") {
                                $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                            }
                            if($tipo == "A") {
                                if($campossup == "" && $camposdados == "") {
                                    $collog = $camposop;
                                }
                                else {
                                    $collog = $camposop + $campossup + $camposdados;
                                }

                                ksort($collog);

                                if(!file_exists($nomelog)) {
                                    $crialog = fopen($nomelog, 'a+'); // cria o arquivo
                                    if($semdados == "S") {
                                        $linhalog = array("ID. Upload", "ID. Rel", "Nome Relacionamento", "Nome Arquivo", "OBS");
                                    }
                                    else {
                                        $linhalog = array("ID. Upload", "ID. Rel", "Nome Relacionamento", implode(";",$collog),"Nome Arquivo", "OBS");
                                    }
                                    $linhacolw = implode(";", $linhalog); // implode para uma string a primeira linha com o nome das colunas
                                    fwrite($crialog, "$linhacolw\r\n"); // escreve no arquivo a linha acima
                                    chmod($nomelog, 0777);
                                    $monta = array();
                                    foreach($linhasfora as $l) {
                                        $quebra = explode(".",$l);
                                        foreach($quebra as $q) {
                                            if($q != end($quebra)) {
                                                $monta[] = $q;
                                            }
                                        }
                                        fwrite($crialog,"$idup;;;".str_replace("$divcampos",";",implode("",$monta)).";$l;O arquivo não possui o código do RELACIONAMENTO\r\n");
                                    }
                                    $alinha = array();
                                    foreach($audiosfora as $audiof) {
                                        $aquebra = explode(".",$audiof);
                                        foreach($aquebra as $qa) {
                                            if($qa != end($aquebra)) {
                                                $alinha[] = $qa;
                                            }
                                        }
                                        fwrite($crialog,"$idup;$idrel;".nomeapres($idrel).";".str_replace("$divcampos",";",implode("",$alinha)).";$audiof;A extensão do audio não é permitida\r\n");
                                    }
                                    fclose($crialog); // fecha o arquivo
                                }
                                if($tipoimp == "U" && !file_exists($logunico) && $acao == "i") {
                                    $openunico = fopen($logunico, 'a+');
                                    fwrite($openunico,implode(",",$linhalog)."\r\n");
                                    chmod($openunico,0777);
                                    foreach($linhasfora as $l) {
                                       fwrite($openunico,  str_replace($divcampos, ";", "$idup;;;$l;O arquivo não possui o código do RELACIONAMENTO\r\n"));
                                    }
                                    fclose($openunico);
                                }
                            }
                            else {
                                if($campossup == "" && $camposdados == "") {
                                    $collog = $colrelop;
                                }
                                else {
                                    $collog = $colrelop + $colrelsup + $colreldados; // cria o array que contem as colunas do arquivo de log
                                }

                                if(!file_exists($nomelog)) {
                                   $crialog = fopen($nomelog, 'a+'); // cria o arquivo
                                   $crialinha = array();
                                   if($tipo == "I" OR $tipo == "LA") {
                                       $collog[] = "gravacao";
                                       $collog[] = "OBS"; // coloca a coluna de OBS
                                   }
                                   else {
                                       $collog[] = "OBS"; // coloca a coluna de OBS
                                   }
                                   ksort($collog); // organiza as colunas por ordem da chave
                                   $linhacolw = implode(";", $collog); // implode para uma string a primeira linha com o nome das colunas
                                   if($tipoimp == "U") {
                                       fwrite($crialog, "idrel_filtros;$linhacolw\r\n"); // escreve no arquivo a linha acima
                                   }
                                   else {
                                       fwrite($crialog, "$linhacolw\r\n"); // escreve no arquivo a linha acima
                                   }
                                   chmod($nomelog, 0777);
                                   if($acao == "v") {
                                       foreach($linhasfora as $l) {
                                        fwrite($crialog,"$l;A linha do arquivo não contém o código do RELACIONAMENTO\r\n");
                                    }
                                   }
                                   fclose($crialog); // fecha o arquivo
                               }
                               if($tipoimp == "U" && !file_exists($logunico) && $acao == "i") {
                                   $openunico = fopen($logunico, 'a+');
                                   if($tipoimp == "U") {
                                       fwrite($openunico, "idrel_filtros;$linhacolw\r\n"); // escreve no arquivo a linha acima
                                   }
                                   else {
                                       fwrite($openunico, "$linhacolw\r\n"); // escreve no arquivo a linha acima
                                   }
                                   chmod($openunico, 0777);
                                   foreach($linhasfora as $l) {
                                       fwrite($openunico,"$l;A linha do arquivo não contém o código do RELACIONAMENTO\r\n");
                                   }
                                   fclose($openunico); // fecha o arquivo
                               }
                            }

                            $lo = true;
                            foreach($linhasrel[$idrel] as $kl => $linha) { // faz o loop para percorrer as linhas do arquivo
                                $obs1 = array();
                                $obs2 = "";
                                $obsgrav = "";
                                $lidos++;
                                $lidostt++;
                                $imp = 0;


                                if($tipo == "A") {
                                    $manipula3 = array();
                                    if($tiposerver == "FTP") {
                                        $audio = $linha;
                                    }
                                    else {
                                        $audio = mysql_real_escape_string($linha);
                                    }

                                    $manipula2 = end(explode(".",$audio));
                                    $manipula2 = str_replace(".$manipula2","",$audio);
                                    $exte = end(explode(".",$audio));
                                    $separador = $divcampos;
                                    $sparte = $divpartes;
                                    $parte = explode("$sparte",$manipula2);
                                    if(count($parte) == 2) {
                                        $audiofim= $parte[0];
                                    }
                                    else {
                                        $audiofim = $manipula2;
                                    }
                                    $uni = array();
                                    if($parte[1] != "") {
                                        if(!in_array($audiofim.".$exte",$unitmp)) {
                                            $uni[$parte[1]] = $caminho."/".$manipula2.".$exte";
                                            //$unitmp[$parte[1]] = $manipula2.".$exte";
                                            foreach($arvsaudio as $checkpart) {
                                                $partcheck = array();
                                                $manipulapart = end(explode(".",$checkpart));
                                                $manipulapart = str_replace(".$manipulapart","",$checkpart);
                                                if($manipulapart == $manipula2) {
                                                }
                                                else {
                                                    $partcheck = explode("$sparte",$manipulapart);
                                                    if($partcheck[0] == $parte[0]) {
                                                        $uni[$partcheck[1]] = $caminho."/".$manipulapart.".$exte";
                                                        $unitmp[$partcheck[1]] = $audiofim.".$exte";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ksort($uni);

                                    if(in_array($manipula2.".$exte",$unitmp) && $part[1] != "") {
                                    }
                                    else {
                                        if(count($uni) == 1 && $parte[1] != "") {
                                            $obs1[] = "O arquivo está dividido em partes e não possui as demais partes";
                                            $audio = $parte[0].".".$exte;
                                        }
                                        if(count($uni) > 1 && $parte[1] != "") {
                                            if($acao == "v") {
                                                $audio = $parte[0].".".$exte;
                                            }
                                            else {
                                                $audio = $parte[0].".".$exte;
                                                $arquni = $caminho."/".$parte[0].'.'.$exte;
                                                $comand = "cat '".implode("' '",$uni)."' > '$arquni'";
                                                $exeunion = system("$comand",$retval);
                                                //if($exeunion) {
                                                foreach($uni as $udel) {
                                                    if(in_array($udel,$delaudios)) {
                                                    }
                                                    else {
                                                        $delaudios[] = $udel;
                                                    }
                                                }
                                            }
                                        }

                                        if(count($parte) >= 2 && count($uni) < 2) {
                                        }
                                        else {
                                            $retespecial = explode("$separador",$audiofim);
                                            foreach($retespecial as $kdado => $dado) {
                                                if($tipoimp == "U") {
                                                    if($kdado != 0) {
                                                        $manipula3[] = utf8_encode(removeAcentos(addslashes($dado)));
                                                    }
                                                }
                                                else {
                                                    $manipula3[] = utf8_encode(removeAcentos(addslashes($dado)));
                                                }
                                            }
                                            $qtdecampos = count($manipula3);
                                            if($_SESSION['selbanco'] == "monitoria_itau") {
                                              if(count($collog) == $qtdecampos) {
                                                  $qtdecampos = $qtdecampos;
                                              }
                                              else {
                                                  $auditor = $manipula3[$qtdecampos - 1];
                                              }
                                            }
                                        }
                                    }

                                    if($tipoimp == "U") { // verifica se existe o id da operacao na primeira posicao
                                        $checkrel = explode("#",$linha);
                                        if(is_numeric($checkrel[0])) {
                                            $selrel = "SELECT count(*) as r FROM rel_filtros WHERE idrel_filtros='$checkrel[0]'";
                                            $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                                            if($eselrel['r'] >= 1) {
                                                $lo == true;
                                            }
                                            else {
                                                $obs1[] = "O código da operação é inválido";
                                                $lo = false;
                                            }
                                        }
                                        else {
                                            $obs1[] = "O código da operação é inválido";
                                            $lo = false;
                                        }
                                    }
                                    else {
                                        $lo = true;
                                    }

                                    if($lo ==  true) {

                                        //vincula valores da nomenclatura do audio aos dados necessários
                                        ksort($colrelop);
                                        ksort($colrelsup);
                                        ksort($colreldados);
                                        $valop = array_intersect_key($manipula3, $colrelop);
                                        $valsup = array_intersect_key($manipula3, $colrelsup);
                                        $valdados = array_intersect_key($manipula3, $colreldados);
                                        $dadosop = array_combine($colrelop, $valop);
                                        $dadossup = array_combine($colrelsup, $valsup);
                                        $dadosdados = array_combine($colreldados, $valdados);

                                        if($qtdecampos < count($collog) OR (($qtdecampos - count($collog)) >= 2 && $_SESSION['selbanco'] == "monitoria_itau") OR ($qtdecampos > count($collog) && $_SESSION['selbanco'] != "monitoria_itau")) {
                                            $manipula3[] = "";
                                            $obs1[] = "O nome do arquivo não possui a quantidade de campos necessários para importação!!!";
                                        }
                                        else {
                                            $selaud = "SELECT obriga_aud FROM conf_rel WHERE idrel_filtros='$idrel'";
                                            $eselaud = $_SESSION['fetch_array']($_SESSION['query']($selaud)) or die ("erro na query de consulta da obrigação da Auditoria");
                                            if($eselaud['obriga_aud'] == "S") {
                                                if((count($collog) + 1) != count($manipula3)) {
                                                    $erroaud = 1;
                                                }
                                                else {
                                                    $erroaud = 0;
                                                }
                                            }
                                            else {
                                                $erroaud = 0;
                                            }
                                            if($erroaud == 1) {
                                                $obs1[] = "A gravação não possui o Auditor nas colunas!!!";
                                            }
                                            else {
                                                $verifaudio = "SELECT idupload,COUNT(*) as result FROM fila_grava fg
                                                            WHERE fg.narquivo='".implode($separador,$manipula3)."'";
                                                $everifaudi = $_SESSION['fetch_array']($_SESSION['query']($verifaudio)) or die ("erro na query de consulta do audio no sistema");
                                                if($everifaudi['result'] >= 1) {
                                                $obsgrav = "Audio já importado para o sistema no upload ''".$everifaudi['idupload']."''";
                                                }
                                                $checksel = explode(".",implode($separador,$manipula3));
                                                $checksel = $checksel[0];
                                                if($confere == "S") {
                                                    $confselecao = "SELECT count(*) as result FROM selecao WHERE nomearquivo='$checksel' and selecionado='1'";
                                                    $econfselecao = $_SESSION['fetch_array']($_SESSION['query']($confselecao)) or die (mysql_error());
                                                    if($econfselecao['result'] == 0) {
                                                        $obs1[] = "O audio não está presente na seleção";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if($tipo == "LA" OR $tipo == "I") {
                                    if(pathinfo($caminhoarq, PATHINFO_EXTENSION) == "xls") {
                                        $lcolunas = array();
                                        foreach($colunas as $pk => $col) {
                                            if($linha[$pk + 1] == "") {
                                                $lcolunas[] = "";
                                            }
                                            else {
                                               $lcolunas[] = scapeaspas($linha[$pk + 1]);
                                            }
                                        }
                                    }
                                    else {
                                         $lcolunas = explode(";", $linha); // transforma em array linha atual do loop
                                    }

                                    if($tipoimp == "U") { // verifica se existe o id da operacao na primeira posicao
                                        $checkrel = $lcolunas[$keyrel];
                                        if(is_numeric($checkrel)) {
                                            $selrel = "SELECT count(*) as r FROM rel_filtros WHERE idrel_filtros='$checkrel'";
                                            $eselrel = $_SESSION['fetch_array']($_SESSION['query']($selrel)) or die (mysql_error());
                                            if($eselrel['r'] >= 1) {
                                                $lo == true;
                                            }
                                            else {
                                                $obs1[] = "O código da operação não é válido";
                                                $lo = false;
                                            }
                                        }
                                        else {
                                            $obs1[] = "O código da operação não é válido";
                                            $lo = false;
                                        }
                                    }
                                    else {
                                        $lo == true;
                                    }

                                    if($lo == true) {
                                        $dados = array();
                                        $audio = array();
                                        foreach($lcolunas as $kcoluna => $lcoluna) { // faz loop para inserir os dados da coluna na linha atual
                                            $verifcol = $collog[$kcoluna];
                                            $selreplace = "SELECT caracter, COUNT(*) as result FROM coluna_oper WHERE nomecoluna='$verifcol'";
                                            $eselreplace = $_SESSION['fetch_array']($_SESSION['query']($selreplace)) or die (mysql_error());
                                            if($eselreplace['result'] == 0) {
                                                if(!in_array($verifcol, $descart)) {
                                                    if($colgrava["gravacao"] != $kcoluna) {
                                                        $audio[] = utf8_encode(addslashes($lcoluna));
                                                    }
                                                }
                                                $dados[] = utf8_encode(addslashes($lcoluna));
                                            }
                                            else {
                                                if($eselreplace['caracter'] == "") {
                                                    if(!in_array($verifcol, $descart)) {
                                                        if($colgrava["gravacao"] != $kcoluna) {
                                                            $audio[] = utf8_encode(trim(removeAcentos(addslashes($lcoluna))));
                                                        }
                                                    }
                                                    $dados[] = utf8_encode(trim(removeAcentos(addslashes($lcoluna))));
                                                }
                                                else {
                                                    $caracter = str_split($eselreplace['caracter']);
                                                    $newcol = $lcoluna;
                                                    foreach($caracter as $c) {
                                                        if(ereg("$c", $newcol)) {
                                                          $newcol = str_replace("$c", "", $newcol);
                                                        }
                                                    }
                                                    if(!in_array($verifcol, $descart)) {
                                                        if($colgrava["gravacao"] != $kcoluna) {
                                                            $audio[] = scapeaspas(utf8_encode(trim(removeAcentos(addslashes($newcol)))));
                                                        }
                                                    }
                                                    $dados[] = utf8_encode(trim(removeAcentos(addslashes($newcol))));
                                                }
                                            }
                                        }

                                        ksort($colrelop);
                                        ksort($colrelsup);
                                        ksort($colreldados);
                                        $valop = array_intersect_key($dados, $colrelop);
                                        $valsup = array_intersect_key($dados, $colrelsup);
                                        $valdados = array_intersect_key($dados, $colreldados);
                                        $dadosop = array_combine($colrelop, $valop);
                                        $dadossup = array_combine($colrelsup, $valsup);
                                        $dadosdados = array_combine($colreldados, $valdados);
                                        //$verifdata = "SELECT * FROM coluna_oper WHERE tipo='DATE'";
                                        //$everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
                                        if(count($dadosop) &&  $dadosop != false) {
                                            $dadosverif = $dadosop;
                                            if(count($dadossup) && $dadossup != false) {
                                                $dadosverif = $dadosop + $dadossup;
                                                if(count($dadosdados) && $dadosdados != false) {
                                                    $dadosverif = $dadosop + $dadossup + $dadosdados;
                                                }
                                            }
                                            else {
                                                if(count($dadosdados) &&  $dadosdados != false) {
                                                    $dadosverif = $dadosop + $dadosdados;
                                                }
                                            }
                                        }

                                        $checkgrav = scapeaspas($lcolunas[$colgrava["gravacao"]]);
                                        if($tipo == "A") {
                                            if(in_array($linha, $audiosfora)) {
                                                $obsgrav = "O arquivo de gravação não possui extensão liberada para importação no sistema!!!";
                                            }
                                        }
                                        if($tipo == "LA") {
                                            if(in_array($checkgrav, $audios)) { // verifica se o arquivo de audio informado no campo gravaÃƒÂ§ÃƒÂ£o existe, se existir nÃƒÂ£o faz criticas
                                                $obsgrav = "";
                                            }
                                            else { // se o arquivo audio nÃƒÂ£o existir na pasta informada no cadastro dos parametros, coloca a observaÃƒÂ§ÃƒÂ£o abaixo
                                                if(in_array($checkgrav, $audiosfora)) {
                                                    $obsgrav = "O arquivo de gravação não possui extensão liberada para importação no sistema!!!";
                                                }
                                                else {
                                                    $obsgrav = "O arquivo de gravação não foi localizado!!!";
                                                }
                                            }
                                        }
                                        foreach($ext as $vext) {
                                            if($tipo == "I") {
                                                $colcheck = "camaudio";
                                            }
                                            else {
                                                $colcheck = "narquivo";
                                            }
                                            $chgrav = "SELECT fg.idupload, COUNT(*) as result FROM fila_grava fg
                                                      WHERE fg.$colcheck='".str_replace(".".$vext, "",$checkgrav)."'";
                                            $echgrav = $_SESSION['fetch_array']($_SESSION['query']($chgrav)) or die (mysql_error());
                                            if($echgrav['result'] >= 1) {
                                                $obsgrav = "A gravação já foi importada para o sistema no upload ''".$echgrav['idupload']."''";
                                            }
                                            $chgrav = "SELECT fg.idupload, COUNT(*) as result FROM fila_grava fg
                                                      WHERE fg.$colcheck='".str_replace(".".strtoupper($vext), "",$checkgrav)."'";
                                            $echgrav = $_SESSION['fetch_array']($_SESSION['query']($chgrav)) or die (mysql_error());
                                            if($echgrav['result'] >= 1) {
                                                $obsgrav = "A gravação já foi importada para o sistema no upload ''".$echgrav['idupload']."''";
                                            }
                                        }
                                        $checksel = scapeaspas(implode("#",$audio));
                                        if($confere == "S") {
                                            $confselecao = "SELECT count(*) as result FROM selecao WHERE nomearquivo='$checksel' and selecionado='1'";
                                            $econfselecao = $_SESSION['fetch_array']($_SESSION['query']($confselecao)) or die (mysql_error());
                                            if($econfselecao['result'] == 0) {
                                                $obs1[] = "O audio não está presente na seleção";
                                            }
                                        }
                                    }
                                }

                                if($lo == true) {

                                    $obsop = tipodado ($dadosop);
                                    $obssup = tipodado ($dadossup);
                                    $obsdados = tipodado ($dadosdados);
                                    if($obsop != "") {
                                        $obs1[] = $obsop;
                                    }
                                    if($obssup != "") {
                                        $obs1[] = $obssup;
                                    }
                                    if($obsdados != "") {
                                        $obs1[] = $obsdados;
                                    }
                                    if($obsgrav != "") {
                                        $obs1[] = $obsgrav;
                                    }
                                    foreach($obs1 as $ob) {
                                        if($ob == "") {
                                            $obs2 .= "";
                                        }
                                        else {
                                            $obs2 .= $ob.",";
                                        }
                                    }

                                    //verifica inconsistencia dos dados com a base de dados
                                    if($tipo == "A") {
                                        foreach($colverif as $kcol => $lcol) {
                                            $kverif = $lcol;
                                            $selcol = "SELECT incorpora, nomecoluna, unico FROM coluna_oper WHERE nomecoluna='$kverif'";
                                            $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta da coluna para verificaÃƒÂ§ÃƒÂ£o");
                                            if($eselcol['unico'] == "S") {
                                                if($eselcol['incorpora'] == "OPERADOR") {
                                                    $dadosv['op'][$kverif] = $kverif."='$dadosop[$kverif]'";
                                                }
                                                if($eselcol['incorpora'] == "SUPER_OPER") {
                                                    $dadosv['sup'][$kverif] = $kverif."='$dadossup[$kverif]'";
                                                }
                                                if($eselcol['incorpora'] == "DADOS") {
                                                    $dadosv['dados'][$kverif] = $kverif."='$dadosdados[$kverif]'";
                                                }
                                            }
                                        }
                                    }
                                    if($tipo == "LA" OR $tipo == "I") {
                                        foreach($lcolunas as $kcol => $lcol) {
                                            $kverif = $colunas[$kcol];
                                            if($kverif != "gravacao" && $kverif != "idrel_filtros") {
                                                $selcol = "SELECT incorpora, nomecoluna, unico FROM coluna_oper WHERE nomecoluna='$kverif'";
                                                $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta da coluna para verificação");
                                                if($eselcol['unico'] == "S") {
                                                    if($eselcol['incorpora'] == "OPERADOR") {
                                                        $dadosv['op'][$kverif] = $kverif."='$dadosop[$kverif]'";
                                                    }
                                                    if($eselcol['incorpora'] == "SUPER_OPER") {
                                                        $dadosv['sup'][$kverif] = $kverif."='$dadossup[$kverif]'";
                                                    }
                                                    if($eselcol['incorpora'] == "DADOS") {
                                                        $dadosv['dados'][$kverif] = $kverif."='$dadosdados[$kverif]'";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    $compara = $dadosv;
                                    $obscomp = array();
                                    foreach($dadosv as $tpdados => $check) {
                                        $checksql = implode(" OR ",$check);
                                        $cols = array_keys($check);
                                        $ccheck = count($check);
                                        if(count($check) > 1) {
                                            if($tpdados == "op") {
                                                $vop = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM operador WHERE $checksql";
                                                $evop = $_SESSION['fetch_array']($_SESSION['query']($vop)) or die ("erro na query de consulta dos dados do operador");
                                                if($evop['result'] == 0) {
                                                }
                                                else {
                                                    foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                                        if($evop[$kcomp] != $dadosop[$kcomp]) {
                                                            $msgcomp = "Os dados do operador, estão inconsistentes com o banco de dados";
                                                            if(in_array($msgcomp,$obscomp)) {
                                                            }
                                                            else {
                                                              $obscomp[] = $msgcomp;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if($tpdados == "sup") {
                                                $vsup = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM super_oper WHERE $checksql";
                                                $evsup = $_SESSION['fetch_array']($_SESSION['query']($vsup)) or die ("erro na query de consulta dos dados do operador");
                                                if($evsup['result'] == 0) {
                                                }
                                                else {
                                                    foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                                        if($evsup[$kcomp] != $dadossup[$kcomp]) {
                                                            $msgcomp = "Os dados do supervisor, estão inconsistentes com o banco de dados";
                                                            if(in_array($msgcomp,$obscomp)) {
                                                            }
                                                            else {
                                                              $obscomp[] = $msgcomp;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if($tpdados == "dados") {
                                                $vdados = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM $tabfila WHERE $checksql";
                                                $evdados = $_SESSION['fetch_array']($_SESSION['query']($vdados)) or die ("erro na query de consulta dos dados do operador");
                                                if($evop['result'] == "") {
                                                }
                                                else {
                                                    foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                                        if($evdados[$kcomp] != $dadosdados[$kcomp]) {
                                                            $msgcomp = "Os dados da fila, estão inconsistentes com o banco de dados";
                                                            if(in_array($msgcomp,$obscomp)) {
                                                            }
                                                            else {
                                                              $obscomp[] = $msgcomp;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // termino comparação de dados
                                }
                                else {
                                    foreach($obs1 as $ob) {
                                        if($ob == "") {
                                            $obs2 .= "";
                                        }
                                        else {
                                            $obs2 .= $ob.",";
                                        }
                                    }
                                }

                                if(count($obs1) == 0 && $obscomp[0] == "") { // se a variÃƒÂ¡vel $obs1 continuar vazia apÃƒÂ³s a verificaÃƒÂ§ÃƒÂ£o do tipo do campos, considera-se que nÃƒÂ£o existe erro nos caracteres informados e a importaÃƒÂ§ÃƒÂ£o serÃƒÂ¡ OK
                                    if($acao == "v") {
                                        $obs2 = "Os dados estao corretos para importação!!!";
                                        $imp = 0;
                                    }
                                    else {
                                      $cgrav++;
                                      $obs2 = "importacao realizada com sucesso!!!";
                                      $imp = 1;
                                    }
                                }
                                else {
                                   $imp = 0;
                                }

                                if($tipo == "A") {
                                    $arraycol = $valop + $valsup + $valdados;
                                    ksort($arraycol);
                                    $relapres = nomeapres($idrel);
                                    if($imp == 0) {
                                        $delaudios[] = $caminho."/".$audio;
                                        unset($auditor);
                                        $linhalog = array("$idup", "$idrel", "$relapres",implode(";",$arraycol), "$audio", $obs2.implode(",",$obscomp));
                                    }
                                    else {
                                        $linhalog = array("$idup", "$idrel", "$relapres",implode(";",$arraycol), "$audio", "$obs2");
                                    }
                                    $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                                    $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                                    fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                                    fclose($crialog); // fecha arquivo de log;
                                    $openunico = fopen($logunico, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                                    fwrite($openunico, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                                    fclose($openunico); // fecha arquivo de log;
                                }
                                if($tipo == "LA" OR $tipo == "I") {
                                    if($tipo == "I") {
                                        $gravacao = scapeaspas($lcolunas[$colgrava['gravacao']]);
                                        $ngravacao = basename($gravacao);
                                    }
                                    if($obs2 == "") {
                                         $linhawrite = implode(";", $lcolunas).";".implode(",",$obscomp); // transforma a linha atual em string
                                    }
                                    else {
                                         $linhawrite = implode(";", $lcolunas).";".$obs2; // transforma a linha atual em string
                                    }
                                    $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                                    fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                                    fclose($crialog); // fecha arquivo de log
                                    $openunico = fopen($logunico, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                                    fwrite($openunico, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                                    fclose($openunico); // fecha arquivo de log;
                                }

                                if($imp == 1) {
                                    if($tipoimp == "U") {
                                        if(!array_key_exists($idrel,$idsup)) {
                                            if($tipo == "A") {
                                                copy($edadosup['camupload']."/$linha", "$criaarq/$linha");
                                                unlink($edadosup['camupload']."/$linha");
                                            }
                                            if($tipo == "I") {
                                                $arqscam = scandir($edadosup['camupload']);
                                                foreach($arqscam as $acam) {
                                                    if($acam != "." && $acam != ".." && !eregi("logimp",$acam) && !eregi("logverif",$acam)) {
                                                        copy($edadosup['camupload']."/$acam","$criaarq/$acam");
                                                    }
                                                }
                                            }
                                            if($tipo == "LA") {
                                                copy($edadosup['camupload']."/".$lcolunas[$colgrava['gravacao']],"$criaarq/".$lcolunas[$colgrava['gravacao']]);
                                                unlink($edadosup['camupload']."/".$lcolunas[$colgrava['gravacao']]);
                                            }
                                        }
                                        else {
                                            $idup = $idsup[$idrel];
                                            if($tipo == "A") {
                                                copy($edadosup['camupload']."/$linha", "$criaarq/$linha");
                                                unlink($edadosup['camupload']."/$linha");
                                            }
                                            if($tipo == "LA") {
                                                copy($edadosup['camupload']."/".$lcolunas[$colgrava['gravacao']],"$criaarq/".$lcolunas[$colgrava['gravacao']]);
                                                unlink($edadosup['camupload']."/".$lcolunas[$colgrava['gravacao']]);
                                            }
                                        }
                                    }
                                    $verifdata = "SELECT * FROM coluna_oper
                                                 INNER JOIN camposparam ON camposparam.idcoluna_oper = coluna_oper.idcoluna_oper
                                                 INNER JOIN param_moni ON param_moni.idparam_moni = camposparam.idparam_moni
                                                 WHERE param_moni.idparam_moni='$idparam' AND coluna_oper.tipo='DATE' AND camposparam.ativo='S'";
                                    $everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
                                    while($lverifdata = $_SESSION['fetch_array']($everifdata)) {
                                       if($lverifdata['incorpora'] == "OPERADOR") {
                                         foreach($dadosop as $key => $dadoop) {
                                           if($key == $lverifdata['nomecoluna']) {
                                             $arrayalt = array($key => dataimp($dadoop));
                                             $dadosop = array_replace($dadosop, $arrayalt);
                                           }
                                         }
                                       }
                                       if($lverifdata['incorpora'] == "SUPER_OPER") {
                                           foreach($dadossup as $key => $dadosup) {
                                               if($key == $lverifdata['nomecoluna']) {
                                                 $arrayalt = array($key => dataimp($dadosup));
                                                 $dadossup = array_replace($dadossup, $arrayalt);
                                               }
                                           }
                                       }
                                       if($lverifdata['incorpora'] == "DADOS") {
                                         foreach($dadosdados as $key => $dado) {
                                           if($key == $lverifdata['nomecoluna']) {
                                             $arrayalt = array($key => dataimp($dado));
                                             $dadosdados = array_replace($dadosdados, $arrayalt);
                                           }
                                         }
                                       }
                                   }

                                   if($dadossup['super_oper'] != "") { // se os dados do supervisor constarem nas colunas relacionada ÃƒÂ  importaÃƒÂ§ÃƒÂ£o executa as linha abaixo
                                       $sqlcolsup = implode(",", $colrelsup); // cria a string com o nome das colunas do supervisor
                                       $sqldadossup = implode("', '",$dadossup); // cria a string com os dados do supervisor
                                       $combinesup = array_combine($colrelsup, $dadossup); // combina as duas arrays acima para criar um array onde as colunas $colrelsup sÃƒÂ£o chaves e $dadossup sÃƒÂ£o os dados
                                       $verifsup = array(); // cria array de verificaÃƒÂ§ÃƒÂ£o dos dados do supervisor
                                       $colsuper = "SHOW COLUMNS FROM super_oper"; // lista as colunas presentas na tabela super_oper para verificar quais campos sÃƒÂ£o UNICOS, ou seja, precisam ser verificados
                                       $ecolsuper = $_SESSION['query']($colsuper);
                                       $colunassup = array();
                                       while($lcolsuper = $_SESSION['fetch_array']($ecolsuper)) { // faz o loop nas linha encontradas no select
                                           if($lcolsuper['Key'] == "UNI") { // se a coluna listada no select for UNI acrescenta o nome da coluna no array das colunas ÃƒÂ  serem verificadas
                                               $colunassup[] = $lcolsuper['Field'];
                                           }
                                       }
                                       $chavesup = array_intersect($colunassup, $colrelsup); // cria um array com as colunas que constam no arquivo lido e sÃƒÂ£o unicas na tabela do supervisor
                                       $valuesup = array(); // cria array dos valores que sÃƒÂ£o unicos
                                       foreach($chavesup as $chave) { // faz um loop das cchaves identificadas para obter da array de dados da linha quais dados se relacionam com a chave unica
                                           if(array_key_exists($chave, $combinesup)) { // verifica se a chave exists no dados supervisor
                                               $valuesup[] = $combinesup[$chave];
                                           }
                                       }
                                       $combsup = array_combine($chavesup, $valuesup); // cria um array de combinaÃƒÂ§ÃƒÂ£o das chaves com os dados unicos para servir de contagem no loop que irÃƒÂ¡ cria a string do SQL
                                       foreach($combsup as $kcol => $vcol) {
                                            $verifsup[] = $kcol."='$vcol'";
                                       }
                                       $sqlverifsup = implode(" OR ",$verifsup); // transforma a SQL em string para colocar na Query
                                       if($sqlverifsup == "") {
                                       }
                                       else {
                                           $sqlsuper = utf8_encode("SELECT COUNT(*) as result FROM super_oper WHERE $sqlverifsup"); // verifica se os dados unicos informados jÃƒÂ¡ estÃƒÂ£o em sistema
                                           $esqlsuper = $_SESSION['fetch_array']($_SESSION['query']($sqlsuper)) or die (mysql_error());
                                       }
                                       if($esqlsuper['result'] >= 1) { // se os dados jÃƒÂ¡ estiverem em sistema, entÃƒÂ£o nÃƒÂ£o ÃƒÂ© necessÃƒÂ¡rio inserir os dados novamente no banco, sÃƒÂ³ obter o id do supervisor
                                         $selidsuper = utf8_encode("SELECT * FROM super_oper WHERE $sqlverifsup");
                                         $eidsuper = $_SESSION['fetch_array']($_SESSION['query']($selidsuper)) or die (mysql_error());
                                         $idsuper = $eidsuper['idsuper_oper'];
                                       }
                                       else {
                                         $insertsuper = utf8_encode("INSERT INTO super_oper ($sqlcolsup, dtbase, horabase) VALUE ('$sqldadossup', NOW(), NOW())"); // se os dados nÃƒÂ£o existirem na tabela em questÃƒÂ£o, faz inserÃƒÂ§ÃƒÂ£o
                                         $esuper = $_SESSION['query']($insertsuper) or die (mysql_error);
                                         $idsuper = str_pad($_SESSION['insert_id'](), 6, '0', STR_PAD_LEFT);
                                       }
                                       $colrelop[] = "idsuper_oper";
                                       $dadosop['idsuper_oper'] = $idsuper;
                                       $sqlcolop = implode(",", $colrelop);
                                       $sqldadosop = implode("', '", $dadosop);
                                   }
                                   else {
                                       $colrelop[] = "idsuper_oper";
                                       $selsuper = "SELECT * FROM super_oper WHERE super_oper='NAO INFORMADO'";
                                       $eselsuper = $_SESSION['fetch_array']($_SESSION['query']($selsuper)) or die ("erro na query de consulta do supervisor nulo");
                                       $idsuper = $eselsuper['idsuper_oper'];
                                       $dadosop[] = $eselsuper['idsuper_oper'];
                                       $sqlcolop = implode(",", $colrelop);
                                       $sqldadosop = implode("', '", $dadosop);
                                   }

                                   // verficar os dados do operador
                                   $combineop = array_combine($colrelop, $dadosop);
                                   $verifop = array();
                                   $colop = "SHOW COLUMNS FROM operador";
                                   $ecolop = $_SESSION['query']($colop) or die (mysql_error());
                                   $colunasop = array();
                                   while($lcolop = $_SESSION['fetch_array']($ecolop)) {
                                     if($lcolop['Key'] == "UNI") {
                                         $colunasop[] = $lcolop['Field'];
                                     }
                                   }
                                   $chaveop = array_intersect($colunasop, $colrelop);
                                   $valueop = array();
                                   foreach($chaveop as $chave) {
                                     if(array_key_exists($chave, $combineop)) {
                                         $valueop[] = $combineop[$chave];
                                     }
                                   }
                                   $combop = array_combine($chaveop, $valueop);
                                   $countchave = count($chaveop);
                                   foreach($combop as $kcolop => $vcolop) {
                                       $verifop[] = "$kcolop='$vcolop'";
                                   }
                                   $sqlverifop = implode(" OR ",$verifop);

                                   if(count($dadosop) == 1) {
                                       $selop = "SELECT idoperador FROM operador WHERE operador='NAO LOCALIZADO'";
                                       $eselop = $_SESSION['fetch_array']($_SESSION['query']($selop)) or die ("erro na procura do operador NAO LOCALIZADO");
                                       $idop = $eselop['idoperador'];
                                   }
                                   else {
                                       if($sqlverifop == "") {
                                       }
                                       else {
                                           $seloper = utf8_encode("SELECT COUNT(*) as result FROM operador WHERE $sqlverifop");
                                           $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die (mysql_error());
                                           if($eseloper['result'] >= 1) {
                                               $selidop = utf8_encode("SELECT * FROM operador WHERE $sqlverifop");
                                               $eselidop = $_SESSION['fetch_array']($_SESSION['query']($selidop)) or die (mysql_error());
                                               $idop = $eselidop['idoperador'];
                                               foreach($combineop as $colop => $dop) {
                                                   $dadossupop[] = "$colop='$dop'";
                                               }
                                               $update = utf8_encode("UPDATE operador SET ".implode(", ",$dadossupop)." WHERE idoperador='".$idop."'");
                                               $eup = $_SESSION['query']($update) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o do cadastro do operador");
                                           }
                                           else {
                                               $insertoper = utf8_encode("INSERT INTO operador ($sqlcolop, dtbase, horabase,senha) VALUE ('$sqldadosop', NOW(), NOW(),'')");
                                               $eoper = $_SESSION['query']($insertoper) or die (mysql_error());
                                               $idop = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                                           }
                                       }
                                   }


                                   if($tipo == "A") {
                                       if($tipoimp == "U") {
                                           $camaudio = $criaarq;
                                       }
                                       else {
                                          $camaudio = $caminho;
                                       }
                                       $extarq = explode(".",$audio);
                                       $extaudio = end($extarq);
                                       $newname = $idup."_".$idrel."_".$idop."_".$idsuper."_".date('dmY')."_".date('His')."_".$cgrav.".".$extaudio;
                                       if($tiposerver == "FTP") {
                                           $mod = ftp_chmod($ftp, 0777, $camaudio."/".$audio);
                                           $rename = ftp_rename($ftp, $camaudio."/".$audio, $camaudio."/".$newname);
                                       }
                                       else {
                                           chmod($camaudio."/".trim($audio),0777);
                                           rename($camaudio."/".trim($audio), $camaudio."/".$newname);
                                       }
                                       $dadosdados['camaudio'] = trim($camaudio);
                                       $newnaudio = $audio;
                                       foreach($ext as $vext) {
                                           $newnaudio = str_replace(".".$vext, "", $newnaudio);
                                           $extup = strtoupper($vext);
                                           $newnaudio = str_replace(".".$extup, "", $newnaudio);
                                       }
                                       $dadosdados['narquivo'] = trim($newnaudio);
                                       $dadosdados['arquivo'] = trim($newname);
                                   }
                                   if($tipo == "LA") {
                                       if($tipoimp == "U") {
                                          $camaudio = $criaarq;
                                       }
                                       else {
                                          $camaudio = $caminho;
                                       }
                                        $partext = explode(".",$checkgrav);
                                        $extarq = end($partext);
                                        $newname = $idup."_".$idrel."_".$idop."_".$idsuper."_".date('dmY')."_".date('His')."_".$cgrav.".".$extarq;
                                        if($tiposerver == "FTP") {
                                            $mod = ftp_chmod($ftp, 0777, $camaudio."/".$checkgrav);
                                            $rename = ftp_rename($ftp, $camaudio."/".$checkgrav, $camaudio."/".$newname);
                                        }
                                        else {
                                            $rename = rename($camaudio."/".$checkgrav, $camaudio."/".$newname);
                                        }
                                        foreach($ext as $e) {
                                            $checkgrav = str_replace(".".$e, "", $checkgrav);
                                            $extup = strtoupper($e);
                                            $checkgrav = str_replace(".".$extup, "", $checkgrav);
                                        }
                                        $dadosdados['narquivo'] = trim($checkgrav);
                                        $dadosdados['arquivo'] = trim($newname);
                                        $dadosdados['camaudio'] = $camaudio;

                                   }

                                   $sqlcoldados = implode(",", array_keys($dadosdados));
                                   $sqldados = implode("', '", $dadosdados);

                                   if($tipo == "I" OR $tipo == "LA" OR $tipo == "A") {
                                       $verifhist = "SELECT COUNT(*) as result FROM fila_grava fo
                                                     WHERE fo.idupload='$idup' AND fo.idoperador='$idop' AND fo.idsuper_oper='$idsuper' AND fo.idrel_filtros='$idrel'";
                                   }
                                   else {
                                       $verifhist = "SELECT COUNT(*) as result FROM fila_oper fo
                                                     WHERE fo.idupload='$idup' AND fo.idoperador='$idop' AND fo.idsuper_oper='$idsuper' AND fo.idrel_filtros='$idrel'";
                                   }
                                   $everifhist = $_SESSION['fetch_array']($_SESSION['query']($verifhist)) or die (mysql_error());
                                   if($sqlcoldados == "") {
                                       if($tipo == "I" OR $tipo == "LA" OR $tipo == "A") {
                                           if($tipo == "A" OR $tipo == "LA") {
                                               if($auditor == "") {
                                                   $historico = "INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros,monitorando, monitorado) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0)";
                                               }
                                               else {
                                                   $verifaud = "SELECT idoperador, COUNT(*) as result FROM operador WHERE operador='$auditor'";
                                                   $everifaud = $_SESSION['fetch_array']($_SESSION['query']($verifaud));
                                                   if($everifaud['result'] >= 1) {
                                                       $idauditor = $everifaud['idoperador'];
                                                   }
                                                   else {
                                                       $insertaud = "INSERT INTO operador (operador,dtbase,horabase,senha) VALUES ('$auditor',NOW(),NOW(),'')";
                                                       $exeaud = $_SESSION['query']($insertaud) or die (mysql_error());
                                                       $idauditor = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                                                   }
                                                   $historico = "INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idauditor,idrel_filtros,monitorando, monitorado) VALUE ('$idup', '$idop', '$idsuper', '$idauditor','$idrel',0,0)";
                                               }
                                           }
                                           else {
                                               $historico = "INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado,camaudio,narquivo,arquivo) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0,'$gravacao','$ngravacao','$gravacao')";
                                           }
                                       }
                                       else {
                                           $historico = "INSERT INTO fila_oper (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0)";
                                       }
                                    }
                                    if($sqlcoldados != "") {
                                       if($tipo == "I" OR $tipo == "LA" OR $tipo == "A") {
                                           if($tipo == "A" OR $tipo == "LA") {
                                               if($auditor == "") {
                                                   $historico = "INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros,monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0, '$sqldados')";
                                               }
                                               else {
                                                   $verifaud = "SELECT idoperador, COUNT(*) as result FROM operador WHERE operador='$auditor'";
                                                   $everifaud = $_SESSION['fetch_array']($_SESSION['query']($verifaud));
                                                   if($everifaud['result'] >= 1) {
                                                       $idauditor = $everifaud['idoperador'];
                                                   }
                                                   else {
                                                       $insertaud = "INSERT INTO operador (operador,dtbase,horabase,senha) VALUES ('$auditor',NOW(),NOW(),'')";
                                                       $exeaud = $_SESSION['query']($insertaud) or die (mysql_error());
                                                       $idauditor = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                                                   }
                                                   $historico = "INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idauditor,idrel_filtros,monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idauditor','$idrel',0,0, '$sqldados')";
                                               }
                                           }
                                           else {
                                               $historico = "INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado, camaudio,narquivo,arquivo, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0,'$gravacao','$ngravacao','$gravacao','$sqldados')";
                                           }
                                       }
                                       else {
                                           $historico = "INSERT INTO fila_oper (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0, '$sqldados')";
                                       }
                                   }
                                   $ehist = $_SESSION['query']($historico) or die (mysql_error());
                                   unset($auditor);
                                   array_pop($colrelop);
                                   if($imp == 1) {
                                       $linhassql++;
                                       $importtt++;
                                   }
                                   else {
                                       $linhassql = $linhassql;
                                   }

                                   if($delaudios != "") {
                                       foreach($delaudios as $del) {
                                           if($tiposerver == "FTP") {
                                               ftp_rmdir($ftp, $del);
                                           }
                                           else {
                                               $delaudio = unlink($del);
                                           }
                                       }
                                   }
                                }
                            }

                            if($acao == "i") {
                                $dataimp = date('Y-m-d');
                                $horaimp = date('H:i:s');
                                $erro = $lidos - $linhassql;
                                if($erro != $lidos) {
                                  $atuupload = "UPDATE upload SET imp='S', dataimp='$dataimp', horaimp='$horaimp', qtdeimp='$linhassql', qtdeerro='$erro' WHERE idupload='$idup'";
                                  $eatupup = $_SESSION['query']($atuupload) or die (mysql_error());
                                }


                                /*envia e-mail*/
                                $confemail = "SELECT *,count(*) as r FROM relemailimp WHERE idrel_filtros='$idrel'";
                                $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail)) or die ("erro na query de consulta da configuraÃƒÂ§ÃƒÂ£o de email relacionada");
                                if($econfemail['r'] >= 1) {
                                    $envmail = new ConfigMail();
                                    $envmail->IsSMTP();
                                    $envmail->IsHTML(true);
                                    $envmail->SMTPAuth = true;
                                    $envmail->idconf = $econfemail['idconfemailenv'];
                                    $envmail->iduser = $_SESSION['usuarioID'];
                                    $envmail->tipouser = $_SESSION['user_tabela'];
                                    $envmail->upload = $idup;
                                    $envmail->nomeuser = $_SESSION['usuarioNome'];
                                    $envmail->qtdeimp = $linhassql;
                                    $envmail->qtdelido = $lidos;
                                    $envmail->tipoacao = "importacao";
                                    $envmail->config();
                                    $emailsadm = explode(",",$econfemail['usersadm']);
                                    $emailsweb = explode(",",$econfemail['usersweb']);
                                    foreach($emailsadm as $adm) {
                                        if($adm != "") {
                                            $emails['user_adm'][] = $adm;
                                        }
                                    }
                                    foreach($emailsweb as $web) {
                                        if($web != "") {
                                            $emails['user_web'][] = $web;
                                        }
                                    }
                                    foreach($emails as $kemail => $m) {
                                        foreach($m as $km => $e) {
                                            if($e != "") {
                                                $selusers = "SELECT nome$kemail, email FROM $kemail WHERE id$kemail='$e'";
                                                $eselusers = $_SESSION['fetch_array']($_SESSION['query']($selusers)) or die ("erro na query para consultar nome do usuÃƒÂ¡rio");
                                                $email = $eselusers['email'];
                                                $user = $eselusers['nome'.$m];
                                                $envmail->AddAddress("$email","$user");
                                                $arqlog = explode("/",$nomelog);
                                                $arqlog = end($arqlog);
                                                $envmail->AddAttachment("$nomelog");
                                                if($envmail->Send()) {
                                                }
                                                else {
                                                    $erroemail = ". Ocorreu um erro no envio de e-mail";
                                                }
                                                $envmail->ClearAddresses();
                                            }
                                        }
                                    }

                                    if($tipo == "A") {
                                        $comaudios = 0;
                                        if($tiposerver == "FTP") {
                                            $selpasta = ftp_chdir($ftp, $edadosup['camupload']);
                                            $copy = ftp_put($ftp, $nlog, $ctemp, FTP_BINARY);
                                            $checkpasta = ftp_nlist($ftp, $edadosup['camupload']);
                                            $dellog = unlink($caminho."/".$arqlog);
                                        }
                                        else {
                                            $checkpasta = scandir($caminho);
                                        }
                                        foreach($checkpasta as $checkaudio) {
                                            $ckaudio = end(explode("/",$checkaudio));
                                            if($ckaudio != "." && $ckaudio != "..") {
                                                if(eregi(".txt",$ckaudio) OR eregi(".csv",$ckaudio)) {
                                                }
                                                else {
                                                    $comaudios++;
                                                }
                                            }
                                        }
                                        if($comaudios == 0) {
                                            if($tiposerver != "FTP") {
                                                unlink($nomelog);
                                                rmdir($caminho);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if($tipoimp == "U" && $acao == "i") {
                $upunico = "UPDATE upload set imp='S',dataimp='".date('Y-m-d')."',horaimp='".date('His')."',qtdeimp=$importtt,qtdeerro=".($lidostt - $importtt)." WHERE idupload='$iduporigem'";
                $eupunico = $_SESSION['query']($upunico) or die (mysql_error());

            }
            if($importtt > 0) {
                $msgimp = "Importação realizada com sucesso e ''$importtt'' registro/s inserido/s ou ataulizado/s com sucesso!!!";
            }
            else {
                if($acao == "v") {
                    if($msgimp == "") {
                        $msgimp = "Verificação realizada com sucesso!!!";
                    }
                }
                else {
                    $msgimp = "Nenhum registro foi importado pois todos os dados estavam inconsistentes, favor realizar a verificação antes da importação!!!";
                }
            }
            header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=$relapres&msgimp=$msgimp$erroemail");
        }
    }
}

function importa_arq ($tipoimp,$tipo,$idup, $idrel, $caminho, $caminhoarq, $relapres, $dataini, $datafim, $acao,$tiposerver,$ftp) { // funÃƒÂ§ÃƒÂ£o para importaÃƒÂ§ÃƒÂ£o de dados
  $data = date('d.m.Y');
  $hora = date('H:i:s');
  $arquivo = pathinfo($caminhoarq);
  if($arquivo['extension'] == "xls") {
      $dados = new Spreadsheet_Excel_Reader($caminhoarq);
      $ncol = $dados->sheets[0]['cells'][1];
      foreach($ncol as $n) {
          $colunas[] = strtolower(trim($n));
      }
      $linhas = $dados->sheets[0]['cells'];
  }
  else {
  $linhas = file($caminhoarq, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); // LÃƒÂª o arquivo linha por linha e ignorando linhas vazias
    $ncol = explode(";", $linhas[0]);
    foreach($ncol as $n) {
        $colunas[] = strtolower(trim($n)); // transforma a primeira linha( ou descriÃƒÂ§ÃƒÂ£o das colunas) em array
    }
  }
  $dadosup = "SELECT * FROM upload u
              INNER JOIN periodo p ON p.idperiodo = u.idperiodo
              INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
              WHERE u.idupload='$idup'";
  $edadosup = $_SESSION['fetch_array']($_SESSION['query']($dadosup)) or die ("erro na query de consulta do upload");
  $tabfila = $edadosup['tabfila'];
  $selconfig = "SELECT incorpora, nomecoluna, co.idcoluna_oper, pm.idparam_moni,tamanho,tipo,cp.posicao FROM param_moni pm
                INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                INNER JOIN camposparam cp ON cp.idparam_moni = pm.idparam_moni
                INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper WHERE cr.idrel_filtros='$idrel' AND cp.ativo='S' ORDER BY cp.posicao";
  //$selconfig = "SELECT idparam_moni FROM conf_rel WHERE idrel_filtros='$idrel'"; // seleciona o parametro cadastrado para a configuraÃƒÂ§ÃƒÂ£o da importaÃƒÂ§ÃƒÂ£o
  $econfig = $_SESSION['query']($selconfig) or die (mysql_error());
  $ncolunas = $_SESSION['num_rows']($econfig);
    $camposger = array();  // cria variavel dos campos
    $camposop = array();  // cria variavel dos campos do operador
    $campossup = array(); // cria variavel dos campos do supervisor
    $camposdados = array();
    while($lcampos = $_SESSION['fetch_array']($econfig)) { // passa campo ÃƒÂ  campo
      $idparam = $lcampos['idparam_moni'];
      //$selnomecampos = "SELECT incorpora, nomecoluna FROM coluna_oper WHERE idcoluna_oper='".$lcampos['idcoluna_oper']."'"; // seleciona o nome do campos atual do while
      //$enomecampos = $_SESSION['fetch_array']($_SESSION['query']($selnomecampos)) or die (mysql_error());
      if($lcampos['incorpora'] == "SUPER_OPER") { // se o campo for para supervisor inclui no array $campossup
	$campossup[$lcampos['posicao']] = $lcampos['nomecoluna'];
      }
      if($lcampos['incorpora'] == "OPERADOR") { // se o campo for para o operador inclui no array $camposop
	$camposop[$lcampos['posicao']] = $lcampos['nomecoluna'];
      }
      if($lcampos['incorpora'] == "DADOS") { // se o campo for de dados inclui no array $camposdados
	$camposdados[$lcampos['posicao']] = $lcampos['nomecoluna'];
      }
    }
    $colnrel = array(); // cria o array das colunas que nÃƒÂ£o constam nos parametros da importaÃƒÂ§ÃƒÂ£o
    $colrelop = array(); // cria o array das chave do operador
    $colrelsup = array(); // cria o array das chaves do supervisor
    $colreldados = array(); // cria o array da chave do campos dados
    foreach($colunas as $key => $value) {
         if($value != "gravacao") {
               if(in_array($value, $camposop)) {
                   $colrelop[$key] = ereg_replace("[-=+.;:,!@#$%&*)(]","",$value);
               }
               if(in_array($value, $campossup)) {
                   $colrelsup[$key] = ereg_replace("[-=+.;:,!@#$%&*)(]","",$value);
               }
               if(in_array($value, $camposdados)) {
                   $colreldados[$key] = ereg_replace("[-=+.;:,!@#$%&*)(]","",$value);
               }
               if(!in_array($value, $camposop) && !in_array($value, $campossup) && !in_array($value, $camposdados)) {
                   $colnrel[] =  $value;
               }
         }
         else {
              $colgrava["gravacao"] = $key;
         }
    }
    if($tipo == "I") {
         if(!isset($colgrava)) {
              $colnrel[] = "gravacao";
         }
    }
    if($acao == "v") {
        $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
        if(file_exists($nomelog)) {
            unlink($nomelog);
        }
    }
    else {
        $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
    }

    if($campossup == "" && $camposdados == "") {
      $collog = $colrelop;
    }
    else {
      $collog = $colrelop + $campossup + $camposdados; // cria o array que contem as colunas do arquivo de log
    }
    ksort($collog); // organiza as colunas por ordem da chave
    if(count($colnrel) == 1) { // verifica se alguma coluna nÃƒÂ£o consta em nenhuma das tabelas de operador ou supervisor, se sim nÃƒÂ£o executa a importaÃƒÂ§ÃƒÂ£o
	$colnrels = implode(",", $colnrel);
	$msgimp = "A coluna ''$colnrels'' não está relacionada com as colunas cadastradas para esta importação, favor verificar!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    if(count($colnrel) > 1) { // verifica se alguma coluna nÃƒÂ£o consta em nenhuma das tabelas de operador ou supervisor, se sim nÃƒÂ£o executa a importaÃƒÂ§ÃƒÂ£o
	$colnrels = implode(",", $colnrel);
	$msgimp = "As colunas ''$colnrels'' não estão relacionadas com as colunas cadastradas para esta importação, favor verificar!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    if((in_array("gravacao",$colunas) && count($colunas) != count($collog) + 1) OR (!in_array("gravacao",$colunas) && count($colunas) != count($collog))) {
        $colnrels = implode(",", $colnrel);
	$msgimp = "A quantidade de colunas informadas no arquivo estão incorreta para o paramentro vinculado ao Relacionamento!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    else { // se nenhuma linha for encontrada em $colnrel segue abaixo
      $linhassql = 0;
      $lidos = 0;
      foreach($linhas as $kl => $linha) { // faz o loop para percorrer as linhas do arquivo
          $obs1 = array();
          $obs2 = "";

          if(($kl != 0 && pathinfo($caminhoarq, PATHINFO_EXTENSION) != "xls") OR (pathinfo($caminhoarq, PATHINFO_EXTENSION) == "xls" && $kl != 1)) {
               $lidos++; // define a quantidade de linhas lidas do arquivo;

               if(pathinfo($caminhoarq, PATHINFO_EXTENSION) == "xls") {
                   $lcolunas = array();
                   foreach($colunas as $pk => $col) {
                       if($linha[$pk + 1] == "") {
                           $lcolunas[] = "";
                       }
                       else {
                          $lcolunas[] = $linha[$pk + 1];
                       }
                   }
               }
               else {
                    $lcolunas = explode(";", $linha); // transforma em array linha atual do loop
               }
               if($kl == 0) {
               }
               else {
                   $dados = array();
                   foreach($lcolunas as $kcoluna => $lcoluna) { // faz loop para inserir os dados da coluna na linha atual
                       $verifcol = $collog[$kcoluna];
                       $selreplace = "SELECT caracter, COUNT(*) as result FROM coluna_oper WHERE nomecoluna='$verifcol'";
                       $eselreplace = $_SESSION['fetch_array']($_SESSION['query']($selreplace)) or die (mysql_error());
                       if($eselreplace['result'] == 0) {
                           $dados[] = $lcoluna;
                       }
                       else {
                           if($eselreplace['caracter'] == "") {
                               $dados[] = trim(removeAcentos($lcoluna));
                           }
                           else {
                               $caracter = str_split($eselreplace['caracter']);
                               $newcol = $lcoluna;
                               foreach($caracter as $c) {
                                   if(ereg("$c", $newcol)) {
                                     $newcol = str_replace("$c", "", $newcol);
                                   }
                               }
                               $dados[] = trim(removeAcentos($newcol));
                           }
                       }
                   }

                   $valop = array_intersect_key($dados, $colrelop);
                   $dadosop = array_combine($colrelop, $valop);
                   $valsup = array_intersect_key($dados, $colrelsup);
                   $dadossup = array_combine($colrelsup, $valsup);
                   $valdados = array_intersect_key($dados, $colreldados);
                   $dadosdados = array_combine($colreldados, $valdados);
                   //$verifdata = "SELECT * FROM coluna_oper WHERE tipo='DATE'";
                   //$everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
                   if(count($dadosop) >= 1 && $dadosop != false) {
                       $dadosverif = $dadosop;
                       if(count($dadossup) >= 1 && $dadossup!= false) {
                           $dadosverif = $dadosop + $dadossup;
                           if(count($dadosdados) &&  $dadosdados != false) {
                               $dadosverif = $dadosop + $dadossup + $dadosdados;
                           }
                       }
                       else {
                           if(count($dadosdados) && $dadosdados != false) {
                               $dadosverif = $dadosop + $dadosdados;
                           }
                       }
                  }
             }
              if($tipo == "I") {
                   if(!in_array("gravacao",$collog)) {
                        $collog[$colgrava['gravacao']] = "gravacao";
                        $collog[] = "OBS"; // coloca a coluna de OBS
                   }
              }
              else {
                   if(!in_array("OBS",$collog)) {
                         $collog[] = "OBS"; // coloca a coluna de OBS
                   }
              }
               $obsop = tipodado ($dadosop);
               $obssup = tipodado ($dadossup);
               $obsdados = tipodado ($dadosdados);
               if($obsop != "") {
                   $obs1[] = $obsop;
               }
               if($obssup != "") {
                   $obs1[] = $obssup;
               }
               if($obsdados != "") {
                   $obs1[] = $obsdados;
               }
               foreach($obs1 as $ob) {
                   if($ob == "") {
                       $obs2 .= "";
                   }
                   else {
                       $obs2 .= $ob.",";
                   }
               }

               //verifica inconsistencia dos dados com a base de dados
               foreach($lcolunas as $kcol => $lcol) {
                   $kverif = $colunas[$kcol];
                   if($kverif != "gravacao") {
                         $selcol = "SELECT incorpora, nomecoluna, unico FROM coluna_oper WHERE nomecoluna='$kverif'";
                         $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta da coluna para verificação");
                         if($eselcol['unico'] == "S") {
                             if($eselcol['incorpora'] == "OPERADOR") {
                                 $dadosv['op'][$kverif] = $kverif."='$dadosop[$kverif]'";
                             }
                             if($eselcol['incorpora'] == "SUPER_OPER") {
                                 $dadosv['sup'][$kverif] = $kverif."='$dadossup[$kverif]'";
                             }
                             if($eselcol['incorpora'] == "DADOS") {
                                 $dadosv['dados'][$kverif] = $kverif."='$dadosdados[$kverif]'";
                             }
                         }
                   }
               }
               $compara = $dadosv;
               $obscomp = array();
               foreach($dadosv as $tpdados => $check) {
                   $checksql = implode(" OR ",$check);
                   $cols = array_keys($check);
                   $ccheck = count($check);
                   if(count($check) < 1) {
                   }
                   else {
                       if($tpdados == "op") {
                           $vop = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM operador WHERE $checksql";
                           $evop = $_SESSION['fetch_array']($_SESSION['query']($vop)) or die ("erro na query de consulta dos dados do operador");
                           if($evop['result'] == 0) {
                           }
                           else {
                               foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                   if($evop[$kcomp] != $dadosop[$kcomp]) {
                                       $msgcomp = "Os dados do operador, estão inconsistentes com o banco de dados";
                                       if(in_array($msgcomp,$obscomp)) {
                                       }
                                       else {
                                         $obscomp[] = $msgcomp;
                                       }
                                   }
                               }
                           }
                       }
                       if($tpdados == "sup") {
                           $vsup = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM super_oper WHERE $checksql";
                           $evsup = $_SESSION['fetch_array']($_SESSION['query']($vsup)) or die ("erro na query de consulta dos dados do operador");
                           if($evsup['result'] == 0) {
                           }
                           else {
                               foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                   if($evsup[$kcomp] != $dadossup[$kcomp]) {
                                       $msgcomp = "Os dados do supervisor, estão inconsistentes com o banco de dados";
                                       if(in_array($msgcomp,$obscomp)) {
                                       }
                                       else {
                                         $obscomp[] = $msgcomp;
                                       }
                                   }
                               }
                           }
                       }
                       if($tpdados == "dados") {
                           $vdados = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM $tabfila WHERE $checksql";
                           $evdados = $_SESSION['fetch_array']($_SESSION['query']($vdados)) or die ("erro na query de consulta dos dados do operador");
                           if($evop['result'] == "") {
                           }
                           else {
                               foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                   if($evdados[$kcomp] != $dadosdados[$kcomp]) {
                                       $msgcomp = "Os dados da fila, estão inconsistentes com o banco de dados";
                                       if(in_array($msgcomp,$obscomp)) {
                                       }
                                       else {
                                         $obscomp[] = $msgcomp;
                                       }
                                   }
                               }
                           }
                       }
                   }
               }
               // termino comparação de dados

            if(count($obs1) == 0 && $obscomp[0] == "") { // se a variÃƒÂ¡vel $obs1 continuar vazia apÃƒÂ³s a verificaÃƒÂ§ÃƒÂ£o do tipo do campos, considera-se que nÃƒÂ£o existe erro nos caracteres informados e a importaÃƒÂ§ÃƒÂ£o serÃƒÂ¡ OK
                   if($acao == "v") {
                       $obs2 = "Os dados estao corretos para importação!!!";
                       $imp = 0;
                   }
                   else {
                     $obs2 = "importacao realizada com sucesso!!!";
                     $imp = 1;
                   }
            }
            else {
               $imp = 0;
            }

            if($tipo == "I") {
                 $gravacao = $lcolunas[$colgrava['gravacao']];
                 $ngravacao = basename($gravacao);
                 if($obs2 == "") {
                      $linhawrite = implode(";", $dados).";".implode(",",$obscomp); // transforma a linha atual em string
                 }
                 else {
                      $linhawrite = implode(";", $dados).";".$obs2; // transforma a linha atual em string
                 }
            }
            else {
                 if($obs2 == "") {
                    $linhawrite = implode(";", $dados).";".implode(",",$obscomp); // transforma a linha atual em string
                 }
                 else {
                    $linhawrite = implode(";", $dados).";".$obs2; // transforma a linha atual em string
                 }
            }
            $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
            fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
            fclose($crialog); // fecha arquivo de log

            if($imp == 1) { // se a importaÃƒÂ§ÃƒÂ£o ocorrer OK
               $verifdata = "SELECT * FROM coluna_oper
                             INNER JOIN camposparam ON camposparam.idcoluna_oper = coluna_oper.idcoluna_oper
                             INNER JOIN param_moni ON param_moni.idparam_moni = camposparam.idparam_moni
                             WHERE param_moni.idparam_moni='$idparam' AND coluna_oper.tipo='DATE' AND camposparam.ativo='S'";
               $everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
               while($lverifdata = $_SESSION['fetch_array']($everifdata)) {
                 if($lverifdata['incorpora'] == "OPERADOR") {
                   foreach($dadosop as $key => $dadoop) {
                     if($key == $lverifdata['nomecoluna']) {
                       $arrayalt = array($key => dataimp($dadoop));
                       $dadosop = array_replace($dadosop, $arrayalt);
                     }
                   }
                 }
                 if($lverifdata['incorpora'] == "SUPER_OPER") {
                     foreach($dadossup as $key => $dadosup) {
                         if($key == $lverifdata['nomecoluna']) {
                           $arrayalt = array($key => dataimp($dadosup));
                           $dadossup = array_replace($dadossup, $arrayalt);
                         }
                     }
                 }
                 if($lverifdata['incorpora'] == "DADOS") {
                   foreach($dadosdados as $key => $dado) {
                     if($key == $lverifdata['nomecoluna']) {
                       $arrayalt = array($key => dataimp($dado));
                       $dadosdados = array_replace($dadosdados, $arrayalt);
                     }
                   }
                 }
             }

             if(count($dadossup) >= 1 && $dadossup != false) { // se os dados do supervisor constarem nas colunas relacionada ÃƒÂ  importaÃƒÂ§ÃƒÂ£o executa as linha abaixo
                 $sqlcolsup = implode(",", $colrelsup); // cria a string com o nome das colunas do supervisor
                 $sqldadossup = implode("', '",$dadossup); // cria a string com os dados do supervisor
                 $combinesup = array_combine($colrelsup, $dadossup); // combina as duas arrays acima para criar um array onde as colunas $colrelsup sÃƒÂ£o chaves e $dadossup sÃƒÂ£o os dados
                 $verifsup = array(); // cria array de verificaÃƒÂ§ÃƒÂ£o dos dados do supervisor
                 $colsuper = "SHOW COLUMNS FROM super_oper"; // lista as colunas presentas na tabela super_oper para verificar quais campos sÃƒÂ£o UNICOS, ou seja, precisam ser verificados
                 $ecolsuper = $_SESSION['query']($colsuper);
                 $colunassup = array();
                 while($lcolsuper = $_SESSION['fetch_array']($ecolsuper)) { // faz o loop nas linha encontradas no select
                   if($lcolsuper['Key'] == "UNI") { // se a coluna listada no select for UNI acrescenta o nome da coluna no array das colunas ÃƒÂ  serem verificadas
                         $colunassup[] = $lcolsuper['Field'];
                   }
                 }
                 $chavesup = array_intersect($colunassup, $colrelsup); // cria um array com as colunas que constam no arquivo lido e sÃƒÂ£o unicas na tabela do supervisor
                 $valuesup = array(); // cria array dos valores que sÃƒÂ£o unicos
                 foreach($chavesup as $chave) { // faz um loop das cchaves identificadas para obter da array de dados da linha quais dados se relacionam com a chave unica
                     if(array_key_exists($chave, $combinesup)) { // verifica se a chave exists no dados supervisor
                         $valuesup[] = $combinesup[$chave];
                     }
                 }
                 $combsup = array_combine($chavesup, $valuesup); // cria um array de combinaÃƒÂ§ÃƒÂ£o das chaves com os dados unicos para servir de contagem no loop que irÃƒÂ¡ cria a string do SQL
                 $count = count($chavesup); // conta a quantidade de chaves
                 foreach($combsup as $kcol => $vcol) {
                      $verifsup[] = $kcol."='$vcol'";
                 }
                 $sqlverifsup = implode(" OR ",$verifsup); // transforma a SQL em string para colocar na Query
                 if($sqlverifsup == "") {
                 }
                 else {
                     $sqlsuper = "SELECT COUNT(*) as result FROM super_oper WHERE $sqlverifsup"; // verifica se os dados unicos informados jÃƒÂ¡ estÃƒÂ£o em sistema
                     $esqlsuper = $_SESSION['fetch_array']($_SESSION['query']($sqlsuper)) or die (mysql_error());
                 }
                 if($esqlsuper['result'] >= 1) { // se os dados jÃƒÂ¡ estiverem em sistema, entÃƒÂ£o nÃƒÂ£o ÃƒÂ© necessÃƒÂ¡rio inserir os dados novamente no banco, sÃƒÂ³ obter o id do supervisor
                   $selidsuper = "SELECT * FROM super_oper WHERE $sqlverifsup";
                   $eidsuper = $_SESSION['fetch_array']($_SESSION['query']($selidsuper)) or die (mysql_error());
                   $idsuper = $eidsuper['idsuper_oper'];
                 }
                 else {
                   $insertsuper = utf8_encode("INSERT INTO super_oper ($sqlcolsup, dtbase, horabase) VALUE ('$sqldadossup', NOW(), NOW())"); // se os dados nÃƒÂ£o existirem na tabela em questÃƒÂ£o, faz inserÃƒÂ§ÃƒÂ£o
                   $esuper = $_SESSION['query']($insertsuper) or die (mysql_error);
                   $idsuper = str_pad($_SESSION['insert_id'](), 6, '0', STR_PAD_LEFT);
                 }
                 $colrelop[] = "idsuper_oper";
                 $dadosop['idsuper_oper'] = $idsuper;
                 $sqlcolop = implode(",", $colrelop);
                 $sqldadosop = implode("', '", $dadosop);
              }
              else {
                     $colrelop[] = "idsuper_oper";
                     $selsuper = "SELECT * FROM super_oper WHERE super_oper='NAO INFORMADO'";
                     $eselsuper = $_SESSION['fetch_array']($_SESSION['query']($selsuper)) or die ("erro na query de consulta do supervisor nulo");
                     $idsuper = $eselsuper['idsuper_oper'];
                     $dadosop[] = $eselsuper['idsuper_oper'];
                     $sqlcolop = implode(",", $colrelop);
                     $sqldadosop = implode("', '", $dadosop);
              }
              $combineop = array_combine($colrelop, $dadosop);
              $verifop = array();
              $colop = "SHOW COLUMNS FROM operador";
              $ecolop = $_SESSION['query']($colop) or die (mysql_error());
              $colunasop = array();
              while($lcolop = $_SESSION['fetch_array']($ecolop)) {
                if($lcolop['Key'] == "UNI") {
                  $colunasop[] = $lcolop['Field'];
                }
              }
              $chaveop = array_intersect($colunasop, $colrelop);
              $valueop = array();
              foreach($chaveop as $chave) {
                if(array_key_exists($chave, $combineop)) {
                    $valueop[] = $combineop[$chave];
                }
              }
              $combop = array_combine($chaveop, $valueop);
              $countchave = count($chaveop);
              foreach($combop as $kcolop => $vcolop) {
                  $verifop[] = $kcolop."='$vcolop'";
              }
               $sqlverifop = implode(" OR ",$verifop);
              if($sqlverifop != "") {
                $seloper = "SELECT COUNT(*) as result FROM operador WHERE $sqlverifop";
                $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die (mysql_error());
              }
              if($eseloper['result'] >= 1) {
                $selidop = utf8_encode("SELECT * FROM operador WHERE $sqlverifop");
                $eselidop = $_SESSION['fetch_array']($_SESSION['query']($selidop)) or die (mysql_error());
                $idop = $eselidop['idoperador'];
                for(;list(,$col) = each($colrelop), list(,$d) = each($dadosop);) {
                    $dadossupop[] = $col."='".$d."'";
                }
                $update = utf8_encode("UPDATE operador SET ".implode(", ",$dadossupop)." WHERE idoperador='".$idop."'");
                $eup = $_SESSION['query']($update) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o do cadastro do operador");
              }
               else {
                 $insertoper = utf8_encode("INSERT INTO operador ($sqlcolop, dtbase, horabase) VALUE ('$sqldadosop', NOW(), NOW())");
                 $eoper = $_SESSION['query']($insertoper) or die (mysql_error());
                 $idop = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
               }
               $sqlcoldados = implode(",", $colreldados);
               $sqldados = implode("', '", $dadosdados);
               if($tipo == "I") {
                    $verifhist = "SELECT COUNT(*) as result FROM fila_grava fo
                                  WHERE fo.idupload='$idup' AND fo.idoperador='$idop' AND fo.idsuper_oper='$idsuper' AND fo.idrel_filtros='$idrel'";
               }
               else {
                    $verifhist = "SELECT COUNT(*) as result FROM fila_oper fo
                                  WHERE fo.idupload='$idup' AND fo.idoperador='$idop' AND fo.idsuper_oper='$idsuper' AND fo.idrel_filtros='$idrel'";
               }
               $everifhist = $_SESSION['fetch_array']($_SESSION['query']($verifhist)) or die (mysql_error());
                if($sqlcoldados == "") {
                     if($tipo == "I") {
                          $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado,camaudio,narquivo,arquivo) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0,'$gravacao','$ngravacao','$gravacao')");
                     }
                     else {
                          $historico = utf8_encode("INSERT INTO fila_oper (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0)");
                     }
                }
                if($sqlcoldados != "") {
                     if($tipo == "I") {
                          $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado, camaudio,narquivo,arquivo, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0,'$gravacao','$ngravacao','$gravacao','$sqldados')");
                     }
                     else {
                           $historico = utf8_encode("INSERT INTO fila_oper (idupload, idoperador, idsuper_oper, idrel_filtros, monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0, '$sqldados')");
                     }
                }
                $ehist = $_SESSION['query']($historico) or die (mysql_error());
               array_pop($colrelop);
               if($imp == 1) {
                 $linhassql = $linhassql + 1;
               }
               else {
                 $linhassql = $linhassql;
               }
           }
           else {
               unlink($nomelog);
               $crialog = fopen($nomelog, 'w'); // cria o arquivo
               if($crialog) {
                 $crialinha = array();
                 if($tipo == "I") {
                      $collog[$colgrava['gravacao']] = "gravacao";
                      $collog[] = "OBS"; // coloca a coluna de OBS
                 }
                 else {
                      $collog[] = "OBS"; // coloca a coluna de OBS
                 }
                 $linhacolw = implode(";", $collog); // implode para uma string a primeira linha com o nome das colunas
                 fwrite($crialog, "$linhacolw\r\n"); // escreve no arquivo a linha acima
                 chmod($nomelog, 0777);
                 fclose($crialog); // fecha o arquivo
               }
           }
        }
      }

        if($acao == "v") {
            $msgimp = "Verificação realizada";
            header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp);
        }
        else {
            $dataimp = date('Y-m-d');
            $horaimp = date('H:i:s');
            $erro = $lidos - $linhassql;
            if($erro != $lidos) {
              $atuupload = "UPDATE upload SET imp='S', dataimp='$dataimp', horaimp='$horaimp', qtdeimp='$linhassql', qtdeerro='$erro' WHERE idupload='$idup'";
              $eatupup = $_SESSION['query']($atuupload) or die (mysql_error());
              $msgimp = "Importação finalizada e ''$linhassql'' registro/s atualizados/s ou inserido/s com sucesso!!!";
            }
            else {
                $msgimp = "Nenhum dado importado para o sistema, pois todo arquivo estava inconsistente, importação não realizada!!!";
            }


            /*envia e-mail*/
            $confemail = "SELECT *,count(*) as r FROM relemailimp WHERE idrel_filtros='$idrel'";
            $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail)) or die ("erro na query de consulta da configuraÃƒÂ§ÃƒÂ£o de email relacionada");
            $envmail = new ConfigMail();
            $envmail->IsSMTP();
            $envmail->IsHTML(true);
            $envmail->SMTPAuth = true;
            $envmail->idconf = $econfemail['idconfemailenv'];
            $envmail->iduser = $_SESSION['usuarioID'];
            $envmail->tipouser = $_SESSION['user_tabela'];
            $envmail->upload = $idup;
            $envmail->nomeuser = $_SESSION['usuarioNome'];
            $envmail->qtdeimp = $linhasql;
            $envmail->qtdelido = $lidos;
            $envmail->tipoacao = "importacao";
            $envmail->config();
            $emailsadm = explode(",",$econfemail['usersadm']);
            $emailsweb = explode(",",$econfemail['usersweb']);
            foreach($emailsadm as $adm) {
                  if($adm != "") {
                    $emails['user_adm'][] = $adm;
                }
            }
            foreach($emailsweb as $web) {
                  if($web != "") {
                    $emails['user_web'][] = $web;
                }
            }
            foreach($emails as $kemail => $m) {
                foreach($m as $km => $e) {
                      if($e != "") {
                        $selusers = "SELECT nome$kemail, email FROM $kemail WHERE id$kemail='$e'";
                        $eselusers = $_SESSION['fetch_array']($_SESSION['query']($selusers)) or die ("erro na query para consultar nome do usuÃƒÂ¡rio");
                        $email = $eselusers['email'];
                        $user = $eselusers['nome'.$m];
                        $envmail->AddAddress("$email","$user");
                        $arqlog = explode("/",$nomelog);
                        $arqlog = end($arqlog);
                        $envmail->AddAttachment("$nomelog");
                        if($envmail->Send()) {
                        }
                        else {
                            $erroemail = ". Ocorreu um erro no envio de e-mail";
                        }
                        $envmail->ClearAddresses();
                    }
                }
            }
            header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp." $erroemail ");
        }
    }
}

function scapeaspas($data) {
    return str_replace("'",'',(str_replace('"', '', $data)));
}

function importa_semdados ($tipoimp,$idup, $idrel, $idparam, $caminho, $relapres, $dataini, $datafim, $datactt, $acao,$tiposerver,$ftp) {
      $selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
      $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
      $semdados = $eselparam['semdadps'];
      $dadosup = "SELECT * FROM upload u
                  INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                  INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
                  WHERE u.idupload='$idup'";
      $edadosup = $_SESSION['fetch_array']($_SESSION['query']($dadosup)) or die ("erro na query de consulta do upload");
      $ext = explode(",",$eselparam['tpaudio']);
      $tabfila = $edadosup['tabfila'];
      $arvsaudio = array();
      //$dir = $caminho;
          $data = date('d.m.Y');

          //if(!file_exists($file)) { // verifica se o arquivo jÃƒÂ¡ existe, se nÃƒÂ£o existir cria.
          if($acao == "v") {
              if($tiposerver == "FTP") {
                  $nlog = "logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
              }
              else {
                  $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
              }
              if(file_exists($nomelog)) {
                  unlink($nomelog);
              }
          }
          else {
              if($tiposerver == "FTP") {
                  $nlog = "logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
              }
              else {
                  $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
              }
          }
          if($tiposerver == "FTP") {
              $ctemp = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/log/$nlog");
              $crialog = fopen($ctemp,'w');
              chmod($ctemp,0666);
              $dir = ftp_nlist($ftp, $caminho);
          }
          else {
              $crialog = fopen($nomelog, 'w'); // cria o arquivo
              chmod($nomelog,0777);
              $dir = scandir($caminho);
          }
          //}
          foreach($dir as $arq) {
              if(basename($arq) != "." && basename($arq) != "..") {
                  $part = explode(".",basename($arq));
                  $extarq = end($part);
                if(in_array(strtolower($extarq),$ext)) {
                    $arvsaudio[] = basename($arq);
                }
                else {
                    $arqfora[] = $arq;
                }
              }
          }
          $lidos = 0;
          $linhassql = 0;
          $selcampos = "SHOW COLUMNS FROM fila_grava";
          $eselcampos = $_SESSION['query']($selcampos) or die (mysql_error());
          while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
              if($lselcampos['Field'] == "idfila_grava") {

              }
              else {
                  if($lselcampos['Field'] == "idupload") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = "'".$idup."'";
                  }
                  if($lselcampos['Field'] == "idrel_filtros") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = "'".$idrel."'";
                  }
                  if($lselcampos['Field'] == "datactt") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = "'".$datactt."'";
                  }
                  if($lselcampos['Field'] == "camaudio") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = "'".$caminho."'";
                  }
                  if($lselcampos['Field'] == "monitorado") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = 0;
                  }
                  if($lselcampos['Field'] == "monitorando") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = 0;
                  }
                  if($lselcampos['Field'] != "datactt" && $lselcampos['Field'] != "monitorando" && $lselcampos['Field'] != "monitorado" && $lselcampos['Field'] != "camaudio" && $lselcampos['Field'] != "idrel_filtros" && $lselcampos['Field'] != "idupload") {
                      $colsql[$lselcampos['Field']] = $lselcampos['Field'];
                      $valsql[$lselcampos['Field']] = "NULL";
                  }
              }
          }

          if($crialog) {
              $linhalog = array("ID. Upload", "ID. Rel", "Nome Relacionamento", "Nome Arquivo", "OBS");
              $linhacolw = implode(";", $linhalog); // implode para uma string a primeira linha com o nome das colunas
              fwrite($crialog, "$linhacolw\r\n"); // escreve no arquivo a linha acima
              chmod($nomelog, 0777);
              fclose($crialog); // fecha o arquivo
          }
          $n = 0;

          foreach($arvsaudio as $naudio) {
            if($tiposerver == "FTP") {
                $audio = $naudio;
            }
            else {
                $audio = mysql_real_escape_string($naudio);
            }
            $lidos++;
            $obs1 = array();
            $obs2 = "";
            $imp = 0;
            $erros = 0;

            $extarq = end(explode(".",$audio));
            $arqcheck = str_replace(".".$extarq,"",$audio);
            $verifaudio = "SELECT u.dataimp, COUNT(*) as result FROM fila_grava fg
                           INNER JOIN upload u ON u.idupload = fg.idupload
                           WHERE fg.arquivo='$arqcheck'";
            $everifau = $_SESSION['fetch_array']($_SESSION['query']($verifaudio)) or die ("erro na query de consulta do audio");

            if($everifau['result'] >= 1) {
                $obs1[] = "O arquuivo de audio já foi importado para o sistema em ".$everifau['dataimp'];
                if($acao == "v") {
                      if($tiposerver == "FTP") {
                          $nomelog = $ctemp;
                      }
                      else {
                          $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                      }
                  }
                  else {
                      if($tiposerver == "FTP") {
                          $nomelog = $ctemp;
                      }
                      else {
                          $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                      }
                      $arraycol = $valop + $valsup + $valdados;
                      ksort($arraycol);
                      $linhalog = array("$idup", "$idrel", "$relapres","$naudio", "O arquivo já existe no servidor com data de trabalho entre: ".$eaudio['dataini']." ÃƒÂ  ".$eaudio['datafim']."!!!");
                      $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                      $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                      fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                      chmod($nomelog,0777);
                      fclose($crialog); // fecha arquivo de log
                      $delaudios[] = $caminho."/".$audio;
                  }
                  $erros++;
            }
            else {
                  if($acao == "v") {
                      $obs2 = "Os dados estão corretos para importação!!!";
                      if($tiposerver == "FTP") {
                          $nomelog = $ctemp;
                      }
                      else {
                          $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                      }
                      $arraycol = $valop + $valsup + $valdados;
                      ksort($arraycol);
                      $linhalog = array("$idup", "$idrel", "$relapres","$naudio", "$obs2");
                      $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                      $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                      fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                      chmod($nomelog,0666);
                      fclose($crialog); // fecha arquivo de log;
                  }
                  else {
                    $obs2 = "importacao realizada com sucesso!!!";
                    $n++;
                    if($tiposerver == "FTP") {
                          $nomelog = $ctemp;
                      }
                      else {
                        $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                    }
                    $arraycol = $valop + $valsup + $valdados;
                    ksort($arraycol);
                    $linhalog = array("$idup", "$idrel", "$relapres","$naudio", "$obs2");
                    $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                    $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                    fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                    fclose($crialog); // fecha arquivo de log;
                    $imp++;
                    $datan = date('dmY');
                    $horan = date('His');
                    $camaudio = $caminho;
                    $extarq = explode(".",$audio);
                    $extaudio = end($extarq);
                    $newname = $idup."_".$idrel."_".$datan."_".$horan."_".$n.".".$extaudio;
                    if($tiposerver == "FTP") {
                        $mod = ftp_chmod($ftp, 0777, $camaudio."/".$audio);
                        $rename = ftp_rename($ftp, $caminho."/".$audio, $caminho."/".$newname);
                    }
                    else {
                        chmod($caminho."/".trim($audio),0777);
                        rename($caminho."/".trim($audio), $caminho."/".$newname);
                    }
                    $valsql['narquivo'] = "'".$arqcheck."'";
                    $valsql['arquivo'] = "'".$newname."'";

                    $insertfila = utf8_encode("INSERT INTO fila_grava (".implode(",",$colsql).") VALUES (".implode(",",$valsql).")");
                    $einsert = $_SESSION['query']($insertfila) or die ("erro na query de inserção da fila");
                  }
            }
          }

        if($acao == "v") {
              if($tiposerver == "FTP") {
                  $copy = ftp_put($ftp, $nlog, $ctemp, FTP_BINARY);
              }
              $msgimp = "Verficação reaizada";
              //unlink($ctemp);
              header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp);
        }
        else {
            $dataimp = date('Y-m-d');
            $horaimp = date('H:i:s');
            $erros = $lidos - $imp;
            if($erros != $lidos) {
                $atuupload = "UPDATE upload SET tabfila='fila_grava', imp='S', dataimp='$dataimp', horaimp='$horaimp', qtdeimp='$linhassql',qtdeerro='$erros' WHERE idupload='$idup'";
                $eatupup = $_SESSION['query']($atuupload) or die (mysql_error());
                $msgimp = "Importação finalizada e ''$imp'' registro/s inserido/s com sucesso!!!";
            }
            else {
                $msgimp = "Nenhum dado foi importado para o sistema, pois todos audios estavam inconsistenteste";
            }

            /*envia e-mail*/
              $confemail = "SELECT * FROM relemailimp WHERE idrel_filtros='$idrel'";
              $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail)) or die ("erro na query de consulta da configuraÃƒÂ§ÃƒÂ£o de email relacionada");
              $envmail = new ConfigMail();
              $envmail->IsSMTP();
              $envmail->IsHTML(true);
              $envmail->SMTPAuth = true;
              $envmail->idconf = $econfemail['idconfemailenv'];
              $envmail->iduser = $_SESSION['usuarioID'];
              $envmail->tipouser = $_SESSION['user_tabela'];
              $envmail->upload = $idup;
              $envmail->nomeuser = $_SESSION['usuarioNome'];
              $envmail->qtdeimp = $linhassql;
              $envmail->qtdelido = $lidos;
              $envmail->tipoacao = "importacao";
              $envmail->config();
              $emailsadm = explode(",",$econfemail['usersadm']);
              $emailsweb = explode(",",$econfemail['usersweb']);
              foreach($emailsadm as $adm) {
                  if($adm != "") {
                    $emails['user_adm'][] = $adm;
                  }
              }
              foreach($emailsweb as $web) {
                  if($web != "") {
                    $emails['user_web'][] = $web;
                  }
              }
              foreach($emails as $kemail => $m) {
                  foreach($m as $km => $e) {
                      if($e == "") {
                      }
                      else {
                          $selusers = "SELECT nome$kemail, email FROM $kemail WHERE id$kemail='$e'";
                          $eselusers = $_SESSION['fetch_array']($_SESSION['query']($selusers)) or die ("erro na query para consultar nome do usuÃƒÂ¡rio");
                          $nome = $eselusers['nome'.$kemail];
                          $mail =  $eselusers['email'];
                          $envmail->AddAddress("$mail", "$nome");
                          $arqlog = explode("/",$nomelog);
                          $arqlog = end($arqlog);
                          if($tiposerver == "FTP") {
                              $caminho = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/tmp");
                          }
                          $envmail->AddAttachment("$nomelog");
                          if($envmail->Send()) {
                          }
                          else {
                              $erroemail = ". Ocorreu um erro no envio de e-mail";
                          }
                          $envmail->ClearAllRecipients();
                      }
                  }
              }
              $comaudios = 0;
              if($tiposerver == "FTP") {
                  $selpasta = ftp_chdir($ftp, $edadosup['camupload']);
                  $copy = ftp_put($ftp, $nlog, $ctemp, FTP_BINARY);
                  $checkpasta = ftp_nlist($ftp, $edadosup['camupload']);
                  $dellog = unlink($caminho."/".$nlog);
              }
              else {
                  $checkpasta = scandir($caminho);
              }
              foreach($checkpasta as $checkaudio) {
                  $ckaudio = end(explode("/",$checkaudio));
                  if($ckaudio != "." && $ckaudio != "..") {
                      if(eregi(".txt",$ckaudio) OR eregi(".csv",$ckaudio)) {
                      }
                      else {
                          $comaudios++;
                      }
                  }
              }
              if($comaudios == 0) {
                  if($tiposerver == "FTP") {
                  }
                  else {
                    unlink($nomelog);
                    rmdir($caminho);
                  }
              }
            header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp." $erroemail ");
        }
}

function importa_audio ($tipoimp,$idup, $idrel, $idparam, $caminho, $relapres, $dataini, $datafim, $acao,$tiposerver,$ftp) {
  $selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
  $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
  $semdados = $eselparam['semdadps'];
  $dadosup = "SELECT * FROM upload u
                        INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                        INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
                        LEFT JOIN fila_grava fg ON fg.idupload = u.idupload
                        WHERE u.idupload='$idup' GROUP BY u.idupload";
  $edadosup = $_SESSION['fetch_array']($_SESSION['query']($dadosup)) or die ("erro na query de consulta do upload");
  if($edadosup['idfila_grava'] != "") {
        $msgimp = "O UPLOAD ''$idup'' não pode ser importado, pois já consta com fila Gerada no Servidor, favor contatar o Administrador";
        header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp);
  }
  else {
    $confere = $eselparam['conf'];
    $ext = explode(",",$eselparam['tpaudio']);
    $tabfila = $edadosup['tabfila'];
    $arvsaudio = array();
    //$dir = $caminho;
        $data = date('d.m.Y');

        //if(!file_exists($file)) { // verifica se o arquivo jÃƒÂ¡ existe, se nÃƒÂ£o existir cria.
        if($acao == "v") {
            if($tiposerver == "FTP") {
                $nlog = "logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
            }
            else {
                $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
            }
            if(file_exists($nomelog)) {
                unlink($nomelog);
            }
        }
        else {
            if($tiposerver == "FTP") {
                $nlog = "logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
            }
            else {
                $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
            }
        }
        if($tiposerver == "FTP") {
            $ctemp = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/log/$nlog");
            $crialog = fopen($ctemp,'w');
            chmod($ctemp,0777);
            $dir = ftp_nlist($ftp, $caminho);
        }
        else {
            $crialog = fopen($nomelog, 'w'); // cria o arquivo
            chmod($nomelog,0777);
            $dir = scandir($caminho);
        }
        //}
        foreach($dir as $arq) {
            if(basename($arq) != "." && basename($arq) != "..") {
                $part = explode(".",basename($arq));
                $extarq = strtolower(end($part));
                if(in_array($extarq,$ext)) {
                    $arvsaudio[] = basename($arq);
                }
                else {
                    $arqfora[] = $arq;
                }
            }
        }
        $lidos = 0;
        $linhassql = 0;
        $selcampos = "SELECT coluna_oper.nomecoluna, coluna_oper.incorpora, camposparam.idparam_moni, camposparam.idcoluna_oper,
                        camposparam.posicao,tamanho,tipo FROM camposparam
                        INNER JOIN coluna_oper ON coluna_oper.idcoluna_oper = camposparam.idcoluna_oper
                        WHERE idparam_moni='$idparam' AND camposparam.ativo='S' ORDER BY posicao";
        $eselcampos = $_SESSION['query']($selcampos) or die (mysql_error());
        $colvalop = array();
        $colvalsup = array();
        $colvaldados = array();
        while($lselcampos = $_SESSION['fetch_array']($eselcampos)) {
          $chave = $lselcampos['posicao'] - 1;
          if($lselcampos['incorpora'] == "OPERADOR") {
              $colvalop[$chave] = $lselcampos['nomecoluna'];
          }
          if($lselcampos['incorpora'] == "SUPER_OPER") {
              $colvalsup[$chave] = $lselcampos['nomecoluna'];
          }
          if($lselcampos['incorpora'] == "DADOS") {
              if($lselcampos['nomecoluna'] == "auditor") {
                  $colvalop[$chave] = "operador";
              }
              else {
                  $colvaldados[$chave] = $lselcampos['nomecoluna'];
              }
          }
        }
        $colrelop = $colvalop;
        $colrelsup = $colvalsup;
        $colreldados = $colvaldados;
        if($colrelsup == "") {
            $colverif = $colrelop + $colreldados;
        }
        if($colreldados == "") {
            $colverif = $colrelop + $colrelsup;
        }
        if($colrelsup == "" && $colreldados == "") {
            $colverif = $colrelop;
        }
        if($colrelop != "" && $colrelop != "" && $colreldados != "") {
            $colverif = $colrelop + $colrelsup + $colreldados;
        }
        ksort($colverif);
        if($crialog) {
            if($semdados == "S") {
                $linhalog = array("ID. Upload", "ID. Rel", "Nome Relacionamento", "Nome Arquivo", "OBS");
            }
            else {
                $linhalog = array("ID. Upload", "ID. Rel", "Nome Relacionamento", implode(";",$colverif),"Nome Arquivo", "OBS");
            }
            $linhacolw = implode(";", $linhalog); // implode para uma string a primeira linha com o nome das colunas
            fwrite($crialog, "$linhacolw\r\n"); // escreve no arquivo a linha acima
            chmod($nomelog, 0777);
            fclose($crialog); // fecha o arquivo
        }
        $n = 0;

        foreach($arvsaudio as $naudio) {
            $manipula3 = array();
            if($tiposerver == "FTP") {
                $audio = $naudio;
            }
            else {
                $audio = mysql_real_escape_string($naudio);
            }
            $lidos++;
            $obs1 = array();
            $obs2 = "";
            $imp = 0;

            $manipula2 = end(explode(".",$audio));
            $manipula2 = str_replace(".$manipula2","",$audio);
            $exte = end(explode(".",$audio));
            $separador = $eselparam['ccampos'];
            $sparte = $eselparam['cpartes'];
            $parte = explode("$sparte",$manipula2);
            if(count($parte) == 2) {
                $audiofim= $parte[0];
            }
            else {
                $audiofim = $manipula2;
            }
            $uni = array();
            if($parte[1] != "") {
                if(in_array($audiofim.".$exte",$unitmp)) {
                }
                else {
                    $uni[$parte[1]] = $caminho."/".$manipula2.".$exte";
                    //$unitmp[$parte[1]] = $manipula2.".$exte";
                    foreach($arvsaudio as $checkpart) {
                        $partcheck = array();
                        $manipulapart = end(explode(".",$checkpart));
                        $manipulapart = str_replace(".$manipulapart","",$checkpart);
                        if($manipulapart == $manipula2) {
                        }
                        else {
                            $partcheck = explode("$sparte",$manipulapart);
                            if($partcheck[0] == $parte[0]) {
                                $uni[$partcheck[1]] = $caminho."/".$manipulapart.".$exte";
                                $unitmp[$partcheck[1]] = $audiofim.".$exte";
                            }
                        }
                    }
                }
            }
            ksort($uni);

        if(in_array($manipula2.".$exte",$unitmp) && $part[1] != "") {
        }
        else {
        if(count($uni) == 1 && $parte[1] != "") {
            $obs1[] = "O arquivo está dividido em partes e não possui as demais partes";
            $audio = $parte[0].".".$exte;
        }
        if(count($uni) > 1 && $parte[1] != "") {
            if($acao == "v") {
                $audio = $parte[0].".".$exte;
            }
            else {
                $audio = $parte[0].".".$exte;
                $arquni = $caminho."/".$parte[0].'.'.$exte;
                $comand = "cat '".implode("' '",$uni)."' > '$arquni'";
                $exeunion = system("$comand",$retval);
                //if($exeunion) {
                foreach($uni as $udel) {
                    if(in_array($udel,$delaudios)) {
                    }
                    else {
                        $delaudios[] = $udel;
                    }
                }
            }
        }

        if(count($parte) >= 2 && count($uni) < 2) {
        }
        else {
        $retespecial = explode("$separador",$audiofim);
        foreach($retespecial as $dado) {
            $manipula3[] = removeAcentos($dado);
        }
        $qtdecampos = count($manipula3);
        if($_SESSION['selbanco'] == "monitoria_itau") {
          if(count($colverif) == $qtdecampos) {
              $qtdecampos = $qtdecampos;
          }
          else {
              $auditor = $manipula3[$qtdecampos - 1];
          }
        }

        //vincula valores da nomenclatura do audio aos dados necessários
        $valop = array_intersect_key($manipula3, $colrelop);
        $valsup = array_intersect_key($manipula3, $colrelsup);
        $valdados = array_intersect_key($manipula3, $colreldados);

        if($qtdecampos < count($colverif) OR (($qtdecampos - count($colverif)) >= 2 && $_SESSION['selbanco'] == "monitoria_itau") OR ($qtdecampos > count($colverif) && $_SESSION['selbanco'] != "monitoria_itau")) {
            $manipula3[] = "";
            $obs1[] = "O nome do arquivo não possui a quantidade de campos necessários para importação!!!";
        }
        else {
          $selaud = "SELECT obriga_aud FROM conf_rel WHERE idrel_filtros='$idrel'";
          $eselaud = $_SESSION['fetch_array']($_SESSION['query']($selaud)) or die ("erro na query de consulta da obrigação da Auditoria");
          if($eselaud['obriga_aud'] == "S") {
              if((count($colverif) + 1) != count($manipula3)) {
                  $erroaud = 1;
              }
              else {
                  $erroaud = 0;
              }
          }
          else {
              $erroaud = 0;
          }
          if($erroaud == 1) {
              $obs1[] = "A gravação não possui o Auditor nas colunas!!!";
          }
          else {
              foreach($ext as $vaudio) {
                  $verifaudio = "SELECT idupload,COUNT(*) as result FROM fila_grava fg
                                  WHERE fg.narquivo='".str_replace(".".$vaudio, "", $audio)."'";
                  $everifaudi = $_SESSION['fetch_array']($_SESSION['query']($verifaudio)) or die ("erro na query de consulta do audio no sistema");
                  if($everifaudi['result'] >= 1) {
                          $obs1[] = "Audio já importado para o sistema no upload ''".$everifaudi['idupload']."''";
                  }
                  $verifaudio = "SELECT idupload,COUNT(*) as result FROM fila_grava fg
                                  WHERE fg.narquivo='".str_replace(".".strtoupper($vaudio), "", $audio)."'";
                  $everifaudi = $_SESSION['fetch_array']($_SESSION['query']($verifaudio)) or die ("erro na query de consulta do audio no sistema");
                  if($everifaudi['result'] >= 1) {
                          $obs1[] = "Audio já importado para o sistema no upload ''".$everifaudi['idupload']."''";
                  }
              }
              if($confere == "S") {
                  $checksel = explode(".",$audio);
                  $confselecao = "SELECT count(*) as result FROM selecao WHERE nomearquivo='$checksel[0]' and selecionado='1'";
                  $econfselecao = $_SESSION['fetch_array']($_SESSION['query']($confselecao)) or die (mysql_error());
                  if($econfselecao['result'] == 0) {
                      $obs1[] = "O audio não está presente na seleção";
                  }
              }
              $dadosop = array_combine($colrelop, $valop);
              $dadossup = array_combine($colrelsup, $valsup);
              $dadosdados = array_combine($colreldados, $valdados);
              $obsop = tipodado($dadosop);
              $obssup = tipodado($dadossup);
              $obsdados = tipodado($dadosdados);
              if($obsop != "") {
                  $obs1[] = $obsop;
              }
              if($obssup != "") {
                  $obs1[] = $obssup;
              }
              if($obsdados != "") {
                  $obs1[] = $obsdados;
              }
              $obs2 = implode(",",$obs1);

              foreach($colverif as $kcol => $lcol) {
                  $kverif = $lcol;
                  $selcol = "SELECT incorpora, nomecoluna, unico FROM coluna_oper WHERE nomecoluna='$kverif'";
                  $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta da coluna para verificaÃƒÂ§ÃƒÂ£o");
                  if($eselcol['unico'] == "S") {
                      if($eselcol['incorpora'] == "OPERADOR") {
                          $dadosv['op'][$kverif] = $kverif."='$dadosop[$kverif]'";
                      }
                      if($eselcol['incorpora'] == "SUPER_OPER") {
                          $dadosv['sup'][$kverif] = $kverif."='$dadossup[$kverif]'";
                      }
                      if($eselcol['incorpora'] == "DADOS") {
                          $dadosv['dados'][$kverif] = $kverif."='$dadosdados[$kverif]'";
                      }
                  }
              }
              $compara = $dadosv;
              $obscomp = array();
              foreach($dadosv as $tpdados => $check) {
                  $checksql = implode(" OR ",$check);
                  $cols = array_keys($check);
                  if(count($check) <= 1) {
                  }
                  else {
                      if($tpdados == "op") {
                          $vop = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM operador WHERE $checksql";
                          $evop = $_SESSION['fetch_array']($_SESSION['query']($vop)) or die ("erro na query de consulta dos dados do operador");
                          if($evop['result'] >= 1) {
                              foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                  if($evop[$kcomp] != $dadosop[$kcomp]) {
                                      $msgcomp = "Os dados do operador, estão inconsistentes com o banco de dados";
                                      if(!in_array($msgcomp,$obscomp)) {
                                          $obscomp[] = $msgcomp;
                                      }
                                  }
                              }
                          }
                      }
                      if($tpdados == "sup") {
                          $vsup = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM super_oper WHERE $checksql";
                          $evsup = $_SESSION['fetch_array']($_SESSION['query']($vsup)) or die ("erro na query de consulta dos dados do operador");
                          if($evop['result'] >= 1) {
                              foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                  if($evsup[$kcomp] != $dadossup[$kcomp]) {
                                      $msgcomp = "Os dados do supervisor, estão inconsistentes com o banco de dados";
                                      if(!in_array($msgcomp,$obscomp)) {
                                          $obscomp[] = $msgcomp;
                                      }
                                  }
                              }
                          }
                      }
                      if($tpdados == "dados") {
                          $vdados = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM $tabfila WHERE $checksql";
                          $evdados = $_SESSION['fetch_array']($_SESSION['query']($vdados)) or die ("erro na query de consulta dos dados do operador");
                          if($evop['result'] >= 1) {
                              foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                  if($evdados[$kcomp] != $dadosdados[$kcomp]) {
                                      $msgcomp = "Os dados estão inconsistentes com o banco de dados";
                                      if(!in_array($msgcomp,$obscomp)) {
                                          $obscomp[] = "Os dados estão inconsistentes com o banco de dados";
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }
        }
        if(count($obs1) == 0 && $obscomp[0] == "")  { // se a variÃƒÂ¡vel $obs1 continuar vazia apÃƒÂ³s a verificaÃƒÂ§ÃƒÂ£o do tipo do campos, considera-se que nÃƒÂ£o existe erro nos caracteres informados e a importaÃƒÂ§ÃƒÂ£o serÃƒÂ¡ OK
                $verifaudio = "SELECT idupload, COUNT(*) as result FROM fila_grava fg
                                WHERE fg.narquivo='".str_replace(".".$exte,"",trim($audio))."'";
                $eaudio = $_SESSION['fetch_array']($_SESSION['query']($verifaudio)) or die ("erro na query de consulta do arquivo");
                if($eaudio['result'] >= 1) {
                    if($acao == "v") {
                        if($tiposerver == "FTP") {
                            $nomelog = $ctemp;
                        }
                        else {
                            $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                        }
                    }
                    else {
                        if($tiposerver == "FTP") {
                            $nomelog = $ctemp;
                        }
                        else {
                            $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                        }
                        $arraycol = $valop + $valsup + $valdados;
                        ksort($arraycol);
                        $linhalog = array("$idup", "$idrel", "$relapres",implode(";",$arraycol), "$audio", "O arquivo já existe no servidor com data de trabalho entre: ".$eaudio['dataini']." e ".$eaudio['datafim']."!!!");
                        $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                        $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                        fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                        chmod($nomelog,0777);
                        fclose($crialog); // fecha arquivo de log
                        $delaudios[] = $caminho."/".$audio;
                    }
                    unset($auditor);
                }
                else {
                    if($acao == "v") {
                        $obs2 = "Os dados estão corretos para importação!!!";
                        if($tiposerver == "FTP") {
                            $nomelog = $ctemp;
                        }
                        else {
                            $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                        }
                        $arraycol = $valop + $valsup + $valdados;
                        ksort($arraycol);
                        $linhalog = array("$idup", "$idrel", "$relapres",implode(";",$arraycol), "$audio", "$obs2");
                        $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                        $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                        fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                        chmod($nomelog,0666);
                        fclose($crialog); // fecha arquivo de log;
                    }
                    else {
                        $obs2 = "importacao realizada com sucesso!!!";
                        $n++;
                        $imp = 1;
                        if($tiposerver == "FTP") {
                            $nomelog = $ctemp;
                        }
                        else {
                            $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                        }
                        $arraycol = $valop + $valsup + $valdados;
                        ksort($arraycol);
                        $linhalog = array("$idup", "$idrel", "$relapres",implode(";",$arraycol), "$audio", "$obs2");
                        $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                        $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                        fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                        fclose($crialog); // fecha arquivo de log;
                        $verifdata = "SELECT * FROM coluna_oper
                                            INNER JOIN camposparam ON camposparam.idcoluna_oper = coluna_oper.idcoluna_oper
                                            INNER JOIN param_moni ON param_moni.idparam_moni = camposparam.idparam_moni
                                            WHERE param_moni.idparam_moni='$idparam' AND coluna_oper.tipo='DATE' AND camposparam.ativo='S'";
                        $everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
                        while($lverifdata = $_SESSION['fetch_array']($everifdata)) {
                        if($lverifdata['incorpora'] == "OPERADOR") {
                            foreach($dadosop as $key => $dadoop) {
                                if($key == $lverifdata['nomecoluna']) {
                                    $arrayalt = array($key => dataimp($dadoop));
                                    $dadosop = array_replace($dadosop, $arrayalt);
                                }
                            }
                        }
                        if($lverifdata['incorpora'] == "SUPER_OPER") {
                            foreach($dadossup as $key => $dadosup) {
                                if($key == $lverifdata['nomecoluna']) {
                                    $arrayalt = array($key => dataimp($dadosup));
                                    $dadossup = array_replace($dadossup, $arrayalt);
                                }
                            }
                        }
                        if($lverifdata['incorpora'] == "DADOS") {
                            foreach($dadosdados as $key => $dado) {
                                if($key == $lverifdata['nomecoluna']) {
                                    $arrayalt = array($key => dataimp($dado));
                                    $dadosdados = array_replace($dadosdados, $arrayalt);
                                }
                            }
                        }
                        }
                        $sqlcolop = implode(",", $colrelop);
                        $sqlvalop = implode("', '", $dadosop);
                        $sqlcolsup = implode(",", $colrelsup);
                        $sqlvalsup = implode("', '", $dadossup);
                        if(count($dadossup) >= 1 && $dadossup != false) {
                            $colsuper = "SHOW COLUMNS FROM super_oper"; // lista as colunas presentas na tabela super_oper para verificar quais campos sÃƒÂ£o UNICOS, ou seja, precisam ser verificados
                            $ecolsuper = $_SESSION['query']($colsuper);
                            $colunassup = array();
                            while($lcolsuper = $_SESSION['fetch_array']($ecolsuper)) { // faz o loop nas linha encontradas no select
                                if($lcolsuper['Key'] == "UNI") { // se a coluna listada no select for UNI acrescenta o nome da coluna no array das colunas ÃƒÂ  serem verificadas
                                        array_push($colunassup, $lcolsuper['Field']);
                                }
                            }
                            $chavesup = array_intersect($colunassup, $colrelsup); // cria um array com as colunas que constam no arquivo lido e sÃƒÂ£o unicas na tabela do supervisor
                            $valuesup = array(); // cria array dos valores que sÃƒÂ£o unicos
                            foreach($chavesup as $chave) { // faz um loop das cchaves identificadas para obter da array de dados da linha quais dados se relacionam com a chave unica
                                if(array_key_exists($chave, $dadossup)) { // verifica se a chave exists no dados supervisor
                                        array_push($valuesup, $dadossup[$chave]);
                                }
                            }
                            $verifsup = array();
                            $combsup = array_combine($chavesup, $valuesup); // cria um array de combinaÃƒÂ§ÃƒÂ£o das chaves com os dados unicos para servir de contagem no loop que irÃƒÂ¡ cria a string do SQL
                            foreach($combsup as $kcol => $vcol) {
                                $verifsup[] = $kcol."='$vcol'";
                            }
                            $sqlverifsup = implode(" OR ",$verifsup); // transforma a SQL em string para colocar na Query
                            if($sqlverifsup != "") {
                                $sqlsuper = "SELECT COUNT(*) as result FROM super_oper WHERE $sqlverifsup"; // verifica se os dados unicos informados jÃƒÂ¡ estÃƒÂ£o em sistema
                                $esqlsuper = $_SESSION['fetch_array']($_SESSION['query']($sqlsuper)) or die (mysql_error());
                            }
                            if($esqlsuper['result'] >= 1) { // se os dados jÃƒÂ¡ estiverem em sistema, entÃƒÂ£o nÃƒÂ£o ÃƒÂ© necessÃƒÂ¡rio inserir os dados novamente no banco, sÃƒÂ³ obter o id do supervisor
                                $selidsuper = "SELECT * FROM super_oper WHERE $sqlverifsup";
                                $eidsuper = $_SESSION['fetch_array']($_SESSION['query']($selidsuper)) or die (mysql_error());
                                $idsuper = $eidsuper['idsuper_oper'];
                            }
                            else {
                                $insertsuper = utf8_encode("INSERT INTO super_oper ($sqlcolsup, dtbase, horabase) VALUE ('$sqlvalsup', NOW(), NOW())"); // se os dados nÃƒÂ£o existirem na tabela em questÃƒÂ£o, faz inserÃƒÂ§ÃƒÂ£o
                                $esuper = $_SESSION['query']($insertsuper) or die (mysql_error());
                                $idsuper = str_pad($_SESSION['insert_id'](), 6, '0', STR_PAD_LEFT);
                            }
                            $colrelop[] = "idsuper_oper";
                            $dadosop['idsuper_oper'] = $idsuper;
                            $idsuper = $dadosop['idsuper_oper'];
                            $sqlcolop = implode(",", $colrelop);
                            $sqldadosop = implode("', '", $dadosop);
                        }
                        else {
                            $colrelop[] = "idsuper_oper";
                            $selsuper = "SELECT * FROM super_oper WHERE super_oper='NAO INFORMADO'";
                            $eselsuper = $_SESSION['fetch_array']($_SESSION['query']($selsuper)) or die ("erro na query de consulta do supervisor nulo");
                            $dadosop['idsuper_oper'] = $eselsuper['idsuper_oper'];
                            $idsuper = $dadosop['idsuper_oper'];
                            $sqlcolop = implode(",", $colrelop);
                            $sqldadosop = implode("', '", $dadosop);
                        }
                        if(count($dadosop) == 1) {
                            $selop = "SELECT idoperador FROM operador WHERE operador='NAO LOCALIZADO'";
                            $eselop = $_SESSION['fetch_array']($_SESSION['query']($selop)) or die ("erro na procura do operador NAO LOCALIZADO");
                            $idop = $eselop['idoperador'];
                        }
                        else {
                            $verifop = array();
                            $colop = "SHOW COLUMNS FROM operador";
                            $ecolop = $_SESSION['query']($colop) or die (mysql_error());
                            $colunasop = array();
                            while($lcolop = $_SESSION['fetch_array']($ecolop)) {
                                if($lcolop['Key'] == "UNI") {
                                    array_push($colunasop, $lcolop['Field']);
                                }
                            }
                            $chaveop = array_intersect($colunasop, $colrelop);
                            $valueop = array();
                            foreach($chaveop as $chave) {
                                if(array_key_exists($chave, $dadosop)) {
                                    array_push($valueop, $dadosop[$chave]);
                                }
                            }
                            $combop = array_combine($chaveop, $valueop);
                            $countchave = count($chaveop);
                            foreach($combop as $kcolop => $vcolop) {
                                $verifop[] = $kcolop."='$vcolop'";
                            }
                            $sqlverifop = implode(" OR ",$verifop);
                            if($sqlverifop != "") {
                                $seloper = "SELECT COUNT(*) as result FROM operador WHERE $sqlverifop";
                                $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die (mysql_error());
                            }
                            if($eseloper['result'] >= 1) {
                                $selidop = "SELECT * FROM operador WHERE $sqlverifop";
                                $eselidop = $_SESSION['fetch_array']($_SESSION['query']($selidop)) or die (mysql_error());
                                $idop = $eselidop['idoperador'];
                                for(;list(,$col) = each($colrelop), list(,$d) = each($dadosop);) {
                                    $dadossupop[] = $col."='".$d."'";
                                }
                                $update = "UPDATE operador SET ".implode(", ",$dadossupop)." WHERE idoperador='".$idop."'";
                                $eup = $_SESSION['query']($update) or die ("erro na query de atualização do cadastro do operador");
                            }
                            else {
                                $insertoper = utf8_encode("INSERT INTO operador ($sqlcolop, dtbase, horabase) VALUE ('$sqldadosop', NOW(), NOW())");
                                $eoper = $_SESSION['query']($insertoper) or die (mysql_error());
                                $idop = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                            }
                        }
                        $datan = date('dmY');
                        $horan = date('his');
                        $camaudio = $caminho;
                        $extarq = explode(".",$audio);
                        $extaudio = end($extarq);
                        $newname = $idup."_".$idrel."_".$idop."_".$idsuper."_".$datan."_".$horan."_".$n.".".$extaudio;
                        if($tiposerver == "FTP") {
                            $mod = ftp_chmod($ftp, 0777, $camaudio."/".$audio);
                            $rename = ftp_rename($ftp, $caminho."/".$audio, $caminho."/".$newname);
                        }
                        else {
                            chmod($caminho."/".trim($audio),0777);
                            rename($caminho."/".trim($audio), $caminho."/".$newname);
                        }
                        $dadosdados['camaudio'] = trim($caminho);
                        $newnaudio = $audio;
                        foreach($ext as $vext) {
                            $newnaudio = str_replace(".".$vext, "", $newnaudio);
                            $extup = strtoupper($vext);
                            $newnaudio = str_replace(".".$extup, "", $newnaudio);
                        }
                        $dadosdados['narquivo'] = trim($newnaudio);
                        $dadosdados['arquivo'] = trim($newname);

                        //$colreldados = array_push($colreldados, "camaudio");
                        //$valdados = array_push($valdados, $caminho);
                        //$dadosdados = array_combine($colreldados, $valdados);
                        $sqlcoldados = array();
                        $sqlvaldados = array();
                        foreach($dadosdados as $col => $dado) {
                            $sqlcoldados[] = $col;
                            $sqlvaldados[] = "'$dado'";
                        }
                        $sqlcoldados = implode(", ", $sqlcoldados);
                        $sqlvaldados = implode(", ",$sqlvaldados);
                        //$sqlcoldados = implode(",", $colreldados);
                        //$sqlvaldados = implode("', '", $dadosdados);
                        $verifhist = "SELECT COUNT(*) as result FROM fila_grava WHERE narquivo='".trim($audio)."'";
                        $everifhist = $_SESSION['fetch_array']($_SESSION['query']($verifhist)) or die (mysql_error());
                        if($everifhist['result'] >= 1) {
                            unlink($caminho."/".$audio);
                        }
                        else {
                            if(!isset($dadosdados)) {
                                if($auditor == "") {
                                    $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros,monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0, $sqlvaldados)");
                                }
                                else {
                                    $verifaud = "SELECT idoperador, COUNT(*) as result FROM operador WHERE operador='$auditor'";
                                    $everifaud = $_SESSION['fetch_array']($_SESSION['query']($verifaud));
                                    if($everifaud['result'] >= 1) {
                                        $idauditor = $everifaud['idoperador'];
                                    }
                                    else {
                                        $insertaud = utf8_encode("INSERT INTO operador (operador,dtbase,horabase) VALUES ('$auditor',NOW(),NOW())");
                                        $exeaud = $_SESSION['query']($insertaud) or die (mysql_error());
                                        $idauditor = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                                    }
                                    $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idauditor,idrel_filtros,monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idauditor','$idrel',0,0, $sqlvaldados)");
                                }
                            $ehist = $_SESSION['query']($historico) or die (mysql_error());
                            }
                            else {
                                if($auditor == "") {
                                    $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros,monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0, $sqlvaldados)");
                                }
                                else {
                                    $verifaud = "SELECT idoperador, COUNT(*) as result FROM operador WHERE operador='$auditor'";
                                    $everifaud = $_SESSION['fetch_array']($_SESSION['query']($verifaud));
                                    if($everifaud['result'] >= 1) {
                                        $idauditor = $everifaud['idoperador'];
                                    }
                                    else {
                                        $insertaud = utf8_encode("INSERT INTO operador (operador,dtbase,horabase) VALUES ('$auditor',NOW(),NOW())");
                                        $exeaud = $_SESSION['query']($insertaud) or die (mysql_error());
                                        $idauditor = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                                    }
                                    $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idauditor,idrel_filtros,monitorando, monitorado, $sqlcoldados) VALUE ('$idup', '$idop', '$idsuper', '$idauditor','$idrel',0,0, $sqlvaldados)");
                                }
                            $ehist = $_SESSION['query']($historico) or die (mysql_error());
                            }
                        }
                        $selecao = "SELECT COUNT(*) as result FROM selecao WHERE nomearquivo='$newnaudio'";
                        $eselecao = $_SESSION['fetch_array']($_SESSION['query']($selecao)) or die (mysql_error());
                        if($eselecao['result'] >= 1) {
                            $upselecao = "UPDATE selecao SET enviado='1' WHERE nomearquivo='$newnaudio'";
                            $eupselecao = $_SESSION['query']($upselecao) or die (mysql_error());
                        }
                        unset($auditor);
                        array_pop($colrelop);
                        if($imp == 1) {
                            $linhassql++;
                        }
                        else {
                            $linhassql = $linhassql;
                        }
                    }
                }
                }
                else {
                    unset($auditor);
                    if($acao == "v") {
                        if($tiposerver == "FTP") {
                            $nomelog = $ctemp;
                        }
                        else {
                            $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                        }
                    }
                    else {
                        if($tiposerver == "FTP") {
                            $nomelog = $ctemp;
                        }
                        else {
                            $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
                        }
                        $delaudios[] = $caminho."/".$audio;
                    }
                    $obs1[] = implode(",",$obscomp);
                    $obs1 = array_unique($obs1);
                    $dadoslog = $valop + $valsup + $valdados;
                    $linhalog = array("$idup", "$idrel", "$relapres", implode(";",$dadoslog), "$audio", implode(",",$obs1));
                    $linhawrite = implode(";", $linhalog); // transforma a linha atual em string
                    $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
                    fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
                    chmod($nomelog,0777);
                    fclose($crialog); // fecha arquivo de log
            }
        }
        }
    }
    // deleta audios nÃƒÂ£o importados
    if($delaudios != "") {
        foreach($delaudios as $del) {
            if($tiposerver == "FTP") {
                ftp_rmdir($ftp, $del);
            }
            else {
                $delaudio = unlink($del);
            }
        }
    }
    closedir($dir);

    if($acao == "v") {
        if($tiposerver == "FTP") {
            $copy = ftp_put($ftp, $nlog, $ctemp, FTP_BINARY);
        }
        $msgimp = "Verficação reaizada";
        //unlink($ctemp);
        header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp);
    }
    else {
        $dataimp = date('Y-m-d');
        $horaimp = date('H:i:s');
        $erros = $lidos - $linhassql;
        if($erros != $lidos) {
            $atuupload = "UPDATE upload SET tabfila='fila_grava', imp='S', dataimp='$dataimp', horaimp='$horaimp', qtdeimp='$linhassql',qtdeerro='$erros' WHERE idupload='$idup'";
            $eatupup = $_SESSION['query']($atuupload) or die (mysql_error());
            $msgimp = "Importação finalizada e ''$linhassql'' registro/s inserido/s com sucesso!!!";
        }
        else {
            $msgimp = "Nenhum dado foi importado para o sistema, pois todos audios estavam inconsistentes";
        }

        /*envia e-mail*/
        $confemail = "SELECT * FROM relemailimp WHERE idrel_filtros='$idrel'";
        $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail)) or die ("erro na query de consulta da configuração de email relacionada");
        $envmail = new ConfigMail();
        $envmail->IsSMTP();
        $envmail->IsHTML(true);
        $envmail->SMTPAuth = true;
        $envmail->idconf = $econfemail['idconfemailenv'];
        $envmail->iduser = $_SESSION['usuarioID'];
        $envmail->tipouser = $_SESSION['user_tabela'];
        $envmail->upload = $idup;
        $envmail->nomeuser = $_SESSION['usuarioNome'];
        $envmail->qtdeimp = $linhassql;
        $envmail->qtdelido = $lidos;
        $envmail->tipoacao = "importacao";
        $envmail->config();
        $emailsadm = explode(",",$econfemail['usersadm']);
        $emailsweb = explode(",",$econfemail['usersweb']);
        foreach($emailsadm as $adm) {
            if($adm != "") {
                $emails['user_adm'][] = $adm;
            }
        }
        foreach($emailsweb as $web) {
            if($web != "") {
                $emails['user_web'][] = $web;
            }
        }
        foreach($emails as $kemail => $m) {
            foreach($m as $km => $e) {
                if($e != "") {
                    $selusers = "SELECT nome$kemail, email FROM $kemail WHERE id$kemail='$e'";
                    $eselusers = $_SESSION['fetch_array']($_SESSION['query']($selusers)) or die ("erro na query para consultar nome do usuÃƒÂ¡rio");
                    $nome = $eselusers['nome'.$kemail];
                    $mail =  $eselusers['email'];
                    $envmail->AddAddress("$mail", "$nome");
                    $arqlog = explode("/",$nomelog);
                    $arqlog = end($arqlog);
                    if($tiposerver == "FTP") {
                        $caminho = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']."/monitoria_supervisao/tmp");
                    }
                    $envmail->AddAttachment("$nomelog");
                    if($envmail->Send()) {
                    }
                    else {
                        $erroemail = ". Ocorreu um erro no envio de e-mail";
                    }
                    $envmail->ClearAllRecipients();
                }
            }
        }
        $comaudios = 0;
        if($tiposerver == "FTP") {
            $selpasta = ftp_chdir($ftp, $edadosup['camupload']);
            $copy = ftp_put($ftp, $nlog, $ctemp, FTP_BINARY);
            $checkpasta = ftp_nlist($ftp, $edadosup['camupload']);
            $dellog = unlink($caminho."/".$arqlog);
        }
        else {
            $checkpasta = scandir($caminho);
        }
        foreach($checkpasta as $checkaudio) {
            $ckaudio = end(explode("/",$checkaudio));
            if($ckaudio != "." && $ckaudio != "..") {
                if(eregi(".txt",$ckaudio) OR eregi(".csv",$ckaudio)) {
                }
                else {
                    $comaudios++;
                }
            }
        }
        if($comaudios == 0) {
            if($tiposerver != "FTP") {
                unlink($nomelog);
                rmdir($caminho);
            }
        }
        header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp. " $erroemail ");
    }
  }
}

function importa_arq_audio ($tipoimp,$idup, $idrel, $idparam, $caminho, $caminhoarq, $relapres, $dataini, $datafim, $acao,$tiposerver,$ftp) {
$data = date('d.m.Y');
$selparam = "SELECT * FROM param_moni WHERE idparam_moni='$idparam'";
$eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die (mysql_error());
$dadosup = "SELECT * FROM upload u
          INNER JOIN periodo p ON p.idperiodo = u.idperiodo
          INNER JOIN interperiodo it ON it.idperiodo = u.idperiodo AND it.semana = u.semana
          WHERE u.idupload='$idup'";
$edadosup = $_SESSION['fetch_array']($_SESSION['query']($dadosup)) or die ("erro na query de consulta do upload");
$tabfila = $edadosup['tabfila'];
$ext = explode(",",$eselparam['tpaudio']);
$tparq = explode(",",$eselparam['tparquivo']);
if($tiposerver == "FTP") {
    $listadir = ftp_nlist($ftp,$caminho);
}
else {
    $listadir = scandir($caminho);
}
foreach($listadir as $audio) {
    if($tiposerver == "FTP") {
        $partau = explode(".",basename($audio));
    }
    else {
        $partau = explode(".",$audio);
    }
    $cau = count($exau);
    $extau = strtolower(end($partau));
    if(in_array($extau,$tparq)) {
    }
    else {
        if(in_array(strtolower($extau), $ext)) {
            $audios[] = $audio;
        }
        else {
            if($extau == "") {
            }
            if($extau == ".") {
            }
            if($extau == "..") {
            }
            if($extau != "" && $extau != "." && $extau != "..") {
                $audiosfora[] = $audio;
            }
        }
    }
}
if($acao == "v") {
    if($tiposerver == "FTP") {
        $nomelog = str_replace("C:","",$_SERVER['DOCUMENT_ROOT'])."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/log/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
    }
    else {
        $nomelog = "$caminho/logverif-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
        if(file_exists($nomelog)) {
          unlink($nomelog);
        }
        else {
        }
    }
}
else {
    if($tiposerver == "FTP") {
        $nomelog = str_replace("C:","",$_SERVER['DOCUMENT_ROOT'])."/monitoria_supervisao/tmp/".strtoupper($_SESSION['nomecli'])."/log/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
    }
    else {
        $nomelog = "$caminho/logimp-".$idrel."-".$edadosup['nmes']."-".$edadosup['ano']."-".$edadosup['semana']."semana-".$data.".csv"; // cria o arquivo com o id do relacionamento, com o nome do relacionamento e com a data
    }
}
$ctemp = $nomelog;
$crialog = fopen($ctemp,'wb');
chmod($ctemp,0777);
$dir = ftp_nlist($ftp, $caminho);
    $linhas = file($caminhoarq, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES); // LÃƒÂª o arquivo linha por linha e ignorando linhas vazias
    $selcampos = "SELECT cp.idcoluna_oper,nomecoluna,tamanho,incorpora,tipo FROM conf_rel cr
                  INNER JOIN camposparam cp ON cp.idparam_moni = cr.idparam_moni
                  INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                  WHERE cr.idrel_filtros='$idrel' AND cp.ativo='S' ORDER BY posicao ASC"; // seleciona os campos relacionados com o relacionamento
    $ecampos = $_SESSION['query']($selcampos) or die (mysql_error());
    $camposop = array();  // cria variavel dos campos do operador
    $campossup = array(); // cria variavel dos campos do supervisor
    $camposdados = array();
    while($lcampos = $_SESSION['fetch_array']($ecampos)) { // passa campo ÃƒÂ  campo
      if($lcampos['incorpora'] == "SUPER_OPER") { // se o campo for para supervisor inclui no array $campossup
	$campossup[] = $lcampos['nomecoluna'];
      }
      if($lcampos['incorpora'] == "OPERADOR") { // se o campo for para o operador inclui no array $camposop
	$camposop[] = $lcampos['nomecoluna'];
      }
      if($lcampos['incorpora'] == "DADOS") { // se o campo for de dados inclui no array $camposdados
	$camposdados[] = $lcampos['nomecoluna'];
      }
    }
    $colunas = explode(";", $linhas[0]); // transforma a primeira linha( ou descriÃƒÂ§ÃƒÂ£o das colunas) em array
    $colnrel = array(); /** cria o array das @colunas que nÃƒÂ£o constam nos parametros da importaÃƒÂ§ÃƒÂ£o */
    $colkeyop = array(); /** cria o array das @chavedooperador */
    $colvalueop = array(); /** cria o array dos @valoresdooperador ( descriÃƒÂ§ÃƒÂ£o das colunas) do operador */
    $colkeysup = array(); /** cria o array das @chavesdosupervisor */
    $colvaluesup = array(); /** cria o array dos @valoresdosupervidor ( descriÃƒÂ§ÃƒÂ£o das colunas) do supervisor */
    $colkeydados = array(); /** cria o array da @chavedocamposdados */
    $colvaluedados = array(); /** cria o array dos @valoresdosdados */
    foreach($colunas as $key => $value) {
        if($value != "idrel_filtros") {
            if($value == "gravacao") {
              $colkeygrav[] = $key;
              $colvaluegrav[] = $value;
            }
            else {
                if(in_array($value, $camposop)) {
                  $colkeyop[] = $key;
                  $colvalueop[] = ereg_replace("[-=+.;.:,!@#$%&*)(]","",$value);
                }
                if(in_array($value, $campossup)) {
                  $colkeysup[] = $key;
                  $colvaluesup[] = ereg_replace("[-=+.;:,!@#$%&*)(]","",$value);
                }
                if(in_array($value, $camposdados)) {
                  $colkeydados[] =  $key;
                  $colvaluedados[] =  ereg_replace("[-=+.;:,!@#$%&*)(]","",$value);
                }
                if(!in_array($value, $camposop) && !in_array($value, $campossup) && !in_array($value, $camposdados)) {
                  $colnrel[] = $value;
                }
            }
        }
    }
    if($colvalueop[0] == "") {
        $colrelop = array();
    }
    else {
        $colrelop = array_combine($colkeyop, $colvalueop); // combina os valores dos arrays para que a ordem das colunas do arquivo original seja mantida
    }
    if($colvaluesup[0] == "") {
        $colrelsup = array();
    }
    else {
        $colrelsup = array_combine($colkeysup, $colvaluesup); // combina os valores dos arrays para que a ordem das colunas do arquivo original seja mantida
    }
    if($colvaluedados[0] == "") {
        $colreldados = array();
    }
    else {
        $colreldados = array_combine($colkeydados, $colvaluedados);
    }
    if($colvaluegrav[0] == "") {
        $colrelgrav = array();
    }
    else {
        $colrelgrav = array_combine($colkeygrav, $colvaluegrav);
    }
    if($colrelsup[0] == "" && $colreldados[0] == "") {
      $collog = $colrelgrav + $colrelop;
    }
    if($colrelsup != "" && $colreldados[0] == "") {
      $collog = $colrelgrav + $colrelop + $colrelsup;
    }
    if($colrelsup[0] == "" && $colreldados != "") {
      $collog = $colrelgrav + $colrelop + $colreldados;
    }
    if($colreldados != "" && $colrelop != "" && $colrelsup != "" && $colrelgrav != "") {
      $collog = $colrelgrav + $colrelop + $colrelsup + $colreldados; // cria o array que contem as colunas do arquivo de log
    }
    ksort($collog); // organiza as colunas por ordem da chave
    if($collog == "") { // verifica se alguma coluna nÃƒÂ£o consta em nenhuma das tabelas de operador ou supervisor, se sim nÃƒÂ£o executa a importaÃƒÂ§ÃƒÂ£o
	$msgimp = "Nenhuma coluna do arquivo de leitura está relacionada aos paramentros de monitoria!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    if(count($colnrel) == 1) { // verifica se alguma coluna nÃƒÂ£o consta em nenhuma das tabelas de operador ou supervisor, se sim nÃƒÂ£o executa a importaÃƒÂ§ÃƒÂ£o
	$colnrels = implode(",", $colnrel);
	$msgimp = "A coluna ''$colnrels'' não está relacionada com as colunas cadastradas para esta importação, favor verificar!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    if(!in_array("gravacao",$colunas)) {
        $msgimp = "A coluna ''GRAVAÇÃO'' não consta no arquivo de relacionamento, favor veirficar!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    if(count($colnrel) > 1) { // verifica se alguma coluna nÃƒÂ£o consta em nenhuma das tabelas de operador ou supervisor, se sim nÃƒÂ£o executa a importaÃƒÂ§ÃƒÂ£o
	$colnrels = implode(",", $colnrel);
	$msgimp = "A/s coluna/s ''$colnrels'' não estão relacionadas com as colunas cadastradas para esta importação, favor verificar!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    if(in_array("gravacao", $colnrel)) {
	$msgimp = "O arquivo de dados nao contem a coluna ''GRAVACAO'', necessária para vinculo do audio com o arquivo!!";
	header("location:/monitoria_supervisao/inicio.php?menu=importa&msgimp=".$msgimp);
	break;
    }
    else { // se nenhuma linha for encontrada em $colnrel segue abaixo
      $linhassql = 0;
      $n = 0;
      foreach($linhas as $kl => $linha) { // faz o loop para percorrer as linhas do arquivo
          $obschar = "";
          $obs1 = array();
          $obs2 = "";
          $imp = 0;
          if($kl == 0) {
            if($tiposerver == "FTP") {
                $crialog = fopen($ctemp, 'w');
            }
            else {
                $crialog = fopen($nomelog, 'w'); // cria o arquivo
            }
            chmod($nomelog,0777);
            if($crialog) {
              $crialinha = array();
              $collog[] = "OBS"; // coloca a coluna de OBS
              $linhacolw = implode(";", $collog); // implode para uma string a primeira linha com o nome das colunas
              fwrite($crialog, "$linhacolw\r\n"); // escreve no arquivo a linha acima
              fclose($crialog); // fecha o arquivo
            }
          }
          else {
               $lidos = $lidos + 1; // define a quantidade de linhas lidas do arquivo;
               $lcolunas = explode(";", $linha); // transforma em array linha atual do loop
               $dados = array();
               foreach($lcolunas as $kcoluna => $lcoluna) { // faz loop para inserir os dados da coluna na linha atual
                   $verifcol = $collog[$kcoluna];
                   if($verifcol == "gravacao") {
                       $dados[] = removeAcentos(trim(ereg_replace("#","",$lcoluna)));
                   }
                   else {
                       $selreplace = "SELECT caracter, COUNT(*) as result FROM coluna_oper WHERE nomecoluna='$verifcol'";
                       $eselreplace = $_SESSION['fetch_array']($_SESSION['query']($selreplace)) or die (mysql_error());
                       if($eselreplace['result'] == 0) {
                           $dados[] = removeAcentos($lcoluna);
                       }
                       else {
                           if($eselreplace['caracter'] == "") {
                               $dados[] = removeAcentos($lcoluna);
                           }
                           else {
                               $caracter = $eselreplace['caracter'];
                               $dados[] = removeAcentos(trim(ereg_replace("[$caracter]","",$lcoluna)));
                           }
                       }
                   }
               }

               $valop = array_intersect_key($dados, $colrelop);
               $dadosop = array_combine($colrelop, $valop);
               $valsup = array_intersect_key($dados, $colrelsup);
               $dadossup = array_combine($colrelsup, $valsup);
               $valdados = array_intersect_key($dados, $colreldados);
               $dadosdados = array_combine($colreldados, $valdados);
               $valgrav = array_intersect_key($dados, $colrelgrav);
               $dadosgrav = array_combine($colrelgrav, $valgrav);
               $verifdata = "SELECT * FROM coluna_oper WHERE tipo='DATE'";
               $everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
               if($valsup == "" && $valdados != "") {
                   $dadosverif = $valgrav + $valop + $valdados;
               }
               if($valsup == "" && $valdados == "") {
                   $dadosverif = $valgrav + $valop;
               }
               if($valsup != "" && $valdados == "") {
                   $dadosverif = $valgrav + $valop + $valsup;
               }
               if($valsup != "" && $valdados != "") {
                 $dadosverif = $valgrav + $valop + $valsup + $valdados;
               }
               ksort($dadosverif);
             if($kl == 0) { // verifica se o arquivo jÃƒÂ¡ existe, se nÃƒÂ£o existir cria.
             }
             else { // se o arquivo jÃƒÂ¡ existir, segue com os cÃƒÂ³digos abaixo
               $obsop = tipodado ($dadosop);
               $obssup = tipodado ($dadossup);
               $obsdados = tipodado ($dadosdados);
               $checkgrav = implode("",$dadosgrav);
               if($tiposerver == "FTP") {
                   if(in_array($caminho."/".$checkgrav, $audios)) { // verifica se o arquivo de audio informado no campo gravaÃƒÂ§ÃƒÂ£o existe, se existir nÃƒÂ£o faz criticas
                     $obsgrav = "";
                   }
                   else { // se o arquivo audio nÃƒÂ£o existir na pasta informada no cadastro dos parametros, coloca a observaÃƒÂ§ÃƒÂ£o abaixo
                       if(in_array($checkgrav, $audiosfora)) {
                           $obsgrav = "O arquivo de gravação não possui extensão liberada para importação no sistema!!!";
                       }
                       else {
                           $obsgrav = "O arquivo de gravação não foi localizado!!!";
                       }
                   }
               }
               else {
                   if(in_array($checkgrav, $audios)) { // verifica se o arquivo de audio informado no campo gravaÃƒÂ§ÃƒÂ£o existe, se existir nÃƒÂ£o faz criticas
                     $obsgrav = "";
                   }
                   else { // se o arquivo audio nÃƒÂ£o existir na pasta informada no cadastro dos parametros, coloca a observaÃƒÂ§ÃƒÂ£o abaixo
                       if(in_array($checkgrav, $audiosfora)) {
                           $obsgrav = "O arquivo de gravação não possui extensão liberada para importação no sistema!!!";
                       }
                       else {
                           $obsgrav = "O arquivo de gravação não foi localizado!!!";
                       }
                   }
               }
               foreach($ext as $vext) {
                   $chgrav = "SELECT fg.idupload, COUNT(*) as result FROM fila_grava fg
                             WHERE fg.narquivo='".str_replace(".".$vext, "",$checkgrav)."'";
                   $echgrav = $_SESSION['fetch_array']($_SESSION['query']($chgrav)) or die (mysql_error());
                   if($echgrav['result'] >= 1) {
                       $obsgrav = "A gravação já foi importada para o sistema no upload ''".$echgrav['idupload']."''";
                   }
                   $chgrav = "SELECT fg.idupload, COUNT(*) as result FROM fila_grava fg
                             WHERE fg.narquivo='".str_replace(".".strtoupper($vext), "",$checkgrav)."'";
                   $echgrav = $_SESSION['fetch_array']($_SESSION['query']($chgrav)) or die (mysql_error());
                   if($echgrav['result'] >= 1) {
                       $obsgrav = "A gravação já foi importada para o sistema no upload ''".$echgrav['idupload']."''";
                   }
               }

               foreach($collog as $kcol => $lcol) {
                   $kverif = $lcol;
                   if($kverif == "gravacao" OR $kverif == "OBS") {
                   }
                   else {
                       $selcol = "SELECT incorpora, nomecoluna, unico FROM coluna_oper WHERE nomecoluna='$kverif'";
                       $eselcol = $_SESSION['fetch_array']($_SESSION['query']($selcol)) or die ("erro na query de consulta da coluna para verificaÃƒÂ§ÃƒÂ£o");
                       if($eselcol['unico'] == "S") {
                           if($eselcol['incorpora'] == "OPERADOR") {
                               $dadosv['op'][$kverif] = $kverif."='$dadosop[$kverif]'";
                           }
                           if($eselcol['incorpora'] == "SUPER_OPER") {
                               $dadosv['sup'][$kverif] = $kverif."='$dadossup[$kverif]'";
                           }
                           if($eselcol['incorpora'] == "DADOS") {
                               $dadosv['dados'][$kverif] = $kverif."='$dadosdados[$kverif]'";
                           }
                       }
                       else {
                       }
                   }
               }
               $compara = $dadosv;
               $obscomp = array();
               foreach($dadosv as $tpdados => $check) {
                   $checksql = implode(" OR ",$check);
                   $cols = array_keys($check);
                   if(count($check) <= 1) {
                   }
                   else {
                       if($tpdados == "op") {
                           $vop = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM operador WHERE $checksql";
                           $evop = $_SESSION['fetch_array']($_SESSION['query']($vop)) or die ("erro na query de consulta dos dados do operador");
                           if($evop['result'] == 0) {
                           }
                           else {
                               foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                   if($evop[$kcomp] != $dadosop[$kcomp]) {
                                       $msgcomp = "Os dados do operador, estão inconsistentes com o banco de dados";
                                       if(in_array($msgcomp,$obscomp)) {
                                       }
                                       else {
                                         $obscomp[] = $msgcomp;
                                       }
                                   }
                                   else {
                                   }
                               }
                           }
                       }
                       if($tpdados == "sup") {
                           $vsup = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM super_oper WHERE $checksql";
                           $evsup = $_SESSION['fetch_array']($_SESSION['query']($vsup)) or die ("erro na query de consulta dos dados do supervisor");
                           if($evsup['result'] == 0) {
                               }
                               else {
                                   foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                       if($evsup[$kcomp] != $dadossup[$kcomp]) {
                                           $msgcomp = "Os dados do supervisor, estão inconsistentes com o banco de dados";
                                           if(in_array($msgcomp,$obscomp)) {
                                           }
                                           else {
                                             $obscomp[] = $msgcomp;
                                           }
                                       }
                                       else {
                                       }
                                   }
                               }
                       }
                       if($tpdados == "dados") {
                           $vdados = "SELECT ".implode(",",$cols).", COUNT(*) as result FROM $tabfila WHERE $checksql";
                           $evdados = $_SESSION['fetch_array']($_SESSION['query']($vdados)) or die ("erro na query de consulta dos dados");
                           if($evdados['result'] == 0) {
                           }
                           else {
                               foreach($compara[$tpdados] as $kcomp => $checkcomp) {
                                   if($evdados[$kcomp] != $dadosdados[$kcomp]) {
                                       $msgcomp = "Os dados adicionais da fila de monitoria, estão inconsistentes com o banco de dados";
                                       if(in_array($msgcomp,$obscomp)) {
                                       }
                                       else {
                                         $obscomp[] = $msgcomp;
                                       }
                                   }
                                   else {
                                   }
                               }
                           }
                       }
                   }
               }

               if($obsop != "") {
                   $obs1[] = $obsop;
               }
               if($obssup != "") {
                   $obs1[] = $obssup;
               }
               if($obsdados != "") {
                   $obs1[] = $obsdados;
               }
               if($obsgrav != "") {
                 $obs1[] = $obsgrav;
               }
               foreach($obs1 as $ob) {
                   if($ob != "") {
                     $obs2[] = $ob;
                   }
               }
               $obs2[] = implode(",",$obscomp);
               $obs2 = implode(",",$obs2);
               if(count($obs1) == 0) { // se a variÃƒÂ¡vel $obs1 continuar vazia apÃƒÂ³s a verificaÃƒÂ§ÃƒÂ£o do tipo do campos, considera-se que nÃƒÂ£o existe erro nos caracteres informados e a importaÃƒÂ§ÃƒÂ£o serÃƒÂ¡ OK
                   $n++;
                   if($acao == "v") {
                       $obs2 = "Os dados para importação estão corretos!!!";
                       $imp = 0;
                   }
                   else {
                     $obs2 = "importacao realizada com sucesso!!!";
                     $imp = 1;
                   }
               }
               else {
                   if($acao == "v") {
                   }
                   else {
                     $delaudios[] = $caminho."/".$naudio;
                   }
               }
               $dadosverif[] = $obs2; // incrementa as observaÃƒÂ§ÃƒÂµes ÃƒÂ  linha que serÃƒÂ¡ inserida no arquivo de log
               $linhawrite = implode(";", $dadosverif); // transforma a linha atual em string
               $crialog = fopen($nomelog, 'a+'); // abre o arquivo se jÃƒÂ¡ existir
               fwrite($crialog, "$linhawrite\r\n"); // escreve no arquivo a linha acima
               fclose($crialog); // fecha arquivo de log
               if($imp == 1) { // se a importaÃƒÂ§ÃƒÂ£o ocorrer OK
                    $verifdata = "SELECT * FROM coluna_oper INNER JOIN camposparam ON camposparam.idcoluna_oper = coluna_oper.idcoluna_oper INNER JOIN param_moni ON param_moni.idparam_moni = camposparam.idparam_moni
                    WHERE param_moni.idparam_moni='$idparam' AND coluna_oper.tipo='DATE' AND camposparam.ativo='S'";
                    $everifdata = $_SESSION['query']($verifdata) or die (mysql_error());
                    while($lverifdata = $_SESSION['fetch_array']($everifdata)) {
                      if($lverifdata['incorpora'] == "OPERADOR") {
                        foreach($dadosop as $key => $dadoop) {
                          if($key == $lverifdata['nomecoluna']) {
                            $arrayalt = array($key => dataimp($dadoop));
                            $dadosop = array_replace($dadosop, $arrayalt);
                          }
                        }
                      }
                      if($lverifdata['incorpora'] == "SUPER_OPER") {
                        foreach($dadossup as $key => $dadosup) {
                          if($key == $lverifdata['nomecoluna']) {
                            $arrayalt = array($key => dataimp($dadosup));
                            $dadossup = array_replace($dadossup, $arrayalt);
                          }
                        }
                    }
                      if($lverifdata['incorpora'] == "DADOS") {
                        foreach($dadosdados as $key => $dado) {
                          if($key == $lverifdata['nomecoluna']) {
                            $arrayalt = array($key => dataimp($dado));
                            $dadosdados = array_replace($dadosdados, $arrayalt);
                          }
                        }
                      }
                    }
                    if($dadossup != false && count($dadossup) >= 1) { // se os dados do supervisor constarem nas colunas relacionada ÃƒÂ  importaÃƒÂ§ÃƒÂ£o executa as linha abaixo
                      $sqlcolsup = implode(",", $colrelsup); // cria a string com o nome das colunas do supervisor
                      $sqldadossup = implode("', '",$dadossup); // cria a string com os dados do supervisor
                      $combinesup = array_combine($colrelsup, $dadossup); // combina as duas arrays acima para criar um array onde as colunas $colrelsup sÃƒÂ£o chaves e $dadossup sÃƒÂ£o os dados
                      $verifsup = array(); // cria array de verificaÃƒÂ§ÃƒÂ£o dos dados do supervisor
                      $colsuper = "SHOW COLUMNS FROM super_oper"; // lista as colunas presentas na tabela super_oper para verificar quais campos sÃƒÂ£o UNICOS, ou seja, precisam ser verificados
                      $ecolsuper = $_SESSION['query']($colsuper);
                      $colunassup = array();
                      while($lcolsuper = $_SESSION['fetch_array']($ecolsuper)) { // faz o loop nas linha encontradas no select
                        if($lcolsuper['Key'] == "UNI") { // se a coluna listada no select for UNI acrescenta o nome da coluna no array das colunas ÃƒÂ  serem verificadas
                              array_push($colunassup, $lcolsuper['Field']);
                        }
                      }
                      $chavesup = array_intersect($colunassup, $colrelsup); // cria um array com as colunas que constam no arquivo lido e sÃƒÂ£o unicas na tabela do supervisor
                      $valuesup = array(); // cria array dos valores que sÃƒÂ£o unicos
                      foreach($chavesup as $chave) { // faz um loop das cchaves identificadas para obter da array de dados da linha quais dados se relacionam com a chave unica
                          if(array_key_exists($chave, $combinesup)) { // verifica se a chave exists no dados supervisor
                              array_push($valuesup, $combinesup[$chave]);
                          }
                      }
                      $combsup = array_combine($chavesup, $valuesup); // cria um array de combinaÃƒÂ§ÃƒÂ£o das chaves com os dados unicos para servir de contagem no loop que irÃƒÂ¡ cria a string do SQL
                      foreach($combsup as $kcol => $vcol) {
                          $verifsup[] = $kcol."='$vcol'";
                      }
                      $sqlverifsup = implode(" OR ",$verifsup); // transforma a SQL em string para colocar na Query
                      if($sqlverifsup == "") {
                      }
                      else {
                          $sqlsuper = "SELECT COUNT(*) as result FROM super_oper WHERE $sqlverifsup"; // verifica se os dados unicos informados jÃƒÂ¡ estÃƒÂ£o em sistema
                          $esqlsuper = $_SESSION['fetch_array']($_SESSION['query']($sqlsuper)) or die (mysql_error());
                      }
                      if($esqlsuper['result'] >= 1) { // se os dados jÃƒÂ¡ estiverem em sistema, entÃƒÂ£o nÃƒÂ£o ÃƒÂ© necessÃƒÂ¡rio inserir os dados novamente no banco, sÃƒÂ³ obter o id do supervisor
                        $selidsuper = "SELECT * FROM super_oper WHERE $sqlverifsup";
                        $eidsuper = $_SESSION['fetch_array']($_SESSION['query']($selidsuper)) or die (mysql_error());
                        $idsuper = $eidsuper['idsuper_oper'];
                      }
                      else {
                        $insertsuper = utf8_encode("INSERT INTO super_oper ($sqlcolsup, dtbase, horabase) VALUE ('$sqldadossup', NOW(), NOW())"); // se os dados nÃƒÂ£o existirem na tabela em questÃƒÂ£o, faz inserÃƒÂ§ÃƒÂ£o
                        $esuper = $_SESSION['query']($insertsuper) or die (mysql_error);
                        $idsuper = str_pad($_SESSION['insert_id'](), 6, '0', STR_PAD_LEFT);
                      }
                      $colrelop[] = "idsuper_oper";
                      $dadosop['idsuper_oper'] = $idsuper;
                      $sqlcolop = implode(",", $colrelop);
                      $sqldadosop = implode("', '", $dadosop);
                    }
                    else {
                      $colrelop[] = "idsuper_oper";
                      $selsuper = "SELECT * FROM super_oper WHERE super_oper='NAO INFORMADO'";
                      $eselsuper = $_SESSION['fetch_array']($_SESSION['query']($selsuper)) or die ("erro na query de consulta do supervisor nulo");
                      $dadosop['idsuper_oper'] = $eselsuper['idsuper_oper'];
                      $idsuper = $eselsuper['idsuper_oper'];
                      $sqlcolop = implode(",", $colrelop);
                      $sqldadosop = implode("', '", $dadosop);
                    }
                    $combineop = array_combine($colrelop, $dadosop);
                    $verifop = array();
                    $colop = "SHOW COLUMNS FROM operador";
                    $ecolop = $_SESSION['query']($colop) or die (mysql_error());
                    $colunasop = array();
                    while($lcolop = $_SESSION['fetch_array']($ecolop)) {
                      if($lcolop['Key'] == "UNI") {
                        array_push($colunasop, $lcolop['Field']);
                      }
                    }
                    $chaveop = array_intersect($colunasop, $colrelop);
                    $valueop = array();
                    foreach($chaveop as $chave) {
                          if(array_key_exists($chave, $combineop)) {
                              array_push($valueop, $combineop[$chave]);
                          }
                    }
                    $combop = array_combine($chaveop, $valueop);
                    $countchave = count($chaveop);
                    foreach($combop as $kcolop => $vcolop) {
                        $verifop[] = $kcolop."='$vcolop'";
                    }
                    $sqlverifop = implode(" OR ",$verifop);
                    if($sqlverifop == "") {
                    }
                    else {
                        $seloper = "SELECT COUNT(*) as result FROM operador WHERE $sqlverifop";
                        $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die (mysql_error());
                    }
                    if($eseloper['result'] >= 1) {
                      $selidop = "SELECT * FROM operador WHERE $sqlverifop";
                      $eselidop = $_SESSION['fetch_array']($_SESSION['query']($selidop)) or die (mysql_error());
                      $idop = $eselidop['idoperador'];
                    }
                    else {
                      $insertoper = utf8_encode("INSERT INTO operador ($sqlcolop, dtbase, horabase) VALUE ('$sqldadosop', NOW(), NOW())");
                      $eoper = $_SESSION['query']($insertoper) or die (mysql_error());
                      $idop = str_pad($_SESSION['insert_id'](), 7, '0', STR_PAD_LEFT);
                    }
                    if($eseloper['result'] >= 1) {
                      for(;list(,$col) = each($colrelop), list(,$d) = each($dadosop);) {
                          $dadossupop[] = $col."='".$d."'";
                      }
                      $update = "UPDATE operador SET ".implode(", ",$dadossupop)." WHERE idoperador='".$idop."'";
                      $eup = $_SESSION['query']($update) or die ("erro na query de atualizaÃƒÂ§ÃƒÂ£o do cadastro do operador");
                    }
                    $datan = date('dmY');
                    $horan = date('His');
                    $camaudio = $caminho;
                    $partext = explode(".",$checkgrav);
                    $extarq = end($partext);
                    $newname = $idup."_".$idrel."_".$idop."_".$idsuper."_".$datan."_".$horan."_".$n.".".$extarq;
                    if($tiposerver == "FTP") {
                        $mod = ftp_chmod($ftp, 0777, $caminho."/".$checkgrav);
                        $rename = ftp_rename($ftp, $caminho."/".$checkgrav, $caminho."/".$newname);
                    }
                    else {
                        $rename = rename($caminho."/".$checkgrav, $caminho."/".$newname);
                    }
                    foreach($ext as $e) {
                        $checkgrav = str_replace(".".$e, "", $checkgrav);
                        $extup = strtoupper($e);
                        $checkgrav = str_replace(".".$extup, "", $checkgrav);
                    }
                    $colreldados[] = "narquivo";
                    $colreldados[] = "arquivo";
                    $dadosdados['narquivo'] = trim($checkgrav);
                    $dadosdados['arquivo'] = trim($newname);
                    $verifhist = "SELECT COUNT(*) as result FROM fila_grava WHERE narquivo='$checkgrav'";
                    $everifhist = $_SESSION['fetch_array']($_SESSION['query']($verifhist)) or die (mysql_error());
                    if($everifhist['result'] >= 1) {
                    }
                    else {
                      $sqlcoldados = implode(",", $colreldados);
                      $sqlvaldados = implode("', '", $dadosdados);
                      if(!isset($dadosdados)) {
                        $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros,monitorando, monitorado, camaudio) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0,'$camaudio')");
                        $ehist = $_SESSION['query']($historico) or die (mysql_error());
                      }
                      else {
                        $historico = utf8_encode("INSERT INTO fila_grava (idupload, idoperador, idsuper_oper, idrel_filtros,monitorando, monitorado, $sqlcoldados, camaudio) VALUE ('$idup', '$idop', '$idsuper', '$idrel',0,0, '$sqlvaldados', '$camaudio')");
                        $ehist = $_SESSION['query']($historico) or die (mysql_error());
                      }
                    }
                    $checkgrav = "";
                    array_pop($colrelop);
                    array_pop($colreldados);
                    array_pop($colreldados);
                    if($imp == 1) {
                      $linhassql = $linhassql + 1;
                    }
                    else {
                      $linhassql = $linhassql;
                    }
               }
             }
          }
      }
      }
      // deleta audios nÃƒÂ£o importados
      if($delaudios != "") {
          foreach($delaudios as $del) {
              if($tiposerver == "FTP") {
                  $delftp = ftp_rmdir($ftp,$del);
              }
              else {
                  unlink($del);
              }
          }
      }

      if($acao == "v") {
          if($tiposerver == "FTP") {
            $copy = ftp_put($ftp, $caminho."/".basename($nomelog), $nomelog, FTP_BINARY);
            //$deltmplog = unlink($nomelog);
            $deltmparq = unlink($caminhoarq);
          }
          $msgimp = "Verificação realizada";
          header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=".$relapres."&msgimp=".$msgimp);
      }
      else {
          $dataimp = date('Y-m-d');
          $horaimp = date('H:i:s');
          $erros = $lidos - $linhassql;
          if($erros != $lidos) {
              $atuupload = "UPDATE upload SET tabfila='fila_grava', imp='S', dataimp='$dataimp', horaimp='$horaimp', qtdeimp='$linhassql', qtdeerro='$erros' WHERE idupload='$idup'";
              $eatupup = $_SESSION['query']($atuupload) or die (mysql_error());
              $msgimp = "Importação finalizada e ''$linhassql'' registro/s atualizados/s ou inserido/s com sucesso!!!";
          }
          else {
              $msgimp = "Nenhyhum dado foi importado para o sistema, pois todo arquivo estava inconsistente";
          }

          /*envia e-mail*/
          $confemail = "SELECT * FROM relemailimp WHERE idrel_filtros='$idrel'";
          $econfemail = $_SESSION['fetch_array']($_SESSION['query']($confemail)) or die ("erro na query de consulta da configuraÃƒÂ§ÃƒÂ£o de email relacionada");
          $envmail = new ConfigMail();
          $envmail->IsSMTP();
          $envmail->IsHTML(true);
          $envmail->SMTPAuth = true;
          $envmail->idconf = $econfemail['idconfemailenv'];
          $envmail->iduser = $_SESSION['usuarioID'];
          $envmail->tipouser = $_SESSION['user_tabela'];
          $envmail->upload = $idup;
          $envmail->nomeuser = $_SESSION['usuarioNome'];
          $envmail->qtdeimp = $linhassql;
          $envmail->qtdelido = $lidos;
          $envmail->tipoacao = "importacao";
          $envmail->config();
          $emailsadm = explode(",",$econfemail['usersadm']);
          $emailsweb = explode(",",$econfemail['usersweb']);
          foreach($emailsadm as $adm) {
              if($adm != "") {
                $emails['user_adm'][] = $adm;
              }
          }
          foreach($emailsweb as $web) {
              if($web != "") {
                $emails['user_web'][] = $web;
              }
          }
          foreach($emails as $keym => $m) {
              foreach($m as $km => $e) {
                  if($e == "") {
                  }
                  else {
                      $selusers = "SELECT nome$keym, email FROM $keym WHERE id$keym='$e'";
                      $eselusers = $_SESSION['fetch_array']($_SESSION['query']($selusers)) or die ("erro na query para consultar nome do usuÃƒÂ¡rio");
                      $envmail->AddAddress($eselusers['email'],$eselusers['nome'.$keym]);
                      $arqlog = explode("/",$nomelog);
                      $arqlog = end($arqlog);
                      $envmail->AddAttachment("$nomelog");
                      if($envmail->Send()) {
                      }
                      else {
                          $erroemail = ". Ocorreu um erro na envio de e-mail";
                      }
                      $envmail->ClearAddresses();
                  }
              }
          }
          $comaudios = 0;
          if($tiposerver == "FTP") {
              $selpasta = ftp_chdir($ftp, $edadosup['camupload']);
              $copy = ftp_put($ftp, $caminho."/".basename($nomelog), $ctemp, FTP_BINARY);
              $checkpasta = ftp_nlist($ftp, $edadosup['camupload']);
              $dellog = unlink($nomelog);
          }
          else {
              $checkpasta = scandir($caminho);
          }
          foreach($checkpasta as $checkaudio) {
              if($checkaudio != "." && $checkaudio != "..") {
                  if(eregi(".txt",basename($checkaudio)) OR eregi(".csv",basename($checkaudio))) {
                  }
                  else {
                      $comaudios++;
                  }
              }
              else {
              }
          }
          if($comaudios == 0) {
              unlink($nomelog);
              rmdir($caminho);
          }
          else {
          }
          header("location:/monitoria_supervisao/inicio.php?menu=importa&relapres=$relapres&msgimp=$msgimp$erroemail");
      }
}

function & nomeapres($idrelapres) {
    $i = 0;
    $selvisu = "SELECT * FROM filtro_nomes ORDER BY nivel";
    $eselvisu = $_SESSION['query']($selvisu) or die ("erro na consulta dos filtros visualizaveis");
    while($lselvisu = $_SESSION['fetch_array']($eselvisu)) {
        $visu[$lselvisu['nivel']] = "id_".strtolower($lselvisu['nomefiltro_nomes']);
    }
    $selcolunas = "SHOW COLUMNS FROM rel_filtros";
    $ecolunas = $_SESSION['query']($selcolunas) or die (mysql_error());
    while($lcolunas = $_SESSION['fetch_array']($ecolunas)) {
      if($lcolunas['Field'] == "idrel_filtros") {
      }
      else {
          $alias = "a".$i++;
          $colrel['alias'][] = $alias;
          $colrel['colunas'][] = strtolower($lcolunas['Field']);
          $colrel['nomecolunas'][] = str_replace("id_","",$lcolunas['Field']);
          $result[] = $alias.".nomefiltro_dados as ".str_replace("id_","",$lcolunas['Field']);
      }
    }
    for($j = 0; $j < $i; $j++) {
        $inner[] = "INNER JOIN filtro_dados ".$colrel['alias'][$j]." ON ".$colrel['alias'][$j].".idfiltro_dados = rf.".$colrel['colunas'][$j]."";
    }
    $select = "SELECT ".implode(",",$result)." FROM rel_filtros rf ".implode(" ",$inner)." WHERE idrel_filtros='$idrelapres'";
    $esel = $_SESSION['query']($select) or die ("erro na query de consulta do nome do filtro");
    $nsel = $_SESSION['num_rows']($esel);
    if($nsel >= 1) {
        while($lsel = $_SESSION['fetch_array']($esel)) {
            foreach($colrel['nomecolunas'] as $r) {
                $arraynomes[array_search("id_".$r, $visu)] = $lsel[$r];
            }
            krsort($arraynomes);
           $nomeapres = implode(" - ",$arraynomes);
        }
    }
    else {
        $nomeapres = "";
    }
    return $nomeapres;
}

function & nomevisu($idrelapres) {
    $i = 0;
    $selvisu = "SELECT * FROM filtro_nomes WHERE visualiza='S' ORDER BY nivel";
    $eselvisu = $_SESSION['query']($selvisu) or die ("erro na consulta dos filtros visualizaveis");
    while($lselvisu = $_SESSION['fetch_array']($eselvisu)) {
        $visu[$lselvisu['nivel']] = "id_".strtolower($lselvisu['nomefiltro_nomes']);
    }
    if($visu == "") {
        $nomeapres = "SEM FILTRO PARA VISUALIZAR";
    }
    else {
        $selcolunas = "SHOW COLUMNS FROM rel_filtros";
        $ecolunas = $_SESSION['query']($selcolunas) or die (mysql_error());
        while($lcolunas = $_SESSION['fetch_array']($ecolunas)) {
          if($lcolunas['Field'] == "idrel_filtros" OR !in_array($lcolunas['Field'],$visu)) {
          }
          else {
              $alias = "a".$i++;
              $colrel['alias'][] = $alias;
              $colrel['colunas'][] = strtolower($lcolunas['Field']);
              $colrel['nomecolunas'][] = str_replace("id_","",$lcolunas['Field']);
              $result[] = $alias.".nomefiltro_dados as ".str_replace("id_","",$lcolunas['Field']);
          }
        }
        for($j = 0; $j < $i; $j++) {
            $inner[] = "INNER JOIN filtro_dados ".$colrel['alias'][$j]." ON ".$colrel['alias'][$j].".idfiltro_dados = rf.".$colrel['colunas'][$j]."";
        }
        $select = "SELECT ".implode(",",$result)." FROM rel_filtros rf ".implode(" ",$inner)." WHERE idrel_filtros='$idrelapres'";
        $esel = $_SESSION['query']($select) or die ("erro na query de consulta do nome do filtro");
        $nsel = $_SESSION['num_rows']($esel);
        if($nsel >= 1) {
            while($lsel = $_SESSION['fetch_array']($esel)) {
                foreach($colrel['nomecolunas'] as $r) {
                    $arraynomes[array_search("id_".$r, $visu)] = $lsel[$r];
                }
               krsort($arraynomes);
               $nomeapres = implode(" - ",$arraynomes);
            }
        }
        else {
            $nomeapres = "";
        }
    }
   return $nomeapres;
}

function combofiltro() {
  $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel";
  $efiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta dos filtro");
  while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
    $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
    $filtro = strtolower($lfiltros['nomefiltro_nomes']);
    $file = $rais."/monitoria_supervisao/users/combo".$filtro."_".strtolower($_SESSION['nomecli']).".php";
    if(file_exists($file)) {
      $del = unlink($file);
    }
    else {
    }
      $cria = fopen($file, 'w');
      chmod($file, 0666);
      fwrite($cria, "<?php\r\n\n");
      fwrite($cria, '$rais = $_SERVER['."'DOCUMENT_ROOT'".'];'."\r\n");
      fwrite($cria, 'include_once($rais."/monitoria_supervisao/seguranca.php");'."\r\n");
      fwrite($cria, 'include_once($rais."/monitoria_supervisao/config/conexao.php");'."\r\n");
      fwrite($cria, 'include_once($rais."/monitoria_supervisao/selcli.php");'."\r\n\n");
      fwrite($cria, '$iduser = $_SESSION['."'usuarioID'".'];'."\r\n");
      fwrite($cria, '$idfiltro = $_POST['."'filtro'".'];'."\r\n");
      fwrite($cria, '$campo = str_replace("filtro_","",$_POST['."'campo'".']);'."\r\n");
      fwrite($cria, '$combo = str_replace("filtro_","",$_POST['."'combo'".']);'."\r\n");
      fwrite($cria, '$colunasql = array();'."\r\n");
      fwrite($cria, '$valsql = array();'."\r\n\n");
      $self = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel";
      $eself = $_SESSION['query']($self) or die ("erro na query de seleÃƒÂ§ÃƒÂ£o dos nomes dos filtros");
      while($lself = $_SESSION['fetch_array']($eself)) {
          $filt = strtolower($lself['nomefiltro_nomes']);
          fwrite($cria, '$'.$filt.' = $_POST['."'".$filt."'".'];'."\r\n");
          fwrite($cria, 'if($'.$filt.' == "") {'."\r\n");
          fwrite($cria, '}'."\r\n");
          fwrite($cria, 'else {'."\r\n");
          fwrite($cria, 'if($idfiltro == $'.$filt.') {'."\r\n");
          fwrite($cria, '}'."\r\n");
          fwrite($cria, 'else {'."\r\n");
          fwrite($cria, '$colunasql[] = "'.$filt.'";'."\r\n");
          fwrite($cria, '$valsql[] = $'.$filt.';'."\r\n");
          fwrite($cria, '}'."\r\n");
          fwrite($cria, '}'."\r\n\n");
      }
      fwrite($cria, '$valsql = array_combine($colunasql, $valsql);'."\r\n");
      fwrite($cria, 'if($colunasql == "") {'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, 'else {'."\r\n");
      fwrite($cria, 'foreach($valsql as $key => $valor) {'."\r\n");
      fwrite($cria, '$wheresql[] = " AND id_$key=".'.'"'."'".'"'.'.'.'$valor'.'.'.'"'."'".'"'.';'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, '$wheresql = implode("",$wheresql);'."\r\n");
      fwrite($cria, '$selfiltros = "SELECT * FROM rel_filtros WHERE id_$campo=');
      fwrite($cria, ''."'$".'idfiltro'."'".' $wheresql";'."\r\n");
      fwrite($cria, '$eselfiltros = $_SESSION['."'query'".']($selfiltros) or die ("erro na query de consulta dos filtros");'."\r\n");
      fwrite($cria, 'echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\" style=\"height:15px;padding:5px\">Selecione...</option>";'."\r\n");
      fwrite($cria, 'while($lselfiltros = $_SESSION['."'fetch_array'".']($eselfiltros)) {'."\r\n");
      fwrite($cria, 'if($_SESSION['."'user_tabela'".'] == "user_adm") {'."\r\n");
      fwrite($cria, '$tab = "useradmfiltro";'."\r\n");
      fwrite($cria, '$varuser = "iduser_adm";'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, 'if($_SESSION['."'user_tabela'".'] == "user_web") {'."\r\n");
      fwrite($cria, '$tab = "userwebfiltro";'."\r\n");
      fwrite($cria, '$varuser = "iduser_web";'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, '$verifuser = "SELECT COUNT(*) as result FROM $tab WHERE $varuser='."'".'$iduser'."'".' AND idrel_filtros='."'".'".$lselfiltros['."'idrel_filtros'".']."'."'".'";'."\r\n");
      fwrite($cria, '$everifuser = $_SESSION['."'fetch_array'".']($_SESSION['."'query'".']($verifuser)) or die ("erro na query de verificacao do relacionamento vinculado ao usuÃƒÂ¡rio");'."\r\n");
      fwrite($cria, 'if($everifuser['."'result'".'] >= 1) {'."\r\n");
      fwrite($cria, '$select = $lselfiltros["id_"'.'.$combo'.'];'."\r\n");
      fwrite($cria, '$selndados = "SELECT *,count(*) as r FROM filtro_dados WHERE idfiltro_dados='."'".'$select'."' AND ativo='S'".'";'."\r\n");
      fwrite($cria, '$endados = $_SESSION['."'fetch_array'".']($_SESSION['."'query'".']($selndados)) or die ("erro na query de consulta dos nomes dos filtros_dados");'."\r\n");
      fwrite($cria, 'if(in_array($endados['."'nomefiltro_dados'".'],$filtrod) OR $endados['."'r'".'] == 0) {'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, 'else {'."\r\n");
      fwrite($cria, '$filtrod[$lselfiltros["id_"'.'.$combo'.']] = $endados['."'nomefiltro_dados'".'];'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, '}'."\r\n");
      fwrite($cria, '}'."\n\n");
      fwrite($cria, 'asort($filtrod);'."\n\n");
      fwrite($cria, 'foreach($filtrod as $idfilt => $filt) {'."\n");
      fwrite($cria, 'echo "<option value=\"".$idfilt."\" style=\"height:15px;padding:5px\">$filt</option>";'."\n");
      fwrite($cria, '}'."\n\n");
      fwrite($cria, '?>');
      fclose($cria);
  }
}

function comboplan() {
    $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
    $file = $rais."/monitoria_supervisao/users/comboplan_".strtolower($_SESSION['nomecli']).".php";
    if(file_exists($file)) {
      $del = unlink($file);
    }
    else {
    }
    $criacombo = fopen($rais.'/monitoria_supervisao/users/comboplan_'.strtolower($_SESSION['nomecli']).'.php', 'w');
    fwrite($criacombo, '<?php'."\r\n\n");
    fwrite($criacombo, '$rais = $_SERVER['."'DOCUMENT_ROOT'".'];'."\r\n");
    fwrite($criacombo, 'include_once($rais."/monitoria_supervisao/seguranca.php");'."\r\n");
    fwrite($criacombo, 'include_once($rais."/monitoria_supervisao/config/conexao.php");'."\r\n");
    fwrite($criacombo, 'include_once($rais."/monitoria_supervisao/selcli.php");'."\r\n\n");
    fwrite($criacombo, '$idfiltro = $_POST['."'filtro'".'];'."\r\n");
    fwrite($criacombo, '$campo = str_replace("filtro_","",$_POST['."'campo'".']);'."\r\n");
    fwrite($criacombo, '$combo = str_replace("filtro_","",$_POST['."'combo'".']);'."\r\n\n");
    fwrite($cria, '$colunasql = array();'."\r\n");
    fwrite($cria, '$valsql = array();'."\r\n");
    $self = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel";
    $eself = $_SESSION['query']($self) or die ("erro na query de seleÃƒÂ§ÃƒÂ£o dos nomes dos filtros");
    while($lself = $_SESSION['fetch_array']($eself)) {
        $filt = strtolower($lself['nomefiltro_nomes']);
        fwrite($criacombo, '$'.$filt.' = $_POST['."'".$filt."'".'];'."\r\n");
        fwrite($criacombo, "\n");
        fwrite($criacombo, 'if($'.$filt.' == "") {'."\r\n");
        fwrite($criacombo, '}'."\r\n");
        fwrite($criacombo, 'else {'."\r\n");
        fwrite($criacombo, 'if($idfiltro == $'.$filt.') {'."\r\n");
        fwrite($criacombo, '}'."\r\n");
        fwrite($criacombo, 'else {'."\r\n");
        fwrite($criacombo, '$colunasql[] = "'.$filt.'";'."\r\n");
        fwrite($criacombo, '$valsql[] = $'.$filt.';'."\r\n");
        fwrite($criacombo, '}'."\r\n");
        fwrite($criacombo, '}'."\r\n\n");
    }
    fwrite($criacombo, '$valsql = array_combine($colunasql, $valsql);'."\r\n");
    fwrite($criacombo, 'if($colunasql == "") {'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, 'else {'."\r\n");
    fwrite($criacombo, 'foreach($valsql as $key => $valor) {'."\r\n");
    fwrite($criacombo, '$wheresql[] = " AND id_$key=".'.'"'."'".'"'.'.'.'$valor'.'.'.'"'."'".'"'.';'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, '$wheresql = implode("",$wheresql);'."\r\n");
    fwrite($criacombo, '$selplan = "SELECT * FROM conf_rel INNER JOIN rel_filtros ON rel_filtros.idrel_filtros = conf_rel.idrel_filtros WHERE rel_filtros.id_$campo=');
    fwrite($criacombo, ''."'$".'idfiltro'."'".' $wheresql";'."\r\n");
    fwrite($criacombo, '$eselplan = $_SESSION['."'query'".']($selplan) or die ("erro na query de consulta das planilhas");'."\r\n");
    fwrite($criacombo, '$plans = array();'."\r\n");
    fwrite($criacombo, 'while($lselplan = $_SESSION['."'fetch_array'".']($eselplan)) {'."\r\n");
    fwrite($criacombo, '$plansweb = explode(",",$lselplan['."'idplanilha_web'".']);'."\r\n").
    fwrite($criacombo, 'foreach($plansweb as $plan) {'."\r\n");
    fwrite($criacombo, '$plans[] = $plan;'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, '$plans = array_unique($plans);'."\r\n\n");
    fwrite($criacombo, 'echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\" style=\"height:15px;padding:5px\">Selecione uma Planilha</option>";'."\r\n");
    fwrite($criacombo, 'if($plans == "") {'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, 'else {'."\r\n");
    fwrite($criacombo, 'foreach($plans as $p) {'."\r\n");
    fwrite($criacombo, '$selnplan = "SELECT descriplanilha FROM planilha WHERE idplanilha=');
    fwrite($criacombo, ''."'$".'p'."' AND ativo='S'".'";'."\r\n");
    fwrite($criacombo, '$eselplan = $_SESSION['."'fetch_array'".']($_SESSION['."'query'".']($selnplan)) or die ("erro na consulta do nome da planilha");'."\r\n");
    fwrite($criacombo, 'echo "<option value=\"".$p."\" style=\"height:15px;padding:5px\">".$eselplan['."'descriplanilha'".']."</option>";'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fwrite($criacombo, '}'."\r\n");
    fclose($criacombo);
}

function combojs() {
      $rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
      $cria = fopen($rais.'/monitoria_supervisao/users/combofiltros_'.strtolower($_SESSION['nomecli']).'.js', 'w');
      chmod($cria, 0666);
      fwrite($cria, '$(document).ready(function() {'."\r\n");
      fwrite($cria, '$("select[id*='."'filtro_'".']").change(function() {'."\r\n");
      $sfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel";
      $esfiltros = $_SESSION['query']($sfiltros) or die ("erro na query de consulta dos filtros");
      while($lselfiltros = $_SESSION['fetch_array']($esfiltros)) {
        $filtros[] = strtolower($lselfiltros['nomefiltro_nomes']).": ".strtolower($lselfiltros['nomefiltro_nomes']);
        $f = strtolower($lselfiltros['nomefiltro_nomes']);
        fwrite($cria, 'var '.$f.' = $('."'".'#filtro_'.$f."'".").val();"."\r\n");
      }
      fwrite($cria, 'var user = "<?php echo $_SESSION['."'usuarioID'".']; ?>";'."\r\n");
      fwrite($cria, 'var campo = $(this).attr("id");'."\r\n");
      fwrite($cria, 'var combo;'."\r\n");
      fwrite($cria, "\r\n");
      fwrite($cria, "$('#aval').html('<option disabled=".'"disabled"'." selected=".'"selected"'." value=".'"'.'"'." style=\"height:15px;padding:5px\">Selecione uma Avaliação...</option>');"."\r\n");
      fwrite($cria, "\r\n");
      fwrite($cria, "$('#grupo').html('<option disabled=".'"disabled"'." selected=".'"selected"'." value=".'"'.'"'." style=\"height:15px;padding:5px\">Selecione um Grupo...</option>');"."\r\n");
      fwrite($cria, "\r\n");
      fwrite($cria, "$('#sub').html('<option disabled=".'"disabled"'." selected=".'"selected"'." value=".'"'.'"'." style=\"height:15px;padding:5px\">Selecione um Sub-Grupo...</option>');"."\r\n");
      fwrite($cria, "\r\n");
      fwrite($cria, "$('#perg').html('<option disabled=".'"disabled"'." selected=".'"selected"'." value=".'"'.'"'." style=\"height:15px;padding:5px\">Selecione uma Pergunta...</option>');"."\r\n");
      fwrite($cria, "\r\n");
      fwrite($cria, "$('#resp').html('<option disabled=".'"disabled"'." selected=".'"selected"'." value=".'"'.'"'." style=\"height:15px;padding:5px\">Selecione uma Resposta...</option>');"."\r\n");
      fwrite($cria, "\r\n");
      $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel";
      $efiltros = $_SESSION['query']($selfiltros) or die ("erro na query de consulta dos fltros");
      while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
        $valor[] = "valor_".strtolower($lfiltros['nomefiltro_nomes']);
        $filtro = strtolower($lfiltros['nomefiltro_nomes']);
        fwrite($cria, 'if('.$filtro.' == "") {'."\r\n");
        fwrite($cria, 'if(campo == "filtro_'.$filtro.'") {'."\r\n");
        fwrite($cria, '}'."\r\n");
        fwrite($cria, 'else {'."\r\n");
        fwrite($cria, 'combo = '.'"'.$filtro.'"'.';'."\r\n");
        fwrite($cria, "$('#filtro_".$filtro."').");
        fwrite($cria, 'load("/monitoria_supervisao/users/combo'.$filtro.'_'.strtolower($_SESSION['nomecli']).'.php", {filtro:$(this).val(), combo: combo, campo: campo, '.implode(",",$filtros).', user: user});'."\r\n");
        fwrite($cria, '}'."\r\n");
        fwrite($cria, '}'."\r\n");
        fwrite($cria, 'else {'."\r\n");
        fwrite($cria, 'if(campo == "filtro_'.$filtro.'" || '.$filtro.' != "") {'."\r\n");
        fwrite($cria, '}'."\r\n");
        fwrite($cria, 'else {'."\r\n");
        fwrite($cria, 'combo = '.'"'.$filtro.'"'.';'."\r\n");
        fwrite($cria, "$('#filtro_".$filtro."').");
        fwrite($cria, 'load("/monitoria_supervisao/users/combo'.$filtro.'_'.strtolower($_SESSION['nomecli']).'.php", {filtro:$(this).val(), combo: combo, campo: campo, '.implode(",",$filtros).', user: user});'."\r\n");
        fwrite($cria, '}'."\r\n");
        fwrite($cria, '}'."\r\n\n");
      }
      fwrite($cria, "$('#indice').load(".'"'."/monitoria_supervisao/users/comboconfig.php".'"'.",{tpacao:'indice',origem:'filtro',filtro:$(this).val(),campo:campo,".implode(",",$filtros)."});\r\n");
      fwrite($cria, "$('#fluxo').load(".'"'."/monitoria_supervisao/users/comboconfig.php".'"'.",{tpacao:'fluxo',origem:'filtro',filtro:$(this).val(),campo:campo,".implode(",",$filtros)."});\r\n");
      fwrite($cria, "$('#plan').load(".'"/monitoria_supervisao/users/comboplan.php", {filtro:$(this).val(), combo: combo, campo: campo,'.implode(",",$filtros).", user: user});\r\n");
      fwrite($cria, '})'."\r\n");
      fwrite($cria, '})'."\r\n");
}

function InsertSemana($dtiniinter, $dtfiminter, $datafim, $idperiodo) {
    $semana = array('Sunday' => 'Domingo','Monday' => 'Segunda','Tuesday' => 'Terca','Wednesday' => 'Quarta','Thursday' => 'Quinta','Friday' => 'Sexta','Saturday' => 'Sabado');
    $dias = array('Segunda' => '1', 'Terca' => '2','Quarta' => '3','Quinta' => '4','Sexta' => '5','Sabado' => '6','Domingo' => '0');
   $i = 0;
   $sem = 1;
   for(;$i == 0;) {
       $inisem = date('Ymd',$dtiniinter);
       $fim = date('Ymd',$dtfiminter);
       $verifdata = "SELECT $inisem < $fim as result";
       $everifdata = $_SESSION['fetch_array']($_SESSION['query']($verifdata)) or die ("erro na query de comparaÃƒÂ§ÃƒÂ£o das datas");
       if($everifdata['result'] == 1) {
            $mesini = substr($inisem, 4,2);
            $diaini = substr($inisem, 6,2);
            $anoini = substr($inisem, 0,4);
            $diasemana = $dias[$semana[jddayofweek(cal_to_jd(CAL_GREGORIAN, $mesini, $diaini, $anoini), 1)]];
            $ultdiasem = 6;
            $contdias = $ultdiasem - $diasemana;
            for($i = 0; $i < $contdias; $i++) {
                if($dtiniinter <= $dtfiminter) {
                    $dtiniinter += 86400;
                }
                else {
                }
            }
            if($dtiniinter > $dtfiminter) {
                $fimsem = $fim;
            }
            else {
                $fimsem = date('Ymd',$dtiniinter);
            }
            $cadsem = "INSERT INTO interperiodo (idperiodo,dataini,datafim,semana) VALUES ('$idperiodo','$inisem','$fimsem','$sem')";
            $ecadsem = $_SESSION['query']($cadsem) or die ("erro na query de cadastramento da semana");
            $dtiniinter += 86400;
            $i = 0;
            $sem++;
       }
       else {
           $verifdata = "SELECT $inisem = $fim as result";
           $everifdata = $_SESSION['fetch_array']($_SESSION['query']($verifdata)) or die ("erro na query de comparaÃƒÂ§ÃƒÂ£o das datas");
           if($everifdata['result'] == 1) {
                $cadsem = "INSERT INTO interperiodo (idperiodo,dataini,datafim,semana) VALUES ('$idperiodo','$datafim','$datafim','$sem')";
                $ecadsem = $_SESSION['query']($cadsem) or die ("erro na query de cadastramento da semana");
                $i = 1;
           }
           else {
               $i = 1;
           }
           $sem++;
       }
   }
   return true;
}

function filtros() {
    $filt = "SELECT idfiltro_nomes,nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
    $efilt = $_SESSION['query']($filt) or die ("erro na query de consulta dos filtros cadastrados");
    while($lfiltros = $_SESSION['fetch_array']($efilt)) {
        if($lfiltros['nomefiltro_nomes'] == "") {
            $filtros = 0;
        }
        else {
            $filtros[$lfiltros['idfiltro_nomes']] = strtolower(trim($lfiltros['nomefiltro_nomes']));
        }
    }
    return $filtros;
}

function relfiltros() {
    $filt = "SELECT fn.nomefiltro_nomes FROM filtro_nomes fn
             INNER JOIN filtro_dados fd ON fd.idfiltro_nomes = fn.idfiltro_nomes
             WHERE fn.ativo='S' GROUP BY fn.idfiltro_nomes";
    $efiltros = $_SESSION['query']($filt) or die ("erro na query de consulta dos filtros cadastrados");
    while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
        if($lfiltros['nomefiltro_nomes'] == "") {
            $idfiltros = 0;
        }
        else {
            $idfiltros[] = trim("id_".strtolower(trim($lfiltros['nomefiltro_nomes'])));
        }
    }
    return $idfiltros;
}

function val_vazio($valor) {
    if($valor == "" OR $valor == "NULL") {
        $val = "NULL";
    }
    else {
        $val = "'".$valor."'";
    }
    return $val;
}

function null_vazio($valor) {
    if($valor == "NULL") {
        $val = "";
    }
    else {
        $val = $valor;
    }
    return $val;
}

function caracter_mes($mes) {
    if(strlen($mes) == 1) {
        $newmes = "0".$mes;
    }
    else {
        $newmes = $mes;
    }
    return $newmes;
}

function is_utf8($string) {

    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?: [x09x0Ax0Dx20-x7E] # ASCII
    | [xC2-xDF][x80-xBF] # non-overlong 2-byte
    | xE0[xA0-xBF][x80-xBF] # excluding overlongs
    | [xE1-xECxEExEF][x80-xBF]{2} # straight 3-byte
    | xED[x80-x9F][x80-xBF] # excluding surrogates
    | xF0[x90-xBF][x80-xBF]{2} # planes 1-3
    | [xF1-xF3][x80-xBF]{3} # planes 4-15
    | xF4[x80-x8F][x80-xBF]{2} # plane 16
    )*$%xs', $string);
}

function removeAcentos($string, $slug = false) {

    //Setamos o localidade
    setlocale(LC_ALL, 'pt_BR');

    //Verificamos se a string é UTF-8
    if (is_utf8($string))
        $string = utf8_decode($string);

    //Se a flag 'slug' for verdadeira, transformamos o texto para lowercase
    if ($slug)
        $string = strtolower($string);

    // Código ASCII das vogais
    $ascii['a'] = range(224, 230);
    $ascii['e'] = range(232, 235);
    $ascii['i'] = range(236, 239);
    $ascii['o'] = array_merge(range(242, 246), array(240, 248));
    $ascii['u'] = range(249, 252);

    // Código ASCII dos outros caracteres
    $ascii['b'] = array(223);
    $ascii['c'] = array(231);
    $ascii['d'] = array(208);
    $ascii['n'] = array(241);
    $ascii['y'] = array(253, 255);

    //Fazemos um loop para criar as regras de troca dos caracteres acentuados
    foreach ($ascii as $key => $item) {

        $acentos = '';
        foreach ($item AS $codigo)
            $acentos .= chr($codigo);
        $troca[$key] = '/[' . $acentos . ']/i';
    }

    //Aplicamos o replace com expressao regular
    $string = preg_replace(array_values($troca), array_keys($troca), $string);

        //Se a flag 'slug' for verdadeira...
    if ($slug) {

        //Troca tudo que não for letra ou número por um caractere ($slug)
        $string = preg_replace('/[^a-z0-9]/i', $slug, $string);

        //Tira os caracteres ($slug) repetidos
        $string = preg_replace('/' . $slug . '{2,}/i', $slug, $string);
        $string = trim($string, $slug);
    }

    return trim($string);
}

function lock ($tab,$tipo) {
    if($tipo == "") {
        $tipo = "WRITE";
    }
    else {
        $tipo = $tipo;
    }
    if(is_array($tab)) {
        $lock = "LOCK TABLES ".implode(" $tipo, ",$tab)." $tipo";
    }
    else {
        $lock = "LOCK TABLES $tab $tipo";
    }
    $elock = $_SESSION['query']($lock) or die ("erro na query para bloquear a tabela $tab");
}

function unlock ($tab) {
    $unlock = "UNLOCK TABLES";
    $enlock = $_SESSION['query']($unlock) or die ("erro na query para bloquear a tabela $tab");
}

function cortdrel($apres,$dado,$cor,$tipodado,$col) {
    if($apres == "TABELA" && ($dado == "MO" OR $dado == "MS" OR $dado == "F" OR $dado == "A" OR $dado == "IT") && $cor == "cor1" && $col != "descri") {
        return "bgcolor=\"#A2D0EB\"";
    }
    if($apres == "TABELA" && ($dado == "MO" OR $dado == "MS" OR $dado == "F" OR $dado == "A" OR $dado == "IT") && $cor == "cor1" && $col == "descri") {
        return "bgcolor=\"#A2D0EB\"";
    }
    if($apres == "TABELA" && ($dado == "MO" OR $dado == "MS" OR $dado == "F" OR $dado == "A" OR $dado == "IT") && $cor == "cor2" && $col != "descri") {
        return "bgcolor=\"#92B2EB\"";
    }
    if($apres == "TABELA" && ($dado == "MO" OR $dado == "MS" OR $dado == "F" OR $dado == "A" OR $dado == "IT") && $cor == "cor2" && $col == "descri") {
        return "bgcolor=\"#A2D0EB\"";
    }
    if($apres == "TABELA" && ($dado == "P" OR $dado == "PG" OR $dado == "PI" OR $dado == "TAB") && $cor == "cor1" && ($col != "descri" OR $col == "descri")) {
        if(eregi("aval",$tipodado)) {
            return "bgcolor=\"#61855F\"";
        }
        if(eregi("ggrupo",$tipodado)) {
            return "bgcolor=\"#EAAD7E\"";
        }
        if(eregi("subgrupo",$tipodado)) {
            return "bgcolor=\"#FFFF9C\"";
        }
        if(eregi("perg",$tipodado)) {
            return "bgcolor=\"#A3A3A3\"";
        }
        if(eregi("resp",$tipodado)) {
            return "bgcolor=\"#E0E0E0\"";
        }
    }
    if($apres == "TABELA" && ($dado == "P" OR $dado == "PG" OR $dado == "PI" OR $dado == "TAB") && $cor == "cor2" && $col != "descri") {
        if(eregi("aval",$tipodado)) {
            return "bgcolor=\"#476145\"";
        }
        if(eregi("ggrupo",$tipodado)) {
            return "bgcolor=\"#C4916A\"";
        }
        if(eregi("subgrupo",$tipodado)) {
            return "bgcolor=\"#D7DB4D\"";
        }
        if(eregi("perg",$tipodado)) {
            return "bgcolor=\"#C9C7C7\"";
        }
        if(eregi("resp",$tipodado)) {
            return "bgcolor=\"#F7F5F5\"";
        }
    }
}

function checkdatas($data1,$data2) {
    $datas = "SELECT '$data1' > '$data2' as data";
    $edatas = $_SESSION['fetch_array']($_SESSION['query']($datas)) or die ("erro na query de consulta das datas");
    if($edatas['data'] >= 1) {
        return false;
    }
    else {
        return true;
    }
}

function export($dados,$tipoarq) {
    if($tipo == "PDF") {
        $exp = "pdf";
    }
    if($tipo == "EXCEL") {
        $exp = "x-msexcel";
    }
    if($tipo == "WORD") {
        $exp = "x-msword";
    }
    $arquivo = $_GET['nome'];
    header ("Last-Modified: ".date('d/m/Y H:i:s')."");
    header ("Cache-Control: no-cache, must-revalidate");
    header ("Content-type: application/$exp");
    header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
    header ("Pragma: no-cache");
    header ("Expires: 0");

    echo $dados;
}

function arvoreescalar($tipomoni,$iduser) {
    // o id do usuário tem que estar acompanhado da tabela do usuário "user_adm,user_web,monitor;
    $tab = new TabelasSql();
    $self = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
    $eself = $_SESSION['query']($self) or die ("erro na query de consulta dos fitros cadastros");
    while($lself = $_SESSION['fetch_array']($eself)) {
        $fnomes[$lself['idfiltro_nomes']] = strtolower($lself['nomefiltro_nomes']);
    }
    foreach($fnomes as $idfnome => $nome) {
        $inners[] = "LEFT JOIN filtro_dados ".$tab->AliasTab(filtro_dados).$tab->AliasTab($nome)." ON ".$tab->AliasTab(filtro_dados).$tab->AliasTab($nome).".idfiltro_dados=rf.id_$nome";
        $order[] = $tab->AliasTab(filtro_dados).$tab->AliasTab($nome).".nomefiltro_dados";
    }
    if($tipomoni != "") {
        $inners[] = "INNER JOIN conf_rel ".$tab->Aliastab(conf_rel)." ON ".$tab->Aliastab(conf_rel).".idrel_filtros=rf.idrel_filtros INNER JOIN param_moni ".$tab->Aliastab(param_moni)." ON ".$tab->Aliastab(param_moni).".idparam_moni = ".$tab->Aliastab(conf_rel).".idparam_moni";
        $w[] = $tab->Aliastab(param_moni).".tipomoni='$tipomoni'";
    }
    else {
    }
    if($iduser != "") {
        $dadosu = explode("-",$iduser);
        $tabu = $dadosu[1];
        $id = $dadosu[0];
        $tabperfil = array('user_adm' => 'useradmfiltro','user_web' => 'userwebfiltro');
        $atabperfil = $tab->Aliastab($tabperfil[$tabu]);
        //$inners[] = "INNER JOIN conf_rel ".$tab->Aliastab(conf_rel)." ON ".$tab->Aliastab(conf_rel).".idrel_filtros=rf.idrel_filtros";
        $inners[] = "INNER JOIN $tabperfil[$tabu] $atabperfil ON  $atabperfil.idrel_filtros = rf.idrel_filtros";
        $w[] = $atabperfil.".id$tabu='$id'";
        //$w[] = $tab->AliasTab(conf_rel).".ativo='S'";
    }
    else {
    }
    if($w != "") {
        $where = "WHERE ".implode(" AND ",$w);
    }
    else {
        $where = "";
    }
    if($order == "") {
        $order = "";
    }
    else {
        $order = "ORDER BY ".implode(",",$order);
    }
    $selrel = "SELECT * FROM rel_filtros rf ".implode(" ",$inners)." $where $order";
    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relacionamento");
    $nrel = $_SESSION['num_rows']($eselrel);
    if($nrel == 0) {
        $idsrel = "";
    }
    else {
        while($lselrel = $_SESSION['fetch_array']($eselrel)) {
            $idsrel[] = $lselrel['idrel_filtros'];
        }
    }
    return $idsrel;
}

function getBrowser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

function atualizafluxo($usertab,$iduser) {
     // ataualização do fluxo automaticamente
    $ip = $_SERVER['REMOTE_ADDR'];
    $feriados = getFeriados(date('Y'));
    $periodo = periodo(datas);
    $dtm = date('m');
    $dta = date('Y');
    if($dtm == "01") {
        $dtatu = ($dta - 1)."-12-01";
    }
    else {
        $mes = array('02' => '01','03' => '02','04' => '03','05' => '04','06' => '05','07' => '06','08' => '07','09' => '08','10' => '09','11' => '10','12' =>'11');
        $dtatu = $dta."-".$mes[$dtm]."-01";
    }
    $dtini = substr($periodo['dataini'],0,4)."-".substr($periodo['dataini'],5,2)."-01";
    $dtfim = ultdiames(substr($dtini, 8,2).substr($dtini,5,2).substr($dtini,0,4));
      $tabs = array('fluxo','rel_fluxo','user_adm','atualiza_status','monitoria_fluxo','monitoria','monitoria m','monitoria_fluxo mf','rel_fluxo rf',
          'status','definicao');
      $selatu = "SELECT COUNT(*) as result FROM atualiza_status WHERE data='".date('Y-m-d')."'";
      $eselatu = $_SESSION['fetch_array']($_SESSION['query']($selatu)) or die ("erro na query de consulta se a ataulização de status já foi executada");
      if($eselatu['result'] == 0) {
          $atu = 0;
          $qtdeatu = 0;
          $selfulxo = "SELECT * FROM fluxo";
          $eselfluxo = $_SESSION['query']($selfulxo) or die ("erro na query de consulta do fluxo");
          $nfluxo = $_SESSION['num_rows']($eselfluxo);
          if($nfluxo >= 1) {
              while($lfluxo = $_SESSION['fetch_array']($eselfluxo)) {
                  $seldefini = "SELECT * FROM definicao WHERE idfluxo='".$lfluxo['idfluxo']."' AND tipodefinicao='automatico'";
                  $eseldefini = $_SESSION['query']($seldefini) or die ("erro na query de consulta da definicao automatica");
                  $ndefini = $_SESSION['num_rows']($eseldefini);
                  if($ndefini >= 1) {
                      while($lseldefini = $_SESSION['fetch_array']($eseldefini)) {
                          $selatu = "SELECT * FROM status WHERE iddefiniauto='".$lseldefini['iddefinicao']."'";
                          $eselatu = $_SESSION['query']($selatu) or die (mysql_error());
                          $natu = $_SESSION['num_rows']($eselatu);
                          if($natu >= 1) {
                              while($lselatu = $_SESSION['fetch_array']($eselatu)) {
                                  $dias = $lselatu['dias'];
                                  $selrel = "SELECT * FROM rel_fluxo WHERE idstatus='".$lselatu['idstatus']."' AND iddefinicao='".$lselatu['iddefiniauto']."' AND ativo='S'";
                                  $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relacionamento");
                                  $nrel = $_SESSION['num_rows']($eselrel);
                                  if($nrel >= 1) {
                                      $atu++;
                                      $lselrel = $_SESSION['fetch_array']($eselrel);
                                      $seluser = "SELECT * FROM user_adm WHERE nomeuser_adm='ADMIN'";
                                      $eseluser = $_SESSION['fetch_array']($_SESSION['query']($seluser));
                                      $useradm = $eseluser['iduser_adm'];
                                      $selmoni = "SELECT m.idmonitoria,mf.idmonitoria_fluxo, rf.idrel_fluxo, mf.data, ADDDATE(mf.data,1) as dtatu FROM monitoria m
                                                          INNER JOIN monitoria_fluxo mf ON mf.idmonitoria_fluxo = m.idmonitoria_fluxo
                                                          INNER JOIN rel_fluxo rf ON rf.idrel_fluxo = mf.idrel_fluxo
                                                          WHERE rf.idatustatus='".$lselatu['idstatus']."' AND datactt >= '$dtatu'";
                                      $eselmoni = $_SESSION['query']($selmoni) or die ("erro na query de consulta das monitorias para atualização de status");
                                      $nmoni = $_SESSION['num_rows']($eselmoni);
                                      if($nmoni >= 1) {
                                          while($lmoni = $_SESSION['fetch_array']($eselmoni)) {
                                              if(!in_array($lmoni['idmonitoria'],$idmonicheck)) {
                                                $dt = 0;
                                                $acres = 0;
                                                $datu = $lmoni['data'];
                                                if($lmoni['data'] == "2012-02-17") {
                                                    echo "OK";
                                                }
                                                $diaacres = "SELECT ADDDATE('$datu',$dias) as dias";
                                                $ediaacres = $_SESSION['fetch_array']($_SESSION['query']($diaacres)) or die ("erro na query de consulta para acrescentar dias");
                                                $vdatu = $datu;
                                                $datu = $ediaacres['dias'];
                                                for(;$dt == 0;) {
                                                    $diasem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($vdatu,5,2), substr($vdatu,8,2), substr($vdatu,0,4)));
                                                    if($diasem == "6" OR $diasem == "0") {
                                                        $acres++;
                                                    }
                                                    else {
                                                        if(in_array(substr($vdatu,5,2)."-".substr($vdatu,8,2),$feriados)) {
                                                            $acres++;
                                                        }
                                                    }
                                                    $acresdt = "SELECT ADDDATE('$vdatu',1) as acres, ADDDATE('$vdatu',1) > '".date('Y-m-d')."' as termina";
                                                    $eacresdt = $_SESSION['fetch_array']($_SESSION['query']($acresdt)) or die ("erro na query para acrescentar dias");
                                                    $vdatu = $eacresdt['acres'];
                                                    if($eacresdt['termina'] >= 1) {
                                                        $dt = 1;
                                                    }
                                                    else {
                                                        $dt = 0;
                                                    }
                                                }
                                                if($acres > 0) {
                                                    $newdt = "SELECT ADDDATE('$datu',$acres) as acres";
                                                    $enewdt = $_SESSION['fetch_array']($_SESSION['query']($newdt)) or die ("erro na query de atualização da data");
                                                    $datu = $enewdt['acres'];
                                                }
                                                $diffdate = "SELECT ('$datu' < '".date('Y-m-d')."') as result";
                                                $ediffdate = $_SESSION['fetch_array']($_SESSION['query']($diffdate)) or die ("erro na query de comparação das datas");
                                                if($ediffdate['result'] >= 1) {
                                                    $idmonicheck[] = $lmoni['idmonitoria'];
                                                    $qtdeatu++;
                                                    $idmoni = $lmoni['idmonitoria'];
                                                    $idrelfluxo = $lselrel['idrel_fluxo'];
                                                    $idstatus = $lselrel['idstatus'];
                                                    $iddefini = $lselrel['iddefinicao'];
                                                    $data = date('Y-m-d');
                                                    $hora = date('H:i:s');
                                                    $obs = "Atualização de status automático por prazo vencido!!!";
                                                    $insert = "INSERT INTO monitoria_fluxo (idmonitoria,iduser,tabuser,idrel_fluxo,idstatus,iddefinicao,data,hora,obs)
                                                               VALUES ('$idmoni','$useradm','user_adm','$idrelfluxo','$idstatus','$iddefini','$data','$hora','$obs')";
                                                    $einsert = $_SESSION['query']($insert) or die ("erro na query de inserção do fluxo para monitoria");
                                                    $idinsert = $_SESSION['insert_id']();
                                                    $upmoni = "UPDATE monitoria SET idmonitoria_fluxo='$idinsert' WHERE idmonitoria='$idmoni'";
                                                    $eupmoni = $_SESSION['query']($upmoni) or die ("erro na query de inserção do fluxo na tabela monitoria");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
          }
          //echo $qtdeatu;
          if($atu >= 1) {
              $iatu = "INSERT INTO atualiza_status (data,hora,tab_user,iduser,qtdeatu) VALUES ('".date('Y-m-d')."','".date('H:i:s')."','$usertab','$iduser','$qtdeatu')";
              $eiatu = $_SESSION['query']($iatu) or die (mysql_error());
          }
      }
      // fim da avaliação do fluxo
}

class ArvoreTab {
    public $continua;
    public $idproxperg;
    public $idtab;
    public $idresp;
    public $idsproxperg;

    function arvore_tab($idtab,$idprox) {
        $selprox = "SELECT t.idperguntatab,pt.descriperguntatab,t.idrespostatab,rt.descrirespostatab,t.idproxpergunta FROM tabulacao t
                    INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                    INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab
                    WHERE t.idtabulacao='$idtab' AND t.idperguntatab='$idprox'";
        $eselprox = $_SESSION['query']($selprox) or die ("erro na query de consulta da pergunta na tabulaÃƒÂ§ÃƒÂ£o");
        $nprox =  $_SESSION['num_rows']($eselprox);
        if($nprox == 0) {
            echo "<li>INCONSISTENCIA - A proxima pergunta nÃƒÂ£o estÃƒÂ¡ relacionada na tabulaÃƒÂ§ÃƒÂ£o</li>";
            $this->continua = 0;
        }
        else {
            while($lselprox = $_SESSION['fetch_array']($eselprox)) {
                $this->idproxperg = $lselprox['idproxpergunta'];
                $this->idtab = $idtab;
                if($lselprox['idperguntatab'] != $idperg) {
                    echo "<li class=\"perg".$lselprox['idperguntatab']."\"><strong>".$lselprox['descriperguntatab']."</strong>";
                }
                else {

                }
                if($lselprox['idproxpergunta'] != "") {
                        $this->continua = 1;
                        echo "<ul>";
                            echo "<li class=\"resp".$lselprox['idperguntatab']."\">".$lselprox['descrirespostatab']."</li>";
                        break;
                }
                else {
                    if($lselprox['idperguntatab'] == $idperg) {
                    }
                    else {
                        echo "<ul>";
                    }
                    echo "<li class=\"resp".$lselprox['idperguntatab']."\">".$lselprox['descrirespostatab']."</li>";
                    $this->continua = 0;
                    if($lselprox['idperguntatab'] == $idperg) {
                    }
                    else {
                        echo "</ul>";
                    }
                }
                $idperg = $lselprox['idperguntatab'];
            }
            echo "</ul>";
        }
    }

    function arvore_moni($idmoni,$tipostatus,$idtab,$idperg,$idprox,$idresp,$tab,$tabmoni)  {
        $this->idsproxperg[] = $idprox;
        $selprox = "SELECT t.idperguntatab,pt.descriperguntatab,t.idproxpergunta FROM tabulacao t
                          INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab
                         WHERE t.idtabulacao='$idtab' AND t.idperguntatab='$idprox' GROUP BY t.idperguntatab";
        $eselprox = $_SESSION['query']($selprox) or die ("erro na query de consulta da pergunta na tabulaÃƒÂ§ÃƒÂ£o");
        $nprox =  $_SESSION['num_rows']($eselprox);
        if($nprox == 0) {
            echo "<tr class=\"idpergtab".$eselprox['idperguntatab']."\">";
                echo "<td>INCONSISTENCIA - A proxima pergunta não está relacionada na tabulação</td>";
            echo "</td>";
            $this->continua = 0;
        }
        else {
            $loop = 0;
            while($lselprox = $_SESSION['fetch_array']($eselprox)) {
                if($tab == "") {
                    $verifresp = "SELECT *, COUNT(*) as result FROM monitabulacao WHERE idmonitoria='$idmoni' AND idtabulacao='$idtab' AND idperguntatab='".$lselprox['idperguntatab']."'";
                }
                else {
                    $verifresp = "SELECT *, COUNT(*) as result FROM $tab WHERE id$tabmoni='$idmoni' AND idtabulacao='$idtab' AND idperguntatab='".$lselprox['idperguntatab']."'";
                }
                $everifresp = $_SESSION['fetch_array']($_SESSION['query']($verifresp)) or die ("erro na query de consulta da resposta da tabulacao");

                ?>
                    <tr class="idpergtabprox<?php echo $idperg."_".$idresp;?>">
                        <td style="text-align:left; background-color:#69C"><strong><?php echo $lselprox['descriperguntatab'];?></strong></td>
                    </tr>
                    <tr class="idresptabprox_<?php echo $idperg."_".$idresp;?>">
                        <td>
                            <?php
                            if($tipostatus != "editar" && $tipostatus != "") {
                                $selresp = "SELECT t.idrespostatab, rt.descrirespostatab FROM tabulacao t
                                            INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab
                                            WHERE idperguntatab='".$lselprox['idperguntatab']."' AND idtabulacao='$idtab'";
                                $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na query de consulta das respostas cadastradas para a pergunta");
                                ?>
                                <input style="width:930px; border: 0px"name="idpergtab<?php echo $lselprox['idperguntatab']."_".$idtab;?>" id="idpergtab<?php echo $lselprox['idperguntatab']."_".$idtab;?>" value="<?php echo $eselresp['descrirespostatab'];?>" />
                                <?php
                            }
                            else {
                                ?>
                                <select name="idpergtab<?php echo $lselprox['idperguntatab']."_".$idtab;?>" id="idpergtab<?php echo $lselprox['idperguntatab']."_".$idtab;?>" style="width:930px">
                                    <option value=""></option>
                                   <?php
                                    $selresp = "SELECT t.idrespostatab, rt.descrirespostatab FROM tabulacao t
                                                INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab
                                                WHERE idperguntatab='".$lselprox['idperguntatab']."' AND idtabulacao='$idtab'";
                                    $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas cadastradas para a pergunta");
                                    while($lselresp = $_SESSION['fetch_array']($eselresp)) {
                                        if($tipostatus == "editar") {
                                            if($lselresp['idrespostatab'] == $everifresp['idrespostatab']) {
                                                ?>
                                                <option value="<?php echo $lselresp['idrespostatab'];?>" selected="selected"><?php echo $lselresp['descrirespostatab'];?></option>
                                                <?php
                                            }
                                            else {
                                                ?>
                                                <option value="<?php echo $lselresp['idrespostatab'];?>"><?php echo $lselresp['descrirespostatab'];?></option>
                                                <?php
                                            }
                                        }
                                        else {
                                            ?>
                                            <option value="<?php echo $lselresp['idrespostatab'];?>"><?php echo $lselresp['descrirespostatab'];?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            <?php
                            }
                            ?>
                        </td>
                    </tr>
                <?php
                $newprox = "SELECT idproxpergunta, idperguntatab, idrespostatab, COUNT(*) as result FROM tabulacao WHERE idtabulacao='$idtab' AND idperguntatab='$idprox' AND idproxpergunta<>''";
                $enewprox = $_SESSION['fetch_array']($_SESSION['query']($newprox)) or die ("erro na query para consultar se a pergunta tem prox pergunta");
                if($enewprox['result'] >= 1) {
                    $this->continua = 1;
                    $this->idproxperg = $enewprox['idproxpergunta'];
                    $this->idperg = $enewprox['idperguntatab'];
                    $this->idsproxperg[] = $enewprox['idproxpergunta'];
                    $this->idresp = $enewprox['idrespostatab'];
                }
                else {
                    $this->continua = 0;
                }
            }
        }
    }
}

?>
