<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");
include_once($rais."/monitoria_supervisao/classes/class.corsistema.php");
include_once($rais."/monitoria_supervisao/classes/class.tabelas.php");

$idperiodo = $_GET['idperiodo'];
$cor = new CoresSistema();

protegePagina();

$tipo = "SISTEMA";
$cor->Cores($_SESSION['selbanco'],$_SESSION['idcli'],$tipo);
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CADASTRO SEMEANAS</title>
        <link href="../styleadmin.css" rel="stylesheet" type="text/css" />
        <script src="/monitoria_supervisao/js/jquery-1.4.4.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {

            })       
        </script>
    </head>
    <body style="font:Verdana, Geneva, sans-serif; font-size:10px;">
        <table width="300" style="background-color:#EFEFEF; border: 1px solid #999">
            <tr align="center">
                <td colspan="3" class="corfd_ntab"><strong>SEMANAS PERÃƒï¿½ODO</strong></td>
            </tr>
            <tr>
                <td class="corfd_coltexto" align="center"><strong>ID</strong></td>
                <td class="corfd_coltexto" align="center"><strong>Data Ini</strong></td>
                <td class="corfd_coltexto" align="center"><strong>Data Fim</strong></td>
                <td></td>
            </tr>
        <?php
        $selinter = "SELECT * FROM interperiodo WHERE idperiodo='$idperiodo'";
        $eselinter = $_SESSION['query']($selinter) or die ("erro na query de consulta dos intervalos cadastrados para o periodo");
        while($lselinter = $_SESSION['fetch_array']($eselinter)) {
            ?>
            <form action="cadcalendario.php" method="POST">
                <tr>
                    <td align="center" class="corfd_colcampos"><input type="hidden" name="idperiodo" id="idperiodo" value="<?php echo $idperiodo;?>" /><input type="text" style="width:40px; border: 1px solid #999; text-align:center" readonly="readonly" name="idinter" id="idinter" value="<?php echo $lselinter['idinterperiodo'];?>" /></td>
                    <td align="center" class="corfd_colcampos"><input type="text" style="width:80px; border: 1px solid #999; text-align:center" name="dataini" id="dataini" value="<?php echo banco2data($lselinter['dataini']);?>" /></td>
                    <td align="center" class="corfd_colcampos"><input type="text" style="width:80px; border: 1px solid #999; text-align:center" name="datafim" id="datafim" value="<?php echo banco2data($lselinter['datafim']);?>" /></td>
                    <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="atuinter" type="submit" value="Alterar" /></td>
                </tr>
           </form>
            <?php
        }
        ?>
        </table>
        <font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
    </body>
</html>
