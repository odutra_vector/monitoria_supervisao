<?php

session_start();
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once('functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once($rais.'/monitoria_supervisao/classes/excel_reader2.php');

$tabsql = new TabelasSql();

if(isset($_POST['exclui'])) {
    $cam = $_POST['caminho'];
    $arquivo = $rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']."/$cam";
    if(unlink($arquivo)) {
        echo 1;
    }
    else {
        echo 0;
    }
}

if(isset($_POST['enviar'])) {
    $idrel = $_POST['idrelselecao'];
    $url = $_POST['envfiltros'];
    $arquivo = $_FILES['arquivo']['tmp_name'];
    $tamanho = $_FILES['arquivo']['size'];
    $tipo = $_FILES['arquivo']['type'];
    $nomearq = $idrel."-".date('dmY')."-".date("His").".".pathinfo($_FILES['arquivo']['name'],PATHINFO_EXTENSION);
    $seltipoarq = "SELECT * FROM param_moni pm
                   INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                   WHERE idrel_filtros='$idrel'";
    $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipoarq)) or die (mysql_error());
    $tipoarqdb = array("txt","csv","xls");
    foreach(mimes() as $mime) {
        $quebra = explode(",",$mime);
        if(in_array($quebra[1], $tipoarqdb)) {
            $permitidos[] = $quebra[0];
        }
    }
    $selcol = "SELECT nomecoluna FROM coluna_oper co
               INNER JOIN camposparam cp ON cp.idcoluna_oper = co.idcoluna_oper
               INNER JOIN param_moni pm ON pm.idparam_moni = cp.idparam_moni
               INNER JOIN conf_rel cf ON cf.idparam_moni = pm.idparam_moni
               WHERE idrel_filtros='$idrel' ORDER BY posicao";
    $eselcol = $_SESSION['query']($selcol) or die (mysql_error());
    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
        $confcol[] = $lselcol['nomecoluna'];
    }
    //$confcol[] = "tempo";
    $falta = 0;
    $feriados = getFeriados(date('Y'));
    if($tamanho <= "5000000") {
        if(in_array($tipo, $permitidos)) {
            $selfiltros = "SELECT nomefiltro_nomes as nome FROM filtro_nomes ORDER BY nivel DESC";
            $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
            while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                $filtros[] = $lfiltros['nome'];
            }

            $caminho = $rais."/monitoria_supervisao/basesaleatoria/".$_SESSION['nomecli'];
            mkdir($caminho,0777);
            $selrel = "SELECT * FROM rel_filtros WHERE idrel_filtros='$idrel'";
            $eselrel = $_SESSION['query']($selrel) or die (mysql_error());
            $nselrel = $_SESSION['num_rows']($eselrel);
            if($nselrel >= 1) {
                while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                    foreach($filtros as $col) {
                        $selnome = "SELECT nomefiltro_dados as nome FROM filtro_dados WHERE idfiltro_dados='".$lselrel['id_'.strtolower($col)]."'";
                        $eselnome = $_SESSION['query']($selnome) or die (mysql_error());
                        $nselnome = $_SESSION['num_rows']($eselnome);
                        if($nselnome >= 1) {
                            while($lnome = $_SESSION['fetch_array']($eselnome)) {
                                $caminho .= "/".$lnome['nome'];
                                if(!file_exists($caminho)) {
                                    mkdir($caminho, 0777);
                                }
                            }
                        }
                    }
                }
            }
            
            $dia = date('Y-m-d',mktime(0,0,0,date('m'),date('d')-1,date('Y')));
            $diasem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($dia,5,2),substr($dia,8,2),substr($dia,0,4)));
            if($diasem == 6) {
                $dia = date('Y-m-d',mktime(0,0,0,substr($dia,5,2),substr($dia,8,2)-1,substr($dia,0,4)));
            }
            if($diasem == 0) {
                $dia = date('Y-m-d',mktime(0,0,0,substr($dia,5,2),substr($dia,8,2)-2,substr($dia,0,4)));
            }
            $primeiro = primeirodiautil();
            
            if(in_array(date("Y-m-d"),$primeiro)) {
                if(date('m') == "01") {
                    $pridiamesant = date('Y-m-d',mktime (0, 0, 0, "12", "01", date('Y')-1));
                }
                else {
                    $pridiamesant = date('Y-m-d',mktime (0, 0, 0, date('m')-1 , "01", date('Y')));
                }
                $ultdiamesant = ultdiames(substr($pridiamesant,8,2).substr($pridiamesant, 5,2).substr($pridiamesant,0,4));
                $selperiodo = "SELECT *,count(*) as result FROM periodo WHERE '$ultdiamesant' between dtinictt and dtfimctt";
                $eselper = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_errno());
                if($eselper['dtinictt'] != "" && $eselper['dtfimctt'] != "") {
                    $dtinictt = $eselper['dtinictt'];
                    $dtfimctt = $eselper['dtfimctt'];
                }
                else {
                    $dtinictt = $pridiamesant;
                    $dtfimctt = $ultdiamesant;
                }
                $dtinitrab = $eselper['dataini'];
                $dtfimtrab = $eselper['datafim'];
                $idperiodo = $eselper['idperiodo'];
                $diasprod = $eselper['diasprod'];
            }
            else {
                $selperiodo = "SELECT *,count(*) as result FROM periodo WHERE '$dia' between dtinictt and dtfimctt";
                $eselper = $_SESSION['fetch_array']($_SESSION['query']($selperiodo)) or die (mysql_errno());
                if($eselper['dtinictt'] != "" && $eselper['dtfimctt'] != "") {
                    $dtinictt = $eselper['dtinictt'];
                    $dtfimctt = $eselper['dtfimctt'];
                }
                else {
                    $dtinictt = date('Y')."-".date('m')."-01";
                    $dtfimctt = ultdiames(substr($dtinictt,8,2).substr($dtinictt,5,2).substr($dtinictt,0,4));
                }
                $dtinitrab = $eselper['dataini'];
                $dtfimtrab = $eselper['datafim'];
                $idperiodo = $eselper['idperiodo'];
                $diasprod = $eselper['diasprod'];
            }
            
            //criar semanas baseado na datactt
            $diaini = $dtinictt;
            $loop = 0;
            $s = 1;
            for(;$loop == 0;) {
                $checkdia = "SELECT '$diaini' <= '$dtfimctt' as d";
                $echeckdia = $_SESSION['fetch_array']($_SESSION['query']($checkdia)) or die (mysql_error());
                if($echeckdia['d'] == 0) {
                    break;
                }
                else {
                    $semanas[$s]['dtini'] = $diaini;
                    for($i = 1; $i < 7;$i++) {
                        $checkdia = "SELECT '$diaini' <= '$dtfimctt' as d";
                        $echeckdia = $_SESSION['fetch_array']($_SESSION['query']($checkdia)) or die (mysql_error());
                        if($echeckdia['d'] == 0) {
                            $loop = 1;
                            break;
                        }
                        else {
                            $incre = "SELECT ADDDATE('$diaini',+1) as dia";
                            $eincre = $_SESSION['fetch_array']($_SESSION['query']($incre)) or die (mysql_error());
                            $diaini = $eincre['dia'];
                        }
                    }
                    $semanas[$s]['dtfim'] = $diaini;
                    $incre = "SELECT ADDDATE('$diaini',+1) as dia";
                    $eincre = $_SESSION['fetch_array']($_SESSION['query']($incre)) or die (mysql_error());
                    $diaini = $eincre['dia'];
                    $s++;
                }
            }            
            
            if($eselper['result'] >= 1) {
                $caminho .= "/".$eselper['nmes']."_".$eselper['ano'];
                if(!file_exists($caminho)) {
                    mkdir($caminho, 0777);
                }
                if(move_uploaded_file($arquivo, $caminho."/".$nomearq)) {
                    if(pathinfo($_FILES['arquivo']['name'],PATHINFO_EXTENSION) == "xls") {
                        $data = new Spreadsheet_Excel_Reader($caminho."/".$nomearq);
                        $ncol = $data->sheets[0]['cells'][1];
                        foreach($ncol as $n) {
                            $ncolunas[] = strtolower($n);
                        }
                        $colunas = count($data->sheets[0]['cells'][1]);
                        $linhas = count($data->sheets[0]['cells']);
                    }
                    else {
                        $data = file($caminho."/".$nomearq, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                        $ncol = explode(";",$data[0]);
                        foreach($ncol as $n) {
                            $ncolunas[] = strtolower($n);
                        }
                        $linhas = count($data);
                    }
                    foreach($confcol as $conf) {
                        if(!in_array($conf, $ncolunas)) {
                            $colfalta[] = $conf;
                            $falta++;
                        }
                    }
                    if($falta == 0) {
                        foreach($ncolunas as $ncol => $colarq) {
                            if($colarq == "datactt" or $colarq == "DATACTT") {
                                $coldata[$ncol] = $colarq;
                                $createcol[] = $colarq." DATE DEFAULT NULL";
                            }
                            else {
                                if($colarq == "horactt" || $colarq == "HOTACTT") {
                                    $hora[$ncol] = $colarq;
                                    $createcol[] = $colarq." TIME DEFAULT NULL";
                                }
                                else {
                                    if($colarq == "tempo" || $colarq == "TEMPO") {
                                        $hora[$ncol] = $colarq;
                                        $createcol[] = $colarq." TIME DEFAULT NULL";
                                    }
                                    else {
                                        $createcol[] = utf8_decode($colarq)." varchar(500) DEFAULT NULL";
                                    }
                                }
                            }
                        }
                        $tabletmp = "CREATE TEMPORARY TABLE tmpselecao (idtmpselecao int(7) unsigned zerofill NOT NULL AUTO_INCREMENT,".implode(",",$createcol).",PRIMARY KEY (`idtmpselecao`)) ENGINE=MEMORY";
                        $etemp = $_SESSION['query']($tabletmp) or die (mysql_error());
                        $i = 0;
                        $exporte = "";
                        $nomelog = "log-".$idrel."-".date('dmY')."-".date('His').".csv";
                        $flog = fopen("$caminho/$nomelog", "w");
                        $a = 0;
                        if(pathinfo($_FILES['arquivo']['name'],PATHINFO_EXTENSION) == "xls") {
                            foreach($data->sheets[0]['cells'] as $ld => $d) {
                                $checknome = array();
                                $insert = array();
                                $whereext = array();
                                $obs = array();
                                $erro = 0;
                                if($ld != 1) {
                                    $linha = $d;
                                    foreach ($ncolunas as $keycol => $colsql) {
                                        if(in_array($colsql,$coldata)) {
                                            if(verifdata($linha[$keycol+1]) && strstr($linha[$keycol+1],"/") && strlen($linha[$keycol+1]) == 10) {
                                                if(in_array($colsql,$confcol)) {
                                                    $checknome[array_search($colsql, $confcol)] = trim(str_replace("/","",$linha[$keycol+1]));
                                                    if($linha[$keycol+1] == "") {
                                                        $erro++;
                                                    }
                                                }
                                                $insert[$colsql] = val_vazio(data2banco(trim($linha[$keycol+1])));
                                            }
                                            else {
                                                if($linha[$keycol+1] == "") {
                                                    $erro++;
                                                }
                                                else {
                                                    $obs[] = "Data com formato inválido";
                                                }
                                            }
                                        }
                                        else {
                                            if(in_array($colsql,$hora)) {
                                                if(checkhora($linha[$keycol+1])) {
                                                    if(in_array($colsql,$confcol)) {
                                                        $checknome[array_search($colsql, $confcol)] = trim($linha[$keycol+1]);
                                                        if($linha[$keycol+1] == "") {
                                                            $erro++;
                                                        }
                                                    }
                                                    $insert[$colsql] = val_vazio(trim($linha[$keycol+1]));
                                                }
                                                else {
                                                    if($linha[$keycol+1] == "") {
                                                        $erro++;
                                                    }
                                                    else {
                                                        $obs[] = "Hora no formato inválido";
                                                    }
                                                }
                                            }
                                            else {
                                                if(in_array($colsql,$confcol)) {
                                                    $checknome[array_search($colsql, $confcol)] = utf8_decode(trim($linha[$keycol+1]));
                                                    if($linha[$keycol+1] == "") {
                                                        $erro++;
                                                    }
                                                }
                                                $insert[$colsql] = val_vazio(mysql_real_escape_string(utf8_decode(trim($linha[$keycol+1]))));
                                            }
                                        }
                                    }
                                    ksort($checknome);
                                    foreach(explode(",",$eseltipo['tpaudio']) as $ext) {
                                        $whereext[] = "narquivo = '".implode("#",$checknome).".$ext'";
                                    }
                                    $whereext = "WHERE ".implode(" OR ",$whereext);
                                    $sqlcheck = "SELECT COUNT(*) as result FROM fila_grava $whereext";
                                    $esqlcheck = $_SESSION['fetch_array']($_SESSION['query']($sqlcheck)) or die (mysql_error());
                                    if($esqlcheck['result'] >= 1) {
                                        $obs[] = "Gravação já existe na base";
                                    }
                                    if(count($ncolunas) == count($insert)) {
                                        if($erro >= 1) {
                                            $obs[] = "Erro, existem valores obrigatórios em branco";
                                        }
                                        else {
                                            if(!in_array(str_replace("'", "", $insert['operador']), $operadores)) {
                                                $operadores[] = str_replace("'", "", $insert['operador']);
                                            }
                                            $nomearquivo = trim(implode("#",$checknome));
                                            $selselecao = "SELECT selecionado,count(*) as result FROM selecao WHERE nomearquivo='$nomearquivo'";
                                            $eselecao = $_SESSION['fetch_array']($_SESSION['query']($selselecao)) or die (mysql_error());
                                            if($eselecao['result'] == "1") {
                                                $obs[] = "Audio já enviado";
                                                $a++;
                                            }
                                            else {
                                                $inserts[] = "(".implode(",",$insert).")";
                                                /**if($eselecao['result'] == "0") {
                                                    //$upselecao = "INSERT INTO selecao (idrel_filtros,idupselecao,operador,datactt,horactt,nomearquivo,selecionado,enviado) VALUES('$idrel',NULL,".$insert['operador'].",".$insert['datactt'].",".$insert['horactt'].",'$nomearquivo',0,'0')";
                                                    //$eupselecao = $_SESSION['query']($upselecao) or die (mysql_error());
                                                //}
                                                $sqlinsert = "INSERT INTO tmpselecao (".implode(",",$ncolunas).") VALUES (".implode(",",$insert).")";
                                                $einsert = $_SESSION['query']($sqlinsert) or die (mysql_error());
                                                if($einsert) {
                                                    $i++;
                                                    $arraysql[] = $sqlinsert;
                                                }**/
                                            }
                                        }
                                    }
                                    else {
                                        if($erro >= 1) {
                                            $obs[] = "Erro, existem valores obrigatórios em branco";
                                        }
                                    }
                                    $linhalog = implode(";",$d).";".implode("-",$obs);
                                    fwrite($flog,$linhalog."\n");
                                }
                                else {
                                    fwrite($flog, implode(";",$d).";obs\n");
                                }
                            }
                            if(count($inserts) > 0) {
                                $sqlinsert = "INSERT INTO tmpselecao (".implode(",",$ncolunas).") VALUES ".implode(",",$inserts);
                                $einsert = $_SESSION['query']($sqlinsert) or die (mysql_error());
                            }
                        }
                        else {
                            foreach($data as $ld => $d) {
                                $erro = 0;
                                $insert = array();
                                $checknome = array();
                                $whereext = array();
                                $obs = array();
                                if($ld != 0) {
                                    $linha = explode(";",$d);
                                    foreach ($ncolunas as $keycol => $colsql) {
                                        if(in_array($colsql,$coldata)) {
                                            if(verifdata($linha[$keycol]) && strstr($linha[$keycol],"/") && strlen($linha[$keycol]) == 10) {
                                                if(in_array($colsql,$confcol)) {
                                                    $checknome[array_search($colsql, $confcol)] = trim(str_replace("/","",$linha[$keycol]));
                                                    if($linha[$keycol] == "") {
                                                        $erro++;
                                                    }
                                                }
                                                $insert[$colsql] = val_vazio(data2banco(trim($linha[$keycol])));
                                            }
                                            else {
                                                if($linha[$keycol] == "") {
                                                    $erro++;
                                                }
                                                else {
                                                    $obs[] = "Data com formato inválido";
                                                }
                                            }
                                        }
                                        else {
                                            if(in_array($colsql,$hora)) {
                                                if(checkhora($linha[$keycol])) {
                                                    if(in_array($colsql,$confcol)) {
                                                        $checknome[array_search($colsql, $confcol)] = trim($linha[$keycol]);
                                                        if($linha[$keycol] == "") {
                                                            $erro++;
                                                        }
                                                    }
                                                    $insert[$colsql] = val_vazio(trim($linha[$keycol]));
                                                }
                                                else {
                                                    if($linha[$keycol] == "") {
                                                        $erro++;
                                                    }
                                                    else {
                                                        $obs[] = "Hora no formato inválido";
                                                    }
                                                }
                                            }
                                            else {
                                                if(in_array($colsql,$confcol)) {
                                                    $checknome[array_search($colsql, $confcol)] = utf8_decode(trim($linha[$keycol]));
                                                    if($linha[$keycol] == "") {
                                                        $erro++;
                                                    }
                                                }
                                                $insert[$colsql] = val_vazio(mysql_real_escape_string(utf8_decode(trim($linha[$keycol]))));
                                            }
                                        }
                                    }
                                    ksort($checknome);
                                    foreach(explode(",",$eseltipo['tpaudio']) as $ext) {
                                        $whereext[] = "narquivo = '".implode("#",$checknome).".$ext'";
                                    }
                                    $whereext = "WHERE ".implode(" OR ",$whereext);
                                    $sqlcheck = "SELECT COUNT(*) as result FROM fila_grava $whereext";
                                    $esqlcheck = $_SESSION['fetch_array']($_SESSION['query']($sqlcheck)) or die (mysql_error());
                                    if($esqlcheck['result'] >= 1) {
                                        $obs[] = "Gravação já existe na base";
                                    }
                                    if(count($ncolunas) == count($insert)) {
                                        if($erro >= 1) {
                                            $obs[] = "Erro, existem valores obrigatórios em branco";
                                        }
                                        else {
                                            if(!in_array(str_replace("'", "", $insert['operador']), $operadores)) {
                                                $operadores[] = str_replace("'", "", $insert['operador']);
                                            }
                                            $nomearquivo = trim(implode("#",$checknome));
                                            $selselecao = "SELECT selecionado,count(*) as result FROM selecao WHERE nomearquivo='".implode("#",$checknome)."'";
                                            $eselecao = $_SESSION['fetch_array']($_SESSION['query']($selselecao)) or die (mysql_error());
                                            if($eselecao['result'] == "1") {
                                                $obs[] = "Audio já enviado e selecionado";
                                                $a++;
                                            }
                                            else {
                                                $inserts[] = "(".implode(",",$insert).")";
                                                /**if($eselecao['count'] == "0") {
                                                    //$upselecao = "INSET INTO selecao (idrel_filtros,idupselecaom,operador,datactt,horactt,nomearquivo,selecionado,enviado) VALUES('$idrel',NULL,".$insert['operador'].",".$insert['datactt'].",".$insert['horactt'].",'$nomearquivo',0,'0')";
                                                    //$eupselecao = $_SESSION['query']($upselecao) or die (mysql_error());
                                                //}
                                                $sqlinsert = "INSERT INTO tmpselecao (".implode(",",$ncolunas).") VALUES (".implode(",",$insert).")";
                                                $einsert = $_SESSION['query']($sqlinsert) or die (mysql_error());
                                                if($einsert) {
                                                    $i++;
                                                }
                                                $arraysql[] = $sqlinsert;**/
                                            }
                                        }
                                    }
                                    else {
                                        if($erro >= 1) {
                                            $obs[] = "Erro, existem valores obrigatórios em branco";
                                        }
                                    }
                                    $linhalog = $d.";".implode("-",$obs);
                                    fwrite($flog,$linhalog."\n");
                                }
                                else {
                                    fwrite($flog, $d.";obs\n");
                                }
                            }
                            if(count($inserts) > 0) {
                                $sqlinsert = "INSERT INTO tmpselecao (".implode(",",$ncolunas).") VALUES ".implode(",",$inserts);
                                $einsert = $_SESSION['query']($sqlinsert) or die (mysql_error());
                            }
                        }
                        $arraysql = implode(";",$arraysql);
                        unlink($caminho."/".$nomearq);
                        if(count($inserts) == 0 && $einsert != true) {
                            unlink($caminho."/".$nomelog);
                            $drop = "DROP TABLE tmpselecao";
                            $edrop = $_SESSION['query']($drop) or die (mysql_error());
                            unlock();
                            if($linhas - 1 == $a) {
                                $msg = "Nenhuma linha do arquivo foi aproveitada pois todos os audios já foram selecionados em outro envio, favor verificar!!!";
                            }
                            else {
                                $msg = "Nenhuma linha do arquivo foi aproveitada pois as datas ou hora podem estar incorreta, favor verificar os campos obrigatórios";
                            }
                            header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                        } 
                        else {
                            $selmeta = "SELECT qtdedia,reposicao,dmenos,count(*) as result FROM configselecao WHERE idrel_filtros='$idrel'";
                            $emeta = $_SESSION['fetch_array']($_SESSION['query']($selmeta)) or die (mysql_error());
                            if($emeta['result'] >= 1) {
                                $qnt = $emeta['qtdedia'];
                                $reposicao = $emeta['reposicao'];
                                $idt = 0;
                                if(in_array($dia, $primeiro)) {
                                    $dtini = date('Y-m')."-01";
                                }
                                else {
                                    $dtini = date('Y-m-d');
                                }
                                $dmenos = 1;
                                for(;$idt != $emeta['dmenos'];) {
                                    $menosdt = "SELECT ADDDATE('$dtini',-".$dmenos.") as data";
                                    $emenosdt = $_SESSION['fetch_array']($_SESSION['query']($menosdt)) or die ("erro na query de verificação das datas"); 
                                    $diasem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($emenosdt['data'],5,2), substr($emenosdt['data'],8,2), substr($emenosdt['data'],0,4)));
                                    if($diasem == "6" || $diasem == "0" || in_array(substr($emenosdt['data'],5,2)."-".substr($emenosdt['data'],8,2),$feriados)) { // verifica se é sabado ou domingo, pois não pode contar e incfrementa 1 dia na subtração
                                        $reduz++;
                                        $dmenos++;
                                    }
                                    else {
                                        $idt++;
                                        $dmenos++;
                                    }
                                }
                                $dreduz = $emeta['dmenos'] + $reduz;
                                $datalim = "SELECT ADDDATE('$dtini',-$dreduz) as reduz";
                                $edatalim = $_SESSION['fetch_array']($_SESSION['query']($datalim)) or die ("erro na query de consulta da data limite para audios");
                                $validadatalim = "SELECT '".$edatalim['reduz']."' >= '$dtinictt' as valida";
                                $evalidalim = $_SESSION['fetch_array']($_SESSION['query']($validadatalim)) or die (mysql_error());
                                if($evalidalim['valida'] >= 1) {
                                    $datalimite = $edatalim['reduz'];
                                }
                                else {
                                    $datalimite = $dtinictt;
                                }

                                //verifica a quantidade faltante nos dias anteriores
                                $checkdt = $dtini;
                                $d = 0;
                                $soma = 0;
                                $seldimen = "SELECT estimativa,count(*) as result FROM dimen_mod WHERE idperiodo='$idperiodo' AND idrel_filtros='$idrel'";
                                $eseldimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
                                if($eseldimen['result'] >= 1) {
                                    $qtdedimen = round($eseldimen['estimativa']/$eselper['diasprod']);
                                    $selenv = "SELECT qtdeefetiva FROM selecao s
                                                INNER JOIN upselecao u ON u.idupselecao = s.idupselecao
                                                WHERE selecionado=1 AND idrel_filtros='$idrel' AND datactt BETWEEN '$dtinictt' AND '$dtfimctt' GROUP BY u.idupselecao";
                                    $eselenv = $_SESSION['query']($selenv) or die (mysql_error());
                                    $nenv = $_SESSION['num_rows']($eselenv);
                                    if($nenv >= 1) {
                                        while($lenv = $_SESSION['fetch_array']($eselenv)) {
                                            $enviados = $enviados + $lenv['qtdeefetiva'];
                                        }
                                    }
                                    else {
                                        $enviados = 0;
                                    }

                                    //verifica a quantidade de dias do mes e a quantidade de dias passados
                                    $dias = $dtinictt;
                                    $cdiasatehj = 0;
                                    $diasmes = 0;
                                    while($dias != date('Y-m-d',  mktime(0, 0, 0, substr($dtfimctt,5,2), substr($dtfimctt,8,2)+1, substr($dtfimctt,0,4)))) {
                                        $dm = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($dias,5,2), substr($dias,8,2), substr($dias,0,4)));
                                        if($dm == 6 || $dm == 0 || in_array(substr($dias,5,2)."-".substr($dias,8,2),$feriados)) {
                                        }
                                        else {
                                            $sqldias = "SELECT ('$dias' < curdate()) as conta";
                                            $esqldias = $_SESSION['fetch_array']($_SESSION['query']($sqldias)) or die (mysql_error());
                                            if($esqldias['conta'] >= 1) {
                                                $cdiasatehj++;
                                            }
                                            $diasmes++;
                                        }
                                        $dias = date('Y-m-d',  mktime(0, 0, 0, substr($dias,5,2), substr($dias,8,2)+1, substr($dias,0,4)));
                                    }
                                    // dias mes e passados
                                    $meta = round(($eseldimen['estimativa'] + ($eseldimen['estimativa']*0.12))/$diasmes);
                                    if($reposicao != 0) {
                                        $soma = $reposicao;
                                        $repo = 1;
                                    }
                                    else {
                                        $soma = (round($meta*$cdiasatehj) - $enviados) + $qnt;
                                        $repo = 0;
                                    }

                                    if((round($meta*$cdiasatehj) - $enviados) < 0) {
                                        $selpura = 0;
                                    }
                                    else {
                                        $selpura = (round($meta*$cdiasatehj) - $enviados);
                                    }
                                    if(date('Y-m-d') == $dtfimctt) {
                                        $dmenosum = $dtfimctt;
                                    }
                                    else {
                                        if(in_array(date('Y-m-d'), $primeiro) && $_SESSION['selbanco'] != "monitoria_pj_itau") {
                                            $dmenosum = $dtfimctt;
                                        }
                                        else {
                                            $dmenosum = date('Y-m-d',mktime (0, 0, 0, date('m')  , date('d'), date('Y')));
                                        }
                                    }
                                    $diff = 0;
                                    $qtdesol = 0;
                                    $qtdesel = 0;
                                    
				      $selrange = "SELECT count(*) as result FROM tmpselecao 
						  WHERE datactt BETWEEN '$datalimite' AND '$dmenosum'
						  ORDER BY RAND()";
                                    $exeselrange = $_SESSION['fetch_array']($_SESSION['query']($selrange)) or die (mysql_error());
                                    $tabslock = array("upselecao","selecao","tmpselecao","configselecao");
                                    if($exeselrange['result'] >= 1) {
                                        if($soma >= 1) {
                                            $qt = 0;
                                            $clw = 0;
                                            $qtoper = 0;
                                            if($reposicao != 0) {
                                                $insertsel = "INSERT INTO upselecao (data,hora,qtdeenv,descarte,qtdesel,qtdeexced,qtdeefetiva,qtdeselexced,reposicao,caminhoarq,nomearquivo,nomesistema,tab_user,iduser) 
                                                              VALUE ('".date('Y-m-d')."','".date('H:i:s')."',0,0,0,0,0,0,'$reposicao','$caminho','".$_FILES['arquivo']['name']."',NULL,'".$_SESSION['user_tabela']."','".$_SESSION['usuarioID']."')";
                                            }
                                            else {
                                                $insertsel = "INSERT INTO upselecao (data,hora,qtdeenv,descarte,qtdesel,qtdeexced,qtdeefetiva,qtdeselexced,reposicao,caminhoarq,nomearquivo,nomesistema,tab_user,iduser) 
                                                              VALUE ('".date('Y-m-d')."','".date('H:i:s')."',0,0,'$selpura','$qnt',0,0,0,'$caminho','".$_FILES['arquivo']['name']."',NULL,'".$_SESSION['user_tabela']."','".$_SESSION['usuarioID']."')";
                                            }
                                            $einsert = $_SESSION['query']($insertsel) or die (mysql_error());
                                            $idinsert = str_pad($_SESSION['insert_id'](),6,0,STR_PAD_LEFT);
                                            $nomesist = str_replace("/","",banco2data($datalimite))."_".$idrel."_".$idinsert."_".date('dmY')."_".date('His').".csv";
                                            $upsel = "UPDATE upselecao SET nomesistema='$nomesist' WHERE idupselecao='$idinsert'";
                                            $eupsel = $_SESSION['query']($upsel) or die (mysql_error());
                                            rename($caminho."/".$nomelog, $caminho."/".$nomesist);
                                            $file = fopen($caminho."/"."selecao-".$nomesist, "w");
                                            chmod($caminho."/"."selecao-".$nomesist,0777);
                                            fwrite($file, implode(";",$confcol)."\n");
                                            
                                            //seleção por quantidade por operador para o cliente ITAU_PJ
                                            $somaoper = 0;
                                            if($_SESSION['selbanco'] == "monitoria_pj_itau") {
                                                shuffle($operadores);
                                                foreach($operadores as $oper) {
                                                    $coper++;
                                                    if($coper >= $soma) {
                                                        break;
                                                    }
                                                    else {
                                                        $sempass = 0;
                                                        $soper = 0;
                                                        $nomeup = array();
                                                        $linhaarq = array();
                                                        foreach($semanas as $ksem => $dsem) {
                                                            $sempass++;
                                                            $alvo = "SELECT '$dia' BETWEEN '".$dsem['dtini']."' AND '".$dsem['dtfim']."' as alvo";
                                                            $ealvo = $_SESSION['fetch_array']($_SESSION['query']($alvo)) or die (mysql_error());
                                                            if($ealvo['alvo'] >= 1) {
                                                                $selecao = "SELECT count(*) as s FROM selecao WHERE operador='$oper' AND datactt BETWEEN '".$dsem['dtini']."' AND '".$dsem['dtfim']."'";
                                                                $eselecao = $_SESSION['fetch_array']($_SESSION['query']($selecao)) or die (mysql_error());
                                                                if($eselecao['s'] >= 1) {
                                                                    $soper++;
                                                                }
                                                                break;
                                                            }
                                                        }
                                                        if($soper == 0) {
                                                            $rand = "SELECT *,count(*) as r FROM tmpselecao 
                                                                    WHERE datactt BETWEEN '$datalimite' AND '$dmenosum' AND operador='$oper'
                                                                    ORDER BY RAND() LIMIT 1";
                                                            $erand = $_SESSION['fetch_array']($_SESSION['query']($rand)) or die (mysql_error());
                                                            if($erand['r'] >= 1) {
                                                                $notin[] = $erand['idtmpselecao'];
                                                                $somaoper++;
                                                                $clw++;
                                                                $nomeup = array();
                                                                foreach($confcol as $coltmp) {
                                                                    if(in_array($coltmp,$coldata)) {
                                                                        $nomeup[] = str_replace("/","",banco2data($erand[$coltmp]));
                                                                    }
                                                                    else {
                                                                        $nomeup[] = str_replace(":", "", $erand[$coltmp]);
                                                                    }
                                                                }
                                                                foreach($confcol as $colnome) {
                                                                    if($colnome != "tempo") {
                                                                        if(in_array($colnome,$coldata)) {
                                                                            $linhaarq[] = banco2data($erand[$colnome]);
                                                                        }
                                                                        else {
                                                                            if(in_array($colnome,$hora)) {
                                                                                $linhaarq[] = mysql_real_escape_string(str_replace(":", "", $erand[$colnome]));
                                                                            }
                                                                            else {
                                                                                $linhaarq[] = mysql_real_escape_string($erand[$colnome]);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                $wlinha = implode(";",$linhaarq)."\n";
                                                                fwrite($file,$wlinha);
                                                                $insertoper[] = "('$idinsert','$idrel','".$erand['operador']."','".$erand['datactt']."','".$erand['horactt']."','".implode("#",$nomeup)."',1,'O',$repo,0)";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if(count($insertoper) > 0) {
                                                $insertsel = "INSERT INTO selecao (idupselecao,idrel_filtros,operador,datactt,horactt,nomearquivo,selecionado,tpselecao,reposicao,enviado) VALUES ".  implode(",", $insertoper);
                                                $einsertsel = $_SESSION['query']($insertsel) or die (mysql_error());
                                            }
                                            $soma = $soma - $somaoper;
                                            
                                            //seleção normal
                                            if($soma > 0) {
                                                if($_SESSION['selbanco'] == "monitoria_pj_itau") {
                                                    if(count($insertoper) > 0) {
                                                        $rand = "SELECT * FROM tmpselecao 
                                                                WHERE datactt BETWEEN '$datalimite' AND '$dmenosum' AND idtmpselecao NOT IN (".implode(",",$notin).")
                                                                ORDER BY RAND() LIMIT $soma";
                                                    }
                                                    else {
                                                        $rand = "SELECT * FROM tmpselecao 
                                                                WHERE datactt BETWEEN '$datalimite' AND '$dmenosum'
                                                                ORDER BY RAND() LIMIT $soma";
                                                    }
                                                }
                                                else {
                                                    if($noper >= $soma) {
                                                        $rand = "SELECT * FROM tmpselecao 
                                                                WHERE datactt BETWEEN '$datalimite' AND '$dmenosum'
                                                                GROUP BY operador
                                                                ORDER BY RAND() LIMIT $soma";
                                                    }
                                                    else {
                                                        $rand = "SELECT * FROM tmpselecao 
                                                                WHERE datactt BETWEEN '$datalimite' AND '$dmenosum'
                                                                ORDER BY RAND() LIMIT $soma";
                                                    }
                                                }
                                                $erand = $_SESSION['query']($rand) or die (mysql_error());
                                                while($lrand = $_SESSION['fetch_array']($erand)) {
                                                    $nomeup = array();
                                                    foreach($confcol as $coltmp) {
                                                        if(in_array($coltmp,$coldata)) {
                                                            $nomeup[] = str_replace("/","",banco2data($lrand[$coltmp]));
                                                        }
                                                        else {
                                                            $nomeup[] = str_replace(":", "", $lrand[$coltmp]);
                                                        }
                                                    }
                                                    $sqlsel[] = "('$idinsert','$idrel','".$lrand['operador']."','".$lrand['datactt']."','".$lrand['horactt']."','".implode("#",$nomeup)."',1,'N',$repo,0)";
                                                    $linhaarq = array();
                                                    $lw = array();
                                                    foreach($confcol as $colnome) {
                                                        if($colnome != "tempo") {
                                                            if(in_array($colnome,$coldata)) {
                                                                $linhaarq[] = banco2data($lrand[$colnome]);
                                                            }
                                                            else {
                                                                if(in_array($colnome,$hora)) {
                                                                    $linhaarq[] = mysql_real_escape_string(str_replace(":", "", $lrand[$colnome]));
                                                                }
                                                                else {
                                                                    $linhaarq[] = mysql_real_escape_string($lrand[$colnome]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    $clw++;
                                                    $wlinha = implode(";",$linhaarq)."\n";
                                                    fwrite($file,$wlinha);
                                                }
                                            }
                                            if(count($sqlsel) > 0) {
                                                $insertsel = "INSERT INTO selecao (idupselecao,idrel_filtros,operador,datactt,horactt,nomearquivo,selecionado,tpselecao,reposicao,enviado) VALUES ".implode(",",$sqlsel);
                                                $einsertsel = $_SESSION['query']($insertsel) or die (mysql_error());
                                            }
                                            
                                            if($selpura == 0) {
                                                $qtefeexed = $clw;
                                                $qtefepura = 0;
                                                if($qnt == 0) {
                                                    $qtdedia = $qnt;
                                                }
                                                else {
                                                    $qtdedia = $qnt - $clw;
                                                }
                                            }
                                            else {
                                                if($clw > $selpura) {
                                                    $qtefepura = $selpura;
                                                    $qtefeexed = $clw - $selpura;
                                                    if($qnt == 0) {
                                                        $qtdedia = $qnt;
                                                    }
                                                    else {
                                                        $qtdedia = $qnt - $qtefeexed;
                                                    }
                                                }
                                                else {
                                                    $qtefepura = $clw;
                                                    $qtefeexed = 0;
                                                    $qtdedia = $qnt;
                                                }
                                            }
                                            $zeroselecao = "UPDATE configselecao SET qtdedia='$qtdedia',reposicao=0 WHERE idrel_filtros='$idrel'";
                                            $exezero = $_SESSION['query']($zeroselecao) or die (mysql_error());
                                            if($reposicao != 0) {
                                                $upqtdesel = "UPDATE upselecao SET reposicao='$clw' WHERE idupselecao='$idinsert'";
                                            }
                                            else {
                                                $upqtdesel = "UPDATE upselecao SET qtdeefetiva='$qtefepura',qtdeselexced='$qtefeexed',qtdeenv='".$exeselrange['result']."',descarte='".(($linhas - 1) - $exeselrange['result'])."' WHERE idupselecao='$idinsert'";
                                            }
                                            $eupqtde = $_SESSION['query']($upqtdesel) or die (mysql_error());
                                            fclose($file);
                                            $drop = "DROP TABLE tmpselecao";
                                            $edrop = $_SESSION['query']($drop) or die (mysql_error());
                                            //unlock();
                                            $msg = "Arquivo de Seleção gerado com sucesso, favor consulta-lo no quadro abaixo do envio dos arquivos";
                                            header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                                        }
                                        else {
                                            unlink($caminho."/".$nomearq);
                                            $drop = "DROP TABLE tmpselecao";
                                            $edrop = $_SESSION['query']($drop) or die (mysql_error());
                                            //unlock();
                                            $msg = "Não existe seleção em falta, não podendo ser gerado mais seleções. Favor contatar os responsáveis";
                                            header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                                        }
                                    }
                                    else {
                                        unlink($caminho."/".$nomearq);
                                        $drop = "DROP TABLE tmpselecao";
                                        $edrop = $_SESSION['query']($drop) or die (mysql_error());
                                        //unlock();
                                        $msg = "Nenhuma linha se enquadra nos filtros de data de contato para seleção de audios";
                                        header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                                    }
                                }
                                else {
                                    unlink($caminho."/".$nomearq);
                                    $drop = "DROP TABLE tmpselecao";
                                    $edrop = $_SESSION['query']($drop) or die (mysql_error());
                                    //unlock();
                                    $msg = "O Relacionamento não possui dimensionamento criado para este mês";
                                    header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                                }
                            }
                            else {
                                $drop = "DROP TABLE tmpselecao";
                                $edrop = $_SESSION['query']($drop) or die (mysql_error());
                                //unlock();
                                $msg = "A operação não tem configuração de seleção criada, favor verificar com os responsáveis";
                                header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                            }
                        }
                    }
                    else {
                        $msg = "As colunas obrigatórias '".implode(",",$colfalta)."' não constam no arquivo enviado, favor verificar";
                        header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                    }
                }
                else {
                    $msg = "Erro no processo de upload do arquivo";
                    header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
                }
            }
            else {
                $msg = "O período para a data atual não está cadastrado no sistema";
                header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
            }
        }
        else {
            $msg = "A extensão do arquivo não está permitida para upload $tipo";
            header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
        }
    }
    else {
        $msg = "O Tamanho do arquivo excede o limite determinado";
        header("location:/monitoria_supervisao/inicio.php?menu=selecao&op=dadosselecao&filtros=$url&msg=$msg");
    }
}

if(isset($_POST['base'])) {
    $idperiodo = $_POST['idperiodo'];
    foreach(explode("#",$_POST['filtros']) as $fpost) {
        $filtro = explode("=",$fpost);
        $filtros[$filtro[0]] = $filtro[1]; 
    }
    $selper = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
    $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die (mysql_error());
    $filtrosrel = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
    $efrel = $_SESSION['query']($filtrosrel) or die (mysql_error());
    $nerel = $_SESSION['num_rows']($efrel);
    if($nerel >= 1) {
        while($lerel = $_SESSION['fetch_array']($efrel)) {
            $nfiltros[] = strtolower($lerel['nomefiltro_nomes']);
            $colnomes[] = $tabsql->AliasTab("filtro_dados").strtolower($lerel['nomefiltro_nomes']).".nomefiltro_dados as ".strtolower($lerel['nomefiltro_nomes']);
            $inners[] = "INNER JOIN filtro_dados ".$tabsql->AliasTab("filtro_dados").strtolower($lerel['nomefiltro_nomes'])." ON ".$tabsql->AliasTab("filtro_dados").strtolower($lerel['nomefiltro_nomes']).".idfiltro_dados=".$tabsql->AliasTab("rel_filtros").".id_".strtolower($lerel['nomefiltro_nomes']);
            if($filtros['f'.strtolower($lerel['nomefiltro_nomes'])] != "") {
                $idsfiltro["id_".strtolower($lerel['nomefiltro_nomes'])] = "id_".strtolower($lerel['nomefiltro_nomes'])."='".$filtros['f'.strtolower($lerel['nomefiltro_nomes'])]."'";
            }
        }
    }
    if(isset($idsfiltro)) {
        $whererel = "WHERE ".implode(" AND ",$idsfiltro);
    }
    else {
        $whererel = "";
    }
    $selrel = "SELECT rf.idrel_filtros FROM rel_filtros rf
                INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                $whererel";
    $eselrel = $_SESSION['query']($selrel) or die (mysql_error());
    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
        $idsreldesor[] = $lselrel['idrel_filtros'];
    }
    $idsrel = arvoreescalar("",$_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
    foreach($idsrel as $idrel) {
        if(in_array($idrel,$idsreldesor)) {
            $idsrelordem[] = $idrel;
        }
    }
    if($eselper['dtinictt'] == "" || $eselper['dtfimctt'] == "") {
        $dtini = substr($eselper['dataini'],0,4)."-".substr($eselper['dataini'],5,2)."-01";
        $dtfim = ultdiames("01".substr($eselper['dataini'],5,2).substr($eselper['dataini'],0,4));
    }
    else {
        $dtini = $eselper['dtinictt'];
        $dtfim = $eselper['dtfimctt'];
    }
    $colunas = implode(";",$nfiltros).";idupload;data;dataimp;nomearquivo;datactt;horactt";
    $colsel = implode(";",$nfiltros).";idupselecao;data;dataimp;nomearquivo;datactt;horactt;selecionada;reposicao;enviado,tpselecao";
    $l = $colunas."\n";
    $nomearq = "VENDAS-".$eselper['nmes']."-".$eselper['ano']."-".date("dmY")."-".date("His")."-".$_SESSION['usuarioLogin'].".csv";
    $nomearqsel = "SELECAO-".$eselper['nmes']."-".$eselper['ano']."-".date("dmY")."-".date("His")."-".$_SESSION['usuarioLogin'].".csv";
    mkdir($rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']);
    $arquivo = fopen($rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']."/$nomearq","w");
    $arqsel = fopen($rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']."/$nomearqsel","w");
    if($arquivo) {
        $add = 0;
        fwrite($arquivo,$l);
        fwrite($arqsel, $colsel."\n");

        $idsteste = implode(",",$idsrelordem);
        foreach($idsrelordem as $idrel) {
            $colobriga = array();
            $operacao = "";
            $noper = "SELECT ".implode(",",$colnomes).",tpaudio,pm.idparam_moni FROM rel_filtros ".$tabsql->AliasTab("rel_filtros")." ".
                      implode(" ",$inners)."
                      INNER JOIN conf_rel cf ON cf.idrel_filtros = ".$tabsql->AliasTab("rel_filtros").".idrel_filtros
                      INNER JOIN param_moni pm ON pm.idparam_moni = cf.idparam_moni
                      WHERE ".$tabsql->AliasTab("rel_filtros").".idrel_filtros='$idrel'";
            $enoper = $_SESSION['fetch_array']($_SESSION['query']($noper)) or die (mysql_error());
            $tpaudio = explode(",",$enoper['tpaudio']);
            foreach($nfiltros as $kf => $f) {
                if($kf == 0) {
                    $operacao .= $enoper[$f];   
                }
                else {
                    $operacao .= ";$enoper[$f]";
                }
            }
            $selcolunas = "SELECT nomecoluna FROM camposparam cp
                          INNER JOIN coluna_oper co ON co.idcoluna_oper = cp.idcoluna_oper
                          WHERE cp.idparam_moni='".$enoper['idparam_moni']."' ORDER BY posicao";
            $eselcolunas = $_SESSION['query']($selcolunas) or die (mysql_error());
            while($lselcolunas = $_SESSION['fetch_array']($eselcolunas)) {
                $colobriga[] = strtolower($lselcolunas['nomecoluna']);
            }
            $sqlselecao = "SELECT ".$tabsql->AliasTab("upselecao").".idupselecao,data,caminhoarq,nomesistema FROM selecao ".$tabsql->AliasTab("selecao")."
                         INNER JOIN upselecao ".$tabsql->AliasTab("upselecao")." ON ".$tabsql->AliasTab("upselecao").".idupselecao=".$tabsql->AliasTab("selecao").".idupselecao
                         WHERE idrel_filtros='$idrel' AND datactt BETWEEN '$dtini' AND '$dtfim' GROUP BY ".$tabsql->AliasTab("upselecao").".idupselecao";
            $esqlselecao = $_SESSION['query']($sqlselecao) or die (mysql_error());
            $nselecao = $_SESSION['num_rows']($esqlselecao);
            if($nselecao >= 1) {
                while($lselecao = $_SESSION['fetch_array']($esqlselecao)) {
                    $selecao = "SELECT * FROM selecao WHERE idupselecao='".$lselecao['idupselecao']."' AND reposicao='0'";
                    $eselecao = $_SESSION['query']($selecao) or die (mysql_error());
                    while($liselecao = $_SESSION['fetch_array']($eselecao)) {
                        $selfila = "SELECT narquivo,dataimp,count(*) as r FROM fila_grava fg"
                                . " INNER JOIN upload u ON u.idupload = fg.idupload WHERE narquivo='".$liselecao['nomearquivo']."'";
                        $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila));
                        $li = "$operacao;".$lselecao['idupselecao'].";".banco2data($lselecao['data']).";".banco2data($eselfila['dataimp']).";".$liselecao['nomearquivo'].";".banco2data($liselecao['datactt']).";".$liselecao['horactt'].";".$liselecao['selecionado'].";".$liselecao['reposicao'].";".$liselecao['enviado'].";".$liselecao['tpselecao']."\n";
                        fwrite($arqsel, $li);
                    }
                    $colarq = array();
                    $modelo = array();
                    $linhas = file($lselecao['caminhoarq']."/".$lselecao['nomesistema'], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                    foreach($linhas as $nlinha => $li) {
                        $narquivo = "";
                        if($nlinha == 0) {
                            $colarray = explode(";",$li);
                            foreach($colarray as $col) {
                                $colarq[] = strtolower($col);
                            }
                            foreach($colarq as $k => $texto) {
                                if(in_array($texto, $colobriga)) {
                                    $modelo[array_search($texto, $colobriga)][$k] = $texto;
                                }
                            }
                            ksort($modelo);
                        }
                        else {
                            $linha .= "$operacao;".$lselecao['idupselecao'].";".banco2data($lselecao['data']);
                            $dados = explode(";",$li);
                            foreach ($modelo as $kc => $c) {
                                foreach($c as $p => $textop) {
                                    if($dados[$p] != "") {
                                        if($textop == "datactt") {
                                            $datactt = str_replace("/", "", trim($dados[$p]));
                                            $narquivo .= "#".str_replace("/", "", trim($dados[$p]));
                                        }
                                        else {
                                            if($textop == "horactt") {
                                                $horactt = trim($dados[$p]);
                                            }
                                            $narquivo .= "#".trim($dados[$p]);
                                        }
                                    }
                                }
                            }
                            if($narquivo != "") {
                                $narquivo = substr($narquivo,1);
                                $dataimp = "";
                                $linha .= ";$dataimp;$narquivo;$datactt;$horactt";
                                $linha .= "\n";
                                fwrite($arquivo,$linha);
                                $linha = "";
                                $add++;
                            }
                            else {
                                $linha = "";
                            }
                        }
                    }
                }
            }
        }
        fclose($arquivo);
        if($add >= 1) {
            echo 1;
        }
        else {
            if($add == 0) {
                echo 0;
            }
            else {
                echo 3;
            }
        }
    }
    else {
        echo 2;
    }
}
?>
