<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
$id = addslashes($_GET['id']);
$sql = "SELECT *, date_format(datacad, '%d/%m/%Y') as dcad, date_format(dataalt, '%d/%m/%Y') as dalt  FROM user_web WHERE iduser_web='$id'";
$luser = $_SESSION['fetch_array']($_SESSION['query']($sql)) or die (mysql_error());
$cpf1 = substr($luser['CPF'], 0, 3);
$cpf2 = substr($luser['CPF'], 3, 3);
$cpf3 = substr($luser['CPF'], 6, 3);
$cpfdig = substr($luser['CPF'], 9, 2);
$cpf = $cpf1.".".$cpf2.".".$cpf3."-".$cpfdig;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sem titulo</title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/monitoria_supervisao/js/jquery-1.4.4.js"></script>
<script type="text/javascript" src="/monitoria_supervisao/js/jquery.maskedinput-1.2.2.js"></script>
<script type="text/javascript">
jQuery(function($){
   $("#dcad").mask("99/99/9999");
   $("#cpf").mask("999.999.999-99");
});
</script>
</head>
<body>
<div id="conteudo" class="corfd_pag">
<form action="caduserweb.php" method="post">
<table width="841" border="0">
  <tr>
  	<td class="corfd_ntab" colspan="6"><strong>ALTERAÇÃO DE USUÁRIO WEB</strong></td>
  </tr>
  <tr>
    <td width="133" class="corfd_coltexto"><strong>Nome</strong></td>
    <td width="213" class="crofd_colcampos"><input type="hidden" name="id" id="id" value="<?php echo $id;?>" /><input style="width:200px; border: 1px solid #333; text-align:center" name="nome" type="text" value="<?php echo $luser['nomeuser_web']; ?>" /></td>
    <td width="110" class="corfd_coltexto"><strong>CPF</strong></td>
    <td width="221" class="crofd_colcampos">
      <label>
        <input style="width:100px; border: 1px solid #333; text-align:center" type="text" name="cpf" id="cpf" value="<?php echo $cpf; ?>" />
      </label>
    </td>
    <td width="91" class="corfd_coltexto"><strong>Ativo</strong></td>
    <td width="47" class="crofd_colcampos"><select name="ativo">
    <?php
	echo "<option value=\"".$luser['ativo']."\">".$luser['ativo']."</option>";
	$atvdb = array($luser['ativo']);
	$atvop = array('S','N');
	$dif = array_diff($atvop, $atvdb);
	foreach($dif as $atv) {
		echo "<option value=\"".$atv."\">".$atv."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Perfil</strong></td>
    <td class="crofd_colcampos">
    <select name="perfil">
    <?php
	$sql = "SELECT idperfil_web, nomeperfil_web FROM perfil_web WHERE ativo='S'";
	$exe = $_SESSION['query']($sql) or die (mysql_error());
	while($lperfil = $_SESSION['fetch_array']($exe)) {
            if($luser['idperfil_web'] == $lperfil['idperfil_web']) {
                echo "<option value=\"".$lperfil['idperfil_web']."\" selected=\"selected\">".$lperfil['nomeperfil_web']."</option>";
            }
            if($luser['idperfil_web'] != $lperfil['idperfil_web']) {
                echo "<option value=\"".$lperfil['idperfil_web']."\">".$lperfil['nomeperfil_web']."</option>";
            }
	}
	?>
    </select></td>
    <td class="corfd_coltexto"><strong>E-mail</strong></td>
    <td class="crofd_colcampos">
      <label>
        <input style="width:200px; border: 1px solid #333; text-align:center" type="text" name="email" value="<?php echo $luser['email']; ?>" />
      </label>
    </td>
    <td class="corfd_coltexto"><strong>Senha Inicial</strong></td>
    <td class="crofd_colcampos"><select name="senha_ini">
    <?php
	echo "<option value=\"".$luser['senha_ini']."\">".$luser['senha_ini']."</option>";
	$s_inidb = array($luser['senha_ini']);
	$s_iniop = array('S','N');
	$dif = array_diff($s_iniop, $s_inidb);
    foreach($dif as $senha_ini) {
		echo "<option value=\"".$senha_ini."\">".$senha_ini."</option>";
	}
	?>
    </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Usuario</strong></td>
    <td class="crofd_colcampos">
    <input style="width:100px; border: 1px solid #333; text-align:center; text-align:center" name="user" type="text" value="<?php echo $luser['usuario']; ?>" />
    <input name="id" type="hidden" value="<?php echo $_GET['id']; ?>" />
    </td>
    <td class="corfd_coltexto"><strong>Importação</strong></td>
    <td class="crofd_colcampos"><select name="import">
    <?php
	echo "<option value=\"".$luser['import']."\">".$luser['import']."</option>";
	$importop = array('S','N');
	foreach($importop as $import) {
		if($import == $luser['import']) {
		}
		else {
			echo "<option value=\"".$import."\">".$import."</option>";
		}
	}
	?>
    </select></td>
    <td class="corfd_coltexto"><strong>Conf. E-mail</strong></td>
      <td colspan="3" class="crofd_colcampos"><select name="confemail">
       <?php
       $selconf = "SELECT idconfemailenv FROM confemailenv WHERE tipo='USUARIO'";
       $eselconf = $_SESSION['query']($selconf) or die ("erro na consulta das configuraÃ§Ãµes cadastradas");
       while($lconf = $_SESSION['fetch_array']($eselconf)) {
               echo "<option value=\"".$lconf['idconfemailenv']."\">".$lconf['idconfemailenv']."</option>";
       }
       ?>
      </select></td>
  </tr>
  <tr>
  <td class="corfd_coltexto"><strong>Data Cad.</strong></td>
  <td class="crofd_colcampos"><input style="width:100px; border: 1px solid #333; text-align:center; text-align:center" readonly="readonly" name="dcad" id="dcad" type="text" value="<?php echo $luser['dcad']; ?>" /></td>
  <td class="corfd_coltexto"><strong>Data Ultima Alt.</strong></td>
  <td class="crofd_colcampos"><input style="width:150px; border: 1px solid #333; text-align:center; text-align:center" readonly="readonly" name="dalt" type="text" value="<?php echo $luser['dalt']." ".$luser['horaalt']; ?>" /></td>
  <td class="corfd_coltexto"><strong>Calibragem</strong></td>
   <td class="crofd_colcampos"><select name="calib">
  <?php
  	echo "<option value=\"".$luser['calibragem']."\" selected=\"selected\">".$luser['calibragem']."</option>";
	$opcalib = array('N','S');
	foreach($opcalib as $calib) {
		if($calib == $luser['calibragem']) {
		}
		else {
			echo "<option value=\"".$calib."\">".$calib."</option>";
		}
	}
  ?>
  </select></td>
  </tr>
  <tr>
    <td class="corfd_coltexto"><strong>Tela Apresentação</strong></td>
    <td class="crofd_colcampos">
    <select name="confapres[]" multiple="multiple" size="4">
    <?php
		$confdb = explode(",",$luser['confapres']);
		$nomeapres = array('CALIB' => 'CALIBRAGEM','REL' => 'RELATÓRIO','EBOOK' => 'EBOOK');
		foreach($nomeapres as $opapres => $napres) {
			if(in_array($opapres, $confdb)) {
				echo "<option value=\"".$opapres."\" selected=\"selected\">".$napres."</option>";
			}
			else {
				echo "<option value=\"".$opapres."\">".$napres."</option>";
			}
		}
	?>
    </select></td>
    <td class="corfd_coltexto"><strong>Seleção Audios</strong></td>
    <td class="corfd_colcampos">
    	<select name="selecao" id="selecao">
        	<?php
			$selecao = array("S","N");
			foreach($selecao as $sel) {
				if($sel == $luser['selecaoaudios']) {
					?>
                    <option selected="selected" value="<?php echo $sel;?>"><?php echo $sel;?></option>	
                    <?php
				}
				else {
					?>
                    <option value="<?php echo $sel;?>"><?php echo $sel;?></option>	
                    <?php
				}
			}
			?>
        </select>
    </td>
  </tr>
  <tr>
  	<td colspan="6"><input style="font-family:Verdana, Geneva, sans-serif; height: 18px; border:1px solid #FFF; width:60px; height: 18px; background-image:url(../images/button.jpg); text-align:center" name="altera" type="submit" value="Alterar" /> <input style="font-family:Verdana, Geneva, sans-serif; height: 18px; border:1px solid #FFF; height: 18px; background-image:url(../images/button.jpg); text-align:center" name="relfiltroweb" type="submit" value="Altera Filtros" /> <input style="font-family:Verdana, Geneva, sans-serif; height: 18px; border:1px solid #FFF; height: 18px; background-image:url(../images/button.jpg); text-align:center" name="reset" type="submit" value="Reset Senha" /> <input style="font-family:Verdana, Geneva, sans-serif; height: 18px; border:1px solid #FFF; height: 18px; background-image:url(../images/button.jpg); text-align:center" name="cancela" type="submit" value="Cancelar" /></td>
  </tr>
</table>
</form>
<font color="#FF0000"><strong><?php echo $_GET['msg'] ?></strong></font>
</div>
</body>
</html>
