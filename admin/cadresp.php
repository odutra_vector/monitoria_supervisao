<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');


$caracter = "[]$[><}{)(:;!?*%&#@]";
$idperg = $_POST['idperg'];
$idresp = $_POST['idresp'];
$resp = $_POST['resp'];
$complemento = $_POST['comple'];
$pos = $_POST['posicao'];
$avalia = val_vazio($_POST['avaliacao']);
$idplanilha = $_POST['idplanilha'];
$idrelavalia = $_POST['idrel_filtro'];
$idrelorigem = val_vazio($_POST['idrel_origem']);
$ativo = $_POST['ativo'];
$padrao = $_POST['padrao'];
$v_resp = val_vazio(str_replace(",", ".", $_POST['valor_resp']));

if(isset($_POST['cadastra'])) {
    $sel = "SELECT COUNT(*) as result, tipo_resp FROM pergunta WHERE idpergunta='$idperg' AND descriresposta='$resp'";
    $esel = $_SESSION['fetch_array']($_SESSION['query']($sel)) or die (mysql_error());
    if($resp == "" || $v_resp == "") {
          $msg = "Todos os campos devem estar preenchido para cadastro da resposta!!!";
          header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
    }
    else {
        if(eregi($caracter, $v_resp)) {
              $msg = "No campo ''Valor Resp.'' só pode constar números e virgula!!!";
              header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
        }
        else {
            if($_POST['avaliacao'] == "TABULACAO" && $v_resp != "NULL") {
                  $msg = "Resposta de tabulação não pode ter valor!!!";
                  header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
            }
            else {
                if($v_resp < 0) {
                      $msg = "O valor da resposta não pode ser menor que zero!!!";
                      header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                }
                else {
                    if($esel['result'] == 1) {
                          $msg = "Já existe resposta cadastrada com o mesmo texto!!!";
                          header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                    }
                    else {
                        if($pos != 0 && $ativo == "N") {
                              $msg = "Quando a resposta estiver ''INATIVA'', a posisão da resposta só poderá ser ''0''!!!";
                              header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                        }
                        else {
                            $cons = "SELECT *, MAX(idresposta) as result FROM pergunta";
                            $execons = $_SESSION['fetch_array']($_SESSION['query']($cons)) or die (mysql_error());
                            $vtotal = "SELECT SUM(valor_resp) as soma FROM pergunta WHERE idpergunta='$idperg'";
                            $evtotal = $_SESSION['fetch_array']($_SESSION['query']($vtotal)) or die ("erro na query de consulta do valor total das respostas");
                            $sqlperg = "SELECT * FROM pergunta WHERE idpergunta='$idperg' ORDER BY idresposta LIMIT 1";
                            $exeperg = $_SESSION['fetch_array']($_SESSION['query']($sqlperg)) or die (mysql_error());
                            if($exeperg['valor_perg'] == "") {
                                $valorperg = "NULL";
                            }
                            else {
                                $valorperg = $exeperg['valor_perg'];
                            }
                            if($_POST['valor_resp'] != "") {
                                if(str_replace("'","",$v_resp) > $valorperg && $_POST['avaliacao'] != "FG" && $_POST['avaliacao'] != "FGM") {
                                      $msg = "O valor da resposta não pode ser maior que o valor da pergunta!!!";
                                      header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                                }
                                else {
                                    if($exeperg['idresposta'] == "0" OR $exeperg['idresposta'] == "") {
                                          $idresp = $execons['result'] + 1;
                                          $idperg = $idperg;
                                          $alter = "UPDATE pergunta SET compleresposta='$complemento',idresposta='$idresp', padrao='$padrao', descriresposta='$resp', avalia=$avalia, avaliaplan='$idplanilha', idrel_origem=$idrelorigem, idrel_avaliaplan='$idrelavalia', valor_resp=$v_resp, posicao='$pos', ativo_resp='$ativo' WHERE idpergunta='$idperg'";
                                          $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                    }
                                    else {
                                          $idresp = $execons['result'] + 1;
                                          $idperg = $idperg;
                                          if($exeperg['modcalc'] == "PERC") {
                                              $percperda = val_vazio($exeperg['percperda']);
                                              $maxrep = val_vazio($exeperg['maxrep']);
                                          }
                                          else {
                                              $percperda = "NULL";
                                              $maxrep = "NULL";
                                          }
                                        $modcalc = val_vazio($exeperg['modcalc']);
                                          $alter = "INSERT INTO pergunta (idpergunta, descripergunta, complepergunta, valor_perg, ativo, tipo_resp, modcalc,percperda,maxrep,idresposta, padrao, descriresposta, compleresposta, avalia, avaliaplan, idrel_origem,idrel_avaliaplan, valor_resp, posicao, ativo_resp) VALUE ('$idperg','".$exeperg['descripergunta']."',
                                          '".$exeperg['complepergunta']."', '".$exeperg['valor_perg']."', '".$exeperg['ativo']."', '".$exeperg['tipo_resp']."',$modcalc,$percperda,$maxrep, '$idresp','$padrao', '$resp','$complemento', $avalia,'$idplanilha', $idrelorigem,'$idrelavalia', $v_resp, '$pos', '$ativo')";
                                          $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                    }
                                    if($exealt) {
                                        $msg = "Resposta cadastrada com sucesso!!!";
                                        header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                                    }
                                    else {
                                        $msg = "Ocorreu um erro no processo de cadastramento, favor contatar o administrador!!!";
                                        header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                                    }
                                }
                            }
                            else {
                                if($exeperg['idresposta'] == "0" || $exeperg['idresposta'] == "") {
                                    $idresp = $execons['result'] + 1;
                                    $idperg = $idperg;
                                    $alter = "UPDATE pergunta SET compleresposta='$comple', idresposta='$idresp', padrao='$padrao', descriresposta='$resp', avalia=$avalia, avaliaplan=".val_vazio($idplanilha).", idrel_origem=$idrelorigem,idrel_avaliaplan=".val_vazio($idrelavalia).", valor_resp=$v_resp, posicao='$pos', ativo_resp='$ativo' WHERE idpergunta='$idperg'";
                                    $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                }
                                else {
                                    if($exeperg['valor_perg'] == "") {
                                        $vperg = "NULL";
                                    }
                                    else {
                                        $vperg = $exeperg['valor_perg'];
                                    }
                                    $idresp = $execons['result'] + 1;
                                    $idperg = $idperg;
                                    if($exeperg['modcalc'] == "PERC") {
                                        $percperda = val_vazio($exeperg['percperda']);
                                        $maxrep = val_vazio($exeperg['maxrep']);
                                    }
                                    else {
                                        $percperda = "NULL";
                                        $maxrep = "NULL";
                                    }
                                    $modcalc = val_vazio($exeperg['modcalc']);
                                    $alter = "INSERT INTO pergunta (idpergunta, descripergunta, complepergunta, valor_perg, ativo, tipo_resp, modcalc,percperda,maxrep,idresposta, padrao, descriresposta, compleresposta, avalia, avaliaplan,idrel_origem,idrel_avaliaplan, valor_resp, posicao, ativo_resp) VALUE ('$idperg', '".$exeperg['descripergunta']."',
                                    '".$exeperg['complepergunta']."', $vperg, '".$exeperg['ativo']."', '".$exeperg['tipo_resp']."',$modcalc,$percperda,$maxrep, '$idresp','$padrao', '$resp','$complemento', $avalia, '$idplanilha', $idrelorigem,'$idrelavalia', $v_resp, '$pos', '$ativo')";
                                    $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                }
                                if($exealt) {
                                  $msg = "Resposta cadastrada com sucesso!!!";
                                  header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                                }
                                else {
                                    $msg = "Ocorreu um erro no processo de cadastramento, favor contatar o administrador!!!";
                                    header("location:admin.php?menu=respostas&id=".$idperg."&msg=".$msg);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $selperg = "SELECT tipo_resp, SUM(valor_resp) as soma FROM pergunta WHERE idpergunta='$idperg' AND idresposta<>$idresp AND avalia='AVALIACAO'";
    $eselperg = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die ("erro na query de consulta da somatÃ³ria dos valores das respostas");
    $selresp = "SELECT * FROM pergunta WHERE idpergunta='$idperg' AND idresposta='$idresp'";
    $eselresp = $_SESSION['fetch_array']($_SESSION['query']($selresp)) or die ("erro na query de consulta da resposta");
    if($resp == "" || $v_resp == "") {
      $msgi = "Todos os campos precisam estar preenchidos para que a alteração seja executada!!!";
      header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
    }
    else {
        if(eregi($caracter, $v_resp)) {
          $msgi = "No campo ''Valor Resp.'' só pode constar números e virgula!!!";
          header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
        }
        else {
            if($_POST['avaliacao'] == "TABULACAO" && $v_resp != "NULL") {
                  $msg = "Resposta de tabulação não pode ter valor!!!";
                  header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msg=".$msg);
            }
            else {
                if($v_resp < 0) {
                  $msg = "O valor da resposta não pode ser menor que zero!!!";
                  header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msg=".$msg);
                }
                else {
                    if($ativo == "N" && $pos != 0) {
                      $msg = "Quando a resposta estiver ''INATIVA'', a posição da resposta só poderÃ¡ ser ''0''!!!";
                      header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msg=".$msg);
                    }
                    else {
                        if($eselresp['valor_perg'] == "") {
                              $valorperg = "";
                        }
                        else {
                              $valorperg = $eselresp['valor_perg'];
                        }
                        if($eselresp['valor_resp'] == "") {
                              $vrespdb = "";
                        }
                        else {
                              $vrespdb = $eselresp['valor_resp'];
                        }
                        if($padrao == $eselresp['padrao'] && $resp == $eselresp['descriresposta'] && $complemento == $eselresp['compleresposta'] && $idplanilha == $eselresp['avaliaplan'] && 
                           $idrelavalia == $eselresp['idrel_avaliaplan'] && $_POST['avaliacao'] == $eselresp['avalia'] && $_POST['idrel_origem'] == $eselresp['idrel_origem'] && $ativo == $eselresp['ativo_resp'] && 
                           str_replace(",", ".", $_POST['valor_resp']) == $vrespdb && $pos == $eselresp['posicao']) {
                          $msgi = "Resposta não alterada, pois nenhum dado foi modificado!!!";
                          header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
                        }
                        else {
                            if($v_resp == "NULL") {
                              $alter = "UPDATE pergunta SET padrao='$padrao', compleresposta='$complemento', descriresposta='$resp', avaliaplan=".val_vazio($idplanilha).", avalia=$avalia, idrel_avaliaplan=".val_vazio($idrelavalia).", idrel_origem=$idrelorigem,valor_resp=$v_resp, posicao='$pos', ativo_resp='$ativo' WHERE idresposta='$idresp'";
                              $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                  if($exealt) {
                                      $msgi = "Resposta Alterada com sucesso!!!";
                                      header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
                                  }
                                  else {
                                      $msgi = "Ocorreu um erro no processo de alteração, favor contatar o administrador!!!";
                                      header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
                                  }
                            }
                            else {
                                if(str_replace("'","",$v_resp) > $valorperg && $_POST['avaliacao'] != "FG" AND $_POST['avaliacao'] != "FGM") {
                                    $msgi = "O valor da resposta não pode ser maior que o valor da pergunta!!!";
                                    header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
                                }
                                else {
                                    $alter = "UPDATE pergunta SET padrao='$padrao', compleresposta='$complemento', descriresposta='$resp', avaliaplan='$idplanilha', idrel_origem=$idrelorigem,idrel_avaliaplan='$idrelavalia', avalia=$avalia, valor_resp=$v_resp, posicao='$pos', ativo_resp='$ativo' WHERE idresposta='$idresp'";
                                    $exealt = $_SESSION['query']($alter) or die (mysql_error());
                                    if($exealt) {
                                        $msgi = "Resposta Alterada com sucesso!!!";
                                        header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
                                    }
                                    else {
                                        $msgi = "Ocorreu um erro no processo de alteraçã, favor contatar o administrador!!!";
                                        header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp."&msgi=".$msgi);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['finaliza'])) {
    $msgi = "Pergunta e respostas cadastradas/ atualizadas com sucesso!!!";
    header("location:admin.php?menu=perguntas&id=".$idperg."&msgi=".$msgi);
}

if(isset($_POST['carrega'])) {
    $idperg = $_POST['idperg'];
    $idresp = $_POST['idresp'];
    header("location:admin.php?menu=respostas&id=".$idperg."&idresp=".$idresp);
}

if(isset($_POST['novo'])) {
    $idperg = $_POST['idperg'];
    header("location:admin.php?menu=respostas&id=".$idperg);
}

if(isset($_POST['volta'])) {
    $idperg = $_POST['idperg'];
    header("location:admin.php?menu=pergdet&id=".$idperg);
}

if(isset($_POST['apaga'])) {
    $idperg = $_POST['idperg'];
    $idresp = $_POST['idresp'];
    $verifmoni = "SELECT COUNT(*) as result FROM moniavalia WHERE idresposta='$idresp'";
    $everifmoni = $_SESSION['fetch_array']($_SESSION['query']($verifmoni)) or die (mysql_error());
    $verif = "SELECT COUNT(*) as result FROM pergunta WHERE idpergunta='$idperg'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die ($_SESSION['query']);
    if($everifmoni['result'] >= 1) {
        $msgi = "Esta resposta nÃ£o pode ser apagada pois jÃ¡ existem monitorias registradas com ela!!!";
        header("location:admin.php?menu=respostas&id=".$idperg."&msgi=".$msgi);
    }
    else {
        if($everif['result'] == 1) {
            $sqldel = "UPDATE pergunta SET idresposta='000000', descriresposta='', compleresposta='', avalia='', valor_resp='0' WHERE idpergunta='$idperg'";
            $exedel = $_SESSION['query']($sqldel) or die (mysql_error());
        }
        if($everif['result'] > 1) {
            $sqldel = "DELETE FROM pergunta WHERE idpergunta='$idperg' AND idresposta='$idresp'";
            $exedel = $_SESSION['query']($sqldel) or die (mysql_error());
        }
        if($exedel) {
            $msgi = "Resposta apagada com sucesso!!!";
            header("location:admin.php?menu=respostas&id=".$idperg."&msgi=".$msgi);
        }
        else {
            $msgi = "Ocorreu algum erro no processo, favor contatar o administrador!!!";
            header("location:admin.php?menu=respostas&id=".$idperg."&msgi=".$msgi);
        }
    }
}

if(isset($_POST['loadfiltros'])) {
    echo "<option value=\"\">SELECIONE...</option>";
    $idsrel = arvoreescalar("", $_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
    foreach($idsrel as $idrel) {
        echo "<option value=\"$idrel\">".nomeapres($idrel)."</option>";
    }
}
?>