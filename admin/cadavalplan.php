<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['cadastra'])) {
    $nome = strtoupper($_POST['nome']);
    $nivel = $_POST['nivel'];
    $caracter = "[]$[><}{)(:;!?*%&#@]";
    $sel = "SELECT COUNT(*) as result FROM aval_plan WHERE nomeaval_plan='$nome' AND nivel='$nivel'";
    $esel = $_SESSION['fetch_array']($_SESSION['query']($sel)) or die (mysql_error());
    if($nome == "" || $nivel == "") {
	$msg = "Os campos ''Nome'' e ''Nivel'' devem estar preenchidos para cadastro de nova avaliação";
	header("location:admin.php?menu=aval_plan&msg=".$msg);
    }
    else {
        if(eregi($caracter, $nome)) {
            $msg = "O campo ''Nome'' nao pode conter caracteres invalidos $caracter !!!";
            header("location:admin.php?menu=aval_plan&msg=".$msg);
        }
        else {
            if($esel['result'] >= 1) {
                $msg = "Ja existe avaliação cadastrada com o mesmo nome, informe outro nome!!!";
                header("location:admin.php?menu=aval_plan&msg=".$msg);
            }
            else {
                $cad = "INSERT INTO aval_plan (nomeaval_plan, nivel) VALUE ('$nome', '$nivel')";
                $ecad = $_SESSION['query']($cad) or die (mysql_error());
                if($ecad) {
                    $msg = "Cadastro efetuado com sucesso!!!";
                    header("location:admin.php?menu=aval_plan&msg=".$msg);
                }
                else {
                    $msg = "Ocorreu um erro no processo, favor contatar o administrador!!!";
                    header("location:admin.php?menu=aval_plan&msg=".$msg);
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $id = $_POST['id'];
    $nome = strtoupper($_POST['nome']);
    $nivel = $_POST['nivel'];
    $caracter = "[]$[><}{)(:;!?*%&#@]";
    $selaval = "SELECT * FROM aval_plan WHERE idaval_plan='$id'";
    $eselaval = $_SESSION['fetch_array']($_SESSION['query']($selaval)) or die (mysql_error());
    $sel = "SELECT COUNT(*) as result FROM aval_plan WHERE nomeaval_plan='$nome' AND idaval_plan<>'$id'";
    $esel = $_SESSION['fetch_array']($_SESSION['query']($sel)) or die (mysql_error());
    if($nome == "" || $nivel == "") {
	$msgi = "Os campos ''Nome'' e ''Nivel'' devem estar preenchidos para cadastro de nova avaliação";
	header("location:admin.php?menu=aval_plan&msgi=".$msgi);
    }
    else {
        if(eregi($caracter, $nome)) {
            $msgi = "O campo ''Nome'' nao pode conter caracteres invalidos $caracter !!!";
            header("location:admin.php?menu=aval_plan&msgi=".$msgi);
        }
        else {
            if($nome == $eselaval['nomeaval_plan'] && $nivel == $eselaval['nivel']) {
                $msgi = "Nenhum campo foi alterado, processo não executado!!!";
                header("location:admin.php?menu=aval_plan&msgi=".$msgi);
            }
            else {
                if($esel['result'] >= 1) {
                    $msgi = "Ja existe avaliação cadastrada com o mesmo nome, informe outro nome!!!";
                    header("location:admin.php?menu=aval_plan&msgi=".$msgi);
                }
                else {
                    $alter = "UPDATE aval_plan SET nomeaval_plan='$nome', nivel='$nivel' WHERE idaval_plan='$id'";
                    $ealter = $_SESSION['query']($alter) or die (mysql_error());
                    if($ealter) {
                        $msgi = "''Avaliação alterada com sucesso!!!";
                        header("location:admin.php?menu=aval_plan&msgi=".$msgi);
                    }
                    else {
                        $msgi = "''Avaliação alterada com sucesso!!!";
                        header("location:admin.php?menu=aval_plan&msgi=".$msgi);
                    }
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $id = $_POST['id'];
    $verif = "SELECT COUNT(*) as result FROM rel_aval WHERE idaval_plan='$id'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($verif)) or die (mysql_error());
    if($everif['result'] >= 1) {
	$msgi = "Esta avaliação nao pode ser apagada pois ja esta relacionada com grupos, desfaça o relacionamento para apagar!!!";
	header("location:admin.php?menu=aval_plan&msgi=".$msgi);
    }
    else {
	$del = "DELETE FROM aval_plan WHERE idaval_plan='$id'";
	$edel = $_SESSION['query']($del) or die (mysql_error());
	$alter = "ALTER TABLE aval_plan AUTO_INCREMENT=1";
	$ealter = $_SESSION['query']($alter) or die (mysql_error());
	if($edel) {
	    $msgi = "''Avaliação apagada com sucesso!!!";
	    header("location:admin.php?menu=aval_plan&msgi=".$msgi);
	}
	else {
	    $msgi = "Ocorreu algum erro no processo, favor contatar o administrador!!!";
	    header("location:admin.php?menu=aval_plan&msgi=".$msgi);
	}
    }
}


?>