<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

$nome = strtoupper($_POST['nome']);
$iduser = $_SESSION['usuarioID'];
$nomeuser = $_SESSION['usuarioNome'];
$tabuser = $_SESSION['user_tabela'];
$mimes = array("323" => "text/h323",
"acx" => "application/internet-property-stream",
"ai" => "application/postscript",
"aif" => "audio/x-aiff",
"aifc" => "audio/x-aiff",
"aiff" => "audio/x-aiff",
"asf" => "video/x-ms-asf",
"asr" => "video/x-ms-asf",
"asx" => "video/x-ms-asf",
"au" => "audio/basic",
"avi" => "video/x-msvideo",
"axs" => "application/olescript",
"bas" => "text/plain",
"bcpio" => "application/x-bcpio",
"bin" => "application/octet-stream",
"bmp" => "image/bmp",
"c" => "text/plain",
"cat" => "application/vnd.ms-pkiseccat",
"cdf" => "application/x-cdf",
"cer" => "application/x-x509-ca-cert",
"class" => "application/octet-stream",
"clp" => "application/x-msclip",
"cmx" => "image/x-cmx",
"cod" => "image/cis-cod",
"cpio" => "application/x-cpio",
"crd" => "application/x-mscardfile",
"crl" => "application/pkix-crl",
"crt" => "application/x-x509-ca-cert",
"csh" => "application/x-csh",
"css" => "text/css",
"dcr" => "application/x-director",
"der" => "application/x-x509-ca-cert",
"dir" => "application/x-director",
"dll" => "application/x-msdownload",
"dms" => "application/octet-stream",
"doc" => "application/msword",
"dot" => "application/msword",
"dvi" => "application/x-dvi",
"dxr" => "application/x-director",
"eps" => "application/postscript",
"etx" => "text/x-setext",
"evy" => "application/envoy",
"exe" => "application/octet-stream",
"fif" => "application/fractals",
"flr" => "x-world/x-vrml",
"gif" => "image/gif",
"gtar" => "application/x-gtar",
"gz" => "application/x-gzip",
"h" => "text/plain",
"hdf" => "application/x-hdf",
"hlp" => "application/winhlp",
"hqx" => "application/mac-binhex40",
"hta" => "application/hta",
"htc" => "text/x-component",
"htm" => "text/html",
"html" => "text/html",
"htt" => "text/webviewhtml",
"ico" => "image/x-icon",
"ief" => "image/ief",
"iii" => "application/x-iphone",
"ins" => "application/x-internet-signup",
"isp" => "application/x-internet-signup",
"jfif" => "image/pipeg",
"jpe" => "image/jpeg",
"jpeg" => "image/jpeg",
"jpg" => "image/jpeg",
"js" => "application/x-javascript",
"latex" => "application/x-latex",
"lha" => "application/octet-stream",
"lsf" => "video/x-la-asf",
"lsx" => "video/x-la-asf",
"lzh" => "application/octet-stream",
"m13" => "application/x-msmediaview",
"m14" => "application/x-msmediaview",
"m3u" => "audio/x-mpegurl",
"man" => "application/x-troff-man",
"mdb" => "application/x-msaccess",
"me" => "application/x-troff-me",
"mht" => "message/rfc822",
"mhtml" => "message/rfc822",
"mid" => "audio/mid",
"mny" => "application/x-msmoney",
"mov" => "video/quicktime",
"movie" => "video/x-sgi-movie",
"mp2" => "video/mpeg",
"mp3" => "audio/mpeg",
"mpa" => "video/mpeg",
"mpe" => "video/mpeg",
"mpeg" => "video/mpeg",
"mpg" => "video/mpeg",
"mpp" => "application/vnd.ms-project",
"mpv2" => "video/mpeg",
"ms" => "application/x-troff-ms",
"mvb" => "application/x-msmediaview",
"nws" => "message/rfc822",
"oda" => "application/oda",
"p10" => "application/pkcs10",
"p12" => "application/x-pkcs12",
"p7b" => "application/x-pkcs7-certificates",
"p7c" => "application/x-pkcs7-mime",
"p7m" => "application/x-pkcs7-mime",
"p7r" => "application/x-pkcs7-certreqresp",
"p7s" => "application/x-pkcs7-signature",
"pbm" => "image/x-portable-bitmap",
"pdf" => "application/pdf",
"pfx" => "application/x-pkcs12",
"pgm" => "image/x-portable-graymap",
"pko" => "application/ynd.ms-pkipko",
"pma" => "application/x-perfmon",
"pmc" => "application/x-perfmon",
"pml" => "application/x-perfmon",
"pmr" => "application/x-perfmon",
"pmw" => "application/x-perfmon",
"pnm" => "image/x-portable-anymap",
"pot" => "application/vnd.ms-powerpoint",
"ppm" => "image/x-portable-pixmap",
"pps" => "application/vnd.ms-powerpoint",
"ppt" => "application/vnd.ms-powerpoint",
"prf" => "application/pics-rules",
"ps" => "application/postscript",
"pub" => "application/x-mspublisher",
"qt" => "video/quicktime",
"ra" => "audio/x-pn-realaudio",
"ram" => "audio/x-pn-realaudio",
"ras" => "image/x-cmu-raster",
"rgb" => "image/x-rgb",
"rmi" => "audio/mid",
"roff" => "application/x-troff",
"rtf" => "application/rtf",
"rtx" => "text/richtext",
"scd" => "application/x-msschedule",
"sct" => "text/scriptlet",
"setpay" => "application/set-payment-initiation",
"setreg" => "application/set-registration-initiation",
"sh" => "application/x-sh",
"shar" => "application/x-shar",
"sit" => "application/x-stuffit",
"snd" => "audio/basic",
"spc" => "application/x-pkcs7-certificates",
"spl" => "application/futuresplash",
"src" => "application/x-wais-source",
"sst" => "application/vnd.ms-pkicertstore",
"stl" => "application/vnd.ms-pkistl",
"stm" => "text/html",
"svg" => "image/svg+xml",
"sv4cpio" => "application/x-sv4cpio",
"sv4crc" => "application/x-sv4crc",
"t" => "application/x-troff",
"tar" => "application/x-tar",
"tcl" => "application/x-tcl",
"tex" => "application/x-tex",
"texi" => "application/x-texinfo",
"texinfo" => "application/x-texinfo",
"tgz" => "application/x-compressed",
"tif" => "image/tiff",
"tiff" => "image/tiff",
"tr" => "application/x-troff",
"trm" => "application/x-msterminal",
"tsv" => "text/tab-separated-values",
"txt" => "text/plain",
"uls" => "text/iuls",
"ustar" => "application/x-ustar",
"vcf" => "text/x-vcard",
"vrml" => "x-world/x-vrml",
"wav" => "audio/x-wav",
"wcm" => "application/vnd.ms-works",
"wdb" => "application/vnd.ms-works",
"wks" => "application/vnd.ms-works",
"wmf" => "application/x-msmetafile",
"wps" => "application/vnd.ms-works",
"wri" => "application/x-mswrite",
"wrl" => "x-world/x-vrml",
"wrz" => "x-world/x-vrml",
"xaf" => "x-world/x-vrml",
"xbm" => "image/x-xbitmap",
"xla" => "application/vnd.ms-excel",
"xlc" => "application/vnd.ms-excel",
"xlm" => "application/vnd.ms-excel",
"xls" => "application/vnd.ms-excel",
"xlt" => "application/vnd.ms-excel",
"xlw" => "application/vnd.ms-excel",
"xof" => "x-world/x-vrml",
"xpm" => "image/x-xpixmap",
"xwd" => "image/x-xwindowdump",
"z" => "application/x-compress",
"zip" => "application/zip");

if(isset($_POST['cadparam'])) {
$camupload = $_POST['camupload'];
$dias = $_POST['dias'];
$checatparq = $_POST['tparq'];
$consta = array();
$nconsta = array();
foreach($checatparq as $ext) {
  if(array_key_exists($ext, $mimes)) {
    $consta[] = $ext;
  }
  else {
    $nconsta[] = $ext;
  }
}
$tamanho = $_POST['tamanho'];
$caracter = "[]$[><}{)(:,!?*%&#@]";

if($camupload == "" || $dias == "" || $_POST['tparq'] == "" || $tamanho == "") {
  $msgbook = "Todos campos devem estar preenchidoas para cadastro dos parÃ¢metros!!!";
  header("location:admin.php?menu=paramebook&msgbook=".$msgbook);
  break;
}
if(count($nconsta) == 1) {
  $nconsta = implode(",", $nconsta);
  $msgalt = "A extensÃ£o ''$nconsta'' não é uma extensão de arquivo válida, favor verificar!!!";
  header("location:admin.php?menu=paramebook&msgbook=".$msgalt);
  break;
}
if(count($nconsta) > 1) {
  $nconsta = implode(",", $nconsta);
  $msgalt = "As extensÃµes ''$nconsta'' nÃ£o sÃ£o extensÃµes de arquivos vÃ¡lidas, favor verificar!!!";
  header("location:admin.php?menu=paramebook&msgbook=".$msgalt);
  break;
}
if(!is_numeric($dias) OR !is_numeric($tamanho)) {
  $msgbook = "O campo dias e tamanho só podem conter números!!!";
  header("location:admin.php?menu=paramebook&msgbook=".$msgbook);
  break;
}
if(eregi($caracter, $tparq)) {
  $msgbook = "O campo tipo arquivos nÃ£o pode conter od caracteres ''$caracter''!!!";
  header("location:admin.php?menu=paramebook&msgbook=".$msgbook);
  break;
}
else {
  $verifcam = explode("/","$camupload");
  $camarq = $rais;
  foreach($verifcam as $pasta) {
    if(file_exists($camarq."/".$pasta)) {
    	if($pasta == "") {
    		$camarq= $camarq;
    	}
    	else {
    		$camarq = $camarq."/".$pasta;
    	}
    }
    else {
      mkdir($camarq."/".$pasta, 0777);
      $camarq = $camarq."/".$pasta;
    }
  }
  if($eselparam['result'] >= 1) {
    $ativo = 'N';
  }
  else {
    $ativo = 'S';
  }
  $cadparam = "INSERT INTO param_book (camarq, tipoarq, tamanho, dias) VALUE ('$camupload', '".implode(";",$checatparq)."', '$tamanho', '$dias')";
  $ecadparam = $_SESSION['query']($cadparam) or die (mysql_error());
  if($ecadparam) {
    $msgbook = "Cadastro de parametros efetuado com sucesso!!!";
    header("location:admin.php?menu=paramebook&msgbook=".$msgbook);
    break;
  }
  else {
    $msgbook = "Erro no processo de cadastramento, favor contatar o administrador!!!";
    header("location:admin.php?menu=paramebook&msgbook=".$msgbook);
    break;
  }
}
}

if(isset($_POST['altparam'])) {
$id = $_POST['id'];
$camupload = $_POST['camup'];
$dias = $_POST['dias'];
$tparq = trim(str_replace(" ","",$_POST['tparq']));
$checatparq = explode(";",$tparq);
$consta = array();
$nconsta = array();
foreach($checatparq as $ext) {
  if(array_key_exists($ext, $mimes)) {
    $consta[] = $ext;
  }
  else {
    $nconsta[] = $ext;
  }
}
$tamanho = $_POST['tamanho'];
$caracter = "[]$[><}{)(:,!?*%&#@]";
$selparam = "SELECT * FROM param_book WHERE idparam_book='$id'";
$eparam = $_SESSION['fetch_array']($_SESSION['query']($selparam));
if(count($nconsta) == 1) {
  $nconsta = implode(",", $nconsta);
  $msgalt = "A extensÃ£o ''$nconsta'' nÃ£o Ã© uma extensÃ£o de arquivo vÃ¡lida, favor verificar!!!";
  header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  break;
}
if(count($nconsta) > 1) {
  $nconsta = implode(",", $nconsta);
  $msgalt = "As extensÃµes ''$nconsta'' nÃ£o sÃ£o extensÃµes de arquivos vÃ¡lidas, favor verificar!!!";
  header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  break;
}
if($camupload == "" || $dias == "" || $tparq == "" || $tamanho == "") {
  $msgalt = "Todos campos devem estar preenchidoas para cadastro dos parÃ¢metros!!!";
  header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  break;
}
if(!is_numeric($dias) OR !is_numeric($tamanho)) {
  $msgalt = "O campo dias e tamanho sÃ³ podem conter nÃºmeros!!!";
  header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  break;
}
if(eregi($caracter, $tparq)) {
  $msgalt = "O campo tipo arquivos nÃ£o pode conter od caracteres ''$caracter''!!!";
  header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  break;
}
if($camupload == $eparam['camarq'] && $tparq == $eparam['tipoarq'] && $tamanho == $eparam['tamanho'] && $dias == $eparam['dias']) {
  $msgalt = "Nenhum campo foi alterado, operaÃ§Ã£o nÃ£o realizada!!!";
  header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  break;
}
else {
  $verifcam = explode("/","$camupload");
  $camarq = $rais;
  if($camupload == $eparam['camarq']) {
  }
  else {
    rmdir($rais.$eparam['camarq']);
  }
  foreach($verifcam as $pasta) {
    if(file_exists($camarq."/".$pasta)) {
    	if($pasta == "") {
    		$camarq= $camarq;
    	}
    	else {
    		$camarq = $camarq."/".$pasta;
    	}
    }
    else {
      mkdir($camarq."/".$pasta, 0777);
      $camarq = $camarq."/".$pasta;
    }
  }
  $update = "UPDATE param_book SET camarq='$camupload', tipoarq='$tparq', tamanho='$tamanho', dias='$dias' WHERE idparam_book='$id'";
  $eupdate = $_SESSION['query']($update);
  if($eupdate) {
  	$msgalt = "AlteraÃ§Ã£o realizada com sucesso!!!";
  	header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
  	break;
  }
  else {
    $msgalt = "Erro durane o procersso de alteraÃ§Ã£o, favor contatar o administrador!!!";
    header("location:admin.php?menu=paramebook&msgalt=".$msgalt);
    break;
  }
  }
}

if(isset($_POST['cadastra'])) {
  if($nome == "") {
    $msg = "O campo nome nÃ£o pode estar vazio, favor incluir um texto";
    header("location:admin.php?menu=paramebook&msg=".$msg);
    break;
  }
  $selcat = "SELECT COUNT(*) as result FROM cat_ebook WHERE nomecat_ebook='$nome'";
  $eselcat = $_SESSION['fetch_array']($_SESSION['query']($selcat)) or die (mysql_error());
  if($eselcat['result'] >= 1) {
    $msg = "JÃ¡ existe assunto cadastrada com este ''Nome'', favor cadastra outra!!!";
    header("location:admin.php?menu=paramebook&msg=".$msg);
    break;
  }
  else {
    $cadcat = "INSERT INTO cat_ebook (nomecat_ebook, iduser, tabuser, ativo) VALUE ('$nome', '$iduser', '".$_SESSION['user_tabela']."', 'S')";
    $ecadcat = $_SESSION['query']($cadcat) or die (mysql_error());
    if($ecadcat) {
      $msg = "Assunto cadastrada com sucesso!!!";
      header("location:admin.php?menu=paramebook&msg=".$msg);
      break;
    }
    else {
      $msg = "Erro durante o processo de cadastramento, favor contatar o administrador";
      header("location:admin.php?menu=paramebook&msg=".$msg);
      break;
    }
  }
}

if(isset($_POST['altera'])) {
  $id = $_POST['id'];
  $ativo = $_POST['ativo'];
  $iduser = $_POST['iduser'];
  $nomeuser = $_POST['user'];
  $selcountcat = "SELECT COUNT(*) as result FROM cat_ebook WHERE nomecat_ebook='$nome' AND idcat_ebook <> '$id'";
  $ecountcat = $_SESSION['fetch_array']($_SESSION['query']($selcountcat)) or die (mysql_error());
  $selcat = "SELECT nomecat_ebook, ativo FROM cat_ebook WHERE idcat_ebook='$id'";
  $eselcat = $_SESSION['fetch_array']($_SESSION['query']($selcat)) or die (mysql_error());
  if($nome == "") {
    $msgi = "O campo nome nÃ£o pode estar vazio, favor incluir um texto";
    header("location:admin.php?menu=paramebook&msgi=".$msgi);
    break;
  }
  if($nome == $eselcat['nomecat_ebook'] && $ativo == $eselcat['ativo']) {
    $msgi = "NÃ£o houve nenhuma alteraÃ§Ã£o nos campos, alteraÃ§Ã£o nÃ£o executada";
    header("location:admin.php?menu=paramebook&msgi=".$msgi);
    break;
  }
  if($ecountcat['result'] >= 1) {
    $msgi = "JÃ¡ existe assunto cadastrada com este nome, favor escolher outra";
    header("location:admin.php?menu=paramebook&msgi=".$msgi);
    break;
  }
  else {
    $update = "UPDATE cat_ebook SET nomecat_ebook='$nome', ativo='$ativo' WHERE idcat_ebook='$id'";
    $eupdate = $_SESSION['query']($update) or die (mysql_error());
    if($eupdate) {
      $msgi = "Cadastro alterado com sucesso!!!";
      header("location:admin.php?menu=paramebook&msgi=".$msgi);
      break;
    }
    else {
      $msgi = "Erro no processo de alteraÃ§Ã£o, favor contatar o administrador";
      header("location:admin.php?menu=paramebook&msgi=".$msgi);
      break;
    }
  }
}

if(isset($_POST['apaga'])) {
  $id = $_POST['id'];
  $nome = $_POST['nome'];
  $selebook = "SELECT COUNT(*) as result FROM ebook WHERE categoria='$nome'";
  $eselebook = $_SESSION['fetch_array']($_SESSION['query']($selebook)) or die (mysql_error());
  if($eselebook['result'] >= 1) {
    $msgi = "Esta assunto nÃ£o pode ser apagada pois jÃ¡ estÃ¡ relacionada com uma orientaÃ§Ã£o";
    header("location:admin.php?menu=paramebook&msgi=".$msgi);
    break;
  }
  else {
    $delcat = "DELETE FROM cat_ebook WHERE idcat_ebook='$id'";
    $edelcat = $_SESSION['query']($delcat) or die (mysql_error());
    $alt = "ALTER TABLE cat_ebook AUTO_INCREMENT=1";
    $ealt = $_SESSION['query']($alt) or die (mysql_error());
    if($edelcat) {
      $msgi = "Assunto apagada com sucesso!!!";
      header("location:admin.php?menu=paramebook&msgi=".$msgi);
      break;
    }
    else {
      $msgi = "Erro na execuÃ§Ã£o do processo, favor contatar o administrador";
      header("location:admin.php?menu=paramebook&msgi=".$msgi);
      break;
    }
  }
}

if(isset($_POST['cadastrasub'])) {
  if($nome == "") {
    $msgsub = "O campo nome nÃ£o pode estar vazio, favor incluir um texto";
    header("location:admin.php?menu=paramebook&msgsub=".$msgsub."#msgsub");
    break;
  }
  $selcat = "SELECT COUNT(*) as result FROM subebook WHERE nomesubebook='$nome'";
  $eselcat = $_SESSION['fetch_array']($_SESSION['query']($selcat)) or die (mysql_error());
  if($eselcat['result'] >= 1) {
    $msgsub = "JÃ¡ existe sub-assunto cadastrada com este ''Nome'', favor cadastra outra!!!";
    header("location:admin.php?menu=paramebook&msgsub=".$msgsub."#msgsub");
    break;
  }
  else {
    $cadcat = "INSERT INTO subebook (nomesubebook, iduser, tabuser, ativo) VALUE ('$nome', '$iduser', '$tabuser', 'S')";
    $ecadcat = $_SESSION['query']($cadcat) or die (mysql_error());
    if($ecadcat) {
      $msgsub = "Sub-Assunto cadastrada com sucesso!!!";
      header("location:admin.php?menu=paramebook&msgsub=".$msgsub."#msgsub");
      break;
    }
    else {
      $msgsub = "Erro durante o processo de cadastramento, favor contatar o administrador";
      header("location:admin.php?menu=paramebook&msgsub=".$msgsub."#msgsub");
      break;
    }
  }
}

if(isset($_POST['alterasub'])) {
  $id = $_POST['id'];
  $ativo = $_POST['ativo'];
  $iduser = $_POST['iduser'];
  $nomeuser = $_POST['user'];
  $selcountcat = "SELECT COUNT(*) as result FROM subebook WHERE nomesubebook='$nome' AND idsubebook <> '$id'";
  $ecountcat = $_SESSION['fetch_array']($_SESSION['query']($selcountcat)) or die (mysql_error());
  $selcat = "SELECT nomesubebook, ativo FROM subebook WHERE idsubebook='$id'";
  $eselcat = $_SESSION['fetch_array']($_SESSION['query']($selcat)) or die (mysql_error());
  if($nome == "") {
    $msgisub = "O campo nome nÃ£o pode estar vazio, favor incluir um texto";
    header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
    break;
  }
  if($nome == $eselcat['nomesubebook'] && $ativo == $eselcat['ativo']) {
    $msgisub = "NÃ£o houve nenhuma alteraÃ§Ã£o nos campos, alteraÃ§Ã£o nÃ£o executada";
    header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
    break;
  }
  if($ecountcat['result'] >= 1) {
    $msgisub = "JÃ¡ existe Sub-Assunto cadastrada com este nome, favor escolher outra";
    header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
    break;
  }
  else {
    $update = "UPDATE subebook SET nomesubebook='$nome', ativo='$ativo' WHERE idsubebook='$id'";
    $eupdate = $_SESSION['query']($update) or die (mysql_error());
    if($eupdate) {
      $msgisub = "Cadastro alterado com sucesso!!!";
      header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
      break;
    }
    else {
      $msgisub = "Erro no processo de alteraÃ§Ã£o, favor contatar o administrador";
      header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
      break;
    }
  }
}

if(isset($_POST['apagasub'])) {
  $id = $_POST['id'];
  $nome = $_POST['nome'];
  $selebook = "SELECT COUNT(*) as result FROM ebook WHERE subcategoria='$nome'";
  $eselebook = $_SESSION['fetch_array']($_SESSION['query']($selebook)) or die (mysql_error());
  if($eselebook['result'] >= 1) {
    $msgisub = "Esta sub-assunto nÃ£o pode ser apagada pois jÃ¡ estÃ¡ relacionada com uma orientaÃ§Ã£o";
    header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
    break;
  }
  else {
    $delcat = "DELETE FROM subebook WHERE idsubebook='$id'";
    $edelcat = $_SESSION['query']($delcat) or die (mysql_error());
    $alt = "ALTER TABLE subebook AUTO_INCREMENT=1";
    $ealt = $_SESSION['query']($alt) or die (mysql_error());
    if($edelcat) {
      $msgisub = "Sub-Assunto apagada com sucesso!!!";
      header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
      break;
    }
    else {
      $msgisub = "Erro na execuÃ§Ã£o do processo, favor contatar o administrador";
      header("location:admin.php?menu=paramebook&msgisub=".$msgisub."#msgsub");
      break;
    }
  }
}

?>