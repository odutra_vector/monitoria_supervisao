<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
?>

<script type="text/javascript">
	$(document).ready(function() {
            //$('#tabrel').tablesorter();
            $("tr[id*='tabconf']").hide();
            $("tr[id*='tabparam']").hide();
            var abre = false;
            $("select[name=filtro_m]").change(function() {
                $("select[name=filtro_f[]]").html('<option value="0">Carregando...</option>');

                $.post("carrfiltrof.php",{filtro_m:$(this).val()},function(valor) {
                     $("select[name=filtro_f[]]").html(valor);
                })
            })
            
            $("select[name=filtro]").change(function() {
                $("select[name=filtro_m]").html('<option value="0">Carregando...</option>');

                $.post("carrfiltrom.php",{filtro:$(this).val()},function(valor) {
                      $("select[name=filtro_m]").html(valor);
                })
            })

            $("input[id*='tdrel']").click(function() {
                var rel = $(this).attr('id');
                var idrel = rel.substr(5,6);
                if(abre == false) {
                    $('#tabconf'+idrel).show();
                    $('#tabparam'+idrel).show();
                    abre = true;
                }
                else {
                    $('#tabconf'+idrel).hide();
                    $('#tabparam'+idrel).hide();
                    abre = false;
                }
            })

            $('#vincusers').click(function() {
                var ids = "";
                $("input[id*='idrelcheck']").each(function() {
                    if($(this).attr('checked')) {
                        ids = ids+","+$(this).val();
                    }
                })
                ids = ids.substr(1);
                if(ids != "") {
                    var idspost = ids.split(",");
                    var cids = idspost.length;
                    if(cids >= 2) {
                        alert("Favor selecionar, somente 1 RELACIONAMENTO para alteração");
                        return false;
                    }
                    else {
                        var esquerda = (screen.width - 620)/2;
                        var topo = (screen.height - 400)/2;
                        window.open('relacionausers.php?idrel='+ids,"relaciona usuários",'height='+400+', width='+620+', top='+topo+', left='+esquerda+',scrollbars=YES,resizable=NO,Menubar=0,Status=0,Toolbar=0');
                    }
                }
                else {
                    alert("Favor selecionar 1 RELACIONAMENTO para alteração");
                    return false;
                }
            });
            
            $("#listar").click(function() {
                $("#listarel").load("cadrelestru.php",{listar:"listar",
                <?php
                $cloop = 0;
                $selfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                $eselfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
                $nrows = $_SESSION['num_rows']($eselfiltros);
                while($lsel = $_SESSION['fetch_array']($eselfiltros)) {
                    $cloop++;
                    if($cloop == $nrows) {
                        echo "id_".strtolower($lsel['nomefiltro_nomes']).":$('#".strtolower($lsel['nomefiltro_nomes'])."').val()";
                    }
                    else {
                        echo "id_".strtolower($lsel['nomefiltro_nomes']).":$('#".strtolower($lsel['nomefiltro_nomes'])."').val(),";
                    }
                }
                ?>
                })
            });
            
            $("#alterardias").click(function() {
                var dias = $('#dias').val();
                var ids = "";
                $("#idrel option:selected").each(function() {
                    ids = ids+","+$(this).val();
                })
                ids = ids.substr(1);
                if(dias == "") {
                    alert("Favor informar a quantidade de DIAS para alteração!!!");
                    return false;
                }
                else {
                    $.blockUI({ message: '<strong>AGUARDE EXECUTANDO AÇÃO...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }})
                    $.post("cadrelestru.php",{alterardias:'alterardias',dias:dias,idsrel:ids,
                    <?php
                    $cloop = 0;
                    $selfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                    $eselfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
                    $nrows = $_SESSION['num_rows']($eselfiltros);
                    while($lsel = $_SESSION['fetch_array']($eselfiltros)) {
                        $cloop++;
                        if($cloop == $nrows) {
                            echo "id_".strtolower($lsel['nomefiltro_nomes']).":$('#".strtolower($lsel['nomefiltro_nomes'])."').val()";
                        }
                        else {
                            echo "id_".strtolower($lsel['nomefiltro_nomes']).":$('#".strtolower($lsel['nomefiltro_nomes'])."').val(),";
                        }
                    }
                    ?>},function(retorno) {
                        var ret = retorno.split(",");
                        if(ret[0] == 0) {
                            if(ret[1] == 1) {
                                $("#spanaltera").html("ALTERAÇÃO EFETUADA COM SUCESSO, EM "+ret[1]+" RELACIONAMENTO");
                            }
                            else {
                                $("#spanaltera").html("ALTERAÇÃO EFETUADA COM SUCESSO, EM "+ret[1]+" RELACIONAMENTOS");
                            }
                        }
                        if(ret[0] == 1) {
                            $("#spanaltera").html("NEMHUM RELACIONAMENTO FOI ALTERADO");
                        }
                        if(ret[0] == 2) {
                            $("#spanaltera").html("OCORREU UM ERRO NO PROCESSO DE ALTERAÇÃO, FAVOR CONTATAR O ADMINISTRADOR");
                        }
                        $.unblockUI();
                    })
                }
            })
	})
</script>
<div id="cadrel" style="width: 1024px; float: left" class="corfd_pag">
    <form action="cadrelestru.php" method="post">
        <table width="323" border="0">
        <tr>
        <td class="corfd_ntab" colspan="2" align="center"><strong>RELACIONAMENTO DA ESTRUTURA DE FILTROS</strong></td>
        </tr>
        <?php
        $selcolum = "SHOW COLUMNS FROM rel_filtros";
        $ecolum = $_SESSION['query']($selcolum) or die (mysql_error());
        $numcoluns = $_SESSION['num_rows']($ecolum) or die (mysql_error());
        $coluns = $numcoluns;
        $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
        $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
        while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
            echo "<tr>";
            echo "<td width=\"78\" class=\"corfd_coltexto\"><strong>".$lfiltros['nomefiltro_nomes']."</strong></td>";
            $filtrodados = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".$lfiltros['idfiltro_nomes']."' AND ativo='S' ORDER BY nomefiltro_dados";
            $edados = $_SESSION['query']($filtrodados) or die (mysql_error());
                echo "<td width=\"235\" class=\"corfd_colcampos\"><select style=\"width:250px\" name=\"id".strtolower($lfiltros['nomefiltro_nomes'])."\"><option value=\"0\" disabled=\"disabled\" selected=\"selected\">Selecione um Filtro</option>";
            while($ldados = $_SESSION['fetch_array']($edados)) {
                echo "<option value=\"".$ldados['idfiltro_dados']."\" >".$ldados['nomefiltro_dados']."</option>";
            }
            echo "</select></td></tr>";

        }
        ?>
        <tr>
            <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="relaciona" type="submit" value="Relacionar" /></td>
        </tr>
        </table><font color="#FF0000"><strong><?php echo $_GET['msg']; ?></strong></font>
    </form>
</div>
<div style="float:left; width: 1024px" class="corfd_pag">
    <div id="filtrodias" style="width: 400px; float: left; padding-top: 20px">
        <span id="spanaltera" style="font-size:12px; color: red; font-weight: bold"></span>
        <table id="tabfiltrodias">
            <tr>
                <td class="corfd_ntab" colspan="2"><strong>ALTERA DIAS LIMITE DATA CTT</strong></td>
            </tr>
            <tr>
                <td class="corfd_coltexto"><strong>DIAS</strong></td>
                <td class="corfd_colcampos"><input name="dias" id="dias" type="text" value="" style="width:50px; border: 1px solid #69C; text-align:center" maxlength="3"/></td>
            </tr>
            <?php
            $selcolum = "SHOW COLUMNS FROM rel_filtros";
            $ecolum = $_SESSION['query']($selcolum) or die (mysql_error());
            $numcoluns = $_SESSION['num_rows']($ecolum) or die (mysql_error());
            $coluns = $numcoluns;
            $selfiltros = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
            $efiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
            while($lfiltros = $_SESSION['fetch_array']($efiltros)) {
                echo "<tr>";
                echo "<td width=\"78\" class=\"corfd_coltexto\"><strong>".$lfiltros['nomefiltro_nomes']."</strong></td>";
                $filtrodados = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".$lfiltros['idfiltro_nomes']."' AND ativo='S' ORDER BY nomefiltro_dados";
                $edados = $_SESSION['query']($filtrodados) or die (mysql_error());
                    echo "<td width=\"235\" class=\"corfd_colcampos\"><select style=\"width:250px\" name=\"".strtolower($lfiltros['nomefiltro_nomes'])."\" id=\"".strtolower($lfiltros['nomefiltro_nomes'])."\"><option value=\"\" disabled=\"disabled\" selected=\"selected\">Selecione um Filtro</option>";
                while($ldados = $_SESSION['fetch_array']($edados)) {
                    echo "<option value=\"".$ldados['idfiltro_dados']."\" >".$ldados['nomefiltro_dados']."</option>";
                }
                echo "</select></td></tr>";

            }
            ?>
            <tr>
                <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="listar" id="listar" type="button" value="Listar" /></td>
                <td><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="alterardias" id="alterardias" type="button" value="Alterar" /></td>
            </tr>
        </table>
    </div>
    <div id="listarel" style="width:570px; float: right; margin-top: 20px; margin-bottom: 40px; margin-right: 10px; height: 220px; border: 2px solid #999; overflow: auto">
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
        $("#altconf").click(function() {
            var ids = "";
            $("input[id*='idrelcheck']").each(function() {
                if($(this).attr('checked')) {
                    ids = ids+","+$(this).val();
                }
            })
            ids = ids.substr(1);
            if(ids != "") {
                var idspost = ids.split(",");
                var cids = idspost.length;
                if(cids >= 2) {
                    alert("Favor selecionar, somente 1 RELACIONAMENTO para alteração");
                    return false;
                }
                else {
                    window.location = "admin.php?menu=relestrudet&idrel="+ids;
                }
            }
            else {
                alert("Favor selecionar 1 RELACIONAMENTO");
                return false;
            }
        });
        
        $("#altera").click(function() {
            var ids = "";
            $("input[id*='idrelcheck']").each(function() {
                if($(this).attr('checked')) {
                    ids = ids+","+$(this).val();
                }
            })
            ids = ids.substr(1);
            if(ids != "") {
                var idspost = ids.split(",");
                var cids = idspost.length;
                if(cids >= 2) {
                    alert("Favor selecionar, somente 1 RELACIONAMENTO para alteração");
                    return false;
                }
                else {
                    $.post("cadrelestru.php",{altera:"altera",idrelcheck:ids,
                    <?php
                    $selfiltros = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                    $eselfiltros = $_SESSION['query']($selfiltros) or die (mysql_error());
                    $nrows = $_SESSION['num_rows']($eselfiltros);
                    while($lsel = $_SESSION['fetch_array']($eselfiltros)) {
                        $cloop++;
                        if($cloop == $nrows) {
                            echo "id_".strtolower($lsel['nomefiltro_nomes']).":$('#id_".strtolower($lsel['nomefiltro_nomes'])."'+ids).val()";
                        }
                        else {
                            echo "id_".strtolower($lsel['nomefiltro_nomes']).":$('#id_".strtolower($lsel['nomefiltro_nomes'])."'+ids).val(),";
                        }
                    }
                    ?>},function(retorno) {
                        if(retorno == 0) {
                            alert("Nenhum filtro do RELACIONAMENTO foi alterado, processo não executado");
                        }
                        if(retorno == 1) {
                            alert("Já existe outro RELACIONAMENTO com os mesmos filtros");
                        }
                        if(retorno == 2) {
                            alert("Alteração executada com sucesso!!!");
                        }
                        if(retorno == 3) {
                            alert("Erro no processo de alteração, favor contatar o Administrador");
                        }
                    })
                }
            }
            else {
                alert("Favor selecionar 1 RELACIONAMENTO");
                return false;
            }
        });
        
        $("#apaga").click(function() {
            var ids = "";
            $("input[id*='idrelcheck']").each(function() {
                if($(this).attr('checked')) {
                    ids = ids+","+$(this).val();
                }
            })
            ids = ids.substr(1);
            if(ids != "") {
                var confirma = confirm("VOCÊ TEM CERTEZA QUE DESEJA APAGAR O RELACIONAMENTO?");
                if(confirma) {
                    var idspost = ids.split(",");
                    var cids = idspost.length;
                    if(cids >= 2) {
                        alert("Favor selecionar, somente 1 RELACIONAMENTO para alteração");
                        return false;
                    }
                    else {
                        $.post("cadrelestru.php",{apaga:"apaga",idrelcheck:ids},function(retorno) {
                            if(retorno == 0) {
                                alert("Este RELACIONAMENTO não pode ser apagado, pois já possui monuitorias realizadas");
                            }
                            if(retorno == 1) {
                                alert("RELACIONAMENTO APAGADO COM SUCESSO");
                                window.location = "admin.php?menu=relestru";
                            }
                            if(retorno == 2) {
                                alert("Ocorreu um erro no processo, favor contatar o Administrador");
                            }
                        })
                    }
                }
                else {
                }
            }
            else {
                alert("Favor selecionar 1 RELACIONAMENTO");
                return false;
            }
        });
        
        $("#replicarelemail").click(function() {
            var esquerda = (screen.width - 912)/2;
            var topo = (screen.height - 350)/2;
            window.open('replicaemail.php',"Replica",'height=350, width=952, top='+topo+', left='+esquerda+',scrollbars=YES,resizable=NO,Menubar=0,Status=0,Toolbar=0');
            return false;
        })
    })
</script>
<div style="width: 1024px; height: 420px; overflow: auto; float: left" class="corfd_pag">
<table width="1020" border="0">
  <tr>
    <td class="corfd_ntab" colspan="<?php echo $coluns; ?>" align="center"><strong>RELACIONAMENTOS CADASTRADOS</strong></td>
  </tr>
</table>
<?php
$_SESSION['dadosexp'] = "<table width=\"1004\" border=\"0\" id=\"tabrel\">";
$_SESSION['dadosexp'] .= "<thead>";
?>
<table width="1004" border="0" id="tabrel">
    <thead>
      <?php
      $tam = (980 - 60);
      $_SESSION['dadosexp'] .= "<th align=\"center\">ID</th>";
      ?>
      <th class="corfd_coltexto" align="center"></th>
      <th class="corfd_coltexto" align="center" width="<?php echo $tam;?>">ID</th>
      <?php
      $selfiltros = "SELECT * FROM filtro_nomes ORDER BY nivel DESC";
      $eselfiltros = $_SESSION['query']($selfiltros) or die ("erro na consulta das colunas da tabela rel_fitlros");
      while($lfiltros = $_SESSION['fetch_array']($eselfiltros)) {
            $col[] = "id_".strtolower($lfiltros['nomefiltro_nomes']);
            $sqlnome[] = "id_".strtolower($lfiltros['nomefiltro_nomes']);
            $coluna = $lfiltros['nomefiltro_nomes'];
            $_SESSION['dadosexp'] .= "<th align=\"center\">$coluna</th>";
            ?>
            <th class="corfd_coltexto" align="center" width="<?php echo $tam;?>"><?php echo $coluna;?></th>
        <?php
      }
      $tam = $tam / count($col);
      $tabcol = array("conf_rel","param_moni");
      $coldesc = array("idconf_rel","idrel_filtros");
      $alteranomes["idfluxo"] = "nomefluxo";
      $alteranomes["idrel_aud"] = "relacionamento_auditoria";
      $alteranomes["idindice"] = "nomeindice";
      $alteranomes["idplanilha_moni"] = "planilha monitor";
      $alteranomes["idplanilha_web"] = "planilha web";
      foreach($tabcol as $tab) {
        $selcolconf = "SHOW COLUMNS FROM $tab";
        $ecolconf = $_SESSION['query']($selcolconf) or die (mysql_error());
        while($lcol = $_SESSION['fetch_array']($ecolconf)) {
            if(in_array($lcol['Field'],$cols) OR in_array($lcol['Field'],$coldesc)) {
            }
            else {
                if(key_exists($lcol['Field'],$alteranomes)) {
                    $cols[$alteranomes[$lcol['Field']]] = $lcol['Field'];
                    $_SESSION['dadosexp'] .= "<th align=\"center\">".$alteranomes[$lcol['Field']]."</th>";
                }
                else {
                    $cols[$lcol['Field']] = $lcol['Field'];
                    $_SESSION['dadosexp'] .= "<th align=\"center\">".$lcol['Field']."</th>";
                }
            }
        }
      }
      ?>
  </thead>
  <tbody>
  <?php
    $_SESSION['dadosexp'] .= "</thead>";
    $_SESSION['dadosexp'] .= "<tbody>";
    $idsrel = arvoreescalar('','');
    foreach($idsrel as $idrel) {
  	$relcad = "SELECT ".implode(",",$sqlnome).",rf.idrel_filtros, cr.tipoacesso,cr.ativo,cr.trocanome,cr.trocafiltro,cr.idparam_moni,cr.idconf_calib, cr.vdn,f.nomefluxo,
                    cr.idplanilha_moni,cr.idplanilha_web,cr.tipoacesso,cr.limita_meta,cr.diasctt,cr.obriga_aud,cr.idrel_aud,i.nomeindice,pm.tipomoni,pm.tipoimp,pm.tpaudio,pm.tpcompress,pm.tparquivo,
                    pm.filtro_dataimp, pm.filtro_ultmoni, pm.filtro_datafimimp,pm.filtro_dultimp,pm.filtro_limmulti,pm.checkfg,pm.ordemctt,pm.tiposervidor,pm.endereco,pm.loginftp,pm.senhaftp,
                    pm.camaudio, pm.ccampos,pm.cpartes,pm.conf,pm.hpa,pm.hmonitor,pm.semdados FROM rel_filtros rf
                    LEFT JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                    LEFT JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                    LEFT JOIN indice i ON i.idindice = pm.idindice
                    LEFT JOIN fluxo f ON f.idfluxo = cr.idfluxo
                    WHERE rf.idrel_filtros='$idrel' ORDER BY rf.idrel_filtros";
  	$ecad = $_SESSION['query']($relcad) or die (mysql_error());
	while($lcad = $_SESSION['fetch_array']($ecad)) {
            $_SESSION['dadosexp'] .= "<tr><td align=\"cenrer\">".$lcad['idrel_filtros']."</td>";
            if($lcad['ativo'] == "N") {
                 $color = "background-color:#EA7A81";
            }
            else {
                 $color = "background-color:#FFFFFF";
            }
            ?>
              <tr>
              <td><input type="checkbox" name="idrelcheck[]" id="idrelcheck" value="<?php echo $lcad['idrel_filtros'];?>" /></td>
              <td align="center" class="corfd_colcampos" style="<?php echo $color;?>"><?php echo $lcad['idrel_filtros'];?></td>
              <?php
              $linhas = count($sql);
              foreach($sqlnome as $colunas) {
              $idopdados = array();
              $selcount = "SELECT COUNT(*) as result FROM filtro_dados WHERE idfiltro_dados='".$lcad[$colunas]."'";
              $ecount = $_SESSION['fetch_array']($_SESSION['query']($selcount)) or die (mysql_error());
              if($lcad[$colunas] == 0000 OR $lcad[$colunas] == "") {
                  $sfiltro = "SELECT * FROM filtro_nomes WHERE nomefiltro_nomes='".str_replace("ID_", "", strtoupper($colunas))."'";
                  $esfiltro = $_SESSION['fetch_array']($_SESSION['query']($sfiltro)) or die (mysql_error());
                  ?>
                  <td align="center" class="corfd_colcampos"><select style="width:<?php echo $tamcol;?>px; text-align:center" name="<?php echo strtolower($colunas);?>[]" id="<?php echo strtolower($colunas).$lcad['idrel_filtros'];?>">
                  <option value="" selected="selected">Selecione</option>
                  <?php
                  $sdados = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".$esfiltro['idfiltro_nomes']."' AND ativo='S'";
                  $edados = $_SESSION['query']($sdados) or die (mysql_error());
                  while($ldados = $_SESSION['fetch_array']($edados)) {
                  ?>
                    <option value="<?php echo $ldados['idfiltro_dados'];?>"><?php echo $ldados['nomefiltro_dados'];?></option>
                  <?php
                  }
                  ?>
                  </select></td>
                <?php
                }
                else {
                $selnome = "SELECT idfiltro_dados, nomefiltro_dados FROM filtro_dados WHERE idfiltro_dados='".$lcad[$colunas]."'";
                $enome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                $_SESSION['dadosexp'] .= "<td align=\"center\">".$enome['nomefiltro_dados']."</td>";
                ?>
                  <td align="center" class="corfd_colcampos"><select style="width:<?php echo $tam;?>px; text-align:center" name="<?php echo strtolower($colunas);?>[]" id="<?php echo strtolower($colunas).$lcad['idrel_filtros'];?>">
                  <option value="<?php echo $enome['idfiltro_dados'];?>" selected="selected"><?php echo $enome['nomefiltro_dados'];?></option>
                  <?php
                  $iddb = array($enome['idfiltro_dados']);
                  $nomedb = array($enome['nomefiltro_dados']);
                  $idopdados['0000'] = 'NENHUM';
                  $sfiltros = "SELECT * FROM filtro_nomes WHERE nomefiltro_nomes='".str_replace("ID_", "", strtoupper($colunas))."' ORDER BY nivel DESC";
                  $esfiltros = $_SESSION['query']($sfiltros) or die (mysql_error());
                  while($lsfiltros = $_SESSION['fetch_array']($esfiltros)) {
                    $listdados = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='".$lsfiltros['idfiltro_nomes']."' AND ativo='S'";
                    $elist = $_SESSION['query']($listdados) or die (mysql_error());
                    while($lelist = $_SESSION['fetch_array']($elist)) {
                      $idopdados[$lelist['idfiltro_dados']] = $lelist['nomefiltro_dados'];
                    }
                  }
                  asort($idopdados);
                  foreach($idopdados as $id => $nome) {
                        if($id != $iddb) {
                        ?>
                        <option value="<?php echo $id;?>"><?php echo $nome;?></option>
                        <?php
                        }
                  }
                  ?>
                  </select></td>
                  <?php
                }
              }
              ?>
              </tr>
              <?php /* <tr id="tabconf<?php echo $lcad['idrel_filtros'];?>" style="background-color:#999">
                  <td></td>
                  <td colspan="<?php echo (count($col) + 1);?>">
                    <table width="1000">
                      <tr>
                          <td align="center" width="50" style="background-color:#FC9" colspan="10"><strong>DADOS CONFIGURAÇÃO</strong></td>
                      </tr>
                      <tr style="background-color:#FC9">
                        <td align="center" width="50"><strong>Ativo</strong></td>
                        <td align="center" width="50"><strong>Tr. Filtro</strong></td>
                        <td align="center" width="50"><strong>Tr. Nome</strong></td>
                        <td align="center" width="50"><strong>P. Moni</strong></td>
                        <td align="center" width="50"><strong>Conf. Calib.</strong></td>
                        <td align="center" width="70"><strong>VDN</strong></td>
                        <td align="center" width="150"><strong>Fluxo</strong></td>
                        <td align="center" width="60"><strong>Acesso</strong></td>
                        <td align="center" width="220"><strong>Plan. Moni</strong></td>
                        <td align="center"><strong>Plan. WEB</strong></td>
                      </tr>
                      <tr class="corfd_colcampos">
                        <td align="center"><?php echo $lcad['ativo'];?></td>
                        <td align="center"><?php echo $lcad['trocafiltro'];?></td>
                        <td align="center"><?php echo $lcad['trocanome'];?></td>
                        <td align="center"><?php echo $lcad['idparam_moni'];?></td>
                        <td align="center"><?php echo $lcad['idconf_calib'];?></td>
                        <td align="center"><?php echo $lcad['vdn'];?></td>
                        <td align="center"><?php echo $lcad['nomefluxo'];?></td>
                        <td align="center"><?php $acesso = array('L' => 'LOGIN', 'R' => 'RAMAL', 'V' => 'VDN'); echo $lcad['tipoacesso'];?></td>
                        <td align="center">
                        <?php
                        $planmoni = explode(",",$lcad['idplanilha_moni']);
                        if($lcad['idplanilha_moni'] == "") {
                        }
                        else {
                            foreach($planmoni as $planm) {
                                $selplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$planm'";
                                $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta do nome da planilha");
                                echo $eselplan['descriplanilha']."<br />";
                            }
                        }
                        ?></td>
                        <td align="center">
                        <?php
                        $planweb = explode(",",$lcad['idplanilha_web']);
                        if($lcad['idplanilha_web'] == "") {
                        }
                        else {
                            foreach($planweb as $planw) {
                                $selplan = "SELECT descriplanilha FROM planilha WHERE idplanilha='$planw'";
                                $eselplan = $_SESSION['fetch_array']($_SESSION['query']($selplan)) or die ("erro na query de consulta do nome da planilha");
                                echo $eselplan['descriplanilha'];
                            }
                        }
                        ?></td>
                      </tr>
                    </table>
                  </td>
              </tr>
              <tr id="tabparam<?php echo $lcad['idrel_filtros'];?>" style="background-color:#999">
                  <td></td>
                  <td colspan="<?php echo (count($col) + 1);?>">
                    <table width="1000">
                      <tr>
                          <td align="center" width="50" style="background-color:#FC9" colspan="12"><strong>DADOS PARAMETROS</strong></td>
                      </tr>
                      <tr style="background-color:#FC9">
                        <td align="center" width="100"><strong>Indice</strong></td>
                        <td align="center" width="80"><strong>Tipo Moni</strong></td>
                        <td align="center" width="80"><strong>Tipo Imp</strong></td>
                        <td align="center" width="50"><strong>Sep. Campos</strong></td>
                        <td align="center" width="50"><strong>Sep. Partes</strong></td>
                        <td align="center" width="50"><strong>Tipo Audio</strong></td>
                        <td align="center" width="50"><strong>Tipo Comp.</strong></td>
                        <td align="center" width="50"><strong>Data Imp</strong></td>
                        <td align="center" width="50"><strong>Ult. Moni</strong></td>
                        <td align="center" width="50"><strong>Dt. fim Imp.</strong></td>
                        <td align="center" width="50"><strong>Dias Imp.</strong></td>
                        <td align="center" width="50"><strong>Lim. Multi.</strong></td>
                      </tr>
                      <tr class="corfd_colcampos">
                        <td align="center"><?php echo $lcad['nomeindice'];?></td>
                        <td align="center"><?php $tipomoni = array('G' => 'GRAVAÇÃO','O' => 'ON-LINE'); echo $tipomoni[$lcad['tipomoni']];?></td>
                        <td align="center"><?php $tipoimp = array('LA' => 'LISTA AUDIO','A' => 'LEITURA AUDIO','SL' => 'SEM LISTA', 'L' => 'LISTA','I' => 'INTERNET'); echo $tipoimp[$lcad['tipoimp']];?></td>
                        <td align="center"><?php echo $lcad['ccampos'];?></td>
                        <td align="center"><?php echo $lcad['cpartes'];?></td>
                        <td align="center">
                        <?php
                        $tpaudio = explode(",",$lcad['tpaudio']);
                        foreach($tpaudio as $audio) {
                            echo $audio."<br />";
                        }
                        ?></td>
                        <td align="center">
                        <?php
                        $tipocomp = explode(",",$lcad['tpcompress']);
                        foreach($tipocomp as $tcomp) {
                            echo $tcomp."<br />";
                        }
                        ?></td>
                        <td align="center"><?php echo $lcad['filtro_dataimp'];?></td>
                        <td align="center"><?php echo $lcad['filtro_ultmoni'];?></td>
                        <td align="center"><?php echo $lcad['filtro_datafimimp'];?></td>
                        <td align="center"><?php echo $lcad['filtro_dultimp'];?></td>
                        <td align="center"><?php echo $lcad['filtro_limmulti'];?></td>
                      </tr>
                    </table>
                  </td>
              </tr> */
              foreach($cols as $colexp => $colname) {
                  if($colname == "idplanilha_moni" OR $colname == "idplanilha_web") {
                      $nomesplans = array();
                      if($lcad[$colname] == "") {
                          $_SESSION['dadosexp'] .= "<td align=\"center\"></td>";
                      }
                      else {
                        $plans = explode(",",$lcad[$colname]);
                        foreach($plans as $p) {
                            $selplans = "SELECT descriplanilha FROM planilha WHERE idplanilha='$p'";
                            $eselplans = $_SESSION['fetch_array']($_SESSION['query']($selplans)) or die (mysql_error());
                            $nomesplans[] = $eselplans['descriplanilha'];
                        }
                        $_SESSION['dadosexp'] .= "<td align=\"center\">".implode(",",$nomesplans)."</td>";
                      }
                  }
                  else {
                    $_SESSION['dadosexp'] .= "<td align=\"center\">".$lcad[$colexp]."</td>";
                  }
              }
              $_SESSION['dadosexp'] .= "</tr>";
	}
    }
    $_SESSION['dadosexp'] .= "</tbody>";
    $_SESSION['dadosexp'] .= "</table>";
  ?>
    </tbody>
</table>
</div>
<div style="float:left; width: 1024px" class="corfd_pag">
    <table width="1000">
        <tr>
            <td width="60%"><input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altconf" id="altconf" type="submit" value="Alt. Config." /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="altera" id="altera" type="submit" value="Alt. Rel." /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="vincusers" id="vincusers" type="button" value="Vincula Usuários" /> <input style="border: 1px solid #fff; height: 18px; background-image:url(../images/button.jpg)" name="replicarelemail" id="replicarelemail" type="submit" value="Replicar Rel. E-mail" /> <input style="border: 1px solid #FFF; height: 18px; background-image:url(../images/button.jpg)" name="apaga" id="apaga" type="submit" value="Apaga Rel." /> </td>
            <td align="left" width="25%"><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=relacionamentos"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333;">LISTA RELACIONAMENTO</div></a></td>
            <td><form action="/monitoria_supervisao/admin/listaemail.php" method="post"><input style="text-decoration:none;text-align: center; color: #000;" id="alistaemail" class="botao" type="submit" value="LISTA DE EMAIL"></form></td>
        </tr>
    </table>
    <font color="#FF0000"><strong><?php echo $_GET['msgi']; ?></strong></font>
</div>
