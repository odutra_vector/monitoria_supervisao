<script type="text/javascript">
    $(document).ready(function() {
        $("#alterar").click(function() {
            var ids = "";
            $("input[id*='idrel']").each(function() {
                if($(this).attr('checked')) {
                    ids = ids+","+$(this).val();
                }
            });
            if(ids == "") {
                alert("Favor selecionar pelo menos 1 registro");
                return false;
            }
            else {
                var i = 0;
                var idsarray = ids.split(",");
                var erro = 0;
                for(i; i < idsarray.length;i++) {
                    if($("#qtdedia"+idsarray[i]).val() == "" || $("#dmenos"+idsarray[i]).val() == "") {
                        erro++;
                    }
                }
                if(erro >= 1) {
                    alert("A Quantidade e os Dias devem estar preenchidos em todos os registros que serão alterados");
                    return false;
                }
            }
        });
    });
</script>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<br/>
<span style="color: #EA071E; font-size: 12px"><strong><?php echo $_GET['msg'];?></strong></span>
<form action="cadselecao.php" method="post">
<div style=" height:500px; overflow:auto; border: 1px solid #999;margin-top: 10px"><br/>
    <table width="927" id="tabselecao">
        <tr>
            <td class="corfd_ntab" align="center" colspan="5"><strong>CONFIGURAÇÕES DE SELEÇÃO CADASTRADAS</strong></td>
        </tr>
        <tr>
            <td align="center" width="26" class="corfd_coltexto"></td>
            <td width="23" align="center" class="corfd_coltexto"><strong>ID</strong></td>
            <td width="645" align="center" class="corfd_coltexto"><strong>RELACIONAMENTO</strong></td>
            <td width="83" align="center" class="corfd_coltexto"><strong>QTDE DIA</strong></td>
            <td width="126" align="center" class="corfd_coltexto"><strong>DIAS MEMOS</strong></td>
        </tr>
        <?php
        $idsrel = arvoreescalar("", $_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
        if($idsrel == "") {
            ?>
            <tr class="corfd_colcampos">
                <td colspan="5" style=" font-weight: bold; text-align: center ">NÃO EXISTEM RELACIONAMENTO VINCULADOS AO PERFIL DO USUÁRIO</td>
            </tr>
            <?php
        }
        else {
            $colunas = array();
            $selcolunas = "SHOW COLUMNS FROM configselecao";
            $ecolunas = $_SESSION['query']($selcolunas) or die (mysql_error());
            while($lcolunas = $_SESSION['fetch_array']($ecolunas)) {
                $colunas[] = $lcolunas['Field'];
            }
            $selconfig = "SELECT * FROM configselecao";
            $eselconfig = $_SESSION['query']($selconfig) or die (mysql_error());
            $nconfig = $_SESSION['num_rows']($eselconfig);
            if($nconfig >= 1) {
                while($lselconfig = $_SESSION['fetch_array']($eselconfig)) {
                    foreach($colunas as $colarray) {
                        $idsselecao[$lselconfig['idrel_filtros']][$colarray] = $lselconfig[$colarray];   
                    }
                }
            }
            foreach($idsrel as $idrel) {
                if($idsselecao[$idrel] != "") {
                    ?>
                    <tr class="corfd_colcampos">
                    <td align="center"><input type="checkbox" name="idrel[]" id="idrel" value="<?php echo $idrel;?>" /></td>
                    <?php
                    foreach($colunas as $col) {
                        if($col == "idconfigselecao" || $col == "idrel_filtros") {
                            ?>
                            <td <?php if($col == "idconfigselecao") { echo "align=\"center\"";} else { echo "style=\"padding-left:20px\""; }?>><?php if($col == "idrel_filtros") { echo nomeapres($idsselecao[$idrel][$col]); } else { echo $idsselecao[$idrel][$col];}?></td>
                            <?php
                        }
                        else {
                            if($col == "qtdedia") {
                                ?>
                                <td align="center"><input name="qtdedia<?php echo $idrel;?>" id="qtdedia<?php echo $idrel;?>" value="<?php echo $idsselecao[$idrel][$col];?>" style="border: 1px solid #9CF; text-align:center; width:50px" maxlength="4" /></td>
                                <?php
                            }
                            else {
                                ?>
                                <td align="center"><input name="dmenos<?php echo $idrel;?>" id="dmenos<?php echo $idrel;?>" value="<?php echo $idsselecao[$idrel][$col];?>" style="border: 1px solid #9CF; text-align:center; width:50px" maxlength="4" /></td>
                                <?php
                            }
                        }
                    }
                }
            }
        }
        ?>
    </table>
</div>
<div>
    <ul style="margin-left:-25px">
       <li style="list-style:none;"><input style="border: 1px solid #FFF; height: 18px; background-image:url(/monitoria_supervisao/images/button.jpg)" name="alterar" id="alterar" type="submit" value="ALTERAR" /></li>
    </ul>
    <br/>
</div>
</form>
