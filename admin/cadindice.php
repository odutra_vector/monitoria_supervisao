<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['cadastra'])) {
    $nome = strtoupper($_POST['nome']);
    $valini = trim(str_replace(",", ".", $_POST['valini']));
    $valfim = trim(str_replace(",", ".", $_POST['valfim']));

    $verifnome = "SELECT COUNT(*) as result FROM indice WHERE nomeindice='$nome'";
    $everifinome = $_SESSION['query']($verifnome) or die ("erro na query de consulta de indices");
    if($nome == "" OR $valini == "" OR $valfim == "") {
        $msg = "O cadastro nÃ£o pode ser efetuado com nenhum campo vazio";
        header("location:admin.php?menu=indice&msg=".$msg);
    }
    else {
        if($everifnome['result'] >= 1) {
            $msg = "Este nome de indice jÃ¡ estÃ¡ cadastrado, favor escolhe outro!!!";
            header("location:admin.php?menu=indice&msg=".$msg);
        }
        else {
            if(!is_numeric($valini) OR !is_numeric($valfim)) {
                $msg = "Os campos ''Valor Inicio'' e ''Valor Fim'' devem ser nÃºmeros separado por ponto";
                header("location:admin.php?menu=indice&msg=".$msg);
            }
            else {
                if(eregi(",",$valfim) OR eregi(",",$valfim)) {
                    $msg = "Os campos ''Valor Inicio'' ou ''Valor Fim'' estÃ£o com o caracter de separaÃ§Ã£o errado";
                    header("location:admin.php?menu=indice&msg=".$msg);
                }
                else {
                    $cadindice = "INSERT INTO indice (nomeindice, inicio, fim) VALUES ('$nome', '$valini', '$valfim')";
                    $ecad = $_SESSION['query']($cadindice) or die ("erro na query de cadastramento do indice");
                    if($ecad) {
                        $msg = "O cadastro foi efetuado com sucesso";
                        header("location:admin.php?menu=indice&msg=".$msg);
                    }
                    else {
                        $msg = "Erro no processo de acadastramento, favor contatar o administrador";
                        header("location:admin.php?menu=indice&msg=".$msg);
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $id = $_POST['id'];
    $nome = strtoupper($_POST['nome']);
    $valini = trim(str_replace(",", ".", $_POST['valini']));
    $valfim = trim(str_replace(",", ".", $_POST['valfim']));

    $verifdados = "SELECT * FROM indice WHERE idindice='$id'";
    $edados = $_SESSION['fetch_array']($_SESSION['query']($verifdados)) or die ("erro na consulta dos dados do indice");
    $verifnome = "SELECT COUNT(*) as result FROM indice WHERE nomeindice='$nome' AND idindice<>'$id'";
    $everif = $_SESSION['fetch_array']($_SESSION['query']($verifnome)) or die ("erro na query de consutla dos indices");
    if($nome == "" OR $valini == "" OR $valfim == "") {
        $msgi = "A alteraÃ§Ã£o de cadastro nÃ£o pode ser efetuada com campos vazios";
        header("location:admin.php?menu=indice&msgi=".$msgi);
    }
    else {
        if($everif['result'] >= 1) {
            $msgi = "O nome informado jÃ¡ estÃ¡ cadastrado, favor informar outro";
            header("location:admin.php?menu=indice&msgi=".$msgi);
        }
        else {
            if(!is_numeric($valini) OR !is_numeric($valfim)) {
                $msg = "Os campos ''Valor Inicio'' e ''Valor Fim'' devem ser nÃºmeros separado por ponto";
                header("location:admin.php?menu=indice&msg=".$msg);
            }
            else {
                if($nome == $edados['nomeindice'] && $valini == $edados['inicio'] && $valfim == $edados['fim']) {
                    $msgi = "Nenhum campo foi alterado processo nÃ£o executado";
                    header("location:admin.php?menu=indice&msgi=".$msgi);
                }
                else {
                    $update = "UPDATE indice SET nomeindice='$nome', inicio='$valini', fim='$valfim' WHERE idindice='$id'";
                    $eupdate = $_SESSION['query']($update) or die ("erro na query de atualizaÃ§Ã£o");
                    if($eupdate) {
                        $msgi = "Cadastro atualizado com sucesso";
                        header("location:admin.php?menu=indice&msgi=".$msgi);
                    }
                    else {
                        $msgi = "Erro no processo de atualizaÃ§Ã£o, favor contatar o administrador";
                        header("location:admin.php?menu=indice&msgi=".$msgi);
                    }
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $id = $_POST['id'];
    $selinter = "SELECT COUNT(*) as result FROM intervalo_notas WHERE idindice='$id'";
    $eselinter = $_SESSION['fetch_array']($_SESSION['query']($selinter)) or die ("erro na query de consulta dos intervalos cadastrados");
    $selparam = "SELECT COUNT(*) as result FROM param_moni WHERE idindice='$id'";
    $eselparam = $_SESSION['fetch_array']($_SESSION['query']($selparam)) or die ("erro na query de consulta dos parametros");
    if($eselinter['result'] >= 1) {
        $msgi = "O Indice nÃ£o pode ser apagado pois existem intervalos relacionados Ã  ele";
        header("location:admin.php?menu=indice&msgi=".$msgi);
    }
    else {
        if($eselparam['result'] >= 1) {
            $msgi = "O Indice nÃ£o pode ser apagado pois existem parametros de monitoria relacionados ao mesmo";
            header("location:admin.php?menu=indice&msgi=".$msgi);
        } 
        else {
            $delind = "DELETE FROM indice WHERE idindice='$id'";
            $edelind = $_SESSION['query']($delind) or die ("erro na query para apagar o indice");
            $incre = "ALTER TABLE indice AUTO_INCREMENT=1";
            $eincre = $_SESSION['query']($incre) or die ("erro na query para reset do Auto Incremento");
            if($edelind) {
                $msgi = "Indice apagado com sucesso!!!";
                header("location:admin.php?menu=indice&msgi=".$msgi);
            }
            else {
                $msgi = "Erro no processo para apagar o registro, favor contatar o administrador";
                header("location:admin.php?menu=indice&msgi=".$msgi);
            }
        }
    }
}

if(isset($_POST['cadfaixa'])) {
    $idindice = $_POST['indice'];
    $nome = strtoupper($_POST['nome']);
    $inicio = trim(str_replace(",", ".", $_POST['notaini']));
    $fim = trim(str_replace(",", ".", $_POST['notafim']));

    if($idindice != "") {
        $selindice = "SELECT inicio, fim FROM indice WHERE idindice='$idindice'";
        $eselindice = $_SESSION['fetch_array']($_SESSION['query']($selindice)) or die ("erro na query de consulta do indice");
        $iniind = $eselindice['inicio'];
        $fimind = $eselindice['fim'];
        $countfaixa = "SELECT COUNT(*) as result FROM intervalo_notas WHERE idindice='$idindice'";
        $ecount = $_SESSION['fetch_array']($_SESSION['query']($countfaixa)) or die ("erro na query de consulta para contagem de faixas");
        if($ecount['result'] >= 1) {
            $selfaixas = "SELECT * FROM intervalo_notas WHERE idindice='$idindice'";
            $eselfaixas = $_SESSION['query']($selfaixas) or die ("erro na query de consulta das faixas");
        }
    }
    if($indice == "" && $nome == "" && $inicio == "" && $fim == "") {
        $msgf = "Todos campos devem estar preenchidos para efetuar o cadastro!!!";
        header("location:admin.php?menu=indice&msgf=".$msgf);
    }
    else {
        if($everifnome['result'] >= 1) {
          $msgf = "JÃ¡ existe faixa de nota cadastrada neste indice com o mesmo nome!!!";
          header("location:admin.php?menu=indice&msgf=".$msgf);
        }
        else {
            if($inicio < $iniind OR $fim > $fimind) {
              $msgf = "Os Valores nÃ£o podem ser cadastrados, pois estÃ£o divergentes da faixa de nota informada para o indice escolhido!!!";
              header("location:admin.php?menu=indice&msgf=".$msgf);
            }
            else {
                if(!is_numeric($inicio) OR !is_numeric($fim)) {
                    $msgf = "Os valores informandos nos campos ''Nota Ini'' ou ''Nota Fim'' devem ser nÃºmeros separados pelo caracter ponto!!!";
                    header("location:admin.php?menu=indice&msgf=".$msgf);
                }
                else {
                    if($ecount['result'] >= 1) {
                        $val = 0;
                        while($lfaixas = $_SESSION['fetch_array']($eselfaixas)) {
                            if($inicio == $lfaixas['numini'] OR $inicio == $lfaixas['numfim'] OR $fim == $lfaixas['numini'] OR $fim == $lfaixas['numfim']) {
                              $val =++ $val;
                            }
                            if(($inicio > $lfaixas['numini'] && $inicio < $lfaixas['numfim']) OR ($fim > $lfaixas['numini'] && $fim < $lfaixas['numfim'])) {
                              $val =++ $val;
                            }
                        }
                        if($val != 0) {
                            $msgf = "Os Valores nÃ£o podem ser cadastrados, pois jÃ¡ existe faixa de nota cadastrada neste intervalo!!!";
                            header("location:admin.php?menu=indice&msgf=".$msgf);
                        }
                        else {
                            $cadfaixa = "INSERT INTO intervalo_notas (idindice, numini, numfim, nomeintervalo_notas) VALUES ('$idindice', '$inicio', '$fim', '$nome')";
                            $ecad = $_SESSION['query']($cadfaixa) or die ("erro na query de insert dos dados");
                            if($ecad) {
                                  $msgf = "Cadastro efetuado com sucesso!!!";
                                  header("location:admin.php?menu=indice&msgf=".$msgf);
                            }
                            else {
                                  $msgf = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                                  header("location:admin.php?menu=indice&msgf=".$msgf);
                            }
                        }
                    }
                    else {
                        $cadfaixa = "INSERT INTO intervalo_notas (idindice, numini, numfim, nomeintervalo_notas) VALUES ('$idindice', '$inicio', '$fim', '$nome')";
                        $ecad = $_SESSION['query']($cadfaixa) or die ("erro na query de insert dos dados");
                        if($ecad) {
                            $msgf = "Cadastro efetuado com sucesso!!!";
                            header("location:admin.php?menu=indice&msgf=".$msgf);
                        }
                        else {
                            $msgf = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                            header("location:admin.php?menu=indice&msgf=".$msgf);
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['altfaixa'])) {
    $id = $_POST['id'];
    $nome = strtoupper($_POST['nome']);
    $nini = trim(str_replace(",", ".", $_POST['notaini']));
    $nfim = trim(str_replace(",", ".", $_POST['notafim']));
    $idindice = $_POST['idindice'];

    $selinter = "SELECT * FROM intervalo_notas WHERE idintervalo_notas='$id'";
    $einter = $_SESSION['fetch_array']($_SESSION['query']($selinter)) or die ("erro na consulta do intervalo");
    $selfaixas = "SELECT * FROM intervalo_notas WHERE idindice='$idindice' AND idintervalo_notas<>'$id'";
    $eselfaixas = $_SESSION['query']($selfaixas) or die ("erro na query de consulta das faixas");
    if($nome == "" && $nini == "" && $nfim == "") {
      $msgfi = "Todos os campos devem estar preenchidos para alteraÃ§Ã£o do intervalo!!!";
      header("location:admin.php?menu=indice&msgfi=".$msgfi);
    }
    else {
        if($nome == $einter['nomeintervalo_notas'] && $nini == $einter['numini'] && $nfim == $einter['numfim'] && $idindice == $einter['idindice']) {
          $msgfi = "Nenhum campo foi alterado, processo de atualizaÃ§Ã£o nÃ£o executado!!!";
          header("location:admin.php?menu=indice&msgfi=".$msgfi);
        }
        else {
            if(!is_numeric($nini) OR !is_numeric($nfim)) {
                $msgf = "Os valores informandos nos campos ''Nota Ini'' ou ''Nota Fim'' devem ser nÃºmeros separados pelo caracter ponto!!!";
                header("location:admin.php?menu=indice&msgf=".$msgf);
            }
            else {
                $val = 0;
                while($lselfaixas = $_SESSION['fetch_array']($eselfaixas)) {
                    if($nini == $lselfaixas['numini'] OR $nfim == $lselfaixas['numini'] OR $nfim == $lselfaixas['numini'] OR $nfim == $lselfaixas['numfim']) {
                        $val =++ $val;
                    }
                    if(($nini > $lselfaixas['numini'] && $nini < $lselfaixas['numfim']) OR ($nfim > $lselfaixas['numini'] && $nfim < $lselfaixas['numfim'])) {
                        $val =++ $val;  
                    }
                }
                if($val != 0) {
                    $msgfi = "Os Valores nÃ£o podem ser alterados, pois jÃ¡ existe faixa de nota cadastrada neste intervalo!!!";
                    header("location:admin.php?menu=indice&msgfi=".$msgfi);
                }
                else {
                    $update = "UPDATE intervalo_notas SET idindice='$idindice', numini='$nini', numfim='$nfim', nomeintervalo_notas='$nome' WHERE idintervalo_notas='$id'";
                    $eupdate = $_SESSION['query']($update) or die ("erro na query de UPDATE");
                    if($eupdate) {
                        $msgfi = "Intervalo atualizado com sucesso!!!";
                        header("location:admin.php?menu=indice&msgfi=".$msgfi);
                    }
                    else {
                        $msgfi = "Erro na atualizaÃ§Ã£o do intervalor, favor contatar o administrador!!!";
                        header("location:admin.php?menu=indice&msgfi=".$msgfi);
                    }
                }
            }
        }
    }
}

if(isset($_POST['apagafaixa'])) {
    $id = $_POST['id'];
    $delf = "DELETE FROM intervalo_notas WHERE idintervalo_notas='$id'";
    $edelf = $_SESSION['query']($delf) or die ("erro na query ''DELETE'' do item selecionado");
    $incre = "ALTER TABLE intervalo_notas AUTO_INCREMENT=1";
    $eincre = $_SESSION['query']($incre) or die ("erro na query para reset do Auto Incremento");
    if($edelf) {
        $msgfi = "Intervalo de nota apagado com sucesso!!!";
        header("location:admin.php?menu=indice&msgfi=".$msgfi);
    }
    else {
        $msgfi = "Erro no processo para apagar o intervalo, favor contatar o administrador!!!";
        header("location:admin.php?menu=indice&msgfi=".$msgfi);
    }
}

?>