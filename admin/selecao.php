<?php

include_once($rais.'/monitoria_supervisao/admin/visufiltros.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');

$tabsql = new TabelasSql();
$geturl = explode("*",$_GET['filtros']);
foreach($geturl as $get) {
    if($get != "") {
        $part = explode("-",$get);
        $getfiltros["filtro".substr($part[0],1)] = $part[1];
    }
}

if($_SESSION['user_tabela'] == "user_adm") {
    $tabuser = "user_adm";
    $part = explode("_",$tabuser);
    $part = $part[1];
    $tabreluser = "useradmfiltro";
    $atabuser = "a";
    $atabreluser = "af";
}
if($_SESSION['user_tabela'] == "user_web") {
    $tabuser = "user_web";
    $part = explode("_",$tabuser);
    $part = $part[1];
    $tabreluser = "userwebfiltro";
    $atabuser = "w";
    $atabreluser = "wf";
}
?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link href="../styleadmin.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    #liarqorigem:hover {
        background:#EACFA6;
    }
</style>
<script type="text/javascript" src="users/combofiltros_<?php echo strtolower($_SESSION['nomecli']);?>.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
            $('#bases').hide();
            <?php
            if(isset($_POST['pesquisa']) || isset($_POST['gerar'])) {
                echo "$('#spanmsg').hide();\n";
            }
            if($_SESSION['user_tabela'] == "user_adm" OR ($_SESSION['user_tabela'] == "user_web" && $_SESSION['selecao'] == "S")) {
                if($_SESSION['user_tabela'] == "user_adm") {
                    echo "$('#bases').show();\n";
                }
                echo "$('#dadosselecao').hide();\n";
                echo "$('#relatorio').hide();\n";
                echo "$('#divperiodo').hide();\n";
                echo "$('#pesquisa').hide();\n";
                echo "$('#gerar').hide();\n";
                echo "$('#trperiodo').hide();\n";
                echo "$('#filtros').hide();\n";
                echo "$('#divenvio').hide();\n";
            }
            if(!isset($_SESSION['selecao']) && $_SESSION['user_tabela'] == "user_web") {
                echo "$('#opcoes').hide();\n";
                echo "$('#divperiodo').hide();\n";
                echo "$('#trperiodo').hide();\n";
                echo "$('#divenvio').hide();\n";
            }
            if($_GET['op'] == "dadosselecao" || $_POST['opcaopesq'] == "dadosselecao") {
                echo "$('#filtros').show();\n";
                echo "$('#dadosselecao').show();\n";
                echo "$('#pesquisa').show();\n";
                echo "$('#divperiodo').hide();\n";
                echo "$('#trperiodo').hide();\n";
                echo "$('#gerar').hide();\n";
                echo "$('#bases').hide();\n";
                echo '$("#opcao option:eq(1)").attr("selected","selected");'."\n";
                echo "$('#opcaopesq').attr('value','dadosselecao');\n";
            }
            if($_GET['op'] == "relatorio" || $_POST['opcaopesq'] == "relatorio") {
                echo "$('#filtros').show();\n";
                echo "$('#relatorio').show();\n";
                echo "$('#gerar').show();\n";
                echo "$('#trperiodo').show();\n";
                echo "$('#divrel').hide();\n";
                echo "$('#pesquisa').hide();\n";
                echo "$('#divperiodo').hide();\n";
                echo "$('#divenvio').hide();\n";
                echo '$("#opcao option:eq(2)").attr("selected","selected");'."\n";
                echo "$('#opcaopesq').attr('value','relatorio');\n";
            }
            ?>
            $('#opcao').change(function() {
                var op = $(this).val();
                $('#filtros').show();
                if(op == "dadosselecao") {
                    $('#dadosselecao').show();
                    $('#pesquisa').show();
                    $('#divenvio').hide();
                    $("#trperiodo").hide();
                    $('#divperiodo').hide();
                    $('#dadosorigem').hide();
                    $('#relatorio').hide();
                    $('#gerar').hide();
                    $('#bases').hide();
                    $('#opcaopesq').attr('value','dadosselecao');
                }
                if(op == "relatorio") {
                    $('#dadosselecao').hide();
                    $('#pesquisa').hide();
                    $('#divenvio').hide();
                    $('#divperiodo').hide();
                    $('#dadosorigem').hide();
                    $('#relatorio').show();
                    $("#trperiodo").show();
                    $("#bases").show();
                    $('#gerar').show();
                    $('#opcaopesq').attr('value','relatorio');
                }
            });

            $("a[id*='alink']").click(function() {
                var idrel = $(this).attr('name');
                var url = $("#urlfiltros"+idrel).val();
                $('#divenvio').show();
                $('#divperiodo').show();
                $('#dadosorigem').show();
                $("#idrelselecao").attr('value',idrel);
                $("#envfiltros").attr('value',url);
                $("li[id*='liselecao']").css('background-color','#FFFFFF');
                $("#liselecao"+idrel).css('background-color','#FFCC99');
                $("#periodo option:eq(0)").attr("selected","selected");
                $("#dadosorigem").hide();
            });
            
            $("#periodo").change(function() {
                var idrel = $("#idrelselecao").val();
                var idperiodo = $(this).val();
                $("#dadosorigem").show();
                $("#dadosorigem").load("/monitoria_supervisao/admin/histselecao.php",{idrel:idrel,idperiodo:idperiodo});
            });
            
            $("#enviar").click(function() {
                var idrel = $("#idrelselecao").val();
                var arquivo = $("#arquivo").val();
                if(idrel == "" || arquivo == "") {
                    alert("Favor escolher o arquivo para envio");
                    return false;
                }
                else {
                    $.blockUI({ message: '<strong>AGUARDE ANALISANDO DADOS E GERANDO SELEÇÃO...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }})
                }
            });
            
            $("#gera").click(function() {
                var idper = $("#periodorel").val();
                if(idper == "") {
                    alert("Favor escolher o período do relatório");
                    return false;
                }
                else {
                    $.blockUI({ message: '<strong>AGUARDE GERANDO RELATÓRIO...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }})
                }
            });
            
            $("#base").click(function() {
                var filtros = "";
                <?php
                $seldados = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
                $eseldados = $_SESSION['query']($seldados) or die (mysql_error());
                while ($lseldados = $_SESSION['fetch_array']($eseldados)) {
                    echo "var f".strtolower($lseldados['nomefiltro_nomes'])."=$('#filtro_".strtolower($lseldados['nomefiltro_nomes'])."').val();\n";
                    $varsbase[] = "f".strtolower($lseldados['nomefiltro_nomes']);
                    $ifjava[] = "f".strtolower($lseldados['nomefiltro_nomes'])."!=''";
                }
                foreach ($varsbase as $var) {
                    echo "filtros = filtros+'#$var='+$var;\n";
                }
                echo "filtros = filtros.substr(1);\n";
                ?>
                var idper = $("#periodorel").val();
                if(idper == "") {
                    alert("Favor escolher o período do relatório");
                    return false;
                }
                else {
                    $.blockUI({ message: '<strong>AGUARDE GERANDO RELATÓRIO...</strong>', css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5,
                    color: '#fff'
                    }})
                    $.post("/monitoria_supervisao/admin/geraselecao.php",{base:'base',idperiodo:idper,filtros:filtros},function(ret) {
                        if(ret == 1) {
                            alert("BASE GERADA COM SUCESSO");
                        }
                        else {
                            if(ret == 2) {
                                alert("A PASTA PARA GERAR A BASE NÃO ESTÁ ACESSÍVEL PARA EDIÇÃO, FAVOR CONTATAR O ADMNISTRADOR");
                            }
                            else {
                                if(ret == 0) {
                                    alert("BASE GERADA COM SUCESSO, PORÉM ESTÁ VAZIA");
                                }
                                if(ret == 3) {
                                    alert("ERRO NO PROCESSO PARA GERAR A BASE");
                                }
                            }
                        }
                        $.unblockUI();
                        location.reload();
                    });
                }
            });
            
            $("input[id*='excluir']").click(function() {
               var caminho = $(this).attr("name");
               $.post("/monitoria_supervisao/admin/geraselecao.php",{caminho:caminho,exclui:"exclui"},function(ret) {
                   if(ret == 1) {
                       alert("Exportacao apagada com sucesso");
                       location.reload();
                   }
                   else {
                       alert("Ocorreu algum erro para apagar a Exportação");
                   }
               });
               return false;
            })
	});
</script>
</head>
<body>
    <div id="conteudo">
        <div id="opcoes" style="margin-top:20px; margin-bottom:20px">
            <?php
            if($_SESSION['user_tabela'] == "user_adm" OR ($_SESSION['user_tabela'] == "user_web" && $_SESSION['selecao'] == "S")) {
                ?>
                <table width="191">
                  <tr>
                    <td width="68" class="corfd_coltexto"><strong>OPÇÕES</strong></td>
                    <td width="107" class="corfd_colcampos">
                        <select name="opcao" id="opcao">
                            <option value="">SELECIONE...</option>
                            <option value="dadosselecao">SELEÇÃO</option>
                            <option value="relatorio">RELATÓRIO</option>
                        </select>
                    </td>
                  </tr>
                </table>
                <?php
            }
            else {
            }
            ?>
        </div>
        <div id="filtros">
            <span id="spanmsg" style="color:#FF0000; font-weight:bold;font-size:13px"><?php echo $_GET['msg'];?></span>
            <form action="" method="post">
              <table style="border: 1px solid #999">
              <tr>
                <td class="corfd_ntab" align="center" colspan="2"><strong>FILTROS</strong><input type="hidden" name="opcaopesq" id="opcaopesq" value="<?php if($_GET['op'] == "dadosselecao" || $_POST['opcaopes'] == "dadosselecao") { echo "dadosselecao"; }if($_GET['op'] == "relatorio" || $_POST['opcaopes'] == "relatorio") { echo "relatorio"; }?>" /></td>
              </tr>
              <tr>
              <td width="176"><input name="iduser" type="hidden" value="<?php echo $_SESSION['uduarioID'];?>" /></td>
                <td colspan="3"></td>
              </tr>
              <?php
              $filtnome = "SELECT * FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
              $efilt = $_SESSION['query']($filtnome) or die (mysql_error());
              $idfiltros = array();
              $nomefiltros = array();
              $url = array();
              $urlid = array();
              while($lfilt = $_SESSION['fetch_array']($efilt)) {
                if($lfilt['idfiltro_nomes'] == $eultimo['idfiltro_nomes']) {
                    $style = "style=\"display:none\"";
                }
                else {
                    $style = "";
                }
                  echo "<tr $style>";
                    echo "<td class=\"corfd_coltexto\"><strong>".$lfilt['nomefiltro_nomes']."</strong></td>";
                        echo "<td class=\"corfd_colcampos\"><select style=\"width:252px\" name=\"filtro_".strtolower($lfilt['nomefiltro_nomes'])."\" id=\"filtro_".strtolower($lfilt['nomefiltro_nomes'])."\">";
                        if($_POST['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == "") {
                            $selected = "selected=\"selected\"";
                            echo "<option $selected value=\"\" >Selecione um filtro</option>";
                        }
                        else {
                            $selected = "";
                            echo "<option $selected value=\"\" >Selecione um filtro</option>";
                        }
                        $seldados = "SELECT fd.idfiltro_dados, fd.nomefiltro_dados FROM $tabuser $atabuser INNER JOIN $tabreluser $atabreluser ON $atabreluser.iduser_$part = $atabuser.id$tabuser
                                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = $atabreluser.idrel_filtros
                                    INNER JOIN filtro_dados fd ON fd.idfiltro_dados = rf.id_".strtolower($lfilt['nomefiltro_nomes'])."
                                    WHERE $atabuser.id$tabuser='".$_SESSION['usuarioID']."' GROUP BY rf.id_".strtolower($lfilt['nomefiltro_nomes'])." ORDER BY nomefiltro_dados";
                        $edados = $_SESSION['query']($seldados) or die (mysql_error());
                        while($ldados = $_SESSION['fetch_array']($edados)) {
                          if($_POST['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == $ldados['idfiltro_dados'] || $getfiltros['filtro_'.strtolower($lfilt['nomefiltro_nomes'])] == $ldados['idfiltro_dados']) {
                              $selected = "selected=\"selected\"";
                              $idfiltros[] = $ldados['idfiltro_dados'];
                              $nomefiltros[] = 'id_'.strtolower($lfilt['nomefiltro_nomes']);
                          }
                          else {
                              $selected = "";
                          }
                          echo "<option $selected value=\"".$ldados['idfiltro_dados']."\">".$ldados['nomefiltro_dados']."</option>";
                        }
                        echo "</select></td>";
                    echo "</tr>";
              }
              if($_SESSION['user_tabela'] == "user_adm" OR ($_SESSION['user_tabela'] == "user_web" && $_SESSION['selecao'] == "S")) {
              ?>
              <tr id="trperiodo">
                    <td class="corfd_coltexto" width="100px"><strong>PERIODO</strong></td>
                    <td class="corfd_colcampos">
                        <select name="periodorel" id="periodorel">
                        <option value="" disable="disable">SELECIONE...</option>
                       <?php
                       $setdata = "SELECT DATE_ADD('".date('Y-m-d')."',INTERVAL -1 YEAR) as result";
                       $esetdata = $_SESSION['fetch_array']($_SESSION['query']($setdata)) or die (mysql_error());
                       $selperiodo = "SELECT * FROM periodo WHERE dataini >= '".$esetdata['result']."' ORDER BY dataini DESC";
                       $eselperiodo = $_SESSION['query']($selperiodo) or die (mysql_error());
                       $nper = $_SESSION['num_rows']($eselperiodo);
                       while($lperiodo = $_SESSION['fetch_array']($eselperiodo)) {
                           if($_POST['periodorel'] == $lperiodo['idperiodo']) {
                               $selectedp = "selected=\"selected\"";
                           }
                           else {
                               $selectedp = "";
                           }
                           ?>
                            <option <?php echo $selectedp;?> value="<?php echo $lperiodo['idperiodo'];?>"><?php echo $lperiodo['nmes']."-".$lperiodo['ano'];?></option>
                           <?php
                       }
                       ?>                            
                    </td>
              </tr>
              <tr id="pesquisa">
                <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; width:90px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="pesquisa" id="pesquisa" type="submit" value="Pesquisar" /></td>
              </tr>
              <tr id="gerar">
                <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; width:90px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="gera" id="gera" type="submit" value="Gerar" /> <?php if($_SESSION['user_tabela'] == "user_adm") { ?><input style="border: 1px solid #FFF; height: 18px; width:120px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="base" id="base" type="button" value="Exportar Base" /><?php };?> </td>
              </tr>
              <?php
              }
              else {
              ?>
              <tr id="pesquisa">
                <td colspan="4"><input style="border: 1px solid #FFF; height: 18px; width:90px;background-image:url(/monitoria_supervisao/images/button.jpg)" name="pesquisa" id="pesquisa" type="submit" value="Pesquisar" /></td>
              </tr>
              <?php
              }
              ?>
              </table>
            </form>
        </div>
        <div id="dadosselecao">
            <div id="divrel">
            <?php
            if(isset($_POST['pesquisa'])) {
                $urlfiltro = "";
                $selfiltro = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
                $efiltro = $_SESSION['query']($selfiltro) or die (mysql_error());
                while($lfiltro = $_SESSION['fetch_array']($efiltro)) {
                    if($_POST['filtro_'.strtolower($lfiltro['nomefiltro_nomes'])] != "") {
                        $idsfiltro[] = 'id_'.strtolower($lfiltro['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lfiltro['nomefiltro_nomes'])]."'";
                        $urlfiltro .= "f_".strtolower($lfiltro['nomefiltro_nomes'])."-".$_POST['filtro_'.strtolower($lfiltro['nomefiltro_nomes'])]."*";
                    }
                }
                if(isset($idsfiltro)) {
                    $whererel = " AND ".implode(" AND ",$idsfiltro);
                }
                else{
                    $whererel = "";
                }					
                $selrels = "SELECT rf.idrel_filtros as idrel, $atabreluser.id$tabreluser as iddados FROM rel_filtros rf 
                            INNER JOIN $tabreluser $atabreluser ON $atabreluser.idrel_filtros = rf.idrel_filtros
                            INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                            WHERE cr.ativo='S' AND $atabreluser.iduser_$part='".$_SESSION['usuarioID']."' $whererel";
                $eselrel = $_SESSION['query']($selrels) or die (mysql_error());
                while($lrels = $_SESSION['fetch_array']($eselrel)) {
                    $idsrel[] = $lrels['idrel'];
                }
                $arvore = arvoreescalar("",$_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
                foreach($arvore as $idrel) {
                    if(in_array($idrel,$idsrel)) {
                        ?>
                        <a id="alink<?php echo $idrel;?>" class="linkidrel" name="<?php echo $idrel;?>" style="text-decoration:none; color:#000">
                            <li id="liselecao<?php echo $idrel;?>" style="width:650px; height:15px; border:1px #000 solid; background:#FFF; margin-top: 5px; list-style: none; font-weight: bold;padding-top:4px; padding-left:4px"><?php echo nomeapres($idrel);?><input type="hidden" name="urlfiltros<?php echo $idrel;?>" id="urlfiltros<?php echo $idrel;?>" value="<?php echo $urlfiltro;?>" /></li>
                        </a>
                        <?php		
                    }
                }
            }
            if(isset($_GET['filtros']) && !isset($_POST['pesquisa'])) {
                $urlfiltro = "";
                $selfiltro = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S'";
                $efiltro = $_SESSION['query']($selfiltro) or die (mysql_error());
                while($lfiltro = $_SESSION['fetch_array']($efiltro)) {
                    if($getfiltros['filtro_'.strtolower($lfiltro['nomefiltro_nomes'])] != "") {
                        $idsfiltro[] = 'id_'.strtolower($lfiltro['nomefiltro_nomes'])."='".$getfiltros['filtro_'.strtolower($lfiltro['nomefiltro_nomes'])]."'";
                        $urlfiltro .= "f_".strtolower($lfiltro['nomefiltro_nomes'])."-".$getfiltros['filtro_'.strtolower($lfiltro['nomefiltro_nomes'])]."*";
                    }
                }
                if(isset($idsfiltro)) {
                    $whererel = " AND ".implode(" AND ",$idsfiltro);
                }
                else{
                    $whererel = "";
                }					
                $selrels = "SELECT rf.idrel_filtros as idrel, $atabreluser.id$tabreluser as iddados FROM rel_filtros rf 
                            INNER JOIN $tabreluser $atabreluser ON $atabreluser.idrel_filtros = rf.idrel_filtros 
                            INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                            WHERE cr.ativo='S' AND $atabreluser.iduser_$part='".$_SESSION['usuarioID']."' $whererel";
                $eselrel = $_SESSION['query']($selrels) or die (mysql_error());
                while($lrels = $_SESSION['fetch_array']($eselrel)) {
                    $idsrel[] = $lrels['idrel'];
                }
                $arvore = arvoreescalar("",$_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
                foreach($arvore as $idrel) {
                    if(in_array($idrel,$idsrel)) {
                        ?>
                        <a id="alink<?php echo $idrel;?>" class="linkidrel" name="<?php echo $idrel;?>" style="text-decoration:none; color:#000">
                            <li id="liselecao<?php echo $idrel;?>" style="width:650px; height:15px; border:1px #000 solid; background:#FFF; margin-top: 5px; list-style: none; font-weight: bold;padding-top:4px; padding-left:4px"><?php echo nomeapres($idrel);?><input type="hidden" name="urlfiltros<?php echo $idrel;?>" id="urlfiltros<?php echo $idrel;?>" value="<?php echo $urlfiltro;?>" /></li>
                        </a>
                        <?php		
                    }
                }
            }
            ?>
            </div>
            <div id="divenvio" style=" background-color: #ccc; border: 2px solid #999; margin-top: 10px; padding:10px">
                <table border="0" bgcolor="#FFFFFF">
                    <tbody>
                        <tr>
                            <form action="admin/geraselecao.php" method="post" enctype="multipart/form-data">
                            <td width="246"><input type="file" name="arquivo" id="arquivo" /><input type="hidden" name="idrelselecao" id="idrelselecao" value="" /><input type="hidden" name="envfiltros" id="envfiltros" value="" /></td>
                            <td width="68" align="right"><input type="submit" name="enviar" id="enviar" value="ENVIAR" /></td>
                            </form>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
        if($_SESSION['user_tabela'] == "user_web" && !isset($_SESSION['selecao'])) {
        }
        else {
            ?>
            <div id="relatorio" style="margin-top:10px;">
            <?php
            if(isset($_POST['gera'])) {
                $idperiodo = $_POST['periodorel'];
                $selper = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
                $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die (mysql_error());
                $filtrosrel = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
                $efrel = $_SESSION['query']($filtrosrel) or die (mysql_error());
                $nerel = $_SESSION['num_rows']($efrel);
                if($nerel >= 1) {
                    while($lerel = $_SESSION['fetch_array']($efrel)) {
                        $nfiltros[] = $lerel['nomefiltro_nomes'];
                        if($_POST['filtro_'.strtolower($lerel['nomefiltro_nomes'])] != "") {
                            $idsfiltro["id_".strtolower($lerel['nomefiltro_nomes'])] = "id_".strtolower($lerel['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lerel['nomefiltro_nomes'])]."'";
                        }
                    }
                }
                if(isset($idsfiltro)) {
                    $whererel = "WHERE ".implode(" AND ",$idsfiltro);
                }
                else {
                    $whererel = "";
                }
                $selrel = "SELECT rf.idrel_filtros FROM rel_filtros rf
                            INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                            $whererel";
                $eselrel = $_SESSION['query']($selrel) or die (mysql_error());
                while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                    $idsreldesor[] = $lselrel['idrel_filtros'];
                }
                $idsrel = arvoreescalar("",$_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
                foreach($idsrel as $idrel) {
                    if(in_array($idrel,$idsreldesor)) {
                        $idsrelordem[] = $idrel;
                    }
                }
                ?>
                <table width="1024">
                    <tr>
                        <td><a style="text-decoration:none;text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=RELATORIO_MAPA"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">EXCEL</div></a></td>
                        <td><a style="text-decoration:none; text-align: center; color: #000;" href="/monitoria_supervisao/users/export.php?tipo=WORD&nome=RELATORIO_MAPA"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">WORD</div></a></td>
                        <td><a style="text-decoration:none;text-align: center; color: #000;" target="blank" href="/monitoria_supervisao/users/export.php?tipo=PDF&nome=RELATORIO_MAPA"><div style="width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto">PDF</div></a></td>
                    </tr>
                </table><br/>
                <div style="overflow:auto;width:1024px;height:400px; font-size:13px">
                    <table id="relatoriomapa">
                        <?php
                        $dadosexp .= "<table id=\"relatoriomapa\">";
                        foreach($nfiltros as $cnome => $nome) {
                            $dadosexp .= "<tr class=\"corfd_coltexto\" style=\"font-weight:bold;\">";
                            $dadosexp .= "<td></td>";
                            ?>
                            <tr class="corfd_coltexto" style="font-weight:bold;">
                            <td></td>
                            <?php
                            foreach($idsrelordem as $id) {
                                $selnomerel = "SELECT nomefiltro_dados FROM filtro_dados fd
                                               INNER JOIN rel_filtros rf ON rf.id_".strtolower($nome)." = fd.idfiltro_dados
                                               WHERE idrel_filtros='$id'";
                                $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnomerel)) or die (mysql_error());
                                $dadosexp .= "<td align=\"center\" width=\"100px\" colspan=\"4\">".$eselnome['nomefiltro_dados']."</td>";
                                ?>
                                <td align="center" width="100px" colspan="4"><?php echo $eselnome['nomefiltro_dados'];?></td>
                                <?php
                            }
                            if(count($nfiltros) - 1 == $cnome) {
                            $dadosexp .= "</tr>";
                            $dadosexp .= "<tr><td class=\"corfd_colcampos\" style=\"font-weight:bold\" ><div style=\"width:100px\">META-MÊS/META-DIA</div></td>";
                            ?>
                           </tr>
                           <tr>
                               <td class="corfd_colcampos" style="font-weight:bold" ><div style="width:100px">META-MÊS/DIA</div></td>
                               <?php
                               for($m = 0;$m < count($idsrelordem);$m++) {
                                   $selmeta = "SELECT *,count(*) as result FROM dimen_mod WHERE idrel_filtros='$idsrelordem[$m]' AND idperiodo='$idperiodo'";
                                   $eselmeta = $_SESSION['fetch_array']($_SESSION['query']($selmeta)) or die (mysql_error());
                                   if($eselmeta['result'] == 0) {
                                        $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" colspan=\"4\">--</td>";
                                        ?>
                                        <td class="corfd_colcampos" align="center" colspan="4">--</td>
                                        <?php
                                   }
                                   else {
                                        $metadia[$idsrelordem[$m]] = round($eselmeta['estimativa'] / $eselper['diasprod']);
                                        $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" colspan=\"2\">".$eselmeta['estimativa']."</td>";
                                        $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" colspan=\"2\">".round($eselmeta['estimativa'] / $eselper['diasprod'])."</td>";
                                        ?>
                                        <td class="corfd_colcampos" align="center" colspan="2"><?php echo $eselmeta['estimativa'];?></td>
                                        <td class="corfd_colcampos" align="center" colspan="2"><?php echo round($eselmeta['estimativa'] / $eselper['diasprod']);?></td>
                                        <?php
                                   }
                               }
                               $dadosexp .= "</tr>";
                               ?>
                           </tr>
                           <tr class="corfd_ntab">
                               <td></td>
                               <?php
                               $dadosexp .= "<tr class=\"corfd_ntab\">";
                               $dadosexp .= "<td></td>";
                               for($o = 0;$o < count($idsrelordem);$o++) {
                                $dadosexp .= "<td align=\"center\">VENDAS</td><td align=\"center\">SOL.</td><td align=\"center\">SEL.</td><td align=\"center\">IMP.</td>";
                                ?>
                                <td align="center">VENDAS</td>
                                <td align="center">SOL.</td>
                                <td align="center">SEL.</td>
                                <td align="center">IMP.</td>
                                <?php
                               }
                               $dadosexp .= "</tr>";
                               ?>
                           </tr>
                            <?php
                            }
                            else {
                            $dadosexp .= "</tr>";
                            ?>
                           </tr>
                           <?php
                            }
                        }
                        if($eselper['dtinictt'] == "" || $eselper['dtfimctt'] == "") {
                            $dtini = substr($eselper['dataini'],0,4)."-".substr($eselper['dataini'],5,2)."-01";
                            $dtfim = ultdiames("01".substr($eselper['dataini'],5,2).substr($eselper['dataini'],0,4));
                        }
                        else {
                            $dtini = $eselper['dtinictt'];
                            $dtfim = $eselper['dtfimctt'];
                        }
                        $feriados = getFeriados(date('Y'));
                        $i = 0;
                        while($i < 1) {
                            $datas = "datactt = '$dtini'";
                            $dadosexp .= "<tr>";
                            $dadosexp .= "<td class=\"corfd_colcampos\">".banco2data($dtini)."</td>";
                            ?>
                            <tr>
                            <td class="corfd_colcampos"><?php echo banco2data($dtini);?></td>
                            <?php
                            foreach($idsrelordem as $iddados) {
                                $qtdeenv = 0;
                                $qtdesel = 0;
                                $qtdeefe = 0;
                                $selselecao = "SELECT count(*) as result FROM selecao
                                               WHERE idrel_filtros='$iddados' AND $datas AND selecionado=1 and reposicao='0'";
                                $eselselecao = $_SESSION['fetch_array']($_SESSION['query']($selselecao)) or die (mysql_error());
                                $qtdeefe = $eselselecao['result'];
                                $selenv = "SELECT qtdeenv, count(*) as r FROM selecao s
                                           INNER JOIN upselecao up ON up.idupselecao = s.idupselecao
                                           WHERE idrel_filtros='$iddados' AND $datas GROUP BY up.idupselecao";
                                $eselenv = $_SESSION['query']($selenv) or die (mysql_error());
                                $nselenv = $_SESSION['num_rows']($eselenv);
                                if($nselenv >= 1) {
                                     while($lselenv = $_SESSION['fetch_array']($eselenv)) {
                                          $qtdeenv = $qtdeenv + $lselenv['qtdeenv'];
                                     }
                                }
                                $dtup = "data='".date('Y-m-d',mktime(0,0,0,substr($dtini,5,2),substr($dtini,8,2)+1,substr($dtini,0,4)))."'";
                                $selup = "SELECT qtdesel FROM upselecao up
                                          INNER JOIN selecao s ON s.idupselecao = up.idupselecao
                                          WHERE $dtup AND s.idrel_filtros='$iddados' GROUP BY up.idupselecao";
                                $eselup = $_SESSION['query']($selup) or die (mysql_error());
                                while($lselup = $_SESSION['fetch_array']($eselup)) {
                                    $qtdesel = $qtdesel + $lselup['qtdesel'];
                                }
                                if($qtdesel - $qtdeefe > ($metadia[$iddados] + $metadia[$iddados]*0.5)) {
                                    //$corcel = "style=\"background-color:#FF8A8A\"";
                                }
                                else {
                                    //$corcel = "style=\"background-color:#FFFFFF\"";
                                }
                                $selfila = "SELECT COUNT(*) as result FROM fila_grava WHERE idrel_filtros='$iddados' AND $datas";
                                $eselfila = $_SESSION['fetch_array']($_SESSION['query']($selfila)) or die (mysql_error());
                                $totais[$iddados]['qtdeenv'] =  $totais[$iddados]['qtdeenv'] + $qtdeenv;
                                $totais[$iddados]['qtdesel'] =  $totais[$iddados]['qtdesel'] + $qtdesel;
                                $totais[$iddados]['qtdeefetiva'] =  $totais[$iddados]['qtdeefetiva'] + $qtdeefe;
                                $totais[$iddados]['importado'] =  $totais[$iddados]['importado'] + $eselfila['result'];
                                $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" $corcel>$qtdeenv</td>";
                                $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" $corcel>$qtdesel</td>";
                                $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" $corcel>$qtdeefe</td>";
                                $dadosexp .= "<td class=\"corfd_colcampos\" align=\"center\" $corcel>".$eselfila['result']."</td>";
                                ?>
                                <td class="corfd_colcampos" align="center" <?php echo $corcel;?>><?php echo $qtdeenv;?></td>
                                <td class="corfd_colcampos" align="center" <?php echo $corcel;?>><?php echo $qtdesel;?></td>
                                <td class="corfd_colcampos" align="center" <?php echo $corcel;?>><?php echo $qtdeefe;?></td>
                                <td class="corfd_colcampos" align="center" <?php echo $corcel;?>><?php echo $eselfila['result'];?></td>
                                <?php
                            }
                            $dadosexp .= "</tr>";
                            ?>
                            </tr>
                            <?php
                            $acresdia = "SELECT ADDDATE('$dtini',+1) as acres";
                            $eacres = $_SESSION['fetch_array']($_SESSION['query']($acresdia)) or die (mysql_error());
                            $dtini = $eacres['acres'];
                            $checkfim = "SELECT '$dtini' > '".$dtfim."' as checkdt";
                            $echeckfim = $_SESSION['fetch_array']($_SESSION['query']($checkfim)) or die (mysql_error());
                            if($echeckfim['checkdt'] == 1) {
                                $i = 1;
                            }
                        }
                        $dadosexp .= "<tr class=\"corfd_coltexto\" align=\"center\" style=\"font-weight:bold\"><td>Totais</td>";
                        ?>
                        <tr class="corfd_coltexto" align="center" style="font-weight:bold">
                            <td>Totais</td>
                        <?php
                        for($t = 0;$t < count($idsrelordem);$t++) {
                            $dadosexp .= "<td>".$totais[$idsrelordem[$t]]['qtdeenv']."</td>";
                            $dadosexp .= "<td>".$totais[$idsrelordem[$t]]['qtdesel']."</td>";
                            $dadosexp .= "<td>".$totais[$idsrelordem[$t]]['qtdeefetiva']."</td>";
                            $dadosexp .= "<td>".$totais[$idsrelordem[$t]]['importado']."</td>";
                            ?>
                            <td><?php echo $totais[$idsrelordem[$t]]['qtdeenv'];?></td>
                            <td><?php echo $totais[$idsrelordem[$t]]['qtdesel'];?></td>
                            <td><?php echo $totais[$idsrelordem[$t]]['qtdeefetiva'];?></td>
                            <td><?php echo $totais[$idsrelordem[$t]]['importado'];?></td>
                            <?php
                        }
                        $dadosexp .= "</tr></table>";
                        ?>
                        </tr>
                    </table>
                </div>
                <?php
                $_SESSION['dadosexp'] = $dadosexp;
            }
            ?>
            </div>
            <?php
        }
        if(isset($_POST['base'])) {
            $idperiodo = $_POST['periodorel'];
            $selper = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
            $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die (mysql_error());
            $filtrosrel = "SELECT nomefiltro_nomes FROM filtro_nomes WHERE ativo='S' ORDER BY nivel DESC";
            $efrel = $_SESSION['query']($filtrosrel) or die (mysql_error());
            $nerel = $_SESSION['num_rows']($efrel);
            if($nerel >= 1) {
                while($lerel = $_SESSION['fetch_array']($efrel)) {
                    $nfiltros[] = strtolower($lerel['nomefiltro_nomes']);
                    $colnomes[] = $tabsql->AliasTab("filtro_dados").strtolower($lerel['nomefiltro_nomes']).".nomefiltro_dados as ".strtolower($lerel['nomefiltro_nomes']);
                    $inners[] = "INNER JOIN filtro_dados ".$tabsql->AliasTab("filtro_dados").strtolower($lerel['nomefiltro_nomes'])." ON ".$tabsql->AliasTab("filtro_dados").strtolower($lerel['nomefiltro_nomes']).".idfiltro_dados=".$tabsql->AliasTab("rel_filtros").".id_".strtolower($lerel['nomefiltro_nomes']);
                    if($_POST['filtro_'.strtolower($lerel['nomefiltro_nomes'])] != "") {
                        $idsfiltro["id_".strtolower($lerel['nomefiltro_nomes'])] = "id_".strtolower($lerel['nomefiltro_nomes'])."='".$_POST['filtro_'.strtolower($lerel['nomefiltro_nomes'])]."'";
                    }
                }
            }
            if(isset($idsfiltro)) {
                $whererel = "WHERE ".implode(" AND ",$idsfiltro);
            }
            else {
                $whererel = "";
            }
            $selrel = "SELECT rf.idrel_filtros FROM rel_filtros rf
                        INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                        $whererel";
            $eselrel = $_SESSION['query']($selrel) or die (mysql_error());
            while($lselrel = $_SESSION['fetch_array']($eselrel)) {
                $idsreldesor[] = $lselrel['idrel_filtros'];
            }
            $idsrel = arvoreescalar("",$_SESSION['usuarioID']."-".$_SESSION['user_tabela']);
            foreach($idsrel as $idrel) {
                if(in_array($idrel,$idsreldesor)) {
                    $idsrelordem[] = $idrel;
                }
            }
            if($eselper['dtinictt'] == "" || $eselper['dtfimctt'] == "") {
                $dtini = substr($eselper['dataini'],0,4)."-".substr($eselper['dataini'],5,2)."-01";
                $dtfim = ultdiames("01".substr($eselper['dataini'],5,2).substr($eselper['dataini'],0,4));
            }
            else {
                $dtini = $eselper['dtinictt'];
                $dtfim = $eselper['dtfimctt'];
            }
            $colunas = implode(";",$nfiltros).";data;dataimp;nomearquivo;operador;datactt;horactt;selecionado;enviado";
            $l = $colunas."\n";
            $nomearq = $eselper['nmes']."-".$eselper['ano']."-".date("dmY")."-".date("His")."-".$_SESSION['usuarioLogin'].".csv";
            mkdir($rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']);
            $arquivo = fopen($rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']."/$nomearq","w");
            fwrite($arquivo,$l);
            
            $idsteste = implode(",",$idsrelordem);
            foreach($idsrelordem as $idrel) {
                $operacao = "";
                $sqlselecao = "SELECT operador,data,datactt,horactt,".$tabsql->AliasTab("selecao").".nomearquivo,selecionado,enviado FROM selecao ".$tabsql->AliasTab("selecao")."
                             INNER JOIN upselecao ".$tabsql->AliasTab("upselecao")." ON ".$tabsql->AliasTab("upselecao").".idupselecao=".$tabsql->AliasTab("selecao").".idupselecao
                             WHERE idrel_filtros='$idrel' AND datactt BETWEEN '$dtini' AND '$dtfim'";
                $esqlselecao = $_SESSION['query']($sqlselecao) or die (mysql_error());
                $nselecao = $_SESSION['num_rows']($esqlselecao);
                if($nselecao >= 1) {
                    $noper = "SELECT ".implode(",",$colnomes).",tpaudio FROM rel_filtros ".$tabsql->AliasTab("rel_filtros")." ".
                              implode(" ",$inners)."
                              INNER JOIN conf_rel cf ON cf.idrel_filtros = ".$tabsql->AliasTab("rel_filtros").".idrel_filtros
                              INNER JOIN param_moni pm ON pm.idparam_moni = cf.idparam_moni
                              WHERE ".$tabsql->AliasTab("rel_filtros").".idrel_filtros='$idrel'";
                    $enoper = $_SESSION['fetch_array']($_SESSION['query']($noper)) or die (mysql_error());
                    $tpaudio = explode(",",$enoper['tpaudio']);
                    foreach($nfiltros as $kf => $f) {
                        if($kf == 0) {
                            $operacao .= $enoper[$f];   
                        }
                        else {
                            $operacao .= ";$enoper[$f]";
                        }
                    }
                    while($lselecao = $_SESSION['fetch_array']($esqlselecao)) {
                        $linha = $operacao;
                        foreach(explode(";",$colunas) as $col) {
                            if(!in_array($col,$nfiltros)) {
                                if($col == "dataimp") {
                                    $selimp = "SELECT dataimp,count(*) as result FROM fila_grava fg
                                               INNER JOIN upload u ON u.idupload = fg.idupload
                                                WHERE narquivo='".$lselecao['nomearquivo']."'";
                                    $eselimp = $_SESSION['fetch_array']($_SESSION['query']($selimp)) or die (mysql_error());
                                    if($eselimp['result'] >= 1) {
                                        $linha .= ";".banco2data($eselimp['dataimp']);
                                    }
                                    else {
                                        $linha .= ";";
                                    }
                                }
                                else {
                                    if(strstr($col,"data")) {
                                        $linha .= ";".banco2data($lselecao[$col]);
                                    }
                                    else {
                                        $linha .= ";".$lselecao[$col];
                                    }
                                }
                            }
                        }
                        $linha .= "\n";
                        fwrite($arquivo,$linha);
                        $linha = "";
                    }
                }
            }
            fclose($arquivo);
            
            //$_SESSION['dadosexp'] = $dados;
        }
        ?>
        <div id="divperiodo" name="divperiodo" style="margin-top:20px; border-top:2px solid #999"><br/>
            <table border="0">
                <tbody>
                    <tr>
                        <td class="corfd_coltexto" width="100px"><strong>PERIODO</strong></td>
                        <td class="corfd_colcampos">
                            <select name="periodo" id="periodo">
                            <option value="" disable="disable">SELECIONE...</option>
                           <?php
                           $setdata = "SELECT DATE_ADD('".date('Y-m-d')."',INTERVAL -1 YEAR) as result";
                           $esetdata = $_SESSION['fetch_array']($_SESSION['query']($setdata)) or die (mysql_error());
                           $selperiodo = "SELECT * FROM periodo WHERE dataini >= '".$esetdata['result']."' ORDER BY dataini DESC";
                           $eselperiodo = $_SESSION['query']($selperiodo) or die (mysql_error());
                           $nper = $_SESSION['num_rows']($eselperiodo);
                           while($lperiodo = $_SESSION['fetch_array']($eselperiodo)) {
                               ?>
                                <option value="<?php echo $lperiodo['idperiodo'];?>"><?php echo $lperiodo['nmes']."-".$lperiodo['ano'];?></option>
                               <?php
                           }
                           ?>                            
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div id="dadosorigem" name="dadosorigem" style="margin-top:20px">         
        </div>
        <?php
        if($_SESSION['user_tabela'] == "user_adm") {
        ?>
        <div style="width:810px" id="bases">
            <fieldset style="border:2px solid #999">
                <legend style="margin-left:10px;padding:5px; border:2px solid #333; background-color:#FFF;font-weight:bold ">
                    BASES SELEÇÃO
                </legend>
                <div style="width:800px;height: 150px;overflow:auto">
                <ul style="margin-left: 20px; list-style:none">
                    <?php
                    $export = scandir($rais."/monitoria_supervisao/exportacaoselecao/".$_SESSION['nomecli']);
                    $c = 0;
                    foreach($export as $ex) {
                        if($ex == "." || $ex == "..") {
                        }
                        else {
                        ?>
                        <li style="padding:5px"><a target="_blank" style="text-decoration:none; font-size:12px" href="/monitoria_supervisao/exportacaoselecao/<?php echo $_SESSION['nomecli']."/$ex";?>"><?php echo $ex;?></a> <input id="excluir<?php echo $c++;?>" style="margin-left:10px;border:0px;width:16px; height:16px;background-image:url(/monitoria_supervisao/images/exit.png)" name="<?php echo $ex;?>" /></li>
                        <?php
                        }
                    }
                    ?>
                </ul>
                </div>
            </fieldset><br/>
        </div>
        <?php
        }
        ?>
    </div>
</body>
