<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');

if(isset($_POST['teste'])) {
  $idplan = $_POST['idplan'];
  $horaini = $_POST['horaini'];
  $horafim = date('H:i:s');
  $data = date('Y-m-d');
  $tmpaudio = "00:03:00";
  $idaval = $_POST['idaval'];
  $vaval = $_POST['valor_aval'];
  $idgrup = $_POST['idgrup'];
  $vgrup = $_POST['valor_grup'];
  $idsub = $_POST['idsub'];
  $vsub = $_POST['valor_sub'];
  $idperg = $_POST['idperg'];
  $vperg = $_POST['valor_perg'];
  $idresp = $_POST['idresp'];
  $idfiltro = "000001";
  $monitor = 'Otto Dutra';
  $gravacao = "/monitoria_supervisao/audio-player/teste.mp3";
  $nperg = count($idperg);
  $nresp = count($idresp);
  if($nresp < $nperg) {
    echo ("<script>alert(\"Todas as perguntas devem estar preenchidas para que a monitoria seja salva!!!\");
    window.location = 'visualiza.php?menu=&id=".$idplan."';
    </script>");
    break;
  }
  $vpergfinal = array_combine($pergvalor, $valorresp);
  $selmoni = "SELECT * FROM monitoriaadm";
  $emoni = $_SESSION['query']($selmoni) or die (mysql_error());
  $qmoni = $_SESSION['num_campos']($emoni) or die (mysql_error());
  $selcolum = "SHOW COLUMNS FROM monitoriaadm";
  $ecolum = $_SESSION['query']($selcolum) or die (mysql_error());
  $sql = array();
  $sqlid = array();
  while($lcolum = $_SESSION['fetch_array']($ecolum)) {
      if($lcolum['Field'] != 'idmoni') {
      //$selid = "SELECT * FROM rel_filtros WHERE idrelfiltro";
      //$eid = $_SESSION['fetch_array']($_SESSION['query']($selid)) or die (mysql_error());
      }
      if($lcolum['Field'] == 'idmonitor') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$monitor'");
      }
      if($lcolum['Field'] == 'horaini') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$horaini'");
      }
      if($lcolum['Field'] == 'horafim') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$horafim'");
      }
      if($lcolum['Field'] == 'tmpaudio') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$tmpaudio'");
      }
      if($lcolum['Field'] == 'idplanilha') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, $idplan);
      }
      if($lcolum['Field'] == 'gravacao') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$gravacao'");
      }
      if($lcolum['Field'] == "idrel_filtros") {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$idfiltro'");
      }
      if($lcolum['Field'] == 'data') {
	array_push($sql, $lcolum['Field']);
	array_push($sqlid, "'$data'");
      }
  }
  $pergsub = array();
  $perggrup = array();
  foreach($idresp as $idrel) {
    $selperg = "SELECT DISTINCT(idpergunta) as id_perg FROM pergunta WHERE idresposta='$idrel'";
    $epergrel = $_SESSION['fetch_array']($_SESSION['query']($selperg)) or die (mysql_error());
    $selsub = "SELECT COUNT(*) as result FROM subgrupo WHERE idpergunta='".$epergrel['id_perg']."'";
    $esubrel = $_SESSION['fetch_array']($_SESSION['query']($selsub)) or die (mysql_error());
    if($esubrel['result'] >= 1) {
      array_push($pergsub, $idrel);
    }
    else {
      array_push($perggrup, $idrel);
    }
  }
  $idperggrup = implode(",", $perggrup);
  $idpergsub = implode(",", $pergsub);
  $sqlvar = implode(",",$sql);
  $sqlval = implode(",",$sqlid);
  echo $sqlvar;
  echo $sqlval;
  $insertmoni = "INSERT INTO monitoriaadm ($sqlvar) VALUE ($sqlval)";
  $einsertmoni = $_SESSION['query']($insertmoni) or die (mysql_error());
  $idmoni = mysql_insert_id();
  $horatemp = "DROP TABLE horatemp";
  $ehoratemp = $_SESSION['query']($horatemp);
  // levanta campos da tabela moniavalia
  $camposmoni = array();
  $campmoni = "SHOW COLUMNS FROM moniavaliaadm";
  $ecampos = $_SESSION['query']($campmoni) or die (mysql_error());
  while($lcampos = $_SESSION['fetch_array']($ecampos)) {
    array_push($camposmoni, $lcampos['Field']);
  }
  $listcampmoni = implode(",",$camposmoni);
  // criar tabela com todos os itens da monitoria, relacionando com planilha, avaliacao, grupo e pergunta

  $tabsub = "SELECT DISTINCT(pergunta.idresposta) as id_resp, planilha.idplanilha as id_plan, planilha.idaval_plan, rel_aval.valor as valor_aval,
	      grupo.idgrupo as id_grup, grupo.valor_grupo,planilha.tab as tab_grup,
	      pergunta.idpergunta as id_perg, grupo.tab as tab_perg, pergunta.valor_perg,
	      pergunta.valor_resp, pergunta.redist, pergunta.tipo_resp, pergunta.avalia
	      FROM planilha
	      INNER JOIN rel_aval ON rel_aval.idaval_plan = planilha.idaval_plan
	      INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	      INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel
	      WHERE pergunta.idresposta
	      IN ( $idperggrup )";
  $etabsub = $_SESSION['query']($tabsub) or die (mysql_error());
  while($ltabsub = $_SESSION['fetch_array']($etabsub)) {
    $avaliasub = "INSERT INTO moniavaliaadm ($listcampmoni) VALUE ($idmoni, '".$ltabsub['id_plan']."', '".$ltabsub['idaval_plan']."', '".$ltabsub['valor_aval']."', '', '".$ltabsub['idgrupo']."', '".$ltabsub['valor_grupo']."', '', '".$ltabsub['tab_grup']."', NULL, NULL, NULL, NULL, '".$ltabsub['id_perg']."', '".$ltabsub['tipo_resp']."', '".$ltabsub['valor_perg']."', '', '".$ltabsub['tab_perg']."', '".$ltabsub['id_resp']."', '".$ltabsub['redist']."', '".$ltabsub['avalia']."', '".$ltabsub['valor_resp']."')";
    $eavaliasub = $_SESSION['query']($avaliasub) or die (mysql_error());
  }
  if($idpergsub == "") {
  }
  else {
  $tabgrup = "SELECT DISTINCT(pergunta.idresposta) as id_resp, planilha.idplanilha as id_plan, planilha.idaval_plan, rel_aval.valor as valor_aval,
	      grupo.idgrupo as id_grup, grupo.valor_grupo, planilha.tab as tab_grup,
	      subgrupo.idsubgrupo as id_sub, subgrupo.valor_sub, grupo.tab as tab_sub,
	      pergunta.idpergunta as id_perg, pergunta.valor_perg, subgrupo.tab as tab_perg,
	      pergunta.valor_resp, pergunta.redist, pergunta.tipo_resp, pergunta.avalia
	      FROM planilha
	      INNER JOIN rel_aval ON rel_aval.idaval_plan = planilha.idaval_plan
	      INNER JOIN grupo ON grupo.idgrupo = planilha.idgrupo
	      INNER JOIN subgrupo ON subgrupo.idsubgrupo = grupo.idrel
	      INNER JOIN pergunta ON pergunta.idpergunta = grupo.idrel OR pergunta.idpergunta = subgrupo.idpergunta
	      WHERE pergunta.idresposta
	      IN ( $idpergsub )";
  $etabgrup = $_SESSION['query']($tabgrup) or die (mysql_error());
  while($ltabgrup = $_SESSION['fetch_array']($etabgrup)) {
    $avaliagrup = "INSERT INTO moniavaliaadm ($listcampmoni) VALUE ($idmoni, '".$ltabgrup['id_plan']."', '".$ltabgrup['idaval_plan']."', '".$ltabgrup['valor_aval']."', '', '".$ltabgrup['id_grup']."',
    '".$ltabgrup['valor_grupo']."', '', '".$ltabgrup['tab_grup']."', '".$ltabgrup['id_sub']."', '".$ltabgrup['valor_sub']."', '', '".$ltabgrup['tab_sub']."', '".$ltabgrup['id_perg']."',
    '".$ltabgrup['tipo_resp']."', '".$ltabgrup['valor_perg']."', '', '".$ltabgrup['tab_perg']."', '".$ltabgrup['id_resp']."', '".$ltabgrup['redist']."', '".$ltabgrup['avalia']."',
    '".$ltabgrup['valor_resp']."')";
    $egrupavalia = $_SESSION['query']($avaliagrup) or die (mysql_error("erro no processamento da query"));
  }
  }
  $vfperg = "SELECT DISTINCT(idpergunta), SUM(valor_resp) as valor_resp, idmonitoria FROM moniavaliaadm WHERE idmonitoria='$idmoni' GROUP BY idpergunta";
  $evfperg = $_SESSION['query']($vfperg) or die (mysql_error());
  while($lvfperg = $_SESSION['fetch_array']($evfperg)) {
    $cadvfperg = "UPDATE moniavaliaadm SET valor_final_perg='".$lvfperg['valor_resp']."' WHERE idmonitoria='".$idmoni."' AND idpergunta='".$lvfperg['id_perg']."'";
    $ecadvfperg = $_SESSION['query']($cadvfperg) or die (mysql_error());
  }
  $vfsub = "SELECT id_sub, SUM(valor_final_perg) as soma FROM tempvalsub WHERE id_sub IS NOT NULL GROUP BY id_sub ";
  $evfsub = $_SESSION['query']($vfsub) or die (mysql_error());
      while($lvfsub = $_SESSION['fetch_array']($evfsub)) {
	$cadvfsub = "UPDATE moniavaliaadm SET valor_final_sub='".$lvfsub['soma']."' WHERE idmonitoria='$idmoni' AND idsubgrupo='".$lvfsub['id_sub']."'";
	$ecadvfsub = $_SESSION['query']($cadvfsub) or die (mysql_error());
      }
  $droptabsub = "DROP TABLE tempvalsub";
  $edropsub = $_SESSION['query']($droptabsub) or die (mysql_error());
  $tempgrup = "CREATE TABLE tempvalgrup ENGINE=MEMORY SELECT DISTINCT id_perg, id_grup, valor_final_perg as valor_final_perg, idmoni FROM moniavaliaadm WHERE idmoni='$idmoni' AND id_sub IS NULL";
  $etempgrup = $_SESSION['query']($tempgrup) or die (mysql_error());
  $vfgrupperg = "SELECT id_perg, id_grup, SUM(valor_final_perg) as soma, idmoni FROM tempvalgrup GROUP BY id_grup";
  $evfgrupperg = $_SESSION['query']($vfgrupperg) or die (mysql_error());
  while($lvfgrupperg = $_SESSION['fetch_array']($evfgrupperg)) {
    $cadvfgrupperg = "UPDATE moniavaliaadm SET valor_final_grup='".$lvfgrupperg['soma']."' WHERE idmoni='$idmoni' AND id_grup='".$lvfgrupperg['id_grup']."'";
    $ecadvfgrupperg = $_SESSION['query']($cadvfgrupperg) or die (mysql_error());
  }
  $droptabgrup = "DROP TABLE tempvalgrup";
  $edropgrup = $_SESSION['query']($droptabgrup) or die (mysql_error());
  $tempgrup = "CREATE TABLE tempvalgrup ENGINE=MEMORY SELECT DISTINCT id_perg, id_sub, id_grup, valor_final_perg as valor_final_perg, idmoni FROM moniavaliaadm WHERE idmoni='$idmoni' AND id_sub IS NOT NULL";
  $etempgrup = $_SESSION['query']($tempgrup) or die (mysql_error());
  $vfgrup = "SELECT id_perg, id_sub, id_grup, SUM(valor_final_perg) as soma, idmoni FROM tempvalgrup WHERE idmoni='$idmoni' GROUP BY id_grup";
  $evfgrup = $_SESSION['query']($vfgrup) or die (mysql_error());
  while($lvfgrup = $_SESSION['fetch_array']($evfgrup)) {
    $cadvfgrup = "UPDATE moniavaliaadm SET valor_final_grup='".$lvfgrup['soma']."' WHERE idmoni='$idmoni' AND id_grup='".$lvfgrup['id_grup']."'";
    $ecadvfgrup = $_SESSION['query']($cadvfgrup) or die (mysql_error());
  }
  $droptabgrup = "DROP TABLE tempvalgrup";
  $edropgrup = $_SESSION['query']($droptabgrup) or die (mysql_error());
  $vfaval = "SELECT id_resp, SUM(valor_resp) as soma, id_aval FROM moniavaliaadm WHERE idmoni='$idmoni' GROUP BY id_aval";
  $evfaval = $_SESSION['query']($vfaval) or die (mysql_error());
  while($lvfaval = $_SESSION['fetch_array']($evfaval)) {
    $cadaval = "UPDATE moniavaliaadm SET valor_final_aval='".$lvfaval['soma']."' WHERE idmoni='$idmoni' AND id_aval='".$lvfaval['id_aval']."'";
    $ecadaval = $_SESSION['query']($cadaval) or die (mysql_error());
  }
  $evfaval = $_SESSION['query']($vfaval) or die (mysql_error());
  if($ecadaval) {
    echo ("<script>alert(\"Monitoria realizada com sucesso, ID: ''$idmoni''!!!\");
    window.location = 'visualiza.php?menu=&id=".$idplan."';
    </script>");
  }
  else {
    echo ("<script>alert(\"Erro no processo de conclusÃ£o da monitoria!!!\");
    window.location = 'visualiza.php?menu=&id=".$idplan."';
    </script>");
  }
}
if(isset($_POST['voltaplan'])) {
    $idplan = $_POST['idplan'];
    $delhoratemp = "DROP TABLE horatemp";
    $ehoratemp = $_SESSION['query']($delhoratemp);
    header("location:admin.php?menu=planilha");
}
?>