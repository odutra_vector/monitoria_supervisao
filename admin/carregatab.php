<ul id="visuarvore">
<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once('functionsadm.php');
include_once($rais.'/monitoria_supervisao/classes/class.arvore.php');

$idtab = $_POST['idtab'];

$selpergs = "SELECT t.idperguntatab,pt.descriperguntatab FROM tabulacao t INNER JOIN perguntatab pt ON pt.idperguntatab = t.idperguntatab WHERE idtabulacao='$idtab' GROUP BY t.idperguntatab ORDER BY t.posicao";
$eselpergs = $_SESSION['query']($selpergs) or die ("erro na query de consulta das perguntas");
while($lpergs = $_SESSION['fetch_array']($eselpergs)) {
    $pergs[$lpergs['idperguntatab']] = $lpergs['descriperguntatab'];
}
$idsprox = array();
foreach($pergs as $idperg => $descri) {
    if(in_array($idperg,$idsprox))  {
    }
    else {
?>
    <li class="perg<?php echo $idperg;?>"><strong><?php echo $descri;?></strong>
        <ul>
            <?php
            $selresp = "SELECT t.idrespostatab,rt.descrirespostatab,t.idproxpergunta FROM tabulacao t INNER JOIN respostatab rt ON rt.idrespostatab = t.idrespostatab WHERE t.idtabulacao='$idtab' AND t.idperguntatab='".$idperg."'";
            $eselresp = $_SESSION['query']($selresp) or die ("erro na query de consulta das respostas");
            while($lresp = $_SESSION['fetch_array']($eselresp)) {
                ?>
                <li class="resp<?php echo $idperg;?>"><?php echo $lresp['descrirespostatab']."</li>";
                if($lresp['idproxpergunta'] != "") {
                    $tabprox = new ArvoreTab();
                    $tabprox->continua = 1;
                    $tabprox->idproxperg = $lresp['idproxpergunta'];
                    $idsprox[] = $lresp['idproxpergunta'];
                    while($tabprox->continua > $c) {
                        echo "<ul>";
                        $c = 0;
                        $tabprox->arvore_tab($idtab, $tabprox->idproxperg);
                        $idsprox[] = $tabprox->idproxperg;
                    }
                }
                else {
                }
                ?>
            <?php
            }
            ?>
        </ul>
    </li>
<?php
    }
}
?>
</ul>