<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/classes/class.tabelas.php');
include_once("functionsadm.php");

if(isset($_POST['altera'])) {
    $idsuper = $_POST['idsuper_oper'];
    $block = array('idsuper_oper','dtbase','horabase');
    $selcols = "SHOW COLUMNS FROM super_oper";
    $eselcols = $_SESSION['query']($selcols) or die ("erro na query de consulta das colunas da tabela super_oper");
    while($lselcols = $_SESSION['fetch_array']($eselcols)) {
        if($lselcols['Field'] != "idsuper_oper") {
            if(!in_array($lselcols['Field'],$block)) {
                $uni = "SELECT unico, tipo FROM coluna_oper WHERE nomecoluna='".$lselcols['Field']."'";
                $euni = $_SESSION['fetch_array']($_SESSION['query']($uni)) or die ("erro na query de consulta se o campo Ã© Ãºnico ou nÃ£o");
                if($euni['unico'] == "S") {
                    $unicos[] = $lselcols['Field'];
                }
                if($lselcols['Type'] == "date") {
                    $upsql[] = $lselcols['Field']."=".val_vazio(data2banco($_POST[$lselcols['Field']]));
                    $campos[] = $lselcols['Field']; 
                    $valores[$lselcols['Field']] = data2banco($_POST[$lselcols['Field']]);
                }
                $upsql[] = $lselcols['Field']."=".val_vazio($_POST[$lselcols['Field']]);
                $campos[] = $lselcols['Field']; 
                $valores[$lselcols['Field']] = $_POST[$lselcols['Field']];
            }
        }
    }

    $duplicado = 0;
    foreach($unicos as $u) {
        $verifu = "SELECT COUNT(*) as result FROM super_oper WHERE $u='$valores[$u]' AND idsuper_oper<>'$idsuper'";
        $everifu = $_SESSION['fetch_array']($_SESSION['query']($verifu)) or die ("erro na query de verificaÃ§Ã£o dos campos");
        if($everifu['result'] >= 1) {
            $duplicado++;
            $campd[] = strtoupper($u);
        }
    }

    if($duplicado >= 1) {
        $msg = "O/s valor/es do/s campo/s ".implode(",",$u)." já está/ão cadastrado/s para outro Supervisor";
        header("location:admin.php?menu=supervisor&visu=det&idsuper=$idsuper&msg=$msg");
    }
    else {
        $alt = 0;
        $check = "SELECT * FROM super_oper WHERE idsuper_oper='$idsuper'";
        $echeck = $_SESSION['fetch_array']($_SESSION['query']($check)) or die ("erro na query de consulta dos dados do supervisor");
        foreach($campos as $c) {
            if($valores[$c] == $echeck[$c]) {
            }
            else {
                $alt++;
            }
        }
        if($alt >= 1) {
            $up = "UPDATE super_oper SET ".implode(",",$upsql)." WHERE idsuper_oper='$idsuper'";
            $eup = $_SESSION['query']($up) or die ("erro na query de atualizaÃ§Ã£o do supervisor");
            if($eup) {
                $msg = "Cadastro atualizado com sucesso";
                header("location:admin.php?menu=supervisor&visu=det&idsuper=$idsuper&msg=$msg");
            }
            else {
                $msg = "Erro na query de atualização dos dados, favor contatar o administrador";
                header("location:admin.php?menu=supervisor&visu=det&idsuper=$idsuper&msg=$msg");
            }
        }
        else {
            $msg = "Nenhum dado foi alterado, processo não executado";
            header("location:admin.php?menu=supervisor&visu=det&idsuper=$idsuper&msg=$msg");
        }
    }
}

if(isset($_POST['delete'])) {
    if(!is_numeric($_POST['id'])) {
        echo "-1";
    }
    else {
        $idoper = $_POST['iddel'];
        $idnewoper = $_POST['id'];
        $tabelas = array('fila_grava','fila_oper','monitoriacalib','monitoria','regmonitoria','operador');
        $c = 0;

        $checkid = "SELECT count(*) as r FROM super_oper WHERE idsuper_oper ='$idnewoper'";
        $echeck = $_SESSION['fetch_array']($_SESSION['query']($checkid)) or die (mysql_error());
        if($echeck['r'] >= 1) {
            foreach($tabelas as $tab) {
                $sel = "SELECT count(*) as r FROM $tab WHERE idsuper_oper='$idoper'";
                $esel = $_SESSION['fetch_array']($_SESSION['query']($sel)) or die (mysql_error());
                if($esel['r'] >= 1) {
                    $updados = "UPDATE $tab SET idsuper_oper='$idnewoper' WHERE idsuper_oper='$idoper'";
                    $eupdados = $_SESSION['query']($updados);
                    if($eupdados) {
                        $c++;
                    }
                }
            }
            $deloper = "DELETE FROM super_oper WHERE idsuper_oper='$idoper'";
            $edeloper = $_SESSION['query']($deloper) or die (mysql_error());
            echo $c;
        }
        else {
            echo "-1";
        }
    }
}
?>
