<?php
$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$caracter = "[&*)(%&$#@!'~;?/<>,.]";
$user = $_SESSION['usuarioID'];
$data = date('Y-m-d');
$hora = date('H:i:s');
$tpmoni = $_POST['tpmoni'];
$nome = $_POST['nome'];
$pesq = $_POST['pesquisa'];
$idrel = $_POST['id'];
$qtdgrava = $_POST['qtdgrava'];
if($_POST['qtdeext'] == "") {
    $qtdeext = 0;
}
else {
    $qtdeext = $_POST['qtdeext'];
}
$qtdoper = $_POST['qtdoper'];
$multiplica = $_POST['multiplica'];
if($multiplica == "") {
  $estima = $qtdgrava;
}
if($multiplica != "") {
  $estima = $qtdoper * $multiplica;
}
$tma = $_POST['tma'];
$tm = $_POST['tm'];
if($tma == "" OR $estima == "" OR $tm == "" OR $tma == "") {
    if(isset($_POST['replicadimen'])) {
    }
    else {
        $seltipo = "SELECT tipomoni FROM conf_rel cr
                    INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                    WHERE idrel_filtros='$idrel'";
        $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
    }
}
else {
    $calctempo = "SELECT (TIME_TO_SEC('$tma') * $estima) as tta, ((TIME_TO_SEC('$tm') + TIME_TO_SEC('$tma')) * $estima) as ttm";
    $ecalc = $_SESSION['fetch_array']($_SESSION['query']($calctempo)) or die ("erro na query de calculo dos tempos");
}
$ttaseg = $ecalc['tta'];
$ttmseg = $ecalc['ttm'];
$ttahr = $_POST['tta'];
$ttmhr = $_POST['ttm'];
$periodo = $_POST['periodo'];
$dias = $_POST['dias'];
$filtros = array();
if($ttmseg == "") {
}
else {
    $seltipo = "SELECT ($ttmseg / p.diasprod / TIME_TO_SEC(hmonitor)) as qtdemoni, pm.tipomoni, pm.hpa, pm.hmonitor as hm, TIME_TO_SEC(pm.hpa) as hpa, TIME_TO_SEC(pm.hmonitor) as hmonitor, cr.idconf_rel FROM param_moni pm
                INNER JOIN conf_rel cr ON cr.idparam_moni = pm.idparam_moni
                INNER JOIN periodo p
                INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros
                WHERE rf.idrel_filtros='$idrel'";
    $eseltipo = $_SESSION['fetch_array']($_SESSION['query']($seltipo)) or die (mysql_error());
}
$seldias = "SELECT p.diasprod, COUNT(idinterperiodo) as result FROM periodo p INNER JOIN interperiodo ip ON ip.idperiodo = p.idperiodo WHERE p.idperiodo='$periodo'";
$eseldias = $_SESSION['fetch_array']($_SESSION['query']($seldias)) or die (mysql_error());
$divper = $eseldias['result'];
$hrhpa = $eseltipo['hpa'];
$hrmonitor = $eseltipo['hmonitor'];
$qtdepa = round($ttmseg / $eseldias['diasprod'] / $hrhpa,2);
$qtdemonitor = round($ttmseg / $eseldias['diasprod'] / $eseltipo['hmonitor'],2);
if($qtdemonitor <= 1) {
  $qtdemonitor = 1;
}
else {
  $qtdemonitor = $qtdemonitor;
}
$metamonitor = ceil($estima / $eseldias['diasprod'] / $qtdemonitor);
if($eseltipo['qtdemoni'] >= 1) {
  $hrpmonitor = $eseltipo['hm'];
}
else {
  $extraihr = round(($ttmseg / $eseldias['diasprod']),2);
  $calcmoni = "SELECT SEC_TO_TIME($extraihr) as hrmonitor";
  $ecalcmoni = $_SESSION['fetch_array']($_SESSION['query']($calcmoni)) or die ("erro na query de calculo das horas do monitor");
  $hrpmonitor = $ecalcmoni['hrmonitor'];
}
$tipomoni = $eseltipo['tipomoni'];
$url = "location:admin.php?menu=dimensiona&periodo=".$periodo."&tpmoni=".$tpmoni."&idrel=".$idrel."&nome=".$nome."&ativo=".$_POST['ativop']."&pesquisa=".$pesq;

if(isset($_POST['cadastra'])) {
    if($tma == "" OR $estima == "" OR $tm == "" OR $tma == "" OR $ttmseg == "") {
        $msg = "Os campos de tempo não podem estar vazios para cadastro do dimensionamento";
        header(url($url, $msg));
    }
    else {
        $seldimen = "SELECT COUNT(*) as result FROM dimen_estim WHERE idrel_filtros='$idrel' AND idperiodo='$periodo'";
        $eseldimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
        if($tipomoni == "G") {
            if($eseldimen['result'] >= 1) {
                $msg = "Já existe dimensionamento cadastrado para este relacionamento com o mesmo período informado";
                header(url($url, $msg));
            }
            else {
                if($qtdgrava == "" || $tma == "" || $tm == "" || $tma == "00:00:00" || $tm == "00:00:00") {
                    $msg = "O/s campo/s ''Qtde. gravações'' não podem estar vazia";
                    header(url($url, $msg));
                }
                else {
                    if(!is_numeric($qtdgrava) || !is_numeric($estima) || !is_numeric($divper)) {
                        $msg = "O/s campo/s ''Qtde. Operadores ou Estimativa ou Divisão Período'' devem conter somente numeros inteiros";
                        header(url($url, $msg));
                    }
                    else {
                        if(eregi($caracter, $qtdgrava) || eregi($caracter, $estima) || eregi($caracter, $divper)) {
                            $msg = "O/s campo/s ''Qtde. Gravação ou Estimativa ou Divisão Período'' contém caracteres inválidos, favor verificar!!!";
                            header(url($url, $msg));
                        }
                        else {
                            if(eregi($caracter, $tma) || eregi($caracter, $tta) || eregi($caracter, $tm) || eregi($caracter, $ttm)) {
                                $msg = "O/s campo/s ''TMA ou TTA ou TM ou TTM'' contém caracteres invÃ¡lidos para tempo, favor verificar!!!";
                                header(url($url, $msg));
                            }
                            else {
                                $tabs = array('dimen_estim','dimen_mod','conf_rel','configselecao');
                                $insert = "INSERT INTO dimen_estim (idrel_filtros, qnt_grava, estimativa, qtdeext, tma, tta, tm, ttm, idperiodo, divperiodo, qnt_pad, qnt_monitores, hs_monitor, meta_monitor, datacad, horacad, iduser)
                                  VALUE ('$idrel', '$qtdgrava', '$estima', '$qtdeext', '$tma', '$ttahr', '$tm', '$ttmhr', '$periodo', '$divper', '$qtdepa', '$qtdemonitor', '$hrpmonitor', '$metamonitor', '$data', '$hora', '$user')";
                                $einsert = $_SESSION['query']($insert) or die (mysql_error());
                                $idinsert = $_SESSION['insert_id']();
                                if($einsert) {
                                    $verifselecao = "SELECT COUNT(*) as result FROM configselecao WHERE idrel_filtros='$idrel'";
                                    $everifselecao = $_SESSION['fetch_array']($_SESSION['query']($verifselecao)) or die (mysql_error());
                                    $checkatv = "SELECT COUNT(*) as result FROM conf_rel WHERE idrel_filtros='$idrel'";
                                    $echeckatv = $_SESSION['fetch_array']($_SESSION['query']($checkatv)) or die (mysql_error());
                                    if($echeckatv['result'] >= 1) {
                                        if($everifselecao['result'] >= 1) {
                                            $sqlselecao = "UPDATE configselecao SET qtdedia='0',reposicao='0',dmenos='1' WHERE idrel_filtros='$idrel'";
                                            $acaolog = "update";
                                        }
                                        if($everifselecao['result'] == 0) {
                                            $sqlselecao = "INSERT INTO configselecao (qtdedia,reposicao,dmenos,idrel_filtros) VALUES ('0',0,'1','$idrel')";
                                            $acaolog = "insert";
                                        }
                                        $exeselecao = $_SESSION['query']($sqlselecao) or die (mysql_error());
                                    }
                                }
                                $insertmod = "INSERT INTO dimen_mod (iddimen_mod,idrel_filtros, qnt_grava, estimativa,qtdeext, tma, tta, tm, ttm, idperiodo, divperiodo, qnt_pad, qnt_monitores, hs_monitor, meta_monitor, datacad, horacad, iduser) 
                                                         VALUE ('$idinsert','$idrel', '$qtdgrava', '$estima','$qtdeext', '$tma', '$ttahr', '$tm', '$ttmhr', '$periodo', '$divper', '$qtdepa', '$qtdemonitor', '$hrpmonitor', '$metamonitor', '$data', '$hora', '$user')";
                                $einsertmod = $_SESSION['query']($insertmod);
                                $somattm = "SELECT SUM(TIME_TO_SEC(ttm)) as somattm, SUM(estimativa) as metatotal FROM dimen_mod WHERE idperiodo='$periodo'";
                                $esomattm = $_SESSION['fetch_array']($_SESSION['query']($somattm)) or die (mysql_error());
                                $selpart = "SELECT estimativa, idrel_filtros, iddimen_mod, TIME_TO_SEC(ttm) as ttm FROM dimen_mod WHERE idperiodo='$periodo'";
                                $eselpart = $_SESSION['query']($selpart) or die (mysql_error());
                                while($lselpart = $_SESSION['fetch_array']($eselpart)) {
                                    $hrttm = $lselpart['ttm'];
                                    $percpart = round(($hrttm / $esomattm['somattm'] * 100),2);
                                    $percmeta = round(($lselpart['estimativa'] / $esomattm['metatotal'] * 100),2);
                                    $updateperc = "UPDATE dimen_estim SET percpartpa='$percpart', percpartmeta='$percmeta' WHERE idrel_filtros='".$lselpart['idrel_fitlros']."' AND idperiodo='$periodo'";
                                    $eupdateperc = $_SESSION['query']($updateperc) or die (mysql_error());
                                    $upercmod = "UPDATE dimen_mod SET percpartpa='$percpart', percpartmeta='$percmeta' WHERE iddimen_mod='".$lselpart['iddimen_mod']."'";
                                    $eupercmod = $_SESSION['query']($upercmod) or die (mysql_error());
                                }
                                if($einsert) {
                                    $msg = "Dimensionamento cadastrado com sucesso!!!";
                                    header(url($url, $msg));
                                }
                                else {
                                    $msg = "Erro no processo de cadastramento, favor contatar o administrador";
                                    header(url($url, $msg));
                                }
                            }
                        }
                    }
                }
            }
        }
        if($tipomoni == "O") {
            if($qtdoper == "" || $tma == "" || $tm == "" || $multiplica == "" || $tma == "00:00:00" || $tm == "00:00:00") {
                $msg = "O campo ''Qtde. Operadores ou Multiplicador ou Tempo de Atendimento ou Tempo de Monitoria'' não podem estar vazio";
                header(url($url, $msg));
            }
            else {
                if($eseldimen['result'] >= 1) {
                    $msg = "Já existe dimensionamento cadastrado para este relacionamento com o mesmo período informado";
                    header(url($url, $msg));
                }
                else {
                    if(!is_numeric($qtdoper) || !is_numeric($multiplica) || !is_numeric($estima) || !is_numeric($divper)) {
                        $msg = "O/s campo/s ''Qtde. Operadores ou Estimativa ou DivisÃ£o Período'' devem conter somente numeros inteiros";
                        header(url($url, $msg));
                    }
                    else {
                        if(eregi($caracter, $qtdoper) || eregi($caracter, $estima) || eregi($caracter, $divper)) {
                            $msg = "O/s campo/s ''Qtde. Gravações ou Estimativa ou Divisão do Período'' contém caracteres inválidos, favor verificar!!!";
                            header(url($url, $msg));
                        }
                        else {
                            if(eregi($caracter, $tma) || eregi($caracter, $tta) || eregi($caracter, $tm) || eregi($caracter, $ttm)) {
                                $msg = "O/s campo/s ''TMA ou TTA ou TM ou TTM'' contém caracteres invélidos para tempo, favor verificar!!!";
                                header(url($url, $msg));
                            }
                            else {
                                $tabs = array('dimen_estim','dimen_mod','conf_rel','configselecao');
                                $insert = "INSERT INTO dimen_estim (idrel_filtros, qnt_oper, multiplicador, estimativa, qtdeext,tma, tta, tm, ttm, idperiodo, divperiodo, qnt_pad, qnt_monitores, hs_monitor, meta_monitor, datacad, horacad, iduser) 
                                                 VALUE ('$idrel', '$qtdoper', '$multiplica', '$estima','$qtdeext', '$tma', '$ttahr', '$tm', '$ttmhr', '$periodo', '$divper', '$qtdepa', '$qtdemonitor', '$hrpmonitor', '$metamonitor','$data', '$hora', '$user')";
                                $einsert = $_SESSION['query']($insert) or die (mysql_error());
                                $idinsert = $_SESSION['insert_id']();
                                $insertmod = "INSERT INTO dimen_mod (iddimen_mod,idrel_filtros, qnt_oper, multiplicador, estimativa,qtdeext, tma, tta, tm, ttm, idperiodo, divperiodo, qnt_pad, qnt_monitores, hs_monitor, meta_monitor, datacad, horacad, iduser) 
                                                        VALUE ('$idinsert','$idrel', '$qtdoper', '$multiplica', '$estima', '$qtdeext','$tma', '$ttahr', '$tm', '$ttmhr', '$periodo', '$divper', '$qtdepa', '$qtdemonitor', '$hrpmonitor', '$metamonitor','$data', '$hora', '$user')";
                                $einsertmod = $_SESSION['query']($insertmod);
                                $somattm = "SELECT SUM(TIME_TO_SEC(ttm)) as somattm, SUM(estimativa) as metatotal FROM dimen_mod WHERE idperiodo='$periodo'";
                                $esomattm = $_SESSION['fetch_array']($_SESSION['query']($somattm)) or die (mysql_error());
                                $selpart = "SELECT estimativa, idrel_filtros, iddimen_mod, TIME_TO_SEC(ttm) as ttm FROM dimen_mod WHERE idperiodo='$periodo'";
                                $eselpart = $_SESSION['query']($selpart) or die (mysql_error());
                                while($lselpart = $_SESSION['fetch_array']($eselpart)) {
                                    $hrttm = $lselpart['ttm'];
                                    $percpart = round(($hrttm / $esomattm['somattm'] * 100),2);
                                    $percmeta = round(($lselpart['estimativa'] / $esomattm['metatotal'] * 100),2);
                                    $updateperc = "UPDATE dimen_estim SET percpartpa='$percpart', percpartmeta='$percmeta' WHERE idrel_filtros='".$lselpart['idrel_filtros']."' AND idperiodo='$periodo'";
                                    $eupdateperc = $_SESSION['query']($updateperc) or die (mysql_error());
                                    $upercmod = "UPDATE dimen_mod SET percpartpa='$percpart', percpartmeta='$percmeta' WHERE iddimen_mod='".$lselpart['iddimen_mod']."'";
                                    $eupercmod = $_SESSION['query']($upercmod) or die (mysql_error());
                                }
                                if($einsert) {
                                    $msg = "Dimensionamento cadastrado com sucesso!!!";
                                    header(url($url, $msg));
                                }
                                else {
                                    $msg = "Erro no processo de cadastramento, favor contatar o administrador";
                                    header(url($url, $msg));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['altera'])) {
    $iddimen = $_POST['iddimen'];
    $seldimen = "SELECT * FROM dimen_mod WHERE iddimen_mod='$iddimen'";
    $eseldimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
    $selper = "SELECT diasprod, COUNT(*) as result FROM periodo p INNER JOIN interperiodo i ON i.idperiodo = p.idperiodo WHERE p.idperiodo='$periodo'";
    $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta dos dias produtivos do perÃ­odo");
    if($_POST['qtdemonitor'] != $eseldimen['qnt_monitores']) {
        $qtdemonitor = $_POST['qtdemonitor'];
        $extraihr = round(($ttmseg / $eseldias['diasprod'] / $qtdemonitor),2);
        $calcmoni = "SELECT SEC_TO_TIME($extraihr) as hrmonitor";
        $ecalcmoni = $_SESSION['fetch_array']($_SESSION['query']($calcmoni)) or die ("erro na query de calculo das horas do monitor");
        $hrpmonitor = $ecalcmoni['hrmonitor'];
        $metamonitor = ceil($estima / $eseldias['diasprod'] / $qtdemonitor);
    }
    $dias = $eselper['diasprod'];
    $sem = $eselper['result'];

    if($tipomoni == "G") {
        if($qtdgrava == "" || $tma == "" || $tm == "" || $tma == "00:00:00" || $tm == "00:00:00") {
            $msg = "O/s campo/s ''Qtde. Gravações ou Tempo de Atendimento ou Tempo de Monitoria'' não podem estar vazio";
            header(url($url, $msg));
        }
        else {
            if(!is_numeric($qtdgrava) || !is_numeric($estima) || !is_numeric($divper)) {
                $msg = "O/s campo/s ''Qtde. Gravações ou Estimativa ou Divisão Período'' devem conter somente numeros inteiros";
                header(url($url, $msg));
            }
            else {
                if(eregi($caracter, $qtdgrava) || eregi($caracter, $estima) || eregi($caracter, $divper)) {
                  $msg = "O/s campo/s ''Qtde. Gravação ou Estimativa ou Divsão Período'' contém caracteres invÃ¡lidos, favor verificar!!!";
                  header(url($url, $msg));
                }
                else {
                    if(eregi($caracter, $tma) || eregi($caracter, $tta) || eregi($caracter, $tm) || eregi($caracter, $ttm)) {
                      $msg = "O/s campo/s ''TMA ou TTA ou TM ou TTM'' contém caracteres inválidos para tempo, favor verificar!!!";
                      header(url($url, $msg));
                    }
                    else {
                        if($qtdgrava == $eseldimen['qnt_grava'] && $estima == $eseldimen['estimativa'] && $eseldimen['qtdeext'] == $qtdeext && $tma == $eseldimen['tma'] && $ttahr == $eseldimen['tta'] && $tm == $eseldimen['tm']
                        && $ttmhr == $eseldimen['ttm'] && $divper == $eseldimen['divperiodo'] && $qtdepa == $eseldimen['qnt_pad'] && $_POST['qtdemonitor'] == $eseldimen['qnt_monitores']) {
                          $msg = "Não foi efetuada nenhuma alteração nos campos, processo não executado!!!";
                          header(url($url, $msg));
                        }
                        else {
                            $update = "UPDATE dimen_mod SET qnt_oper='0', multiplicador='0', qnt_grava='$qtdgrava', estimativa='$estima', qtdeext='$qtdeext',tma='$tma', tta='$ttahr', tm='$tm', ttm='$ttmhr', divperiodo='$divper', qnt_pad='$qtdepa', qnt_monitores='$qtdemonitor', hs_monitor='$hrpmonitor', meta_monitor='$metamonitor' WHERE iddimen_mod='$iddimen'";
                            $eupdate = $_SESSION['query']($update) or die (mysql_error());
                            $somattm = "SELECT SUM(TIME_TO_SEC(ttm)) as somattm, SUM(estimativa) as metatotal FROM dimen_mod WHERE idperiodo='$periodo'";
                            $esomattm = $_SESSION['fetch_array']($_SESSION['query']($somattm)) or die (mysql_error());
                            $selpart = "SELECT iddimen_mod, TIME_TO_SEC(ttm) as ttm, estimativa FROM dimen_mod WHERE idperiodo='$periodo'";
                            $eselpart = $_SESSION['query']($selpart) or die (mysql_error());
                            $metamoni = $qtdgrava / $qtdemonitor;
                            $metasem = $metamoni / $sem;
                            $metadia = $metamoni / $dias;
                            $selmeta = "SELECT iddimen_mod,iddimen_moni FROM dimen_moni WHERE iddimen_mod='$iddimen'";
                            $eselmeta = $_SESSION['query']($selmeta) or die ("erro na query de consulta do dimensionamento dos monitores");
                            while($lselmeta = $_SESSION['fetch_array']($eselmeta)) {
                                $upmonimeta = "UPDATE dimen_moni SET meta_m='$metamoni',meta_s='$metasem',meta_d='$metadia' WHERE iddimen_moni='".$lselmeta['iddimen_moni']."'";
                                $eupmoni = $_SESSION['query']($upmonimeta) or die ("erro na query para atualizaÃ§Ã£o das meta por monitores");
                            }
                            while($lselpart = $_SESSION['fetch_array']($eselpart)) {
                                $hrttm = $lselpart['ttm'];
                                $percpart = round(($hrttm / $esomattm['somattm'] * 100),2);
                                $percmeta = round(($lselpart['estimativa'] / $esomattm['metatotal'] * 100),2);
                                $updateperc = "UPDATE dimen_mod SET percpartpa='$percpart', percpartmeta='$percmeta' WHERE iddimen_mod='".$lselpart['iddimen_mod']."'";
                                $eupdateperc = $_SESSION['query']($updateperc) or die (mysql_error());
                            }
                            if($eupdate) {
                                $msg = "Alteração realizada com sucesso!!!";
                                header(url($url, $msg));
                            }
                            else {
                                $msg = "Erro no processo de alteraçã, favor contatar o administrador!!!";
                                header(url($url, $msg));
                            }
                        }
                    }
                }
            }
        }
    }
    if($tipomoni == "O") {
        if($qtdoper == "" || $tma == "" || $tm == "" || $multiplica == "" || $tma == "00:00:00" || $tm == "00:00:00") {
            $msg = "O/s campo/s ''Qtde. Operadores ou Multiplicador ou Tempo de Atendimentou Tempo de Monitoria'' não podem estar vazio";
            header(url($url, $msg));
        }
        else {
            if(!is_numeric($qtdoper) || !is_numeric($multiplica) || !is_numeric($estima) || !is_numeric($divper)) {
                $msg = "O/s campo/s ''Qtde. Operadores ou Estimativa ou Divisão Período'' devem conter somente numeros inteiros";
                header(url($url, $msg));
            }
            else {
                if(eregi($caracter, $qtdoper) || eregi($caracter, $multiplica) || eregi($caracter, $estima) || eregi($caracter, $divper)) {
                    $msg = "O/s campo/s ''Qtde. Operadores ou Estimativa ou Divsão Período'' contém caracteres inválidos, favor verificar!!!";
                    header(url($url, $msg));
                }
                else {
                    if(eregi($caracter, $tma) || eregi($caracter, $tta) || eregi($caracter, $tm) || eregi($caracter, $ttm)) {
                      $msg = "O/s campo/s ''TMA ou TTA ou TM ou TTM'' contém caracteres inválidos para tempo, favor verificar!!!";
                      header(url($url, $msg));
                    }
                    else {
                        if($qtdoper == $eseldimen['qnt_oper'] && $multiplica == $eseldimen['multiplicador'] && $estima == $eseldimen['estimativa'] && $tma == $eseldimen['tma'] && $ttahr == $eseldimen['tta'] && $tm == $eseldimen['tm'] && $ttmhr == $eseldimen['ttm'] && $divper == $eseldimen['divperiodo'] && $qtdepa == $eseldimen['qnt_pad'] && $_POST['qtdemonitor'] == $eseldimen['qnt_monitores']) {
                          $msg = "Não foi efetuada nenhuma alteração nos campos, processo não executado!!!";
                          header(url($url, $msg));
                        }
                        else {
                            $update = "UPDATE dimen_mod SET qnt_grava='0', qnt_oper='$qtdoper', multiplicador='$multiplica', estimativa='$estima', tma='$tma', tta='$ttahr', tm='$tm', ttm='$ttmhr', divperiodo='$divper', qnt_pad='$qtdepa', qnt_monitores='$qtdemonitor', hs_monitor='$hrpmonitor', meta_monitor='$metamonitor' WHERE iddimen_mod='$iddimen'";
                            $eupdate = $_SESSION['query']($update) or die (mysql_error());
                            $somattm = "SELECT SUM(TIME_TO_SEC(ttm)) as somattm, SUM(estimativa) as metatotal FROM dimen_mod WHERE idperiodo='$periodo'";
                            $esomattm = $_SESSION['fetch_array']($_SESSION['query']($somattm)) or die (mysql_error());
                            $selpart = "SELECT iddimen_mod, TIME_TO_SEC(ttm) as ttm, estimativa FROM dimen_mod WHERE idperiodo='$periodo'";
                            $eselpart = $_SESSION['query']($selpart) or die (mysql_error());
                            $metamoni = $estima / $qtdemonitor;
                            $metasem = $metamoni / $sem;
                            $metadia = $metamoni / $dias;
                            $selmeta = "SELECT iddimen_mod,iddimen_moni FROM dimen_moni WHERE iddimen_mod='$iddimen'";
                            $eselmeta = $_SESSION['query']($selmeta) or die ("erro na query de consulta do dimensionamento dos monitores");
                            while($lselmeta = $_SESSION['fetch_array']($eselmeta)) {
                                $upmonimeta = "UPDATE dimen_moni SET meta_m='$metamoni',meta_s='$metasem',meta_d='$metadia' WHERE iddimen_moni='".$lselmeta['iddimen_moni']."'";
                                $eupmoni = $_SESSION['query']($upmonimeta) or die ("erro na query para atualizaÃ§Ã£o das meta por monitores");
                            }
                            while($lselpart = $_SESSION['fetch_array']($eselpart)) {
                                $hrttm = $lselpart['ttm'];
                                $percpart = round(($hrttm / $esomattm['somattm'] * 100),2);
                                $percmeta = round(($lselpart['estimativa'] / $esomattm['metatotal'] * 100),2);
                                $updateperc = "UPDATE dimen_mod SET percpartpa='$percpart', percpartmeta='$percmeta' WHERE iddimen_mod='".$lselpart['iddimen_mod']."'";
                                $eupdateperc = $_SESSION['query']($updateperc) or die (mysql_error());
                            }
                            if($eupdate) {
                                $msg = "Alteração realizada com sucesso!!!";
                                header(url($url, $msg));
                            }
                            else {
                                $msg = "Erro no processo de alteração, favor contatar o administrador!!!";
                                header(url($url, $msg));
                            }
                        }
                    }
                }
            }
        }
    }
}

if(isset($_POST['replicadimen'])) {
    $periodoref = $_POST['referencia'];
    $periodonovo = $_POST['referenciado'];
    $seldias = "SELECT p.diasprod, COUNT(idinterperiodo) as result FROM periodo p INNER JOIN interperiodo ip ON ip.idperiodo = p.idperiodo WHERE p.idperiodo='$periodonovo'";
    $eseldias = $_SESSION['fetch_array']($_SESSION['query']($seldias)) or die (mysql_error());
    $selpernovo = "SELECT *,COUNT(*) as result FROM dimen_mod WHERE idperiodo='$periodonovo'";
    $eselnovo = $_SESSION['fetch_array']($_SESSION['query']($selpernovo)) or die ("erro na query de consulta do periodo que será referenciado");
    $selref = "SELECT * FROM dimen_mod WHERE idperiodo='$periodoref'";
    $eselref = $_SESSION['query']($selref) or die ("erro na query de consulta do dimensionamento de referencia");
    $nlinhasref = $_SESSION['num_rows']($eselref);
    $selcol = "SHOW COLUMNS FROM dimen_mod";
    $eselcol = $_SESSION['query']($selcol) or die ("erro na query de consulta das colunas do dimensionamento");
    while($lselcol = $_SESSION['fetch_array']($eselcol)) {
        if($lselcol['Field'] == "iddimen_mod") {
        }
        else {
            $cols[] = $lselcol['Field']; 
        }
    }
    $selcolmoni = "SHOW COLUMNS FROM dimen_moni";
    $eselmoni = $_SESSION['query']($selcolmoni) or die ("erro na query de consulta do dimensionamento dos monitores");
    while($lselmoni = $_SESSION['fetch_array']($eselmoni)) {
        if($lselmoni['Field'] == "iddimen_moni") {
        }
        else {
            $colsmoni[] = $lselmoni['Field'];
        }
    }
    if($periodoref == "" OR $periodonovo == "") {
        $msg = "Ambos os períodos precisam ser informados para realizar a ação";
        header("location:/monitoria_supervisao/admin/replicadimen.php?msg=$msg");
    }
    else {
        if($periodoref == $periodonovo) {
            $msg = "O período de referencia não pode ser igual ao período referenciado";
            header("location:/monitoria_supervisao/admin/replicadimen.php?msg=$msg");
        }
        else {
            if($eselnovo['result'] >= 1) {
                $msg = "O período referenciado já possui dimensionamento cadastrado";
                header("location:/monitoria_supervisao/admin/replicadimen.php?msg=$msg");
            }
            else {
                if($nlinhasref == 0) {
                    $msg = "Não existe dimensionamento cadastrado para o mês referenciado";
                    header("location:/monitoria_supervisao/admin/replicadimen.php?msg=$msg");
                }
                else {
                    $tabs = array('dimen_mod','dimen_estim','configselecao','conf_rel','dimen_moni');
                    while($lselref = $_SESSION['fetch_array']($eselref)) {
                        $val = array();
                        foreach($cols as $col) {
                            if($col == "idperiodo") {
                                $val[] = $periodonovo;
                            }
                            else {
                                if($col == "datacad" OR $col == "horacad") {
                                    if($col == "datacad") {
                                        $val[] = val_vazio(date('Y-m-d'));
                                    }
                                    else {
                                        $val[] = val_vazio(date('H:i:s'));
                                    }
                                }
                                else {
                                    if($col == "iduser") {
                                        $val[] = val_vazio($_SESSION['usuarioID']);
                                    }
                                    else {
                                        $val[] = val_vazio($lselref[$col]);
                                    }
                                }
                            }
                        }
                        $verifselecao = "SELECT COUNT(*) as result FROM configselecao WHERE idrel_filtros='".$lselref['idrel_filtros']."'";
                        $everifselecao = $_SESSION['fetch_array']($_SESSION['query']($verifselecao)) or die (mysql_error());
                        $checkatv = "SELECT COUNT(*) as result FROM conf_rel WHERE idrel_filtros='".$lselref['idrel_filtros']."'";
                        $echeckatv = $_SESSION['fetch_array']($_SESSION['query']($checkatv)) or die (mysql_error());
                        if($echeckatv['result'] >= 1) {
                            if($everifselecao['result'] >= 1) {
                                $sqlselecao = "UPDATE configselecao SET qtdedia='0',reposicao=0,dmenos='1' WHERE idrel_filtros='".$lselref['idrel_filtros']."'";
                            }
                            if($everifselecao['result'] == 0) {
                                $sqlselecao = "INSERT INTO configselecao (qtdedia,reposicao,dmenos,idrel_filtros) VALUES ('0',0,'1','".$lselref['idrel_filtros']."')";
                            }
                            $exeselecao = $_SESSION['query']($sqlselecao) or die (mysql_error());
                        }
                        $insertmod = "INSERT INTO dimen_mod (".implode(",",$cols).") VALUES (".implode(",",$val).")";
                        $einsertmod = $_SESSION['query']($insertmod) or die ("erro na query de inserção do dimensionamento");
                        $idinsert = $_SESSION['insert_id']();
                        $insert = "INSERT INTO dimen_estim (iddimen_estim,".implode(",",$cols).") VALUES ('$idinsert',".implode(",",$val).")";
                        $einsert = $_SESSION['query']($insert) or die ("erro na query de inserção do dimensionamento");
                    }
                    $dimemmoni = "SELECT * FROM dimen_moni WHERE idperiodo='$periodoref' AND ativo='S'";
                    $edimemmoni  = $_SESSION['query']($dimemmoni) or die ("erro na consulta do dimensionamento do monitor");
                    $nmoni = $_SESSION['num_rows']($edimemmoni);
                    if($nmoni >= 1) {
                        while($lmoni = $_SESSION['fetch_array']($edimemmoni)) {
                            $dados = array();
                            foreach($colsmoni as $cmoni) {
                                if($cmoni == "idperiodo") {
                                    $dados[] = $periodonovo;
                                }
                                else {
                                    $dados[] = val_vazio($lmoni[$cmoni]);
                                }
                            }
                            $insertmoni = "INSERT INTO dimen_moni (".implode(",",$colsmoni).") VALUES (".implode(",",$dados).")";
                            $einsertmoni = $_SESSION['query']($insertmoni) or die ("erro na query de inserção dos dados do mmonitor");
                        }
                    }

                    $seldimen = "SELECT idrel_filtros, iddimen_moni  FROM dimen_moni WHERE idperiodo='$periodonovo' GROUP BY idrel_filtros";
                    $eseldimen = mysql_query($seldimen) or die (mysql_error());
                    while($ldimen = mysql_fetch_array($eseldimen)) {
                        $sd = "SELECT iddimen_mod, COUNT(*) as result FROM dimen_mod WHERE idrel_filtros='".$ldimen['idrel_filtros']."' and idperiodo='$periodonovo'";
                        $esd = mysql_fetch_array(mysql_query($sd)) or die (mysql_error());
                        if($esd['result'] == 0) {
                            $del = "DELETE FROM dimen_moni WHERE idrel_filtros='".$ldimen['idrel_filtros']."' AND idperiodo='$periodonovo'";
                            $edel = mysql_query($del) or die (mysql_error());
                        }
                        else {
                            $upd = "UPDATE dimen_moni SET iddimen_mod='".$esd['iddimen_mod']."' WHERE idrel_filtros='".$ldimen['idrel_filtros']."' and idperiodo='$periodonovo'";
                            $eup = mysql_query($upd) or die (mysql_error());
                        }
                    }
                    if($einsertmod) {
                        $msg .= "Dimensionamento copiado com sucesso";
                    }
                    if($einsertmoni) {
                        $msg .= ",e dimensionamento dos monitores copiado.";
                    }
                    if($einsertmod) {
                        header("location:/monitoria_supervisao/admin/replicadimen.php?msg=$msg");
                    }
                    else {
                        $msg = "Erro no processo para replicar o dimensionamento, favor contatar o administrador";
                        header("location:/monitoria_supervisao/admin/replicadimen.php?msg=$msg");
                    }
                }    
            }
        }
    }
}

if(isset($_POST['vincmoni'])) {
    $iddimen = $_POST['iddimen'];
    $monitores = $_POST['monitor'];
    $metad = $_POST['metamonitor'];
    $dias = $eseldias['diasprod'];
    $metam = $metad * $dias;
    $metas = round($metam / $eseldias['result']);
    $selmoni = "SELECT COUNT(*) as result FROM dimen_moni WHERE idrel_filtros='$idrel' AND iddimen_mod='$iddimen' AND idperiodo='$periodo' AND ativo='S'";
    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_error());
    if($_POST['qtdemonitor'] < 1) {
        if($eselmoni['result'] >= 1) {
             $faltante = 0;
        }
        else {
             $faltante = 1;
        }
    }
    if($_POST['qtdemonitor'] >= 1) {
      $faltante = $_POST['qtdemonitor'] - $eselmoni['result'];
    }
    if($monitores == "") {
        $msg = "Favor selecionar pelo menos 1 monitor para vincular ao relacionamento!!!";
        header(url($url, $msg));
    }
    else {
        if(count($monitores) > $faltante) {
            $msg = "A quantidade de monitores selecionados excede a quantidade definida de monitores!!!";
            header(url($url, $msg));
        }
        else {
            $insert = 0;
            foreach($monitores as $idmoni) {
                $cdimen = "SELECT *, COUNT(*) as result FROM dimen_moni WHERE idrel_filtros='$idrel' AND iddimen_mod='$iddimen' AND idperiodo='$periodo' AND idmonitor='$idmoni'";
                $ecdimen = $_SESSION['fetch_array']($_SESSION['query']($cdimen)) or die (mysql_error());
                if($ecdimen['result'] == 0) {
                    $selnome = "SELECT nomemonitor FROM monitor WHERE idmonitor='$idmoni'";
                    $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                    $nmonitor = $eselnome['nomemonitor'];
                    $cadmoni = "INSERT INTO dimen_moni (idrel_filtros, iddimen_mod, idmonitor, idperiodo, meta_m, meta_s, meta_d) VALUE ('$idrel', '$iddimen', '$idmoni', '$periodo', '$metam', '$metas', '$metad')";
                    $ecadmoni = $_SESSION['query']($cadmoni) or die (mysql_error());
                    if($ecadmoni) {
                      $insert = $insert + 1;
                    }
                    else {
                        $msg = "Erro no processo de cadastramento, favor contatar o administrador!!!";
                        header(url($url, $msg));
                    }
                }
                if($ecdimen['result'] >= 1) {
                    $iddimenmoni = $ecdimen['iddimen_moni'];
                    if($ecdimen['ativo'] == "N") {
                        $selnome = "SELECT nomemonitor FROM monitor WHERE idmonitor='$idmoni'";
                        $eselnome = $_SESSION['fetch_array']($_SESSION['query']($selnome)) or die (mysql_error());
                        $nmonitor = $eselnome['nomemonitor'];
                        $update = "UPDATE dimen_moni SET iddimen_mod='$iddimen', idmonitor='$idmoni', idperiodo='$periodo', meta_m='$metam', meta_s='$metas', meta_d='$metad', ativo='S' WHERE iddimen_moni='$iddimenmoni'";
                        $eupdate = $_SESSION['query']($update);
                        if($eupdate) {
                            $insert = $insert + 1;
                        }
                    }
                }
            }
            if($insert >= 1) {
                $msg = "$insert monitor/es vinculado/s com sucesso!!!";
                header(url($url, $msg));
            }
            else {
                $msg = "Nenhum monitor foi vinculado, pois todos escolhidos já estão cadastrados!!!";
                header(url($url, $msg));
            }
        }
    }
}

if(isset($_POST['desvmoni'])) {
    $iddimen = $_POST['iddimen'];
    $monitores = $_POST['monitor'];
    if(count($monitores) == 0) {
        $msg = "É necessário selecionar 1 monitor para desfazer o vinculo!!!";
        header(url($url, $msg));
    }
    else {
        $idmonitores = implode(",",$monitores);
        $seldimen = "SELECT COUNT(*) as result FROM dimen_moni WHERE idrel_filtros='$idrel' AND iddimen_mod='$iddimen' AND idperiodo='$periodo' AND idmonitor IN ($idmonitores) AND ativo='S'";
        $eseldimen = $_SESSION['fetch_array']($_SESSION['query']($seldimen)) or die (mysql_error());
        if($monitores == "") {
            $msg = "É necessÃ¡rio selecionar 1 monitor para desfazer o vinculo!!!";
            header(url($url, $msg));
        }
        else {
            if($eseldimen['result'] == 0) {
                $msg = "Este/s monitore/s nÃ£o estÃ¡/Ã£o vinculados e este relacionamento!!!";
                header(url($url, $msg));
            }
            else {
                $dellinhas = "UPDATE dimen_moni SET ativo='N' WHERE idrel_filtros='$idrel' AND iddimen_mod='$iddimen' AND idperiodo='$periodo' AND idmonitor IN ($idmonitores)";
                $edellinhas = $_SESSION['query']($dellinhas) or die (mysql_error());
                if($edellinhas) {
                    $msg = "Monitores desvinculados com sucesso!!!";
                    header(url($url, $msg));
                }
                else {
                    $msg = "Erro na execução do processo, favor contatar o administrador!!!";
                    header(url($url, $msg));
                }
            }
        }
    }
}

if(isset($_POST['apaga'])) {
    $iddimen = $_POST['iddimen'];
    $speriodo = "SELECT dataini, datafim FROM periodo WHERE idperiodo='$periodo'";
    $eperiodo = $_SESSION['query']($speriodo) or die (mysql_error());
    while($lperiodo = $_SESSION['fetch_array']($eperiodo)) {
        $dtinidb_ano = substr($lperiodo['dataini'],0,4);
        $dtinidb_mes = substr($lperiodo['dataini'],5,2);
        $dtinidb_dia = substr($lperiodo['dataini'],8,2);
        $dtfimdb_ano = substr($lperiodo['datafim'],0,4);
        $dtfimdb_mes = substr($lperiodo['datafim'],5,2);
        $dtfimdb_dia = substr($lperiodo['datafim'],8,2);
        $dinidb = mktime(0,0,0,$dtinidb_mes, $dtinidb_dia, $dtinidb_ano);
        $dfimdb = mktime(0,0,0,$dtfimdb_mes, $dtfimdb_dia, $dtfimdb_ano);
        $intervaldtdb = array($dinidb);
        while($dinidb <= $dfimdb) {
            $data = date('Y-m-d',$dinidb);
            $intervalper[] = $data;
            $dinidb += 86400;
        }
    }
    $diasmoni = 0;
    foreach($intervalper as $dia) {
        $selmoni = "SELECT COUNT(*) as result FROM monitoria WHERE data='$dia' AND idrel_filtros='$idrel'";
        $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die (mysql_error());
        if($eselmoni['result'] >= 1) {
          $diasmoni++;
        }
    }
    if($diasmoni >= 1) {
        $msg = "O dimensionamento não pode ser apagado pois já existe monitorias realizadas dentro do período vinculado ao relacionamento!!!";
        header(url($url, $msg));
    }
    else {
        $deldimenesti = "DELETE FROM dimen_estim WHERE idperiodo='$periodo' AND idrel_filtros='$idrel'";
        $edeldimenesti = $_SESSION['query']($deldimenesti) or die (mysql_error());
        $deldimenmod = "DELETE FROM dimen_mod WHERE idperiodo='$periodo' AND idrel_filtros='$idrel'";
        $edeldimenmod = $_SESSION['query']($deldimenmod) or die (mysql_error());
        $atudimenmoni = "DELETE FROM dimen_moni WHERE idperiodo='$periodo' AND iddimen_mod='$iddimen'";
        $eatudimen = ($_SESSION['query']($atudimenmoni)) or die ("erro na query de atualização do dimensionamento do monitor");
        $selult = "SELECT MAX(iddimen_mod) as ult FROM dimen_mod";
        $eselult = $_SESSION['fetch_array']($_SESSION['query']($selult)) or die ("erro na query de consulta do ultimo dimensionamento cadastrado");
        $auto = $eselult['ult'] + 1;
        $tabsatu = array('dimen_mod','dimen_estim');
        foreach($tabsatu as $tab) {
            $atu = "ALTER TABLE $tab AUTO_INCREMENT=$auto";
            $eatu = $_SESSION['query']($atu) or die ("erro na query de atualização do auto incremento");
        }
        if($edeldimenesti && $edeldimenmod) {
          $msg = "Dimensionamento apagado com sucesso!!!";
          header(url($url, $msg));
        }
        else {
          $msg = "Erro na execução do processo, favor contatar o administrador!!!";
          header(url($url, $msg));
        }
    }
}

function url($url,$msg) {
    $url = $url."&msg=".$msg;
    return $url;
}

?>