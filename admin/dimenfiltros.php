<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');

$idperiodo = $_GET['idperiodo'];
$agrupa = $_GET['agrupa'];

?>

<thead>
  <tr>
    <td width="250" class="corfd_coltexto" title="<?php echo $agrupa;?>"><strong><?php echo $agrupa;?></strong></td>
    <td width="81" align="center" class="corfd_coltexto"><strong>Qtde. GravaÃ§Ã£o</strong></td>
    <td width="81" align="center" class="corfd_coltexto"><strong>Qtde. Operadores</strong></td>
    <td width="72" align="center" class="corfd_coltexto" title="Multiplicador"><strong>Multiplicador</strong></td>
    <td width="61" align="center" class="corfd_coltexto" title="Estimativa"><strong>Estimativa</strong></td>
    <td width="50" align="center" class="corfd_coltexto" title="Tempo MÃ©dio de Atendimento"><strong>TMA</strong></td>
    <td width="50" align="center" class="corfd_coltexto" title="Tempo Total de Atendimento"><strong>TTA</strong></td>
    <td width="54" align="center" class="corfd_coltexto" title="Tempo de MÃ©dio Monitoria"><strong>TMM</strong></td>
    <td width="50" align="center" class="corfd_coltexto" title="Tempo Total de Monitoria"><strong>TTM</strong></td>
    <td width="62" align="center" class="corfd_coltexto" title="Quantidade de PA por dia"><strong>Qtde.PA. DIA</strong></td>
    <td width="56" align="center" class="corfd_coltexto" title="Quantidade de Monitores"><strong>Qtde. Monitores</strong></td>
    <td width="54" align="center" class="corfd_coltexto" title="Horas por Monitor"><strong>Hs. Monitor</strong></td>
    <td width="57" align="center" class="corfd_coltexto" title="Meta Monitor"><strong>Meta Monitor</strong></td>
    <td width="47" align="center" class="corfd_coltexto" title="Percentual de ParticipaÃ§Ã£o na PA"><strong>Perc. PA %</strong></td>
    <td width="51" align="center" class="corfd_coltexto" title="Percentual de ParticipaÃ§Ã£o na Meta"><strong>Perc. Meta %</strong></td>
    <td width="87" align="center" class="corfd_coltexto" title="Data do Cadastro"><strong>Data Cad.</strong></td>
  </tr>
</thead>
<tbody>
    <?php
    $seldados = "SELECT fd.idfiltro_dados, nomefiltro_dados FROM filtro_dados fd INNER JOIN filtro_nomes fn ON fn.idfiltro_nomes = fd.idfiltro_nomes WHERE fn.nomefiltro_nomes='$agrupa'";
    $eseldados = $_SESSION['query']($seldados) or die ("erro na query de consulta dos nomes dos dados");
    while($lseldados = $_SESSION['fetch_array']($eseldados)) {
        $cselrels = "SELECT COUNT(DISTINCT(dm.idrel_filtros)) as result FROM dimen_mod dm
                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                    WHERE rf.id_".strtolower($agrupa)."='".$lseldados['idfiltro_dados']."'";
        $ecrels = $_SESSION['fetch_array']($_SESSION['query']($cselrels)) or die ("erro na query de consulta da quantidade de relacionamentos no dimensionamento");
        $clinhas = $ecrels['result'];
        $seldimen = "SELECT dm.idrel_filtros, pm.tipomoni, SUM(dm.qnt_oper) as qnt_oper, SUM(dm.qnt_grava) as qnt_grava,AVG(dm.multiplicador) as multiplicador,
                    SUM(dm.estimativa) as estimativa,SEC_TO_TIME(SUM(TIME_TO_SEC(dm.tma))) as tma,SEC_TO_TIME(SUM(TIME_TO_SEC(dm.tta))) as tta,SEC_TO_TIME(SUM(TIME_TO_SEC(dm.tm))) as tm,
                    SEC_TO_TIME(SUM(TIME_TO_SEC(dm.ttm))) as ttm,SUM(dm.qnt_pad) as qnt_pad,SUM(dm.qnt_monitores) as qnt_monitores,SEC_TO_TIME(SUM(TIME_TO_SEC(dm.hs_monitor))) as hs_monitor,SUM(dm.meta_monitor) as meta_monitor,SUM(dm.percpartpa) as percpartpa,SUM(dm.percpartmeta) as percpartmeta,dm.datacad FROM dimen_mod dm
                    INNER JOIN rel_filtros rf ON rf.idrel_filtros = dm.idrel_filtros
                    INNER JOIN conf_rel cr ON cr.idrel_filtros = dm.idrel_filtros
                    INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                    WHERE dm.idperiodo='$idperiodo' AND rf.id_".strtolower($agrupa)."='".$lseldados['idfiltro_dados']."' GROUP BY rf.id_".strtolower($agrupa)."";
        $eseldimen = $_SESSION['query']($seldimen)  or die ("erro na query de consulta do dimensionamento");
        while($lseldimen = $_SESSION['fetch_array']($eseldimen)) {
              $tipomoni = $lseldimen['tipomoni'];
              ?>
              <tr>
                <td class="corfd_colcampos"><?php echo $lseldados['nomefiltro_dados'];?>
                  <?php
                    if($tipomoni == "G" && $lseldimen['qnt_oper'] != 0) {
                        echo "<img src=\"/monitoria_supervisao/images/interrogacao_16x16.png\" width=\"16\" height=\"16\" title=\"A configuraÃ§Ã£o do relacionamento estÃ¡ conflitante com o Dimenionamento, favor verificar!!!\" />";
                    }
                    if($tipomoni == "O" && $lseldimen['qnt_grava'] != 0) {
                        echo "<img src=\"/monitoria_supervisao/images/interrogacao_16x16.png\" width=\"16\" height=\"16\" title=\"A configuraÃ§Ã£o do relacionamento estÃ¡ conflitante com o Dimenionamento, favor verificar!!!\" />";
                    }
                    ?></td>
                <td class="corfd_colcampos" align="center"><?php if($tipomoni == "G") { echo $lseldimen['qnt_grava']; } else { echo "--";}?></td>
                <td class="corfd_colcampos" align="center"><?php if($tipomoni == "O") { echo $lseldimen['qnt_oper']; } else { echo "--";}?></td>
                <td class="corfd_colcampos" align="center"><?php if($tipomoni == "O") { echo $lseldimen['multiplicador']; } if($tipomoni == "G") { echo "--"; }?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['estimativa'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['tma'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['tta'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['tm'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['ttm'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['qnt_pad'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['qnt_monitores'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['hs_monitor'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['meta_monitor'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['percpartpa'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo $lseldimen['percpartmeta'];?></td>
                <td class="corfd_colcampos" align="center"><?php echo banco2data($lseldimen['datacad']);?></td>
              </tr>
              <?php
        }
    }
    ?>
</tbody>
