<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once("functionsadm.php");

$idrel = $_POST['idrel'];
$idoper = $_POST['idoper'];
$idfluxo = $_POST['idfluxo'];
$idmoni = $_POST['idmoni'];
$select = $_POST['select'];
if($idrel != "") {
    $sqlfilt[] = "rf.idrel_filtros='".$idrel."'";
}
if($idoper != "") {
    $sqlfilt[] = "o.idoperador='".$idoper."'";
}
if($select == "opermoni" OR $select == "supermoni") {
    $sqlfilt[] = "m.idmonitoria='$idmoni'";
}
else {
}


if($select == "oper") {
    echo "<option value=\"\">SELECIONE...</option>";
    $seloper = "SELECT o.idoperador, o.operador FROM operador o
                INNER JOIN monitoria m ON m.idoperador = o.idoperador
                INNER JOIN rel_filtros rf ON rf.idrel_filtros = m.idrel_filtros
                WHERE rf.idrel_filtros='$idrel' GROUP BY o.idoperador ORDER BY o.operador";
    $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consulta dos operadores");
    while($lseloper = $_SESSION['fetch_array']($eseloper)) {
        echo "<option value=\"".$lseloper['idoperador']."\">".$lseloper['operador']."</option>";
    }
}
if($select == "fluxo") {
    echo "<option value=\"\">SELECIONE...</option>";
    $selfluxo = "SELECT f.idfluxo, f.nomefluxo FROM fluxo f
                 INNER JOIN conf_rel cr ON cr.idfluxo = f.idfluxo
                 INNER JOIN rel_filtros rf ON rf.idrel_filtros = cr.idrel_filtros
                 INNER JOIN monitoria m ON m.idrel_filtros = rf.idrel_filtros
                 INNER JOIN operador o ON o.idoperador = m.idoperador
                 WHERE ".implode(" AND ",$sqlfilt)." GROUP BY f.idfluxo";
    $eselfluxo = $_SESSION['query']($selfluxo) or die ("erro na query de consulta dos fluxos relacinados aos filtros");
    while($lselfluxo = $_SESSION['fetch_array']($eselfluxo)) {
        echo "<option value=\"".$lselfluxo['idfluxo']."\">".$lselfluxo['nomefluxo']."</option>";
    }
}

if($select == "opermoni") {
    $seldados = "SELECT m.idfila, m.tabfila, m.idrel_filtros, cr.trocafiltro FROM monitoria m
                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                WHERE idmonitoria='$idmoni'";
    $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta da monitoria");
    $idfila = $eseldados['idfila'];
    $tabfila = $eseldados['tabfila'];
    $selfilt = "SELECT nomefiltro_nomes, COUNT(*) as result FROM filtro_nomes WHERE trocafiltro='S'";
    $eselfilt = $_SESSION['fetch_array']($_SESSION['query']($selfilt)) or die ("erro na query de consutla do filtro de troca");
    if($eseldados['trocafiltro'] == "S" && $eselfilt['result'] >= 1) {
        $col = "id_".strtolower(trim($eselfilt['nomefiltro_nomes']));
        $idfiltrodado = "SELECT $col FROM rel_filtros WHERE idrel_filtros='$idrel'";
        $eidreldados = $_SESSION['fetch_array']($_SESSION['query']($idfiltrodado)) or die ("erro na query de consulta do filtro_dado");
        $colval = $eidreldados[$col];
        $relfiltro = "SELECT * FROM rel_filtros rf
                      INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                      WHERE cr.ativo='S' AND rf.$col='$colval'";
        $erelfiltro = $_SESSION['query']($relfiltro) or die ("erro na query de consulta dos rel_filtros");
        while($lrelfiltro = $_SESSION['fetch_array']($erelfiltro)) {
            $idsrel[] = "'".$lrelfiltro['idrel_filtros']."'";
        }
        $data = "SELECT p.dataini, p.datafim FROM $tabfila 
                INNER JOIN upload u ON u.idupload = $tabfila.idupload
                INNER JOIN periodo p ON p.idperiodo = u.idperiodo
                WHERE id$tabfila='$idfila'";
        $edatas = $_SESSION['fetch_array']($_SESSION['query']($data)) or die ("erro na query de consulta das datas da fila");
        $dtini = $edatas['dataini'];
        $dtfim = $edatas['datafim'];
        $where[] = "$tabfila.idrel_filtros IN (".implode(",",$idsrel).")";
        $where[] = "$tabfila.dataini BETWEEN '$dtini' AND '$dtfim'";
        $seloper = "SELECT DISTINCT(o.idoperador), o.operador FROM $tabfila
                    INNER JOIN operador o ON o.idoperador = $tabfila.idoperador
                    WHERE ".implode(" AND ",$where)." ORDER BY o.operador";
        $eseloper = $_SESSION['query']($seloper) or die ("erro na query para listar os operadores");
        while($lseloper = $_SESSION['fetch_array']($eseloper)) {
            echo "<option value=\"".$lseloper['idoperador']."\">".$lseloper['operador']."</option>";
        }
    }
    else {
    }
    $seloper = "SELECT m.idoperador,o.operador FROM monitoria m
                INNER JOIN operador o ON o.idoperador = m.idoperador WHERE m.idmonitoria='$idmoni'";
    $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die ("erro na query de consulta da monitoria");
    echo "<option value=\"".$eseloper['idoperador']."\" selected=\"selected\">".$eseloper['operador']."</option>";
}

if($select == "supermoni") {
    $seldados = "SELECT m.idfila, m.tabfila, m.idrel_filtros, cr.trocafiltro FROM monitoria m
                INNER JOIN conf_rel cr ON cr.idrel_filtros = m.idrel_filtros
                WHERE idmonitoria='$idmoni'";
    $eseldados = $_SESSION['fetch_array']($_SESSION['query']($seldados)) or die ("erro na query de consulta da monitoria");
    $idfila = $eseldados['idfila'];
    $tabfila = $eseldados['tabfila'];
    $selfilt = "SELECT nomefiltro_nomes, COUNT(*) as result FROM filtro_nomes WHERE trocafiltro='S'";
    $eselfilt = $_SESSION['fetch_array']($_SESSION['query']($selfilt)) or die ("erro na query de consutla do filtro de troca");
    if($eseldados['trocafiltro'] == "S" && $eselfilt['result'] >= 1) {
        $col = "id_".strtolower(trim($eselfilt['nomefiltro_nomes']));
        $idfiltrodado = "SELECT $col FROM rel_filtros WHERE idrel_filtros='$idrel'";
        $eidreldados = $_SESSION['fetch_array']($_SESSION['query']($idfiltrodado)) or die ("erro na query de consulta do filtro_dado");
        $colval = $eidreldados[$col];
        $relfiltro = "SELECT * FROM rel_filtros rf
                      INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                      WHERE cr.ativo='S' AND rf.$col='$colval'";
        $erelfiltro = $_SESSION['query']($relfiltro) or die ("erro na query de consulta dos rel_filtros");
        while($lrelfiltro = $_SESSION['fetch_array']($erelfiltro)) {
            $idsrel[] = "'".$lrelfiltro['idrel_filtros']."'";
        }
        $data = "SELECT dataini, datafim FROM $tabfila WHERE id$tabfila='$idfila'";
        $edatas = $_SESSION['fetch_array']($_SESSION['query']($data)) or die ("erro na query de consulta das datas da fila");
        $dtini = $edatas['dataini'];
        $dtfim = $edatas['datafim'];
        $where[] = "$tabfila.idrel_filtros IN (".implode(",",$idsrel).")";
        $where[] = "$tabfila.dataini BETWEEN '$dtini' AND '$dtfim'";
        $seloper = "SELECT DISTINCT(s.idsuper_oper), s.super_oper FROM $tabfila
                    INNER JOIN super_oper s ON s.idsuper_oper = $tabfila.idsuper_oper
                    WHERE ".implode(" AND ",$where)."";
        $eseloper = $_SESSION['query']($seloper) or die ("erro na query para listar os operadores");
        while($lseloper = $_SESSION['fetch_array']($eseloper)) {
            echo "<option value=\"".$lseloper['idsuper_oper']."\">".$lseloper['super_oper']."</option>";
        }
    }
    else {
    }
    $seloper = "SELECT m.idsuper_oper,s.super_oper FROM monitoria m
                INNER JOIN super_oper s ON s.idsuper_oper = m.idsuper_oper WHERE m.idmonitoria='$idmoni'";
    $eseloper = $_SESSION['fetch_array']($_SESSION['query']($seloper)) or die ("erro na query de consulta da monitoria");
    echo "<option value=\"".$eseloper['idsuper_oper']."\" selected=\"selected\">".$eseloper['super_oper']."</option>";
}


?>
