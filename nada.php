<?php
$path_depth = "../../";

include_once($path_depth . "includes/configuration.php");

if(!isset($_SESSION['admin_login']) || $_SESSION['admin_login']!="yes"){
	$_SESSION['redirect_to']=substr($_SERVER['PHP_SELF'],strpos($_SERVER['PHP_SELF'],"administrator/") + 14);
   	$_SESSION['redirect_to_query_string']= $_SERVER['QUERY_STRING'];
	
    $_SESSION['message']="Please login to view this page!";
	$general_func->header_redirect($general_func->admin_url."index.php");
}

$invoices_print_info = $db->fetch_all_array(
                               " SELECT
                                    iv.id,
                                    iv.payment_amount,
                                    iv.order_id,
                                    orders.program_length,
                                    orders.order_type,
                                    orders.notes,
                                    users.fname,
                                    users.lname,
                                    users.phone,
                                    users.street_address,
                                    users.email_address,
                                    suburb.suburb_name,
                                    suburb.suburb_state, 
                                    suburb.suburb_postcode,
                                    suburb.order_cutoff_day,
                                    suburb.order_cutoff_time,
                                    suburb.payment_debit_day,
                                    suburb.delivery_day
                                FROM
                                    invoice iv
                                INNER JOIN users ON iv.user_id = users.id
                                INNER JOIN suburb ON users.suburb_id = suburb.id 
                                INNER JOIN orders ON iv.order_id = orders.id
                                WHERE iv.id='" . intval($_REQUEST['id']). "'");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>

<body onload="window.print();">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:0">
		  <tr>
		    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:0; border-bottom:2px solid #666;">
		        <tr>
		          <td width="50%" align="left" valign="middle" style="padding:10px;"><img src="https://www.foober.com.au/email_images/logo.jpg" style="float:left; width:150px; height:auto" alt="" /></td>
		          <td width="50%" align="left" valign="middle" style="padding:10px; color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif;"><strong>Ph: <?= $general_func->phone ?><br />
		            <a style="color:#333; text-decoration:none"><?=$general_func->email ?> </a><br />
                                                       <?=nl2br($general_func->site_address) ?></strong></td>
		        </tr>
		      </table></td>
		  </tr>
		  <tr>
		    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:0 0 10px">
		        <tr>
		          <td width="50%" align="left" valign="top" style="padding:10px"><h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0">Customer Information</h3>
		            Name : <?= $invoices_print_info[0]['fname']." ". $invoices_print_info[0]['lname']   ?>   <br />
		            Address : <?= $invoices_print_info[0]['street_address']   ?>  <br />
		            Suburbs : <?= $invoices_print_info[0]['suburb_name']." , ". $invoices_print_info[0]['suburb_state'].", ". $invoices_print_info[0]['suburb_postcode']  ?> <br />
		           </td>
		          <td align="left" valign="top" style="padding:10px">Email : <a  style="color:#333; text-decoration:none"><?= $invoices_print_info[0]['email_address']   ?></a><br />
		            Mobile : <?= $invoices_print_info[0]['phone']?></td>
		        </tr>

		        <tr>
		          <td width="50%" align="left" valign="top" style="padding:10px"><h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0">Invoice Details</h3>
		            Invoice No :  <?= $invoices_print_info[0]['id'] ?><br />
                            Order No :  FOOB - A000<?= $invoices_print_info[0]['order_id'] ?><br />
		            Invoice Amount :  $<?= $invoices_print_info[0]['payment_amount'] ?> p/w (GST 10% included)
                            <?php
                            $f = 0;
                            $friends = $db->fetch_all_array("SELECT * FROM refer_friends r"
                                    . " INNER JOIN users u ON u.email_adress r.email"
                                    . " WHERE invoice_id=".$invoices_print_info[0]['id']);
                            if(count($friends) > 0) {
                                ?>
                                <br/>Discount Referred Friends:
                                <?php
                                foreach($friends as $k => $dtfriends) {
                                    $f++;
                                    ?>
                                    <br/>
                                    Friend <?= $f?>: <?= $dtfriends['fname']?>
                                    <?php
                                }
                            }
                            ?>
                          </td>
		          <td align="left" valign="top" style="padding:10px">Cut off Date & Time :   <?=$general_func->day_name($invoices_print_info[0]['order_cutoff_day']) . " " . date("h:i A", strtotime($invoices_print_info[0]['order_cutoff_time'])); ?> <br />
		           Payment Debit Day :  <?=$general_func->day_name($invoices_print_info[0]['payment_debit_day']); ?><br />Delivery Date :<?=$general_func->day_name($invoices_print_info[0]['delivery_day']); ?> &nbsp;</td>
		        </tr>
		      </table></td>
		  </tr> <tr>
		    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:0 0 10px">
		        <tr>
		          <td width="100%" align="left" valign="top" style="padding:10px"><h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0">Additional Delivery Notes</h3><br/>
		           <?= $invoices_print_info[0]['notes'] ?>
		           </td>
		        </tr>
		      </table></td>
		  </tr> <tr>
		    <td align="left" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:0 0 10px">
		        <tr>
                            <td colspan="2" width="100%" align="left" valign="top" style="padding:10px"><h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0">Program Length</h3>
                          <?php $rs_program_length = $db->fetch_all_array("select name, details from discounts where id='" . intval($invoices_print_info[0]['program_length']) . "' limit 1");?>    
		          <strong>  <?= $rs_program_length[0]['name'] ?></strong><br/>
				   <?= $rs_program_length[0]['details'] ?>
		           </td>
		        </tr> 
                        <tr>
                            <td width="30%" align="left" valign="top" style="padding:10px">
                                <h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0; width: 30%">Week Days</h3>
                                <?php $days = $db->fetch_all_array("SELECT max(which_day) as max FROM invoice_meals o "
                                      . " INNER JOIN invoice i ON i.id = o.invoice_id WHERE i.id=" . intval($_REQUEST['id']). "");?>   
				   <?= $days[0]['max'] ?>
		           </td>
                            <td width="30%" align="left" valign="top" style="padding:10px"><h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0">How many Meals per Day</h3>
                                <?php $time = $db->fetch_all_array("SELECT count(meal_id) as time FROM invoice_meals o "
                                      . " INNER JOIN invoice i ON i.id = o.invoice_id WHERE i.id=" . intval($_REQUEST['id']). " GROUP BY which_day");?>   
                                   <?php if($invoices_print_info[0]['order_type'] == 4) { echo "A La Carte"; } else { echo $time[0]['time']; } ?>
		           </td>
                        </tr>
		      </table></td>
		  </tr> 
                <tr>
		    <td align="left" valign="top" style="border-top:2px dashed #666; padding:10px 0 0">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:0">
		        <tr>
		          <td align="left" valign="top" style="padding:10px"><h3 style="color:#000; font:bold 14px/18px Arial, Helvetica, sans-serif; margin:0; padding:0">Invoice Details</h3>
                              <?php
                              $total = 0;
                              $invoices_meals = $db->fetch_all_array(
                               " SELECT
                                     SUM(invoice_meals.total) as total,
                                     meals.`name`,extras
                                FROM
                                    invoice_meals
                                INNER JOIN meals ON meals.id = invoice_meals.meal_id
                                WHERE 
                                     invoice_meals.invoice_id = '" . intval($invoices_print_info[0]['id']). "' GROUP BY meal_id ORDER BY name");
                              
                              ?>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; margin:20px 0"> 
                               <?php for($i = 0; $i < count($invoices_meals); $i++){
                                   if($invoices_meals[$i]['extras'] != '') {
                                        $split = array();
                                        $split = explode(",",$invoices_meals[$i]['extras']);
                                        foreach($split as $ex) {
                                            if($ex != '') {
                                                $extras[trim($ex)] = $extras[trim($ex)] + $invoices_meals[$i]['total'];
                                            }
                                        }
                                   }
                                   $total = $total + $invoices_meals[$i]['total']; ?>
                                        <tr>
                                            <td><?=$invoices_meals[$i]['name']." X (".$invoices_meals[$i]['total'] ?>)</td>
                                        </tr> 
                               <?php }  ?> 
                                  
                              <?php 
                              
                              $invoices_snack = $db->fetch_all_array(
                               " SELECT
                                     SUM(iv.total) as total,
                                     snk.`name`,extras
                                FROM
                                        invoice_meals iv
                                INNER JOIN snacks snk ON iv.meal_id = snk.id
                                WHERE
                                        iv.invoice_id = '" . intval($invoices_print_info[0]['id']). "'
                                AND type = 2 GROUP BY meal_id ORDER BY name
                                "); 
                              if(count($invoices_snack) >= 1) {
                              ?>    
                                <tr>
                                    <td style="padding: 10px; padding-left: 0px"><span style="font-weight: bold;">Snacks</span></td>
                                </tr>
                               <?php 
                              }
                               for($i = 0; $i < count($invoices_snack); $i++){
                                   if($invoices_snack[$i]['extras'] != '') {
                                        $split = array();
                                        $split = explode(",",$invoices_snack[$i]['extras']);
                                        foreach($split as $ex) {
                                            if($ex != '') {
                                                $extras[trim($ex)] = $extras[trim($ex)] + $invoices_snack[$i]['total'];
                                            }
                                        }
                                   }
                                   $total = $total + $invoices_snack[$i]['total'];?>
                                        <tr>
                                            <td><?=$invoices_snack[$i]['name']." X (".$invoices_snack[$i]['total'] ?>)</td>
                                        </tr> 
                               <?php }  ?>  
                                <tr>
                                    <td style="padding: 10px; padding-left: 0px"><span style="font-weight: bold;">Extras</span></td>
                                </tr>
                                <?php
                                foreach($extras as $kname => $val) {
                                    ?>
                                  <tr>
                                      <td><?= $kname." X (".$val;?>)</td>
                                  </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td align="left" valign="top" style=" padding: 10px; padding-left: 0px; border-top: 2px dashed #333"><span style="font-weight: bold;">Total: <?=$total;?></span></td>
                                </tr>
                                
                              </table>
                                  
                          </td>
		  </tr>
		  <tr>
		    <td align="center" valign="middle" style="color:#333333; font:normal 13px/18px Arial, Helvetica, sans-serif; padding:20px 0">&copy; Copyright 2016 Foober</td>
		  </tr>
		</table>
            </td>
          </tr> 
    
    
      </table>
                        
</body>
</html>
