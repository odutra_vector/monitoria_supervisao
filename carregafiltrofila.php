<?php

$rais = str_replace("C:","",$_SERVER['DOCUMENT_ROOT']);
include_once($rais.'/monitoria_supervisao/seguranca.php');
include_once($rais.'/monitoria_supervisao/config/conexao.php');
include_once($rais.'/monitoria_supervisao/selcli.php');
include_once($rais.'/monitoria_supervisao/admin/functionsadm.php');
include_once ($rais.'/monitoria_supervisao/classes/class.tabelas.php');

$meses = array('1' => '01','2' => '02','3' => '03','4' => '04','5' => '05','6' => '06','7' => '07','8' => '08','9' => '09','10' => '10','11' => '11','12' => '12');
$idperiodo = $_POST['idperiodo'];
$idfiltro = $_POST['idfiltro'];
$iddados = $_POST['iddados'];
$nomefiltro = $_POST['nome'];
$tipo = $_POST['tipo'];
$idrelfiltro = $_POST['idrelfiltro'];
$monitor = $_POST['monitor'];
$visualiza = $_POST['visualiza'];

$tabelas = new TabelasSql();
$amoni = $tabelas->AliasTab(monitoria);
$asuper = $tabelas->AliasTab(super_oper);
$aoper = $tabelas->AliasTab(operador);
$aupload = $tabelas->AliasTab(upload);
$afilaoper = $tabelas->AliasTab(fila_oper);
$afilagrava = $tabelas->AliasTab(fila_grava);
$aupg = $tabelas->AliasTab(uploadfg);
$aupo = $tabelas->AliasTab(uploadfo);
$aper = $tabelas->AliasTab(periodo);



if($tipo == "carregafiltro") {
    echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">SELECIONE...</option>";
    $selfiltro = "SELECT * FROM filtro_dados WHERE idfiltro_nomes='$idfiltro' ORDER BY nomefiltro_dados";
    $eselfiltro = $_SESSION['query']($selfiltro) or die ("erro na consulta do filtro de dados");
    while($lselfiltro = $_SESSION['fetch_array']($eselfiltro)) {
        echo "<option value=\"".$lselfiltro['idfiltro_dados']."\">".$lselfiltro['nomefiltro_dados']."</option>";
    }
}

if($tipo == "carregarelfiltro") {
    echo "<option value=\"\" disabled=\"disabled\" selected=\"selected\">SELECIONE...</option>";
    $coluna = "id_".strtolower($nomefiltro);
    $selrel = "SELECT * FROM rel_filtros WHERE $coluna='$iddados'";
    $eselrel = $_SESSION['query']($selrel) or die ("erro na query de consulta do relacionamentos");
    while($lselrel = $_SESSION['fetch_array']($eselrel)) {
        echo "<option value=\"".$lselrel['idrel_filtros']."\">".nomeapres($lselrel['idrel_filtros'])."</option>";
    }
}

if($tipo == "filaoper") {
    $paramfila = "SELECT * FROM rel_filtros rf
                  INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros
                  INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni
                  WHERE rf.idrel_filtros='$idrelfiltro' and cr.ativo='S'";
    $eparam = $_SESSION['query']($paramfila) or die ("erro na query de consulta dos paramentros da fila");
    $nparam = $_SESSION['num_rows']($eparam);
    if($nparam >= 1) {
        $lparam = $_SESSION['fetch_array']($eparam);
        $filtrosfila['dataimp'] = explode(",",$lparam['filtro_dataimp']);
        $filtrosfila['ultmoni'] = explode(",",$lparam['filtro_ultmoni']);
        $filtrosfila['datafimimp'] = explode(",",$lparam['filtro_datafimimp']);
        $filtrosfila['diasimp'] = explode(",",$lparam['filtro_dultimp']);
        $filtrosfila['limmulti'] = explode(",",$lparam['filtro_limmulti']);
        $selsem = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
        $eselsem = $_SESSION['fetch_array']($_SESSION['query']($selsem)) or die ("erro na query de consulta do intervalo do perÃ­odo");
        $dtini = mktime(0,0,0,substr($eselsem['dataini'], 5,2),substr($eselsem['dataini'], 8,2),substr($eselsem['dataini'], 0,4));
        $dtfim = mktime(0,0,0,substr($eselsem['datafim'], 5,2),substr($eselsem['datafim'], 8,2),substr($eselsem['datafim'], 0,4));
        for(;$dtini <= $dtfim;) { 
            $dias[] = date('Y-m-d',$dtini);
            $dtini += 86400;
        }
    ?>
    
    <table width="auto">
      <tr>
        <td colspan="20" class="corfd_ntab" align="center"><strong>FILA DE MONITORIA E IMPORTACÃO</strong></td>
      </tr>
      <tr>
            <td width="50" rowspan="2" align="center" class="corfd_coltexto"><strong>ID. OPER</strong></td>
            <td width="312" align="center" class="corfd_coltexto" rowspan="2"><strong>OPERADOR</strong></td>
            <td width="50" class="corfd_coltexto" align="center" rowspan="2"><strong>ID. SUPER</strong></td>
            <td width="309" align="center" class="corfd_coltexto" rowspan="2"><strong>SUPERVISOR</strong></td>
            <td width="150" rowspan="2" class="corfd_coltexto" align="center"><strong>DATA/ HORA ULT. MONI</strong></td>
            <?php
            $csem = count($dias);
            foreach($dias as $dcol) {
                ?>
                <td colspan="3" align="center" class="corfd_coltexto"><strong><?php echo banco2data($dcol);?></strong></td>
                <?php
            }
            ?>
          </tr>
          <tr>
              <?php
                foreach($dias as $ddados) {
                    ?>
                    <td width="51" align="center" class="corfd_coltexto"><strong>IMP.</strong></td>
                    <td width="54" align="center" class="corfd_coltexto"><strong>PROD.</strong></td>
                    <td width="51" align="center" class="corfd_coltexto"><strong>IMPROD.</strong></td>
                    <?php
                }
                ?>
          </tr>
    <?php
    $i = 0;
    if($filtrosfila['dataimp'][0] == "S") {
        $orderfg[$filtrosfila['ultmoni'][1]] = "$aupg.dataimp DESC";
        $orderfo[$filtrosfila['ultmoni'][1]] = "$aupo.dataimp DESC";
    }
    if($filtrosfila['ultmoni'][0] == "S") {
        $orderfg[$filtrosfila['ultmoni'][1]] = "$amoni.data";
        $orderfo[$filtrosfila['ultmoni'][1]] = "$amoni.data";
    }
    if($filtrosfila['datafimimp'][0] == "S") {
        $diasimp = $filtrosfila['diasimp'][0];
        $consdult = "SELECT dataimp FROM upload WHERE idrel_filtros='$idrelfiltro' ORDER BY idupload DESC LIMIT 1";
        $econsdsult = $_SESSION['query']($consdult) or die ("erro na query para consulta o ultimo ulpoad");
        $nconsult = $_SESSION['num_rows']($econsdsult);
        if($nconsult >= 1) {
            $lconsult = $_SESSION['fetch_array']($econsdsult);
            if($lconsult['dataimp'] == "0000-00-00") {

            }
            else {
                $dult = mktime(0,0,0,substr($lconsult['dataimp'],5,2),substr($lconsult['dataimp'],8,2),substr($lconsult['dataimp'],0,4));
                for($i = 0; $i < $diasimp; $i++) {
                    $dult -= 86400;
                }
                $dult = date('Y-m-d',$dult);
                $where[$filtrosfila['datafimimp'][0]] = "$afilagrava.datactt >= '$dult'";
            }
        }
        else {
        }
    }
    if($where != "") {
        $wheresql = "AND ".implode(",",$where);
    }
    $cdatas = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
    $edatas = $_SESSION['fetch_array']($_SESSION['query']($cdatas)) or die ("erro na quey de consulta das dadtas");
    $datas[] = $edatas['dataini'];
    $datas[] = $edatas['datafim'];
    $filas = array('fila_grava','fila_oper');
    $i = 0;
    foreach($filas as $fila) {
        if($fila == "fila_grava") {
            if($orderfg == "") {
            }
            else {
                $orderby = "ORDER BY ".implode(",",$orderfg);
            }
            $seloper = "SELECT $aupg.dataimp, $aoper.idoperador, $aoper.operador, $asuper.idsuper_oper, $asuper.super_oper, COUNT(DISTINCT($afilagrava.idfila_grava)) as registrofg
                          FROM operador $aoper
                          INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $aoper.idsuper_oper
                          INNER JOIN fila_grava $afilagrava ON $afilagrava.idoperador = $aoper.idoperador
                          INNER JOIN upload $aupg ON $aupg.idupload = $afilagrava.idupload
                          INNER JOIN periodo $aper ON $aper.idperiodo = $aupg.idperiodo
                          WHERE $aper.datafim BETWEEN '$datas[0]' AND '$datas[1]' AND $afilagrava.idrel_filtros='$idrelfiltro' $wheresql GROUP BY $aoper.idoperador $orderby";
            //echo $seloper;
        }
        else {
            if($orderfo == "") {
            }
            else {
                $orderby = "ORDER BY ".implode(",",$orderfo);
            }
            $seloper = "SELECT $aoper.idoperador, $aoper.operador, $asuper.idsuper_oper, $asuper.super_oper, COUNT(DISTINCT($afilaoper.idfila_oper)) as registrofo
                          FROM operador $aoper
                          INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $aoper.idsuper_oper
                          INNER JOIN fila_oper $afilaoper ON $afilaoper.idoperador = $aoper.idoperador
                          INNER JOIN upload $aupg ON $aupg.idupload = $afilaoper.idupload
                          INNER JOIN periodo $aper ON $aper.idperiodo = $aupg.idperiodo
                          WHERE $aper.datafim BETWEEN '$datas[0]' AND '$datas[1]' AND $afilaoper.idrel_filtros='$idrelfiltro' GROUP BY $aoper.idoperador $orderby";
        }
        $eseloper = $_SESSION['query']($seloper) or die ("erro na query de consulta da fila");
        $noper = $_SESSION['num_rows']($eseloper);
        if($noper >= 1) {
            while($lseloper = $_SESSION['fetch_array']($eseloper)) {
                if(key_exists($lseloper['idoperador'], $idsoper)) {
                }
                else {
                    $idsoper[$i]['idoper'] = $lseloper['idoperador'];
                    $idsoper[$i]['operador'] = $lseloper['operador'];
                    $idsoper[$i]['idsuper'] = $lseloper['idsuper_oper'];
                    $idsoper[$i]['super'] = $lseloper['super_oper'];
                }
                $i++;
            }
        }
        else {
        }
    }
    if($orderfg != "" OR $orderfo != "") {
    }
    else {
        ksort($idsoper);
    }
    foreach($idsoper as $idoper) {
        ?>
      <tr>
        <?php
        $selult = "SELECT data, horafim FROM monitoria WHERE idoperador='".$idoper['idoper']."' ORDER BY idmonitoria DESC LIMIT 1";
        $eselult = $_SESSION['query']($selult) or die ("erro na query de consulta da ultima monitoria");
        $nselult = $_SESSION['num_rows']($eselult);
        $lselult = $_SESSION['fetch_array']($eselult);
        if($nselult >= 1) {
            $ult = banco2data($lselult['data'])." - ".$lselult['horafim'];
        }
        else {
            $ult = "--";
        }
        ?>
          <td class="corfd_colcampos" align="center"><?php echo $idoper['idoper'];?></td>
          <td class="corfd_colcampos" align="center"><?php echo $idoper['operador'];?></td>
          <td class="corfd_colcampos" align="center"><?php echo $idoper['idsuper'];?></td>
          <td class="corfd_colcampos" align="center"><?php echo $idoper['super'];?></td>
          <td class="corfd_colcampos" align="center"><?php echo $ult;?></td>
      <?php
      $sema = 0;
      foreach($dias as $d) {
          $sema++;
          foreach($filas as $f) {
              if($f == "fila_grava") {
                  $selopersem = "SELECT o.idoperador, o.operador, so.idsuper_oper, so.super_oper, COUNT(DISTINCT(fg.idfila_grava)) as imp
                              FROM operador o
                              INNER JOIN super_oper so ON so.idsuper_oper = o.idsuper_oper
                              LEFT JOIN fila_grava fg ON fg.idoperador = o.idoperador
                              INNER JOIN upload u ON u.idupload = fg.idupload
                              WHERE u.dataimp='$d' AND o.idoperador='".$idoper['idoper']."' AND fg.idrel_filtros='$idrelfiltro' GROUP BY o.idoperador";
              }
              if($f == "fila_oper") {
                  $selopersem = "SELECT o.idoperador, o.operador, so.idsuper_oper, so.super_oper, COUNT(DISTINCT(fo.idfila_oper)) as imp
                              FROM operador o
                              INNER JOIN super_oper so ON so.idsuper_oper = o.idsuper_oper
                              LEFT JOIN fila_oper fo ON fo.idoperador = o.idoperador
                              INNER JOIN upload u ON u.idupload = fo.idupload
                              WHERE u.dataimp='$d' AND o.idoperador='".$idoper['idoper']."' AND fo.idrel_filtros='$idrelfiltro' GROUP BY o.idoperador";
              }
              $eopersem = $_SESSION['query']($selopersem) or die ("erro na query de consulta da fila");
              $nopersem = $_SESSION['num_rows']($eopersem);
              if($nopersem >= 1) {
                $lopersem = $_SESSION['fetch_array']($eopersem);
                $imp = $imp + $lopersem['imp'];
              }
              else {
                  $imp = $imp + 0;
              }
          }
          $imptt[$d] = $imptt[$d] + $imp;
          $selqtde = "SELECT COUNT(DISTINCT(idmonitoria)) as qtde FROM monitoria WHERE idrel_filtros='$idrelfiltro' AND idoperador='".$idoper['idoper']."' AND data='$d'";
          $eselqtde = $_SESSION['fetch_array']($_SESSION['query']($selqtde)) or die ("erro na query de consulta da quantidade de monitorias");
          $qtde = $eselqtde['qtde'];
          $qtdett[$d] = $qtdett[$d] + $qtde;
          $selreg = "SELECT COUNT(DISTINCT(idregmonitoria)) as qtde FROM regmonitoria WHERE idrel_filtros='$idrelfiltro' AND idoperador='".$idoper['idoper']."' AND data='$d'";
          $eselreg = $_SESSION['fetch_array']($_SESSION['query']($selreg)) or die ("erro na query de consulta dos registros");
          $qtdereg = $eselreg['qtde'];
          $qtdettreg[$d] = $qtdettreg[$d] + $qtdereg;
          ?>
          <td class="corfd_colcampos" align="center"><?php echo $imp;?></td>
          <td class="corfd_colcampos" align="center"><?php echo $qtde;?></td>
          <td class="corfd_colcampos" align="center"><?php echo $qtdereg;?></td>
          <?php
          $imp = 0;
        }
        ?>
      </tr>
      <?php
    }
    ?>
      <tr>
          <td colspan="5" align="center" class="corfd_coltexto"><strong>TOTAL</strong></td>
          <?php
          $csem = 0;
          foreach($dias as $dtt) {
              $csem++;
              ?>
            <td align="center" class="corfd_colcampos"><?php echo $imptt[$dtt];?></td>
            <td align="center" class="corfd_colcampos"><?php echo $qtdett[$dtt];?></td>
            <td align="center" class="corfd_colcampos"><?php echo $qtdettreg[$dtt];?></td>
            <?php
          }
          ?>
      </tr>
      </table>
<?php
    }
    else {
        ?>
        <table width="600" align="center" bgcolor="#FFFFFF">
            <tr>
                <td style="color:#F00; text-align:center"><strong>NÃO EXISTE CONFIGURAÇÃO CADASTRADA PARA ESTE RELACIONAMENTO</strong></td>
            </tr>
        </table>
        <?php
    }
    ?>
    <script type="text/javascript">
         $.unblockUI();
    </script>
    <?php
}

if($tipo == "filarel") {
    if($idperiodo == "") {
        ?>
        <script type="text/javascript">
            $.unblockUI();
        </script>
        <?php
    }
    else {
        $cdatas = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
	$eselsem = $_SESSION['fetch_array']($_SESSION['query']($cdatas)) or die ("erro na query de consulta do intervalo do perÃ­odo");
	$dt = 0;
        if($eselsem['dtinictt'] == "") {
            $dtloop = substr($eselsem['dataini'],0,4)."-".substr($eselsem['dataini'],5,2)."-01";
            $dtfimloop = ultdiames(substr($eselsem['datafim'],8,2).substr($eselsem['datafim'],5,2).substr($eselsem['datafim'],0,4));
        }
        else {
            $dtloop = $eselsem['dtinictt'];
            $dtfimloop = $eselsem['dtfimctt'];
        }
	for(;$dt == 0;) {
            $comploop = "SELECT ('$dtloop' <= '$dtfimloop') as result, ADDDATE('$dtloop',interval 1 day) as newdata";
            $ecomploop = $_SESSION['fetch_array']($_SESSION['query']($comploop)) or die ("erro na query de comparação do loop");
            if($ecomploop['result'] >= 1) {
                $dias[] = $dtloop;
                $dtloop = $ecomploop['newdata'];
                $dt = 0;
            }
            else {
                $dt = 1;
            }
	}
        $dados .= "<table width=\"auto\">";
          $dados .= "<tr>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td class=\"corfd_ntab\" colspan=\"".((count($dias) * 3) + 1)."\"><strong>FILA DE MONITORIA E IMPORTAÇÃO</strong></td>";
          $dados .= "</tr>";
          $dados .= "<tr>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            $dados .= "<td></td>";
            foreach($dias as $d) {
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\" colspan=\"3\">".banco2data($d)."</td>";
            }
          $dados .= "</tr>";
          $dados .= "<tr>";
          $dados .= "<td></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\"><strong>T.I</strong></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\"><strong>T.R</strong></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\"><strong>T.IP</strong></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\" width=\"20\"><strong>EST.</strong></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\" width=\"20\"><strong>EST. D-1</strong></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\" width=\"20\"><strong>EST. D-2</strong></td>";
              $dados .= "<td class=\"corfd_coltexto\" align=\"center\" width=\"20\"><strong>EST. D-3</strong></td>";
              foreach($dias as $dcol) {
                  $dados .= "<td class=\"corfd_coltexto\" align=\"center\">I.</td>";
                  $dados .= "<td class=\"corfd_coltexto\" align=\"center\">R.</td>";
                  $dados .= "<td class=\"corfd_coltexto\" align=\"center\">IP.</td>";
              }
          $dados .= "</tr>";
          $totaldia = array();
          $diasmenos = "SELECT ADDDATE(NOW(),interval - 3 day) as data";
          $emenosdias = $_SESSION['fetch_array']($_SESSION['query']($diasmenos));
          $dataini = date('Y-m-d');
          $loopdt = 0;
          $cdias = 0;
          $feriados = getFeriados("2012");
          for(;$loopdt == 0;) {
              $reduz = "SELECT ADDDATE('$dataini',interval - 1 day) as data";
              $ereduz = $_SESSION['fetch_array']($_SESSION['query']($reduz)) or die (mysql_error());
              $dataini = $ereduz['data'];
              if(jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($ereduz['data'],5,2), substr($ereduz['data'],8,2), substr($ereduz['data'],0,4))) == 0 OR jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($ereduz['data'],5,2), substr($ereduz['data'],8,2), substr($ereduz['data'],0,4))) == 6) {
              }
              else {
                  if(in_array(substr($ereduz['data'],5,2)."-".$ereduz['data'],8,2,$feriados)) {
                  }
                  else {
                    $verifprox = "SELECT ADDDATE('".$ereduz['data']."',interval + 1 day) as data";
                    $eprox = $_SESSION['fetch_array']($_SESSION['query']($verifprox)) or die (mysql_error());
                    if(jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($eprox['data'],5,2), substr($eprox['data'],8,2), substr($eprox['data'],0,4))) == 6 && !in_array(substr($eprox['data'],5,2)."-".$eprox['data'],8,2,$feriados)) {
                        $dtdmenos[] = $ereduz['data'].",".$eprox['data'];
                    }
                    else {
                        if(in_array(substr($eprox['data'],5,2)."-".$eprox['data'],8,2,$feriados)) {
                            $proxsab = "SELECT ADDDATE('".$eprox['data']."',interval + 1 day) as data";
                            $eproxsab = $_SESSION['fetch_array']($_SESSION['query']($proxsab)) or die (mysql_error());
                            if(jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($eproxsab['data'],5,2), substr($eproxsab['data'],8,2), substr($eproxsab['data'],0,4))) == 6) {
                                $dtdmenos[] = $ereduz['data'].",".$eproxsab['data'];
                            }
                            else {
                                $dtdmenos[] = $ereduz['data'];
                            }
                        }
                        else {
                            $dtdmenos[] = $ereduz['data'];
                        }
                    }
                    $cdias++;
                  }
              }
              if($cdias >= 3) {
                  $loopdt = 1;
              }
              
          }
          $iduser = $_SESSION['usuarioID']."-".$_SESSION['user_tabela'];
          $idsrel = arvoreescalar('',$iduser);
          $selreluser = "SELECT * FROM useradmfiltro ua
                        INNER JOIN conf_rel cr ON cr.idrel_filtros=ua.idrel_filtros
                        WHERE iduser_adm='".$_SESSION['usuarioID']."' and cr.ativo='S'";
          $eselrel = $_SESSION['query']($selreluser) or die ("erro na query de consulta dos relacionamentos vinculados aos usuários");
          foreach($idsrel as $idrel) {
            $est_dmenos = array();
            $selativo = "SELECT cr.ativo,pm.tipomoni, COUNT(*) as result FROM rel_filtros rf INNER JOIN conf_rel cr ON cr.idrel_filtros = rf.idrel_filtros 
                                INNER JOIN param_moni pm ON pm.idparam_moni = cr.idparam_moni 
                                WHERE rf.idrel_filtros='$idrel'";
            $eselativo = $_SESSION['fetch_array']($_SESSION['query']($selativo)) or die ("erro na query de consulta da configuração do relacionamento");
            if($eselsem['dtinictt'] == "" OR $eselsem['dtfimctt'] == "") {
                $dtinictt = "01".substr($eselsem['dataini'],5,2).substr($eselsem['dataini'],0,4);
                $dtfimctt = ultdiames($dtinictt);
                $dtiniest = substr($eselsem['dataini'],0,4)."-".substr($eselsem['dataini'],5,2)."-01";
            }
            else {
                $dtiniest = $eselsem['dtinictt'];
                $dtfimctt = $eselsem['dtfimctt'];
            }
            if($eselativo['tipomoni'] == "O") {
                $wheredtctt = "";
            }
            else {
                $wheredtctt = "AND fg.datactt BETWEEN '$dtiniest' AND '$dtfimctt'";
            }
            $totalimp = "SELECT COUNT(DISTINCT(idfila_grava)) as result FROM fila_grava fg
                        INNER JOIN upload u ON u.idupload = fg.idupload 
                        WHERE fg.idrel_filtros='$idrel' AND fg.datactt BETWEEN '".$dtiniest."' AND '".$dtfimctt."'";
            $etotalimp = $_SESSION['fetch_array']($_SESSION['query']($totalimp)) or die ("erro na query de consulta da quantidade importada");
            if($etotalimp['result'] == "") {
                $timp = "0";
            }
            else {
                $timp = $etotalimp['result'];
            }
            if(($eselativo['ativo'] == "N" && $timp == 0) || $eselativo['result'] == 0) {
            }
            else {
                $dados .= "<tr>";
                $dados .= "<td class=\"corfd_coltexto\"><strong>".nomevisu($idrel)."</strong></td>";
                $ttimp = $ttimp + $timp;
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$timp."</td>";
                $totalmoni = "SELECT COUNT(idmonitoria) as result FROM monitoria fg WHERE idrel_filtros='$idrel' $wheredtctt";
                $etmoni = $_SESSION['fetch_array']($_SESSION['query']($totalmoni)) or die ("erro na query de consulta do total de monitorias");
                $ttmoni = $ttmoni + $etmoni['result'];
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$etmoni['result']."</td>";
                $totalimpro = "SELECT COUNT(DISTINCT(idfila)) as result FROM regmonitoria r 
                               INNER JOIN fila_grava fg ON fg.idfila_grava = r.idfila 
                               WHERE r.idrel_filtros='$idrel' $wheredtctt";
                $etimpro = $_SESSION['fetch_array']($_SESSION['query']($totalimpro)) or die ("erro na query de consulta do total de improdutirivade");
                $ttimpro = $ttimpro + $etimpro['result'];
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$etimpro['result']."</td>";
                foreach($dtdmenos as $dt) {
                    $datas = explode(",",$dt);
                    if(count($datas) >= 2) {
                        $wdata = "datactt BETWEEN '$datas[0]' AND '$datas[1]'";
                    }
                    else {
                        $wdata = "datactt = '$datas[0]'";
                    }
                    $seldisp = "SELECT *, COUNT(*) as result FROM fila_grava fg
                                INNER JOIN upload u ON u.idupload = fg.idupload 
                                where fg.idrel_filtros='$idrel' AND (monitorado='0' OR monitorando='0') AND $wdata";
                    $eseldisp = $_SESSION['fetch_array']($_SESSION['query']($seldisp)) or die ("erro na query de consulta da disponibilidade");
                    $est_dmenos[] = $eseldisp['result'];
                }
                $selest = "SELECT COUNT(DISTINCT(idfila_grava)) as result FROM fila_grava fg
                           INNER JOIN upload u ON u.idupload = fg.idupload
                           WHERE fg.idrel_filtros='$idrel' AND (monitorando='0' OR monitorado='0')
                           AND fg.datactt BETWEEN '$dtiniest' AND '$dtfimctt'";
                $eselest = $_SESSION['fetch_array']($_SESSION['query']($selest)) or die ("erro na query de consulta da quantidade em estoque");
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$eselest['result']."</td>";
                foreach($est_dmenos as $est) {
                    $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$est."</td>";
                }
                foreach($dias as $dia) {
                    $qtde = 0;
                    $qtdeimp = 0;
                    $imp = 0;
                    $selimp = "SELECT COUNT(*) as qtde FROM fila_grava fg
                               INNER JOIN upload u on u.idupload = fg.idupload 
                               WHERE fg.idrel_filtros='$idrel' AND datactt='$dia'";
                    $eselimp = $_SESSION['fetch_array']($_SESSION['query']($selimp)) or die ("erro na query de consulta da quantidade importada");
                    if($eselimp['qtde'] >= 1) {
                        $imp = $eselimp['qtde'];
                    }
                    else {
                        $imp = 0;
                    }
                    $totaldia[$dia]['I'] = $totaldia[$dia]['I'] + $imp;
                    $filas = array('fila_grava' => 'fg','fila_oper' => 'fo');
                    //foreach($filas as $kfila => $fila) {
                    $selmoni = "SELECT COUNT(idmonitoria) as qtde FROM monitoria m
                                INNER JOIN fila_grava fg on fg.idfila_grava = m.idfila
                                WHERE m.idrel_filtros='$idrel' AND m.datactt='$dia'";
                    $eselmoni = $_SESSION['fetch_array']($_SESSION['query']($selmoni)) or die ("erro na query de consulta das monitorias realizadas");
                    $qtde = $qtde + $eselmoni['qtde'];
                    $totaldia[$dia]['R'] = $totaldia[$dia]['R'] + $eselmoni['qtde'];
                    $selimprod = "SELECT COUNT(DISTINCT(idfila)) as qtde FROM regmonitoria r
                                  INNER JOIN fila_grava fg ON fg.idfila_grava = r.idfila
                                  WHERE datactt='$dia' AND r.idrel_filtros='$idrel'";
                    $eselimprod = $_SESSION['fetch_array']($_SESSION['query']($selimprod)) or die ("erro na query de consulta da improdutividade");
                    $qtdeimp = $qtdeimp + $eselimprod['qtde'];
                    $totaldia[$dia]['IP'] = $totaldia[$dia]['IP'] + $eselimprod['qtde'];
                    //}
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">$imp</td>";
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$eselmoni['qtde']."</td>";
                $dados .= "<td class=\"corfd_colcampos\" align=\"center\">".$eselimprod['qtde']."</td>";
                }
              $dados .= "</tr>";
            }
          }
          $dados .= "<tr>";
            $dados .= "<td class=\"corfd_coltexto\"><strong>TOTAIS</strong></td>";
            $dados .= "<td class=\"corfd_colcampos\">$ttimp</td>";
            $dados .= "<td class=\"corfd_colcampos\">$ttmoni</td>";
            $dados .= "<td class=\"corfd_colcampos\">$ttimpro</td>";
            $estto = "SELECT *, COUNT(*) as result FROM fila_grava fg
                      INNER JOIN upload u ON u.idupload = fg.idupload 
                      where (monitorado='0' OR monitorando='0') $wheredtctt";
            $exeesto = $_SESSION['fetch_array']($_SESSION['query']($estto)) or die ("erro na query de consulta do total de monitorias disponíveis");
            $dados .= "<td class=\"corfd_colcampos\">".$exeesto['result']."</td>";
            foreach($dtdmenos as $dttotal) {
                $datas = explode(",",$dttotal);
                if(count($datas) >= 2) {
                    $wdata = "datactt BETWEEN '$datas[0]' AND '$datas[1]'";
                }
                else {
                    $wdata = "datactt = '$datas[0]'";
                }
                $estt = "SELECT *, COUNT(*) as result FROM fila_grava fg
                        INNER JOIN upload u ON u.idupload = fg.idupload 
                        where monitorado='0' AND monitorando='0' AND $wdata";
                $exeest = $_SESSION['fetch_array']($_SESSION['query']($estt)) or die ("erro na query de consulta do total de monitorias disponíveis");
                $dados .= "<td class=\"corfd_colcampos\">".$exeest['result']."</td>";
            }
            foreach($totaldia as $td) {
                $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$td['I']."</td>";
                $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$td['R']."</td>";
                $dados .= "<td align=\"center\" class=\"corfd_colcampos\">".$td['IP']."</td>";
            }
          $dados .= "</tr>";
     $dados .= "</table>";
     $dados .= "<script type=\"text/javascript\">";
         $dados .= "$.unblockUI();\n";
     $dados .= "</script>";
     echo "<table width=\"1024\">";
        echo "<tr>";
            echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=EXCEL&nome=filamonitoria\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">EXCEL</div></a></td>";
            echo "<td><a style=\"text-decoration:none; text-align: center; color: #000;\" href=\"/monitoria_supervisao/users/export.php?tipo=WORD&nome=filamonitoria\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">WORD</div></a></td>";
            echo "<td><a style=\"text-decoration:none;text-align: center; color: #000;\" target=\"blank\" href=\"/monitoria_supervisao/users/export.php?tipo=PDF&nome=filamonitoria\"><div style=\"width:200px; height: 15px; background-color: #FFCC33; border: 1px solid #333; margin:auto\">PDF</div></a></td>";
        echo "</tr>";
    echo "</table><br/>";
    $_SESSION['dadosexp'] = $dados;
    echo $dados;
    }
}

if($tipo == "filatempo") {
    $feriados = getFeriados(date("Y"));
    if($visualiza == "monitor") {
        
    }
    if($visualiza == "relacionamento") {
        $amoni = $tabelas->AliasTab(monitoria);
        $asuper = $tabelas->AliasTab(super_oper);
        $aoper = $tabelas->AliasTab(operador);
        $aupload = $tabelas->AliasTab(upload);
        $afilaoper = $tabelas->AliasTab(fila_oper);
        $afilagrava = $tabelas->AliasTab(fila_grava);
        $aup = $tabelas->AliasTab(upload);
        $aconf = $tabelas->Aliastab(conf_rel);
        $aparam = $tabelas->Aliastab(param_moni);
        $arel = $tabelas->AliasTab(rel_filtros);
        $adimenmoni = $tabelas->AliasTab(dimen_moni);
        $adimenmod = $tabelas->AliasTab(dimen);
        $acampos = $tabelas->AliasTab(camposparam);
        $acol = $tabelas->AliasTab(coluna_oper);
        $amonitor = $tabelas->AliasTab(monitor);
        $audi = $tabelas->AliasTab(auditor);

        $selper = "SELECT * FROM periodo WHERE idperiodo='$idperiodo'";
        $eselper = $_SESSION['fetch_array']($_SESSION['query']($selper)) or die ("erro na query de consulta do periodo");
        $selcon = "SELECT *, COUNT(*) as result FROM conf_rel cf
                          INNER JOIN rel_filtros rf ON rf.idrel_filtros = cf.idrel_filtros
                          INNER JOIN param_moni pm ON pm.idparam_moni = cf.idparam_moni
                          WHERE cf.idrel_filtros='$idrelfiltro'";
        $eselcon = $_SESSION['fetch_array']($_SESSION['query']($selcon)) or die (mysql_error());
        if($eselcon['result'] == 0) {
            ?>
            <table align="center">
                <tr>
                    <td style="color: red;font-weight: bold; background-color: white; font-size: 16px">O RELACIONAMENTO NÃO POSSUI CONFIGURAÇÃO CADASTRADA, NÃO SENDO POSSÍVEL EFETUAR IMPORTAÇÕES</td>
                </tr>
            </table>
            <?php
        }
        else {
        $filtrosfila['dataimp'] = explode(",",$eselcon['filtro_dataimp']);
        $filtrosfila['ultmoni'] = explode(",",$eselcon['filtro_ultmoni']);
        $filtrosfila['datafimimp'] = explode(",",$eselcon['filtro_datafimimp']);
        $filtrosfila['dultimp'] = explode(",",$eselcon['filtro_dultimp']);
        $filtrosfila['limmulti'] = explode(",",$eselcon['filtro_limmulti']);
        $filtrosfila['ordemctt'] = explode(",",$eselcon['ordemctt']);
        if($eselper['dtinictt'] == "" OR $eselper['dtfimctt'] == "") {
            $dtinictt = substr($eselper['dataini'], 0, 4)."-".substr($eselper['dataini'], 5, 2)."-01";
            $dtfimctt = ultdiames("01".substr($eselper['dataini'], 5, 2).substr($eselper['dataini'], 0, 4));
        }
        else {
            $dtinictt = $eselper['dtinictt'];
            $dtfimctt = $eselper['dtfimctt'];
        }
        $dadoswhere['datactt'] = "datactt BETWEEN '$dtinictt' AND '$dtfimctt'";
        $dtini = date('Y-m-d');
        for(;$idt < $eselcon['diasctt'];) {
            $menosdt = "SELECT ADDDATE('$dtini',-1) as data";
            $emenosdt = $_SESSION['fetch_array']($_SESSION['query']($menosdt)) or die ("erro na query de verificação das datas");
            $dtini = $emenosdt['data']; 
            $diasem = jddayofweek(cal_to_jd(CAL_GREGORIAN, substr($emenosdt['data'],5,2), substr($emenosdt['data'],8,2), substr($emenosdt['data'],0,4)));
            if($diasem == "6" OR $diasem == "0") {
                $reduz++;
            }
            else {
                if(in_array(substr($emenosdt['data'],5,2)."-".substr($emenosdt['data'],8,2),$feriados)) {
                    $reduz++;
                }
                else {
                    $idt++;
                }
            }
        }
        $dreduz =$eselcon['diasctt'] + $reduz;
        $datalim = "SELECT ADDDATE('".date('Y-m-d')."',-$dreduz) as reduz";
        $edatalim = $_SESSION['fetch_array']($_SESSION['query']($datalim)) or die ("erro na query de consulta da data limite para audios");
        if($dtlim == "6") {
            $dreduz++;
            $datalim = "SELECT ADDDATE('".date('Y-m-d')."',-$dreduz) as reduz";
            $edatalim = $_SESSION['fetch_array']($_SESSION['query']($datalim)) or die ("erro na query de consulta da data limite para audios");
        }
        else {
        }
        $dadoswhere['dtreduz'] = "datactt >= '".$edatalim['reduz']."'";
        
        // verifica os dias que se passaram desde o inicio do mês escolhido
        $diaspass = array();
        $datamoni = $amoni.".datactt BETWEEN '$dtinictt' AND '$dtfimctt' AND tmonitoria='I'";
        $sellimit = "SELECT limita_meta,estimativa, COUNT(*) as result FROM conf_rel $aconf
                            INNER JOIN dimen_mod $adimenmod ON $adimenmod.idrel_filtros = $aconf.idrel_filtros AND $adimenmod.idperiodo='".$eselper['idperiodo']."'
                            WHERE $aconf.idrel_filtros='$idrelfiltro'";
        $esellimit = $_SESSION['fetch_array']($_SESSION['query']($sellimit)) or die ("erro na query de consulta do limite de meta");
        if($esellimit['result'] == "0") {
            $dimen = 1;
        }
        $limita = explode(",",$esellimit['limita_meta']);
        $dtloop = $eselper['dataini'];
        $dt = 0;
        for(;$dt == 0;) {
            $comdt = "SELECT '$dtloop' < '".$eselper['datafim']."' as menor, '$dtloop' = '".$eselper['datafim']."' as igual";
            $ecomdt = $_SESSION['fetch_array']($_SESSION['query']($comdt)) or die ("erro na query de comparação das datas");
            if($ecomdt['menor'] >= 1 OR $ecomdt['igual'] >= 1) {
                $verifdt = jddayofweek ( cal_to_jd(CAL_GREGORIAN, substr($dtloop,5,2),substr($dtloop,8,2), substr($dtloop,0,4)), 0);
                if($verifdt == 0 OR $verifdt == 6) {
                }
                else {
                    $compatu = "SELECT '$dtloop' > '".date('Y-m-d')."' as dtatu";
                    $ecompatu = $_SESSION['fetch_array']($_SESSION['query']($compatu)) or die ("erro na query de comparação da data com a data atual");
                    if($ecompatu['dtatu'] >= 1) {
                    }
                    else {
                    $diaspass[] = $dtloop;
                    }
                }
                if($ecomdt['menor'] == 1) {
                    $dt = 0;
                    $adddt = "SELECT ADDDATE('$dtloop',interval 1 day) as data";
                    $eadddt = $_SESSION['fetch_array']($_SESSION['query']($adddt)) or die ("erro na query para acrescentar 1 dia na data");
                    $dtloop = $eadddt['data'];
                }
                if($ecomdt['igual'] == 1) {
                    $dt = 1;
                }
            }
            else {
            }
        }
        $seldias = "SELECT DATEDIFF('".date('Y-m-d')."','$dtinictt') as dif";
        $eseldias = $_SESSION['fetch_array']($_SESSION['query']($seldias)) or die ("erro na query de consulta da quatidades de dias decorridos");
        $diasdif = ($eseldias['dif'] + 1);
        $ano = date('Y');
        $diasmes = $eselper['diasprod'];
        foreach($feriados as $d) {
            $comp = "SELECT '$ano-$d' BETWEEN '$dtinictt' AND '".date('Y-m-d')."' as result";
            $ecomp = $_SESSION['fetch_array']($_SESSION['query']($comp)) or die ("erro na query de comparação da datas");
            if($ecomp['result'] >= 1) {
                $diasdif--;
            }
            else {
            }
        }
        $selrelperc = "SELECT COUNT(DISTINCT(idmonitoria)) as qtde  FROM monitoria $amoni
                            WHERE $amoni.idrel_filtros='$idrelfiltro' AND $datamoni";
        $eselperc = $_SESSION['fetch_array']($_SESSION['query']($selrelperc)) or die ("erro na query de calculo do percentual de meta atingida");
        $meta = (($esellimit['estimativa'] / $diasmes) * count($diaspass));
        $perc = round((($eselperc['qtde'] / $meta) * 100),5);
        if($limita[0] == "S") {
            if($perc > $limita[1]) {
                $limite = 1;
            }
            else {
                if($filtrosfila['dataimp'][0] == "S") {
                    $orderfg[$filtrosfila['dataimp'][1]] = "$aup.dataimp DESC";
                    $orderfo[$filtrosfila['dataimp'][1]] = "$aup.dataimp DESC";
                }
                if($filtrosfila['ultmoni'][0] == "S") {
                    $orderfg[$filtrosfila['ultmoni'][1]] = "$amoni.data";
                    $orderfo[$filtrosfila['ultmoni'][1]] = "$amoni.data";
                }
                $tabela = "fila_grava";
                $aliastab = $afilagrava;
                if($filtrosfila['ordemctt'][0] == "S") {
                    $ordem = array('C' => 'ASC','D' => 'DESC');
                    if($orderfg == "") {
                        // alterado a ordenação de ctt mais antigo para ctt mais novo 13/12/2011
                        $orderby = "ORDER BY $aliastab.datactt ".$ordem[$filtrosfila['ordemctt'][1]]."";
                    }
                    else {
                        // alterado a ordenação de ctt mais antigo para ctt mais novo 13/12/2011
                        $orderby = "ORDER BY $aliastab.datactt ".$ordem[$filtrosfila['ordemctt'][1]].",".implode(",",$orderfg);
                    }
                }
                else {
                    if($orderfg == "") {
                    }
                    else {
                        $orderby = "ORDER BY".implode(",",$orderfg);
                    }
                }
            }
        }
        else {
            if($filtrosfila['dataimp'][0] == "S") {
                $orderfg[$filtrosfila['dataimp'][1]] = "$aup.dataimp DESC";
                $orderfo[$filtrosfila['dataimp'][1]] = "$aup.dataimp DESC";
            }
            if($filtrosfila['ultmoni'][0] == "S") {
                $orderfg[$filtrosfila['ultmoni'][1]] = "$amoni.data";
                $orderfo[$filtrosfila['ultmoni'][1]] = "$amoni.data";
            }
            $tabela = "fila_grava";
            $aliastab = $afilagrava;
            if($filtrosfila['ordemctt'][0] == "S") {
                $ordem = array('C' => 'ASC','D' => 'DESC');
                if($orderfg == "") {
                    // alterado a ordenação de ctt mais antigo para ctt mais novo 13/12/2011
                    $orderby = "ORDER BY $aliastab.datactt ".$ordem[$filtrosfila['ordemctt'][1]]."";
                }
                else {
                    // alterado a ordenação de ctt mais antigo para ctt mais novo 13/12/2011
                    $orderby = "ORDER BY $aliastab.datactt ".$ordem[$filtrosfila['ordemctt'][1]].",".implode(",",$orderfg);
                }
            }
            else {
                if($orderfg == "") {
                }
                else {
                    $orderby = "ORDER BY".implode(",",$orderfg);
                }
            }
        }

        ?>
        <div width="1010">
            <table id="dadosfila" width="500">
                <tr>
                    <td class="corfd_ntab" colspan="2" align="center"><strong>PARAMETROS RELACIONAMENTO</strong></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>TIPO MONITORIA</strong></td>
                    <td class="corfd_colcampos" width="180"><?php if($eselcon['tipomoni'] == "G") { echo "GRAVAÇÃO";} if($eselcon['tipomoni'] == "O") { echo "ON-LINE";}?> </td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>LIMITE DE META</strong></td>
                    <td class="corfd_colcampos"><?php if($limita[0] == "S") { echo "Limite: $limita[1], Atingido: ".round($perc,2).""; } else { echo "NÃO POSSUI LIMITE";}?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>ORDEM DATAS</strong></td>
                    <td class="corfd_colcampos"><?php if($filtrosfila['ordemctt'][0] == "S") { echo "Ordenação: ".$filtrosfila['ordemctt'][1].""; } else { echo "Sem ordenação, padrão Crescente"; }?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>DIAS D- DATA CTT</strong></td>
                    <td class="corfd_colcampos"><?php echo $eselcon['diasctt']." dias - ".banco2data($edatalim['reduz']);?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>FILTRO DATA IMPORTAÇÃO</strong></td>
                    <td class="corfd_colcampos"><?php echo "Ativo: ".$filtrosfila['dataimp'][0].", Prioridade: ".$filtrosfila['dataimp'][1];?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>FILTRO UlTIMA MONITORIA</strong></td>
                    <td class="corfd_colcampos"><?php echo "Ativo: ".$filtrosfila['ultmoni'][0].", Prioridade: ".$filtrosfila['ultmoni'][1];?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>FILTRO DATA FINAL IMPORTAÇÃO</strong></td>
                    <td class="corfd_colcampos"><?php echo "Ativo: ".$filtrosfila['datafimimp'][0].", Prioridade: ".$filtrosfila['datafimimp'][1];?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>DIAS ULTIMA IMPORTAÇÃO</strong></td>
                    <td class="corfd_colcampos"><?php echo "Dias: ".$filtrosfila['dultimp'][0].", Prioridade: ".$filtrosfila['dultimp'][1];?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>FILTRO LIMITA MULTIPLICADOR</strong></td>
                    <td class="corfd_colcampos"><?php if($eselcon['tipomoni'] == "G") { echo "Não utilizado";} else { echo "Ativo: ".$filtrosfila['limmulti'][0].", Prioridade: ".$filtrosfila['limmulti'][1];}?></td>
                </tr>
            </table>
        <?php

        if($dimen == 1) {
            ?>
            <table align="center">
                <tr>
                    <td style="color: red;font-weight: bold; background-color: white; font-size: 16px">O RELACIONAMENTO NÃO POSSUI DIMENSIONAMENTO CADASTRADO PARA O PERÍODO SELECIONADO</td>
                </tr>
            </table>
        </div>
            <?php
        }
        if($limite == 1) {
            ?>
            <table align="center">
            <tr>
                <td style="color: red;font-weight: bold; background-color: white; font-size: 16px">A OPERAÇÃO POSSUI LIMITAÇÃO DE META E JÁ ATINGIU "<?php echo round($perc,2)."%";?>" DO/S "<?php echo $limita[1]."%";?>" DEFINIDOS</td>
            </tr>
            </table>
        </div>
            <?php
        }
        if($dimen == "" && $limite == "") {
            $trocacol = array("idoperador" => "$aoper.operador as operador", "idsuper_oper" => "$asuper.super_oper", "idauditor" => "$audi.operador as auditor",
            "idupload" => "$aliastab.idupload","idrel_filtros" => "$aliastab.idrel_filtros","hora" => "$aliastab.hora");
            $selcolunas = "SHOW COLUMNS FROM fila_grava";
            $eselcol = $_SESSION['query']($selcolunas) or die (mysql_error());
            while($lselcol = $_SESSION['fetch_array']($eselcol)) {
                if($lselcol['Field'] == "idfila_grava") {
                }
                else {
                    if(key_exists($lselcol['Field'], $trocacol)) {
                        $colunas[$trocacol[$lselcol['Field']]]['Type'] = $lselcol['Type'];
                        $colunas[$trocacol[$lselcol['Field']]]['Null'] = $lselcol['Null'];
                        $colunas[$trocacol[$lselcol['Field']]]['Key'] = $lselcol['Key'];
                    }
                    else {
                        $colunas[$lselcol['Field']]['Type'] = $lselcol['Type'];
                        $colunas[$lselcol['Field']]['Null'] = $lselcol['Null'];
                        $colunas[$lselcol['Field']]['Key'] = $lselcol['Key'];
                    }
                }
            }
           $selfila = "SELECT ".implode(",",array_keys($colunas))." FROM $tabela $aliastab
                            INNER JOIN operador $aoper ON $aoper.idoperador = $aliastab.idoperador
                            INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $aliastab.idsuper_oper
                            LEFT JOIN operador $audi ON $audi.idoperador = $aliastab.idauditor
                            LEFT JOIN upload $aup ON $aup.idupload = $aliastab.idupload
                            LEFT JOIN periodo p ON p.idperiodo = $aup.idperiodo
                            LEFT JOIN conf_rel $aconf ON $aconf.idrel_filtros = $aliastab.idrel_filtros
                            WHERE monitorando='0' AND monitorado='0' AND $aliastab.idrel_filtros='$idrelfiltro' AND ".implode(" AND ",$dadoswhere)." $orderby";
           $eselfila = $_SESSION['query']($selfila) or die (mysql_error());
           $linhas = $_SESSION['num_rows']($eselfila);
           $seltt = "SELECT COUNT(*) as result FROM $tabela $aliastab
                            INNER JOIN operador $aoper ON $aoper.idoperador = $aliastab.idoperador
                            INNER JOIN super_oper $asuper ON $asuper.idsuper_oper = $aliastab.idsuper_oper
                            LEFT JOIN operador $audi ON $audi.idoperador = $aliastab.idauditor
                            LEFT JOIN upload $aup ON $aup.idupload = $aliastab.idupload
                            LEFT JOIN periodo p ON p.idperiodo = $aup.idperiodo
                            LEFT JOIN conf_rel $aconf ON $aconf.idrel_filtros = $aliastab.idrel_filtros
                            WHERE monitorando='0' AND monitorado='0' AND $aliastab.idrel_filtros='$idrelfiltro' AND ".$dadoswhere['datactt']."";
           $eseltt = $_SESSION['fetch_array']($_SESSION['query']($seltt)) or die (mysql_error());
            ?>
        </div><br/>
        <div>
            <table width="500">
                <tr>
                    <td class="corfd_coltexto"><strong>CONTATOS DISPONÍVEIS NO PERIODO</strong></td>
                    <td class="corfd_colcampos" width="180" align="center"><?php echo $eseltt['result'];?></td>
                </tr>
                <tr>
                    <td class="corfd_coltexto"><strong>CONTATOS DISPONÍVEIS COM RESTRIÇÃO DOS FILTROS</strong></td>
                    <td class="corfd_colcampos" align="center"><?php echo $linhas;?></td>
                </tr>
                
            </table>
        </div><br/>
        <div style="overflow: auto; width: 1000px">
            <table>
                <tr>
                    <td class="corfd_ntab" align="center" colspan="20"><strong>FILA ORDENADA DO RELACIONAMENTO</strong></td>
                </tr>
                <tr>
                    <?php
                    foreach($colunas as $nomecol => $type) {
                        $splitapres = explode(" as ",$nomecol);
                        if(count($splitapres) == 2) {
                            $apres = trim($splitapres[1]);
                        }
                        else {
                            $apres = explode(".",$splitapres[0]);
                            if(count($apres) == 2) {
                                $apres = $apres[1];
                            }
                            else {
                                $apres = $apres[0];
                            }
                        }
                        ?>
                        <td class="corfd_coltexto" align="center"><strong><?php echo $apres;?></strong></td>
                        <?php
                    }
                    ?>
                </tr>
            <?php
            $linhas = $_SESSION['num_rows']($eselfila);
            if($linhas >= 1) {
            }
            else {
                ?>
                <tr>
                    <td class="corfd_colcampos" style="color: red; font-size: 14; font-weight: bold" colspan="20" align="center">NÃO FORAM ENCONTRADOS AUDIOS DISPONÍVEIS COM OS PARAMETROS CONFIGURADOS</td>
                </tr>
                <?php
            }
            while($lselfila = $_SESSION['fetch_array']($eselfila)) {
                ?>
                <tr>
                <?php
                foreach($colunas as $col => $estrutura) {
                    $partecol = explode(" as ",$col);
                    if(count($partecol) == 2) {
                        $newcol = trim($partecol[1]);
                    }
                    else {
                        $p = explode(".",$partecol[0]);
                        if(count($p) == 2) {
                            $newcol = $p[1];
                        }
                        else {
                            $newcol = $p[0];
                        }
                    }
                    if($estrutura['Type'] == "date") {
                        $val = banco2data($lselfila[$newcol]);
                    }
                    else {
                        $val = $lselfila[$newcol];
                    }
                    ?>
                    <td class="corfd_colcampos" align="center"><?php echo $val;?></td>
                    <?php
                }
                ?>
                </tr>
                <?php
            }
            ?>
            </table>
        </div>
            <?php
        }
        // fim das verificações
    }
}
?>
    <script type="text/javascript">
    $.unblockUI();
</script>
<?php
}

?>
