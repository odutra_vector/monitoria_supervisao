<?php

function checkCPF($cpf, $perfil) {
    $cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
    if($cpf == "00000000000" && $perfil == "Administrador") {
	return true;
    }
    if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '99999999999') {
        return false;
    } else {
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}

function subs_caracter($var) {
#
$var = ereg_replace("[ÁÀÂÃ]","A",$var);
#
$var = ereg_replace("[áàâãª]","a",$var);
#
$var = ereg_replace("[ÉÈÊ]","E",$var);
#
$var = ereg_replace("[éèê]","e",$var);
#
$var = ereg_replace("[ÓÒÔÕ]","O",$var);
#
$var = ereg_replace("[óòôõº]","o",$var);
#
$var = ereg_replace("[ÚÙÛ]","U",$var);
#
$var = ereg_replace("[úùû]","u",$var);
#
$var = str_replace("Ç","C",$var);
#
$var = str_replace("ç","c",$var);

return $var;
}

?>