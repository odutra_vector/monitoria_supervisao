<?php

session_start();

include_once 'securimage/securimage.php';
$img = new securimage(0,0,255);
$img->code_length = 8; // 8 ao invés de apenas 4 caracteres
$img->charset = "abcdefghklmnprstuvwyz0123456789"; // Apenas vogais
$img->image_width = 300; // Padrão é 175px para 4 caracteres
$img->image_height = 100;
$img->perturbation = 0;
$img->num_lines = 2;
$img->text_color = new Securimage_Color("#000000"); // Letras azuis e vermelhas
//$img->image_bg_color =new Securimage_Color("#FFA500"); // Fundo amarelo
$_SESSION['captcha'] = $img->check();
 
$img->show($_SERVER['DOCUMENT_ROOT'].'/monitoria_supervisao/securimage/backgrounds/bg3.jpg');

?>
